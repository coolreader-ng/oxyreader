/*
 * Color selection view
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.mycolorselectorview

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.GestureDetector
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.annotation.ColorInt
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.PopupWindowCompat
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.roundToInt

@SuppressLint("ClickableViewAccessibility", "ResourceType")
class MyColorSelectorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : LinearLayout(context, attrs, defStyleAttr) {

    interface OnColorSelected {
        fun onColorSelected(index: Int, @ColorInt color: Int)
    }

    var onColorSelectedListener: OnColorSelected? = null
    var colors: Array<Int>? = null
        set(value) {
            field = value
            populateColors(mFlexbox, field)
        }
    var color: Int
        get() = mCurrentColor
        set(value) = setCurrentColor(value)
    var overlapAnchor: Boolean = false
        get() = PopupWindowCompat.getOverlapAnchor(mDropDownPopup)
        set(value) {
            PopupWindowCompat.setOverlapAnchor(mDropDownPopup, value)
            field = value
        }
    var dropDownIsFocusable: Boolean = false
        get() = mDropDownPopup.isFocusable
        set(value) {
            field = value
            mDropDownPopup.isFocusable = value
        }

    var popupParent: View? = null

    private val mColorBoxColorStateList: ColorStateList
    private val m1dip: Float
    private val m2dip: Float
    private val m4dip: Float
    private val m32dip: Float
    private var mDropDownWidth = 0
    private var mDropDownHeight = 0
    private val mTextInputLayout: TextInputLayout
    private val mEditText: EditText
    private var mDropDownPopup: PopupWindow
    private val mFlexbox: FlexboxLayout
    private val mColorBoxes = ArrayList<MyColorBoxView>()
    private var mCurrentColorBox: MyColorBoxView? = null

    @ColorInt
    private var mCurrentColor: Int
    private val mColorBoxOnClickListener = View.OnClickListener { v ->
        if (v is MyColorBoxView) {
            if (null != v.tag) {
                val idx = v.tag as Int
                val color = v.color
                mCurrentColor = color
                mCurrentColorBox = v
                // Update text field
                updateTextField(color)
                // hide dropDown
                hideDropDown()
                // run listener
                onColorSelectedListener?.onColorSelected(idx, color)
            }
        }
    }

    private fun showDropDown() {
        val metrics = context.resources.displayMetrics
        // The program crashes if we try to use the showAsDropDown() method with API < 26
        //  when this view is hosted in another PopupWindow.
        //  In this case, the popupParent property must be set and then the showAtLocation() method will be used.
        val showAsDropDown = (null == popupParent) || (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        if (0 == mDropDownWidth) {
            mDropDownWidth =
                if (showAsDropDown) mTextInputLayout.width else 3 * metrics.widthPixels / 4
        }
        if (0 == mDropDownHeight) {
            val view = mDropDownPopup.contentView
            val widthSpec = MeasureSpec.makeMeasureSpec(mDropDownWidth, MeasureSpec.EXACTLY)
            //val maxAvailHeight = mDropDownPopup.getMaxAvailableHeight(mTextInputLayout)
            val maxAvailHeight = 3 * metrics.heightPixels / 4
            val heightSpec = MeasureSpec.makeMeasureSpec(maxAvailHeight, MeasureSpec.AT_MOST)
            view.measure(widthSpec, heightSpec)
            val height = view.measuredHeight
            var padding = 0
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                val paddingRect = Rect()
                mDropDownPopup.background?.getPadding(paddingRect)
                padding = paddingRect.top + paddingRect.bottom
            }
            mDropDownHeight = height + view.paddingTop + view.paddingBottom + padding
        }
        mDropDownPopup.width = mDropDownWidth
        mDropDownPopup.height = mDropDownHeight
        if (mDropDownPopup.isShowing) {
            mDropDownPopup.update()
            mCurrentColorBox?.requestFocus()
            return
        }
        if (showAsDropDown) {
            PopupWindowCompat.showAsDropDown(
                mDropDownPopup,
                mTextInputLayout,
                0,
                0,
                Gravity.NO_GRAVITY
            )
        } else {
            // We can't use showAsDropDown() here since it's crashed
            //  if this dropDown created on top of the other PopupWindow
            // TODO: calculate position to show popup like in showAsDropDown()
            mDropDownPopup.showAtLocation(popupParent, Gravity.CENTER, 0, 0)
        }
    }

    private fun hideDropDown() {
        if (mDropDownPopup.isShowing)
            mDropDownPopup.dismiss()
    }

    private fun showColorComposeForm() {
        // Finding suggested window size
        val metrics = resources.displayMetrics
        val screenWidth: Int
        val screenHeight: Int
        if (metrics.widthPixels > metrics.heightPixels) {
            screenWidth = metrics.heightPixels
            screenHeight = metrics.widthPixels
        } else {
            screenWidth = metrics.widthPixels
            screenHeight = metrics.heightPixels
        }
        val minWidth =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300f, metrics).toInt()
        val minHeight =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 430f, metrics).toInt()
        val maxWidth = 4 * screenWidth / 5
        val maxHeight = 2 * screenHeight / 3
        val fullScreenMode = maxWidth < minWidth || maxHeight < minHeight

        val layoutInflater = LayoutInflater.from(context)
        val formView = layoutInflater.inflate(R.layout.mcsv2_color_compose_form, null, false)
        val form = MyColorComposeForm(formView, mCurrentColor)
        val formPopup =
            PopupWindow(context, null, com.google.android.material.R.attr.popupWindowStyle)
        formPopup.contentView = formView
        formPopup.isOutsideTouchable = true
        formPopup.isTouchable = true
        formPopup.isFocusable = true    // needed for EditText controls
        form.onColorFormListener = object : MyColorComposeForm.OnColorFormListener {
            override fun onCancel() {
            }

            override fun onColorSelectionConfirmed(color: Int) {
                this@MyColorSelectorView.setCurrentColor(color)
                val index: Int = this@MyColorSelectorView.mCurrentColorBox?.tag as Int
                this@MyColorSelectorView.onColorSelectedListener?.onColorSelected(
                    index,
                    color
                )
            }

            override fun onDismiss() {
                formPopup.dismiss()
            }
        }
        val gravity: Int
        val background: Drawable?
        if (fullScreenMode) {
            formPopup.width = ViewGroup.LayoutParams.MATCH_PARENT
            formPopup.height = ViewGroup.LayoutParams.MATCH_PARENT
            gravity = Gravity.FILL
            background =
                AppCompatResources.getDrawable(context, R.drawable.mcsv2_popup_background)
        } else {
            formPopup.width = minWidth
            formPopup.height = minHeight
            gravity = Gravity.CENTER
            background =
                AppCompatResources.getDrawable(
                    context,
                    R.drawable.mcsv2_popup_background_border
                )
        }
        formPopup.setBackgroundDrawable(background)
        val popupParent =
            if (null != this.popupParent) this.popupParent else this@MyColorSelectorView
        formPopup.showAtLocation(popupParent, gravity, 0, 0)
        if (!fullScreenMode) {
            // dim screen behind this popup window
            val root = formView.rootView
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager?
            if (null != wm) {
                val layoutParams = root.layoutParams as WindowManager.LayoutParams
                layoutParams.flags =
                    layoutParams.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND
                layoutParams.dimAmount = 0.7f
                wm.updateViewLayout(root, layoutParams)
            }
        }
    }

    private fun updateEndIcon() {
        if (mDropDownPopup.isShowing) {
            mTextInputLayout.setEndIconDrawable(R.drawable.mcsv2_ic_arrow_drop_up)
        } else {
            mTextInputLayout.setEndIconDrawable(R.drawable.mcsv2_ic_arrow_drop_down)
        }
    }

    private fun updateTextField(@ColorInt color: Int) {
        mTextInputLayout.setStartIconTintList(ColorStateList.valueOf(color or 0xFF000000.toInt()))
        // On API16-API20 without setStartIconDrawable the color is not updated
        //  Note: using 'com.google.android.material:material:1.9.0'
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            mTextInputLayout.setStartIconDrawable(R.drawable.mcsv2_ic_colorbox)
        val str = ("#%06x").format(color and 0x00FFFFFF)
        mEditText.setText(str)
    }

    private fun setCurrentColor(@ColorInt color: Int) {
        var found = false
        for (box in mColorBoxes) {
            if ((color and 0x00FFFFFF) == (box.color and 0x00FFFFFF)) {
                mCurrentColorBox = box
                found = true
                break
            }
        }
        if (!found) {
            // add new color box
            val size = m32dip.roundToInt()
            val margin = m4dip.roundToInt()
            val boxLayoutParams = ViewGroup.MarginLayoutParams(size, size)
            boxLayoutParams.setMargins(margin, margin, margin, margin)
            val idx = mColorBoxes.size
            val box = buildColorBoxImpl(mFlexbox.context, color)
            box.tag = idx
            mCurrentColorBox = box
            mFlexbox.addView(box, idx, boxLayoutParams)
            mColorBoxes.add(box)
        }
        mCurrentColor = color
        if (mDropDownPopup.isShowing)
            mCurrentColorBox?.requestFocus()
        updateTextField(color)
    }

    private fun populateColors(flexbox: FlexboxLayout, colors: Array<Int>?) {
        for (box in mColorBoxes)
            flexbox.removeView(box)
        mColorBoxes.clear()
        if (null != colors) {
            val size = m32dip.roundToInt()
            val margin = m4dip.roundToInt()
            val boxLayoutParams = ViewGroup.MarginLayoutParams(size, size)
            boxLayoutParams.setMargins(margin, margin, margin, margin)
            for ((i, color) in colors.withIndex()) {
                val box = buildColorBoxImpl(flexbox.context, color)
                box.tag = i
                flexbox.addView(box, i, boxLayoutParams)
                mColorBoxes.add(box)
            }
        }
        // clear cached dropdown height to recalc it
        mDropDownHeight = 0
    }

    private fun buildColorBoxImpl(context: Context, @ColorInt color: Int): MyColorBoxView {
        val box = MyColorBoxView(context)
        box.color = color
        box.borderWidth = m2dip.roundToInt()
        box.borderColorStateList = mColorBoxColorStateList
        val gestureDetector = GestureDetector(context, FocusNClickOnTapGestureListener(box))
        box.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
        }
        box.isFocusable = true
        box.isFocusableInTouchMode = true
        box.isClickable = true
        box.setOnClickListener(mColorBoxOnClickListener)
        return box
    }

    init {
        val metrics = resources.displayMetrics
        m1dip = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, metrics)
        m2dip = 2f * m1dip
        m4dip = 4f * m1dip
        m32dip = 32f * m1dip
        orientation = HORIZONTAL
        var hintValue: String? = null
        var overlapAnchorValue: Boolean? = null
        var dropDownFocusable = false
        var colorFocused = Color.BLACK
        var colorsArray: Array<Int>? = null
        if (null != attrs) {
            val attributes = context.obtainStyledAttributes(
                attrs,
                R.styleable.MyColorSelectorView,
                defStyleAttr,
                0
            )
            overlapAnchorValue =
                attributes.getBoolean(R.styleable.MyColorSelectorView_dropDownOverlapAnchor, false)
            dropDownFocusable =
                attributes.getBoolean(R.styleable.MyColorSelectorView_dropDownFocusable, false)
            val colorsResId = attributes.getResourceId(R.styleable.MyColorSelectorView_colors, 0)
            if (0 != colorsResId) {
                val colorsTypedArray = attributes.resources.obtainTypedArray(colorsResId)
                colorsArray =
                    Array(colorsTypedArray.length()) { i -> colorsTypedArray.getColor(i, 0) }
                colorsTypedArray.recycle()
            }
            attributes.recycle()
            val attributes2 = context.obtainStyledAttributes(
                attrs,
                intArrayOf(
                    android.R.attr.hint,
                    android.R.attr.colorFocusedHighlight,
                ),
                defStyleAttr,
                0
            )
            hintValue = attributes2.getString(0)
            colorFocused = attributes2.getColor(1, colorFocused)
            attributes2.recycle()
        }
        mColorBoxColorStateList = ColorStateList(
            arrayOf(intArrayOf(android.R.attr.state_focused), intArrayOf()),
            intArrayOf(
                colorFocused,
                Color.TRANSPARENT
            )
        )
        // Inflate view
        val layoutInflater = LayoutInflater.from(context)
        val view = layoutInflater.inflate(R.layout.mcsv2_view_container, this, true)
        // controls
        mTextInputLayout = view.findViewById(R.id.mcsv2_textInputLayout)
        mTextInputLayout.hint = hintValue
        mEditText = view.findViewById(R.id.mcsv2_inputText)

        mTextInputLayout.setEndIconOnClickListener {
            mEditText.requestFocus()
            showDropDown()
        }
        mTextInputLayout.setStartIconOnClickListener {
            mEditText.requestFocus()
            showDropDown()
        }
        mEditText.setOnClickListener {
            showDropDown()
        }
        // By default, for the first single tap, the input focus is first set, and the "OnClick" event is not fired
        // To immediately set the input focus and trigger the "OnClick" event, we use a GestureDetector
        val gestureDetector = GestureDetector(context, FocusNClickOnTapGestureListener(mEditText))
        mEditText.setOnTouchListener { _, event ->
            gestureDetector.onTouchEvent(event)
        }
        // inflate drop down
        val dropDownView = layoutInflater.inflate(R.layout.mcsv2_dropdown_container, null, false)
        mFlexbox = dropDownView.findViewById(R.id.mcsv2_flexbox)
        mDropDownPopup =
            PopupWindow(context, null, com.google.android.material.R.attr.popupMenuStyle)
        mDropDownPopup.contentView = dropDownView
        mDropDownPopup.isOutsideTouchable = true
        mDropDownPopup.isTouchable = true
        mDropDownPopup.isFocusable = dropDownFocusable
        if (null != overlapAnchorValue)
            overlapAnchor = overlapAnchorValue
        mDropDownPopup.setOnDismissListener {
            updateEndIcon()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            mDropDownPopup.isTouchModal = true
        dropDownView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                mCurrentColorBox?.requestFocus()
                // On some devices (API 23?), when the dropDown popup appears,
                //  it always sets focus to the first element, ignoring
                //  already set focus on another element
                // Therefore sets focus to the desired element with a slight delay
                //  to fix this bogus behavior
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M)
                    postDelayed({ mCurrentColorBox?.requestFocus() }, 200)
                updateEndIcon()
            }

            override fun onViewDetachedFromWindow(v: View) {
            }
        })
        colors = colorsArray    // implicitly call populateColors()
        mCurrentColor = Color.BLACK
        val btnColorForm = dropDownView.findViewById<Button>(R.id.mcsv2_btnColorForm)
        btnColorForm.setOnClickListener {
            hideDropDown()
            showColorComposeForm()
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val superState = super.onSaveInstanceState()
        val bundle = Bundle()
        bundle.putParcelable("superState", superState)
        bundle.putInt("color", mCurrentColor)
        return bundle
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is Bundle) {
            val superState: Parcelable? = state.getParcelable("superState")
            super.onRestoreInstanceState(superState)
            setCurrentColor(state.getInt("color"))
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mDropDownWidth = 0
        mDropDownHeight = 0
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        if (mDropDownPopup.isShowing)
            mDropDownPopup.dismiss()
    }
}