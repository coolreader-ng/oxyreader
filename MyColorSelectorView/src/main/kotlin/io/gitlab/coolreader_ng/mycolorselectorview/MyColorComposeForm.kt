/*
 * Color selection view
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.mycolorselectorview

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.annotation.ColorInt
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import io.gitlab.coolreader_ng.textvalidators.IntValidator
import io.gitlab.coolreader_ng.textvalidators.RegExpValidator
import java.text.NumberFormat
import java.util.regex.Pattern
import kotlin.math.roundToInt


class MyColorComposeForm(contentView: View, @ColorInt color: Int) {

    interface OnColorFormListener {
        fun onCancel()
        fun onColorSelectionConfirmed(@ColorInt color: Int)
        fun onDismiss()
    }

    var onColorFormListener: OnColorFormListener? = null

    private var mCurrentColor: Int = 0x000000

    private val mContext: Context
    private val mRedValueEditText: EditText
    private val mGreenValueEditText: EditText
    private val mBlueValueEditText: EditText
    private val mSliderRed: Slider
    private val mSliderGreen: Slider
    private val mSliderBlue: Slider
    private val mBtnRedDec: Button
    private val mBtnRedInc: Button
    private val mBtnGreenDec: Button
    private val mBtnGreenInc: Button
    private val mBtnBlueDec: Button
    private val mBtnBlueInc: Button
    private val mColorBox: MyColorBoxView
    private val mHexColorEditText: EditText

    private fun propagateChanges() {
        // mCurrentColor is changed: update the value/content of all controls (if necessary)
        // Always check the current value of the controls to avoid endless recursion
        mColorBox.color = mCurrentColor or 0xFF000000.toInt()
        val red = (mCurrentColor and 0xFF0000).shr(16)
        val green = (mCurrentColor and 0x00FF00).shr(8)
        val blue = mCurrentColor and 0x0000FF
        val redStr = red.toString()
        val greenStr = green.toString()
        val blueStr = blue.toString()
        if (mSliderRed.value.roundToInt() != red)
            mSliderRed.value = red.toFloat()
        if (mSliderGreen.value.roundToInt() != green)
            mSliderGreen.value = green.toFloat()
        if (mSliderBlue.value.roundToInt() != blue)
            mSliderBlue.value = blue.toFloat()
        mBtnRedDec.isEnabled = red > 0
        mBtnRedInc.isEnabled = red < 255
        mBtnGreenDec.isEnabled = green > 0
        mBtnGreenInc.isEnabled = green < 255
        mBtnBlueDec.isEnabled = blue > 0
        mBtnBlueInc.isEnabled = blue < 255
        if (mRedValueEditText.text.toString() != redStr) {
            var pos = mRedValueEditText.selectionStart
            if (pos >= redStr.length)
                pos = redStr.length - 1
            mRedValueEditText.setText(redStr)
            if (pos >= 0)
                mRedValueEditText.setSelection(pos)
        }
        if (mGreenValueEditText.text.toString() != greenStr) {
            var pos = mGreenValueEditText.selectionStart
            if (pos >= greenStr.length)
                pos = greenStr.length - 1
            mGreenValueEditText.setText(greenStr)
            if (pos >= 0)
                mGreenValueEditText.setSelection(pos)
        }
        if (mBlueValueEditText.text.toString() != blueStr) {
            var pos = mBlueValueEditText.selectionStart
            if (pos >= blueStr.length)
                pos = blueStr.length - 1
            mBlueValueEditText.setText(blueStr)
            if (pos >= 0)
                mBlueValueEditText.setSelection(pos)
        }
        val hexValue = ("#%06x").format(mCurrentColor)
        if (mHexColorEditText.text.toString() != hexValue) {
            var pos = mHexColorEditText.selectionStart
            if (pos >= hexValue.length)
                pos = hexValue.length - 1
            mHexColorEditText.setText(hexValue)
            if (pos >= 0)
                mHexColorEditText.setSelection(pos)
        }
    }

    init {
        mContext = contentView.context
        mCurrentColor = color
        mRedValueEditText = contentView.findViewById(R.id.mcsv2_redValueEditText)
        mGreenValueEditText = contentView.findViewById(R.id.mcsv2_greenValueEditText)
        mBlueValueEditText = contentView.findViewById(R.id.mcsv2_blueValueEditText)
        mSliderRed = contentView.findViewById(R.id.mcsv2_sliderRed)
        mSliderGreen = contentView.findViewById(R.id.mcsv2_sliderGreen)
        mSliderBlue = contentView.findViewById(R.id.mcsv2_sliderBlue)
        mBtnRedDec = contentView.findViewById(R.id.mcsv2_btnRedDec)
        mBtnRedInc = contentView.findViewById(R.id.mcsv2_btnRedInc)
        mBtnGreenDec = contentView.findViewById(R.id.mcsv2_btnGreenDec)
        mBtnGreenInc = contentView.findViewById(R.id.mcsv2_btnGreenInc)
        mBtnBlueDec = contentView.findViewById(R.id.mcsv2_btnBlueDec)
        mBtnBlueInc = contentView.findViewById(R.id.mcsv2_btnBlueInc)
        mColorBox = contentView.findViewById(R.id.mcsv2_colorBox)
        mHexColorEditText = contentView.findViewById(R.id.mcsv2_hexColorEditText)

        val intLabelFormatter = LabelFormatter { value ->
            val numberFormat = NumberFormat.getNumberInstance()
            numberFormat.format(value.roundToInt())
        }
        val intValidator = IntValidator(mContext, 0, 255)
        mRedValueEditText.addTextChangedListener(object :
            TextWatcherValidator(intValidator, mRedValueEditText, true) {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                if (null != s) {
                    try {
                        val value = s.toString().toInt()
                        val newColor = (mCurrentColor and 0x00FFFF) or (value and 0xFF).shl(16)
                        if (newColor != mCurrentColor) {
                            mCurrentColor = newColor
                            propagateChanges()
                        }
                    } catch (_: Exception) {
                    }
                }
            }
        })
        mGreenValueEditText.addTextChangedListener(object :
            TextWatcherValidator(intValidator, mGreenValueEditText, true) {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                if (null != s) {
                    try {
                        val value = s.toString().toInt()
                        val newColor = (mCurrentColor and 0xFF00FF) or (value and 0xFF).shl(8)
                        if (newColor != mCurrentColor) {
                            mCurrentColor = newColor
                            propagateChanges()
                        }
                    } catch (_: Exception) {
                    }
                }
            }
        })
        mBlueValueEditText.addTextChangedListener(object :
            TextWatcherValidator(intValidator, mBlueValueEditText, true) {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                if (null != s) {
                    try {
                        val value = s.toString().toInt()
                        val newColor = (mCurrentColor and 0xFFFF00) or (value and 0xFF)
                        if (newColor != mCurrentColor) {
                            mCurrentColor = newColor
                            propagateChanges()
                        }
                    } catch (_: Exception) {
                    }
                }
            }
        })
        mHexColorEditText.addTextChangedListener(
            object : TextWatcherValidator(
                RegExpValidator(
                    mContext,
                    "^#[0-9a-fA-F]{0,6}$"
                ), mHexColorEditText, false
            ) {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    super.onTextChanged(s, start, before, count)
                    if (null != s) {
                        val pattern = Pattern.compile("^#([0-9a-fA-F]{6})$")
                        val matcher = pattern.matcher(s.toString())
                        if (matcher.matches()) {
                            val colorStr = matcher.group(1)
                            try {
                                val colorValue = colorStr?.toInt(16)
                                if (colorValue != null && mCurrentColor != colorValue) {
                                    mCurrentColor = colorValue
                                    propagateChanges()
                                }
                            } catch (_: Exception) {
                            }
                        } else {
                            val pattern2 = Pattern.compile("^#([0-9a-fA-F]{3})$")
                            val matcher2 = pattern2.matcher(s.toString())
                            if (matcher2.matches()) {
                                val colorStr = matcher2.group(1)
                                try {
                                    var colorValue = colorStr?.toInt(16)
                                    if (colorValue != null) {
                                        var redValue = (colorValue and 0xF00).shr(8)
                                        redValue = redValue or redValue.shl(4)
                                        var greenValue = (colorValue and 0x0F0).shr(4)
                                        greenValue = greenValue or greenValue.shl(4)
                                        var blueValue = (colorValue and 0x00F)
                                        blueValue = blueValue or blueValue.shl(4)
                                        colorValue =
                                            redValue.shl(16) or greenValue.shl(8) or blueValue
                                        if (mCurrentColor != colorValue) {
                                            mCurrentColor = colorValue
                                            propagateChanges()
                                        }
                                    }
                                } catch (_: Exception) {
                                }
                            }
                        }
                    }
                }
            }
        )
        mSliderRed.setLabelFormatter(intLabelFormatter)
        mSliderGreen.setLabelFormatter(intLabelFormatter)
        mSliderBlue.setLabelFormatter(intLabelFormatter)
        mSliderRed.addOnChangeListener(Slider.OnChangeListener { _, value, _ ->
            @ColorInt val newColor: Int =
                (mCurrentColor and 0x00FFFF) or (value.roundToInt() and 0xFF).shl(16)
            if (newColor != mCurrentColor) {
                mCurrentColor = newColor
                propagateChanges()
            }
        })
        mSliderGreen.addOnChangeListener(Slider.OnChangeListener { _, value, _ ->
            @ColorInt val newColor: Int =
                (mCurrentColor and 0xFF00FF) or (value.roundToInt() and 0xFF).shl(8)
            if (newColor != mCurrentColor) {
                mCurrentColor = newColor
                propagateChanges()
            }
        })
        mSliderBlue.addOnChangeListener(Slider.OnChangeListener { _, value, _ ->
            @ColorInt val newColor: Int =
                (mCurrentColor and 0xFFFF00) or (value.roundToInt() and 0xFF)
            if (newColor != mCurrentColor) {
                mCurrentColor = newColor
                propagateChanges()
            }
        })
        mBtnRedDec.setOnClickListener {
            val value = mSliderRed.value.toInt()
            if (value > 0)
                mSliderRed.value = (value - 1).toFloat()
        }
        mBtnRedInc.setOnClickListener {
            val value = mSliderRed.value.toInt()
            if (value < 255)
                mSliderRed.value = (value + 1).toFloat()
        }
        mBtnGreenDec.setOnClickListener {
            val value = mSliderGreen.value.toInt()
            if (value > 0)
                mSliderGreen.value = (value - 1).toFloat()
        }
        mBtnGreenInc.setOnClickListener {
            val value = mSliderGreen.value.toInt()
            if (value < 255)
                mSliderGreen.value = (value + 1).toFloat()
        }
        mBtnBlueDec.setOnClickListener {
            val value = mSliderBlue.value.toInt()
            if (value > 0)
                mSliderBlue.value = (value - 1).toFloat()
        }
        mBtnBlueInc.setOnClickListener {
            val value = mSliderBlue.value.toInt()
            if (value < 255)
                mSliderBlue.value = (value + 1).toFloat()
        }

        val cancelButton = contentView.findViewById<Button>(R.id.mcsv2_cancel_button)
        val okButton = contentView.findViewById<Button>(R.id.mcsv2_confirm_button)
        cancelButton.setOnClickListener {
            this@MyColorComposeForm.onColorFormListener?.onCancel()
            this@MyColorComposeForm.onColorFormListener?.onDismiss()
        }
        okButton.setOnClickListener {
            this@MyColorComposeForm.onColorFormListener?.onColorSelectionConfirmed(
                mCurrentColor
            )
            this@MyColorComposeForm.onColorFormListener?.onDismiss()
        }
        contentView.addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
            }

            override fun onViewDetachedFromWindow(v: View) {
                // Hide the soft keyboard if it is active (fullscreen dialog)
                val focusedView = contentView.findFocus() ?: View(mContext)
                val imm =
                    mContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(focusedView.windowToken, 0);
                contentView.clearFocus()
            }
        })
        propagateChanges()
    }
}