/*
 * Color selection view
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.mycolorselectorview

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import io.gitlab.coolreader_ng.textvalidators.AbstractValidator

open class TextWatcherValidator(
    private val validator: AbstractValidator,
    private val editText: EditText,
    private val autoFix: Boolean = false
) : TextWatcher {

    private var mPrevText: String? = null
    private var mPrevPos: Int = 0
    private var mIgnoreNextEvent = false

    fun validate(value: String) {
        val res = validator.validate(value)
        if (!res.res) {
            if (res.needUndo) {
                mIgnoreNextEvent = true
                mPrevText?.let {
                    editText.setText(it)
                    if (mPrevPos >= 0 && mPrevPos < it.length)
                        editText.setSelection(mPrevPos)
                }
                editText.error = validator.errorString
            } else {
                if (autoFix) {
                    mIgnoreNextEvent = true
                    editText.setText(res.replacement)
                } else {
                    editText.error = validator.errorString
                }
            }
        } else {
            editText.error = null
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        mPrevPos = editText.selectionStart - 1
        mPrevText = s.toString()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
    }

    override fun afterTextChanged(s: Editable?) {
        if (mIgnoreNextEvent) {
            mIgnoreNextEvent = false
            return
        }
        s?.let { validate(s.toString()) }
    }
}