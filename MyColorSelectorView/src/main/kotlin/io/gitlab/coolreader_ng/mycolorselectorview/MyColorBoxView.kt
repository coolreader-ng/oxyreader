/*
 * Color selection view
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.mycolorselectorview

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.annotation.ColorInt
import kotlin.math.abs
import kotlin.math.roundToInt


class MyColorBoxView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : View(context, attrs, defStyleAttr) {
    private val mBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mFillPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mBorderWidth = 1f

    private val m1dip: Float
    private val m5dip: Float
    private val m6dip: Float
    private var mAtMostWidth = 48
    private var mMinWidth = 22
    private var mMinHeight = 22
    private var mEdgeRect = RectF()
    private var mFillRect = RectF()

    var color: Int
        @ColorInt
        get() = mFillPaint.color
        set(@ColorInt color) {
            mFillPaint.color = color or 0xFF000000.toInt()
            invalidate()
        }

    var borderColorStateList: ColorStateList
    var borderWidth: Int
        get() = mBorderWidth.toInt()
        set(width) {
            if (abs(mBorderWidth - width.toFloat()) > 0.1) {
                mBorderWidth = width.toFloat()
                updateRects(this.width, this.height)
                mBorderPaint.strokeWidth = mBorderWidth
                invalidate()
            }
        }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateRects(w, h)
    }

    override fun onDraw(canvas: Canvas) {
        if (mBorderWidth > 0) {
            val state: IntArray = if (isFocused)
                intArrayOf(android.R.attr.state_focused)
            else
                intArrayOf()
            mBorderPaint.color = borderColorStateList.getColorForState(state, Color.TRANSPARENT)
            canvas.drawRoundRect(mEdgeRect, m6dip, m6dip, mBorderPaint)
            canvas.drawRoundRect(
                mFillRect,
                m5dip - mBorderWidth / 2,
                m5dip - mBorderWidth / 2,
                mFillPaint
            )
        } else {
            canvas.drawRoundRect(mFillRect, m6dip, m6dip, mFillPaint)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val wMode = MeasureSpec.getMode(widthMeasureSpec)
        var wSize = MeasureSpec.getSize(widthMeasureSpec)
        val hMode = MeasureSpec.getMode(heightMeasureSpec)
        var hSize = MeasureSpec.getSize(heightMeasureSpec)
        when (wMode) {
            MeasureSpec.AT_MOST -> wSize = mAtMostWidth
            MeasureSpec.UNSPECIFIED -> wSize = mMinWidth
            MeasureSpec.EXACTLY -> {
                if (wSize < mMinWidth)
                    wSize = mMinWidth
            }
        }
        when (hMode) {
            MeasureSpec.AT_MOST,
            MeasureSpec.UNSPECIFIED -> hSize = mMinHeight

            MeasureSpec.EXACTLY -> {
                if (hSize < mMinHeight)
                    hSize = mMinHeight
            }
        }
        setMeasuredDimension(wSize, hSize)
    }

    override fun getSuggestedMinimumWidth(): Int {
        return mMinWidth
    }

    override fun getSuggestedMinimumHeight(): Int {
        return mMinHeight
    }

    private fun updateRects(w: Int, h: Int) {
        mEdgeRect.set(
            mBorderWidth / 2,
            mBorderWidth / 2,
            w - mBorderWidth / 2,
            h - mBorderWidth / 2
        )
        if (mBorderWidth > 0) {
            mFillRect.set(
                mBorderWidth + m1dip,
                mBorderWidth + m1dip,
                w - mBorderWidth - m1dip,
                h - mBorderWidth - m1dip
            )
        } else {
            mFillRect.set(0f, 0f, w.toFloat(), h.toFloat())
        }
    }

    init {
        isFocusable = true
        isFocusableInTouchMode = true
        val metrics = context.resources.displayMetrics
        val density = metrics.density
        var color = Color.LTGRAY
        var borderColor: ColorStateList? = null
        m1dip = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, metrics)
        m5dip = 5f * m1dip
        m6dip = 6f * m1dip
        mBorderWidth = density.roundToInt().toFloat()
        if (mBorderWidth < 1f)
            mBorderWidth = 1f
        if (null != attrs) {
            val attributes =
                context.obtainStyledAttributes(attrs, R.styleable.MyColorBoxView, defStyleAttr, 0)
            color = attributes.getColor(R.styleable.MyColorBoxView_color, color)
            mBorderWidth = attributes.getDimension(R.styleable.MyColorBoxView_border, mBorderWidth)
            borderColor = attributes.getColorStateList(R.styleable.MyColorBoxView_borderColor)
            attributes.recycle()
        }
        mFillPaint.style = Paint.Style.FILL
        mFillPaint.color = color
        mFillPaint.strokeWidth = 0f
        mBorderPaint.style = Paint.Style.STROKE
        mBorderPaint.strokeWidth = mBorderWidth
        mAtMostWidth = (48f * density).roundToInt()
        mMinWidth = (22f * density).roundToInt()
        mMinHeight = (22f * density).roundToInt()
        borderColorStateList = borderColor
            ?: ColorStateList(
                arrayOf(
                    intArrayOf(android.R.attr.state_focused),
                    intArrayOf()
                ),
                intArrayOf(
                    Color.LTGRAY,
                    Color.TRANSPARENT
                )
            )
    }
}