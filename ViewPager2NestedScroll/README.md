This library contains class NestedScrollableHost from <https://github.com/android/views-widgets-samples.git>

Full path: <https://github.com/android/views-widgets-samples/blob/master/ViewPager2/app/src/main/java/androidx/viewpager2/integration/testapp/NestedScrollableHost.kt>

Layout to wrap a scrollable component inside a ViewPager2.
Provided as a solution to the problem where pages of ViewPager2 have nested scrollable elements that scroll in the same direction as ViewPager2.
The scrollable element needs to be the immediate and only child of this host layout.

This solution has limitations when using multiple levels of nested scrollable elements (e.g. a horizontal RecyclerView in a vertical RecyclerView in a horizontal ViewPager2).

Usage:
Wrap the scrollable view (for example RecyclerView) in this NestedScrollableHost like here:
```xml
    <androidx.viewpager2.integration.testapp.NestedScrollableHost
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_weight="1">

        <androidx.recyclerview.widget.RecyclerView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content" />

    </androidx.viewpager2.integration.testapp.NestedScrollableHost>
```

Copyright 2019 The Android Open Source Project
License: Apache Licence v2.0
