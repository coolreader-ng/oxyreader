/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.recyclerview.utils

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.coolreader_ng.mygesturedetector.MyGestureDetector
import io.gitlab.coolreader_ng.project_s.extensions.getViewAt

internal class RecyclerViewSimpleItemOnClickListener(
    context: Context,
    isLongPressEnabled: Boolean = false
) : RecyclerView.OnItemTouchListener {

    abstract class OnItemSelectedListener {
        abstract fun onItemSelected(index: Int)
        open fun onLongPressed(index: Int) {}
    }

    var onItemSelectedListener: OnItemSelectedListener? = null

    private val mGestureDetector =
        MyGestureDetector(context, object : GestureDetector.OnGestureListener {
            override fun onDown(e: MotionEvent): Boolean {
                return true
            }

            override fun onShowPress(e: MotionEvent) {
            }

            override fun onSingleTapUp(e: MotionEvent): Boolean {
                if (mItemIndex >= 0) {
                    onItemSelectedListener?.onItemSelected(mItemIndex)
                    return true
                }
                return false
            }

            override fun onScroll(
                e1: MotionEvent?,
                e2: MotionEvent,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                return false
            }

            override fun onLongPress(e: MotionEvent) {
                if (mItemIndex >= 0)
                    onItemSelectedListener?.onLongPressed(mItemIndex)
            }

            override fun onFling(
                e1: MotionEvent?,
                e2: MotionEvent,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                return false
            }
        })
    private var mItemIndex = -1

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        val itemView: View? = rv.findChildViewUnder(e.x, e.y)
        if (null != itemView) {
            val itemX = itemView.x
            val itemY = itemView.y
            val innerView = if (itemView is ViewGroup)
                itemView.getViewAt(e.x - itemX, e.y - itemY)
            else
                null
            if (innerView is Button) {
                // ignore this touch event on inner button
                mItemIndex = -1
            } else {
                val pos = rv.getChildAdapterPosition(itemView)
                if (pos >= 0) {
                    mItemIndex = pos
                    mGestureDetector.onTouchEvent(e)
                } else {
                    mItemIndex = -1
                }
            }
        } else {
            mItemIndex = -1
        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        mGestureDetector.onTouchEvent(e)
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
    }

    init {
        mGestureDetector.setIsLongpressEnabled(isLongPressEnabled)
    }
}
