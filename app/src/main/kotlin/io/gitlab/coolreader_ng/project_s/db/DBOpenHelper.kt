/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.db

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import io.gitlab.coolreader_ng.project_s.SRLog

class DBOpenHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        const val DATABASE_NAME = "base.db"

        // If you change the database schema, you must increment the database version
        //  and update onUpgrade() method
        const val DATABASE_VERSION = 9

        private val log = SRLog.create("dbhelper")
    }

    override fun onConfigure(db: SQLiteDatabase?) {
        super.onConfigure(db)
        // We use one database instance in a separate service, so we don't need parallel query execution
        // Disable parallel execution of queries from multiple threads on the same database.
        db?.disableWriteAheadLogging()
        // Enable foreign key constraints.
        db?.setForeignKeyConstraintsEnabled(true)
        try {
            // See https://www.sqlite.org/pragma.html#pragma_auto_vacuum
            db?.execSQL("PRAGMA auto_vacuum=off")
        } catch (e: SQLException) {
            log.error("onConfigure(): PRAGMA failed", e)
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        try {
            for (sql in DBContract.AuthorTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.DirectoryTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.SeriesTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.KeywordTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.UnsupportedGenreTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookAuthorTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookGenreTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookUnsupportedGenreTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookKeywordTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookmarkTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookCoverTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.BookSearchHistoryTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.TextSearchHistoryTable.SQL_CREATE)
                db?.execSQL(sql)
            for (sql in DBContract.CurrentBookTable.SQL_CREATE)
                db?.execSQL(sql)
        } catch (e: SQLException) {
            log.error("SQL (CREATE) failed", e)
        }
    }

    private fun execSQLIgnoringErrors(db: SQLiteDatabase, sql: String): Boolean {
        var res = false
        try {
            db.execSQL(sql)
            res = true
        } catch (_: SQLException) {
        }
        return res
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // Execute some SQL queries to update database schema from `oldVersion` to `newVersion`
        if (null != db) {
            log.info("Upgrading database from version $oldVersion to $newVersion")
            if (oldVersion < newVersion) {
                var totalCount = 0
                var okCount = 0
                if (oldVersion < 2) {
                    totalCount += DBContract.BookTable.SQL_QUERY_UPGRADE_VER_1_to_2.size
                    for (sql in DBContract.BookTable.SQL_QUERY_UPGRADE_VER_1_to_2) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 3) {
                    totalCount += DBContract.KeywordTable.SQL_CREATE.size + DBContract.BookKeywordTable.SQL_CREATE.size
                    for (sql in DBContract.KeywordTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                    for (sql in DBContract.BookKeywordTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 4) {
                    totalCount += DBContract.UnsupportedGenreTable.SQL_CREATE.size + DBContract.BookUnsupportedGenreTable.SQL_CREATE.size
                    for (sql in DBContract.UnsupportedGenreTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                    for (sql in DBContract.BookUnsupportedGenreTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 5) {
                    totalCount += DBContract.BookSearchHistoryTable.SQL_CREATE.size
                    for (sql in DBContract.BookSearchHistoryTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 6) {
                    totalCount += DBContract.BookTable.SQL_QUERY_UPGRADE_VER_5_to_6.size
                    for (sql in DBContract.BookTable.SQL_QUERY_UPGRADE_VER_5_to_6) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 7) {
                    totalCount += DBContract.CurrentBookTable.SQL_CREATE.size
                    for (sql in DBContract.CurrentBookTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 8) {
                    totalCount += DBContract.BookTable.SQL_QUERY_UPGRADE_VER_7_to_8.size
                    for (sql in DBContract.BookTable.SQL_QUERY_UPGRADE_VER_7_to_8) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                if (oldVersion < 9) {
                    totalCount += DBContract.TextSearchHistoryTable.SQL_CREATE.size
                    for (sql in DBContract.TextSearchHistoryTable.SQL_CREATE) {
                        if (execSQLIgnoringErrors(db, sql))
                            okCount++
                    }
                }
                // Add here other database version variants
                log.info("Successfully performed $okCount upgrade queries from $totalCount")
            }
        }
    }

}