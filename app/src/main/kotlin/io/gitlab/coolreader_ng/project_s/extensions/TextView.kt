/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import android.graphics.text.LineBreaker
import android.os.Build
import android.text.Layout
import android.widget.TextView
import io.gitlab.coolreader_ng.project_s.CREngineNGBinding

fun TextView.setHyphenatedText(langTag: String?, text: String?) {
    var hyphenatedText: String? = null
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (null != text && null != langTag)
            hyphenatedText = CREngineNGBinding.hyphenate(langTag, text)
        if (null != hyphenatedText) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                this.breakStrategy = LineBreaker.BREAK_STRATEGY_HIGH_QUALITY
                this.justificationMode = LineBreaker.JUSTIFICATION_MODE_NONE
            }
            this.hyphenationFrequency = Layout.HYPHENATION_FREQUENCY_FULL
        } else
            this.hyphenationFrequency = Layout.HYPHENATION_FREQUENCY_NONE
    }
    if (null != hyphenatedText)
        this.text = hyphenatedText
    else
        this.text = text
}
