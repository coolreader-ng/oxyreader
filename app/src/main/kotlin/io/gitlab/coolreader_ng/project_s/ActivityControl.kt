/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

interface ActivityControl {
    fun onUserActivity()
    fun isFullScreen(): Boolean
    fun setFullscreen(value: Boolean)
    fun setSetting(name: String, value: Any, notify: Boolean)
    fun setSettings(props: SRProperties, notify: Boolean)
    fun getScreenBacklightLevel(): Int
    fun setScreenBacklightLevel(backlight: Int)
    fun showToast(message: String)
    fun showToast(resId: Int)
    fun showToast(resId: Int, vararg formatArgs: Any?)
    fun showBookInfo(bookInfo: BookInfo)
    fun copyBookQuote(bookInfo: BookInfo, quote: String)
    fun shareBookQuote(bookInfo: BookInfo, quote: String)
    fun showBookmarkEditFragment(
        bookmark: Bookmark,
        isNew: Boolean,
        actionsListener: BookmarkEditDialogFragment.OnActionsListener
    )

    fun saveDataToFile(bookInfo: BookInfo, data: ByteArray): BookInfo?
    fun lockOrientation()
    fun unlockOrientation()
    fun openSettings(fragmentClassName: String? = null)
    fun openAboutDialog()
    fun openLibrary(pageNo: Int)
    fun openFileSelector()
    fun openExternalDictionary(text: String)
    fun requestPostNotificationPermissions()
    fun openURL(url: String, confirmation: Boolean)
    fun openSearchFragment(bookInfo: BookInfo)
    fun overrideToScrollViewMode()
    fun restoreViewMode()
    fun exitReader()
}
