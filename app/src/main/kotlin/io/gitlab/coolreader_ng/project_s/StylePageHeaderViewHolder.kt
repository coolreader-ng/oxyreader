/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import com.google.android.material.chip.Chip
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import io.gitlab.coolreader_ng.project_s.extensions.getCurrentSelectedPosition
import io.gitlab.coolreader_ng.project_s.extensions.setCurrentSelectedItem
import io.gitlab.coolreader_ng.project_s.extensions.setSimpleItems
import java.text.NumberFormat
import kotlin.math.roundToInt

class StylePageHeaderViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private val mFontFaceEdit: MaterialAutoCompleteTextView?
    private val mFontFaceSpinner: Spinner?
    private val mFontSizeSlider: Slider
    private val mFontSizeValueView: TextView
    private val mBtnFontSizeDec: Button
    private val mBtnFontSizeInc: Button
    private val mStatusLocationEdit: MaterialAutoCompleteTextView?
    private val mStatusLocationSpinner: Spinner?
    private val mStatusChipChapterMarks: Chip
    private val mStatusChipTime: Chip
    private val mStatusChipPositionPercent: Chip
    private val mStatusChipPositionPage: Chip
    private val mStatusChipBattery: Chip
    private val mStatusChipBatteryPercent: Chip
    private val mNumberFormat: NumberFormat

    override fun onUpdateViewImpl() {
        // Font face
        val fontFace = mProps.getProperty(PropNames.Engine.PROP_STATUS_FONT_FACE, "")
        mFontFaceEdit?.setCurrentSelectedItem(fontFace)
        mFontFaceSpinner?.setCurrentSelectedItem(fontFace)

        // Font size
        val fontSizePx = mProps.getInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, -1)
        val fontSizePt =
            Utils.convertUnits(fontSizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
        mFontSizeValueView.text =
            context.getString(R.string.format_font_size_str_pt, mNumberFormat.format(fontSizePt))
        var sliderValue = fontSizePt
        if (sliderValue < MIN_STATUS_FONT_SIZE_PT)
            sliderValue = MIN_STATUS_FONT_SIZE_PT
        else if (sliderValue > MAX_STATUS_FONT_SIZE_PT)
            sliderValue = MAX_STATUS_FONT_SIZE_PT
        mFontSizeSlider.value = sliderValue
        mBtnFontSizeDec.isEnabled = fontSizePt > MIN_STATUS_FONT_SIZE_PT
        mBtnFontSizeInc.isEnabled = fontSizePt < MAX_STATUS_FONT_SIZE_PT

        // Running title (status) location
        val statusLocationIdx = when (mProps.getInt(PropNames.Engine.PROP_STATUS_LINE, 0)) {
            0 -> 0  // disabled
            1 -> 1  // header
            2 -> 2  // footer
            else -> 1
        }
        mStatusLocationEdit?.setCurrentSelectedItem(statusLocationIdx)
        mStatusLocationSpinner?.setSelection(statusLocationIdx)

        // Running title (status) elements
        mStatusChipChapterMarks.isChecked =
            mProps.getBool(PropNames.Engine.PROP_STATUS_CHAPTER_MARKS, true)
        mStatusChipTime.isChecked = mProps.getBool(PropNames.Engine.PROP_SHOW_TIME, true)
        mStatusChipPositionPercent.isChecked =
            mProps.getBool(PropNames.Engine.PROP_SHOW_POS_PERCENT, true)
        mStatusChipPositionPage.isChecked =
            mProps.getBool(PropNames.Engine.PROP_SHOW_PAGE_COUNT, true)
        val showStatusBattery = mProps.getBool(PropNames.Engine.PROP_SHOW_BATTERY, true)
        mStatusChipBattery.isChecked = showStatusBattery
        mStatusChipBatteryPercent.visibility = if (showStatusBattery) View.VISIBLE else View.GONE
        mStatusChipBatteryPercent.isChecked =
            mProps.getBool(PropNames.Engine.PROP_SHOW_BATTERY_PERCENT, true)
    }

    override fun onResetViewImpl() {
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // On layout for API26+
            mFontFaceEdit = itemView.findViewById(R.id.fontFaceEdit)
            mStatusLocationEdit = itemView.findViewById(R.id.statusLocationEdit)
            mFontFaceSpinner = null
            mStatusLocationSpinner = null
        } else {
            // On layout for API less than 26
            mFontFaceEdit = null
            mStatusLocationEdit = null
            mFontFaceSpinner = itemView.findViewById(R.id.fontFaceSpinner)
            mStatusLocationSpinner = itemView.findViewById(R.id.statusLocationSpinner)
        }
        mFontSizeSlider = itemView.findViewById(R.id.fontSizeSlider)
        mFontSizeValueView = itemView.findViewById(R.id.fontSizeValueView)
        mBtnFontSizeDec = itemView.findViewById(R.id.btnFontSizeDec)
        mBtnFontSizeInc = itemView.findViewById(R.id.btnFontSizeInc)
        mStatusChipChapterMarks = itemView.findViewById(R.id.statusChipChapterMarks)
        mStatusChipTime = itemView.findViewById(R.id.statusChipTime)
        mStatusChipPositionPercent = itemView.findViewById(R.id.statusChipPositionPercent)
        mStatusChipPositionPage = itemView.findViewById(R.id.statusChipPositionPage)
        mStatusChipBattery = itemView.findViewById(R.id.statusChipBattery)
        mStatusChipBatteryPercent = itemView.findViewById(R.id.statusChipBatteryPercent)

        mNumberFormat = NumberFormat.getNumberInstance(SettingsManager.activeLang.locale)
        mNumberFormat.minimumFractionDigits = 0
        mNumberFormat.maximumFractionDigits = 1

        // Font faces
        val engineAcc = ReaderEngineObjectsAccessor.leaseEngineObjects()
        val fontFaces = engineAcc.crEngineNGBinding?.getFontFaceList()
        ReaderEngineObjectsAccessor.releaseEngineObject(engineAcc)
        fontFaces?.let { it ->
            mFontFaceEdit?.setSimpleItems(it)
            mFontFaceSpinner?.setSimpleItems(it)
        }
        // On layout for API26+
        mFontFaceEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    val prevFontFace =
                        mProps.getProperty(PropNames.Engine.PROP_STATUS_FONT_FACE, "")
                    if (prevFontFace != it.toString()) {
                        mProps.setProperty(PropNames.Engine.PROP_STATUS_FONT_FACE, it.toString())
                        commitChanges()
                    }
                }
            }
        })
        // On layout for API less than 26
        mFontFaceSpinner?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevFontFace = mProps.getProperty(PropNames.Engine.PROP_STATUS_FONT_FACE, "")
                val fontFace = parent?.adapter?.getItem(position)
                if (prevFontFace != fontFace.toString()) {
                    mProps.setProperty(PropNames.Engine.PROP_STATUS_FONT_FACE, fontFace.toString())
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Font size
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed (when slider is using on PopupWindow)
            // TODO: Test on other API, new material design library (possible fixed)
            mFontSizeSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mFontSizeSlider.setLabelFormatter { value ->
                context.getString(R.string.format_font_size_str_pt, mNumberFormat.format(value))
            }
        }
        mFontSizeSlider.valueFrom = MIN_STATUS_FONT_SIZE_PT
        mFontSizeSlider.valueTo = MAX_STATUS_FONT_SIZE_PT
        mFontSizeSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value
                val valuePx = Utils.convertUnits(value, from = Utils.Units.PT, to = Utils.Units.PX)
                    .roundToInt()
                val prevValuePx = mProps.getInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, -1)
                if (prevValuePx != valuePx) {
                    mProps.setInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, valuePx)
                    commitChanges()
                }
            }
        })
        mBtnFontSizeDec.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(10f, from = Utils.Units.PT, to = Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt > MIN_STATUS_FONT_SIZE_PT) {
                sizePx--
                mProps.setInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, sizePx)
                commitChanges()
            }
        }
        mBtnFontSizeInc.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(10f, Utils.Units.PT, Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt < MAX_STATUS_FONT_SIZE_PT) {
                sizePx++
                mProps.setInt(PropNames.Engine.PROP_STATUS_FONT_SIZE, sizePx)
                commitChanges()
            }
        }

        // Running title (status) location
        // On layout for API26+
        mStatusLocationEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevStatusLocationCode = mProps.getInt(PropNames.Engine.PROP_STATUS_LINE, 0)
                val statusLocationCode = when (mStatusLocationEdit.getCurrentSelectedPosition()) {
                    0 -> 0  // disabled
                    1 -> 1  // header
                    2 -> 2  // footer
                    else -> 1
                }
                if (prevStatusLocationCode != statusLocationCode) {
                    mProps.setInt(PropNames.Engine.PROP_STATUS_LINE, statusLocationCode)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mStatusLocationSpinner?.setSimpleItems(R.array.header_location)
        mStatusLocationSpinner?.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevStatusLocationCode = mProps.getInt(PropNames.Engine.PROP_STATUS_LINE, 0)
                val statusLocationCode = when (position) {
                    0 -> 0  // disabled
                    1 -> 1  // header
                    2 -> 2  // footer
                    else -> 1
                }
                if (prevStatusLocationCode != statusLocationCode) {
                    mProps.setInt(PropNames.Engine.PROP_STATUS_LINE, statusLocationCode)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Running title (status) elements
        mStatusChipChapterMarks.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_STATUS_CHAPTER_MARKS, isChecked)
            commitChanges()
        }
        mStatusChipTime.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_SHOW_TIME, isChecked)
            commitChanges()
        }
        mStatusChipPositionPercent.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_SHOW_POS_PERCENT, isChecked)
            commitChanges()
        }
        mStatusChipPositionPage.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_SHOW_PAGE_NUMBER, isChecked)
            mProps.setBool(PropNames.Engine.PROP_SHOW_PAGE_COUNT, isChecked)
            commitChanges()
        }
        mStatusChipBattery.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_SHOW_BATTERY, isChecked)
            commitChanges()
        }
        mStatusChipBatteryPercent.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_SHOW_BATTERY_PERCENT, isChecked)
            commitChanges()
        }
    }

    companion object {
        const val MIN_STATUS_FONT_SIZE_PT = 2f
        const val MAX_STATUS_FONT_SIZE_PT = 20f
    }
}
