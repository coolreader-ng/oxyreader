/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import android.view.View
import android.view.Window
import android.view.WindowManager

val Window.inFullscreenMode: Boolean
    @Suppress("deprecation")
    get() {
        val uiSystemFlags = this.decorView.systemUiVisibility
        val attrFlags = this.attributes.flags
        return ((uiSystemFlags and View.SYSTEM_UI_FLAG_FULLSCREEN) != 0) ||
                ((attrFlags and WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0)
        // TODO: implement for API30+ without deprecated methods
    }
