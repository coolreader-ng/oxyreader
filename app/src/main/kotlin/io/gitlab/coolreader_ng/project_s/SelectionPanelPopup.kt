/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow
import com.google.android.material.materialswitch.MaterialSwitch

class SelectionPanelPopup(private val parent: View, contentView: View) {

    interface SelectionPanelListener {
        fun onStickToWordBoundary(value: Boolean)
        fun onCopyToClipboard()
        fun onShare()
        fun onNewBookmark()
        fun onOpenDictionary()
        fun onCancel()
    }

    var selectionPanelListener: SelectionPanelListener? = null
    val isShowing: Boolean
        get() = mPopup.isShowing
    var isSelectionActive: Boolean = false
        set(value) {
            if (value) {
                mButtonsPanel.visibility = View.VISIBLE
                mTextPanel.visibility = View.INVISIBLE
            } else {
                mButtonsPanel.visibility = View.INVISIBLE
                mTextPanel.visibility = View.VISIBLE
            }
            field = value
        }

    private var mPopup: PopupWindow
    private val mButtonsPanel: ViewGroup
    private val mTextPanel: ViewGroup
    private val mSwSelectionStyle: MaterialSwitch

    fun show() {
        mPopup.isClippingEnabled = true
        if (!mPopup.isShowing)
            mSwSelectionStyle.isChecked = false
        mPopup.showAtLocation(parent, Gravity.BOTTOM or Gravity.CENTER_HORIZONTAL, 0, 0)
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    init {
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.BOTTOM)
            mPopup.exitTransition = Slide(Gravity.BOTTOM)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = false
        mPopup.isTouchable = true
        mPopup.isFocusable = false

        mButtonsPanel = contentView.findViewById(R.id.buttonsPanel)
        mTextPanel = contentView.findViewById(R.id.textPanel)
        mSwSelectionStyle = contentView.findViewById(R.id.swSelectionStyle)

        contentView.findViewById<Button>(R.id.btnCopy)
            .setOnClickListener { selectionPanelListener?.onCopyToClipboard() }
        contentView.findViewById<Button>(R.id.btnShare)
            .setOnClickListener { selectionPanelListener?.onShare() }
        contentView.findViewById<Button>(R.id.btnAddBookmark)
            .setOnClickListener { selectionPanelListener?.onNewBookmark() }
        contentView.findViewById<Button>(R.id.btnOpenDictionary)
            .setOnClickListener { selectionPanelListener?.onOpenDictionary() }
        contentView.findViewById<Button>(R.id.btnCancel)
            .setOnClickListener { selectionPanelListener?.onCancel() }
        mSwSelectionStyle.setOnCheckedChangeListener { _, isChecked ->
            selectionPanelListener?.onStickToWordBoundary(isChecked)
        }
    }
}