/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.ViewFlipper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import io.gitlab.coolreader_ng.mycolorselectorview.MyColorSelectorView
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener
import java.text.NumberFormat
import kotlin.math.roundToInt

class StylePageCommonViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private inner class BackgroundImageViewData {
        // The status of this Background image is either selected (true) or not (false).
        var selected = false
    }

    private val mPageBackgroundInfoViewProps =
        HashMap<SettingsManager.PageBackgroundInfo, BackgroundImageViewData>()

    // extension property for PageBackgroundInfo
    private val SettingsManager.PageBackgroundInfo.viewProps: BackgroundImageViewData
        @Synchronized
        get() {
            var viewProps = mPageBackgroundInfoViewProps[this]
            if (null == viewProps) {
                viewProps = BackgroundImageViewData()
                mPageBackgroundInfoViewProps[this] = viewProps
            }
            return viewProps
        }

    private inner class BackgroundImageViewAdapter :
        RecyclerView.Adapter<BackgroundImageViewAdapter.BackgroundImageViewHolder>() {

        inner class BackgroundImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val card: MaterialCardView = itemView.findViewById(R.id.card)
            val imageView: ImageView = itemView.findViewById(R.id.imageView)
            val imageName: TextView = itemView.findViewById(R.id.imageName)
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): BackgroundImageViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val itemView = layoutInflater.inflate(R.layout.background_item, parent, false)
            return BackgroundImageViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: BackgroundImageViewHolder, position: Int) {
            val item: SettingsManager.PageBackgroundInfo? =
                if (position >= 0 && position < mBackgroundImages.size)
                    mBackgroundImages[position]
                else
                    null
            if (null != item) {
                if (item.viewProps.selected) {
                    holder.card.isChecked = true
                    holder.card.strokeWidth =
                        Utils.convertUnits(3f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                } else {
                    holder.card.isChecked = false
                    holder.card.strokeWidth =
                        Utils.convertUnits(1f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                }
                if (item.isFile()) {
                    var bitmap: Bitmap? = null
                    try {
                        bitmap = BitmapFactory.decodeFile(item.filePath)
                    } catch (_: Exception) {
                    }
                    if (null != bitmap)
                        holder.imageView.setImageBitmap(bitmap)
                    else {
                        // TODO: insert error image or text...
                    }
                } else {
                    try {
                        holder.imageView.setImageResource(item.resId)
                    } catch (e: OutOfMemoryError) {
                        log.error("Failed to set image", e)
                    }
                }
                holder.imageName.text = item.name ?: item.id
            }
        }

        override fun getItemCount(): Int {
            return mBackgroundImages.size
        }
    }

    private val mTextFamilyLabel: TextView
    private val mTextColorSelectorView: MyColorSelectorView
    private val mBackgroundStyle: RadioGroup
    private val mBackgroundSolidColor: RadioButton
    private val mBackgroundImage: RadioButton
    private val mBackgroundViewFlipper: ViewFlipper
    private val mBgColorSelectorView: MyColorSelectorView
    private val mImageRecyclerView: RecyclerView
    private val mAdapter = BackgroundImageViewAdapter()
    private var mPrevBackgroundStyle: Int
    private var mBackgroundImages: Array<SettingsManager.PageBackgroundInfo>
    private var mCurrentBackgroundItemIndex = 0

    override fun onUpdateViewImpl() {
        // Font Face
        val fontFace = mProps.getProperty(PropNames.Engine.PROP_FONT_FACE, "<Not set yet>")
        val fontSizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
        val fontSizePt =
            Utils.convertUnits(fontSizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
        val numberFormat = NumberFormat.getNumberInstance(SettingsManager.activeLang.locale)
        numberFormat.minimumFractionDigits = 0
        numberFormat.maximumFractionDigits = 1
        var fontDesc =
            context.getString(R.string.format_font_spec, fontFace, numberFormat.format(fontSizePt))
        val weight = mProps.getInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, 400)
        if (400 != weight)
            fontDesc += Utils.fontWeightToName(weight)
        mTextFamilyLabel.text = fontDesc
        // Text Color
        val textColor = mProps.getColor(PropNames.Engine.PROP_FONT_COLOR, 0)
        mTextColorSelectorView.color = textColor
        // Background style, color, image
        val backgroundId =
            mProps.getProperty(PropNames.App.BACKGROUND_IMAGE, SettingsManager.BACKGROUND_NONE.id)
        if (backgroundId == SettingsManager.BACKGROUND_NONE.id) {
            // background style: solid color
            mBackgroundStyle.check(R.id.rbBackgroundSolidColor)
        } else {
            // background style: image
            setCurrentBackgroundItem(backgroundId, true)
            mBackgroundStyle.check(R.id.rbBackgroundImage)
        }
        val backgroundColor = mProps.getColor(PropNames.Engine.PROP_BACKGROUND_COLOR, 0xFFFFFF)
        mBgColorSelectorView.color = backgroundColor
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
        val decorView = data["decorView"] as View?
        mBgColorSelectorView.popupParent = decorView
        mTextColorSelectorView.popupParent = decorView
    }

    override fun onResetViewImpl() {
        for (info in mBackgroundImages) {
            info.viewProps.selected = false
        }
        mCurrentBackgroundItemIndex = 0
        mAdapter.notifyItemRangeChanged(0, mBackgroundImages.size)
    }

    private fun setCurrentBackgroundItem(pos: Int, scrollTo: Boolean) {
        for ((index, value) in mBackgroundImages.withIndex()) {
            if (index == pos) {
                if (!value.viewProps.selected) {
                    value.viewProps.selected = true
                    mAdapter.notifyItemChanged(index)
                    mCurrentBackgroundItemIndex = index
                    if (scrollTo)
                        mImageRecyclerView.scrollToPosition(mCurrentBackgroundItemIndex)
                }
            } else {
                if (value.viewProps.selected) {
                    value.viewProps.selected = false
                    mAdapter.notifyItemChanged(index)
                }
            }
        }
    }

    private fun setCurrentBackgroundItem(id: String, scrollTo: Boolean) {
        for ((index, value) in mBackgroundImages.withIndex()) {
            if (value.id == id) {
                if (!value.viewProps.selected) {
                    value.viewProps.selected = true
                    mAdapter.notifyItemChanged(index)
                    mCurrentBackgroundItemIndex = index
                    if (scrollTo)
                        mImageRecyclerView.scrollToPosition(mCurrentBackgroundItemIndex)
                }
            } else {
                if (value.viewProps.selected) {
                    value.viewProps.selected = false
                    mAdapter.notifyItemChanged(index)
                }
            }
        }
    }

    private fun setBackgroundImage(id: String) {
        val prevId =
            mProps.getProperty(PropNames.App.BACKGROUND_IMAGE, SettingsManager.BACKGROUND_NONE.id)
        if (prevId != id) {
            mProps.setProperty(PropNames.App.BACKGROUND_IMAGE, id)
            onStylePanelListener?.onPropertiesChanged(mProps)
        }
    }

    private fun getCurrentBackgroundImageId(): String {
        if (mCurrentBackgroundItemIndex >= 0 && mCurrentBackgroundItemIndex < mBackgroundImages.size)
            return mBackgroundImages[mCurrentBackgroundItemIndex].id
        return SettingsManager.BACKGROUND_NONE.id
    }

    private fun setBackgroundSolidColor(color: Int) {
        val prevColor = mProps.getColor(PropNames.Engine.PROP_BACKGROUND_COLOR, 0xFFFFFF)
        val creColor = (color and 0x00FFFFFF)
        if (creColor != prevColor) {
            mProps.setColor(PropNames.Engine.PROP_BACKGROUND_COLOR, creColor)
            onStylePanelListener?.onPropertiesChanged(mProps)
        }
    }

    private fun setTextColor(color: Int) {
        val prevColor = mProps.getColor(PropNames.Engine.PROP_FONT_COLOR, 0)
        val creColor = (color and 0x00FFFFFF)
        if (creColor != prevColor) {
            mProps.setColor(PropNames.Engine.PROP_FONT_COLOR, creColor)
            onStylePanelListener?.onPropertiesChanged(mProps)
        }
    }

    private fun setStatusTextColor(color: Int) {
        val prevColor = mProps.getColor(PropNames.Engine.PROP_STATUS_FONT_COLOR, 0)
        val creColor = (color and 0x00FFFFFF)
        if (creColor != prevColor) {
            mProps.setColor(PropNames.Engine.PROP_STATUS_FONT_COLOR, creColor)
            onStylePanelListener?.onPropertiesChanged(mProps)
        }
    }

    init {
        mBackgroundImages = Utils.getAvailableTextures(context)
        mPrevBackgroundStyle = 0

        mTextFamilyLabel = itemView.findViewById(R.id.textFamilyLabel)
        mTextColorSelectorView = itemView.findViewById(R.id.textColorSelectorView)
        mBackgroundStyle = itemView.findViewById(R.id.rgBackgroundStyle)
        mBackgroundSolidColor = itemView.findViewById(R.id.rbBackgroundSolidColor)
        mBackgroundImage = itemView.findViewById(R.id.rbBackgroundImage)
        mBackgroundViewFlipper = itemView.findViewById(R.id.backgroundViewFlipper)
        mBgColorSelectorView = itemView.findViewById(R.id.bgColorSelectorView)
        mImageRecyclerView = itemView.findViewById(R.id.imageRecyclerView)
        val btnTextSizeDec: Button = itemView.findViewById(R.id.btnTextSizeDec)
        val btnTextSizeInc: Button = itemView.findViewById(R.id.btnTextSizeInc)

        btnTextSizeDec.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(12f, from = Utils.Units.PT, to = Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt > StylePageText1ViewHolder.MIN_FONT_SIZE_PT) {
                sizePx--
                mProps.setInt(PropNames.Engine.PROP_FONT_SIZE, sizePx)
                commitChanges()
            }
        }
        btnTextSizeInc.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(12f, Utils.Units.PT, Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt < StylePageText1ViewHolder.MAX_FONT_SIZE_PT) {
                sizePx++
                mProps.setInt(PropNames.Engine.PROP_FONT_SIZE, sizePx)
                commitChanges()
            }
        }

        mBackgroundSolidColor.setHyphenatedText(
            SettingsManager.activeLang.langTag,
            mBackgroundSolidColor.text.toString()
        )
        mBackgroundImage.setHyphenatedText(
            SettingsManager.activeLang.langTag,
            mBackgroundImage.text.toString()
        )
        mBackgroundStyle.setOnCheckedChangeListener { _, checkedId ->
            val newBackgroundStyle = when (checkedId) {
                R.id.rbBackgroundSolidColor -> 0
                R.id.rbBackgroundImage -> 1
                else -> 0
            }
            if (mPrevBackgroundStyle != newBackgroundStyle) {
                val animShowTranslateRight: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.show_translate_right
                )
                val animShowTranslateLeft: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.show_translate_left
                )
                val animHideTranslateRight: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.hide_translate_right
                )
                val animHideTranslateLeft: Animation = AnimationUtils.loadAnimation(
                    context,
                    R.anim.hide_translate_left
                )
                when (mPrevBackgroundStyle.shl(1) or newBackgroundStyle) {
                    1 -> {
                        // 0|1: solid color -> image
                        mBackgroundViewFlipper.inAnimation = animShowTranslateRight
                        mBackgroundViewFlipper.outAnimation = animHideTranslateRight
                        mBackgroundViewFlipper.showNext()
                        //mImageRecyclerView.scrollToPosition(mCurrentBackgroundItemIndex)
                        setCurrentBackgroundItem(mCurrentBackgroundItemIndex, true)
                        setBackgroundImage(getCurrentBackgroundImageId())
                    }

                    2 -> {
                        // 1|0: image -> solid color
                        mBackgroundViewFlipper.inAnimation = animShowTranslateLeft
                        mBackgroundViewFlipper.outAnimation = animHideTranslateLeft
                        mBackgroundViewFlipper.showPrevious()
                        setBackgroundImage(SettingsManager.BACKGROUND_NONE.id)
                    }
                }
                mPrevBackgroundStyle = newBackgroundStyle
            }
        }
        mTextColorSelectorView.onColorSelectedListener =
            object : MyColorSelectorView.OnColorSelected {
                override fun onColorSelected(index: Int, color: Int) {
                    setTextColor(color)
                    setStatusTextColor(color)
                }
            }
        mBgColorSelectorView.onColorSelectedListener =
            object : MyColorSelectorView.OnColorSelected {
                override fun onColorSelected(index: Int, color: Int) {
                    setBackgroundSolidColor(color)
                }
            }
        mBackgroundImages = Utils.getAvailableTextures(context)
        mImageRecyclerView.layoutManager =
            LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        mImageRecyclerView.adapter = mAdapter
        mImageRecyclerView.addOnItemTouchListener(RecyclerViewSimpleItemOnClickListener(context).also {
            it.onItemSelectedListener =
                object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                    override fun onItemSelected(index: Int) {
                        val item =
                            if (index >= 0 && index < mBackgroundImages.size) mBackgroundImages[index] else null
                        if (null != item) {
                            setCurrentBackgroundItem(index, false)
                            setBackgroundImage(item.id)
                        }
                    }
                }
        })
    }

    companion object {
        private val log = SRLog.create("style.p.c")
    }
}