/*
 * book reader based on crengine-ng
 * Copyright (C) 2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.coolreader_ng.project_s.db.DBService

internal class SearchHistoryAdapter(private val itemList: List<DBService.SearchHistoryItem>) :
    RecyclerView.Adapter<SearchHistoryAdapter.SearchHistoryItemViewHolder>() {

    inner class SearchHistoryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.findViewById(R.id.textView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHistoryItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.simple_list_item, parent, false)
        return SearchHistoryItemViewHolder(itemView)
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: SearchHistoryItemViewHolder, position: Int) {
        val item: DBService.SearchHistoryItem? = if (position >= 0 && position < itemList.size)
            itemList[position]
        else
            null
        item?.let {
            holder.textView.text = it.searchQuery
        }
    }
}
