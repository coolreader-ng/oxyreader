/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Canvas
import android.os.Build
import android.view.SurfaceHolder
import java.util.concurrent.atomic.AtomicBoolean

class AnimationThread(surfaceHolder: SurfaceHolder?) : Thread("AnimationThread") {

    private var mSurfaceHolder: SurfaceHolder? = null
    private var mAnimation: AbstractAnimation? = null
    private var mForwardDirection = false
    private var mOnCompleteCallback: (() -> Unit)? = null
    private var mOnStopCallback: (() -> Unit)? = null
    private var mAcceleration = 0f
    private val mStopRequested = AtomicBoolean(false)
    private val mIsAnimationActive = AtomicBoolean(false)
    private val mWaitObject = Object()
    private val mSyncObject = Object()
    private val mRingBuffer = RingBuffer(32, 17)

    /**
     * Start specific animation.
     *
     * @param animation        animation to start
     * @param forward
     * @param speed
     * @param acceleration
     * @param completeCallback callback to run after completion.
     */
    fun startAnimation(
        animation: AbstractAnimation,
        forward: Boolean,
        speed: Float,
        acceleration: Float,
        completeCallback: (() -> Unit)?
    ) {
        synchronized(mSyncObject) {
            mAnimation = animation
            mAnimation!!.setSpeed(speed)
            mForwardDirection = forward
            mAcceleration = acceleration
            mOnCompleteCallback = completeCallback
        }
        synchronized(mWaitObject) {
            mWaitObject.notifyAll()
        }
    }

    /**
     * Sets on stop runnable. This runnable is run every time the animation stops.
     *
     * @param runnable runnable
     */
    fun setOnStopCallback(callback: (() -> Unit)?) {
        synchronized(mSyncObject) {
            mOnStopCallback = callback
        }
    }

    /**
     * Stop current animation.
     * Blocked while the last frame is being rendered.
     */
    fun stopAnimation() {
        synchronized(mSyncObject) {
            mAnimation = null
        }
        synchronized(mWaitObject) {
            mWaitObject.notifyAll()
        }
    }

    /**
     * Returns animation status.
     *
     * @return animation status, true if animation is active, false - otherwise.
     */
    val isAnimationActive: Boolean
        get() = mIsAnimationActive.get()

    /**
     * Returns the average frame generation time in milliseconds.
     *
     * @return average frame generation time in milliseconds
     */
    val avgFrameTime: Long
        get() = mRingBuffer.average()

    /**
     * Stops this animation thread. After this operation, no animation can be started.
     */
    fun shutdown() {
        log.debug("stop, waiting")
        synchronized(mWaitObject) {
            mStopRequested.compareAndSet(false, true)
            mWaitObject.notifyAll()
        }
        // wait thread to die
        while (true) {
            try {
                join()
                break
            } catch (ignored: InterruptedException) {
            }
        }
    }

    override fun run() {
        var waitNeeded: Boolean
        var frameStartTime: Long
        var frameGenTime: Long
        while (!mStopRequested.get()) {
            var needCallOnComplete = false
            var needCallOnStop = false
            synchronized(mSyncObject) {
                // 1. check is animation object is set
                if (null != mAnimation) {
                    mIsAnimationActive.compareAndSet(false, true)
                    // 2. increase/advance animation
                    if (mForwardDirection)
                        mAnimation!!.autoIncrementPosition(mAcceleration)
                    else
                        mAnimation!!.autoDecrementPosition(mAcceleration)
                    // 3. render frame
                    frameStartTime = System.currentTimeMillis()
                    var canvas: Canvas? = null
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        try {
                            canvas = mSurfaceHolder!!.lockHardwareCanvas()
                        } catch (ignored: Exception) {
                        }
                    }
                    try {
                        if (canvas == null) canvas = mSurfaceHolder!!.lockCanvas()
                        if (canvas != null) {
                            //Log.i(TAG, "render frame...");
                            if (!mAnimation!!.renderFrame(canvas)) {
                                log.error("failed to render frame, stopping animation!")
                                mAnimation!!.resetPosition()
                                if (!mAnimation!!.renderFrame(canvas))
                                    log.error("failed to render frame (in reset position)!")
                                mAnimation = null
                            }
                        } else {
                            log.error("failed to lock canvas, stopping animation!")
                            mAnimation = null
                        }
                    } finally {
                        if (canvas != null) {
                            mSurfaceHolder!!.unlockCanvasAndPost(canvas)
                        }
                    }
                    frameGenTime = System.currentTimeMillis() - frameStartTime
                    //log.d("frameGenTime=" + frameGenTime + " ms");
                    mRingBuffer.add(frameGenTime)
                    waitNeeded = false
                    if (null != mAnimation && mAnimation!!.isAnimationComplete) {
                        // animation is done
                        // 4. call onComplete callback & unset animation object
                        log.debug("animation finished")
                        mAnimation = null
                        waitNeeded = true
                        needCallOnComplete = true
                    }
                    if (null == mAnimation) {
                        mIsAnimationActive.compareAndSet(true, false)
                        needCallOnStop = true
                    }
                } else {
                    waitNeeded = true
                    // if animation stopped via stopAnimation() method
                    if (mIsAnimationActive.compareAndSet(true, false)) {
                        needCallOnStop = true
                        mOnCompleteCallback = null
                    }
                }
            } // synchronized (m_syncObject)
            if (needCallOnComplete) {
                mOnCompleteCallback?.invoke()
                mOnCompleteCallback = null
                needCallOnComplete = false
            }
            if (needCallOnStop) {
                // 'onStop' callback must be called after the 'onComplete' callback
                mOnCompleteCallback = null
                mOnStopCallback?.invoke()
                needCallOnStop = false
            }
            if (mStopRequested.get()) {
                log.debug("exiting, since stop requested (1)")
                break
            }
            // 5. waiting for the animation object.
            if (waitNeeded) {
                synchronized(mWaitObject) {
                    try {
                        mWaitObject.wait(0)
                    } catch (e: InterruptedException) {
                        log.debug("wait interrupted: $e")
                    } catch (e: IllegalMonitorStateException) {
                        log.debug(e.toString())
                    }
                }
            }
            if (mStopRequested.get()) {
                log.debug("exiting, since stop requested (2)")
                break
            }
        }
        log.debug("animation thread is finished")
    }

    companion object {
        private val log = SRLog.create("animth")
    }

    init {
        priority = MIN_PRIORITY
        synchronized(mSyncObject) {
            mAnimation = null
            mSurfaceHolder = surfaceHolder
            mOnStopCallback = null
            mOnCompleteCallback = null
            mAcceleration = 0.0f
        }
        log.debug("AnimationThread created")
    }
}