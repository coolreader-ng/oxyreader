/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteStatement
import android.provider.BaseColumns
import io.gitlab.coolreader_ng.genrescollection.GenreRecord
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.BookInfo
import io.gitlab.coolreader_ng.project_s.Bookmark
import io.gitlab.coolreader_ng.project_s.CREngineNGBinding
import io.gitlab.coolreader_ng.project_s.DocumentFormat
import io.gitlab.coolreader_ng.project_s.FileInfo
import io.gitlab.coolreader_ng.project_s.SRLog
import io.gitlab.coolreader_ng.project_s.Utils
import java.io.Closeable
import java.util.regex.Pattern


class DBWrapper(private val db: SQLiteDatabase, private val context: Context) : Closeable {

    private var mDirectoryInsertStmt: SQLiteStatement? = null
    private var mSeriesInsertStmt: SQLiteStatement? = null
    private var mAuthorInsertStmt: SQLiteStatement? = null
    private var mKeywordInsertStmt: SQLiteStatement? = null
    private var mUnsupportedGenreInsertStmt: SQLiteStatement? = null

    private val mAuthorsHandbook = Handbook()
    private val mSeriesHandbook = Handbook()
    private val mKeywordsHandbook = Handbook()
    private val mUnsupportedGenresHandbook = Handbook()
    private val mDirectoriesHandbook = Handbook()

    private fun Cursor.getStringWithNull(columnIndex: Int): String? {
        if (this.isNull(columnIndex))
            return null
        return this.getString(columnIndex)
    }

    private fun Cursor.getIntWithNull(columnIndex: Int): Int? {
        if (this.isNull(columnIndex))
            return null
        return this.getInt(columnIndex)
    }

    private fun Cursor.getIntAllowNull(columnIndex: Int, replacement: Int = 0): Int {
        if (this.isNull(columnIndex))
            return replacement
        return this.getInt(columnIndex)
    }

    private fun Cursor.getLongWithNull(columnIndex: Int): Long? {
        if (this.isNull(columnIndex))
            return null
        return this.getLong(columnIndex)
    }

    private fun Cursor.getLongAllowNull(columnIndex: Int, replacement: Long = 0): Long {
        if (this.isNull(columnIndex))
            return replacement
        return this.getLong(columnIndex)
    }

    private fun Cursor.getBlobWithNull(columnIndex: Int): ByteArray? {
        if (this.isNull(columnIndex))
            return null
        return this.getBlob(columnIndex)
    }

    override fun close() {
        try {
            mDirectoryInsertStmt?.close()
            mSeriesInsertStmt?.close()
            mAuthorInsertStmt?.close()
            mKeywordInsertStmt?.close()
            mUnsupportedGenreInsertStmt?.close()
        } catch (_: Exception) {
        }
        mAuthorsHandbook.clear()
        mSeriesHandbook.clear()
        mKeywordsHandbook.clear()
        mDirectoriesHandbook.clear()
        db.close()
    }

    /**
     * Load book information from database by path name.
     * @param fileInfo file information
     * @return if successful, a BookInfo object that contains all information about the book: authors, title, series, genres, bookmarks, etc., null otherwise.
     */
    fun loadBookInfo(fileInfo: FileInfo): BookInfo? {
        try {
            val bookInfo = BookInfo(FileInfo(fileInfo))
            if (loadByPathname(bookInfo)) {
                loadBookmarks(bookInfo)
                return bookInfo
            }
        } catch (e: Exception) {
            log.error("loadBookInfo() failed", e)
        }
        return null
    }

    /**
     * Save book information to database
     * @param bookInfo book to be saved
     * @return true if book information successfully saved, false otherwise.
     *
     * When you save information about a book, all bookmarks for that book are also saved.
     * In addition, when saving, if necessary, the reference book of authors is replenished.
     */
    fun saveBookInfo(bookInfo: BookInfo): Boolean {
        var res = false
        try {
            // save main data
            res = saveMainBookInfo(bookInfo)
            if (res) {
                // save bookmarks
                val existingBookmarks: HashMap<String, Bookmark> = loadBookmarksHash(bookInfo)
                var changed = 0
                var removed = 0
                var added = 0
                var failed = 0
                for (bmk in bookInfo.allBookmarks) {
                    val existing = existingBookmarks[bmk.uniqueKey]
                    if (existing != null) {
                        bmk.id = existing.id
                        if (bmk != existing) {
                            if (saveBookmark(bookInfo.id!!, bmk))
                                changed++
                            else
                                failed++
                        }
                        existingBookmarks.remove(bmk.uniqueKey) // saved
                    } else {
                        // create new
                        if (saveBookmark(bookInfo.id!!, bmk))
                            added++
                        else
                            failed++
                    }
                }
                if (existingBookmarks.isNotEmpty()) {
                    // remove bookmarks that are missing in the new object
                    for ((_, bmk) in existingBookmarks.entries) {
                        if (deleteBookmark(bmk))
                            removed++
                        else
                            failed++
                    }
                }
                if (added + changed + removed > 0)
                    log.info("bookmarks: added:$added, updated: $changed, removed:$removed, failed:$failed")
            }
        } catch (e: Exception) {
            log.error("saveBookInfo() failed", e)
        }
        return res
    }

    /**
     * Delete book record from database
     * @param bookInfo book to be deleted
     * @return ID of the deleted book if successful, null otherwise.
     */
    fun removeBookInfo(bookInfo: BookInfo): Long? {
        val bookId: Long? = getBookId(bookInfo)
        var res = false
        bookId?.let { id ->
            try {
                db.beginTransaction()
                db.delete(
                    DBContract.BookCoverTable.TABLE_NAME,
                    "${DBContract.BookCoverTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookmarkTable.TABLE_NAME,
                    "${DBContract.BookmarkTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookAuthorTable.TABLE_NAME,
                    "${DBContract.BookAuthorTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookKeywordTable.TABLE_NAME,
                    "${DBContract.BookKeywordTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookGenreTable.TABLE_NAME,
                    "${DBContract.BookGenreTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookUnsupportedGenreTable.TABLE_NAME,
                    "${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_BOOK}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.BookTable.TABLE_NAME,
                    "${BaseColumns._ID}=?",
                    arrayOf("$id")
                )
                db.delete(
                    DBContract.CurrentBookTable.TABLE_NAME,
                    "${DBContract.CurrentBookTable.COLUMN_NAME_FILEPATH}=?",
                    arrayOf(bookInfo.fileInfo.pathNameWA)
                )
                db.setTransactionSuccessful()
                res = true
            } catch (e: Exception) {
                log.error("deleteBookInfo() failed", e)
            } finally {
                db.endTransaction()
            }
        }
        return if (res) bookId else null
    }

    /**
     * Find books by fingerprint or by book metadata (if fingerprint not specified)
     * @param bookInfo book metadata
     * @return BookInfo collection if any books are found, null otherwise
     */
    fun findBooks(bookInfo: BookInfo): Collection<BookInfo>? {
        var resList: Collection<BookInfo>? = null
        if (bookInfo.fileInfo.fingerprint?.isNotEmpty() == true)
            resList = findBooksBy(
                DBContract.BookTable.COLUMN_NAME_FINGERPRINT,
                bookInfo.fileInfo.fingerprint
            )
        if (null == resList)
            resList = findBooksByMetadata(bookInfo)
        if (resList != null) {
            for (bi in resList) {
                loadBookmarks(bi)
            }
        }
        return resList
    }

    /**
     * Find book by path
     * @param path full path to book
     * @return BookInfo collection if any books are found, null otherwise
     */
    fun findBooksByPathName(path: String): Collection<BookInfo>? {
        val resList: Collection<BookInfo>? =
            findBooksBy(DBContract.BookTable.COLUMN_NAME_FILEPATH, path)?.also {
                for (bi in it) {
                    loadBookmarks(bi)
                }
            }
        return resList
    }

    /**
     * Find book by fingerprint
     * @param fingerprint fingerprint to search
     * @return BookInfo collection if any books are found, null otherwise
     */
    fun findBooksByFingerprint(fingerprint: String): Collection<BookInfo>? {
        val resList: Collection<BookInfo>? =
            findBooksBy(DBContract.BookTable.COLUMN_NAME_FINGERPRINT, fingerprint)?.also {
                for (bi in it) {
                    loadBookmarks(bi)
                }
            }
        return resList
    }

    /**
     * Get book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @return book cover image data if found, null - otherwise
     */
    fun getBookCoverData(bookInfo: BookInfo): ByteArray? {
        var bookId = bookInfo.id
        if (null == bookId) {
            val books = findBooks(bookInfo)
            if (null != books) {
                val iter = books.iterator()
                if (iter.hasNext()) {
                    val book0 = iter.next()
                    bookId = book0.id
                }
            }
        }
        if (null == bookId) {
            // No books found with this metadata
            return null
        }
        var coverData: ByteArray? = null
        try {
            db.rawQuery(
                "SELECT ${DBContract.BookCoverTable.COLUMN_NAME_COVER_DATA} " +
                        "FROM ${DBContract.BookCoverTable.TABLE_NAME} " +
                        "WHERE ${DBContract.BookCoverTable.COLUMN_NAME_BOOK}=$bookId",
                null
            ).use { rs ->
                if (rs.moveToFirst()) {
                    if (!rs.isNull(0)) {
                        coverData = rs.getBlob(0)
                    }
                }
            }
        } catch (e: SQLException) {
            log.error("getBookCoverData() error", e)
        }
        return coverData
    }

    /**
     * Save book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @return true if successfully, false - otherwise.
     */
    fun saveBookCoverData(bookInfo: BookInfo, data: ByteArray?): Boolean {
        var bookId = bookInfo.id
        if (null == bookId) {
            val books = findBooks(bookInfo)
            if (null != books) {
                val iter = books.iterator()
                if (iter.hasNext()) {
                    val book0 = iter.next()
                    bookId = book0.id
                }
            }
        }
        if (null == bookId) {
            // No books found with this metadata
            return false
        }
        var res = false
        try {
            db.beginTransaction()
            if (null == data) {
                db.execSQL(
                    "DELETE FROM ${DBContract.BookCoverTable.TABLE_NAME} " +
                            "WHERE ${DBContract.BookCoverTable.COLUMN_NAME_BOOK}=$bookId"
                )
                db.setTransactionSuccessful()
                res = true
            } else {
                val countStmt = db.compileStatement(
                    "SELECT COUNT(*) FROM ${DBContract.BookCoverTable.TABLE_NAME} " +
                            "WHERE ${DBContract.BookCoverTable.COLUMN_NAME_BOOK}=$bookId"
                )
                val count = countStmt.simpleQueryForLong()
                val stmt = if (count > 0) {
                    db.compileStatement(
                        "UPDATE ${DBContract.BookCoverTable.TABLE_NAME} " +
                                "SET ${DBContract.BookCoverTable.COLUMN_NAME_COVER_DATA}=? " +
                                "WHERE ${DBContract.BookCoverTable.COLUMN_NAME_BOOK}=$bookId"
                    )
                } else {
                    db.compileStatement(
                        "INSERT INTO ${DBContract.BookCoverTable.TABLE_NAME} " +
                                "(${DBContract.BookCoverTable.COLUMN_NAME_BOOK}, " +
                                "${DBContract.BookCoverTable.COLUMN_NAME_COVER_DATA})" +
                                "VALUES ($bookId, ?)"
                    )
                }
                stmt.bindBlob(1, data)
                res = (1 == stmt.executeUpdateDelete())
                if (res)
                    db.setTransactionSuccessful()
            }
        } catch (e: SQLException) {
            log.error("saveBookCoverData(): $e")
        } finally {
            db.endTransaction()
        }
        return res
    }

    /**
     * Load recent books list (with bookmarks)
     * @param maxCount is max number of recent books to get
     * @return list of loaded books
     */
    fun loadRecentBooks(maxCount: Int): Collection<BookInfo>? {
        val resList = ArrayList<BookInfo>()
        val tailClause = StringBuilder(" WHERE")
        tailClause.append(" ${DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME}>0")
        tailClause.append(" ORDER BY ${DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME}")
        tailClause.append(" DESC LIMIT $maxCount")
        try {
            db.rawQuery("${DBContract.BookTable.SQL_QUERY_ALL} $tailClause", null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val bi = BookInfo()
                        readBookInfoFromCursor(bi, rs)
                        resList.add(bi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadRecentBooks() error", e)
        }
        for (bi in resList) {
            loadBookmarks(bi)
        }
        return resList
    }

    /**
     * Remove book from recent list
     * @param bookInfo book info to remove from recent list
     */
    fun removeBooksFromRecent(bookInfo: BookInfo): Boolean {
        var res = false
        val bookId = getBookId(bookInfo)
        bookId?.let { id ->
            try {
                res = db.update(
                    DBContract.BookTable.TABLE_NAME,
                    ContentValues(1).apply {
                        put(DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME, 0)
                    },
                    "${BaseColumns._ID}=?",
                    arrayOf("$id")
                ) > 0
            } catch (e: Exception) {
                log.error("removeBooksFromRecent():", e)
            }
        }
        return res
    }

    /**
     * Load filtered book list (with bookmarks)
     * @param sorting book sorting attribute
     * @param filters book filters collection
     * @return book information collection
     */
    fun loadBookList(
        sorting: DBService.BooksSorting,
        filters: DBService.BookFilters
    ): Collection<BookInfo>? {
        val resList = ArrayList<BookInfo>()
        val joinClause = StringBuilder()
        val whereClause = StringBuilder()
        val sortClause = StringBuilder()
        var insertDistinctClause = false
        var haveBookAuthorJoin = false
        var haveAuthorJoin = false
        var haveBookGenreJoin = false
        var haveUnsupportedBookGenreJoin = false
        var haveBookKeywordJoin = false
        var haveKeywordJoin = false
        if (filters.isNotEmpty()) {
            whereClause.append(" WHERE")
            val filterIter = filters.iterator()
            while (filterIter.hasNext()) {
                val filter = filterIter.next()
                when (filter.filterField) {
                    DBService.BookFilterType.FilterAuthor -> {
                        insertDistinctClause = true
                        if (!haveBookAuthorJoin) {
                            joinClause.append(" LEFT JOIN ${DBContract.BookAuthorTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                            haveBookAuthorJoin = true
                        }
                        if (!haveAuthorJoin) {
                            joinClause.append(" INNER JOIN ${DBContract.AuthorTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.AuthorTable.TABLE_NAME}.${BaseColumns._ID}=${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_AUTHOR}")
                            haveAuthorJoin = true
                        }
                        whereClause.append(" ${DBContract.AuthorTable.TABLE_NAME}.${DBContract.AuthorTable.COLUMN_NAME_NAME} LIKE '%${filter.filterValue}%'")
                    }

                    DBService.BookFilterType.FilterAuthorId -> {
                        if (!haveBookAuthorJoin) {
                            joinClause.append(" LEFT JOIN ${DBContract.BookAuthorTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                            haveBookAuthorJoin = true
                        }
                        whereClause.append(" ${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_AUTHOR}=${filter.filterValue}")
                    }

                    DBService.BookFilterType.FilterSeries -> {
                        whereClause.append(" ${DBContract.SeriesTable.TABLE_NAME}.${DBContract.SeriesTable.COLUMN_NAME_NAME} LIKE '%${filter.filterValue}%'")
                    }

                    DBService.BookFilterType.FilterSeriesId -> {
                        whereClause.append(" ${DBContract.SeriesTable.TABLE_NAME}.${BaseColumns._ID}=${filter.filterValue}")
                    }

                    DBService.BookFilterType.FilterGenreId -> {
                        val genreId = try {
                            filter.filterValue.toInt()
                        } catch (e: Exception) {
                            -1
                        }
                        if (genreId >= DBService.UnsupportedGenreIdBase) {
                            if (!haveUnsupportedBookGenreJoin) {
                                joinClause.append(" LEFT JOIN ${DBContract.BookUnsupportedGenreTable.TABLE_NAME} ON")
                                joinClause.append(" ${DBContract.BookUnsupportedGenreTable.TABLE_NAME}.${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                                haveUnsupportedBookGenreJoin = true
                            }
                            whereClause.append(" ${DBContract.BookUnsupportedGenreTable.TABLE_NAME}.${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_UNSUPPORTED_GENRE}=${genreId - DBService.UnsupportedGenreIdBase}")
                        } else {
                            if (!haveBookGenreJoin) {
                                joinClause.append(" LEFT JOIN ${DBContract.BookGenreTable.TABLE_NAME} ON")
                                joinClause.append(" ${DBContract.BookGenreTable.TABLE_NAME}.${DBContract.BookGenreTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                                haveBookGenreJoin = true
                            }
                            fun getChildGenreIdsList(list: MutableList<Int>, genre: GenreRecord) {
                                list.add(genre.id)
                                if (genre.hasChilds()) {
                                    for (child in genre.childs) {
                                        getChildGenreIdsList(list, child)
                                    }
                                }
                            }

                            val genresIds = mutableListOf<Int>()
                            val genre = GenresCollection.getInstance(context).byId(genreId)
                            genre?.let { getChildGenreIdsList(genresIds, genre) }
                            if (genresIds.isEmpty())
                                whereClause.append(" ${DBContract.BookGenreTable.TABLE_NAME}.${DBContract.BookGenreTable.COLUMN_NAME_GENRE}=$genreId")
                            else {
                                val genresIdsStr = StringBuilder()
                                val iter = genresIds.iterator()
                                while (iter.hasNext()) {
                                    genresIdsStr.append(iter.next().toString())
                                    if (iter.hasNext())
                                        genresIdsStr.append(",")
                                }
                                whereClause.append(" ${DBContract.BookGenreTable.TABLE_NAME}.${DBContract.BookGenreTable.COLUMN_NAME_GENRE} IN ($genresIdsStr)")
                            }
                        }
                    }

                    DBService.BookFilterType.FilterKeyword -> {
                        insertDistinctClause = true
                        if (!haveBookKeywordJoin) {
                            joinClause.append(" LEFT JOIN ${DBContract.BookKeywordTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.BookKeywordTable.TABLE_NAME}.${DBContract.BookKeywordTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                            haveBookKeywordJoin = true
                        }
                        if (!haveKeywordJoin) {
                            joinClause.append(" INNER JOIN ${DBContract.KeywordTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.KeywordTable.TABLE_NAME}.${BaseColumns._ID}=${DBContract.BookKeywordTable.TABLE_NAME}.${DBContract.BookKeywordTable.COLUMN_NAME_KEYWORD}")
                            haveKeywordJoin = true
                        }
                        whereClause.append(" ${DBContract.KeywordTable.TABLE_NAME}.${DBContract.KeywordTable.COLUMN_NAME_NAME} LIKE '%${filter.filterValue}%'")
                    }

                    DBService.BookFilterType.FilterKeywordId -> {
                        if (!haveBookKeywordJoin) {
                            joinClause.append(" LEFT JOIN ${DBContract.BookKeywordTable.TABLE_NAME} ON")
                            joinClause.append(" ${DBContract.BookKeywordTable.TABLE_NAME}.${DBContract.BookKeywordTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
                            haveBookKeywordJoin = true
                        }
                        whereClause.append(" ${DBContract.BookKeywordTable.TABLE_NAME}.${DBContract.BookKeywordTable.COLUMN_NAME_KEYWORD}=${filter.filterValue}")
                    }

                    DBService.BookFilterType.FilterLanguage -> {
                        whereClause.append(" ${DBContract.BookTable.COLUMN_NAME_LANGUAGE}='${filter.filterValue}'")
                    }

                    DBService.BookFilterType.FilterStatus -> {
                        whereClause.append(" ${DBContract.BookTable.COLUMN_NAME_STATUS}=${filter.filterValue}")
                    }

                    DBService.BookFilterType.FilterRating -> {
                        whereClause.append(" ${DBContract.BookTable.COLUMN_NAME_RATING}=${filter.filterValue}")
                    }
                }
                if (filterIter.hasNext())
                    whereClause.append(" AND")
            }
        }
        var sortingEnabled = true
        when (sorting.sortType) {
            DBService.BookSortType.SortNone -> sortingEnabled = false
            DBService.BookSortType.SortByAuthor -> sortClause.append(" ORDER BY authors")
            DBService.BookSortType.SortByTitle -> sortClause.append(" ORDER BY ${DBContract.BookTable.COLUMN_NAME_TITLE}")
            DBService.BookSortType.SortBySeries -> sortClause.append(" ORDER BY series_name")
            DBService.BookSortType.SortByLastReadTime -> sortClause.append(" ORDER BY ${DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME}")
        }
        if (sortingEnabled && sorting.sortDesc)
            sortClause.append(" DESC")
        var query = "${DBContract.BookTable.SQL_QUERY_ALL} $joinClause $whereClause $sortClause"
        if (insertDistinctClause)
            query = Pattern.compile("^SELECT ").matcher(query).replaceFirst("SELECT DISTINCT ")
        try {
            db.rawQuery(query, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val bi = BookInfo()
                        readBookInfoFromCursor(bi, rs)
                        resList.add(bi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadBookList() error", e)
        }
        for (bi in resList) {
            loadBookmarks(bi)
        }
        return resList
    }

    /**
     * Gets the specified handbook
     * @param handbookType type of the handbook
     * @return handbook
     */
    fun getHandbook(handbookType: DBService.HandbookType): Handbook {
        val handbook =
            when (handbookType) {
                DBService.HandbookType.HandbookAuthor -> Handbook(mAuthorsHandbook)
                DBService.HandbookType.HandbookSeries -> Handbook(mSeriesHandbook)
                DBService.HandbookType.HandbookReferencedGenre -> {
                    // There is no genre handbook in the database, but only references to some genre identifiers.
                    // This way we only get a list of genre identifiers used.
                    val handbook = Handbook()
                    loadReferencedGenresHandbook(handbook)
                    handbook
                }

                DBService.HandbookType.HandbookLanguage -> {
                    // There is no language handbook in the database
                    // This way we only get a list of languages used.
                    val handbook = Handbook()
                    loadReferencedLanguagesHandbook(handbook)
                    handbook
                }

                DBService.HandbookType.HandbookKeywords -> Handbook(mKeywordsHandbook)
                DBService.HandbookType.HandbookUnsupportedGenres -> Handbook(
                    mUnsupportedGenresHandbook
                )

                DBService.HandbookType.HandbookDirectories -> Handbook(mDirectoriesHandbook)
            }
        return handbook
    }

    /**
     * Load book search history
     * @param maxCount maximum items count in result
     * @return search history as collection of items
     */
    fun loadBookSearchHistory(maxCount: Int): Collection<DBService.SearchHistoryItem>? {
        val resList = ArrayList<DBService.SearchHistoryItem>()
        val query = DBContract.BookSearchHistoryTable.SQL_QUERY_ALL +
                " ORDER BY ${DBContract.BookSearchHistoryTable.COLUMN_NAME_TIMESTAMP} DESC LIMIT $maxCount"
        try {
            db.rawQuery(query, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val hi = DBService.SearchHistoryItem(rs.getLong(0), rs.getString(1))
                        resList.add(hi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadBookSearchHistory() error", e)
        }
        return resList
    }

    /**
     * Save new book search history item
     * @param searchItem book search history item
     * @return save operation result
     */
    fun saveBookSearchHistoryItem(searchItem: DBService.SearchHistoryItem): Boolean {
        var res = false
        try {
            val values = ContentValues(2)
            values.put(
                DBContract.BookSearchHistoryTable.COLUMN_NAME_TIMESTAMP,
                searchItem.timeStamp
            )
            values.put(
                DBContract.BookSearchHistoryTable.COLUMN_NAME_SEARCH_QUERY,
                searchItem.searchQuery
            )
            res = db.insertOrThrow(DBContract.BookSearchHistoryTable.TABLE_NAME, null, values) >= 0
        } catch (e: SQLException) {
            log.error("saveBookSearchHistoryItem() error", e)
        }
        return res
    }

    /**
     * Remove book search history item
     * @param searchItem book search history item
     * @return remove operation result
     */
    fun removeBookSearchHistoryItem(searchItem: DBService.SearchHistoryItem): Boolean {
        var res = false
        try {
            db.delete(
                DBContract.BookSearchHistoryTable.TABLE_NAME,
                "${DBContract.BookSearchHistoryTable.COLUMN_NAME_TIMESTAMP}=${searchItem.timeStamp}",
                null
            )
            res = true
        } catch (e: SQLException) {
            log.error("saveBookSearchHistoryItem() error", e)
        }
        return res
    }

    /**
     * Clear book search history
     * @return remove operation result
     */
    fun clearBookSearchHistoryItem(): Boolean {
        var res = false
        try {
            db.delete(DBContract.BookSearchHistoryTable.TABLE_NAME, null, null)
            res = true
        } catch (e: SQLException) {
            log.error("saveBookSearchHistoryItem() error", e)
        }
        return res
    }

    /**
     * Search books by search query
     * @param searchQuery search query
     * @param sorting book sorting attribute
     * @return BookInfo collection if any books are found, null otherwise
     *
     * Search is carried out only by author, title and series.
     */
    fun searchBooks(searchQuery: String, sorting: DBService.BooksSorting): Collection<BookInfo>? {
        val resList = ArrayList<BookInfo>()
        val joinClause = StringBuilder()
        val whereClause = StringBuilder()
        val sortClause = StringBuilder()
        val condition = StringBuilder()
        val parts = searchQuery.split(" ")
        for (p in parts) {
            val part = p.trim()
            if (part.isNotEmpty()) {
                condition.append("%${part}")
            }
        }
        if (condition.isNotEmpty()) {
            condition.append("%")
            joinClause.append(" LEFT JOIN ${DBContract.BookAuthorTable.TABLE_NAME} ON")
            joinClause.append(" ${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_BOOK}=${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}")
            joinClause.append(" INNER JOIN ${DBContract.AuthorTable.TABLE_NAME} ON")
            joinClause.append(" ${DBContract.AuthorTable.TABLE_NAME}.${BaseColumns._ID}=${DBContract.BookAuthorTable.TABLE_NAME}.${DBContract.BookAuthorTable.COLUMN_NAME_AUTHOR}")
            whereClause.append("WHERE")
            whereClause.append(" ${DBContract.AuthorTable.TABLE_NAME}.${DBContract.AuthorTable.COLUMN_NAME_NAME} LIKE '$condition' OR")
            whereClause.append(" ${DBContract.BookTable.COLUMN_NAME_TITLE} LIKE '$condition' OR")
            whereClause.append(" ${DBContract.SeriesTable.TABLE_NAME}.${DBContract.SeriesTable.COLUMN_NAME_NAME} LIKE '$condition'")
        }
        var sortingEnabled = true
        when (sorting.sortType) {
            DBService.BookSortType.SortNone -> sortingEnabled = false
            DBService.BookSortType.SortByAuthor -> sortClause.append(" ORDER BY authors")
            DBService.BookSortType.SortByTitle -> sortClause.append(" ORDER BY ${DBContract.BookTable.COLUMN_NAME_TITLE}")
            DBService.BookSortType.SortBySeries -> sortClause.append(" ORDER BY series_name")
            DBService.BookSortType.SortByLastReadTime -> sortClause.append(" ORDER BY ${DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME}")
        }
        if (sortingEnabled && sorting.sortDesc)
            sortClause.append(" DESC")
        var query = "${DBContract.BookTable.SQL_QUERY_ALL} $joinClause $whereClause $sortClause"
        query = Pattern.compile("^SELECT ").matcher(query).replaceFirst("SELECT DISTINCT ")

        log.debug("query: $query")

        try {
            db.rawQuery(query, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val bi = BookInfo()
                        readBookInfoFromCursor(bi, rs)
                        resList.add(bi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadBookList() error", e)
        }
        for (bi in resList) {
            loadBookmarks(bi)
        }
        return resList
    }

    /**
     * Load text search history (in any book)
     * @param maxCount maximum items count in result
     * @return search history as collection of items
     */
    fun loadTextSearchHistory(maxCount: Int): Collection<DBService.SearchHistoryItem>? {
        val resList = ArrayList<DBService.SearchHistoryItem>()
        val query = DBContract.TextSearchHistoryTable.SQL_QUERY_ALL +
                " ORDER BY ${DBContract.TextSearchHistoryTable.COLUMN_NAME_TIMESTAMP} DESC LIMIT $maxCount"
        try {
            db.rawQuery(query, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val tshi = DBService.SearchHistoryItem(
                            rs.getLong(0),
                            rs.getString(1),
                            rs.getInt(2) != 0,
                            rs.getInt(3) != 0
                        )
                        resList.add(tshi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadTextSearchHistory() error", e)
        }
        return resList
    }

    /**
     * Save new text search history item
     * @param searchItem text search history item
     * @return save operation result
     */
    fun saveTextSearchHistoryItem(searchItem: DBService.SearchHistoryItem): Boolean {
        var res = false
        try {
            db.beginTransaction()
            val countStmt = db.compileStatement(
                "SELECT COUNT(*) FROM ${DBContract.TextSearchHistoryTable.TABLE_NAME} " +
                        "WHERE ${DBContract.TextSearchHistoryTable.COLUMN_NAME_SEARCH_QUERY}=" +
                        DatabaseUtils.sqlEscapeString(searchItem.searchQuery)
            )
            val count = countStmt.simpleQueryForLong()
            val stmt = if (count > 0) {
                db.compileStatement(
                    "UPDATE ${DBContract.TextSearchHistoryTable.TABLE_NAME} SET " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_TIMESTAMP}=?, " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_CASE_SENSITIVITY}=?, " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_WHOLE_WORDS}=? " +
                            "WHERE ${DBContract.TextSearchHistoryTable.COLUMN_NAME_SEARCH_QUERY}='${searchItem.searchQuery}'"
                ).also {
                    it.bindLong(1, searchItem.timeStamp)
                    it.bindLong(2, if (searchItem.caseSensitivity) 1 else 0)
                    it.bindLong(3, if (searchItem.wholeWords) 1 else 0)
                }
            } else {
                db.compileStatement(
                    "INSERT INTO ${DBContract.TextSearchHistoryTable.TABLE_NAME} (" +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_TIMESTAMP}, " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_SEARCH_QUERY}, " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_CASE_SENSITIVITY}, " +
                            "${DBContract.TextSearchHistoryTable.COLUMN_NAME_WHOLE_WORDS} " +
                            ") VALUES (?, ?, ?, ?)"
                ).also {
                    it.bindLong(1, searchItem.timeStamp)
                    it.bindString(2, searchItem.searchQuery)
                    it.bindLong(3, if (searchItem.caseSensitivity) 1 else 0)
                    it.bindLong(4, if (searchItem.wholeWords) 1 else 0)
                }
            }
            res = (1 == stmt.executeUpdateDelete())
            if (res)
                db.setTransactionSuccessful()
        } catch (e: SQLException) {
            log.error("saveTextSearchHistoryItem(): $e")
        } finally {
            db.endTransaction()
        }
        return res
    }

    /**
     * Remove text search history item
     * @param searchItem text search history item
     * @return remove operation result
     */
    fun removeTextSearchHistoryItem(searchItem: DBService.SearchHistoryItem): Boolean {
        var res = false
        try {
            db.delete(
                DBContract.TextSearchHistoryTable.TABLE_NAME,
                "${DBContract.TextSearchHistoryTable.COLUMN_NAME_TIMESTAMP}=${searchItem.timeStamp}",
                null
            )
            res = true
        } catch (e: SQLException) {
            log.error("saveTextSearchHistoryItem() error", e)
        }
        return res
    }

    /**
     * Clear text search history
     * @return remove operation result
     */
    fun clearTextSearchHistoryItem(): Boolean {
        var res = false
        try {
            db.delete(DBContract.TextSearchHistoryTable.TABLE_NAME, null, null)
            res = true
        } catch (e: SQLException) {
            log.error("saveTextSearchHistoryItem() error", e)
        }
        return res
    }

    /**
     * Gets the currently read book.
     * @return full file path to the currently read book, empty string if no current reading book.
     */
    fun getCurrentBook(): String {
        var filePath = ""
        val query = DBContract.CurrentBookTable.SQL_QUERY_ALL +
                " ORDER BY ${DBContract.CurrentBookTable.COLUMN_NAME_TIME} DESC LIMIT 1"
        try {
            db.rawQuery(query, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        filePath = rs.getString(0)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("getCurrentBook() error", e)
        }
        return filePath
    }

    /**
     * Sets the currently read book
     * @param filePath full file path to book file
     * @param timestamp file open event timestamp
     * @return operation result
     */
    fun setCurrentBook(filePath: String, timestamp: Long): Boolean {
        var res = false
        try {
            val values = ContentValues(2)
            values.put(DBContract.CurrentBookTable.COLUMN_NAME_FILEPATH, filePath)
            values.put(DBContract.CurrentBookTable.COLUMN_NAME_TIME, timestamp)
            res = db.insertOrThrow(DBContract.CurrentBookTable.TABLE_NAME, null, values) >= 0
        } catch (e: SQLException) {
            log.error("setCurrentBook() error", e)
        }
        if (!res) {
            // Possible unique constraint failed, trying to update
            try {
                db.update(
                    DBContract.CurrentBookTable.TABLE_NAME,
                    ContentValues(1).apply {
                        put(DBContract.CurrentBookTable.COLUMN_NAME_TIME, timestamp)
                    },
                    "${DBContract.CurrentBookTable.COLUMN_NAME_FILEPATH}=?",
                    arrayOf(filePath)
                ) > 0
            } catch (e: Exception) {
                log.error("setCurrentBook() error", e)
            }
        }
        return res
    }

    /**
     * Unset the currently read book
     * @return operation result
     */
    fun unsetCurrentBook(): Boolean {
        var res = false
        try {
            db.execSQL(DBContract.CurrentBookTable.SQL_DELETE_ALL)
            res = true
        } catch (e: SQLException) {
            log.error("unsetCurrentBook() error", e)
        }
        return res
    }

    // =======================================================================
    // Private functions (implementation)
    // =======================================================================

    private fun getDirectoryId(dirName: String?, allowInsert: Boolean = true): Long? {
        if (dirName == null || dirName.trim().isEmpty())
            return null
        var id: Long? = mDirectoriesHandbook.getByName(dirName)?.id
        if (id != null)
            return id
        if (allowInsert) {
            try {
                if (null == mDirectoryInsertStmt) {
                    mDirectoryInsertStmt = db.compileStatement(
                        "INSERT INTO ${DBContract.DirectoryTable.TABLE_NAME} " +
                                "(${BaseColumns._ID}, ${DBContract.DirectoryTable.COLUMN_NAME_NAME}) " +
                                "VALUES (NULL,?)"
                    )
                }
                mDirectoryInsertStmt!!.bindString(1, dirName)
                id = mDirectoryInsertStmt!!.executeInsert()
                mDirectoriesHandbook.addEntry(id, dirName)
            } catch (_: Exception) {
            }
        }
        return id
    }

    private fun getSeriesId(seriesName: String?, allowInsert: Boolean = true): Long? {
        if (seriesName == null || seriesName.trim().isEmpty())
            return null
        var id: Long? = mSeriesHandbook.getByName(seriesName)?.id
        if (id != null)
            return id
        if (allowInsert) {
            try {
                if (null == mSeriesInsertStmt) {
                    mSeriesInsertStmt = db.compileStatement(
                        "INSERT INTO ${DBContract.SeriesTable.TABLE_NAME} " +
                                "(${BaseColumns._ID}, ${DBContract.SeriesTable.COLUMN_NAME_NAME}) " +
                                "VALUES (NULL,?)"
                    )
                }
                mSeriesInsertStmt!!.bindString(1, seriesName)
                id = mSeriesInsertStmt!!.executeInsert()
                mSeriesHandbook.addEntry(id, seriesName)
            } catch (_: Exception) {
            }
        }
        return id
    }

    private fun getAuthorId(authorName: String?, allowInsert: Boolean = true): Long? {
        if (authorName == null || authorName.trim().isEmpty())
            return null
        var id: Long? = mAuthorsHandbook.getByName(authorName)?.id
        if (id != null)
            return id
        if (allowInsert) {
            try {
                if (mAuthorInsertStmt == null)
                    mAuthorInsertStmt = db.compileStatement(
                        "INSERT INTO ${DBContract.AuthorTable.TABLE_NAME} " +
                                "(${BaseColumns._ID}, ${DBContract.AuthorTable.COLUMN_NAME_NAME}) " +
                                "VALUES (NULL,?)"
                    )
                mAuthorInsertStmt!!.bindString(1, authorName)
                id = mAuthorInsertStmt!!.executeInsert()
                mAuthorsHandbook.addEntry(id, authorName)
            } catch (e: SQLException) {
                log.error("getAuthorId() failed", e)
            }
        }
        return id
    }

    private fun getAuthorIds(
        authorNames: Collection<String>?,
        allowInsert: Boolean = true
    ): Collection<Long>? {
        if (authorNames.isNullOrEmpty())
            return null
        val ids = ArrayList<Long>(authorNames.size)
        for (name in authorNames) {
            val id: Long? = getAuthorId(name, allowInsert)
            if (id != null)
                ids.add(id)
        }
        if (ids.size > 0)
            return ids
        return null
    }

    private fun getKeywordId(keyword: String?, allowInsert: Boolean = true): Long? {
        if (keyword == null || keyword.trim().isEmpty())
            return null
        var id: Long? = mKeywordsHandbook.getByName(keyword)?.id
        if (id != null)
            return id
        if (allowInsert) {
            try {
                if (mKeywordInsertStmt == null)
                    mKeywordInsertStmt = db.compileStatement(
                        "INSERT INTO ${DBContract.KeywordTable.TABLE_NAME} " +
                                "(${BaseColumns._ID}, ${DBContract.KeywordTable.COLUMN_NAME_NAME}) " +
                                "VALUES (NULL,?)"
                    )
                mKeywordInsertStmt!!.bindString(1, keyword)
                id = mKeywordInsertStmt!!.executeInsert()
                mKeywordsHandbook.addEntry(id, keyword)
            } catch (e: SQLException) {
                log.error("getKeywordId() failed", e)
            }
        }
        return id
    }

    private fun getKeywordIds(
        keywords: Collection<String>?,
        allowInsert: Boolean = true
    ): Collection<Long>? {
        if (keywords.isNullOrEmpty())
            return null
        val ids = ArrayList<Long>(keywords.size)
        for (name in keywords) {
            val id: Long? = getKeywordId(name, allowInsert)
            if (id != null)
                ids.add(id)
        }
        if (ids.size > 0)
            return ids
        return null
    }

    private fun getUnsupportedGenreId(genre: String?, allowInsert: Boolean = true): Long? {
        if (genre == null || genre.trim().isEmpty())
            return null
        var id: Long? = mUnsupportedGenresHandbook.getByName(genre)?.id
        if (id != null)
            return id
        if (allowInsert) {
            try {
                if (mUnsupportedGenreInsertStmt == null)
                    mUnsupportedGenreInsertStmt = db.compileStatement(
                        "INSERT INTO ${DBContract.UnsupportedGenreTable.TABLE_NAME} " +
                                "(${BaseColumns._ID}, ${DBContract.UnsupportedGenreTable.COLUMN_NAME_CODE}) " +
                                "VALUES (NULL,?)"
                    )
                mUnsupportedGenreInsertStmt!!.bindString(1, genre)
                id = mUnsupportedGenreInsertStmt!!.executeInsert()
                mUnsupportedGenresHandbook.addEntry(id, genre)
            } catch (e: SQLException) {
                log.error("getUnsupportedGenreId() failed", e)
            }
        }
        return id
    }

    private fun getUnsupportedGenreIds(
        genres: Collection<String>?,
        allowInsert: Boolean = true
    ): Collection<Long>? {
        if (genres.isNullOrEmpty())
            return null
        val ids = ArrayList<Long>(genres.size)
        for (code in genres) {
            val id: Long? = getUnsupportedGenreId(code, allowInsert)
            if (id != null)
                ids.add(id)
        }
        if (ids.size > 0)
            return ids
        return null
    }

    private fun loadAuthorsHandbook(handbook: Handbook) {
        try {
            db.rawQuery(DBContract.AuthorTable.SQL_QUERY_ALL, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        entry.name = rs.getString(1)
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadAuthorsHandbook: $e")
        }
    }

    private fun loadSeriesHandbook(handbook: Handbook) {
        try {
            db.rawQuery(DBContract.SeriesTable.SQL_QUERY_ALL, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        entry.name = rs.getString(1)
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadSeriesHandbook: $e")
        }
    }

    private fun loadReferencedGenresHandbook(handbook: Handbook) {
        try {
            db.rawQuery(
                "SELECT DISTINCT ${DBContract.BookGenreTable.COLUMN_NAME_GENRE} FROM ${DBContract.BookGenreTable.TABLE_NAME}",
                null
            ).use { rs ->
                if (rs.moveToFirst()) {
                    val genresCollection = GenresCollection.getInstance(context)
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        val genre = genresCollection.byId(entry.id.toInt())
                        entry.name = genre?.name ?: "id=${entry.id}"
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadReferencedGenresHandbook: $e")
        }
    }

    private fun loadReferencedLanguagesHandbook(handbook: Handbook) {
        try {
            db.rawQuery(
                "SELECT DISTINCT ${DBContract.BookTable.COLUMN_NAME_LANGUAGE} FROM ${DBContract.BookTable.TABLE_NAME}" +
                        " ORDER BY ${DBContract.BookTable.COLUMN_NAME_LANGUAGE}",
                null
            ).use { rs ->
                if (rs.moveToFirst()) {
                    var i = 0L
                    do {
                        val entry = Handbook.HandbookItem()
                        // fake id
                        entry.id = i
                        entry.name = rs.getString(0)
                        handbook.add(entry)
                        i++
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadReferencedLanguagesHandbook: $e")
        }
    }

    private fun loadKeywordsHandbook(handbook: Handbook) {
        try {
            db.rawQuery(DBContract.KeywordTable.SQL_QUERY_ALL, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        entry.name = rs.getString(1)
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadKeywordsHandbook: $e")
        }
    }

    private fun loadDirectoriesHandbook(handbook: Handbook) {
        try {
            db.rawQuery(DBContract.DirectoryTable.SQL_QUERY_ALL, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        entry.name = rs.getString(1)
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadDirectoriesHandbook: $e")
        }
    }

    private fun loadUnsupportedGenresHandbook(handbook: Handbook) {
        try {
            db.rawQuery(DBContract.UnsupportedGenreTable.SQL_QUERY_ALL, null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val entry = Handbook.HandbookItem()
                        entry.id = rs.getLong(0)
                        entry.name = rs.getString(1)
                        handbook.add(entry)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("loadUnsupportedGenresHandbook: $e")
        }
    }

    private fun getBookId(bookInfo: BookInfo): Long? {
        if (null != bookInfo.id)
            return bookInfo.id
        var bookId: Long? = null
        val bi = BookInfo(bookInfo)
        if (loadByPathname(bi))
            bookId = bi.id
        return bookId
    }

    private inner class QueryHelper(private val tableName: String) {

        private var fields = ArrayList<String>()
        private var values = ArrayList<Any?>()

        constructor(
            newValue: BookInfo,
            oldValue: BookInfo
        ) : this(DBContract.BookTable.TABLE_NAME) {
            add(
                DBContract.BookTable.COLUMN_NAME_FILEPATH,
                newValue.fileInfo.pathNameWA,
                oldValue.fileInfo.pathNameWA
            )
            add(
                DBContract.BookTable.COLUMN_NAME_DIRECTORY,
                getDirectoryId(newValue.fileInfo.dirPath),
                getDirectoryId(oldValue.fileInfo.dirPath, false)
            )
            add(
                DBContract.BookTable.COLUMN_NAME_FILENAME,
                newValue.fileInfo.fileName,
                oldValue.fileInfo.fileName
            )
            add(DBContract.BookTable.COLUMN_NAME_TITLE, newValue.title, oldValue.title)
            add(
                DBContract.BookTable.COLUMN_NAME_SERIES,
                getSeriesId(newValue.series),
                getSeriesId(oldValue.series, false)
            )
            add(
                DBContract.BookTable.COLUMN_NAME_SERIES_NUMBER,
                newValue.seriesNumber,
                oldValue.seriesNumber
            )
            add(
                DBContract.BookTable.COLUMN_NAME_FORMAT,
                documentFormatToInt(newValue.format),
                documentFormatToInt(oldValue.format)
            )
            add(
                DBContract.BookTable.COLUMN_NAME_FILESIZE,
                newValue.fileInfo.size,
                oldValue.fileInfo.size
            )
            add(
                DBContract.BookTable.COLUMN_NAME_ARCSIZE,
                newValue.fileInfo.arcSize,
                oldValue.fileInfo.arcSize
            )
            add(
                DBContract.BookTable.COLUMN_NAME_PACKSIZE,
                newValue.fileInfo.packSize,
                oldValue.fileInfo.packSize
            )
            add(
                DBContract.BookTable.COLUMN_NAME_LAST_VISITED_TIME,
                newValue.fileInfo.lastAccessTime,
                oldValue.fileInfo.lastAccessTime
            )
            add(
                DBContract.BookTable.COLUMN_NAME_MODIFICATION_TIME,
                newValue.fileInfo.modificationTime,
                oldValue.fileInfo.modificationTime
            )
            add(DBContract.BookTable.COLUMN_NAME_FLAGS, newValue.flags, oldValue.flags)
            add(DBContract.BookTable.COLUMN_NAME_LANGUAGE, newValue.language, oldValue.language)
            add(
                DBContract.BookTable.COLUMN_NAME_DESCRIPTION,
                newValue.description,
                oldValue.description
            )
            add(
                DBContract.BookTable.COLUMN_NAME_FINGERPRINT,
                newValue.fileInfo.fingerprint,
                oldValue.fileInfo.fingerprint
            )
            add(
                DBContract.BookTable.COLUMN_NAME_DOMVERSION,
                newValue.domVersion,
                oldValue.domVersion
            )
            add(
                DBContract.BookTable.COLUMN_NAME_RENDFLAGS,
                newValue.blockRenderingFlags,
                oldValue.blockRenderingFlags
            )
            add(
                DBContract.BookTable.COLUMN_NAME_STATUS,
                readingStatusToInt(newValue.readingStatus),
                readingStatusToInt(oldValue.readingStatus)
            )
            add(DBContract.BookTable.COLUMN_NAME_RATING, newValue.rating, oldValue.rating)
            add(DBContract.BookTable.COLUMN_NAME_EXTRA, newValue.extraData, oldValue.extraData)
        }

        constructor(newValue: Bookmark, oldValue: Bookmark, bookId: Long) : this("bookmark") {
            add(
                DBContract.BookmarkTable.COLUMN_NAME_BOOK,
                bookId,
                if (oldValue.id != null) bookId else null
            )
            add(DBContract.BookmarkTable.COLUMN_NAME_TYPE, newValue.type, oldValue.type)
            add(DBContract.BookmarkTable.COLUMN_NAME_PERCENT, newValue.percent, oldValue.percent)
            add(
                DBContract.BookmarkTable.COLUMN_NAME_TIME_STAMP,
                newValue.timeStamp,
                oldValue.timeStamp
            )
            add(
                DBContract.BookmarkTable.COLUMN_NAME_START_POS,
                newValue.startPos,
                oldValue.startPos
            )
            add(DBContract.BookmarkTable.COLUMN_NAME_END_POS, newValue.endPos, oldValue.endPos)
            add(
                DBContract.BookmarkTable.COLUMN_NAME_TITLE_TEXT,
                newValue.titleText,
                oldValue.titleText
            )
            add(DBContract.BookmarkTable.COLUMN_NAME_POS_TEXT, newValue.posText, oldValue.posText)
            add(
                DBContract.BookmarkTable.COLUMN_NAME_COMMENT_TEXT,
                newValue.commentText,
                oldValue.commentText
            )
            add(
                DBContract.BookmarkTable.COLUMN_NAME_READING_TIME,
                newValue.readingTime,
                oldValue.readingTime
            )
        }

        fun add(fieldName: String, value: Int?, oldValue: Int?): QueryHelper {
            if (oldValue != value) {
                fields.add(fieldName)
                values.add(value)
            }
            return this
        }

        fun add(fieldName: String, value: Long?, oldValue: Long?): QueryHelper {
            if (oldValue != value) {
                fields.add(fieldName)
                values.add(value)
            }
            return this
        }

        fun add(fieldName: String, value: String?, oldValue: String?): QueryHelper {
            if (oldValue != value) {
                fields.add(fieldName)
                values.add(value)
            }
            return this
        }

        fun add(fieldName: String, value: Double?, oldValue: Double?): QueryHelper {
            if (oldValue != value) {
                fields.add(fieldName)
                values.add(value)
            }
            return this
        }

        fun add(fieldName: String, value: ByteArray?, oldValue: ByteArray?): QueryHelper {
            if (!Utils.byteArrayIsEquals(value, oldValue)) {
                fields.add(fieldName)
                values.add(value)
            }
            return this
        }

        fun insert(): Long? {
            if (fields.isEmpty()) {
                log.verbose("QueryHelper: no fields to insert")
                return null
            }
            val valueBuf = StringBuilder()
            try {
                val ignoreOption = "" //"OR IGNORE ";
                val buf = StringBuilder("INSERT $ignoreOption INTO ")
                buf.append(tableName)
                buf.append(" (_id")
                for (field in fields) {
                    buf.append(",")
                    buf.append(field)
                }
                buf.append(") VALUES (NULL")
                for (i in fields.indices) {
                    buf.append(",")
                    buf.append("?")
                }
                buf.append(")")
                val sql = buf.toString()
                log.debug("going to execute $sql")
                var stmt: SQLiteStatement? = null
                val id: Long
                try {
                    stmt = db.compileStatement(sql)
                    for (i in 1..values.size) {
                        val v = values[i - 1]
                        valueBuf.append(v.toString())
                        valueBuf.append(",")
                        if (null == v) {
                            stmt!!.bindNull(i)
                        } else {
                            when (v) {
                                is String -> stmt!!.bindString(i, v)
                                is Int -> stmt!!.bindLong(i, v.toLong())
                                is Long -> stmt!!.bindLong(i, v)
                                is Double -> stmt!!.bindDouble(i, v)
                                is ByteArray -> stmt!!.bindBlob(i, v)
                                else -> stmt!!.bindString(i, v.toString())
                            }
                        }
                    }
                    id = stmt!!.executeInsert()
                    log.debug("added entry, id=$id, query=$sql")
                } finally {
                    stmt?.close()
                }
                return id
            } catch (e: Exception) {
                log.error("insert failed: " + e.message)
                log.error("values: $valueBuf")
            }
            return null
        }

        fun update(id: Long?): Boolean {
            if (fields.isEmpty()) {
                log.verbose("QueryHelper: no fields to update")
                return false
            }
            val buf = StringBuilder("UPDATE ")
            buf.append(tableName)
            buf.append(" SET ")
            var first = true
            for (field in fields) {
                if (!first)
                    buf.append(",")
                buf.append(field)
                buf.append("=?")
                first = false
            }
            buf.append(" WHERE ${BaseColumns._ID}=${id}")
            log.verbose("executing $buf")
            try {
                db.execSQL(buf.toString(), values.toTypedArray())
                return true
            } catch (e: Exception) {
                log.error("update failed", e)
            }
            return false
        }
    }

    private fun loadByPathname(bookInfo: BookInfo): Boolean {
        if (findBookBy(
                bookInfo,
                DBContract.BookTable.COLUMN_NAME_FILEPATH,
                bookInfo.fileInfo.pathNameWA
            )
        ) {
            return true
        }
        // TODO: find a moved book with the same fingerprint (if assigned)
        return false
    }

    /**
     * Find book by any specified field value
     * @param resBookInfo BookInfo to write result
     * @param fieldName field name to search
     * @param fieldValue field value
     * @return true if the book is found, false otherwise
     */
    private fun findBookBy(resBookInfo: BookInfo, fieldName: String, fieldValue: Any?): Boolean {
        val whereClause = StringBuilder(" WHERE ")
        whereClause.append(fieldName)
        if (fieldValue == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, fieldValue)
        }
        var found = false
        try {
            db.rawQuery("${DBContract.BookTable.SQL_QUERY_ALL} $whereClause", null).use { rs ->
                if (rs.moveToFirst()) {
                    readBookInfoFromCursor(resBookInfo, rs)
                    found = true
                }
            }
        } catch (e: SQLException) {
            log.error("findBookBy() error", e)
        }
        return found
    }

    /**
     * Find books by any specified field value
     * @param fieldName field name to search
     * @param fieldValue field value
     * @return books collection if found, null otherwise
     */
    private fun findBooksBy(fieldName: String, fieldValue: Any?): Collection<BookInfo>? {
        val resList = ArrayList<BookInfo>()
        val whereClause = StringBuilder(" WHERE ")
        whereClause.append(fieldName)
        if (fieldValue == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, fieldValue)
        }
        try {
            db.rawQuery("${DBContract.BookTable.SQL_QUERY_ALL} $whereClause", null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val bi = BookInfo()
                        readBookInfoFromCursor(bi, rs)
                        resList.add(bi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("findBooksBy() error", e)
        }
        if (resList.isNotEmpty())
            return resList
        return null
    }

    /**
     * Find books by book's metadata
     * @param bookInfo book metadata
     * @return books collection if found, null otherwise
     */
    private fun findBooksByMetadata(bookInfo: BookInfo): Collection<BookInfo>? {
        val resList = ArrayList<BookInfo>()
        val whereClause = StringBuilder(" WHERE ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_TITLE)
        if (bookInfo.title == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, bookInfo.title)
        }
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_SERIES)
        if (bookInfo.series == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, bookInfo.series)
        }
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_SERIES_NUMBER)
        if (bookInfo.seriesNumber == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, bookInfo.seriesNumber)
        }
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_FORMAT)
        whereClause.append("=")
        DatabaseUtils.appendValueToSql(whereClause, documentFormatToInt(bookInfo.format))
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_FILESIZE)
        whereClause.append("=")
        DatabaseUtils.appendValueToSql(whereClause, bookInfo.fileInfo.size)
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_LANGUAGE)
        if (bookInfo.language == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, bookInfo.language)
        }
        whereClause.append(" AND ")
        whereClause.append(DBContract.BookTable.COLUMN_NAME_DESCRIPTION)
        if (bookInfo.description == null) {
            whereClause.append(" IS NULL")
        } else {
            whereClause.append("=")
            DatabaseUtils.appendValueToSql(whereClause, bookInfo.description)
        }
        // TODO: authors, keywords, genresIds
        try {
            db.rawQuery("${DBContract.BookTable.SQL_QUERY_ALL} $whereClause", null).use { rs ->
                if (rs.moveToFirst()) {
                    do {
                        val bi = BookInfo()
                        readBookInfoFromCursor(bi, rs)
                        resList.add(bi)
                    } while (rs.moveToNext())
                }
            }
        } catch (e: SQLException) {
            log.error("findBooksByMetadata() error", e)
        }
        if (resList.isNotEmpty())
            return resList
        return null
    }

    private fun findBookmarkBy(bm: Bookmark, condition: String): Boolean {
        var found = false
        try {
            db.rawQuery("${DBContract.BookmarkTable.SQL_QUERY_ALL} WHERE $condition", null)
                .use { rs ->
                    if (rs.moveToFirst()) {
                        readBookmarkFromCursor(bm, rs)
                        found = true
                    }
                }
        } catch (e: SQLException) {
            log.error("findBookmarkBy() error", e)
        }
        return found
    }

    private fun loadBookmarks(bookInfo: BookInfo): Boolean {
        if (bookInfo.id == null)
            return false // unknown book id
        val bookmarks: ArrayList<Bookmark> = ArrayList()
        if (loadBookmarks(
                bookmarks,
                "${DBContract.BookmarkTable.COLUMN_NAME_BOOK}=${bookInfo.id} ORDER BY" +
                        " ${DBContract.BookmarkTable.COLUMN_NAME_PERCENT}, ${DBContract.BookmarkTable.COLUMN_NAME_TYPE}"
            )
        ) {
            bookInfo.setBookmarks(bookmarks)
            return true
        }
        return false
    }

    private fun loadBookmarksHash(bookInfo: BookInfo): HashMap<String, Bookmark> {
        val map: HashMap<String, Bookmark> = HashMap()
        if (bookInfo.id != null) {
            val bookmarks: ArrayList<Bookmark> = ArrayList()
            if (loadBookmarks(
                    bookmarks,
                    "${DBContract.BookmarkTable.COLUMN_NAME_BOOK}=${bookInfo.id} ORDER BY" +
                            " ${DBContract.BookmarkTable.COLUMN_NAME_TYPE}, ${DBContract.BookmarkTable.COLUMN_NAME_PERCENT}"
                )
            ) {
                for (bm in bookmarks) {
                    // delete non-unique bookmarks
                    val key = bm.uniqueKey
                    if (!map.containsKey(key)) {
                        map[key] = bm
                    } else {
                        log.warn("Removing non-unique bookmark " + bm + " for " + bookInfo.fileInfo.pathNameWA)
                        deleteBookmark(bm)
                    }
                }
            }
        }
        return map
    }

    private fun saveMainBookInfo(bookInfo: BookInfo): Boolean {
        try {
            var authorsChanged = true
            var keywordsChanged = true
            var genresChanged = true
            var unsupportedGenresChanged = true
            val fileInfo = bookInfo.fileInfo
            val oldValue = BookInfo()

            var found = false
            if (null != bookInfo.id) {
                if (findBookBy(
                        oldValue,
                        "${DBContract.BookTable.TABLE_NAME}.${BaseColumns._ID}",
                        bookInfo.id
                    )
                )
                    found = true
            }
            if (!found && fileInfo.fingerprint?.isNotEmpty() == true) {
                if (findBookBy(
                        oldValue,
                        DBContract.BookTable.COLUMN_NAME_FINGERPRINT,
                        fileInfo.fingerprint
                    )
                )
                    found = true
            }
            // There is no need to use findBooksBy() here as there is a chance of overwriting an existing entry
            if (!found && fileInfo.pathNameWA?.isNotEmpty() == true) {
                if (findBookBy(
                        oldValue,
                        DBContract.BookTable.COLUMN_NAME_FILEPATH,
                        fileInfo.pathNameWA
                    )
                )
                    found = true
            }
            if (found) {
                // found, updating
                if (null == bookInfo.id && null != oldValue.id)
                    bookInfo.id = oldValue.id
                if (bookInfo != oldValue) {
                    log.debug("updating book '${fileInfo.pathNameWA}'")
                    val h = QueryHelper(bookInfo, oldValue)
                    h.update(bookInfo.id)
                }
                authorsChanged = bookInfo.authors != oldValue.authors
                keywordsChanged = bookInfo.keywords != oldValue.keywords
                genresChanged = bookInfo.genresIds != oldValue.genresIds
                unsupportedGenresChanged = bookInfo.unsupportedGenres != oldValue.unsupportedGenres
            } else {
                // inserting
                log.debug("inserting new file '${fileInfo.pathNameWA}'")
                val h = QueryHelper(bookInfo, BookInfo())
                bookInfo.id = h.insert()
                authorsChanged = true
                keywordsChanged = true
                genresChanged = true
                unsupportedGenresChanged = true
            }
            bookInfo.id?.let { bookId ->
                if (authorsChanged) {
                    log.debug("updating authors for file '${fileInfo.pathNameWA}'")
                    val toInsert = HashSet<Long>()
                    val toRemove = HashSet<Long>()
                    val newAuthorsIds = getAuthorIds(bookInfo.authors, true)
                    val oldAuthorsIds = getAuthorIds(oldValue.authors, false)
                    oldAuthorsIds?.let { toRemove.addAll(it) }
                    newAuthorsIds?.let { newAuthors ->
                        toInsert.addAll(newAuthors)
                        oldAuthorsIds?.let { toInsert.removeAll(it.toSet()) }
                        toRemove.removeAll(newAuthors.toSet())
                    }
                    saveBookAuthors(bookId, toInsert, toRemove)
                }
                if (keywordsChanged) {
                    log.debug("updating keywords for file '${fileInfo.pathNameWA}'")
                    val toInsert = HashSet<Long>()
                    val toRemove = HashSet<Long>()
                    val newKeywordsIds = getKeywordIds(bookInfo.keywords, true)
                    val oldKeywordsIds = getKeywordIds(oldValue.keywords, false)
                    oldKeywordsIds?.let { toRemove.addAll(it) }
                    newKeywordsIds?.let { newKeywords ->
                        toInsert.addAll(newKeywords)
                        oldKeywordsIds?.let { toInsert.removeAll(it.toSet()) }
                        toRemove.removeAll(newKeywords.toSet())
                    }
                    saveBookKeywords(bookId, toInsert, toRemove)
                }
                if (genresChanged) {
                    log.debug("updating genres for file '${fileInfo.pathNameWA}'")
                    val toInsert = HashSet<Int>()
                    val toRemove = HashSet<Int>()
                    oldValue.genresIds?.let { toRemove.addAll(it) }
                    bookInfo.genresIds?.let { newGenres ->
                        toInsert.addAll(newGenres)
                        oldValue.genresIds?.let { toInsert.removeAll(it) }
                        toRemove.removeAll(newGenres)
                    }
                    saveBookGenres(bookId, toInsert, toRemove)
                }
                if (unsupportedGenresChanged) {
                    log.debug("updating unsupported genres for file '${fileInfo.pathNameWA}'")
                    val toInsert = HashSet<Long>()
                    val toRemove = HashSet<Long>()
                    val newUnGenresIds = getUnsupportedGenreIds(bookInfo.unsupportedGenres, true)
                    val oldUnGenresIds = getUnsupportedGenreIds(oldValue.unsupportedGenres, false)
                    oldUnGenresIds?.let { toRemove.addAll(it) }
                    newUnGenresIds?.let { newUnGenres ->
                        toInsert.addAll(newUnGenres)
                        oldUnGenresIds?.let { toInsert.removeAll(it.toSet()) }
                        toRemove.removeAll(newUnGenres.toSet())
                    }
                    saveBookUnsupportedGenres(bookId, toInsert, toRemove)
                }
                return true
            }
        } catch (e: SQLiteException) {
            log.error("error while writing to DB", e)
        }
        return false
    }

    private fun saveBookmark(bookId: Long, bm: Bookmark): Boolean {
        log.debug("saving bookmark id=${bm.id}, bookId=$bookId, pos=${bm.startPos}")
        var oldValue = Bookmark()
        if (bm.id != null) {
            // update
            oldValue.id = bm.id
            if (findBookmarkBy(
                    oldValue,
                    "${DBContract.BookmarkTable.COLUMN_NAME_BOOK}=$bookId AND ${BaseColumns._ID}=${bm.id}"
                )
            ) {
                // found, updating
                val h = QueryHelper(bm, oldValue, bookId)
                h.update(bm.id)
            } else {
                oldValue = Bookmark()
                val h = QueryHelper(bm, oldValue, bookId)
                bm.id = h.insert()
            }
        } else {
            val h = QueryHelper(bm, oldValue, bookId)
            bm.id = h.insert()
        }
        return true
    }

    private fun deleteBookmark(bm: Bookmark): Boolean {
        return try {
            db.execSQL("DELETE FROM ${DBContract.BookmarkTable.TABLE_NAME} WHERE ${BaseColumns._ID}=${bm.id}")
            true
        } catch (e: SQLException) {
            false
        }
    }

    private fun loadBookmarks(list: ArrayList<Bookmark>, condition: String): Boolean {
        var found = false
        try {
            db.rawQuery("${DBContract.BookmarkTable.SQL_QUERY_ALL} WHERE $condition", null)
                .use { rs ->
                    if (rs.moveToFirst()) {
                        do {
                            val bm = Bookmark()
                            readBookmarkFromCursor(bm, rs)
                            list.add(bm)
                            found = true
                        } while (rs.moveToNext())
                    }
                }
        } catch (e: SQLException) {
            log.error("loadBookmarks() error", e)
        }
        return found
    }

    private fun saveBookAuthors(
        bookId: Long,
        authorIdsToInsert: Collection<Long>?,
        authorIdsToRemove: Collection<Long>?
    ): Boolean {
        // Already in `try` section
        if (authorIdsToInsert.isNullOrEmpty() && authorIdsToRemove.isNullOrEmpty()) {
            log.debug("saveBookAuthors(): nothing to save, exiting")
            return false
        }
        var res = false
        db.beginTransaction()
        var insertStmt: SQLiteStatement? = null
        try {
            if (!authorIdsToInsert.isNullOrEmpty()) {
                insertStmt = db.compileStatement(
                    "INSERT OR IGNORE INTO ${DBContract.BookAuthorTable.TABLE_NAME} " +
                            "(${DBContract.BookAuthorTable.COLUMN_NAME_BOOK},${DBContract.BookAuthorTable.COLUMN_NAME_AUTHOR}) " +
                            "VALUES ($bookId,?)"
                )
                for (id in authorIdsToInsert) {
                    insertStmt.bindLong(1, id)
                    insertStmt.executeInsert()
                }
            }
            if (!authorIdsToRemove.isNullOrEmpty()) {
                for (id in authorIdsToRemove) {
                    db.execSQL(
                        "DELETE FROM ${DBContract.BookAuthorTable.TABLE_NAME} " +
                                "WHERE ${DBContract.BookAuthorTable.COLUMN_NAME_BOOK}=$bookId AND " +
                                "${DBContract.BookAuthorTable.COLUMN_NAME_AUTHOR}=$id"
                    )
                }
            }
            db.setTransactionSuccessful()
            res = true
        } catch (e: SQLException) {
            res = false
        } finally {
            insertStmt?.close()
            db.endTransaction()
        }
        return res
    }

    private fun saveBookKeywords(
        bookId: Long,
        keywordIdsToInsert: Collection<Long>?,
        keywordIdsToRemove: Collection<Long>?
    ): Boolean {
        // Already in `try` section
        if (keywordIdsToInsert.isNullOrEmpty() && keywordIdsToRemove.isNullOrEmpty()) {
            log.debug("saveBookKeywords(): nothing to save, exiting")
            return false
        }
        var res = false
        db.beginTransaction()
        var insertStmt: SQLiteStatement? = null
        try {
            if (!keywordIdsToInsert.isNullOrEmpty()) {
                insertStmt = db.compileStatement(
                    "INSERT OR IGNORE INTO ${DBContract.BookKeywordTable.TABLE_NAME} " +
                            "(${DBContract.BookKeywordTable.COLUMN_NAME_BOOK},${DBContract.BookKeywordTable.COLUMN_NAME_KEYWORD}) " +
                            "VALUES ($bookId,?)"
                )
                for (id in keywordIdsToInsert) {
                    insertStmt.bindLong(1, id)
                    insertStmt.executeInsert()
                }
            }
            if (!keywordIdsToRemove.isNullOrEmpty()) {
                for (id in keywordIdsToRemove) {
                    db.execSQL(
                        "DELETE FROM ${DBContract.BookKeywordTable.TABLE_NAME} " +
                                "WHERE ${DBContract.BookKeywordTable.COLUMN_NAME_BOOK}=$bookId AND " +
                                "${DBContract.BookKeywordTable.COLUMN_NAME_KEYWORD}=$id"
                    )
                }
            }
            db.setTransactionSuccessful()
            res = true
        } catch (e: SQLException) {
            res = false
        } finally {
            insertStmt?.close()
            db.endTransaction()
        }
        return res
    }

    private fun saveBookGenres(
        bookId: Long,
        genresIdsToInsert: Collection<Int>?,
        genresIdsToRemove: Collection<Int>?
    ): Boolean {
        // Already in `try` section
        if (genresIdsToInsert.isNullOrEmpty() && genresIdsToRemove.isNullOrEmpty()) {
            log.debug("saveBookGenres(): nothing to save, exiting")
            return false
        }
        var res = false
        db.beginTransaction()
        var insertStmt: SQLiteStatement? = null
        try {
            if (!genresIdsToInsert.isNullOrEmpty()) {
                insertStmt = db.compileStatement(
                    "INSERT OR IGNORE INTO ${DBContract.BookGenreTable.TABLE_NAME} " +
                            "(${DBContract.BookGenreTable.COLUMN_NAME_BOOK},${DBContract.BookGenreTable.COLUMN_NAME_GENRE}) " +
                            "VALUES ($bookId,?)"
                )
                for (id in genresIdsToInsert) {
                    insertStmt.bindLong(1, id.toLong())
                    insertStmt.executeInsert()
                }
            }
            if (!genresIdsToRemove.isNullOrEmpty()) {
                for (id in genresIdsToRemove) {
                    db.execSQL(
                        "DELETE FROM ${DBContract.BookGenreTable.TABLE_NAME} " +
                                "WHERE ${DBContract.BookGenreTable.COLUMN_NAME_BOOK}=$bookId AND " +
                                "${DBContract.BookGenreTable.COLUMN_NAME_GENRE}=$id"
                    )
                }
            }
            db.setTransactionSuccessful()
            res = true
        } catch (e: SQLException) {
            res = false
        } finally {
            insertStmt?.close()
            db.endTransaction()
        }
        return res
    }

    private fun saveBookUnsupportedGenres(
        bookId: Long,
        genresIdsToInsert: Collection<Long>?,
        genresIdsToRemove: Collection<Long>?
    ): Boolean {
        // Already in `try` section
        if (genresIdsToInsert.isNullOrEmpty() && genresIdsToRemove.isNullOrEmpty()) {
            log.debug("saveBookUnsupportedGenres(): nothing to save, exiting")
            return false
        }
        var res = false
        db.beginTransaction()
        var insertStmt: SQLiteStatement? = null
        try {
            if (!genresIdsToInsert.isNullOrEmpty()) {
                insertStmt = db.compileStatement(
                    "INSERT OR IGNORE INTO ${DBContract.BookUnsupportedGenreTable.TABLE_NAME} " +
                            "(${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_BOOK},${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_UNSUPPORTED_GENRE}) " +
                            "VALUES ($bookId,?)"
                )
                for (id in genresIdsToInsert) {
                    insertStmt.bindLong(1, id)
                    insertStmt.executeInsert()
                }
            }
            if (!genresIdsToRemove.isNullOrEmpty()) {
                for (id in genresIdsToRemove) {
                    db.execSQL(
                        "DELETE FROM ${DBContract.BookUnsupportedGenreTable.TABLE_NAME} " +
                                "WHERE ${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_BOOK}=$bookId AND " +
                                "${DBContract.BookUnsupportedGenreTable.COLUMN_NAME_UNSUPPORTED_GENRE}=$id"
                    )
                }
            }
            db.setTransactionSuccessful()
            res = true
        } catch (e: SQLException) {
            res = false
        } finally {
            insertStmt?.close()
            db.endTransaction()
        }
        return res
    }

    private fun readBookInfoFromCursor(bookInfo: BookInfo, rs: Cursor) {
        val fileInfo = bookInfo.fileInfo
        bookInfo.id = rs.getLong(0)
        val filePath = rs.getString(1)
        fileInfo.dirPath = rs.getString(2)
        fileInfo.fileName = rs.getString(3)
        bookInfo.title = rs.getString(4)
        val authorsRead = rs.getString(5)
        val keywordsRead = rs.getString(6)
        val genresRead = rs.getString(7)
        val unsupportedGenresRead = rs.getString(8)
        bookInfo.series = rs.getStringWithNull(9)
        bookInfo.seriesNumber = rs.getIntWithNull(10)
        bookInfo.format = intToDocumentFormat(rs.getInt(11))
        fileInfo.size = rs.getLongAllowNull(12, 0)
        fileInfo.arcSize = rs.getLongAllowNull(13, 0)
        fileInfo.packSize = rs.getLongAllowNull(14, 0)
        fileInfo.lastAccessTime = rs.getLong(15)
        fileInfo.modificationTime = rs.getLong(16)
        bookInfo.flags = rs.getInt(17)
        bookInfo.language = rs.getStringWithNull(18)
        bookInfo.description = rs.getStringWithNull(19)
        fileInfo.fingerprint = rs.getStringWithNull(20)
        bookInfo.domVersion = rs.getIntAllowNull(21, CREngineNGBinding.DOM_VERSION_CURRENT)
        bookInfo.blockRenderingFlags =
            rs.getIntAllowNull(22, CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB)
        bookInfo.readingStatus = intToReadingStatus(rs.getInt(23))
        bookInfo.rating = rs.getInt(24)
        bookInfo.extraData = rs.getBlobWithNull(25)

        val parts = FileInfo.splitArcName(filePath)
        if (null != parts[1]) {
            fileInfo.isArchive = true
        }
        fileInfo.pathName = parts[0]
        fileInfo.archiveName = parts[1]

        val authorsArray = authorsRead?.split("|")
        if (authorsArray?.isNotEmpty() == true) {
            val authorsFab = HashSet<String>(authorsArray.size)
            for (author in authorsArray) {
                if (author.isNotEmpty())
                    authorsFab.add(author)
            }
            bookInfo.authors = authorsFab
        } else {
            bookInfo.authors = null
        }

        val keywordsArray = keywordsRead?.split("|")
        if (keywordsArray?.isNotEmpty() == true) {
            val keywordsFab = HashSet<String>(keywordsArray.size)
            for (keyword in keywordsArray) {
                if (keyword.isNotEmpty())
                    keywordsFab.add(keyword)
            }
            bookInfo.keywords = keywordsFab
        } else {
            bookInfo.keywords = null
        }

        val genresArray = genresRead?.split("|")
        if (genresArray?.isNotEmpty() == true) {
            val genresFab = HashSet<Int>(genresArray.size)
            for (sId in genresArray) {
                val id = try {
                    sId.toInt(10)
                } catch (e: Exception) {
                    null
                }
                if (null != id)
                    genresFab.add(id)
            }
            bookInfo.genresIds = genresFab
        } else {
            bookInfo.genresIds = null
        }

        val unsupportedGenresArray = unsupportedGenresRead?.split("|")
        if (unsupportedGenresArray?.isNotEmpty() == true) {
            val genresFab = HashSet<String>(unsupportedGenresArray.size)
            for (genre in unsupportedGenresArray) {
                if (genre.isNotEmpty())
                    genresFab.add(genre)
            }
            bookInfo.unsupportedGenres = genresFab
        } else {
            bookInfo.unsupportedGenres = null
        }
    }

    private fun readBookmarkFromCursor(bookmark: Bookmark, rs: Cursor) {
        bookmark.id = rs.getLong(0)
        bookmark.type = rs.getInt(2)
        bookmark.percent = rs.getInt(3)
        bookmark.timeStamp = rs.getLong(4)
        bookmark.startPos = rs.getString(5)
        bookmark.endPos = rs.getString(6)
        bookmark.titleText = rs.getString(7)
        bookmark.posText = rs.getString(8)
        bookmark.setCommentText(rs.getString(9))
        bookmark.readingTime = rs.getLong(10)
    }

    init {
        loadAuthorsHandbook(mAuthorsHandbook)
        loadSeriesHandbook(mSeriesHandbook)
        loadKeywordsHandbook(mKeywordsHandbook)
        loadDirectoriesHandbook(mDirectoriesHandbook)
        loadUnsupportedGenresHandbook(mUnsupportedGenresHandbook)
    }

    companion object {
        private val log = SRLog.create("dbwrapper")

        private fun documentFormatToInt(format: DocumentFormat): Int {
            return format.ordinal
        }

        private fun intToDocumentFormat(value: Int): DocumentFormat {
            return DocumentFormat.byId(value)
        }

        private fun readingStatusToInt(status: BookInfo.ReadingStatus): Int {
            return status.ordinal
        }

        private fun intToReadingStatus(value: Int): BookInfo.ReadingStatus {
            return BookInfo.ReadingStatus.byOrdinal(value)
        }
    }
}
