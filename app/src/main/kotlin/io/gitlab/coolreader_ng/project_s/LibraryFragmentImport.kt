/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.FragmentTransaction
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.library.LibraryProcessingAccessor
import io.gitlab.coolreader_ng.project_s.library.LibraryProcessingBinder
import java.io.File

class LibraryFragmentImport(
    dbServiceAccessor: DBServiceAccessor? = null,
    bookCoverManager: BookCoverManager? = null,
    settings: SRProperties? = null
) :
    LibraryFragmentBase(dbServiceAccessor, bookCoverManager, settings), AbleBackwards {

    private var mODTFolderLinkResultLauncher: ActivityResultLauncher<Intent>? = null
    private var mODTFolderCopyResultLauncher: ActivityResultLauncher<Intent>? = null
    private var mODTFileCopyResultLauncher: ActivityResultLauncher<Intent>? = null
    private var mODTImportFontsResultLauncher: ActivityResultLauncher<Intent>? = null
    private lateinit var mBtnRequestPerms: Button
    private lateinit var mTvPermissionStatus: TextView
    private lateinit var mTvIntBooksFound: TextView
    private lateinit var mTvIntFontsFound: TextView
    private lateinit var mStoragePermsResultLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var mAllFilesPermsResultLauncher: ActivityResultLauncher<Intent>
    private lateinit var mPostNotificationResultLauncher: ActivityResultLauncher<String>
    private var mLibraryProcessingAccessor: LibraryProcessingAccessor? = null

    override fun onUpdateViewImpl() {
        if (BuildConfig.ENABLE_INT_FM) {
            if (Utils.checkStoragePermissions(requireContext())) {
                mTvPermissionStatus.setText(R.string.permissions_granted)
                mBtnRequestPerms.isEnabled = false
            } else {
                mTvPermissionStatus.setText(R.string.permissions_not_granted)
                mBtnRequestPerms.isEnabled = true
            }
        }
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                // get from the library service the number of book files in the app private folder
                binder.enumAppStorage(
                    LibraryProcessingBinder.AppStorageType.AppLibrary,
                    object : LibraryProcessingBinder.EnumStorageResultListener {
                        override fun onResult(data: Collection<String>) {
                            mTvIntBooksFound.text =
                                getString(R.string.___files_installed_, data.size)
                        }
                    }, Handler(Looper.getMainLooper())
                )
                // get from the library service the number of font files in the app private folder
                binder.enumAppStorage(
                    LibraryProcessingBinder.AppStorageType.AppFonts,
                    object : LibraryProcessingBinder.EnumStorageResultListener {
                        override fun onResult(data: Collection<String>) {
                            mTvIntFontsFound.text =
                                getString(R.string.___files_installed_, data.size)
                        }
                    }, Handler(Looper.getMainLooper())
                )
            }
        })
    }

    override fun onHideFragment() {
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.fragmentView)
        val processed = if (fragment is AbleBackwards) fragment.onBackPressed() else false
        if (processed)
            return true
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
            // Consume event
            return true
        }
        // We are saying that this event should be handled in the next chain item
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLibraryProcessingAccessor = LibraryProcessingAccessor(requireContext())
        mLibraryProcessingAccessor?.bind()

        // Register a callback for request storage permission result
        mStoragePermsResultLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                // external storage read & write permissions
                val haveReadPerm =
                    it[Manifest.permission.READ_EXTERNAL_STORAGE]
                        ?: if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            PackageManager.PERMISSION_GRANTED == requireActivity().checkSelfPermission(
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        else
                            true    // On API less than 23, this permission is always granted
                val haveWritePerm =
                    it[Manifest.permission.WRITE_EXTERNAL_STORAGE]
                        ?: if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            PackageManager.PERMISSION_GRANTED == requireActivity().checkSelfPermission(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                        else
                            true    // On API less than 23, this permission is always granted
                log.debug("onActivityResult(): haveReadPerm=$haveReadPerm, haveWritePerm=$haveWritePerm")
                if (haveReadPerm && haveWritePerm) {
                    log.info("read&write to storage permissions GRANTED")
                    mTvPermissionStatus.setText(R.string.permissions_granted)
                    mBtnRequestPerms.isEnabled = false
                }
                if (!haveReadPerm) {
                    log.warn("read storage permission NOT granted, disabling internal FM!")
                    mTvPermissionStatus.setText(R.string.permissions_not_granted)
                    mBtnRequestPerms.isEnabled = true
                    val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                    dialog.showDialog(requireContext())
                }
            }
        // Register a callback for request all files permissions result (API30+)
        mAllFilesPermsResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                val storagePerm = Utils.checkStoragePermissions(requireContext())
                log.debug("activity ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION returns; storagePerm=$storagePerm")
                if (storagePerm) {
                    log.info("All files access permissions GRANTED")
                    mTvPermissionStatus.setText(R.string.permissions_granted)
                    mBtnRequestPerms.isEnabled = false
                } else {
                    log.warn("All files access permission NOT granted, disabling internal FM!")
                    mTvPermissionStatus.setText(R.string.permissions_not_granted)
                    mBtnRequestPerms.isEnabled = true
                    val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                    dialog.showDialog(requireContext())
                }
            }
        // Register a callback for request post notification result
        mPostNotificationResultLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
                log.info("post notification request permission result: $result")
            }

        // Register a callback for request folder access result (API19+)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // (Copy file to library)
            mODTFileCopyResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (Activity.RESULT_OK == it.resultCode) {
                        val uri = it.data?.data
                        log.debug("activity ACTION_OPEN_DOCUMENT returns; file uri: $uri")
                        uri?.let { fileUri -> fileCopyToLibrary(fileUri) }
                    }
                }
        }

        // Register a callback for request folder access result (API21+)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // (Copy folder to library)
            mODTFolderCopyResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (Activity.RESULT_OK == it.resultCode) {
                        val uri = it.data?.data
                        log.debug("activity ACTION_OPEN_DOCUMENT_TREE returns; folder uri: $uri")
                        uri?.let { folderUri -> folderCopyToLibrary(folderUri) }
                    }
                }
            // (Link folder to library)
            mODTFolderLinkResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (Activity.RESULT_OK == it.resultCode) {
                        val uri = it.data?.data
                        log.debug("activity ACTION_OPEN_DOCUMENT_TREE returns; folder uri: $uri")
                        uri?.let { libUri ->
                            // Parse folder uri (convert to absolute path)
                            val treePath = libUri.path
                            val absolutePath = convertTreePathToAbsolutePath(treePath)
                            if (null != absolutePath) {
                                log.debug("Selected library path: $absolutePath")
                                // "take" the persistable URI permission grant
                                val contentResolver =
                                    requireActivity().applicationContext.contentResolver
                                val takeFlags =
                                    Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                                contentResolver.takePersistableUriPermission(libUri, takeFlags)
                                folderLinkToLibrary(absolutePath)
                                // TODO: save this folder to library folders
                            } else {
                                log.error("Failed to convert document tree path to absolute folder path")
                                val message = requireContext().getString(
                                    R.string.failed_to_convert_document_tree_path_to_absolute_folder_path_,
                                    treePath
                                )
                                val dialog = MessageDialog(R.string.error)
                                dialog.setMessage(message)
                                dialog.setButton(MessageDialog.StandardButtons.Ok)
                                dialog.showDialog(requireContext())
                            }
                        }
                    }
                }
            // (Copy fonts)
            mODTImportFontsResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (Activity.RESULT_OK == it.resultCode) {
                        val uri = it.data?.data
                        log.debug("activity ACTION_OPEN_DOCUMENT_TREE returns; folder uri: $uri")
                        uri?.let { folderUri -> importFontsToPrivateDir(folderUri) }
                    }
                }
        }
    }

    override fun onDestroy() {
        mLibraryProcessingAccessor?.unbind()
        super.onDestroy()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.library_import, container, false)
    }

    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)

        val panel = contentView.findViewById<View>(R.id.panel)
        val btnFolderCopy = contentView.findViewById<Button>(R.id.btnFolderCopy)
        mTvIntBooksFound = contentView.findViewById(R.id.tvIntBooksFound)
        val btnFileCopy = contentView.findViewById<Button>(R.id.btnFileCopy)
        val fileCopyBlock = contentView.findViewById<ViewGroup>(R.id.fileCopyBlock)
        val folderLinkBlock = contentView.findViewById<ViewGroup>(R.id.folderLinkBlock)
        val permissionsBlock = contentView.findViewById<ViewGroup>(R.id.permissionsBlock)
        val btnFolderLink = contentView.findViewById<Button>(R.id.btnFolderLink)
        mBtnRequestPerms = contentView.findViewById(R.id.btnRequestPerms)
        mTvPermissionStatus = contentView.findViewById(R.id.tvPermissionStatus)
        mTvIntFontsFound = contentView.findViewById(R.id.tvIntFontsFound)
        val btnImportFonts = contentView.findViewById<Button>(R.id.btnImportFonts)

        ViewCompat.setOnApplyWindowInsetsListener(panel) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(left = insets.left, top = insets.top, right = insets.right)
            // Don't consume insets (WindowInsetsCompat.CONSUMED)
            //  since we use child fragments
            windowInsets
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            btnFileCopy.setOnClickListener {
                mODTFileCopyResultLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    // TODO: add all supported mime types
                    type = "*/*"
                    addCategory(Intent.CATEGORY_OPENABLE)
                })
            }
        } else {
            fileCopyBlock.visibility = View.GONE
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnFolderCopy.setOnClickListener {
                mODTFolderCopyResultLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
            }
        } else {
            btnFolderCopy.setOnClickListener {
                val dialog = MessageDialog(R.string.data_input)
                    .setInputTextMode(R.string.please_enter_the_path_to_the_book_directory)
                val sdCardPath = Environment.getExternalStorageDirectory().path
                dialog.setInputHelperText(R.string.full_path_to_the_book_directory)
                    .setInputPlaceholder("${sdCardPath}/Books")
                dialog.setButton(MessageDialog.StandardButtons.Cancel)
                dialog.setButton(MessageDialog.StandardButtons.Ok) {
                    val bookDir = dialog.inputText
                    if (!bookDir.isNullOrEmpty()) {
                        log.debug("Selected folder path: $bookDir")
                        folderCopyToLibrary(Uri.fromFile(File(bookDir)))
                    }
                }
                dialog.showDialog(requireContext())
            }
        }
        if (BuildConfig.ENABLE_INT_FM) {
            folderLinkBlock.visibility = View.VISIBLE
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                // API < 23 already have storage permissions
                permissionsBlock.visibility = View.GONE
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnFolderLink.setOnClickListener {
                    mODTFolderLinkResultLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
                }
            } else {
                btnFolderLink.setOnClickListener {
                    val dialog = MessageDialog(R.string.data_input)
                        .setInputTextMode(R.string.please_enter_the_path_to_the_book_directory)
                    val sdCardPath = Environment.getExternalStorageDirectory().path
                    dialog.setInputHelperText(R.string.full_path_to_the_book_directory)
                        .setInputPlaceholder("${sdCardPath}/Books")
                    dialog.setButton(MessageDialog.StandardButtons.Cancel)
                    dialog.setButton(MessageDialog.StandardButtons.Ok) {
                        val bookDir = dialog.inputText
                        if (!bookDir.isNullOrEmpty()) {
                            log.debug("Selected library path: $bookDir")
                            folderLinkToLibrary(bookDir)
                            // TODO: save this folder to library folders
                        }
                    }
                    dialog.showDialog(requireContext())
                }
            }

            mBtnRequestPerms.setOnClickListener {
                requestStoragePermissions()
            }
        } else {
            folderLinkBlock.visibility = View.GONE
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnImportFonts.setOnClickListener {
                mODTImportFontsResultLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
            }
        } else {
            btnImportFonts.setOnClickListener {
                val dialog = MessageDialog(R.string.data_input)
                    .setInputTextMode(R.string.please_enter_the_path_to_the_book_directory)
                val sdCardPath = Environment.getExternalStorageDirectory().path
                dialog.setInputHelperText(R.string.full_path_to_the_font_directory)
                    .setInputPlaceholder("${sdCardPath}/Fonts")
                dialog.setButton(MessageDialog.StandardButtons.Cancel)
                dialog.setButton(MessageDialog.StandardButtons.Ok) {
                    val fontsDir = dialog.inputText
                    if (!fontsDir.isNullOrEmpty()) {
                        log.debug("Selected fonts path: $fontsDir")
                        importFontsToPrivateDir(Uri.fromFile(File(fontsDir)))
                    }
                }
                dialog.showDialog(requireContext())
            }
        }
    }

    private fun convertTreePathToAbsolutePath(treePath: String?): String? {
        // NOTE: This code is written based on guesswork and assumptions,
        //  but not on the basis of official documentation.
        // Therefore, unfortunately, there is absolutely no certainty that
        //  it will work in future versions of Android.
        if (treePath?.startsWith("/tree/") == true) {
            val path = treePath.substring(6)
            if (path.isNotEmpty()) {
                val colonPos = path.indexOf(':')
                if (colonPos > 0) {
                    val shortcut = path.substring(0..<colonPos)
                    val subpath = path.substring(colonPos + 1)
                    val shortcutPath = StorageEnumerator.byShortcut(shortcut)
                    if (!shortcutPath.isNullOrEmpty()) {
                        val dir = File(shortcutPath, subpath)
                        if (dir.exists() && dir.isDirectory && dir.canRead())
                            return dir.absolutePath
                    }
                }
            }
        }
        return null
    }

    private fun requestStoragePermissions() {
        // Check or request permission for external/shared storage
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // before API 23 (before Android 6.0)
            // already allowed after application installation
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            // API 23 (Android 6.0) - API 29 (Android 10)
            val readExtStoragePermissionCheck =
                requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            val writeExtStoragePermissionCheck =
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val needPerms: ArrayList<String> = ArrayList()
            if (PackageManager.PERMISSION_GRANTED != readExtStoragePermissionCheck) {
                needPerms.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            } else {
                log.info("READ_EXTERNAL_STORAGE permission already granted.")
            }
            if (PackageManager.PERMISSION_GRANTED != writeExtStoragePermissionCheck) {
                needPerms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } else {
                log.info("WRITE_EXTERNAL_STORAGE permission already granted.")
            }
            if (needPerms.isNotEmpty()) {
                log.info("Some permissions DENIED, requesting from user these permissions: $needPerms")
                // request permission from user
                mStoragePermsResultLauncher.launch(needPerms.toTypedArray())
            }
        } else {
            // API 30+ (Android 11+)
            try {
                // https://developer.android.com/training/data-storage/manage-all-files
                mAllFilesPermsResultLauncher.launch(
                    Intent(
                        Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                        Uri.parse("package:${requireActivity().packageName}")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                log.error("activity (MANAGE_ALL_FILES) not found", e)
                val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                dialog.showDialog(requireContext())
            }
        }
    }

    private fun requestPostNotificationPermissions() {
        // Check or request permission to post notification
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            // before API 33 (before Android 13)
            // already allowed after application installation
        } else {
            // API 33+ (Android 13+)
            val postNotificationsPermissionCheck =
                requireActivity().checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS)
            var needPerm = ""
            if (PackageManager.PERMISSION_GRANTED != postNotificationsPermissionCheck) {
                needPerm = Manifest.permission.POST_NOTIFICATIONS
            } else {
                log.info("POST_NOTIFICATIONS permission already granted.")
            }
            if (needPerm.isNotEmpty()) {
                log.info("Some permissions DENIED, requesting from user these permissions: $needPerm")
                // request permission from user
                mPostNotificationResultLauncher.launch(needPerm)
            }
        }
    }

    private fun ensurePostNotificationsAllowed() {
        if (!Utils.checkPostNotificationPermissions(requireContext())) {
            log.debug("No permission to post notifications")
            log.debug("requesting from user...")
            // TODO: Display an explanatory message to the user
            requestPostNotificationPermissions()
        }
    }

    private fun folderLinkToLibrary(folderPath: String) {
        ensurePostNotificationsAllowed()
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                binder.indexFolder(folderPath)
            }
        })
        val transaction = childFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.replace(R.id.fragmentView, LibraryImportProgressFragment())
            .addToBackStack(null)
        transaction.commit()
    }

    private fun folderCopyToLibrary(folderUri: Uri) {
        ensurePostNotificationsAllowed()
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                binder.copyFolder(folderUri)
            }
        })
        val transaction = childFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.replace(R.id.fragmentView, LibraryImportProgressFragment())
            .addToBackStack(null)
        transaction.commit()
    }

    private fun fileCopyToLibrary(fileUri: Uri) {
        ensurePostNotificationsAllowed()
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                binder.copyFile(fileUri)
            }
        })
        val transaction = childFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.replace(R.id.fragmentView, LibraryImportProgressFragment())
            .addToBackStack(null)
        transaction.commit()
    }

    private fun importFontsToPrivateDir(folderUri: Uri) {
        ensurePostNotificationsAllowed()
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                binder.importFonts(folderUri)
            }
        })
        val transaction = childFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.replace(R.id.fragmentView, LibraryImportProgressFragment())
            .addToBackStack(null)
        transaction.commit()
    }

    companion object {
        private val log = SRLog.create("library.import")
    }
}
