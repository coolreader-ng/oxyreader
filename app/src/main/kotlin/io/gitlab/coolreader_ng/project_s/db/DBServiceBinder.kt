/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.db

import android.os.Binder
import android.os.Handler
import android.os.Looper
import io.gitlab.coolreader_ng.project_s.BookInfo
import io.gitlab.coolreader_ng.project_s.FileInfo


/**
 * Class used for the client Binder.  Because we know this service always
 * runs in the same process as its clients, we don't need to deal with IPC.
 *
 * Most of the functions in this class use callbacks to return a result.
 * We use the caller's thread handle for this, so the callback is executed on the caller's thread.
 */
class DBServiceBinder(val service: DBService) : Binder() {

    fun loadBookInfo(fileInfo: FileInfo, callback: DBService.BookInfoLoadingCallback) {
        service.loadBookInfo(FileInfo(fileInfo), callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Save book information
     * @param bookInfo book information to save
     * @param callback callback to run at the end of the save operation
     *
     * If the callback is not null, the callback will be called even if no books are saved.
     */
    fun saveBookInfo(bookInfo: BookInfo, callback: DBService.BooleanResultCallback? = null) {
        service.saveBookInfo(BookInfo(bookInfo), callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Saving information about multiple books
     * @param books collection of the book information
     * @param callback callback to run at the end of the save operation
     *
     * If the callback is not null, the callback will be called even if no books are saved.
     */
    fun saveMultipleBookInfo(
        books: Collection<BookInfo>,
        callback: DBService.BooksOperationCallback? = null
    ) {
        service.saveMultipleBookInfo(books, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Find books by fingerprint or by book metadata (if fingerprint not specified)
     * @param bookInfo book metadata
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    fun findBooks(bookInfo: BookInfo, callback: DBService.BookSearchCallback) {
        service.findBooks(bookInfo, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Find book by path
     * @param path full path to book
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    fun findBooksByPathName(path: String, callback: DBService.BookSearchCallback) {
        service.findBooksByPathName(path, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Find book by fingerprint
     * @param fingerprint fingerprint to search
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    fun findBooksByFingerprint(fingerprint: String, callback: DBService.BookSearchCallback) {
        service.findBooksByFingerprint(fingerprint, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Get book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @param callback callback to run when cover data is loaded
     */
    fun getBookCoverData(bookInfo: BookInfo, callback: DBService.BookCoverDataResultCallback) {
        service.getBookCoverData(bookInfo, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Save book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @param data book cover image data to save
     * @param callback callback to run when cover data is saved
     */
    internal fun saveBookCoverData(
        bookInfo: BookInfo,
        data: ByteArray?,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.saveBookCoverData(bookInfo, data, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Saving book cover image data for multiple books
     * @param covers image data map for multiple books
     * @param callback callback to run when cover data is saved
     *
     * If the callback is not null, the callback will be called even if no book covers are saved.
     */
    internal fun saveMultipleBookCoverData(
        covers: Map<BookInfo, ByteArray?>,
        callback: DBService.BooksOperationCallback? = null
    ) {
        service.saveMultipleBookCoverData(covers, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Load recent books (with bookmarks)
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no recent books are found. In this case, the `books` callback argument will be empty.
     */
    fun loadRecentBooks(maxCount: Int, callback: DBService.BooksLoadingCallback) {
        service.loadRecentBooks(maxCount, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove book from recent list
     * @param bookInfo book info to remove from recent list
     * @param callback callback to run at the end of remove process
     *
     * The callback will be called regardless of result.
     */
    fun removeBooksFromRecent(bookInfo: BookInfo, callback: DBService.BooleanResultCallback) {
        service.removeBooksFromRecent(bookInfo, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove multiple books from recent list
     * @param books books collection to be deleted
     * @param callback callback to run at the end of operation
     */
    fun removeMultipleBooksFromRecent(
        books: Collection<BookInfo>,
        callback: DBService.BooksOperationCallback? = null
    ) {
        service.removeMultipleBooksFromRecent(books, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Load filtered book list (with bookmarks)
     * @param sorting book sorting attribute
     * @param filters book filters collection
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be empty.
     */
    fun loadBookList(
        sorting: DBService.BooksSorting,
        filters: DBService.BookFilters,
        callback: DBService.BooksLoadingCallback
    ) {
        service.loadBookList(sorting, filters, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove book record from database
     * @param bookInfo book to be deleted
     * @param callback callback to run at the end of operation
     */
    fun removeBookInfo(bookInfo: BookInfo, callback: DBService.BooleanResultCallback? = null) {
        service.removeBookInfo(bookInfo, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove multiple book records from database
     * @param books books collection to be deleted
     * @param callback callback to run at the end of operation
     */
    fun removeMultipleBookInfo(
        books: Collection<BookInfo>,
        callback: DBService.BooksOperationCallback? = null
    ) {
        service.removeMultipleBookInfo(books, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Gets the specified handbook
     * @param handbookType type of the handbook
     * @param callback callback to return loaded data
     */
    fun getHandbook(
        handbookType: DBService.HandbookType,
        callback: DBService.HandbookLoadingCallback
    ) {
        service.getHandbook(handbookType, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Load book search history
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called even if no history are found. In this case, the `history` callback argument will be null.
     */
    fun loadBookSearchHistory(maxCount: Int, callback: DBService.SearchHistoryLoadingCallback) {
        service.loadBookSearchHistory(maxCount, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Save new book search history item
     * @param searchItem book search history item
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun saveBookSearchHistoryItem(
        searchItem: DBService.SearchHistoryItem,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.saveBookSearchHistoryItem(searchItem, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove book search history item
     * @param searchItem book search history item
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun removeBookSearchHistoryItem(
        searchItem: DBService.SearchHistoryItem,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.removeBookSearchHistoryItem(searchItem, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Clear book search history
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun clearBookSearchHistoryItem(callback: DBService.BooleanResultCallback? = null) {
        service.clearBookSearchHistoryItem(callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Search books by search query
     * @param searchQuery search query
     * @param sorting book sorting attribute
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be empty.
     * Search is carried out only by author, title and series.
     */
    fun searchBooks(
        searchQuery: String,
        sorting: DBService.BooksSorting,
        callback: DBService.BookSearchCallback
    ) {
        service.searchBooks(searchQuery, sorting, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Load text search history
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called even if no history are found. In this case, the `history` callback argument will be null.
     */
    fun loadTextSearchHistory(maxCount: Int, callback: DBService.SearchHistoryLoadingCallback) {
        service.loadTextSearchHistory(maxCount, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Save new text search history item
     * @param searchItem text search history item
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun saveTextSearchHistoryItem(
        searchItem: DBService.SearchHistoryItem,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.saveTextSearchHistoryItem(searchItem, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Remove text search history item
     * @param searchItem text search history item
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun removeTextSearchHistoryItem(
        searchItem: DBService.SearchHistoryItem,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.removeTextSearchHistoryItem(searchItem, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Clear text search history
     * @param callback callback to run at the end of the loading
     *
     * The callback will be called on any result.
     */
    fun clearTextSearchHistoryItem(callback: DBService.BooleanResultCallback? = null) {
        service.clearTextSearchHistoryItem(callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Gets the currently read book
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if no books are found. In this case, the `result` callback argument will be empty.
     */
    fun getCurrentBook(callback: DBService.StringResultCallback) {
        service.getCurrentBook(callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Sets the currently read book
     * @param filePath full file path to book file
     * @param timestamp file open event timestamp
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if error occurred. In this case, the `result` callback argument will be false.
     */
    fun setCurrentBook(
        filePath: String,
        timestamp: Long,
        callback: DBService.BooleanResultCallback? = null
    ) {
        service.setCurrentBook(filePath, timestamp, callback, Handler(Looper.myLooper()!!))
    }

    /**
     * Unset the currently read book
     * @param callback callback to run at the end of the search
     *
     * The callback will be called even if error occurred. In this case, the `result` callback argument will be false.
     */
    fun unsetCurrentBook(callback: DBService.BooleanResultCallback? = null) {
        service.unsetCurrentBook(callback, Handler(Looper.myLooper()!!))
    }
}
