/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import java.io.File

object LanguagesRegistry {
    private val m_langs: Array<LanguageItem>
    private val m_dicts = ArrayList<HyphenationDict>()
    private val m_builtins: Array<HyphenationDict>

    val LANG_UND = LanguageItem("Undetermined", "und")
    val LANG_BG = LanguageItem("Bulgarian", "bg")
    val LANG_BN = LanguageItem("Bengali", "bn")
    val LANG_CS = LanguageItem("Czech", "cs")
    val LANG_DA = LanguageItem("Danish", "da")
    val LANG_DE = LanguageItem("German", "de")
    val LANG_EL = LanguageItem("Greek", "el")
    val LANG_EN_GB = LanguageItem("English GB", "en-GB")
    val LANG_EN_US = LanguageItem("English US", "en-US")
    val LANG_ES = LanguageItem("Spanish", "es")
    val LANG_FI = LanguageItem("Finnish", "fi")
    val LANG_FR = LanguageItem("French", "fr")
    val LANG_GRC = LanguageItem("Ancient Greek", "grc")
    val LANG_GU = LanguageItem("Gujarati", "gu")
    val LANG_HU = LanguageItem("Hungarian", "hu")
    val LANG_IT = LanguageItem("Italian", "it")
    val LANG_MR = LanguageItem("Marathi", "mr")
    val LANG_NL = LanguageItem("Dutch", "nl")
    val LANG_PA = LanguageItem("Punjabi", "pa")
    val LANG_PL = LanguageItem("Polish", "pl")
    val LANG_PT = LanguageItem("Portuguese", "pt")
    val LANG_RU = LanguageItem("Russian", "ru")
    val LANG_TA = LanguageItem("Tamil", "ta")
    val LANG_TE = LanguageItem("Telugu", "te")
    val LANG_UK = LanguageItem("Ukrainian", "uk")
    val LANG_ZH_LATN = LanguageItem("Mandarin Chinese, pinyin", "zh-latn")

    val HYPH_NONE = HyphenationDict("@none", HyphenationDict.HYPH_NONE, 0, "[None]", LANG_UND)
    val HYPH_ALGORITHM =
        HyphenationDict("@algorithm", HyphenationDict.HYPH_ALGO, 0, "[Algorythmic]", LANG_UND)
    val HYPH_SOFTHYPHENS = HyphenationDict(
        "@softhyphens",
        HyphenationDict.HYPH_SOFTHYPHENS,
        0,
        "[SoftHyphens]",
        LANG_UND
    )

    val HYPH_BG =
        HyphenationDict("BG", HyphenationDict.HYPH_DICT, R.raw.hyph_bg, "Bulgarian", LANG_BG)
    val HYPH_BN =
        HyphenationDict("BN", HyphenationDict.HYPH_DICT, R.raw.hyph_bn, "Bengali", LANG_BN)
    val HYPH_CS = HyphenationDict("CS", HyphenationDict.HYPH_DICT, R.raw.hyph_cs, "Czech", LANG_CS)
    val HYPH_DA = HyphenationDict("DA", HyphenationDict.HYPH_DICT, R.raw.hyph_da, "Danish", LANG_DA)
    val HYPH_DE =
        HyphenationDict("DE_1996", HyphenationDict.HYPH_DICT, R.raw.hyph_de_1996, "German", LANG_DE)
    val HYPH_EL = HyphenationDict(
        "EL_MONOTON",
        HyphenationDict.HYPH_DICT,
        R.raw.hyph_el_monoton,
        "Greek",
        LANG_EL
    )
    val HYPH_EN_GB = HyphenationDict(
        "EN_GB",
        HyphenationDict.HYPH_DICT,
        R.raw.hyph_en_gb,
        "English GB",
        LANG_EN_GB
    )
    val HYPH_EN_US = HyphenationDict(
        "EN_US",
        HyphenationDict.HYPH_DICT,
        R.raw.hyph_en_us,
        "English US",
        LANG_EN_US
    )
    val HYPH_ES =
        HyphenationDict("ES", HyphenationDict.HYPH_DICT, R.raw.hyph_es, "Spanish", LANG_ES)
    val HYPH_FI =
        HyphenationDict("FI", HyphenationDict.HYPH_DICT, R.raw.hyph_fi, "Finnish", LANG_FI)
    val HYPH_FR = HyphenationDict("FR", HyphenationDict.HYPH_DICT, R.raw.hyph_fr, "French", LANG_FR)
    val HYPH_GRC =
        HyphenationDict("GRC", HyphenationDict.HYPH_DICT, R.raw.hyph_grc, "Ancient Greek", LANG_GRC)
    val HYPH_GU =
        HyphenationDict("GU", HyphenationDict.HYPH_DICT, R.raw.hyph_gu, "Gujarati", LANG_GU)
    val HYPH_HU =
        HyphenationDict("HU", HyphenationDict.HYPH_DICT, R.raw.hyph_hu, "Hungarian", LANG_HU)
    val HYPH_IT =
        HyphenationDict("IT", HyphenationDict.HYPH_DICT, R.raw.hyph_it, "Italian", LANG_IT)
    val HYPH_MR =
        HyphenationDict("MR", HyphenationDict.HYPH_DICT, R.raw.hyph_mr, "Marathi", LANG_MR)
    val HYPH_NL = HyphenationDict("NL", HyphenationDict.HYPH_DICT, R.raw.hyph_nl, "Dutch", LANG_NL)
    val HYPH_PA =
        HyphenationDict("PA", HyphenationDict.HYPH_DICT, R.raw.hyph_pa, "Punjabi", LANG_PA)
    val HYPH_PL = HyphenationDict("PL", HyphenationDict.HYPH_DICT, R.raw.hyph_pl, "Polish", LANG_PL)
    val HYPH_PT =
        HyphenationDict("PT", HyphenationDict.HYPH_DICT, R.raw.hyph_pt, "Portuguese", LANG_PT)
    val HYPH_RU =
        HyphenationDict("RU_RU", HyphenationDict.HYPH_DICT, R.raw.hyph_ru_ru, "Russian", LANG_RU)
    val HYPH_RU_RU_EN_US =
        HyphenationDict(
            "RU_RU_EN_US",
            HyphenationDict.HYPH_DICT,
            R.raw.hyph_ru_ru_en_us,
            "Russian + US English",
            LANG_RU,
            LANG_EN_US
        )
    val HYPH_TA = HyphenationDict("TA", HyphenationDict.HYPH_DICT, R.raw.hyph_ta, "Tamil", LANG_TA)
    val HYPH_TE = HyphenationDict("TE", HyphenationDict.HYPH_DICT, R.raw.hyph_te, "Telugu", LANG_TE)
    val HYPH_UK =
        HyphenationDict("UK", HyphenationDict.HYPH_DICT, R.raw.hyph_uk, "Ukrainian", LANG_UK)
    val HYPH_ZH_LATN = HyphenationDict(
        "ZH_LATN_PINYIN",
        HyphenationDict.HYPH_DICT,
        R.raw.hyph_zh_latn_pinyin,
        "Mandarin Chinese, pinyin",
        LANG_ZH_LATN
    )

    private fun byLanguage(langTag: String?): HyphenationDict {
        if (langTag != null && langTag.trim { it <= ' ' } != "") {
            for (dict in m_dicts) {
                for (lang in dict.languages) {
                    if (lang.langTag == langTag)
                        return dict
                }
            }
        }
        return HYPH_NONE
    }

    fun byCode(code: String?): HyphenationDict {
        for (dict in m_dicts)
            if (dict.code == code)
                return dict
        return HYPH_NONE
    }

    fun byName(name: String?): HyphenationDict {
        for (dict in m_dicts)
            if (dict.name == name)
                return dict
        return HYPH_NONE
    }

    fun byFileName(fileName: String?): HyphenationDict {
        for (dict in m_dicts)
            dict.file?.let {
                if (it.name == fileName)
                    return dict
            }
        return HYPH_NONE
    }

    fun languages(): Array<LanguageItem> {
        return m_langs
    }

    fun dictionaries(): Array<HyphenationDict> {
        return m_dicts.toTypedArray()
    }

    fun addFromDir(dir: File): Int {
        var count = 0
        val files = dir.listFiles()
        if (null != files) {
            for (file in files) {
                if (file.isFile && file.canRead()) {
                    val fn = file.name
                    if (fn.lowercase().endsWith(".pdb") || fn.lowercase().endsWith(".pattern")) {
                        if (byFileName(fn) === HYPH_NONE) {
                            m_dicts.add(HyphenationDict(file))
                            count++
                        }
                    }
                }
            }
        }
        return count
    }

    init {
        m_langs = arrayOf(
            LANG_BG,
            LANG_BN,
            LANG_CS,
            LANG_DA,
            LANG_DE,
            LANG_EL,
            LANG_EN_GB,
            LANG_EN_US,
            LANG_ES,
            LANG_FI,
            LANG_FR,
            LANG_GRC,
            LANG_GU,
            LANG_HU,
            LANG_IT,
            LANG_MR,
            LANG_NL,
            LANG_PA,
            LANG_PL,
            LANG_PT,
            LANG_RU,
            LANG_TA,
            LANG_TE,
            LANG_UK,
            LANG_ZH_LATN
        )
        m_builtins = arrayOf(
            HYPH_NONE,
            HYPH_ALGORITHM,
            HYPH_SOFTHYPHENS,
            HYPH_BG,
            HYPH_BN,
            HYPH_CS,
            HYPH_DA,
            HYPH_DE,
            HYPH_EL,
            HYPH_EN_GB,
            HYPH_EN_US,
            HYPH_ES,
            HYPH_FI,
            HYPH_FR,
            HYPH_GRC,
            HYPH_GU,
            HYPH_HU,
            HYPH_IT,
            HYPH_MR,
            HYPH_NL,
            HYPH_PA,
            HYPH_PL,
            HYPH_PT,
            HYPH_RU,
            HYPH_RU_RU_EN_US,
            HYPH_TA,
            HYPH_TE,
            HYPH_UK,
            HYPH_ZH_LATN
        )
        m_dicts.addAll(m_builtins)
    }
}
