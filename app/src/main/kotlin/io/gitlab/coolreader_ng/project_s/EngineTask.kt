/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import java.util.concurrent.atomic.AtomicBoolean

/**
 * Abstract class for implementing the engine's background task.
 */
abstract class EngineTask() {

    internal var requireWait = AtomicBoolean(false)

    internal val waitObject = Object()

    /**
     * The main code for implementing the task runs on a background thread.
     * If an error occurs during the execution of the work before exiting the function,
     * you need to throw the appropriate exception.
     * If no exception is thrown the task is considered successful.
     * Upon successful completion of the task, the done() function will be called in the GUI thread,
     * otherwise the fail() function will be called.
     */
    @Throws(Exception::class)
    abstract fun work()

    /**
     * Indicate to the task handler that this task is delegating some of its work
     * to another thread and that it needs to wait for this task to complete in a separate thread.
     * This is necessary so that the done() function is called after all the work of this task
     * has been completed, including other threads.
     * In this case, this task MUST call the setDone() function to complete its work in another thread.
     * @see setTaskDone()
     */
    protected fun setTaskRequireWait() {
        requireWait.compareAndSet(false, true)
    }

    /**
     * If this task delegates some of its work to another thread, then somewhere in the callback
     * that runs in another thread, this function must be called upon completion of work.
     * Otherwise, this task will hang and the done() function will never be called.
     * If this task is running on a single thread, there is no need to call this function in the work() function.
     * @see setTaskRequireWait()
     */
    protected fun setTaskDone() {
        requireWait.compareAndSet(true, false)
        synchronized(waitObject) {
            waitObject.notifyAll()
        }
    }

    /**
     * The code that runs if the main job is successful (no exceptions are thrown) runs on the GUI thread.
     */
    abstract fun done()

    /**
     * The code that runs when the main job fails (throws any exceptions) runs on the GUI thread.
     * If this method is called, the done() method is not called.
     *
     * @param e an exception that interrupted the main work.
     */
    abstract fun fail(e: Exception)
}
