/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.db

import android.provider.BaseColumns

object DBContract {
    /**
     * List of authors
     */
    object AuthorTable : BaseColumns {
        const val TABLE_NAME = "author"

        // Author name. First name, Last Name, etc.
        const val COLUMN_NAME_NAME = "name"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_NAME VARCHAR NOT NULL UNIQUE" +
                    ")"
        private const val SQL_CREATE_NAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_NAME}_idx ON $TABLE_NAME($COLUMN_NAME_NAME)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_NAME_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_NAME" +
                " FROM $TABLE_NAME"
    }

    /**
     * File system directories
     */
    object DirectoryTable : BaseColumns {
        const val TABLE_NAME = "directory"

        // Full path to folder
        const val COLUMN_NAME_NAME = "name"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_NAME VARCHAR NOT NULL UNIQUE" +
                    ")"
        private const val SQL_CREATE_NAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_NAME}_idx ON $TABLE_NAME($COLUMN_NAME_NAME)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_NAME_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_NAME" +
                " FROM $TABLE_NAME"
    }

    /**
     * List of book series
     */
    object SeriesTable : BaseColumns {
        const val TABLE_NAME = "series"

        // Series name
        const val COLUMN_NAME_NAME = "name"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_NAME VARCHAR NOT NULL UNIQUE" +
                    ")"
        private const val SQL_CREATE_NAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_NAME}_idx ON $TABLE_NAME($COLUMN_NAME_NAME)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_NAME_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_NAME" +
                " FROM $TABLE_NAME"
    }

    /**
     * List of keywords
     */
    object KeywordTable : BaseColumns {
        const val TABLE_NAME = "keyword"

        // Author name. First name, Last Name, etc.
        const val COLUMN_NAME_NAME = "name"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_NAME VARCHAR NOT NULL UNIQUE" +
                    ")"
        private const val SQL_CREATE_NAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_NAME}_idx ON $TABLE_NAME($COLUMN_NAME_NAME)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_NAME_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_NAME" +
                " FROM $TABLE_NAME"
    }

    /**
     * List of unsupported genres
     */
    object UnsupportedGenreTable : BaseColumns {
        const val TABLE_NAME = "unsupported_genre"

        // Unsupported genre code
        const val COLUMN_NAME_CODE = "code"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_CODE VARCHAR NOT NULL UNIQUE" +
                    ")"
        private const val SQL_CREATE_NAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_CODE}_idx ON $TABLE_NAME($COLUMN_NAME_CODE)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_NAME_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_CODE" +
                " FROM $TABLE_NAME"
    }

    /**
     * List of books
     */
    object BookTable : BaseColumns {
        const val TABLE_NAME = "book"

        // Full path to the book file (if this file is inside an archive, then the path to the archive is added to the beginning of this path).
        const val COLUMN_NAME_FILEPATH = "filepath"

        // Foreign key to the directory where this file is located. Needed to load cached data when opening a directory in the internal file manager.
        const val COLUMN_NAME_DIRECTORY = "directory"

        // File name (with no path)
        const val COLUMN_NAME_FILENAME = "filename"

        // Book title
        const val COLUMN_NAME_TITLE = "title"

        // Foreign key to book series
        const val COLUMN_NAME_SERIES = "series"

        // Book number in the series
        const val COLUMN_NAME_SERIES_NUMBER = "series_number"

        // Book file format code. Code reference is present only in the program.
        const val COLUMN_NAME_FORMAT = "format"

        // Book file size
        const val COLUMN_NAME_FILESIZE = "filesize"

        // The size of the archive, if the book file is in an archive. NULL or 0 otherwise.
        const val COLUMN_NAME_ARCSIZE = "arcsize"

        // Compressed size of the file in archive, if the book file is in an archive. NULL or 0 otherwise.
        const val COLUMN_NAME_PACKSIZE = "packsize"

        // Last visited time in internal file manager or last book open time (timestamp)
        const val COLUMN_NAME_LAST_VISITED_TIME = "last_visited_time"

        // File modification time (timestamp)
        const val COLUMN_NAME_MODIFICATION_TIME = "modification_time"

        // Various document flags (for example, 'use document styles', 'use document fonts', etc)
        const val COLUMN_NAME_FLAGS = "flags"

        // Primary language of the book (ISO-639-2 alpha2 or alpha3 or ISO-639-3)
        const val COLUMN_NAME_LANGUAGE = "language"

        // Annotation of the book and some of its parameters in an arbitrary form.
        const val COLUMN_NAME_DESCRIPTION = "description"

        // Book fingerprint. It is necessary to search for a book opened from an unknown stream, as well as to search for duplicates
        const val COLUMN_NAME_FINGERPRINT = "fingerprint"

        // Specific DOM version for this book.
        const val COLUMN_NAME_DOMVERSION = "domVersion"

        // Specific block rendering flags for this book.
        const val COLUMN_NAME_RENDFLAGS = "rendFlags"

        // Book reading status code (None, in reading, done, planned)
        const val COLUMN_NAME_STATUS = "status"

        // Book rating code. 0 - not set, 1 - 5 - rating
        const val COLUMN_NAME_RATING = "rating"

        const val COLUMN_NAME_EXTRA = "extra"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_FILEPATH VARCHAR NOT NULL UNIQUE, " +
                    "$COLUMN_NAME_DIRECTORY INTEGER REFERENCES ${DirectoryTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_FILENAME VARCHAR NOT NULL, " +
                    "$COLUMN_NAME_TITLE VARCHAR, " +
                    "$COLUMN_NAME_SERIES INTEGER REFERENCES ${SeriesTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_SERIES_NUMBER INTEGER, " +
                    "$COLUMN_NAME_FORMAT INTEGER NOT NULL, " +
                    "$COLUMN_NAME_FILESIZE INTEGER NOT NULL, " +
                    "$COLUMN_NAME_ARCSIZE INTEGER, " +
                    "$COLUMN_NAME_PACKSIZE INTEGER, " +
                    "$COLUMN_NAME_LAST_VISITED_TIME INTEGER, " +
                    "$COLUMN_NAME_MODIFICATION_TIME INTEGER, " +
                    "$COLUMN_NAME_FLAGS INTEGER DEFAULT 0, " +
                    "$COLUMN_NAME_LANGUAGE VARCHAR, " +
                    "$COLUMN_NAME_DESCRIPTION TEXT, " +
                    "$COLUMN_NAME_FINGERPRINT VARCHAR, " +
                    "$COLUMN_NAME_DOMVERSION INTEGER, " +
                    "$COLUMN_NAME_RENDFLAGS INTEGER, " +
                    "$COLUMN_NAME_STATUS INTEGER DEFAULT 0, " +
                    "$COLUMN_NAME_RATING INTEGER DEFAULT 0, " +
                    "$COLUMN_NAME_EXTRA BLOB" +
                    ")"
        private const val SQL_CREATE_FILEPATH_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_FILEPATH}_idx ON $TABLE_NAME($COLUMN_NAME_FILEPATH)"
        private const val SQL_CREATE_FILENAME_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_FILENAME}_idx ON $TABLE_NAME($COLUMN_NAME_FILENAME)"
        private const val SQL_CREATE_TITLE_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_TITLE}_idx ON $TABLE_NAME($COLUMN_NAME_TITLE)"
        val SQL_CREATE = arrayOf(
            SQL_CREATE_TABLE,
            SQL_CREATE_FILEPATH_INDEX,
            SQL_CREATE_FILENAME_INDEX,
            SQL_CREATE_TITLE_INDEX
        )

        // query all fields except cover image data
        const val SQL_QUERY_ALL = "SELECT" +
                "  $TABLE_NAME.${BaseColumns._ID}" +
                ", $COLUMN_NAME_FILEPATH" +
                ", ${DirectoryTable.TABLE_NAME}.${DirectoryTable.COLUMN_NAME_NAME} as path" +
                ", $COLUMN_NAME_FILENAME" +
                ", $COLUMN_NAME_TITLE" +
                ", (SELECT GROUP_CONCAT(${AuthorTable.TABLE_NAME}.${AuthorTable.COLUMN_NAME_NAME},'|')" +
                "    FROM ${AuthorTable.TABLE_NAME}" +
                "    JOIN ${BookAuthorTable.TABLE_NAME} ON" +
                "         ${AuthorTable.TABLE_NAME}.${BaseColumns._ID}=${BookAuthorTable.TABLE_NAME}.${BookAuthorTable.COLUMN_NAME_AUTHOR}" +
                "    WHERE ${BookAuthorTable.TABLE_NAME}.${BookAuthorTable.COLUMN_NAME_BOOK}=$TABLE_NAME.${BaseColumns._ID})" +
                "  as authors" +
                ", (SELECT GROUP_CONCAT(${KeywordTable.TABLE_NAME}.${KeywordTable.COLUMN_NAME_NAME},'|')" +
                "    FROM ${KeywordTable.TABLE_NAME}" +
                "    JOIN ${BookKeywordTable.TABLE_NAME} ON" +
                "         ${KeywordTable.TABLE_NAME}.${BaseColumns._ID}=${BookKeywordTable.TABLE_NAME}.${BookKeywordTable.COLUMN_NAME_KEYWORD}" +
                "    WHERE ${BookKeywordTable.TABLE_NAME}.${BookKeywordTable.COLUMN_NAME_BOOK}=$TABLE_NAME.${BaseColumns._ID})" +
                "  as keywords" +
                ", (SELECT GROUP_CONCAT(${BookGenreTable.TABLE_NAME}.${BookGenreTable.COLUMN_NAME_GENRE},'|')" +
                "    FROM ${BookGenreTable.TABLE_NAME}" +
                "    WHERE ${BookGenreTable.TABLE_NAME}.${BookGenreTable.COLUMN_NAME_BOOK}=$TABLE_NAME.${BaseColumns._ID})" +
                "  as genres" +
                ", (SELECT GROUP_CONCAT(${UnsupportedGenreTable.TABLE_NAME}.${UnsupportedGenreTable.COLUMN_NAME_CODE},'|')" +
                "    FROM ${UnsupportedGenreTable.TABLE_NAME}" +
                "    JOIN ${BookUnsupportedGenreTable.TABLE_NAME} ON" +
                "         ${UnsupportedGenreTable.TABLE_NAME}.${BaseColumns._ID}=${BookUnsupportedGenreTable.TABLE_NAME}.${BookUnsupportedGenreTable.COLUMN_NAME_UNSUPPORTED_GENRE}" +
                "    WHERE ${BookUnsupportedGenreTable.TABLE_NAME}.${BookUnsupportedGenreTable.COLUMN_NAME_BOOK}=$TABLE_NAME.${BaseColumns._ID})" +
                "  as un_genres" +
                ", ${SeriesTable.TABLE_NAME}.${SeriesTable.COLUMN_NAME_NAME} as series_name" +
                ", $COLUMN_NAME_SERIES_NUMBER" +
                ", $COLUMN_NAME_FORMAT" +
                ", $COLUMN_NAME_FILESIZE" +
                ", $COLUMN_NAME_ARCSIZE" +
                ", $COLUMN_NAME_PACKSIZE" +
                ", $COLUMN_NAME_LAST_VISITED_TIME" +
                ", $COLUMN_NAME_MODIFICATION_TIME" +
                ", $COLUMN_NAME_FLAGS" +
                ", $COLUMN_NAME_LANGUAGE" +
                ", $COLUMN_NAME_DESCRIPTION" +
                ", $COLUMN_NAME_FINGERPRINT" +
                ", $COLUMN_NAME_DOMVERSION" +
                ", $COLUMN_NAME_RENDFLAGS" +
                ", $COLUMN_NAME_STATUS" +
                ", $COLUMN_NAME_RATING" +
                ", $COLUMN_NAME_EXTRA" +
                " FROM $TABLE_NAME" +
                " LEFT JOIN ${SeriesTable.TABLE_NAME} ON ${SeriesTable.TABLE_NAME}.${BaseColumns._ID}=$TABLE_NAME.$COLUMN_NAME_SERIES" +
                " LEFT JOIN ${DirectoryTable.TABLE_NAME} ON ${DirectoryTable.TABLE_NAME}.${BaseColumns._ID}=$TABLE_NAME.$COLUMN_NAME_DIRECTORY"

        // Queries to upgrade database from version 1 to 2
        val SQL_QUERY_UPGRADE_VER_1_to_2 = arrayOf(
            "ALTER TABLE $TABLE_NAME ADD COLUMN $COLUMN_NAME_STATUS INTEGER DEFAULT 0",
            "ALTER TABLE $TABLE_NAME ADD COLUMN $COLUMN_NAME_RATING INTEGER DEFAULT 0"
        )

        // Queries to upgrade database from version 5 to 6
        val SQL_QUERY_UPGRADE_VER_5_to_6 = arrayOf(
            "ALTER TABLE $TABLE_NAME ADD COLUMN $COLUMN_NAME_PACKSIZE INTEGER"
        )

        // Queries to upgrade database from version 7 to 8
        val SQL_QUERY_UPGRADE_VER_7_to_8 = arrayOf(
            "ALTER TABLE $TABLE_NAME ADD COLUMN $COLUMN_NAME_EXTRA BLOB"
        )
    }

    /**
     * Table of links between authors and books
     */
    object BookAuthorTable : BaseColumns {
        const val TABLE_NAME = "book_author"

        // Foreign key to the book
        const val COLUMN_NAME_BOOK = "book"

        // Foreign key to the author
        const val COLUMN_NAME_AUTHOR = "author"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_AUTHOR INTEGER NOT NULL  REFERENCES ${AuthorTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "UNIQUE ($COLUMN_NAME_BOOK, $COLUMN_NAME_AUTHOR)" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)
    }

    /**
     * Table of links between keywords and books
     */
    object BookKeywordTable : BaseColumns {
        const val TABLE_NAME = "book_keyword"

        // Foreign key to the book
        const val COLUMN_NAME_BOOK = "book"

        // Foreign key to the keyword
        const val COLUMN_NAME_KEYWORD = "keyword"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_KEYWORD INTEGER NOT NULL  REFERENCES ${KeywordTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "UNIQUE ($COLUMN_NAME_BOOK, $COLUMN_NAME_KEYWORD)" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)
    }

    /**
     * Table of links between genres and books
     */
    object BookGenreTable : BaseColumns {
        const val TABLE_NAME = "book_genre"

        // Foreign key to the book
        const val COLUMN_NAME_BOOK = "book"

        // Genre ID. Code reference is present only in the program
        const val COLUMN_NAME_GENRE = "genre"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_GENRE INTEGER NOT NULL, " +
                    "UNIQUE ($COLUMN_NAME_BOOK, $COLUMN_NAME_GENRE)" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)
    }

    /**
     * Table of links between unsupported genres and books
     */
    object BookUnsupportedGenreTable : BaseColumns {
        const val TABLE_NAME = "book_unsupported_genre"

        // Foreign key to the book
        const val COLUMN_NAME_BOOK = "book"

        // Foreign key to the unsupported genre
        const val COLUMN_NAME_UNSUPPORTED_GENRE = "unsupported_genre"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_UNSUPPORTED_GENRE INTEGER NOT NULL  REFERENCES ${UnsupportedGenreTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "UNIQUE ($COLUMN_NAME_BOOK, $COLUMN_NAME_UNSUPPORTED_GENRE)" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)
    }

    /**
     * List of bookmark for each book
     */
    object BookmarkTable : BaseColumns {
        const val TABLE_NAME = "bookmark"

        // The foreign key to the book this bookmark is for
        const val COLUMN_NAME_BOOK = "book"

        // Bookmark type. Code reference is present only in the program.
        const val COLUMN_NAME_TYPE = "type"

        // The percentage read for this bookmark. In hundredths of a percent.
        const val COLUMN_NAME_PERCENT = "percent"

        // Bookmark creation time (timestamp)
        const val COLUMN_NAME_TIME_STAMP = "time_stamp"

        // ldomXPointer to bookmark start
        const val COLUMN_NAME_START_POS = "start_pos"

        // ldomXPointer to bookmark end
        const val COLUMN_NAME_END_POS = "end_pos"

        // The name of the chapter in which this bookmark is located
        const val COLUMN_NAME_TITLE_TEXT = "title_text"

        // The text of the element of the book pointed to by this bookmark
        const val COLUMN_NAME_POS_TEXT = "pos_text"

        // Comment on this bookmark
        const val COLUMN_NAME_COMMENT_TEXT = "comment_text"

        // Total book reading time (milliseconds)
        const val COLUMN_NAME_READING_TIME = "reading_time"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_TYPE INTEGER NOT NULL, " +
                    "$COLUMN_NAME_PERCENT INTEGER, " +
                    "$COLUMN_NAME_TIME_STAMP INTEGER, " +
                    "$COLUMN_NAME_START_POS VARCHAR NOT NULL, " +
                    "$COLUMN_NAME_END_POS VARCHAR, " +
                    "$COLUMN_NAME_TITLE_TEXT VARCHAR, " +
                    "$COLUMN_NAME_POS_TEXT VARCHAR, " +
                    "$COLUMN_NAME_COMMENT_TEXT VARCHAR, " +
                    "$COLUMN_NAME_READING_TIME INT DEFAULT 0" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)

        const val SQL_QUERY_ALL = "SELECT " +
                "  ${BaseColumns._ID}" +
                ", $COLUMN_NAME_BOOK" +
                ", $COLUMN_NAME_TYPE" +
                ", $COLUMN_NAME_PERCENT" +
                ", $COLUMN_NAME_TIME_STAMP" +
                ", $COLUMN_NAME_START_POS" +
                ", $COLUMN_NAME_END_POS" +
                ", $COLUMN_NAME_TITLE_TEXT" +
                ", $COLUMN_NAME_POS_TEXT" +
                ", $COLUMN_NAME_COMMENT_TEXT" +
                ", $COLUMN_NAME_READING_TIME" +
                " FROM $TABLE_NAME"
    }

    /**
     * Table of book covers
     */
    object BookCoverTable : BaseColumns {
        const val TABLE_NAME = "book_cover"

        // Foreign key to the book
        const val COLUMN_NAME_BOOK = "book"

        // book cover image binary data
        const val COLUMN_NAME_COVER_DATA = "cover_data"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_BOOK INTEGER NOT NULL UNIQUE REFERENCES ${BookTable.TABLE_NAME}(${BaseColumns._ID}), " +
                    "$COLUMN_NAME_COVER_DATA BLOB" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)
    }

    /**
     * Book search history table
     */
    object BookSearchHistoryTable : BaseColumns {
        const val TABLE_NAME = "book_search_history"

        // Timestamp for adding a search query
        const val COLUMN_NAME_TIMESTAMP = "timestamp"

        // Search query
        const val COLUMN_NAME_SEARCH_QUERY = "search_query"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_TIMESTAMP INTEGER PRIMARY KEY NOT NULL, " +
                    "$COLUMN_NAME_SEARCH_QUERY VARCHAR NOT NULL" +
                    ")"
        private const val SQL_CREATE_SEARCH_QUERY_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_SEARCH_QUERY}_idx ON $TABLE_NAME($COLUMN_NAME_SEARCH_QUERY)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_SEARCH_QUERY_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  $COLUMN_NAME_TIMESTAMP" +
                ", $COLUMN_NAME_SEARCH_QUERY" +
                " FROM $TABLE_NAME"
    }

    /**
     * Text search history table (in any book)
     */
    object TextSearchHistoryTable : BaseColumns {
        const val TABLE_NAME = "text_search_history"

        // Timestamp for adding a search query
        const val COLUMN_NAME_TIMESTAMP = "timestamp"

        // Search query
        const val COLUMN_NAME_SEARCH_QUERY = "search_query"

        // Case sensitivity
        const val COLUMN_NAME_CASE_SENSITIVITY = "case_sensitivity"

        // Only whole words
        const val COLUMN_NAME_WHOLE_WORDS = "whole_words"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "$COLUMN_NAME_TIMESTAMP INTEGER PRIMARY KEY NOT NULL, " +
                    "$COLUMN_NAME_SEARCH_QUERY VARCHAR NOT NULL, " +
                    "$COLUMN_NAME_CASE_SENSITIVITY INTEGER DEFAULT 0, " +
                    "$COLUMN_NAME_WHOLE_WORDS INTEGER DEFAULT 0" +
                    ")"
        private const val SQL_CREATE_SEARCH_QUERY_INDEX =
            "CREATE INDEX IF NOT EXISTS ${TABLE_NAME}_${COLUMN_NAME_SEARCH_QUERY}_idx ON $TABLE_NAME($COLUMN_NAME_SEARCH_QUERY)"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE, SQL_CREATE_SEARCH_QUERY_INDEX)

        const val SQL_QUERY_ALL = "SELECT " +
                "  $COLUMN_NAME_TIMESTAMP" +
                ", $COLUMN_NAME_SEARCH_QUERY" +
                ", $COLUMN_NAME_CASE_SENSITIVITY" +
                ", $COLUMN_NAME_WHOLE_WORDS" +
                " FROM $TABLE_NAME"
    }

    /**
     * Currently reading book
     */
    object CurrentBookTable : BaseColumns {
        const val TABLE_NAME = "current_book"

        // Full path to the book file (if this file is inside an archive, then the path to the archive is added to the beginning of this path).
        const val COLUMN_NAME_FILEPATH = "filepath"

        // Book opening time (timestamp)
        const val COLUMN_NAME_TIME = "time"

        private const val SQL_CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS $TABLE_NAME (" +
                    "  $COLUMN_NAME_FILEPATH VARCHAR NOT NULL UNIQUE" +
                    " ,$COLUMN_NAME_TIME INTEGER NOT NULL" +
                    ")"
        val SQL_CREATE = arrayOf(SQL_CREATE_TABLE)

        const val SQL_QUERY_ALL = "SELECT" +
                "  $COLUMN_NAME_FILEPATH" +
                ", $COLUMN_NAME_TIME" +
                " FROM $TABLE_NAME"

        const val SQL_DELETE_ALL = "DELETE FROM $TABLE_NAME"
    }
}
