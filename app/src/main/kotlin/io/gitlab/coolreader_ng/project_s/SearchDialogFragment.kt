/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.CheckBox
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.textfield.TextInputLayout
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.extensions.inFullscreenWindow
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener

class SearchDialogFragment(private var bookInfo: BookInfo? = null) : DialogFragment() {

    private data class SearchData(
        var text: String,
        var caseSensitivity: Boolean,
        var wholeWords: Boolean
    ) {
        fun equalsTo(item: DBService.SearchHistoryItem): Boolean {
            val ignoreCase = !caseSensitivity
            return (0 == item.searchQuery.compareTo(text, ignoreCase) &&
                    item.caseSensitivity == caseSensitivity &&
                    item.wholeWords == wholeWords)
        }
    }

    private class TextSearchHistory : ArrayList<DBService.SearchHistoryItem>() {

        fun indexOf(queryString: String): Int {
            var idx = -1
            for ((i, item) in this.withIndex()) {
                if (item.searchQuery.compareTo(queryString, ignoreCase = true) == 0) {
                    idx = i
                    break
                }
            }
            return idx
        }

        fun removeSearchQuery(query: String): Boolean {
            val idx = indexOf(query)
            if (idx >= 0) {
                removeAt(idx)
                return true
            }
            return false
        }

        companion object {
            private const val serialVersionUID: Long = -5764596650222584471L
        }

    }

    interface OnActionsListener {
        fun onJumpTo(bookInfo: BookInfo, searchResult: SearchResultItem)
    }

    var onActionsListener: OnActionsListener? = null

    private var mShowAsDialog = false
    private val mSearchData = SearchData("", caseSensitivity = false, wholeWords = false)
    private val mSearchResults = ArrayList<SearchResultItem>()
    private lateinit var mSearchResultAdapter: SearchResultViewAdapter
    private val mSearchHistory = TextSearchHistory()
    private lateinit var mSearchHistoryAdapter: SearchHistoryAdapter

    internal var dbServiceAccessor: DBServiceAccessor? = null
    internal var docView: LVDocViewWrapper? = null

    private lateinit var mSearchResultRecyclerView: RecyclerView
    private lateinit var mSearchNotFoundLayout: ViewGroup
    private lateinit var mSearchHistoryLayout: ViewGroup
    private lateinit var mSearchHistoryRecyclerView: RecyclerView

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.text_search_panel, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toolbarLocation =
            arguments?.getInt("toolbar", PropNames.App.TOOLBAR_LOCATION_NONE)
                ?: PropNames.App.TOOLBAR_LOCATION_NONE

        val appBarLayout = view.findViewById<AppBarLayout>(R.id.appBarLayout)
        val topAppBar = view.findViewById<MaterialToolbar>(R.id.topAppBar)
        val bottomAppBar = view.findViewById<BottomAppBar>(R.id.bottomAppBar)
        val contentLayout = view.findViewById<ViewGroup>(R.id.contentLayout)
        val btnCancel = view.findViewById<Button>(R.id.btnCancel)
        val searchTextField = view.findViewById<TextInputLayout>(R.id.searchTextLayout)
        val cbCaseSensitivity = view.findViewById<CheckBox>(R.id.cbCaseSensitivity)
        mSearchResultRecyclerView = view.findViewById(R.id.searchResultRecyclerView)
        mSearchNotFoundLayout = view.findViewById(R.id.searchNotFoundLayout)
        mSearchHistoryLayout = view.findViewById(R.id.searchHistoryLayout)
        mSearchHistoryRecyclerView = view.findViewById(R.id.searchHistoryRecyclerView)
        val btnClearHistory = view.findViewById<Button>(R.id.btnClearHistory)

        val a: TypedArray = requireContext().obtainStyledAttributes(
            intArrayOf(android.R.attr.actionBarSize)
        )
        val bottomToolbarSize = a.getDimensionPixelSize(0, 0)
        a.recycle()

        Utils.applyToolBarColorForMenu(requireContext(), topAppBar.menu)
        Utils.applyToolBarColorForMenu(requireContext(), bottomAppBar.menu)

        when (toolbarLocation) {
            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.VISIBLE
                contentLayout.setPadding(0, 0, 0, bottomToolbarSize)
            }

            else -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.GONE
            }
        }

        searchTextField.editText?.requestFocus()

        mSearchHistoryAdapter = SearchHistoryAdapter(mSearchHistory)
        mSearchHistoryRecyclerView.layoutManager = LinearLayoutManager(context)
        mSearchHistoryRecyclerView.adapter = mSearchHistoryAdapter
        mSearchHistoryRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        // Load search history and show it...
        dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.loadTextSearchHistory(1000, object : DBService.SearchHistoryLoadingCallback {
                    override fun onHistoryLoaded(history: Collection<DBService.SearchHistoryItem>?) {
                        if (null != history) {
                            BackgroundThread.postGUI {
                                Utils.syncListByReference(
                                    mSearchHistory,
                                    history,
                                    mSearchHistoryAdapter
                                )
                                // Show 'Search history' layout
                                mSearchHistoryLayout.visibility = View.VISIBLE
                                mSearchResultRecyclerView.visibility = View.GONE
                                mSearchNotFoundLayout.visibility = View.GONE
                            }
                        }
                    }
                })
            }
        })

        mSearchResultAdapter = SearchResultViewAdapter(mSearchResults)
        mSearchResultRecyclerView.layoutManager = LinearLayoutManager(context)
        mSearchResultRecyclerView.adapter = mSearchResultAdapter
        mSearchResultRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        ViewCompat.setOnApplyWindowInsetsListener(view) { _, windowInsets ->
            val mask = if (inFullscreenWindow)
                WindowInsetsCompat.Type.displayCutout()
            else
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            val insets = windowInsets.getInsets(mask)
            var nsInsets = insets
            if (appBarLayout.isVisible) {
                topAppBar.updatePadding(left = insets.left, right = insets.right, top = insets.top)
                nsInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
            } else if (bottomAppBar.isVisible) {
                bottomAppBar.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    bottom = insets.bottom
                )
                nsInsets = Insets.of(
                    insets.left,
                    insets.top,
                    insets.right,
                    bottomToolbarSize + insets.bottom
                )
            }
            contentLayout.updatePadding(
                nsInsets.left,
                nsInsets.top,
                nsInsets.right,
                nsInsets.bottom
            )
            WindowInsetsCompat.CONSUMED
        }

        // Callbacks
        val closeAction: () -> Unit = {
            if (mShowAsDialog) {
                dialog?.dismiss()
            } else {
                if (parentFragmentManager.backStackEntryCount > 0)
                    parentFragmentManager.popBackStack()
            }
        }
        topAppBar.setNavigationOnClickListener {
            closeAction()
        }
        bottomAppBar.setNavigationOnClickListener {
            closeAction()
        }
        btnCancel.setOnClickListener {
            closeAction()
        }
        var enableProcessing = true
        val handler = Handler(Looper.getMainLooper())
        val handlerRunnable = Runnable {
            bookInfo?.let {
                doSearch()
            } ?: run {
                log.error("Trying to search with null BookInfo instance!")
            }
        }
        searchTextField.editText?.setOnEditorActionListener { _, actionId, event ->
            var isDoneAction = false
            when (actionId) {
                EditorInfo.IME_ACTION_DONE,
                EditorInfo.IME_ACTION_SEARCH -> isDoneAction = true

                EditorInfo.IME_NULL -> isDoneAction =
                    (KeyEvent.ACTION_DOWN == event?.action && KeyEvent.KEYCODE_ENTER == event.keyCode)
            }
            if (isDoneAction) {
                handler.removeCallbacks(handlerRunnable)
                mSearchData.caseSensitivity = cbCaseSensitivity.isChecked
                val text = searchTextField.editText?.text
                if (text?.isNotEmpty() == true) {
                    mSearchData.text = text.toString()
                    handler.post(handlerRunnable)
                } else {
                    if (mSearchResults.isNotEmpty()) {
                        val oldSize = mSearchResults.size
                        mSearchResults.clear()
                        mSearchResultAdapter.notifyItemRangeRemoved(0, oldSize)
                    }
                    // Show 'Search history' layout
                    mSearchHistoryLayout.visibility = View.VISIBLE
                    mSearchResultRecyclerView.visibility = View.GONE
                    mSearchNotFoundLayout.visibility = View.GONE
                }
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
        searchTextField.editText?.doOnTextChanged { text, _, _, _ ->
            if (!enableProcessing)
                return@doOnTextChanged
            if (text?.isNotEmpty() != true) {
                // Empty line or null
                if (mSearchResults.isNotEmpty()) {
                    val oldSize = mSearchResults.size
                    mSearchResults.clear()
                    mSearchResultAdapter.notifyItemRangeRemoved(0, oldSize)
                }
                // Show 'Search history' layout
                mSearchHistoryLayout.visibility = View.VISIBLE
                mSearchResultRecyclerView.visibility = View.GONE
                mSearchNotFoundLayout.visibility = View.GONE
            }
        }
        cbCaseSensitivity.setOnCheckedChangeListener { _, isChecked ->
            if (!enableProcessing)
                return@setOnCheckedChangeListener
            handler.removeCallbacks(handlerRunnable)
            mSearchData.caseSensitivity = isChecked
            val text = searchTextField.editText?.text
            if (text?.isNotEmpty() == true) {
                mSearchData.text = text.toString()
                handler.post(handlerRunnable)
            }
        }
        btnClearHistory.setOnClickListener {
            val dialog = MessageDialog(
                R.string.confirmation,
                R.string.are_you_sure_you_want_to_clear_your_search_history_
            )
            dialog.setButton(MessageDialog.StandardButtons.No)
            dialog.setButton(MessageDialog.StandardButtons.Cancel)
            dialog.setButton(MessageDialog.StandardButtons.Yes) {
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.clearTextSearchHistoryItem(object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                if (result) {
                                    val oldSize = mSearchHistory.size
                                    mSearchHistory.clear()
                                    mSearchHistoryAdapter.notifyItemRangeRemoved(0, oldSize)
                                } else {
                                    log.error("Failed to clear search history!")
                                }
                            }
                        })
                    }
                })
            }
            dialog.showDialog(requireContext())
        }
        mSearchHistoryRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                requireContext(),
                false
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val historyItem =
                                if (index >= 0 && index < mSearchHistory.size) mSearchHistory[index] else null
                            historyItem?.let { hi ->
                                enableProcessing = false
                                searchTextField?.editText?.setText(hi.searchQuery)
                                cbCaseSensitivity.isChecked = hi.caseSensitivity
                                // TODO: whole words
                                mSearchData.text = hi.searchQuery
                                mSearchData.caseSensitivity = hi.caseSensitivity
                                mSearchData.wholeWords = hi.wholeWords
                                enableProcessing = true
                                handler.removeCallbacks(handlerRunnable)
                                handler.post(handlerRunnable)
                            }
                        }
                    }
            })
        mSearchResultRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                requireContext(),
                false
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val resultItem =
                                if (index >= 0 && index < mSearchResults.size) mSearchResults[index] else null
                            bookInfo?.let { bi ->
                                resultItem?.let { ri ->
                                    onActionsListener?.onJumpTo(bi, ri)
                                    closeAction.invoke()
                                }
                            }
                        }
                    }
            })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // Without Window.FEATURE_LEFT_ICON for some reason the bottom content of the dialog is cut off
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE or Window.FEATURE_LEFT_ICON)
        // Setting transparent background to remove padding
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (null == bookInfo) {
            bookInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                savedInstanceState?.getParcelable("bookInfo", BookInfo::class.java)
            else
                savedInstanceState?.getParcelable("bookInfo")
        }
        mShowAsDialog = arguments?.getBoolean("asDialog") ?: false
        // Note: onCreateView() function will be called later
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bookInfo?.let { outState.putParcelable("bookInfo", it) }
    }

    override fun onStart() {
        super.onStart()
        if (mShowAsDialog) {
            // Increase dialog window width
            dialog?.let { dialog ->
                val metrics = resources.displayMetrics
                val displayWidth = metrics.widthPixels
                val displayHeight = metrics.heightPixels
                val layoutParams = WindowManager.LayoutParams()
                val oldParams = dialog.window?.attributes
                if (null != oldParams)
                    layoutParams.copyFrom(dialog.window?.attributes)
                else
                    layoutParams.height = 8 * displayHeight / 10
                layoutParams.width = 8 * displayWidth / 10
                dialog.window?.attributes = layoutParams
            }
        }
    }

    private fun doSearch() {
        if (mSearchData.text.isNotEmpty()) {
            var idx = mSearchHistory.indexOf(mSearchData.text)
            val searchQueryToSave = DBService.SearchHistoryItem(
                Utils.timeStamp(),
                mSearchData.text,
                mSearchData.caseSensitivity,
                wholeWords = false
            )
            var changed = false
            var newItemAdded = false
            if (idx >= 0) {
                if (!mSearchData.equalsTo(mSearchHistory[idx])) {
                    mSearchHistory[idx] = searchQueryToSave
                    changed = true
                }
            } else {
                mSearchHistory.add(0, searchQueryToSave)
                idx = 0
                changed = true
                newItemAdded = true
            }
            if (changed) {
                // Save search query to DB
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.saveTextSearchHistoryItem(
                            searchQueryToSave,
                            object : DBService.BooleanResultCallback {
                                override fun onResult(result: Boolean) {
                                    if (result) {
                                        if (!newItemAdded)
                                            mSearchHistoryAdapter.notifyItemChanged(idx)
                                        else
                                            mSearchHistoryAdapter.notifyItemInserted(idx)
                                    } else {
                                        log.error("Failed to save history item!")
                                    }
                                }
                            })
                    }
                })
            }
            // TODO: check if the same book is open in this docView
            BackgroundThread.postBackground {
                docView?.let { doc ->
                    var searchResult =
                        doc.findTextAll(mSearchData.text, mSearchData.caseSensitivity, 10000)
                    if (null != searchResult) {
                        if (searchResult.isEmpty()) {
                            searchResult = null
                            log.debug("Nothing found.")
                        }
                    } else {
                        log.error("Search text failed!")
                    }
                    BackgroundThread.postGUI {
                        // Hide the soft keyboard if it is active
                        val focusedView = view?.findFocus() ?: View(context)
                        val imm =
                            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(focusedView.windowToken, 0)
                        view?.clearFocus()
                        // Change view panel
                        mSearchHistoryLayout.visibility = View.GONE
                        if (null == searchResult) {
                            // Show 'Nothing found' layout
                            mSearchResultRecyclerView.visibility = View.GONE
                            mSearchNotFoundLayout.visibility = View.VISIBLE
                        } else {
                            // Show RecyclerView with search result
                            mSearchNotFoundLayout.visibility = View.GONE
                            mSearchResultRecyclerView.visibility = View.VISIBLE
                            Utils.syncListByReference(
                                mSearchResults,
                                searchResult,
                                mSearchResultAdapter
                            )
                        }
                    }
                } ?: run {
                    log.error("docView is null!")
                }
            }
        }
    }

    companion object {
        private val log = SRLog.create("search")
    }
}