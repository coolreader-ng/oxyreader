/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.Keep
import androidx.annotation.RequiresApi
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import io.gitlab.coolreader_ng.project_s.tts.OnTTSCreatedListener
import io.gitlab.coolreader_ng.project_s.tts.TTSControlBinder
import io.gitlab.coolreader_ng.project_s.tts.TTSControlService
import io.gitlab.coolreader_ng.project_s.tts.TTSControlServiceAccessor
import java.io.File
import java.util.Locale

@Keep
class SettingsFragmentTTS : PreferenceFragmentCompat(),
    SettingsActivity.SettingsFragmentStorageHolder {

    private var mProperties = SRProperties()
    private lateinit var mTTSControlServiceAccessor: TTSControlServiceAccessor
    private val locker = Any()
    private var mODTImportDictLauncher: ActivityResultLauncher<Intent>? = null

    // key - values, value - label
    private val mTTSEngines = HashMap<String, String>()
    private val mTTSLanguages = HashMap<Locale, String>()
    private val mTTSVoices = HashMap<String, String>()
    private var mSelectedLocale: Locale? = null

    private var mEnginePref: ListPreference? = null
    private var mLanguagePref: ListPreference? = null
    private var mVoicePref: ListPreference? = null
    private var mImportDictPref: Preference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mODTImportDictLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (Activity.RESULT_OK == it.resultCode) {
                    val uri = it.data?.data
                    log.debug("import dictionary: file selected: $uri")
                    uri?.let { fileUri ->
                        mTTSControlServiceAccessor.runWithService(object :
                            TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.importDictionary(
                                    fileUri,
                                    object : TTSControlService.BooleanResultCallback {
                                        override fun onResult(result: Boolean) {
                                            ttsbinder.retrieveDictionarySize(object :
                                                TTSControlService.IntResultCallback {
                                                override fun onResult(result: Int) {
                                                    BackgroundThread.postGUI {
                                                        updateDictionarySummary(result)
                                                    }
                                                }
                                            })
                                            BackgroundThread.postGUI {
                                                val message = if (result)
                                                    requireContext().getString(R.string.the_file_was_imported_successfully)
                                                else
                                                    requireContext().getString(R.string.failed_to_import_file_)
                                                val toast = Toast.makeText(
                                                    requireContext(),
                                                    message,
                                                    Toast.LENGTH_LONG
                                                )
                                                toast.show()
                                            }
                                        }
                                    })
                            }
                        })
                    }
                }
            }
    }

    override fun onDestroy() {
        mTTSControlServiceAccessor.unbind()
        super.onDestroy()
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = SettingsActivity.CREPropsDataStorage(mProperties)

        setPreferencesFromResource(R.xml.preferences_tts, rootKey)

        mEnginePref = findPreference(SettingsActivity.PREF_KEY_TTS_ENGINE)
        val autoSetLangPref =
            findPreference<SwitchPreferenceCompat>(SettingsActivity.PREF_KEY_TTS_AUTOSET_LANGUAGE)
        mLanguagePref = findPreference(SettingsActivity.PREF_KEY_TTS_LANGUAGE)
        mVoicePref = findPreference(SettingsActivity.PREF_KEY_TTS_VOICE)
        val useImmobilityTimeoutPref =
            findPreference<SwitchPreferenceCompat>(SettingsActivity.PREF_KEY_TTS_USE_IMMOBILITY_TIMEOUT)
        val immobilityTimeoutValuePref =
            findPreference<Preference>(SettingsActivity.PREF_KEY_TTS_IMMOBILITY_TIMEOUT_VALUE)
        val useDictPref =
            findPreference<SwitchPreferenceCompat>(SettingsActivity.PREF_KEY_TTS_USE_DICTIONARY)
        mImportDictPref = findPreference(SettingsActivity.PREF_KEY_TTS_IMPORT_DICT)

        //val autoSetLang = mProperties.getBool(PropNames.App.TTS_USE_DOC_LANG, true)
        val autoSetLang = autoSetLangPref?.isChecked ?: true
        mLanguagePref?.isEnabled = !autoSetLang
        mVoicePref?.isEnabled = !autoSetLang
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            mVoicePref?.isVisible = false

        // We must call TTSControlService.initTTS() to obtain TTS properties
        val ttsEngine = mProperties.getProperty(PropNames.App.TTS_ENGINE)
        mTTSControlServiceAccessor = TTSControlServiceAccessor(requireContext())
        mTTSControlServiceAccessor.bind(object : TTSControlServiceAccessor.Callback {
            override fun run(ttsbinder: TTSControlBinder) {
                ttsbinder.initTTS(ttsEngine, object : OnTTSCreatedListener {
                    override fun onCreated() {
                        log.debug("TTS init complete")
                        // Get a list of available TTS engines
                        obtainAvailableEngines(ttsbinder)
                        ttsbinder.setUseDictionary(true == useDictPref?.isEnabled)
                        ttsbinder.retrieveDictionarySize(object :
                            TTSControlService.IntResultCallback {
                            override fun onResult(result: Int) {
                                BackgroundThread.postGUI { updateDictionarySummary(result) }
                            }
                        })
                    }

                    override fun onFailed() {
                        log.error("Failed to init TTS engine $ttsEngine")
                    }

                    override fun onTimedOut() {
                        log.error("Timeout initializing TTS engine $ttsEngine")
                    }
                })
            }
        })

        mEnginePref?.setOnPreferenceChangeListener { _, newValue ->
            updateEngineSummary(newValue.toString())
            // Using the selected TTS engine, obtains a list of available languages
            mTTSControlServiceAccessor.runWithService(
                object : TTSControlServiceAccessor.Callback {
                    override fun run(ttsbinder: TTSControlBinder) {
                        // Stop any TTS playback
                        ttsbinder.stop(null)
                        // Use selected TTS engine and obtain available languages
                        ttsbinder.initTTS(newValue.toString(), object : OnTTSCreatedListener {
                            override fun onCreated() {
                                obtainAvailableLanguages(ttsbinder)
                            }

                            override fun onFailed() {
                                log.error("Failed to init TTS engine $newValue")
                                BackgroundThread.postGUI {
                                    mLanguagePref?.entries = emptyArray
                                    mLanguagePref?.entryValues = emptyArray
                                    mLanguagePref?.summary =
                                        requireContext().getString(R.string.invalid_value)
                                    mVoicePref?.entries = emptyArray
                                    mVoicePref?.entryValues = emptyArray
                                    mVoicePref?.summary =
                                        requireContext().getString(R.string.invalid_value)
                                }
                            }

                            override fun onTimedOut() {
                                log.error("Timeout initializing TTS engine $newValue")
                                BackgroundThread.postGUI {
                                    mLanguagePref?.entries = emptyArray
                                    mLanguagePref?.entryValues = emptyArray
                                    mLanguagePref?.summary =
                                        requireContext().getString(R.string.invalid_value)
                                    mVoicePref?.entries = emptyArray
                                    mVoicePref?.entryValues = emptyArray
                                    mVoicePref?.summary =
                                        requireContext().getString(R.string.invalid_value)
                                }
                            }
                        })
                    }
                }
            )
            return@setOnPreferenceChangeListener true
        }

        autoSetLangPref?.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue as Boolean
            mLanguagePref?.isEnabled = !enabled
            mVoicePref?.isEnabled = !enabled
            return@setOnPreferenceChangeListener true
        }

        mLanguagePref?.setOnPreferenceChangeListener { _, newValue ->
            mSelectedLocale = Utils.langTagToLocale(newValue.toString())
            updateLanguageSummary(newValue.toString())
            // Using the selected TTS language, obtains a list of available voices
            mTTSControlServiceAccessor.runWithService(
                object : TTSControlServiceAccessor.Callback {
                    override fun run(ttsbinder: TTSControlBinder) {
                        ttsbinder.pause(null)
                        ttsbinder.setLanguage(newValue.toString(),
                            object : TTSControlService.BooleanResultCallback {
                                override fun onResult(result: Boolean) {
                                    if (result) {
                                        mSelectedLocale?.let {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                                obtainAvailableVoices(it, ttsbinder)
                                        } ?: run {
                                            BackgroundThread.postGUI {
                                                mVoicePref?.entries = emptyArray
                                                mVoicePref?.entryValues = emptyArray
                                                mVoicePref?.summary =
                                                    requireContext().getString(R.string.value_not_set)
                                            }
                                        }
                                    } else {
                                        log.error("Failed to set language")
                                        BackgroundThread.postGUI {
                                            mVoicePref?.entries = emptyArray
                                            mVoicePref?.entryValues = emptyArray
                                            mVoicePref?.summary =
                                                requireContext().getString(R.string.invalid_value)
                                        }
                                    }
                                }
                            })
                    }
                }
            )
            return@setOnPreferenceChangeListener true
        }

        mVoicePref?.setOnPreferenceChangeListener { _, newValue ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                updateVoiceSummary(newValue.toString())
                mTTSControlServiceAccessor.runWithService(
                    object : TTSControlServiceAccessor.Callback {
                        override fun run(ttsbinder: TTSControlBinder) {
                            ttsbinder.pause(null)
                            val voiceName = newValue.toString()
                            ttsbinder.setVoice(voiceName,
                                object : TTSControlService.BooleanResultCallback {
                                    override fun onResult(result: Boolean) {
                                        if (result)
                                            log.debug("Voice $voiceName is set")
                                        else
                                            log.error("Failed to set voice ${voiceName}!")
                                    }
                                })
                        }
                    }
                )
            }
            return@setOnPreferenceChangeListener true
        }

        val useImmobilityTimeout = useImmobilityTimeoutPref?.isChecked ?: false
        immobilityTimeoutValuePref?.isEnabled = useImmobilityTimeout
        useImmobilityTimeoutPref?.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue as Boolean
            immobilityTimeoutValuePref?.isEnabled = enabled
            return@setOnPreferenceChangeListener true
        }

        useDictPref?.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue as Boolean
            mTTSControlServiceAccessor.runWithService(object : TTSControlServiceAccessor.Callback {
                override fun run(ttsbinder: TTSControlBinder) {
                    ttsbinder.setUseDictionary(enabled)
                    ttsbinder.retrieveDictionarySize(object : TTSControlService.IntResultCallback {
                        override fun onResult(result: Int) {
                            BackgroundThread.postGUI { updateDictionarySummary(result) }
                        }
                    })
                }
            })
            true
        }

        mImportDictPref?.setOnPreferenceClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                mODTImportDictLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                    val mimeTypes = arrayOf("text/plain")
                    putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                    type = "*/*"
                    addCategory(Intent.CATEGORY_OPENABLE)
                })
            } else {
                val dialog = MessageDialog(R.string.data_input)
                    .setInputTextMode(R.string.please_enter_the_path_to_the_dictionary_file)
                val sdCardPath = Environment.getExternalStorageDirectory().path
                dialog.setInputHelperText(R.string.full_path_to_the_dictionary_file)
                    .setInputPlaceholder("${sdCardPath}/")
                dialog.setButton(MessageDialog.StandardButtons.Cancel)
                dialog.setButton(MessageDialog.StandardButtons.Ok) {
                    val dictFilePath = dialog.inputText
                    if (!dictFilePath.isNullOrEmpty()) {
                        log.debug("Selected dictionary path: $dictFilePath")
                        val fileUri = Uri.fromFile(File(dictFilePath))
                        mTTSControlServiceAccessor.runWithService(object :
                            TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.importDictionary(
                                    fileUri,
                                    object : TTSControlService.BooleanResultCallback {
                                        override fun onResult(result: Boolean) {
                                            ttsbinder.retrieveDictionarySize(object :
                                                TTSControlService.IntResultCallback {
                                                override fun onResult(result: Int) {
                                                    BackgroundThread.postGUI {
                                                        updateDictionarySummary(result)
                                                    }
                                                }
                                            })
                                            BackgroundThread.postGUI {
                                                val message = if (result)
                                                    requireContext().getString(R.string.the_file_was_imported_successfully)
                                                else
                                                    requireContext().getString(R.string.failed_to_import_file_)
                                                val toast = Toast.makeText(
                                                    requireContext(),
                                                    message,
                                                    Toast.LENGTH_LONG
                                                )
                                                toast.show()
                                            }
                                        }
                                    })
                            }
                        })
                    }
                }
                dialog.showDialog(requireContext())
            }
            true
        }
    }

    override fun setProperties(props: SRProperties) {
        mProperties = props
    }

    override fun resetToDefaults() {
        for ((key, value) in SettingsManager.Defaults.TTS) {
            mProperties.setProperty(key, value)
        }
    }

    private fun obtainAvailableEngines(binder: TTSControlBinder) {
        binder.retrieveAvailableEngines(
            object : TTSControlService.RetrieveEnginesListCallback {
                override fun onResult(list: List<TextToSpeech.EngineInfo>?) {
                    log.debug("engines: $list")
                    list?.let {
                        val titles = Array(it.size) { i -> it[i].label }
                        val values = Array(it.size) { i -> it[i].name }
                        synchronized(locker) {
                            mTTSEngines.clear()
                            for ((i, value) in values.withIndex()) {
                                mTTSEngines[value] = titles[i]
                            }
                        }
                        BackgroundThread.postGUI {
                            mEnginePref?.entries = titles
                            mEnginePref?.entryValues = values
                            updateEngineSummary()
                        }
                        obtainAvailableLanguages(binder)
                    } ?: run {
                        log.error("Failed to get TTS engines!")
                        synchronized(locker) {
                            mTTSEngines.clear()
                        }
                        mLanguagePref?.entries = emptyArray
                        mLanguagePref?.entryValues = emptyArray
                        mLanguagePref?.summary = requireContext().getString(R.string.invalid_value)
                        mVoicePref?.entries = emptyArray
                        mVoicePref?.entryValues = emptyArray
                        mVoicePref?.summary = requireContext().getString(R.string.invalid_value)
                    }
                }
            })
    }

    private fun obtainAvailableLanguages(binder: TTSControlBinder) {
        binder.retrieveAvailableLocales(object : TTSControlService.RetrieveLocalesListCallback {
            override fun onResult(list: List<Locale>?) {
                log.debug("languages: $list")
                list?.let {
                    val titles = Array(it.size) { i -> it[i].displayName }
                    val locales = Array(it.size) { i -> it[i] }
                    val values = Array(it.size) { i -> Utils.localeToLangTag(it[i]) }
                    synchronized(locker) {
                        mTTSLanguages.clear()
                        for ((i, value) in locales.withIndex()) {
                            mTTSLanguages[value] = titles[i]
                        }
                    }
                    BackgroundThread.postGUI {
                        mLanguagePref?.entries = titles
                        mLanguagePref?.entryValues = values
                        updateLanguageSummary()
                        var locale: Locale? = null
                        val ttsLangTag = mLanguagePref?.value
                        if (null != ttsLangTag) {
                            locale = Utils.langTagToLocale(ttsLangTag)
                        }
                        if (null != locale) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                obtainAvailableVoices(locale, binder)
                        } else {
                            mVoicePref?.entries = emptyArray
                            mVoicePref?.entryValues = emptyArray
                            mVoicePref?.summary = requireContext().getString(R.string.value_not_set)
                        }
                    }
                } ?: run {
                    log.error("Failed to obtain languages list!")
                    synchronized(locker) {
                        mTTSLanguages.clear()
                    }
                    BackgroundThread.postGUI {
                        mLanguagePref?.entries = emptyArray
                        mLanguagePref?.entryValues = emptyArray
                        mLanguagePref?.summary = requireContext().getString(R.string.invalid_value)
                        mVoicePref?.entries = emptyArray
                        mVoicePref?.entryValues = emptyArray
                        mVoicePref?.summary = requireContext().getString(R.string.invalid_value)
                    }
                }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun obtainAvailableVoices(locale: Locale, binder: TTSControlBinder) {
        binder.retrieveAvailableVoices(
            locale,
            object : TTSControlService.RetrieveVoicesListCallback {
                override fun onResult(list: List<Voice>?) {
                    log.debug("voices: $list")
                    list?.let {
                        val titles = Array(it.size) { i -> it[i].name }
                        val values = Array(it.size) { i -> it[i].name }
                        synchronized(locker) {
                            mTTSVoices.clear()
                            for ((i, value) in values.withIndex()) {
                                mTTSVoices[value] = titles[i]
                            }
                        }
                        BackgroundThread.postGUI {
                            mVoicePref?.entries = titles
                            mVoicePref?.entryValues = values
                            updateVoiceSummary()
                        }
                    } ?: run {
                        synchronized(locker) {
                            mTTSVoices.clear()
                        }
                        mVoicePref?.entries = emptyArray
                        mVoicePref?.entryValues = emptyArray
                        mVoicePref?.summary = requireContext().getString(R.string.invalid_value)
                    }
                }
            })
    }

    private fun updateEngineSummary(newValue: String? = null) {
        val ttsEngine = newValue ?: mEnginePref?.value
        synchronized(locker) {
            val title = mTTSEngines[ttsEngine]
            if (null != title)
                mEnginePref?.summary = title
            else
                mEnginePref?.summary = requireContext().getString(R.string.invalid_value)
        }
    }

    private fun updateLanguageSummary(newValue: String? = null) {
        var title: String? = null
        val ttsLangTag = newValue ?: mLanguagePref?.value
        if (null != ttsLangTag) {
            val locale = Utils.langTagToLocale(ttsLangTag)
            synchronized(locker) {
                title = mTTSLanguages[locale]
            }
            mSelectedLocale = locale
        } else {
            mSelectedLocale = null
        }
        if (null != title)
            mLanguagePref?.summary = title
        else
            mLanguagePref?.summary = requireContext().getString(R.string.invalid_value)
    }

    private fun updateVoiceSummary(newValue: String? = null) {
        val ttsVoice = newValue ?: mVoicePref?.value
        synchronized(locker) {
            val title = mTTSVoices[ttsVoice]
            if (null != title)
                mVoicePref?.summary = title
            else
                mVoicePref?.summary = requireContext().getString(R.string.invalid_value)
        }
    }

    private fun updateDictionarySummary(dictSize: Int) {
        mImportDictPref?.summary =
            requireContext().getString(R.string.tap_to_import_dictionary_with_count, dictSize)
    }

    companion object {
        private val log = SRLog.create("tts_prefs")
        private val emptyArray = Array(0) { "" }
    }
}