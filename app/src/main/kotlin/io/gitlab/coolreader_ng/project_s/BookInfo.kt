/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import java.io.ByteArrayInputStream
import java.io.File
import java.io.ObjectInputStream
import java.util.Collections

@UsedInJNI
class BookInfo() : Parcelable {

    enum class ReadingStatus {
        NONE,
        IN_READING,
        DONE,
        PLANNED;

        companion object {
            @JvmStatic
            fun byOrdinal(ord: Int): ReadingStatus {
                if (ord >= 0 && ord < entries.size)
                    return entries[ord]
                return NONE
            }
        }
    }

    /**
     * Book ID in database
     */
    var id: Long? = null

    /**
     * File containing this book
     */
    @UsedInJNI
    var fileInfo: FileInfo = FileInfo("")
        private set

    /**
     * Book title
     */
    @UsedInJNI
    var title: String? = null

    /**
     * Book authors
     */
    @UsedInJNI
    var authors: Set<String>? = null

    /**
     * Title of the book series
     */
    @UsedInJNI
    var series: String? = null

    /**
     * Book number in the series.
     */
    @UsedInJNI
    var seriesNumber: Int? = null

    /**
     * Keyword for EPUB or unknown genre codes for FB2
     */
    @UsedInJNI
    var keywords: Set<String>? = null

    /**
     * genre code identifiers (for FB2)
     */
    var genresIds: Set<Int>? = null

    /**
     * Unsupported genre codes (for FB2)
     */
    var unsupportedGenres: Set<String>? = null

    /**
     * Primary language of the book (ISO-639-2 alpha2 or alpha3 or ISO-639-3).
     */
    @UsedInJNI
    var language: String? = null

    /**
     * Annotation of the book and some of its parameters in an arbitrary form.
     */
    @UsedInJNI
    var description: String? = null

    /**
     * book file format
     */
    @UsedInJNI
    var format: DocumentFormat = DocumentFormat.NONE

    /**
     * Various document flags (for example, 'use document styles', 'use document fonts', etc)
     */
    var flags = 0

    /**
     * Specific DOM version for this book
     */
    var domVersion = 0

    /**
     * Specific block rendering flags for this book.
     */
    var blockRenderingFlags = 0

    /**
     * Book reading status.
     */
    var readingStatus = ReadingStatus.NONE

    /**
     * Book rating code. 0 - not set, 1 - 5 - rating
     */
    var rating = 0

    /**
     * last reading position as bookmark
     */
    var lastPosition: Bookmark? = null
        private set

    /**
     * Extra data (byte array)
     */
    var extraData: ByteArray? = null
        set(value) {
            field = value
            if (null != field)
                doApplyExtraData()
        }

    /**
     * All bookmarks in this book
     */
    private var bookmarks: ArrayList<Bookmark> = ArrayList()

    private class BookmarkComparator : Comparator<Bookmark?> {
        override fun compare(bm1: Bookmark?, bm2: Bookmark?): Int {
            if (null == bm1)
                return if (null != bm2) -1 else 0
            else if (null == bm2)
                return 1
            if (bm1.percent < bm2.percent)
                return -1
            if (bm1.percent > bm2.percent)
                return 1
            // TODO: compare other fields
            return 0
        }
    }

    fun updateAccess() {
        System.currentTimeMillis().let {
            lastPosition?.timeStamp = it
            fileInfo.lastAccessTime = it
        }
    }

    fun updateReadingTime(readingTime: Long) {
        lastPosition?.readingTime = readingTime
    }

    /**
     * Deep copy.
     * @param bookInfo is source object to copy from.
     */
    constructor(bookInfo: BookInfo) : this() {
        id = bookInfo.id
        fileInfo = FileInfo(bookInfo.fileInfo)
        title = bookInfo.title
        bookInfo.authors?.let { authors = HashSet<String>(it) }
        series = bookInfo.series
        seriesNumber = bookInfo.seriesNumber
        bookInfo.keywords?.let { keywords = HashSet<String>(it) }
        bookInfo.genresIds?.let { genresIds = HashSet<Int>(it) }
        bookInfo.unsupportedGenres?.let { unsupportedGenres = HashSet<String>(it) }
        language = bookInfo.language
        description = bookInfo.description
        format = bookInfo.format
        flags = bookInfo.flags
        domVersion = bookInfo.domVersion
        blockRenderingFlags = bookInfo.blockRenderingFlags
        readingStatus = bookInfo.readingStatus
        rating = bookInfo.rating
        bookInfo.lastPosition?.let { lastPosition = Bookmark(it) }
        for (i in 0 until bookInfo.bookmarkCount) {
            addBookmark(Bookmark(bookInfo.getBookmark(i)))
        }
        extraData = bookInfo.extraData?.copyOf()
    }

    constructor(filePath: String) : this() {
        this.fileInfo = FileInfo(filePath)
    }

    constructor(fileInfo: FileInfo) : this() {
        this.fileInfo = fileInfo // FileInfo(fileInfo);
    }

    protected constructor(parcel: Parcel) : this() {
        val mark: Byte = parcel.readByte()
        id = if (0 == mark.toInt())
            null
        else
            parcel.readLong()
        val fi: FileInfo? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            parcel.readParcelable(FileInfo::class.java.classLoader, FileInfo::class.java)
        else
            parcel.readParcelable(FileInfo::class.java.classLoader)
        fileInfo = fi ?: FileInfo("")
        title = parcel.readString()
        val authorsRead = parcel.readString()
        val authorsArray = authorsRead?.split("|")
        if (authorsArray?.isNotEmpty() == true) {
            val authorsFab = HashSet<String>(authorsArray.size)
            for (author in authorsArray) {
                if (author.isNotEmpty())
                    authorsFab.add(author)
            }
            authors = authorsFab
        } else {
            authors = null
        }
        series = parcel.readString()
        seriesNumber = parcel.readInt()
        if (seriesNumber == -1)
            seriesNumber = null
        val keywordsRead = parcel.readString()
        val keywordsArray = keywordsRead?.split("|")
        if (keywordsArray?.isNotEmpty() == true) {
            val keywordsFab = HashSet<String>(keywordsArray.size)
            for (keyword in keywordsArray) {
                if (keyword.isNotEmpty())
                    keywordsFab.add(keyword)
            }
            keywords = keywordsFab
        } else {
            keywords = null
        }
        val genresRead = parcel.readString()
        val genresArray = genresRead?.split("|")
        if (genresArray?.isNotEmpty() == true) {
            val genresFab = HashSet<Int>(genresArray.size)
            for (sId in genresArray) {
                val id = try {
                    sId.toInt(10)
                } catch (e: Exception) {
                    null
                }
                if (null != id)
                    genresFab.add(id)
            }
            genresIds = genresFab
        } else {
            genresIds = null
        }
        val unsupportedGenresRead = parcel.readString()
        val unsupportedGenresArray = unsupportedGenresRead?.split("|")
        if (unsupportedGenresArray?.isNotEmpty() == true) {
            val genresFab = HashSet<String>(unsupportedGenresArray.size)
            for (code in unsupportedGenresArray) {
                if (code.isNotEmpty())
                    genresFab.add(code)
            }
            unsupportedGenres = genresFab
        } else {
            unsupportedGenres = null
        }
        language = parcel.readString()
        description = parcel.readString()
        val formatName = parcel.readString()
        format = try {
            DocumentFormat.valueOf(formatName ?: "")
        } catch (e: Exception) {
            DocumentFormat.NONE
        }
        flags = parcel.readInt()
        domVersion = parcel.readInt()
        blockRenderingFlags = parcel.readInt()
        readingStatus = ReadingStatus.byOrdinal(parcel.readInt())
        rating = parcel.readInt()
        lastPosition = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            parcel.readParcelable(Bookmark::class.java.classLoader, Bookmark::class.java)
        else
            parcel.readParcelable(Bookmark::class.java.classLoader)
        bookmarks = parcel.createTypedArrayList(Bookmark.CREATOR) ?: ArrayList()
        extraData = parcel.createByteArray()
    }

    fun isFlagSet(flag: Int): Boolean {
        return (flags and flag) != 0
    }

    fun setLastPosition(position: Bookmark) {
        synchronized(this) {
            if (lastPosition != null) {
                if (position.startPos != null && position.startPos == lastPosition!!.startPos)
                    return  // not changed
                position.id = lastPosition!!.id
            }
            lastPosition = position
            if (fileInfo.lastAccessTime < lastPosition!!.timeStamp)
                fileInfo.lastAccessTime = lastPosition!!.timeStamp
        }
    }

    @Synchronized
    fun addBookmark(bm: Bookmark): Boolean {
        if (bm.type == Bookmark.TYPE_LAST_POSITION) {
            lastPosition = bm
        } else {
            if (findBookmarkIndex(bm) >= 0) {
                log.warn("duplicate bookmark added " + bm.uniqueKey)
                return false
            } else {
                bookmarks.add(bm)
            }
        }
        return true
    }

    @get:Synchronized
    val bookmarkCount: Int
        get() = bookmarks.size

    @Synchronized
    fun getBookmark(index: Int): Bookmark {
        return bookmarks[index]
    }

    @get:Synchronized
    val allBookmarks: ArrayList<Bookmark>
        get() {
            val list = ArrayList<Bookmark>(bookmarks.size + 1)
            lastPosition?.let { list.add(it) }
            list.addAll(bookmarks)
            return list
        }

    @Synchronized
    fun findBookmark(bm: Bookmark?): Bookmark? {
        if (bm == null)
            return null
        val index = findBookmarkIndex(bm)
        return if (index < 0) null else bookmarks[index]
    }

    private fun findBookmarkIndex(bm: Bookmark?): Int {
        if (bm == null)
            return -1
        for ((i, item) in bookmarks.withIndex()) {
            if (item.equalUniqueKey(bm))
                return i
        }
        return -1
    }

    @Synchronized
    fun syncBookmark(bm: Bookmark?): Bookmark? {
        if (bm == null)
            return null
        val index = findBookmarkIndex(bm)
        if (index < 0) {
            addBookmark(bm)
            return bm
        }
        val item = bookmarks[index]
        if (item.timeStamp >= bm.timeStamp)
            return null
        item.type = bm.type
        item.timeStamp = bm.timeStamp
        item.posText = bm.posText
        item.setCommentText(bm.commentText)
        return item
    }

    @Synchronized
    fun updateBookmark(bm: Bookmark?): Bookmark? {
        if (bm == null)
            return null
        val index = findBookmarkIndex(bm)
        if (index < 0) {
            log.error("cannot find bookmark $bm")
            return null
        }
        val item = bookmarks[index]
        item.timeStamp = bm.timeStamp
        item.posText = bm.posText
        item.setCommentText(bm.commentText)
        return item
    }

    @Synchronized
    fun removeBookmark(bm: Bookmark?): Bookmark? {
        if (bm == null)
            return null
        val index = findBookmarkIndex(bm)
        if (index < 0) {
            log.error("cannot find bookmark $bm")
            return null
        }
        return bookmarks.removeAt(index)
    }

    @Synchronized
    fun sortBookmarks() {
        Collections.sort(bookmarks, BookmarkComparator())
    }

    @Synchronized
    fun formatBookmarksExportText(): String {
        val pathname = File(fileInfo.pathNameWA ?: "")
        val buf = StringBuilder()
        buf.append("# file name: ${pathname.name}\n")
        buf.append("# file path: ${pathname.parent}\n")
        buf.append("# book title: $title\n")
        buf.append("# author: ${formatAuthors(authors)}\n")
        buf.append("# series: $series - $seriesNumber\n")
        buf.append("\n")
        for (bm in bookmarks) {
            if (bm.type == Bookmark.TYPE_COMMENT || bm.type == Bookmark.TYPE_CORRECTION) {
                val percent = bm.percent
                var ps = (percent % 100).toString()
                if (ps.length < 2)
                    ps = "0$ps"
                ps = "${(percent / 100)}.${ps}%"
                if (bm.type == Bookmark.TYPE_COMMENT)
                    buf.append("## $ps - comment\n")
                else
                    buf.append("## $ps - correction\n")
                if (bm.titleText != null)
                    buf.append("## ${bm.titleText}\n")
                if (bm.posText != null)
                    buf.append("<< ${bm.posText}\n")
                if (bm.commentText != null)
                    buf.append(">> ${bm.commentText}\n")
                buf.append("\n")
            }
        }
        return buf.toString()
    }

    @Synchronized
    fun removeBookmark(index: Int): Bookmark? {
        return bookmarks.removeAt(index)
    }

    @Synchronized
    fun setBookmarks(list: List<Bookmark>?) {
        lastPosition = null
        bookmarks = ArrayList()
        if (null != list) {
            for (bm in list)
                addBookmark(bm)
        }
    }

    fun authorsToString(delimiter: String): String {
        val res = StringBuilder()
        authors?.let {
            val iter = it.iterator()
            while (iter.hasNext()) {
                res.append(iter.next())
                if (iter.hasNext())
                    res.append(delimiter)
            }
        }
        return res.toString()
    }

    fun seriesToString(): String {
        val res = StringBuilder()
        if (!series.isNullOrBlank()) {
            res.append(series)
            res.append(" - ")
            seriesNumber?.let { res.append(it.toString()) } ?: res.append("0")
        }
        return res.toString()
    }

    fun deleteBookFile(): Boolean {
        return fileInfo.deleteFile()
    }

    val isBookFileExists: Boolean
        get() = fileInfo.isFileExists

    val isBookFileAvailable: Boolean
        get() = fileInfo.isFileReadable

    override fun toString(): String {
        return ("BookInfo [fileInfo=${fileInfo}, lastPosition=${lastPosition}]")
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        id?.let {
            dest.writeByte(1.toByte())
            dest.writeLong(it)
        } ?: {
            dest.writeByte(0.toByte())
        }
        dest.writeParcelable(fileInfo, flags)
        dest.writeString(title)
        val authorsToWrite = StringBuilder()
        authors?.let {
            for (author in it) {
                if (author.isNotEmpty()) {
                    if (authorsToWrite.isNotEmpty())
                        authorsToWrite.append("|")
                    authorsToWrite.append(author)
                }
            }
        }
        dest.writeString(authorsToWrite.toString())
        dest.writeString(series)
        dest.writeInt(seriesNumber ?: -1)
        val keywordsToWrite = StringBuilder()
        keywords?.let {
            for (keyword in it) {
                if (keyword.isNotEmpty()) {
                    if (keywordsToWrite.isNotEmpty())
                        keywordsToWrite.append("|")
                    keywordsToWrite.append(keyword)
                }
            }
        }
        dest.writeString(keywordsToWrite.toString())
        val genresToWrite = StringBuilder()
        genresIds?.let {
            for (id in it) {
                if (genresToWrite.isNotEmpty())
                    genresToWrite.append("|")
                genresToWrite.append("$id")
            }
        }
        dest.writeString(genresToWrite.toString())
        val unsupportedGenresToWrite = StringBuilder()
        unsupportedGenres?.let {
            for (code in it) {
                if (code.isNotEmpty()) {
                    if (unsupportedGenresToWrite.isNotEmpty())
                        unsupportedGenresToWrite.append("|")
                    unsupportedGenresToWrite.append(code)
                }
            }
        }
        dest.writeString(unsupportedGenresToWrite.toString())
        dest.writeString(language)
        dest.writeString(description)
        dest.writeString(format.name)
        dest.writeInt(this.flags)
        dest.writeInt(domVersion)
        dest.writeInt(blockRenderingFlags)
        dest.writeInt(readingStatus.ordinal)
        dest.writeInt(rating)
        dest.writeParcelable(lastPosition, flags)
        dest.writeTypedList(bookmarks)
        dest.writeByteArray(extraData)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (null == other)
            return false
        if (javaClass != other.javaClass)
            return false
        val obj = other as BookInfo
        if (fileInfo != obj.fileInfo)
            return false
        if (title != obj.title)
            return false
        if (authors != obj.authors)
            return false
        if (series != obj.series)
            return false
        if (seriesNumber != obj.seriesNumber)
            return false
        if (keywords != obj.keywords)
            return false
        if (language != obj.language)
            return false
        // do not compare genres of books, because in the absence of certain genres in the handbook,
        // the 'genres' field obtained from the database will not be equal to the field obtained when parsing the book file.
        // TODO: compare genres & unsupported genres
        /*
        if (!eqGenre(genres, other.genres))
            return false;
        */
        if (description != obj.description)
            return false
        if (format != obj.format)
            return false
        if (flags != obj.flags)
            return false
        if (domVersion != obj.domVersion)
            return false
        if (blockRenderingFlags != obj.blockRenderingFlags)
            return false
        if (readingStatus != obj.readingStatus)
            return false
        if (rating != obj.rating)
            return false
        if (lastPosition != obj.lastPosition)
            return false
        if (bookmarks.size != obj.bookmarks.size)
            return false
        else {
            // TODO: optimize possible slow methods
            //  deep copy, sorting...
            val sortedBookmarks: ArrayList<Bookmark> = ArrayList(bookmarks.size)
            sortedBookmarks.addAll(bookmarks)
            Collections.sort(sortedBookmarks, BookmarkComparator())
            val otherSortedBookmarks: ArrayList<Bookmark> = ArrayList(obj.bookmarks.size)
            otherSortedBookmarks.addAll(obj.bookmarks)
            Collections.sort(otherSortedBookmarks, BookmarkComparator())
            for ((i, bk) in sortedBookmarks.withIndex()) {
                val other_bk = otherSortedBookmarks[i]
                if (bk != other_bk)
                    return false
            }
        }
        // We are ignoring the `extraData` field here
        return true
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = fileInfo.hashCode()
        result = prime * result + (title?.hashCode() ?: 0)
        result = prime * result + (authors?.hashCode() ?: 0)
        result = prime * result + (series?.hashCode() ?: 0)
        result = prime * result + (seriesNumber?.hashCode() ?: 0)
        result = prime * result + (keywords?.hashCode() ?: 0)
        result = prime * result + (genresIds?.hashCode() ?: 0)
        result = prime * result + (unsupportedGenres?.hashCode() ?: 0)
        result = prime * result + (language?.hashCode() ?: 0)
        result = prime * result + (description?.hashCode() ?: 0)
        result = prime * result + format.hashCode()
        result = prime * result + flags.hashCode()
        result = prime * result + domVersion.hashCode()
        result = prime * result + blockRenderingFlags.hashCode()
        result = prime * result + readingStatus.hashCode()
        result = prime * result + rating.hashCode()
        result = prime * result + (lastPosition?.hashCode() ?: 0)
        result = prime * result + bookmarks.hashCode()
        result = prime * result + (extraData?.hashCode() ?: 0)
        return result
    }

    private fun doApplyExtraData() {
        if (!format.canParseProperties && null != extraData) {
            try {
                val byteStream = ByteArrayInputStream(extraData)
                val objectStream = ObjectInputStream(byteStream)
                val map = objectStream.readObject()
                if (map is HashMap<*, *>) {
                    val originalFileName = map[EXTRA_ORIGINAL_FILENAME]
                    if (originalFileName is String) {
                        if (originalFileName.isNotEmpty()) {
                            // Set original file name as title
                            title = originalFileName
                        }
                    }
                }
                objectStream.close()
                byteStream.close()
            } catch (e: Exception) {
                log.error(e.toString())
            }
        }
    }

    fun reApplyExtraData() {
        doApplyExtraData()
    }

    companion object {
        private val log = SRLog.create("bookinfo")

        @Suppress("unused")
        @JvmField
        val CREATOR = object : Parcelable.Creator<BookInfo?> {
            override fun createFromParcel(parcel: Parcel): BookInfo {
                return BookInfo(parcel)
            }

            override fun newArray(size: Int): Array<BookInfo?> {
                return arrayOfNulls(size)
            }
        }

        // Bit masks for the 'flags' field
        // 16 lower bits reserved for document flags
        const val DONT_USE_DOCUMENT_STYLES_FLAG = 1
        const val DONT_REFLOW_TXT_FILES_FLAG = 2
        const val DONT_USE_DOCUMENT_FONTS_FLAG = 4

        // Hash keys for the 'extraData' field
        const val EXTRA_ORIGINAL_FILENAME = "original_filename"

        fun formatAuthors(authors: Collection<String>?): String {
            val builder = StringBuilder()
            authors?.let {
                val iter = it.iterator()
                while (iter.hasNext()) {
                    val author = iter.next()
                    builder.append(author)
                    if (iter.hasNext())
                        builder.append(", ")
                }
            }
            return builder.toString()
        }

    }
}