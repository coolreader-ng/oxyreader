/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import android.view.View
import android.view.ViewGroup

fun ViewGroup.getViewAt(x: Float, y: Float): View? {
    var view: View? = null
    for (i in childCount - 1 downTo 0) {
        val child = getChildAt(i)
        val childX = child.x
        val childY = child.y
        if ((x >= childX) && (x < (childX + child.width))) {
            if ((y >= childY) && (y < (childY + child.height))) {
                view = if (child is ViewGroup) {
                    child.getViewAt(x - child.x, y - child.y)
                } else {
                    child
                }
                if (null != view)
                    break
            }
        }
    }
    return view
}
