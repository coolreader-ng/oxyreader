/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.RadioGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.DialogFragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import io.gitlab.coolreader_ng.project_s.extensions.inFullscreenWindow

class BookmarkEditDialogFragment(private var bookmark: Bookmark? = null) : DialogFragment() {

    interface OnActionsListener {
        fun onAddBookmark(bookmark: Bookmark)
        fun onRemoveBookmark(bookmark: Bookmark)
    }

    var onActionsListener: OnActionsListener? = null

    private var mShowAsDialog = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mShowAsDialog = arguments?.getBoolean("asDialog") ?: false
        val toolbarLocation =
            arguments?.getInt("toolbar", PropNames.App.TOOLBAR_LOCATION_NONE)
                ?: PropNames.App.TOOLBAR_LOCATION_NONE
        val isEditMode = arguments?.getBoolean("isEditMode") ?: false

        val contentView = inflater.inflate(R.layout.bookmark_edit, container, false)

        // For the "back" action to work
        contentView.requestFocus()

        val appBarLayout = contentView.findViewById<AppBarLayout>(R.id.appBarLayout)
        val topAppBar = contentView.findViewById<MaterialToolbar>(R.id.topAppBar)
        val nestedScrollView = contentView.findViewById<NestedScrollView>(R.id.nestedScrollView)
        val bottomAppBar = contentView.findViewById<BottomAppBar>(R.id.bottomAppBar)
        val rgBmType = contentView.findViewById<RadioGroup>(R.id.rgBmType)
        //val rbTypeComment = contentView.findViewById<RadioButton>(R.id.rbTypeComment)
        //val rbTypeCorrection = contentView.findViewById<RadioButton>(R.id.rbTypeCorrection)

        //val selectionTextLayout = contentView.findViewById<LinearLayout>(R.id.selectionTextLayout)
        val selectionTextEdit = contentView.findViewById<TextInputEditText>(R.id.selectionTextEdit)
        //val commentTextLayout = contentView.findViewById<TextInputLayout>(R.id.commentTextLayout)
        val commentTextEdit = contentView.findViewById<TextInputEditText>(R.id.commentTextEdit)

        val a: TypedArray = requireContext().obtainStyledAttributes(
            intArrayOf(android.R.attr.actionBarSize)
        )
        val bottomToolbarSize = a.getDimensionPixelSize(0, 0)
        a.recycle()

        if (isEditMode) {
            topAppBar.menu.findItem(R.id.add).apply { setIcon(R.drawable.ic_action_ok) }
            bottomAppBar.menu.findItem(R.id.add).apply { setIcon(R.drawable.ic_action_ok) }
        } else {
            topAppBar.menu.findItem(R.id.delete).apply { isVisible = false }
            bottomAppBar.menu.findItem(R.id.delete).apply { isVisible = false }
        }
        Utils.applyToolBarColorForMenu(requireContext(), topAppBar.menu)
        Utils.applyToolBarColorForMenu(requireContext(), bottomAppBar.menu)

        when (toolbarLocation) {
            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.VISIBLE
                nestedScrollView.setPadding(0, 0, 0, bottomToolbarSize)
            }

            else -> {
                // This fragment is completely useless without the toolbar.
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }
        }

        bookmark?.let { bm ->
            // Bookmark type status
            when (bm.type) {
                Bookmark.TYPE_COMMENT -> rgBmType.check(R.id.rbTypeComment)
                Bookmark.TYPE_CORRECTION -> rgBmType.check(R.id.rbTypeCorrection)
                else -> rgBmType.check(R.id.rbTypeComment)
            }
            // Selection text
            selectionTextEdit.setText(bm.posText)
            // Comment text
            commentTextEdit.setText(bm.commentText)
        }

        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val mask = if (inFullscreenWindow)
                WindowInsetsCompat.Type.displayCutout()
            else
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            val insets = windowInsets.getInsets(mask)
            var nsInsets = insets
            if (appBarLayout.isVisible) {
                topAppBar.updatePadding(left = insets.left, right = insets.right, top = insets.top)
                nsInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
            } else if (bottomAppBar.isVisible) {
                bottomAppBar.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    bottom = insets.bottom
                )
                nsInsets = Insets.of(
                    insets.left,
                    insets.top,
                    insets.right,
                    bottomToolbarSize + insets.bottom
                )
            }
            nestedScrollView.updatePadding(
                nsInsets.left,
                nsInsets.top,
                nsInsets.right,
                nsInsets.bottom
            )
            WindowInsetsCompat.CONSUMED
        }

        // Callbacks
        val closeAction: () -> Unit = {
            if (mShowAsDialog) {
                dialog?.dismiss()
            } else {
                if (parentFragmentManager.backStackEntryCount > 0)
                    parentFragmentManager.popBackStack()
            }
        }
        topAppBar.setNavigationOnClickListener {
            closeAction()
        }
        bottomAppBar.setNavigationOnClickListener {
            closeAction()
        }
        val onMenuItemClickListener = Toolbar.OnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.add -> {
                    bookmark?.let {
                        it.setCommentText(commentTextEdit.text?.toString())
                        onActionsListener?.onAddBookmark(it)
                    }
                    closeAction()
                    true
                }

                R.id.delete -> {
                    MaterialAlertDialogBuilder(requireContext())
                        .setTitle(requireContext().getString(R.string.confirmation))
                        .setMessage(requireContext().getString(R.string.are_you_sure_you_want_to_delete_these_bookmarks_))
                        .setNeutralButton(requireContext().getString(R.string.cancel), null)
                        .setPositiveButton(requireContext().getString(R.string.yes)) { _, _ ->
                            bookmark?.let { onActionsListener?.onRemoveBookmark(it) }
                            closeAction()
                        }
                        .show()
                    true
                }

                else -> false
            }
        }
        topAppBar.setOnMenuItemClickListener(onMenuItemClickListener)
        bottomAppBar.setOnMenuItemClickListener(onMenuItemClickListener)
        rgBmType.setOnCheckedChangeListener { _, checkedId ->
            val newBmType = when (checkedId) {
                R.id.rbTypeComment -> Bookmark.TYPE_COMMENT
                R.id.rbTypeCorrection -> Bookmark.TYPE_CORRECTION
                else -> Bookmark.TYPE_COMMENT
            }
            bookmark?.apply {
                if (newBmType != type) {
                    type = newBmType
                }
            }
        }

        return contentView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // Without Window.FEATURE_LEFT_ICON for some reason the bottom content of the dialog is cut off
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE or Window.FEATURE_LEFT_ICON)
        // Setting transparent background to remove padding
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (null == bookmark) {
            bookmark = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                savedInstanceState?.getParcelable("bookmark", Bookmark::class.java)
            else
                savedInstanceState?.getParcelable("bookmark")
        }
        // Note: onCreateView() function will be called later
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bookmark?.let { outState.putParcelable("bookmark", it) }
    }

    override fun onStart() {
        super.onStart()
        if (mShowAsDialog) {
            // Increase dialog window width
            dialog?.let { dialog ->
                val metrics = resources.displayMetrics
                val displayWidth = metrics.widthPixels
                val displayHeight = metrics.heightPixels
                val layoutParams = WindowManager.LayoutParams()
                val oldParams = dialog.window?.attributes
                if (null != oldParams)
                    layoutParams.copyFrom(dialog.window?.attributes)
                else
                    layoutParams.height = 8 * displayHeight / 10
                layoutParams.width = 8 * displayWidth / 10
                dialog.window?.attributes = layoutParams
            }
        }
    }

}