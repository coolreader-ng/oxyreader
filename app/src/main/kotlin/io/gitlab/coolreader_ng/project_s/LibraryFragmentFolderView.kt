/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener
import java.io.File
import java.io.Serial
import java.io.Serializable
import java.util.Stack

class LibraryFragmentFolderView(
    dbServiceAccessor: DBServiceAccessor? = null,
    bookCoverManager: BookCoverManager? = null,
    settings: SRProperties? = null
) : LibraryFragmentBase(dbServiceAccessor, bookCoverManager, settings), AbleBackwards {

    enum class FileSortType {
        SortNone,
        SortByName,
        SortByExt,
        SortBySize,
        SortByDate
    }

    data class FileSorting(
        var sortType: FileSortType = FileSortType.SortNone,
        var sortDesc: Boolean = false
    ) : Serializable {
        companion object {
            @Serial
            private const val serialVersionUID: Long = 5273955632552424626L
        }
    }

    private lateinit var mProgressView: View
    private lateinit var mProgressPopup: ProgressPopup
    private lateinit var mAdapter: LibraryFileListViewAdapter
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAppBarLayout: AppBarLayout
    private lateinit var mTopAppBar: MaterialToolbar
    private lateinit var mFabScrollToTop: FloatingActionButton
    private lateinit var mPermissionsStubView: ViewGroup
    private var mRecyclerViewScrollPoxX = 0
    private var mRecyclerViewScrollPoxY = 0
    private val mRecyclerViewPosStack = Stack<Point>()
    private lateinit var mStoragePermsResultLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var mAllFilesPermsResultLauncher: ActivityResultLauncher<Intent>

    private var mPath = ""
    private val mFileList: ArrayList<FileInfo> = ArrayList()
    private val mFileSorting = FileSorting(FileSortType.SortByName, sortDesc = false)
    private val mHandler = Handler(Looper.getMainLooper())

    /**
     * Called when the fragment needs to update its view.
     */
    override fun onUpdateViewImpl() {
        if (BuildConfig.ENABLE_INT_FM) {
            mPermissionsStubView.isVisible = !Utils.checkStoragePermissions(requireContext())
        } else {
            mPermissionsStubView.isVisible = false
        }
        updateFileList(false)
    }

    override fun onHideFragment() {
    }

    /**
     * Called by the activity when the user presses Back.
     * @return true if this fragment processed and consumed the event;
     *  false - if this fragment ignores this event and it must be handled in the next element (parent) of the chain.
     */
    override fun onBackPressed(): Boolean {
        return backToParentImpl()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val savedSorting = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            savedInstanceState?.getSerializable("sorting", FileSorting::class.java)
        else
            savedInstanceState?.getSerializable("sorting") as FileSorting?
        val savedPath: String? = savedInstanceState?.getString("path")
        savedSorting?.let {
            mFileSorting.sortType = it.sortType
            mFileSorting.sortDesc = it.sortDesc
        }
        if (null != savedPath) {
            mPath = savedPath
        } else {
            if (StorageEnumerator.storageEntries.isNotEmpty())
                mPath = StorageEnumerator.storageEntries.first().path
        }
        // Register a callback for request storage permission result
        mStoragePermsResultLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                // external storage read & write permissions
                val haveReadPerm =
                    it[Manifest.permission.READ_EXTERNAL_STORAGE]
                        ?: if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            PackageManager.PERMISSION_GRANTED == requireActivity().checkSelfPermission(
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                        else
                            true    // On API less than 23, this permission is always granted
                val haveWritePerm =
                    it[Manifest.permission.WRITE_EXTERNAL_STORAGE]
                        ?: if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                            PackageManager.PERMISSION_GRANTED == requireActivity().checkSelfPermission(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            )
                        else
                            true    // On API less than 23, this permission is always granted
                log.debug("onActivityResult(): haveReadPerm=$haveReadPerm, haveWritePerm=$haveWritePerm")
                if (haveReadPerm && haveWritePerm) {
                    log.info("read&write to storage permissions GRANTED")
                    requestFragmentUpdate()
                }
                if (!haveReadPerm) {
                    log.warn("read storage permission NOT granted, disabling internal FM!")
                    val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                    dialog.showDialog(requireContext())
                }
            }
        // Register a callback for request all files permissions result (API30+)
        mAllFilesPermsResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                val storagePerm = Utils.checkStoragePermissions(requireContext())
                log.debug("activity ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION returns; storagePerm=$storagePerm")
                if (storagePerm) {
                    log.info("All files access permissions GRANTED")
                    requestFragmentUpdate()
                } else {
                    log.warn("All files access permission NOT granted, disabling internal FM!")
                    val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                    dialog.showDialog(requireContext())
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mProgressView = inflater.inflate(R.layout.library_progress, container, false)
        return inflater.inflate(R.layout.library_fs, container, false)
    }

    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)

        mProgressPopup = ProgressPopup(contentView, mProgressView)

        mPermissionsStubView = contentView.findViewById(R.id.permissionsStubView)
        val btnRequestPerms = contentView.findViewById<Button>(R.id.btnRequestPerms)
        mAppBarLayout = contentView.findViewById(R.id.appBarLayout)
        mTopAppBar = contentView.findViewById(R.id.topAppBar)
        val swipeRefreshLayout =
            contentView.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout)
        mRecyclerView = contentView.findViewById(R.id.recyclerView)
        mFabScrollToTop = contentView.findViewById(R.id.fabScrollToTop)
        mAdapter = LibraryFileListViewAdapter(mFileList)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        // Add storage entries to submenu
        mTopAppBar.menu.findItem(R.id.select_storage)?.let {
            it.subMenu?.let { subMenu ->
                subMenu.clear()
                for ((i, entry) in StorageEnumerator.storageEntries.withIndex()) {
                    if (i < MENU_IDS_FS_STORAGE.size) {
                        val menuItem =
                            subMenu.add(0, MENU_IDS_FS_STORAGE[i], Menu.FIRST + i, entry.label)
                        if (mPath.startsWith(entry.path))
                            menuItem.setIcon(R.drawable.ic_emblem_check)
                        else
                            menuItem.setIcon(R.drawable.ic_emblem_empty)
                    } else {
                        log.debug("Too much storage device entries: size=${MENU_IDS_FS_STORAGE.size}")
                        break
                    }
                }
            }
        }
        Utils.applyToolBarColorForMenu(requireContext(), mTopAppBar.menu)
        updateSortingSubMenu()
        mRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                requireContext(),
                true
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val fileInfo =
                                if (index >= 0 && index < mFileList.size) mFileList[index] else null
                            fileInfo?.let { bi ->
                                if (bi.isDirectory) {
                                    // Save current scroll position to stack
                                    val scrollPos =
                                        Point(mRecyclerViewScrollPoxX, mRecyclerViewScrollPoxY)
                                    mRecyclerViewPosStack.push(scrollPos)
                                    // Change current path
                                    mPath = mPath + File.separator + bi.fileName
                                    updateFileList(false)
                                    mRecyclerViewScrollPoxX = 0
                                    mRecyclerViewScrollPoxY = 0
                                } else {
                                    // try to open book
                                    openBookInReaderViewImpl(bi)
                                }
                            }
                        }

                        override fun onLongPressed(index: Int) {
                        }
                    }
            })
        mRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                mRecyclerViewScrollPoxX += dx
                mRecyclerViewScrollPoxY += dy
                if (mRecyclerViewScrollPoxY > mRecyclerView.height / 3) {
                    if (mFabScrollToTop.isOrWillBeHidden)
                        mFabScrollToTop.show()
                } else {
                    if (mFabScrollToTop.isOrWillBeShown)
                        mFabScrollToTop.hide()
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
        swipeRefreshLayout.setOnRefreshListener {
            mHandler.postDelayed({
                updateFileList(false) { swipeRefreshLayout.isRefreshing = false }
            }, 100)
        }
        mTopAppBar.setNavigationOnClickListener {
            backToParentImpl()
        }
        mTopAppBar.setOnMenuItemClickListener { menuItem ->
            var processed: Boolean
            when (menuItem.itemId) {
                R.id.sort_by_name_asc -> {
                    mFileSorting.sortType = FileSortType.SortByName
                    mFileSorting.sortDesc = false
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_name_desc -> {
                    mFileSorting.sortType = FileSortType.SortByName
                    mFileSorting.sortDesc = true
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_type_asc -> {
                    mFileSorting.sortType = FileSortType.SortByExt
                    mFileSorting.sortDesc = false
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_type_desc -> {
                    mFileSorting.sortType = FileSortType.SortByExt
                    mFileSorting.sortDesc = true
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_size_asc -> {
                    mFileSorting.sortType = FileSortType.SortBySize
                    mFileSorting.sortDesc = false
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_size_desc -> {
                    mFileSorting.sortType = FileSortType.SortBySize
                    mFileSorting.sortDesc = true
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_date_asc -> {
                    mFileSorting.sortType = FileSortType.SortByDate
                    mFileSorting.sortDesc = false
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.sort_by_date_desc -> {
                    mFileSorting.sortType = FileSortType.SortByDate
                    mFileSorting.sortDesc = true
                    updateFileList(false)
                    updateSortingSubMenu()
                    processed = true
                }

                R.id.refresh -> {
                    updateFileList(false)
                    processed = true
                }

                else -> processed = false
            }
            if (!processed) {
                for (i in MENU_IDS_FS_STORAGE.indices) {
                    val id = MENU_IDS_FS_STORAGE[i]
                    if (menuItem.itemId == id) {
                        try {
                            val storagePath = StorageEnumerator.storageEntries[i].path
                            if (!mPath.startsWith(storagePath)) {
                                mPath = storagePath
                                updateFileList(false)
                                updateDeviceSubMenu()
                            }
                            processed = true
                        } catch (e: Exception) {
                            log.error("$e")
                        }
                        break
                    }
                }
            }
            return@setOnMenuItemClickListener processed
        }
        mFabScrollToTop.setOnClickListener {
            mAppBarLayout.setExpanded(true, true)
            mRecyclerView.smoothScrollBy(-mRecyclerViewScrollPoxX, -mRecyclerViewScrollPoxY)
        }
        btnRequestPerms.setOnClickListener {
            requestStoragePermissions()
        }

        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val mask = WindowInsetsCompat.Type.systemBars() or
                    WindowInsetsCompat.Type.displayCutout()
            val insets = windowInsets.getInsets(mask)
            mTopAppBar.updatePadding(
                left = insets.left,
                right = insets.right,
                top = insets.top
            )
            val rvInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
            mRecyclerView.updatePadding(
                rvInsets.left,
                rvInsets.top,
                rvInsets.right,
                rvInsets.bottom
            )
            WindowInsetsCompat.CONSUMED
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("path", mPath)
        outState.putSerializable("sorting", mFileSorting)
    }

    private fun showProgress() {
        mProgressPopup.isIndeterminate = true
        mProgressPopup.x = 0
        mProgressPopup.y = 0
        mProgressPopup.width = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.height = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.show()
    }

    private fun hideProgress() {
        mProgressPopup.hide()
    }

    private fun updateFileList(restoreScrollPos: Boolean, onComplete: (() -> Unit)? = null) {
        BackgroundThread.executeGUI {
            showProgress()
            // Get folder contents in background thread
            BackgroundThread.postBackground {
                crEngineNGBinding?.let { cre ->
                    val f = File(mPath)
                    val list = cre.listFiles(f)
                    val flist = ArrayList<FileInfo>()
                    list?.let {
                        // TODO: filter unsupported files (if requested)
                        for (item in it) {
                            if (null != item) {
                                flist.add(FileInfo(item))
                            }
                        }
                        flist.sortWith(FileInfoComparator(mFileSorting))
                    } ?: run {
                        log.debug("list is null")
                    }
                    BackgroundThread.postGUI {
                        var title = f.name
                        for (entry in StorageEnumerator.storageEntries) {
                            if (f.absolutePath == entry.path) {
                                title = entry.label
                                break
                            }
                        }
                        mTopAppBar.title = title
                        Utils.syncListByReference(mFileList, flist, mAdapter)
                        mAppBarLayout.setExpanded(true, true)
                        if (restoreScrollPos) {
                            if (mRecyclerViewPosStack.isNotEmpty()) {
                                val scrollPos = mRecyclerViewPosStack.pop()
                                mRecyclerView.scrollTo(0, 0)
                                mRecyclerView.smoothScrollBy(scrollPos.x, scrollPos.y)
                            }
                        } else {
                            if (mFabScrollToTop.isOrWillBeShown)
                                mFabScrollToTop.hide()
                            mRecyclerView.scrollToPosition(0)
                            mRecyclerViewScrollPoxX = 0
                            mRecyclerViewScrollPoxY = 0
                        }
                        hideProgress()
                        onComplete?.invoke()
                    }
                } ?: run {
                    log.error("crEngineNGBinding is null!")
                }
            }
        }
    }

    private fun backToParentImpl(): Boolean {
        val file = File(mPath)
        file.parentFile?.let {
            if (it.isDirectory && it.canRead() && it.canExecute()) {
                mPath = it.absolutePath
                updateFileList(true)
                updateDeviceSubMenu()
                return true
            }
        }
        return false
    }

    private fun openBookInReaderViewImpl(fileInfo: FileInfo) {
        val format = fileInfo.fileName?.let { DocumentFormat.byExtension(it) }
        val isSupportedFormat = format != null && format != DocumentFormat.NONE
        if (!fileInfo.isFileExists) {
            val dialog =
                MessageDialog(R.string.error, R.string.file_is_missing_it_may_have_been_deleted)
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else if (!fileInfo.isFileReadable) {
            val dialog = MessageDialog(
                R.string.error,
                R.string.file_is_unavailable_may_not_have_read_permissions
            )
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else if (!isSupportedFormat) {
            val dialog = MessageDialog(
                R.string.error,
                R.string.unfortunately_this_file_type_is_not_supported
            )
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else {
            // open book for reading
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.Builder().scheme("file").path(fileInfo.pathNameWA).build(),
                    context,
                    ReaderActivity::class.java
                ).apply {
                    setPackage(requireContext().packageName)
                })
            if (finishWhenOpeningBook)
                requireActivity().finish()
        }
    }

    private fun updateDeviceSubMenu() {
        val menu = mTopAppBar.menu.findItem(R.id.select_storage).subMenu
        menu?.let { subMenu ->
            for ((i, entry) in StorageEnumerator.storageEntries.withIndex()) {
                if (i < MENU_IDS_FS_STORAGE.size) {
                    val menuItem = subMenu.findItem(MENU_IDS_FS_STORAGE[i])
                    if (mPath.startsWith(entry.path))
                        menuItem.setIcon(R.drawable.ic_emblem_check)
                    else
                        menuItem.setIcon(R.drawable.ic_emblem_empty)
                } else {
                    log.debug("Too much storage device entries: size=${MENU_IDS_FS_STORAGE.size}")
                    break
                }
            }
            Utils.applyToolBarColorForMenu(requireContext(), subMenu)
        }
    }

    private fun updateSortingSubMenu() {
        val menuId = when (mFileSorting.sortType) {
            FileSortType.SortByName ->
                if (!mFileSorting.sortDesc) R.id.sort_by_name_asc else R.id.sort_by_name_desc

            FileSortType.SortByExt ->
                if (!mFileSorting.sortDesc) R.id.sort_by_type_asc else R.id.sort_by_type_desc

            FileSortType.SortBySize ->
                if (!mFileSorting.sortDesc) R.id.sort_by_size_asc else R.id.sort_by_size_desc

            FileSortType.SortByDate ->
                if (!mFileSorting.sortDesc) R.id.sort_by_date_asc else R.id.sort_by_date_desc

            else -> -1
        }
        mTopAppBar.menu.findItem(R.id.sort)?.subMenu?.findItem(menuId)?.setChecked(true)
    }

    private fun requestStoragePermissions() {
        // Check or request permission for external/shared storage
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // before API 23 (before Android 6.0)
            // already allowed after application installation
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            // API 23 (Android 6.0) - API 29 (Android 10)
            val readExtStoragePermissionCheck =
                requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            val writeExtStoragePermissionCheck =
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            val needPerms: ArrayList<String> = ArrayList()
            if (PackageManager.PERMISSION_GRANTED != readExtStoragePermissionCheck) {
                needPerms.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            } else {
                log.info("READ_EXTERNAL_STORAGE permission already granted.")
            }
            if (PackageManager.PERMISSION_GRANTED != writeExtStoragePermissionCheck) {
                needPerms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } else {
                log.info("WRITE_EXTERNAL_STORAGE permission already granted.")
            }
            if (needPerms.isNotEmpty()) {
                log.info("Some permissions DENIED, requesting from user these permissions: $needPerms")
                // request permission from user
                mStoragePermsResultLauncher.launch(needPerms.toTypedArray())
            }
        } else {
            // API 30+ (Android 11+)
            try {
                // https://developer.android.com/training/data-storage/manage-all-files
                mAllFilesPermsResultLauncher.launch(
                    Intent(
                        Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                        Uri.parse("package:${requireActivity().packageName}")
                    )
                )
            } catch (e: ActivityNotFoundException) {
                log.error("activity (MANAGE_ALL_FILES) not found", e)
                val dialog = MessageDialog(R.string.warning, R.string.cant_read_storage)
                dialog.showDialog(requireContext())
            }
        }
    }

    private class FileInfoComparator(private val fileSorting: FileSorting) : Comparator<FileInfo> {
        override fun compare(f1: FileInfo?, f2: FileInfo?): Int {
            if (null != f1 && null != f2) {
                if (f1.isDirectory && !f2.isDirectory)
                    return 1
                else if (!f1.isDirectory && f2.isDirectory)
                    return -1
                // Both directories or both files
                val f1Name = if (f1.isArchive) {
                    val f = File(f1.archiveName.toString())
                    f.name
                } else
                    f1.fileName.toString()
                val f2Name = if (f2.isArchive) {
                    val f = File(f2.archiveName.toString())
                    f.name
                } else
                    f2.fileName.toString()
                var compareRes = 0
                when (fileSorting.sortType) {
                    FileSortType.SortByName -> compareRes = f1Name.compareTo(f2Name)
                    FileSortType.SortNone -> compareRes = 0
                    FileSortType.SortByExt -> {
                        var f1Ext = ""
                        var f2Ext = ""
                        var pos = f1Name.lastIndexOf('.')
                        if (pos >= 0)
                            f1Ext = f1Name.substring(pos)
                        pos = f2Name.lastIndexOf('.')
                        if (pos >= 0)
                            f2Ext = f2Name.substring(pos)
                        compareRes = f1Ext.compareTo(f2Ext)
                        if (0 == compareRes)
                            compareRes = f1Name.compareTo(f2Name)
                    }

                    FileSortType.SortBySize -> {
                        compareRes = f1.size.compareTo(f2.size)
                        if (0 == compareRes)
                            compareRes = f1Name.compareTo(f2Name)
                    }

                    FileSortType.SortByDate -> {
                        compareRes = f1.modificationTime.compareTo(f2.modificationTime)
                        if (0 == compareRes)
                            compareRes = f1Name.compareTo(f2Name)
                    }
                }
                if (fileSorting.sortDesc)
                    compareRes = -compareRes
                return compareRes
            } else if (null == f1 && null == f2) {
                return 0
            } else if (null == f1) {
                return -1
            } else {
                return 1
            }
        }
    }

    companion object {
        private val log = SRLog.create("library.fs")
        private val MENU_IDS_FS_STORAGE = intArrayOf(
            R.id.fs_device1,
            R.id.fs_device2,
            R.id.fs_device3,
            R.id.fs_device4,
            R.id.fs_device5,
            R.id.fs_device6,
            R.id.fs_device7,
            R.id.fs_device8,
            R.id.fs_device9,
            R.id.fs_device10,
        )
    }
}