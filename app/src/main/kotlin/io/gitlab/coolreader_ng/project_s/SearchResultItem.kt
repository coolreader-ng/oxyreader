/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

@UsedInJNI
class SearchResultItem() {
    @UsedInJNI
    var title = ""

    @UsedInJNI
    var textFragment = ""

    @UsedInJNI
    var startPtr = ""

    @UsedInJNI
    var endPtr = ""

    @UsedInJNI
    var offset = -1

    @UsedInJNI
    var length = -1

    @UsedInJNI
    var percent = -1

    @UsedInJNI
    constructor(
        title: String,
        textFragment: String,
        startPtr: String,
        endPtr: String,
        offset: Int,
        length: Int,
        percent: Int
    ) : this() {
        this.title = title
        this.textFragment = textFragment
        this.startPtr = startPtr
        this.endPtr = endPtr
        this.offset = offset
        this.length = length
        this.percent = percent
    }
}
