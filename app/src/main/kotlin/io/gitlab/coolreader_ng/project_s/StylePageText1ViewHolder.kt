/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import com.google.android.material.materialswitch.MaterialSwitch
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import io.gitlab.coolreader_ng.project_s.extensions.getCurrentSelectedPosition
import io.gitlab.coolreader_ng.project_s.extensions.setCurrentSelectedItem
import io.gitlab.coolreader_ng.project_s.extensions.setSimpleItems
import java.text.NumberFormat
import kotlin.math.roundToInt


class StylePageText1ViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private data class FontWeightValue(
        var weight: Int = 400,
        var label: String = "",
        var isSynthetic: Boolean = false
    )

    private val mFontFaceEdit: MaterialAutoCompleteTextView?
    private val mFontFaceSpinner: Spinner?
    private val mFontSizeSlider: Slider
    private val mFontSizeValueView: TextView
    private val mBtnFontSizeDec: Button
    private val mBtnFontSizeInc: Button
    private val mFontWeightEdit: MaterialAutoCompleteTextView?
    private val mFontWeightSpinner: Spinner?
    private val mKerningSwitch: MaterialSwitch
    private val mHangPuncSwitch: MaterialSwitch
    private var mFontWeightValues: Array<FontWeightValue>
    private val mNumberFormat: NumberFormat

    private fun getWeightName(weight: Int): String {
        return when (weight) {
            100 -> context.getString(R.string.font_weight_thin)
            200 -> context.getString(R.string.font_weight_extralight)
            300 -> context.getString(R.string.font_weight_light)
            350 -> context.getString(R.string.font_weight_book)
            400 -> context.getString(R.string.font_weight_regular)
            500 -> context.getString(R.string.font_weight_medium)
            600 -> context.getString(R.string.font_weight_semibold)
            700 -> context.getString(R.string.font_weight_bold)
            800 -> context.getString(R.string.font_weight_extrabold)
            900 -> context.getString(R.string.font_weight_black)
            950 -> context.getString(R.string.font_weight_extrablack)
            else -> weight.toString()
        }
    }

    private fun getFontWeightVariants(faceName: String): Array<FontWeightValue> {
        val list = ArrayList<FontWeightValue>()
        // get available weight for font faceName
        val synthWeights = CREngineNGBinding.getAvailableSynthFontWeight()
        val nativeWeights = CREngineNGBinding.getAvailableFontWeight(faceName)
        if (nativeWeights.isEmpty()) {
            // Invalid font
            // Every font has at least one weight
            return list.toTypedArray()
        }
        // combine with synthetic weights
        val weights = ArrayList<Int>()
        var synthIdx = 0
        var prevWeight = 0
        for (weight in nativeWeights) {
            var j = synthIdx
            while (j < synthWeights.count()) {
                val synthWeight = synthWeights[j]
                if (synthWeight < weight) {
                    if (synthWeight > prevWeight)
                        weights.add(synthWeight)
                } else
                    break
                j++
            }
            synthIdx = j
            weights.add(weight)
            prevWeight = weight
        }
        for (j in synthIdx until synthWeights.count()) {
            if (synthWeights[j] > nativeWeights.last())
                weights.add(synthWeights[j])
        }
        // fill items
        for (weight in weights) {
            val item = FontWeightValue(weight = weight)
            var label = item.weight.toString()
            var descr = getWeightName(item.weight)
            item.isSynthetic = !nativeWeights.contains(item.weight)
            if (item.isSynthetic) {
                if (descr.isNotEmpty())
                    descr += ", " + context.getString(R.string.font_weight_fake)
                else
                    descr = context.getString(R.string.font_weight_fake)
                item.isSynthetic = true
            }
            if (descr.isNotEmpty())
                label += " ($descr)"
            item.label = label
            list.add(item)
        }
        return list.toTypedArray()
    }

    override fun onUpdateViewImpl() {
        // Font face
        val fontFace = mProps.getProperty(PropNames.Engine.PROP_FONT_FACE, "")
        // On layout for API26+
        mFontFaceEdit?.setCurrentSelectedItem(fontFace)
        // On layout for API less than 26
        mFontFaceSpinner?.setCurrentSelectedItem(fontFace)

        // Font size
        val fontSizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
        val fontSizePt =
            Utils.convertUnits(fontSizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
        mFontSizeValueView.text =
            context.getString(R.string.format_font_size_str_pt, mNumberFormat.format(fontSizePt))
        var sliderValue = fontSizePt
        if (sliderValue < MIN_FONT_SIZE_PT)
            sliderValue = MIN_FONT_SIZE_PT
        else if (sliderValue > MAX_FONT_SIZE_PT)
            sliderValue = MAX_FONT_SIZE_PT
        mFontSizeSlider.value = sliderValue
        mBtnFontSizeDec.isEnabled = fontSizePt > MIN_FONT_SIZE_PT
        mBtnFontSizeInc.isEnabled = fontSizePt < MAX_FONT_SIZE_PT

        // Font weight (bold,  etc)
        val fontWeight = mProps.getInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, 400)
        mFontWeightValues = getFontWeightVariants(fontFace)
        val fontWeightLabelsList = ArrayList<String>()
        var fontWeightIdx = -1
        var fontWeightRegularIdx = -1
        for ((i, weight) in mFontWeightValues.withIndex()) {
            fontWeightLabelsList.add(weight.label)
            if (400 == weight.weight)
                fontWeightRegularIdx = i
            if (fontWeight == weight.weight)
                fontWeightIdx = i
        }
        if (fontWeightIdx < 0)
            fontWeightIdx = fontWeightRegularIdx
        val fontWeightLabels = fontWeightLabelsList.toTypedArray()
        // On layout for API26+
        mFontWeightEdit?.setSimpleItems(fontWeightLabels)
        mFontWeightEdit?.setCurrentSelectedItem(fontWeightIdx)
        // On layout for API less than 26
        mFontWeightSpinner?.setSimpleItems(fontWeightLabels)
        mFontWeightSpinner?.setSelection(fontWeightIdx)

        // Kerning
        mKerningSwitch.isChecked = mProps.getBool(PropNames.Engine.PROP_FONT_KERNING_ENABLED, true)

        // Hanging punctuation
        mHangPuncSwitch.isChecked = mProps.getBool(PropNames.Engine.PROP_FLOATING_PUNCTUATION, true)
    }

    override fun onResetViewImpl() {
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // On layout for API26+
            mFontFaceEdit = itemView.findViewById(R.id.fontFaceEdit)
            mFontWeightEdit = itemView.findViewById(R.id.fontWeightEdit)
            mFontFaceSpinner = null
            mFontWeightSpinner = null
        } else {
            // On layout for API less than 26
            mFontFaceEdit = null
            mFontWeightEdit = null
            mFontFaceSpinner = itemView.findViewById(R.id.fontFaceSpinner)
            mFontWeightSpinner = itemView.findViewById(R.id.fontWeightSpinner)
        }
        mFontSizeSlider = itemView.findViewById(R.id.fontSizeSlider)
        mFontSizeValueView = itemView.findViewById(R.id.fontSizeValueView)
        mBtnFontSizeDec = itemView.findViewById(R.id.btnFontSizeDec)
        mBtnFontSizeInc = itemView.findViewById(R.id.btnFontSizeInc)
        mKerningSwitch = itemView.findViewById(R.id.kerningSwitch)
        mHangPuncSwitch = itemView.findViewById(R.id.hangPuncSwitch)

        mNumberFormat = NumberFormat.getNumberInstance(SettingsManager.activeLang.locale)
        mNumberFormat.minimumFractionDigits = 0
        mNumberFormat.maximumFractionDigits = 1

        // Font faces
        val engineAcc = ReaderEngineObjectsAccessor.leaseEngineObjects()
        val fontFaces = engineAcc.crEngineNGBinding?.getFontFaceList()
        ReaderEngineObjectsAccessor.releaseEngineObject(engineAcc)
        fontFaces?.let {
            mFontFaceEdit?.setSimpleItems(it)
            mFontFaceSpinner?.setSimpleItems(it)
        }
        // On layout for API26+
        mFontFaceEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    val prevFontFace = mProps.getProperty(PropNames.Engine.PROP_FONT_FACE, "")
                    if (prevFontFace != it.toString()) {
                        mProps.setProperty(PropNames.Engine.PROP_FONT_FACE, it.toString())
                        commitChanges()
                    }
                }
            }
        })
        // On layout for API less than 26
        mFontFaceSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevFontFace = mProps.getProperty(PropNames.Engine.PROP_FONT_FACE, "")
                val fontFace = parent?.adapter?.getItem(position)
                if (prevFontFace != fontFace.toString()) {
                    mProps.setProperty(PropNames.Engine.PROP_FONT_FACE, fontFace.toString())
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Font size
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed (when slider is using on PopupWindow)
            // TODO: Test on other API, new material design library (possible fixed)
            mFontSizeSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mFontSizeSlider.setLabelFormatter { value ->
                context.getString(R.string.format_font_size_str_pt, mNumberFormat.format(value))
            }
        }
        mFontSizeSlider.valueFrom = MIN_FONT_SIZE_PT
        mFontSizeSlider.valueTo = MAX_FONT_SIZE_PT
        mFontSizeSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value
                val valuePx = Utils.convertUnits(value, from = Utils.Units.PT, to = Utils.Units.PX)
                    .roundToInt()
                val prevValuePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
                if (prevValuePx != valuePx) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_SIZE, valuePx)
                    commitChanges()
                }
            }
        })
        mBtnFontSizeDec.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(12f, from = Utils.Units.PT, to = Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt > MIN_FONT_SIZE_PT) {
                sizePx--
                mProps.setInt(PropNames.Engine.PROP_FONT_SIZE, sizePx)
                commitChanges()
            }
        }
        mBtnFontSizeInc.setOnClickListener {
            var sizePx = mProps.getInt(PropNames.Engine.PROP_FONT_SIZE, -1)
            if (-1 == sizePx)
                sizePx = Utils.convertUnits(12f, Utils.Units.PT, Utils.Units.PX).toInt()
            val sizePt =
                Utils.convertUnits(sizePx.toFloat(), from = Utils.Units.PX, to = Utils.Units.PT)
            if (sizePt < MAX_FONT_SIZE_PT) {
                sizePx++
                mProps.setInt(PropNames.Engine.PROP_FONT_SIZE, sizePx)
                commitChanges()
            }
        }

        // Font weight (bold, etc)
        val fontFace = mProps.getProperty(PropNames.Engine.PROP_FONT_FACE, "")
        mFontWeightValues = getFontWeightVariants(fontFace)
        val fontWeightLabelsList = ArrayList<String>()
        for (weight in mFontWeightValues)
            fontWeightLabelsList.add(weight.label)
        val fontWeightLabels = fontWeightLabelsList.toTypedArray()
        // On layout for API26+
        mFontWeightEdit?.setSimpleItems(fontWeightLabels)
        mFontWeightEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevWeight = mProps.getInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, 400)
                val idx = mFontWeightEdit.getCurrentSelectedPosition()
                val newWeight = if (idx >= 0 && idx < mFontWeightValues.size)
                    mFontWeightValues[idx].weight
                else
                    400
                if (prevWeight != newWeight) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, newWeight)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mFontWeightSpinner?.setSimpleItems(fontWeightLabels)
        mFontWeightSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevWeight = mProps.getInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, 400)
                val newWeight = if (position >= 0 && position < mFontWeightValues.size)
                    mFontWeightValues[position].weight
                else
                    400
                if (prevWeight != newWeight) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_BASE_WEIGHT, newWeight)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Kerning
        mKerningSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_FONT_KERNING_ENABLED, isChecked)
            commitChanges()
        }

        // Hanging punctuation
        mHangPuncSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_FLOATING_PUNCTUATION, isChecked)
            commitChanges()
        }
    }

    companion object {
        const val MIN_FONT_SIZE_PT = 3f
        const val MAX_FONT_SIZE_PT = 30f
    }
}