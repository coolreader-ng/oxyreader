/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

@UsedInJNI
enum class DocumentFormat(
    val cssName: String,
    val cssResourceId: Int,
    val iconResourceId: Int,
    val canParseProperties: Boolean,
    val canParseCoverpages: Boolean,
    val priority: Int,
    val extensions: Array<String>,
    val mimeFormats: Array<String>
) {
    /// crbookformats.h: source document formats
    // Add new types of formats only at the end of this enum to save the correct format number in the history file/database!
    NONE(
        "fb2.css",
        R.raw.fb2,
        R.mipmap.cr3_browser_book,
        false,
        false,
        0,
        arrayOf(),
        arrayOf()
    ),  // doc_format_none,
    FB2(
        "fb2.css",
        R.raw.fb2,
        R.mipmap.cr3_browser_book_fb2,
        true,
        true,
        13,
        arrayOf(".fb2", ".fb2.zip"),
        arrayOf(
            "application/x-fictionbook",
            "application/x-fictionbook+xml",
            "application/fb2",
            "application/fb2.zip",
            "application/fb2+zip"
        )
    ),  // doc_format_fb2,
    FB3(
        "fb3.css",
        R.raw.fb3,
        R.mipmap.cr3_browser_book_fb3,
        true,
        true,
        12,
        arrayOf(".fb3"),
        arrayOf("application/fb3")
    ),  // doc_format_fb3,
    TXT(
        "txt.css",
        R.raw.txt,
        R.mipmap.cr3_browser_book_txt,
        false,
        false,
        3,
        arrayOf(".txt", ".tcr", ".pml"),
        arrayOf("text/plain")
    ),  // doc_format_txt,
    RTF(
        "rtf.css",
        R.raw.rtf,
        R.mipmap.cr3_browser_book_rtf,
        false,
        false,
        8,
        arrayOf(".rtf"),
        arrayOf("text/rtf", "text/richtext", "application/x-rtf")
    ),  // doc_format_rtf,
    EPUB(
        "epub.css",
        R.raw.epub,
        R.mipmap.cr3_browser_book_epub,
        true,
        true,
        11,
        arrayOf(".epub"),
        arrayOf("application/epub", "application/epub+zip")
    ),  // doc_format_epub,
    HTML(
        "htm.css",
        R.raw.htm,
        R.mipmap.cr3_browser_book_html,
        false,
        false,
        10,
        arrayOf(".htm", ".html", ".shtml", ".xhtml"),
        arrayOf("text/html")
    ),  // doc_format_html,
    TXT_BOOKMARK(
        "fb2.css",
        R.raw.fb2,
        R.mipmap.cr3_browser_book_fb2,
        false,
        false,
        0,
        arrayOf(".txt.bmk"),
        arrayOf()
    ),  // doc_format_txt_bookmark, // coolreader TXT format bookmark
    CHM(
        "chm.css",
        R.raw.chm,
        R.mipmap.cr3_browser_book_chm,
        false,
        false,
        4,
        arrayOf(".chm"),
        arrayOf("application/vnd.ms-htmlhelp", "application/x-chm")
    ),  //  doc_format_chm,
    DOC(
        "doc.css",
        R.raw.doc,
        R.mipmap.cr3_browser_book_doc,
        false,
        false,
        5,
        arrayOf(".doc"),
        arrayOf("application/msword", "application/vnd.ms-word")
    ),  // doc_format_doc,
    DOCX(
        "docx.css",
        R.raw.docx,
        R.mipmap.cr3_browser_book_doc,
        true,
        false,
        6,
        arrayOf(".docx"),
        arrayOf("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
    ),  // doc_format_docx,
    PDB(
        "htm.css",
        R.raw.htm,
        R.mipmap.cr3_browser_book_pdb,
        false,
        true,
        2,
        arrayOf(".pdb", ".prc", ".mobi", ".azw"),
        arrayOf(
            "application/vnd.palm",
            "application/x-pilot-prc",
            "application/x-mobipocket-ebook",
            "application/vnd.amazon.mobi8-ebook"
        )
    ),  // doc_format_pdb,
    ODT(
        "docx.css",
        R.raw.docx,
        R.mipmap.cr3_browser_book_odt,
        true,
        false,
        7,
        arrayOf(".odt"),
        arrayOf("application/vnd.oasis.opendocument.text")
    ),  // doc_format_odt,
    MD(
        "markdown.css",
        R.raw.markdown,
        R.mipmap.cr3_browser_book_html,
        false,
        false,
        9,
        arrayOf(".md", ".mkd", ".markdown"),
        arrayOf("text/markdown")
    );  // doc_format_md,
    // don't forget update getDocFormatName() when changing this enum
    // Add new types of formats only at the end of this enum to save the correct format number in the history file/database!
    //} doc_format_t;

    val mimeFormat: String?
        get() = if (mimeFormats.isNotEmpty()) mimeFormats[0] else null

    fun needCoverPageCaching(): Boolean {
        return this == FB2
    }

    fun matchExtension(filename: String): Boolean {
        for (ext in extensions) {
            if (filename.endsWith(ext))
                return true
        }
        return false
    }

    fun matchMimeType(type: String): Boolean {
        for (s in mimeFormats) {
            if (type == s || type.startsWith("$s;"))
                return true
        }
        return false
    }

    companion object {
        @UsedInJNI
        @JvmStatic
        fun byId(i: Int): DocumentFormat {
            if (i >= 0 && i < entries.size)
                return entries[i]
            return NONE
        }

        @JvmStatic
        fun byExtension(filename: String): DocumentFormat? {
            val s = filename.lowercase()
            for (format in entries) {
                if (format.matchExtension(s))
                    return format
            }
            return null
        }

        @JvmStatic
        fun byMimeType(mime: String?): DocumentFormat? {
            if (mime == null)
                return null
            val s = mime.lowercase()
            for (format in entries) {
                if (format.matchMimeType(s))
                    return format
            }
            return null
        }

        @JvmStatic
        fun getSupportedExtension(filename: String): String? {
            val s = filename.lowercase()
            for (format in entries) {
                for (ext in format.extensions) {
                    if (s.endsWith(ext))
                        return ext
                }
            }
            return null
        }
    }
}