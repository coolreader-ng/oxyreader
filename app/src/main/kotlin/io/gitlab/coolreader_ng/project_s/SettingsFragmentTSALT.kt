/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Bundle
import androidx.annotation.Keep
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat

// Long Tap (LT) touch screen actions (TSA)
@Keep
class SettingsFragmentTSALT : PreferenceFragmentCompat(),
    SettingsActivity.SettingsFragmentStorageHolder {

    private var mProperties = SRProperties()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = SettingsActivity.CREPropsDataStorage(mProperties)

        setPreferencesFromResource(R.xml.preferences_tsa_lt, rootKey)

        // actions names & values
        val actionNames = arrayOfNulls<String>(ReaderActionRegistry.AVAILABLE_ACTIONS.size)
        val actionIds = arrayOfNulls<String>(ReaderActionRegistry.AVAILABLE_ACTIONS.size)
        for ((i, action) in ReaderActionRegistry.AVAILABLE_ACTIONS.withIndex()) {
            actionNames[i] = requireContext().getString(action.nameResId)
            actionIds[i] = action.id
        }

        val action0Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_0)
        action0Preference?.entries = actionNames
        action0Preference?.entryValues = actionIds
        val action1Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_1)
        action1Preference?.entries = actionNames
        action1Preference?.entryValues = actionIds
        val action2Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_2)
        action2Preference?.entries = actionNames
        action2Preference?.entryValues = actionIds
        val action3Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_3)
        action3Preference?.entries = actionNames
        action3Preference?.entryValues = actionIds
        val action4Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_4)
        action4Preference?.entries = actionNames
        action4Preference?.entryValues = actionIds
        val action5Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_5)
        action5Preference?.entries = actionNames
        action5Preference?.entryValues = actionIds
        val action6Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_6)
        action6Preference?.entries = actionNames
        action6Preference?.entryValues = actionIds
        val action7Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_7)
        action7Preference?.entries = actionNames
        action7Preference?.entryValues = actionIds
        val action8Preference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_TSA_LT_8)
        action8Preference?.entries = actionNames
        action8Preference?.entryValues = actionIds
    }

    override fun setProperties(props: SRProperties) {
        mProperties = props
    }

    override fun resetToDefaults() {
        for ((key, value) in SettingsManager.Defaults.TAP_ACTIONS_LT) {
            mProperties.setProperty(key, value)
        }
    }
}