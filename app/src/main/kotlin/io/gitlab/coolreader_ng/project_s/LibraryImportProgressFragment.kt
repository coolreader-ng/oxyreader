/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import com.google.android.material.progressindicator.LinearProgressIndicator
import io.gitlab.coolreader_ng.project_s.library.LibraryProcessingAccessor
import io.gitlab.coolreader_ng.project_s.library.LibraryProcessingBinder

class LibraryImportProgressFragment() : Fragment() {

    private class UiHolder {
        lateinit var tvTaskName: TextView
        lateinit var tvProgress: TextView
        lateinit var importProgress: LinearProgressIndicator
        lateinit var tvProcessingFile: TextView
        lateinit var tvProcessed: TextView
        lateinit var tvSkipped: TextView
        lateinit var btnStopExit: Button
        lateinit var tvStatusDone: TextView
        lateinit var tvErrorText: TextView
    }

    private var mImportState = LibraryProcessingBinder.ProcessStatus()
    private var uiHolder: UiHolder? = null
    private var mLibraryProcessingAccessor: LibraryProcessingAccessor? = null
    private var mSavedOrientation: Int = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    private var mIsOrientationLocked = false

    private val mProgressCallback = object : LibraryProcessingBinder.ProcessingProgressCallback {
        override fun onStarted() {
            uiHolder?.apply {
                btnStopExit.setText(R.string.stop)
                tvStatusDone.visibility = View.GONE
            }
            if (!mIsOrientationLocked)
                lockScreenOrientation()
        }

        override fun onItemProcessing(item: String) {
            uiHolder?.tvProcessingFile?.text = item
        }

        override fun onItemProcessed(item: String, status: LibraryProcessingBinder.ItemStatus) {
            // TODO:
        }

        override fun onProgress(
            percent: Int,
            addedCount: Int,
            skippedCount: Int,
            totalFiles: Int
        ) {
            uiHolder?.apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    importProgress.setProgress(percent, true)
                else
                    importProgress.progress = percent
                tvProgress.text = getString(R.string.progress_, percent)
                tvProcessed.text =
                    getString(R.string.processed_____files, addedCount + skippedCount, totalFiles)
                tvSkipped.text = getString(R.string.of_these___files_are_skipped, skippedCount)
            }
        }

        override fun onDone() {
            mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
                override fun run(binder: LibraryProcessingBinder) {
                    mImportState = binder.getProcessStatus()
                    BackgroundThread.postGUI {
                        statusChanged()
                    }
                }
            })
        }

        override fun onError(errorStatus: LibraryProcessingBinder.ErrorStatus) {
            showErrorStatus(errorStatus)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mLibraryProcessingAccessor = LibraryProcessingAccessor(requireContext())
        mLibraryProcessingAccessor?.bind()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.library_import_progress, container, false)
    }

    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)
        uiHolder = UiHolder().apply {
            tvTaskName = contentView.findViewById(R.id.tvTaskName)
            tvProgress = contentView.findViewById(R.id.tvProgress)
            importProgress = contentView.findViewById(R.id.importProgress)
            tvProcessingFile = contentView.findViewById(R.id.tvProcessingFile)
            tvProcessed = contentView.findViewById(R.id.tvProcessed)
            tvSkipped = contentView.findViewById(R.id.tvSkipped)
            btnStopExit = contentView.findViewById(R.id.btnStopExit)
            tvStatusDone = contentView.findViewById(R.id.tvStatusDone)
            tvErrorText = contentView.findViewById(R.id.tvErrorText)
            btnStopExit.setOnClickListener {
                if (mImportState.completed) {
                    if (parentFragmentManager.backStackEntryCount > 0)
                        parentFragmentManager.popBackStack()
                } else {
                    mLibraryProcessingAccessor?.runWithService(object :
                        LibraryProcessingAccessor.Callback {
                        override fun run(binder: LibraryProcessingBinder) {
                            binder.requestStop()
                        }
                    })
                }
            }
        }
        ViewCompat.setOnApplyWindowInsetsListener(contentView) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(insets.left, insets.top, insets.right, insets.bottom)
            WindowInsetsCompat.CONSUMED
        }
    }

    override fun onResume() {
        super.onResume()
        val handler = Handler(Looper.myLooper()!!)
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                mImportState = binder.getProcessStatus()
                // TODO: check if process is started
                //  if not - wait & check again (several attempts)
                BackgroundThread.postGUI {
                    statusChanged()
                }
                binder.registerProgressCallback(mProgressCallback, handler)
            }
        })
    }

    override fun onPause() {
        mLibraryProcessingAccessor?.runWithService(object : LibraryProcessingAccessor.Callback {
            override fun run(binder: LibraryProcessingBinder) {
                binder.unregisterProgressCallback()
            }
        })
        unlockScreenOrientation()
        super.onPause()
    }

    override fun onDestroy() {
        mLibraryProcessingAccessor?.unbind()
        super.onDestroy()
    }

    private fun lockScreenOrientation() {
        if (!mIsOrientationLocked) {
            // Save current screen orientation settings
            mSavedOrientation = Utils.getScreenOrientation(requireActivity())
            // Get actual screen orientation
            val orientation = resources.configuration.orientation
            Utils.applyScreenOrientation(requireActivity(), orientation)
            mIsOrientationLocked = true
        }
    }

    private fun unlockScreenOrientation() {
        if (mIsOrientationLocked) {
            Utils.applyScreenOrientation(requireActivity(), mSavedOrientation)
            mIsOrientationLocked = false
        }
    }

    private fun statusChanged() {
        uiHolder?.apply {
            when (mImportState.lastOperation) {
                LibraryProcessingBinder.OperationType.None -> tvTaskName.text = ""
                LibraryProcessingBinder.OperationType.FolderCopy,
                LibraryProcessingBinder.OperationType.FileCopy -> tvTaskName.setText(R.string.copy_process)

                LibraryProcessingBinder.OperationType.FolderLink -> tvTaskName.setText(R.string.indexing_process)
                LibraryProcessingBinder.OperationType.ImportFonts -> tvTaskName.setText(R.string.copy_fonts)
            }
            mImportState.apply {
                if (completed) {
                    tvStatusDone.visibility = View.VISIBLE
                    btnStopExit.setText(R.string.action_exit)
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    importProgress.setProgress(percent, true)
                else
                    importProgress.progress = percent
                tvProgress.text = getString(R.string.progress_, percent)
                tvProcessed.text =
                    getString(R.string.processed_____files, addedCount + skippedCount, totalFiles)
                tvSkipped.text = getString(R.string.of_these___files_are_skipped, skippedCount)
                tvProcessingFile.text = currentItem
                showErrorStatus(lastError)
                if (started) {
                    if (!mIsOrientationLocked)
                        lockScreenOrientation()
                } else {
                    if (mIsOrientationLocked)
                        unlockScreenOrientation()
                }
            }
        }
    }

    private fun showErrorStatus(errorStatus: LibraryProcessingBinder.ErrorStatus?) {
        uiHolder?.apply {
            if (null == errorStatus) {
                tvErrorText.visibility = View.GONE
            } else {
                when (errorStatus) {
                    LibraryProcessingBinder.ErrorStatus.None ->
                        tvErrorText.visibility = View.GONE

                    LibraryProcessingBinder.ErrorStatus.Busy -> {
                        tvErrorText.visibility = View.VISIBLE
                        tvErrorText.setText(R.string.busy)
                    }

                    LibraryProcessingBinder.ErrorStatus.NoPermissions -> {
                        tvErrorText.visibility = View.VISIBLE
                        tvErrorText.setText(R.string.no_read_permissions)
                    }

                    LibraryProcessingBinder.ErrorStatus.Canceled -> {
                        tvErrorText.visibility = View.VISIBLE
                        tvErrorText.setText(R.string.canceled)
                    }

                    LibraryProcessingBinder.ErrorStatus.StreamFailed -> {
                        tvErrorText.visibility = View.VISIBLE
                        tvErrorText.setText(R.string.failed_to_get_object_contents)
                    }

                    LibraryProcessingBinder.ErrorStatus.ReadDataFailed -> {
                        tvErrorText.visibility = View.VISIBLE
                        tvErrorText.setText(R.string.failed_to_get_object_contents)
                    }
                }
            }
        }
    }
}