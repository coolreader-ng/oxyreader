/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.core.graphics.Insets
import androidx.core.os.LocaleListCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.extensions.reduce
import java.io.File

class LibraryActivity : AppCompatActivity() {

    private var mCurrentThemeResId = -1
    private var mSettingsManager: SettingsManager? = null
    private val settings: SRProperties
        get() {
            return mSettingsManager?.properties ?: SRProperties()
        }
    private lateinit var mMainLayout: View
    private lateinit var mViewPager: ViewPager2
    private lateinit var mTabLayout: TabLayout
    private val mTabFragments: HashMap<Int, LibraryFragmentBase> = HashMap()
    private var mActiveLibraryFragment: LibraryFragmentBase? = null
    private var mActiveLibraryFragmentPos = 0
    private val mDBServiceAccessor = DBServiceAccessor(this)
    private var mEngineObjects: ReaderEngineObjectsAccessor.EngineObjects? = null
    private var mFinishWhenOpeningBook = false
    private val mAppSettingsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.getStringExtra("action")
            log.debug("received action: $action")
            when (action) {
                "finish" -> {
                    log.debug("finish this activity")
                    this@LibraryActivity.finish()
                }
            }
        }
    }

    private inner class LibraryPagerAdapter(
        fa: FragmentActivity,
        private val dbServiceAccessor: DBServiceAccessor,
        private val bookCoverManager: BookCoverManager?
    ) : FragmentStateAdapter(fa) {
        override fun getItemCount(): Int = TABS_COUNT
        override fun createFragment(position: Int): Fragment {
            val fragment = when (position) {
                0 -> LibraryFragmentRecent(dbServiceAccessor, bookCoverManager, settings)
                1 -> LibraryFragmentContents(dbServiceAccessor, bookCoverManager, settings)
                2 -> LibraryFragmentImport(dbServiceAccessor, bookCoverManager, settings)
                3 -> LibraryFragmentFolderView(dbServiceAccessor, bookCoverManager, settings)
                else -> throw IllegalArgumentException("Invalid position $position")
            }
            // Save the fragment's position in the fragment itself
            fragment.customTag = position.toString()
            fragment.finishWhenOpeningBook = mFinishWhenOpeningBook
            mTabFragments[position] = fragment
            if (position == mActiveLibraryFragmentPos)
                mActiveLibraryFragment = fragment
            return fragment
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
            // Reassign *major* fragment properties
            // They are lost when an activity is destroyed and then recreated,
            //  since fragments are saved differently
            // And we cannot save these properties in the onSaveInstanceState() fragment function,
            //  since they are not parcelable
            // Here (and now) the fragments have already been restored, but their properties have not.
            // (They are recreated using the default constructor in Activity.onCreate() from savedInstanceState)
            // Also here we must restore the contents of mTabFragments
            // Note: onBindViewHolder() will be called *after* this function
            val libraryFragments =
                supportFragmentManager.fragments.filterIsInstance<LibraryFragmentBase>()
            for (fragment in libraryFragments) {
                val position = fragment.customTag?.toString()?.toInt()
                position?.let { mTabFragments[it] = fragment }
                if (null == fragment.dbServiceAccessor)
                    fragment.dbServiceAccessor = dbServiceAccessor
                if (null == fragment.bookCoverManager)
                    fragment.bookCoverManager = bookCoverManager
                if (null == fragment.settings)
                    fragment.settings = settings
            }
        }
    }

    override fun setTheme(resId: Int) {
        super.setTheme(resId)
        mCurrentThemeResId = resId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        log.debug("onCreate()")
        savedInstanceState?.let {
            mFinishWhenOpeningBook = it.getBoolean("finishWhenOpeningBook", false)
        }
        var pageNo = -1
        intent?.let {
            pageNo = it.getIntExtra("pageNo", -1)
            mFinishWhenOpeningBook = it.getBooleanExtra("finishWhenOpeningBook", false)
        }
        mSettingsManager = SettingsManager(this, object : SettingsManager.OnSettingsChangeListener {
            override fun onSettingsChanged(props: SRProperties, oldProps: SRProperties?) {
                // Firstly apply this activity options
                val changedProps = if (oldProps != null) props.diff(oldProps) else props
                for (prop in changedProps) {
                    onAppSettingChanged(prop.key.toString(), prop.value.toString())
                }
            }
        })
        val themeCode = mSettingsManager?.properties?.getProperty(PropNames.App.THEME)
        themeCode?.let { setActivityTheme(it) }
        val ifaceLangTag = mSettingsManager?.properties?.getProperty(PropNames.App.LOCALE)
        ifaceLangTag?.let { setLanguage(it) }

        enableEdgeToEdge()

        // Only after theme & language is set we can init UI
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_library)

        Utils.retrieveAndUseDPI(this)

        // bind to DBService
        mDBServiceAccessor.bind()

        StorageEnumerator.scan(this)
        for (entry in StorageEnumerator.storageEntries)
            log.verbose("storage entry: $entry")
        // get engine objects
        val engineObjects = ReaderEngineObjectsAccessor.leaseEngineObjects()
        // init objects this activity
        engineObjects.crEngineNGBinding?.setupEngineData(this)
        // Register non-system fonts in external storage
        for (entry in StorageEnumerator.storageEntries) {
            engineObjects.crEngineNGBinding?.initFontsFromDir(File(entry.path, "fonts"))
        }
        mEngineObjects = engineObjects
        val bookCoverManager = mEngineObjects?.crEngineNGBinding?.let {
            BookCoverManager(
                this,
                mDBServiceAccessor,
                it
            )
        }

        mMainLayout = findViewById(R.id.mainLayout)
        val adapter = LibraryPagerAdapter(this, mDBServiceAccessor, bookCoverManager)
        mViewPager = findViewById(R.id.viewPager)
        mTabLayout = findViewById(R.id.tabLayout)
        mViewPager.adapter = adapter
        TabLayoutMediator(
            mTabLayout, mViewPager
        ) { tab, position ->
            tab.text = when (position) {
                0 -> getString(R.string.tab_recent)  // tab "Recent"
                1 -> getString(R.string.tab_library) // tab "Library"
                2 -> getString(R.string.tab_import)  // tab "Import"
                3 -> getString(R.string.tab_filesystem)  // tab "Folder"
                else -> throw IllegalArgumentException("Invalid position $position")
            }
        }.attach()
        mViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                val prevActiveFragment = mActiveLibraryFragment
                mActiveLibraryFragmentPos = position
                if (position in 0..<TABS_COUNT)
                    mActiveLibraryFragment = mTabFragments[position]
                if (prevActiveFragment != mActiveLibraryFragment)
                    prevActiveFragment?.onHideFragment()
            }
        })
        if (pageNo >= 0)
            mViewPager.currentItem = pageNo

        updateSystemBars()
        ViewCompat.setOnApplyWindowInsetsListener(mMainLayout) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            val consumedInsets = Insets.of(0, 0, 0, insets.bottom)
            mTabLayout.updatePadding(
                left = insets.left,
                right = insets.right,
                bottom = insets.bottom
            )
            windowInsets.reduce(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout(),
                consumedInsets
            )
        }

        // Register OnBackPressedCallback
        onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    var processed = false
                    mActiveLibraryFragment?.let {
                        if (it is AbleBackwards)
                            processed = it.onBackPressed()
                    }
                    if (!processed) {
                        if (supportFragmentManager.backStackEntryCount > 0) {
                            supportFragmentManager.popBackStack()
                        } else {
                            this@LibraryActivity.finish()
                        }
                    }
                }
            }
        )

        // Register broadcast receiver for ReaderActivity
        ContextCompat.registerReceiver(
            this,
            mAppSettingsReceiver,
            IntentFilter("${ReaderActivity::class.qualifiedName}.action"),
            ContextCompat.RECEIVER_NOT_EXPORTED
        )
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("finishWhenOpeningBook", mFinishWhenOpeningBook)
    }

    override fun onResume() {
        super.onResume()
        mSettingsManager?.reloadSettings(true)
    }

    override fun onDestroy() {
        log.debug("onDestroy()")
        super.onDestroy()
        unregisterReceiver(mAppSettingsReceiver)
        mDBServiceAccessor.unbind()
        ReaderEngineObjectsAccessor.releaseEngineObject(mEngineObjects)
    }

    private fun onAppSettingChanged(key: String, value: String) {
        when (key) {
            PropNames.App.THEME -> setActivityTheme(value)
            PropNames.App.LOCALE -> setLanguage(value)
            // TODO: other library options
        }
    }

    private fun setLanguage(langName: String) {
        val lang = try {
            SettingsManager.Lang.valueOf(langName)
        } catch (e: Exception) {
            SettingsManager.Lang.SYSTEM
        }
        setLanguage(lang)
    }

    private fun setLanguage(lang: SettingsManager.Lang) {
        if (lang != SettingsManager.activeLang) {
            log.debug("setLanguage(): lang=${lang}")
            val locales =
                if (lang == SettingsManager.Lang.SYSTEM)
                    LocaleListCompat.getEmptyLocaleList()
                else
                    LocaleListCompat.create(lang.locale)
            AppCompatDelegate.setApplicationLocales(locales)
            SettingsManager.activeLang = lang
            GenresCollection.reloadGenresFromResource(this)
        } else {
            log.debug("  ...skipping since this language already applied")
        }
    }

    private fun setActivityTheme(themeCode: String) {
        var theme = try {
            SettingsManager.Theme.valueOf(themeCode)
        } catch (e: Exception) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                SettingsManager.Theme.DAYNIGHT
            else
                SettingsManager.Theme.LIGHT
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (theme == SettingsManager.Theme.DAYNIGHT)
                theme = SettingsManager.Theme.LIGHT
        }
        log.debug("setAppTheme(): theme=${theme}")
        if (mCurrentThemeResId != theme.themeId) {
            setTheme(theme.themeId)
        } else {
            log.debug("  ...skipping since this theme already applied")
        }
    }

    @SuppressLint("ResourceType")
    private fun updateSystemBars() {
        var windowLightStatusBar = false
        var windowLightNavigationBar = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(android.R.attr.windowLightStatusBar)
                )
                windowLightStatusBar = attrs.getBoolean(0, true)
                attrs.recycle()
            } else {
                // Force the status bar color to be black because the light background color
                // doesn't match the light color of the indicators, which we can't change.
                windowLightStatusBar = false
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(android.R.attr.windowLightNavigationBar)
                )
                windowLightNavigationBar = attrs.getBoolean(0, true)
                attrs.recycle()
            } else {
                windowLightNavigationBar = false
            }
            window.statusBarColor = Color.TRANSPARENT
        }
        val ctrl = WindowCompat.getInsetsController(window, mMainLayout)
        ctrl.isAppearanceLightStatusBars = windowLightStatusBar
        ctrl.isAppearanceLightNavigationBars = windowLightNavigationBar
    }

    companion object {
        val TABS_COUNT = if (BuildConfig.ENABLE_INT_FM) 4 else 3
        private val log = SRLog.create("library")
    }
}