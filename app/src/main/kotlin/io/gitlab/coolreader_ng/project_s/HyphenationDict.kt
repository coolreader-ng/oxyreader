/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import java.io.File

@UsedInJNI
data class HyphenationDict(
    @UsedInJNI
    val code: String,
    @UsedInJNI
    val type: Int,
    @UsedInJNI
    val resourceId: Int,
    @UsedInJNI
    val name: String,       // Human readable language name (English)
    @UsedInJNI
    val languages: Array<LanguageItem>,
    @UsedInJNI
    val isHidden: Boolean
) {
    // Skip this property in auto generated methods
    @UsedInJNI
    var file: File? = null
        private set

    constructor(code: String, type: Int, resourceId: Int, name: String, language: LanguageItem) :
            this(code, type, resourceId, name, arrayOf(language), false)

    constructor(
        code: String,
        type: Int,
        resourceId: Int,
        name: String,
        language1: LanguageItem,
        language2: LanguageItem
    ) :
            this(code, type, resourceId, name, arrayOf(language1, language2), false)

    constructor(f: File) :
            this(f.name, HYPH_DICT, 0, f.name, arrayOf(LanguagesRegistry.LANG_UND), false) {
        file = f
    }

    companion object {
        const val HYPH_NONE = 0
        const val HYPH_ALGO = 1
        const val HYPH_SOFTHYPHENS = 2
        const val HYPH_DICT = 3
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as HyphenationDict

        if (code != other.code) return false
        if (type != other.type) return false
        if (resourceId != other.resourceId) return false
        if (name != other.name) return false
        if (!languages.contentEquals(other.languages)) return false
        if (isHidden != other.isHidden) return false
        if (file != other.file) return false

        return true
    }

    override fun hashCode(): Int {
        var result = code.hashCode()
        result = 31 * result + type
        result = 31 * result + resourceId
        result = 31 * result + name.hashCode()
        result = 31 * result + languages.contentHashCode()
        result = 31 * result + isHidden.hashCode()
        result = 31 * result + (file?.hashCode() ?: 0)
        return result
    }
}
