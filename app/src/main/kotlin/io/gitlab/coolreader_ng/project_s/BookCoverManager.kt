/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ColorFilter
import android.graphics.PixelFormat
import android.graphics.Rect
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.content.res.ResourcesCompat
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import java.io.ByteArrayOutputStream
import java.io.IOException

class BookCoverManager(
    private val context: Context,
    private val dbServiceAccessor: DBServiceAccessor,
    internal val crEngineNGBinding: CREngineNGBinding
) {

    interface BookCoverDrawableResultCallback {
        fun onResult(drawable: Drawable?)
    }

    private class UnavailableDrawable(
        context: Context,
        private val baseDrawable: Drawable,
        overlayResId: Int
    ) : Drawable() {
        private val mOverlayDrawable: Drawable?

        override fun draw(canvas: Canvas) {
            baseDrawable.draw(canvas)
            mOverlayDrawable?.draw(canvas)
        }

        override fun setBounds(bounds: Rect) {
            super.setBounds(bounds)
            baseDrawable.bounds = bounds
            mOverlayDrawable?.bounds = bounds
        }

        override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
            super.setBounds(left, top, right, bottom)
            baseDrawable.bounds = bounds
            mOverlayDrawable?.bounds = bounds
        }

        override fun setAlpha(alpha: Int) {
        }

        override fun setColorFilter(colorFilter: ColorFilter?) {
        }

        @Deprecated("Deprecated in Java")
        override fun getOpacity(): Int {
            return PixelFormat.OPAQUE
        }

        init {
            mOverlayDrawable = ResourcesCompat.getDrawable(context.resources, overlayResId, null)
        }
    }

    val fontFace = "Droid Sans"

    /**
     * Get book cover as drawable.
     * If no book is found for this file, or the file cannot be processed, the callback is called with null as the drawable.
     */
    fun getBookCoverDrawable(
        bookFilePath: String,
        maxWidth: Int,
        maxHeight: Int,
        resultCallback: BookCoverDrawableResultCallback
    ) {
        // Find book in DB to get metadata (or fetch it from real book by engine)
        dbServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.findBooksByPathName(bookFilePath, object : DBService.BookSearchCallback {
                    override fun onBooksFound(books: Collection<BookInfo>?) {
                        var bookInfo: BookInfo? = null
                        books?.let {
                            val iter = it.iterator()
                            if (iter.hasNext())
                                bookInfo = iter.next()
                        }
                        if (null == bookInfo) {
                            // No such book in DB
                            // Retrieve metadata for book at this path from engine
                            //  and save to DB
                            BackgroundThread.postBackground {
                                val newBookInfo = crEngineNGBinding.getBookInfo(bookFilePath)
                                if (null != newBookInfo) {
                                    getBookCoverDrawable(
                                        newBookInfo,
                                        maxWidth,
                                        maxHeight,
                                        resultCallback
                                    )
                                    dbServiceAccessor.runWithService(object :
                                        DBServiceAccessor.Callback {
                                        override fun run(binder: DBServiceBinder) {
                                            binder.saveBookInfo(newBookInfo)
                                        }
                                    })
                                } else {
                                    resultCallback.onResult(null)
                                }
                            }
                        } else {
                            getBookCoverDrawable(bookInfo!!, maxWidth, maxHeight, resultCallback)
                        }
                    }
                })
            }
        })
    }

    fun getBookCoverDrawable(
        bookInfo: BookInfo,
        maxWidth: Int,
        maxHeight: Int,
        resultCallback: BookCoverDrawableResultCallback
    ) {
        getBookCoverImageData(
            bookInfo,
            maxWidth,
            maxHeight,
            object : DBService.BookCoverDataResultCallback {
                override fun onResult(data: ByteArray?) {
                    if (null != data) {
                        BackgroundThread.postBackground {
                            val bitmap = crEngineNGBinding.drawBookCover(
                                data,
                                maxWidth,
                                maxHeight,
                                true,
                                null,
                                null,
                                null,
                                null,
                                0,
                                16
                            )
                            if (null != bitmap) {
                                log.debug("Successfully drawing cover on bitmap")
                                var drawable: Drawable = BitmapDrawable(context.resources, bitmap)
                                if (!bookInfo.isBookFileAvailable)
                                    drawable = UnavailableDrawable(
                                        context,
                                        drawable,
                                        R.drawable.overlay_book_unavailable
                                    )
                                resultCallback.onResult(drawable)
                            } else {
                                log.error("Failed to draw cover on bitmap for book $bookInfo")
                                resultCallback.onResult(null)
                            }
                        }
                    } else {
                        log.error("Failed obtain/generate cover for book $bookInfo")
                        resultCallback.onResult(null)
                    }
                }
            })
    }

    fun getBookCoverImageData(
        bookInfo: BookInfo,
        maxWidth: Int,
        maxHeight: Int,
        result: DBService.BookCoverDataResultCallback
    ) {
        if (bookInfo.fileInfo.pathNameWA != null) {
            dbServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getBookCoverData(
                        bookInfo,
                        object : DBService.BookCoverDataResultCallback {
                            override fun onResult(data: ByteArray?) {
                                if (null == data) {
                                    log.debug("Book cover not found in database")
                                    // Not found in DB, fetch cover from file
                                    BackgroundThread.postBackground {
                                        var imgData =
                                            crEngineNGBinding.getBookCoverData(bookInfo.fileInfo.pathNameWA!!)
                                        if (null == imgData) {
                                            // there is no cover in the book file or it could not be obtained
                                            // make image & draw to the image book name, author, etc.
                                            log.debug("Book without cover or cover can't be obtained, generating from metadata...")
                                            var titleOrFileName = ""
                                            if (!bookInfo.title.isNullOrEmpty())
                                                titleOrFileName = bookInfo.title!!
                                            if (bookInfo.authors.isNullOrEmpty() && bookInfo.series.isNullOrEmpty())
                                                bookInfo.fileInfo.fileName?.let {
                                                    titleOrFileName = it
                                                }
                                            val authors = bookInfo.authorsToString(", ")
                                            val bitmap = crEngineNGBinding.drawBookCover(
                                                null,
                                                maxWidth,
                                                maxHeight,
                                                true,
                                                fontFace,
                                                titleOrFileName,
                                                authors,
                                                bookInfo.series,
                                                bookInfo.seriesNumber ?: 0,
                                                16
                                            )
                                            if (null != bitmap) {
                                                try {
                                                    val outputStream = ByteArrayOutputStream()
                                                    // param quality ignored for Bitmap.CompressFormat.PNG
                                                    val res = bitmap.compress(
                                                        Bitmap.CompressFormat.PNG,
                                                        0,
                                                        outputStream
                                                    )
                                                    if (res) {
                                                        // copy binary data from byte array stream to separate byte array
                                                        imgData = outputStream.toByteArray()
                                                    } else {
                                                        log.error("Failed to compress generated book cover to png!")
                                                    }
                                                    outputStream.close()
                                                } catch (e: IOException) {
                                                    log.error("I/O error: $e")
                                                }
                                            }
                                        }
                                        if (null != imgData)
                                            binder.saveBookCoverData(bookInfo, imgData, null)
                                        result.onResult(imgData)
                                    }
                                } else {
                                    // Found in DB
                                    log.debug("Book cover found in DB")
                                    result.onResult(data)
                                }
                            }
                        })
                }
            })
        } else {
            throw RuntimeException("postBookCoverDataQuery(): pathNameWA can't be null!")
        }
    }

    companion object {
        private val log = SRLog.create("bcover")
    }
}
