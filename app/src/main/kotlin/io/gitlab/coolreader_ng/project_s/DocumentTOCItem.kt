/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

@UsedInJNI
class DocumentTOCItem {

    // Main toc properties, filled in JNI
    @UsedInJNI
    val level = 0

    @UsedInJNI
    val page = 0

    @UsedInJNI
    val percent = 0

    @UsedInJNI
    val name: String? = null

    @UsedInJNI
    val path: String? = null

    // index at its parent
    internal var index = 0
        private set

    // parent toc item
    internal var parent: DocumentTOCItem? = null
        private set

    // Children toc items, if any
    private var mChildren: ArrayList<DocumentTOCItem>? = null

    val hasChildren: Boolean
        get() {
            mChildren?.let { return it.size > 0 } ?: return false
        }

    val children: List<DocumentTOCItem>?
        get() = mChildren

    val nextChapter: DocumentTOCItem?
        get() {
            if (parent == null)
                return null
            val parent = parent!!
            val neighbours = parent.children
            if (null != neighbours) {
                // TODO: use `index` field instead?
                val idx = neighbours.indexOf(this)
                if (idx >= 0 && idx < neighbours.size - 1)
                    return neighbours[idx + 1]
                return parent.nextChapter
            }
            return null
        }

    // creates an empty child item and adds it, used in JNI
    @UsedInJNI
    fun addEmptyChild(): DocumentTOCItem {
        if (mChildren == null)
            mChildren = ArrayList()
        val item = DocumentTOCItem()
        item.parent = this
        item.index = mChildren!!.size
        mChildren!!.add(item)
        return item
    }

    /**
     * @param page page number (starting from 0)
     */
    fun getChapterAtPage(page: Int): DocumentTOCItem? {
        if (hasChildren) {
            val count = mChildren?.size ?: 0
            for (i in count - 1 downTo 0) {
                val currChapter = mChildren?.get(i)
                if (null != currChapter && currChapter.page <= page) {
                    return if (currChapter.hasChildren)
                        currChapter.getChapterAtPage(page)
                    else
                        currChapter
                }
            }
        }
        return this
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = level.hashCode()
        result = prime * result + page.hashCode()
        result = prime * result + percent.hashCode()
        result = prime * result + (name?.hashCode() ?: 0)
        result = prime * result + (path?.hashCode() ?: 0)
        result = prime * result + index.hashCode()
        // do not account `parent` field when calculating hash
        result = prime * result + (mChildren?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "TOC[${path};${name};page=${page + 1}]"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DocumentTOCItem

        // Since level, page, percent, name, path are filled in JNI then
        //  IDE can say something like this: condition 'level != other.level' is always false
        //  this is a false warning
        if (level != other.level) return false
        if (page != other.page) return false
        if (percent != other.percent) return false
        if (name != other.name) return false
        if (path != other.path) return false
        if (index != other.index) return false
        if (mChildren != other.mChildren) return false

        return true
    }
}
