/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import android.widget.ArrayAdapter
import android.widget.Spinner

fun Spinner.setCurrentSelectedItem(text: String) {
    this.adapter?.let {
        var idx = -1
        for (i in 0 until it.count) {
            val item = it.getItem(i)
            if (text == item.toString()) {
                idx = i
                break
            }
        }
        if (idx >= 0)
            this.setSelection(idx)
    }
}

fun Spinner.setSimpleItems(values: Array<String>) {
    val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, values)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    this.adapter = adapter
}

fun Spinner.setSimpleItems(resId: Int) {
    val adapter =
        ArrayAdapter.createFromResource(context, resId, android.R.layout.simple_spinner_item)
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    this.adapter = adapter
}
