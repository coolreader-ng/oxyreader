/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import com.google.android.material.button.MaterialButton
import io.gitlab.coolreader_ng.myflexrowlayout.MyFlexRowLayout
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import kotlin.math.roundToInt

class ReaderMenuPopup(private val parent: View, contentView: View) {
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mPopup: PopupWindow
    private val context = contentView.context
    private var mButtonsGroup: MyFlexRowLayout

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null

    fun show() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
        mButtonsGroup.reset()
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    private fun buildActionButton(context: Context, action: ReaderAction): Button {
        val button =
            MaterialButton(context, null, com.google.android.material.R.attr.borderlessButtonStyle)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            button.textAlignment = View.TEXT_ALIGNMENT_CENTER
        // Get interface language
        val text = context.getString(action.nameResId)
        button.setHyphenatedText(SettingsManager.activeLang.langTag, text)
        val iconResId = action.iconId
        if (iconResId != 0) {
            button.setIconResource(iconResId)
            button.iconGravity = MaterialButton.ICON_GRAVITY_TOP
        }
        button.maxWidth =
            Utils.convertUnits(110f, from = Utils.Units.DP, to = Utils.Units.PX).roundToInt()
        val btnPadding =
            Utils.convertUnits(8f, from = Utils.Units.DP, to = Utils.Units.PX).roundToInt()
        button.setPadding(btnPadding, btnPadding, btnPadding, btnPadding)
        val attrs = intArrayOf(com.google.android.material.R.attr.colorPrimary)
        val a = context.obtainStyledAttributes(attrs)
        val colorPrimary = a.getColorStateList(0)
        a.recycle()
        colorPrimary?.let {
            button.iconTint = it
            button.setTextColor(it)
        }
        button.setOnClickListener {
            if (parent is ReaderView) {
                parent.processReaderAction(action)
            }
            hide()
        }
        return button
    }

    private fun addActionButton(group: ViewGroup, action: ReaderAction) {
        val button = buildActionButton(group.context, action)
        val layoutParams = MarginLayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        group.addView(button, layoutParams)
    }

    init {
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.BOTTOM)
            mPopup.exitTransition = Slide(Gravity.BOTTOM)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = true
        mPopup.isTouchable = true
        mPopup.isFocusable = false
        val glassView = contentView.findViewById<View>(R.id.glass)
        glassView.setOnClickListener { hide() }

        mButtonsGroup = contentView.findViewById(R.id.flexbox)

        ViewCompat.setOnApplyWindowInsetsListener(mButtonsGroup) { v, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(left = insets.left, right = insets.right, bottom = insets.bottom)
            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }

        val moreButton = buildActionButton(context, ReaderActionRegistry.MENU_MORE)
        moreButton.visibility = View.GONE
        mButtonsGroup.goNextButton = moreButton

        addActionButton(mButtonsGroup, ReaderActionRegistry.GO_BACK)
        addActionButton(mButtonsGroup, ReaderActionRegistry.NEW_BOOKMARK_PAGE)
        addActionButton(mButtonsGroup, ReaderActionRegistry.BOOK_INFO)
        addActionButton(mButtonsGroup, ReaderActionRegistry.TTS_PLAY)
        addActionButton(mButtonsGroup, ReaderActionRegistry.TOC)
        addActionButton(mButtonsGroup, ReaderActionRegistry.BOOKMARKS)
        addActionButton(mButtonsGroup, ReaderActionRegistry.SHOW_GO_PANEL)
        addActionButton(mButtonsGroup, ReaderActionRegistry.STYLE)
        addActionButton(mButtonsGroup, ReaderActionRegistry.START_SELECTION)
        addActionButton(mButtonsGroup, ReaderActionRegistry.SEARCH)
        // TODO: add "OPDS", etc
        addActionButton(mButtonsGroup, ReaderActionRegistry.OPTIONS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            addActionButton(mButtonsGroup, ReaderActionRegistry.TOGGLE_FULLSCREEN)
        addActionButton(mButtonsGroup, ReaderActionRegistry.OPEN_BOOK)
        addActionButton(mButtonsGroup, ReaderActionRegistry.RECENT_BOOKS)
        addActionButton(mButtonsGroup, ReaderActionRegistry.LIBRARY)
        addActionButton(mButtonsGroup, ReaderActionRegistry.ABOUT)
        addActionButton(mButtonsGroup, ReaderActionRegistry.EXIT)
    }
}
