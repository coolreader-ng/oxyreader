/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.db

import android.content.Intent
import android.database.SQLException
import android.os.Handler
import android.os.IBinder
import io.gitlab.coolreader_ng.project_s.AbstractService
import io.gitlab.coolreader_ng.project_s.AbstractTask
import io.gitlab.coolreader_ng.project_s.BookInfo
import io.gitlab.coolreader_ng.project_s.FileInfo
import io.gitlab.coolreader_ng.project_s.SRLog
import java.io.Serial
import java.io.Serializable


class DBService : AbstractService("db") {

    interface BookInfoLoadingCallback {
        fun onBooksInfoLoaded(bookInfo: BookInfo?)
    }

    interface BooleanResultCallback {
        fun onResult(result: Boolean)
    }

    interface StringResultCallback {
        fun onResult(result: String)
    }

    interface BookSearchCallback {
        fun onBooksFound(books: Collection<BookInfo>?)
    }

    interface BookCoverDataResultCallback {
        fun onResult(data: ByteArray?)
    }

    interface BooksLoadingCallback {
        fun onBooksListLoaded(books: Collection<BookInfo>?)
    }

    interface HandbookLoadingCallback {
        fun onHandbookLoaded(handbook: Handbook?)
    }

    interface SearchHistoryLoadingCallback {
        fun onHistoryLoaded(history: Collection<SearchHistoryItem>?)
    }

    interface BooksOperationCallback {
        fun onBooksProcessed(result: Map<BookInfo, Boolean>?)
    }

    enum class BookSortType {
        SortNone,
        SortByAuthor,
        SortByTitle,
        SortBySeries,
        SortByLastReadTime
    }

    enum class BookFilterType {
        FilterAuthor,
        FilterAuthorId,
        FilterSeries,
        FilterSeriesId,
        FilterGenreId,
        FilterKeyword,
        FilterKeywordId,
        FilterLanguage,
        FilterStatus,
        FilterRating
    }

    enum class HandbookType {
        HandbookAuthor,
        HandbookSeries,
        HandbookReferencedGenre,
        HandbookKeywords,
        HandbookUnsupportedGenres,
        HandbookLanguage,
        HandbookDirectories
    }

    data class BooksSorting(
        var sortType: BookSortType = BookSortType.SortNone,
        var sortDesc: Boolean = false
    ) : Serializable {
        companion object {
            @Serial
            private const val serialVersionUID: Long = -4841719078921000591L
        }
    }

    data class BookFilter(
        var filterField: BookFilterType,
        var filterValue: String
    )

    class BookFilters(initialCapacity: Int) : ArrayList<BookFilter>(initialCapacity) {
        constructor() : this(6)

        fun copy(): BookFilters {
            val list = BookFilters(size)
            for (filter in this)
                list.add(filter.copy())
            return list
        }

        fun replace(other: BookFilters) {
            clear()
            for (filter in other) {
                add(filter)
            }
        }

        companion object {
            @Serial
            private const val serialVersionUID: Long = 6043610339827755817L
        }
    }

    data class SearchHistoryItem(
        var timeStamp: Long,
        var searchQuery: String,
        var caseSensitivity: Boolean = false,
        var wholeWords: Boolean = false
    )

    private val mLocker = Any()
    private var mDBHelper: DBOpenHelper? = null
    private var mDB: DBWrapper? = null
    private var mOpened = false

    // Binder given to clients
    private val mBinder = DBServiceBinder(this)

    override fun onCreate() {
        super.onCreate()
        log.debug("database service created")
        mDBHelper = DBOpenHelper(this)
        openDatabase()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // If the system kills the service after onStartCommand() returns, recreate the service and call onStartCommand(),
        //  but do not redeliver the last intent.
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onDestroy() {
        log.info("DBService.onDestroy()")
        closeDatabase()
        super.onDestroy()
    }

    fun runOn(runnable: Runnable) {
        postRunnable(runnable)
    }

    // ======================================
    // this service implementation functions

    private fun openDatabase() {
        postTask(object : AbstractTask("openDatabaseTask") {
            override fun work() {
                synchronized(mLocker) {
                    try {
                        val db = mDBHelper!!.writableDatabase
                        mDB = DBWrapper(db, this@DBService)
                        mOpened = true
                    } catch (e: SQLException) {
                        log.error("Failed top open database", e)
                        mDB = null
                        mOpened = false
                    }
                }
            }
        })
    }

    private fun closeDatabase() {
        postTask(object : AbstractTask("closeDatabaseTask") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.close()
                    mDB = null
                    mOpened = false
                }
            }
        })
    }

    // =======================================================================
    // access functions for this service (wrappers)
    // Can be called from any thread
    // =======================================================================

    internal fun loadBookInfo(
        fileInfo: FileInfo,
        callback: BookInfoLoadingCallback,
        callerHandler: Handler
    ) {
        var bookInfo: BookInfo? = null
        postTask(object : AbstractTask("loadBookInfo") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { bookInfo = it.loadBookInfo(fileInfo) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("loadBookInfo.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksInfoLoaded(bookInfo) }
            }
        })
    }

    internal fun saveBookInfo(
        bookInfo: BookInfo,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var result = false
        postTask(object : AbstractTask("saveBookInfo") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { result = it.saveBookInfo(bookInfo) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveBookInfo.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(result) }
                }
            })
        }
    }

    internal fun saveMultipleBookInfo(
        books: Collection<BookInfo>,
        callback: BooksOperationCallback?,
        callerHandler: Handler
    ) {
        var result: HashMap<BookInfo, Boolean>? = null
        postTask(object : AbstractTask("saveMultipleBookInfo") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let {
                        if (books.isNotEmpty()) {
                            val resultMap = HashMap<BookInfo, Boolean>(books.size)
                            for (book in books) {
                                resultMap[book] = it.saveBookInfo(book)
                            }
                            result = resultMap
                        }
                    } ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveMultipleBookInfo.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onBooksProcessed(result) }
                }
            })
        }
    }

    /**
     * Find books by fingerprint or by book metadata (if fingerprint not specified)
     * @param bookInfo book metadata
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    internal fun findBooks(
        bookInfo: BookInfo,
        callback: BookSearchCallback,
        callerHandler: Handler
    ) {
        var books: Collection<BookInfo>? = null
        postTask(object : AbstractTask("findBooks") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { books = it.findBooks(bookInfo) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("findBooks.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksFound(books) }
            }
        })
    }

    /**
     * Find books by path
     * @param path full path to book
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    internal fun findBooksByPathName(
        path: String,
        callback: BookSearchCallback,
        callerHandler: Handler
    ) {
        var books: Collection<BookInfo>? = null
        postTask(object : AbstractTask("findBooksByPathName") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { books = it.findBooksByPathName(path) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("findBooksByPathName.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksFound(books) }
            }
        })
    }

    /**
     * Find books by fingerprint
     * @param fingerprint fingerprint to search
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be null.
     */
    internal fun findBooksByFingerprint(
        fingerprint: String,
        callback: BookSearchCallback,
        callerHandler: Handler
    ) {
        var books: Collection<BookInfo>? = null
        postTask(object : AbstractTask("findBooksByFingerprint") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { books = it.findBooksByFingerprint(fingerprint) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("findBooksByFingerprint.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksFound(books) }
            }
        })
    }

    /**
     * Get book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @param callback callback to run when cover data is loaded
     * @param callerHandler handler to run callback
     */
    internal fun getBookCoverData(
        bookInfo: BookInfo,
        callback: BookCoverDataResultCallback,
        callerHandler: Handler
    ) {
        var data: ByteArray? = null
        postTask(object : AbstractTask("getBookCoverData") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { data = it.getBookCoverData(bookInfo) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("getBookCoverData.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onResult(data) }
            }
        })
    }

    /**
     * Save book cover image data
     * @param bookInfo book information for which to get the book cover image
     * @param data book cover image data to save
     * @param callback callback to run when cover data is saved
     * @param callerHandler handler to run callback
     */
    internal fun saveBookCoverData(
        bookInfo: BookInfo,
        data: ByteArray?,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("saveBookCoverData") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.saveBookCoverData(bookInfo, data) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveBookCoverData.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Saving book cover image data for multiple books
     * @param covers image data map for multiple books
     * @param callback callback to run when cover data is saved
     * @param callerHandler handler to run callback
     */
    internal fun saveMultipleBookCoverData(
        covers: Map<BookInfo, ByteArray?>,
        callback: BooksOperationCallback?,
        callerHandler: Handler
    ) {
        var result: HashMap<BookInfo, Boolean>? = null
        postTask(object : AbstractTask("saveMultipleBookCoverData") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let {
                        if (covers.isNotEmpty()) {
                            val resultMap = HashMap<BookInfo, Boolean>(covers.size)
                            for ((bi, coverData) in covers) {
                                resultMap[bi] = it.saveBookCoverData(bi, coverData)
                            }
                            result = resultMap
                        }
                    } ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveMultipleBookCoverData.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onBooksProcessed(result) }
                }
            })
        }
    }

    /**
     * Load recent books (with bookmarks)
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no recent books are found. In this case, the `books` callback argument will be empty or null.
     */
    internal fun loadRecentBooks(
        maxCount: Int,
        callback: BooksLoadingCallback,
        callerHandler: Handler
    ) {
        var list: Collection<BookInfo>? = null
        postTask(object : AbstractTask("loadRecentBooks") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { list = it.loadRecentBooks(maxCount) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("loadRecentBooks.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksListLoaded(list) }
            }
        })
    }

    /**
     * Remove book from recent list
     * @param bookInfo book info to remove from recent list
     * @param callback callback to run at the end of remove process
     * @param callerHandler handler to run callback
     *
     * The callback will be called regardless of result.
     */
    fun removeBooksFromRecent(
        bookInfo: BookInfo,
        callback: BooleanResultCallback,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("removeBooksFromRecent") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.removeBooksFromRecent(bookInfo) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("removeBooksFromRecent.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onResult(res) }
            }
        })
    }

    /**
     * Remove multiple books from recent list
     * @param books books collection to be deleted
     * @param callback callback to run at the end of operation
     * @param callerHandler handler to run callback
     */
    fun removeMultipleBooksFromRecent(
        books: Collection<BookInfo>,
        callback: BooksOperationCallback?,
        callerHandler: Handler
    ) {
        var result: HashMap<BookInfo, Boolean>? = null
        postTask(object : AbstractTask("removeMultipleBooksFromRecent") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let {
                        if (books.isNotEmpty()) {
                            val resultMap = HashMap<BookInfo, Boolean>(books.size)
                            for (book in books) {
                                resultMap[book] = it.removeBooksFromRecent(book)
                            }
                            result = resultMap
                        }
                    } ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("removeMultipleBooksFromRecent.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onBooksProcessed(result) }
                }
            })
        }
    }

    /**
     * Load filtered book list (with bookmarks)
     * @param sorting book sorting attribute
     * @param filters book filters collection
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be empty or null.
     */
    fun loadBookList(
        sorting: BooksSorting,
        filters: BookFilters,
        callback: BooksLoadingCallback,
        callerHandler: Handler
    ) {
        var list: Collection<BookInfo>? = null
        postTask(object : AbstractTask("loadBookList") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { list = it.loadBookList(sorting, filters) } ?: throw RuntimeException(
                        "Database connection is null"
                    )
                }
            }
        })
        postTask(object : AbstractTask("loadBookList.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksListLoaded(list) }
            }
        })
    }

    /**
     * Delete book record from database
     * @param bookInfo book to be deleted
     * @param callback callback to run at the end of operation
     * @param callerHandler handler to run callback
     */
    fun removeBookInfo(
        bookInfo: BookInfo,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("removeBookInfo") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = (it.removeBookInfo(bookInfo) != null) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("removeBookInfo.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Remove multiple book records from database
     * @param books books collection to be deleted
     * @param callback callback to run at the end of operation
     * @param callerHandler handler to run callback
     */
    fun removeMultipleBookInfo(
        books: Collection<BookInfo>,
        callback: BooksOperationCallback? = null,
        callerHandler: Handler
    ) {
        var result: HashMap<BookInfo, Boolean>? = null
        postTask(object : AbstractTask("removeMultipleBookInfo") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let {
                        if (books.isNotEmpty()) {
                            val resultMap = HashMap<BookInfo, Boolean>(books.size)
                            for (book in books) {
                                resultMap[book] = null != it.removeBookInfo(book)
                            }
                            result = resultMap
                        }
                    } ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("removeMultipleBookInfo.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onBooksProcessed(result) }
                }
            })
        }
    }

    /**
     * Gets the specified handbook
     * @param handbookType type of the handbook
     * @param callback callback to return loaded data
     * @param callerHandler handler to run callback
     */
    fun getHandbook(
        handbookType: HandbookType,
        callback: HandbookLoadingCallback,
        callerHandler: Handler
    ) {
        var handbook: Handbook? = null
        postTask(object : AbstractTask("getHandbook") {
            override fun work() {
                synchronized(mLocker) {
                    if (null != mDB)
                        handbook = mDB!!.getHandbook(handbookType)
                    else
                        throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("getHandbook.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onHandbookLoaded(handbook) }
            }
        })
    }

    /**
     * Load book search history
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no history are found. In this case, the `history` callback argument will be null.
     */
    fun loadBookSearchHistory(
        maxCount: Int,
        callback: SearchHistoryLoadingCallback,
        callerHandler: Handler
    ) {
        var list: Collection<SearchHistoryItem>? = null
        postTask(object : AbstractTask("loadBookSearchHistory") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { list = it.loadBookSearchHistory(maxCount) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("loadBookSearchHistory.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onHistoryLoaded(list) }
            }
        })
    }

    /**
     * Save new book search history item
     * @param searchItem book search history item
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun saveBookSearchHistoryItem(
        searchItem: SearchHistoryItem,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("saveBookSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.saveBookSearchHistoryItem(searchItem) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveBookSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Remove book search history item
     * @param searchItem book search history item
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun removeBookSearchHistoryItem(
        searchItem: SearchHistoryItem,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("removeBookSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.removeBookSearchHistoryItem(searchItem) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("removeBookSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Clear book search history
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun clearBookSearchHistoryItem(callback: BooleanResultCallback?, callerHandler: Handler) {
        var res = false
        postTask(object : AbstractTask("clearBookSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.clearBookSearchHistoryItem() }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("clearBookSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Search books by search query
     * @param searchQuery search query
     * @param sorting book sorting attribute
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `books` callback argument will be empty.
     * Search is carried out only by author, title and series.
     */
    fun searchBooks(
        searchQuery: String,
        sorting: BooksSorting,
        callback: BookSearchCallback,
        callerHandler: Handler
    ) {
        var list: Collection<BookInfo>? = null
        postTask(object : AbstractTask("searchBooks") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { list = it.searchBooks(searchQuery, sorting) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("searchBooks.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onBooksFound(list) }
            }
        })
    }

    /**
     * Load text search history
     * @param maxCount maximum items count in result
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no history are found. In this case, the `history` callback argument will be null.
     */
    fun loadTextSearchHistory(
        maxCount: Int,
        callback: SearchHistoryLoadingCallback,
        callerHandler: Handler
    ) {
        var list: Collection<SearchHistoryItem>? = null
        postTask(object : AbstractTask("loadTextSearchHistory") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { list = it.loadTextSearchHistory(maxCount) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("loadTextSearchHistory.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onHistoryLoaded(list) }
            }
        })
    }

    /**
     * Save new text search history item
     * @param searchItem text search history item
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun saveTextSearchHistoryItem(
        searchItem: SearchHistoryItem,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("saveTextSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.saveTextSearchHistoryItem(searchItem) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("saveTextSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Remove text search history item
     * @param searchItem text search history item
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun removeTextSearchHistoryItem(
        searchItem: SearchHistoryItem,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("removeTextSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.removeTextSearchHistoryItem(searchItem) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("removeTextSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Clear text search history
     * @param callback callback to run at the end of the loading
     * @param callerHandler handler to run callback
     *
     * The callback will be called on any result.
     */
    fun clearTextSearchHistoryItem(callback: BooleanResultCallback?, callerHandler: Handler) {
        var res = false
        postTask(object : AbstractTask("clearTextSearchHistoryItem") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.clearTextSearchHistoryItem() }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("clearTextSearchHistoryItem.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Gets the currently read book
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if no books are found. In this case, the `result` callback argument will be empty.
     */
    fun getCurrentBook(callback: StringResultCallback, callerHandler: Handler) {
        var filePath = ""
        postTask(object : AbstractTask("getCurrentBook") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { filePath = it.getCurrentBook() }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        postTask(object : AbstractTask("getCurrentBook.result") {
            override fun work() {
                sendTask(callerHandler) { callback.onResult(filePath) }
            }
        })
    }

    /**
     * Sets the currently read book
     * @param filePath full file path to book file
     * @param timestamp file open event timestamp
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if error occurred. In this case, the `result` callback argument will be false.
     */
    fun setCurrentBook(
        filePath: String,
        timestamp: Long,
        callback: BooleanResultCallback?,
        callerHandler: Handler
    ) {
        var res = false
        postTask(object : AbstractTask("setCurrentBook") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.setCurrentBook(filePath, timestamp) }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("setCurrentBook.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    /**
     * Unset the currently read book
     * @param callback callback to run at the end of the search
     * @param callerHandler handler to run callback
     *
     * The callback will be called even if error occurred. In this case, the `result` callback argument will be false.
     */
    fun unsetCurrentBook(callback: BooleanResultCallback?, callerHandler: Handler) {
        var res = false
        postTask(object : AbstractTask("unsetCurrentBook") {
            override fun work() {
                synchronized(mLocker) {
                    mDB?.let { res = it.unsetCurrentBook() }
                        ?: throw RuntimeException("Database connection is null")
                }
            }
        })
        callback?.let {
            postTask(object : AbstractTask("unsetCurrentBook.result") {
                override fun work() {
                    sendTask(callerHandler) { it.onResult(res) }
                }
            })
        }
    }

    companion object {
        private val log = SRLog.create("dbsvc")
        const val UnsupportedGenreIdBase = 1000000L
    }
}
