/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Canvas
import android.graphics.Rect
import kotlin.math.min

internal class ScrollAnimation(dir: Int, page1: PageImageCache, page2: PageImageCache, limitOffset: Int? = null) :
    AbstractAnimation() {

    private val mPage1: PageImageCache = page1
    private val mPage2: PageImageCache = page2
    private val mPage1Height: Int
    private val mPage2Height: Int
    private var mPos: Int = 0
    private var mSpeed: Float = 50f
    private val mSrcRect1 = Rect()
    private val mDstRect1 = Rect()
    private val mSrcRect2 = Rect()
    private val mDstRect2 = Rect()
    private var mLimitOffset: Int? = limitOffset
    private var mDone = false

    override val direction: Int = dir

    override val position: Int
        get() = if (direction > 0) mPos else -mPos

    override val isAnimationComplete: Boolean
        get() = mDone

    override fun setSpeed(speed: Float) {
        mSpeed = speed
    }

    override fun autoIncrementPosition(acceleration: Float) {
        if (mDone)
            return
        mSpeed += acceleration
        if (mSpeed <= 0) {
            mSpeed = 0f
            mDone = true
        } else if (mSpeed >= MAX_SPEED) {
            mSpeed = MAX_SPEED
        }
        mPos = (mPos.toFloat() + mSpeed).toInt() //m_pos += 1;
        mLimitOffset?.let { limit ->
            if (mPos >= limit) {
                mPos = limit
                mDone = true
            }
        }
        if (mPos >= mPage2Height) {
            mPos = mPage2Height - 1
            mDone = true
        }
    }

    override fun autoDecrementPosition(acceleration: Float) {
        if (mDone)
            return
        mSpeed += acceleration
        if (mSpeed <= 0) {
            mSpeed = 0f
            mDone = true
        } else if (mSpeed >= MAX_SPEED) {
            mSpeed = MAX_SPEED
        }
        mPos = (mPos.toFloat() - mSpeed).toInt() //m_pos -= 1;
        if (mPos < 0) {
            mPos = 0
            mDone = true
        }
    }

    override fun overridePosition(x: Int, y: Int) {
        // TODO: allow slight overflow of images
        mPos = if (direction > 0) -y else y
        if (mPos < 0)
            mPos = 0
        else if (mPos >= mPage2Height)
            mPos = mPage2Height - 1
    }

    override fun resetPosition() {
        mPos = 0
    }

    override fun renderFrame(canvas: Canvas): Boolean {
        if (mPage1.isReleased || mPage2.isReleased)
            return false
        // TODO: fill unused space with background
        if (direction > 0) {
            // forward direction
            mSrcRect1.set(0, mPos, mPage1.position!!.pageWidth, mPage1Height)
            mDstRect1.set(0, 0, mPage1.position!!.pageWidth, mPage1.position!!.pageHeight - mPos)
            mSrcRect2.set(0, 0, mPage1.position!!.pageWidth, mPos)
            mDstRect2.set(
                0,
                mPage1.position!!.pageHeight - mPos,
                mPage2.position!!.pageWidth,
                mPage2.position!!.pageHeight
            )
        } else {
            // backward direction
            mSrcRect1.set(0, 0, mPage1.position!!.pageWidth, mPage1Height - mPos)
            mDstRect1.set(0, mPos, mPage1.position!!.pageWidth, mPage1.position!!.pageHeight)
            mSrcRect2.set(0, mPage2Height - mPos, mPage2.position!!.pageWidth, mPage2Height)
            mDstRect2.set(0, 0, mPage2.position!!.pageWidth, mPos)
        }
        if (mSrcRect1.height() >= 0)
            canvas.drawBitmap(mPage1.bitmap!!, mSrcRect1, mDstRect1, null)
        if (mSrcRect2.height() >= 0)
            canvas.drawBitmap(mPage2.bitmap!!, mSrcRect2, mDstRect2, null)
        return true
    }

    companion object {
        private const val MAX_SPEED = 500f
    }

    init {
        if (mPage2.position!!.y > mPage1.position!!.y) {
            mPage1Height = min(
                mPage2.position!!.y - mPage1.position!!.y,
                mPage1.position!!.pageHeight
            )
            mPage2Height = min(
                mPage2.position!!.fullHeight - mPage2.position!!.y,
                mPage2.position!!.pageHeight
            )
        } else {
            mPage1Height = min(
                mPage1.position!!.fullHeight - mPage1.position!!.y,
                mPage1.position!!.pageHeight
            )
            mPage2Height = min(
                mPage1.position!!.y - mPage2.position!!.y,
                mPage2.position!!.pageHeight
            )
        }
    }
}