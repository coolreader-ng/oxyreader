/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.Message

class BackgroundThread private constructor() : Thread() {
    private var mBackgroundHandler: Handler? = null
    private val mGUIHandler: Handler
    private val mBackgroundPosted = ArrayList<Runnable>()

    override fun run() {
        log.info("Entering background thread")
        Looper.prepare()
        synchronized(mLocker) {
            mBackgroundHandler = object : Handler(
                Looper.myLooper()!!
            ) {
                override fun handleMessage(message: Message) {
                    log.debug("message: $message")
                }
            }
        }
        log.info("Background thread handler is created")
        synchronized(mBackgroundPosted) {
            for (task in mBackgroundPosted) {
                log.info("Post bg task to handler : $task")
                mBackgroundHandler!!.post(task)
            }
            mBackgroundPosted.clear()
        }
        Looper.loop()
        synchronized(mLocker) {
            mBackgroundHandler = null
            mInstance = null
        }
        log.info("Exiting background thread")
    }


    /*
    private fun guard(r: Runnable): Runnable {
        return r;
        //if (!USE_LOCK)
        //    return r;
        //return Runnable {
        //    synchronized(mLocker) {
        //        r.run()
        //    }
        //}
    }
     */

    private fun postBackgroundImpl(task: Runnable, delay: Long) {
        //task = guard(task);
        synchronized(mLocker) {
            mBackgroundHandler?.let { handler ->
                if (delay > 0) {
                    handler.postDelayed({
                        try {
                            task.run()
                        } catch (e: Throwable) {
                            log.error(
                                "Exception while processing task in Background thread: $task",
                                e
                            )
                        }
                    }, delay)
                } else {
                    handler.post(task)
                }
            } ?: run {
                synchronized(mBackgroundPosted) {
                    log.info("Adding task $task to posted list since handler is not yet created")
                    mBackgroundPosted.add(task)
                }
            }
        }
    }

    private fun postGUIImpl(task: Runnable, delay: Long) {
        try {
            if (delay > 0) {
                val id = ++mDelayedTaskId
                //Log.v(TAG, "posting delayed (" + delay + ") task " + id + " " + task);
                mGUIHandler.postDelayed({
                    try {
                        task.run()
                        //Log.v(TAG, "finished delayed (" + delay + ") task " + id + " " + task);
                    } catch (e: Throwable) {
                        log.error("Exception while processing task in GUI thread: $task", e)
                    }
                }, delay)
            } else mGUIHandler.post(task)
        } catch (e: Throwable) {
            log.error("Exception while posting task to GUI thread: $task", e)
        }
    }

    private fun executeBackgroundImpl(task: Runnable) {
        //task = guard(task);
        if (isBackgroundThread)
            task.run() // run in this thread
        else
            postBackgroundImpl(task, 0) // post
    }

    private fun executeGUIImpl(task: Runnable) {
        try {
            if (isGUIThread)
                task.run() // run in this thread
            else {
                postGUIImpl({
                    try {
                        task.run()
                    } catch (e: Throwable) {
                        log.error("Exception while executing task in GUI thread: $task", e)
                    }
                }, 0)
            }
        } catch (e: Throwable) {
            log.error("Exception in executeGUI: $task", e)
        }
    }

    companion object {
        private val log = SRLog.create("bgth")
        private const val CHECK_THREAD_CONTEXT = true

        //private const val USE_LOCK: Boolean = false
        private val mLocker = Any()
        private var mInstance: BackgroundThread? = null
        private var mDelayedTaskId = 0

        // Singleton
        private val instance: BackgroundThread
            get() {
                synchronized(mLocker) {
                    if (mInstance == null) {
                        mInstance = BackgroundThread()
                        mInstance!!.start()
                    }
                    return mInstance!!
                }
            }

        private val isGUIThread: Boolean
            get() {
                return currentThread() === Looper.getMainLooper().thread
            }

        private val isBackgroundThread: Boolean
            get() {
                synchronized(mLocker) {
                    if (null == mInstance)
                        return false
                    return currentThread() === mInstance
                }
            }

        /**
         * Throws exception if not in background thread.
         */
        @JvmStatic
        fun ensureBackground() {
            if (CHECK_THREAD_CONTEXT && !isBackgroundThread) {
                log.error(
                    "not in background thread",
                    Exception("ensureInBackgroundThread() is failed")
                )
                throw RuntimeException("ensureInBackgroundThread() is failed")
            }
        }

        /**
         * Throws exception if not in GUI thread.
         */
        @JvmStatic
        fun ensureGUI() {
            if (CHECK_THREAD_CONTEXT && !isGUIThread) {
                log.error("not in GUI thread", Exception("ensureGUI() is failed"))
                throw RuntimeException("ensureGUI() is failed")
            }
        }

        /**
         * Post runnable to be executed in background thread.
         *
         * @param task is runnable to execute in background thread.
         */
        @JvmStatic
        fun postBackground(task: Runnable) {
            instance.postBackgroundImpl(task, 0)
        }

        /**
         * Post runnable to be executed in background thread.
         *
         * @param task  is runnable to execute in background thread.
         * @param delay is delay before running task, in millis
         */
        @JvmStatic
        fun postBackground(task: Runnable, delay: Long) {
            instance.postBackgroundImpl(task, delay)
        }

        /**
         * Post runnable to be executed in GUI thread
         *
         * @param task is runnable to execute in GUI thread
         */
        @JvmStatic
        fun postGUI(task: Runnable) {
            instance.postGUIImpl(task, 0)
        }

        /**
         * Post runnable to be executed in GUI thread
         *
         * @param task  is runnable to execute in GUI thread
         * @param delay is delay before running task, in millis
         */
        @JvmStatic
        fun postGUI(task: Runnable, delay: Long) {
            instance.postGUIImpl(task, delay)
        }

        /**
         * Run task instantly if called from the same thread, or post it through message queue otherwise.
         *
         * @param task is task to execute
         */
        @JvmStatic
        fun executeBackground(task: Runnable) {
            instance.executeBackgroundImpl(task)
        }

        /**
         * Run task instantly if called from the same thread, or post it through message queue otherwise.
         *
         * @param task is task to execute
         */
        @JvmStatic
        fun executeGUI(task: Runnable) {
            instance.executeGUIImpl(task)
        }

        @JvmStatic
        fun executeEngineTask(task: EngineTask) {
            executeBackground(EngineTaskHandler(task))
        }

        @JvmStatic
        fun postEngineTask(task: EngineTask) {
            postBackground(EngineTaskHandler(task))
        }

        /**
         * Create (if required) this background thread and wait for it to run.
         */
        @JvmStatic
        fun bootstrap(): Boolean {
            var res = false
            val synchro = Object()
            synchronized(synchro) {
                instance.postBackgroundImpl({ synchronized(synchro) { synchro.notify() } }, 0)
                while (true) {
                    try {
                        synchro.wait()
                        res = true
                        log.debug("BackgroundThread boostrap successful.")
                        break
                    } catch (e: InterruptedException) {
                        continue
                    } catch (e: Exception) {
                        res = false
                        break
                    }
                }
            }
            return res
        }

        /**
         * Shutdown background thread.
         */
        @JvmStatic
        fun gentlyQuit() {
            mInstance?.let { thread ->
                // Wait for all posted but not completed tasks
                val sema = Object()
                synchronized(sema) {
                    thread.postBackgroundImpl({
                        synchronized(sema) {
                            sema.notify()
                        }
                    }, 0)
                    while (true) {
                        try {
                            sema.wait()
                            log.debug("All posted tasks is done.")
                            break
                        } catch (e: InterruptedException) {
                            continue
                        } catch (e: Exception) {
                            break
                        }
                    }
                }
                thread.postBackgroundImpl({
                    thread.mBackgroundHandler?.let { handler ->
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            log.info("Calling quitSafely() on background thread looper.")
                            handler.looper.quitSafely()
                        } else {
                            log.info("Calling quit() on background thread looper.")
                            handler.looper.quit()
                        }
                    } ?: run {
                        synchronized(mLocker) {
                            mInstance = null
                        }
                    }
                }, 0)
            }
        }
    }

    /**
     * Background thread constructor.
     */
    init {
        name = "BackgroundThread" + Integer.toHexString(hashCode())
        log.info("Created new background thread instance")
        mGUIHandler = Handler(Looper.getMainLooper())
    }
}