/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener

/**
 * A class, that can be used as a TouchListener on any view (e.g. a Button).
 * It cyclically perform click, emulating keyboard-like behaviour.
 * First click is fired immediately, next one after the initialInterval,
 * and subsequent ones after the normalInterval.
 */

/**
 * @param initialInterval The interval after first click event
 * @param normalInterval  The interval after second and subsequent click events
 */
class RepeatOnTouchListener(
    private val initialInterval: Int,
    private val normalInterval: Int
) : OnTouchListener {
    private val handler = Handler()
    private var touchedView: View? = null

    private val handlerRunnable: Runnable = object : Runnable {
        override fun run() {
            touchedView?.let { view ->
                if (view.isEnabled) {
                    handler.postDelayed(this, normalInterval.toLong())
                    view.performClick()
                } else {
                    // if the view was disabled by the clickListener, remove the callback
                    handler.removeCallbacks(this)
                    view.isPressed = false
                    touchedView = null
                }
            }
        }
    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                handler.removeCallbacks(handlerRunnable)
                handler.postDelayed(handlerRunnable, initialInterval.toLong())
                touchedView = view
                touchedView?.isPressed = true
                view.performClick()
                return true
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                handler.removeCallbacks(handlerRunnable)
                touchedView?.isPressed = false
                touchedView = null
                return true
            }
        }
        return false
    }
}