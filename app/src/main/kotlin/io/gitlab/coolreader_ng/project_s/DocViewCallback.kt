/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

@UsedInJNI
interface DocViewCallback {
    /// on starting file loading
    @UsedInJNI
    fun onLoadFileStart(filename: String?)

    /// format detection finished
    @UsedInJNI
    fun onLoadFileFormatDetected(fileFormat: DocumentFormat?): String?

    /// file loading is finished successfully - drawCoveTo() may be called there
    @UsedInJNI
    fun onLoadFileEnd()

    /// first page is loaded from file an can be formatted for preview
    @UsedInJNI
    fun onLoadFileFirstPagesReady()

    /// file progress indicator, called with values 0..100
    @UsedInJNI
    fun onLoadFileProgress(percent: Int)

    /// document formatting started
    @UsedInJNI
    fun onFormatStart()

    /// document formatting finished
    @UsedInJNI
    fun onFormatEnd()

    /// format progress, called with values 0..100
    @UsedInJNI
    fun onFormatProgress(percent: Int)

    /// format progress, called with values 0..100
    @UsedInJNI
    fun onExportProgress(percent: Int)

    /// file load finiished with error
    @UsedInJNI
    fun onLoadFileError(message: String?)

    /// Override to handle external links
    @UsedInJNI
    fun onExternalLink(url: String?, nodeXPath: String?)

    /// Override to handle external links
    @UsedInJNI
    fun onImageCacheClear()

    /// Override to handle reload request, return true if request is processed
    @UsedInJNI
    fun onRequestReload(): Boolean
}
