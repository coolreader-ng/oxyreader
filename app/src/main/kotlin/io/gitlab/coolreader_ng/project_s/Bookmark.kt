/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Parcel
import android.os.Parcelable

@UsedInJNI
class Bookmark @UsedInJNI constructor() : Parcelable {
    /**
     * Unique bookmark number. Does not refer to any external directories, this number is for internal use only.
     */
    var id: Long? = null

    /**
     * Bookmark type. Code reference is present only in the program.
     */
    @UsedInJNI
    var type = TYPE_UNSPECIFIED

    /**
     * The percentage read for this bookmark. In hundredths of a percent.
     */
    @UsedInJNI
    var percent = 0

    /**
     * ldomXPointer to bookmark start
     */
    @UsedInJNI
    var startPos: String? = null

    /**
     * ldomXPointer to bookmark end
     */
    @UsedInJNI
    var endPos: String? = null

    /**
     * The name of the chapter in which this bookmark is located
     */
    @UsedInJNI
    var titleText: String? = null

    /**
     * The text of the element of the book pointed to by this bookmark
     */
    @UsedInJNI
    var posText: String? = null

    /**
     * Comment on this bookmark
     */
    @UsedInJNI
    var commentText: String? = null
        private set

    /**
     * Bookmark creation time (timestamp)
     */
    @UsedInJNI
    var timeStamp: Long = System.currentTimeMillis() // UTC timestamp

    /**
     * Total book reading time (milliseconds)
     */
    var readingTime: Long = 0

    @UsedInJNI
    constructor(v: Bookmark) : this() {
        id = v.id
        type = v.type
        percent = v.percent
        startPos = v.startPos
        endPos = v.endPos
        titleText = v.titleText
        posText = v.posText
        commentText = v.commentText
        timeStamp = v.timeStamp
        readingTime = v.readingTime
    }

    protected constructor(parcel: Parcel) : this() {
        val mark: Byte = parcel.readByte()
        id = if (0 == mark.toInt())
            null
        else
            parcel.readLong()
        type = parcel.readInt()
        percent = parcel.readInt()
        startPos = parcel.readString()
        endPos = parcel.readString()
        titleText = parcel.readString()
        posText = parcel.readString()
        commentText = parcel.readString()
        timeStamp = parcel.readLong()
        readingTime = parcel.readLong()
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = id?.hashCode() ?: 0
        result = prime * result + type.hashCode()
        result = prime * result + percent.hashCode()
        result = prime * result + (startPos?.hashCode() ?: 0)
        result = prime * result + (endPos?.hashCode() ?: 0)
        result = prime * result + (titleText?.hashCode() ?: 0)
        result = prime * result + (posText?.hashCode() ?: 0)
        result = prime * result + (commentText?.hashCode() ?: 0)
        result = prime * result + timeStamp.hashCode()
        result = prime * result + readingTime.hashCode()
        return result
    }

    fun equalUniqueKey(bm: Bookmark): Boolean {
        if (type != bm.type)
            return false
        return when (type) {
            TYPE_LAST_POSITION -> true
            TYPE_POSITION -> startPos == bm.startPos
            TYPE_COMMENT,
            TYPE_CORRECTION -> startPos == bm.startPos && endPos == bm.endPos

            else -> false
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (null == other)
            return false
        if (javaClass != other.javaClass)
            return false
        val obj = other as Bookmark
        if (commentText != obj.commentText)
            return false
        if (endPos != obj.endPos)
            return false
        if (id != obj.id)
            return false
        if (percent != obj.percent)
            return false
        if (posText != obj.posText)
            return false
        if (startPos != obj.startPos)
            return false
        if (timeStamp != obj.timeStamp)
            return false
        if (readingTime != obj.readingTime)
            return false
        if (titleText != obj.titleText)
            return false
        if (type != obj.type)
            return false
        return true
    }

    val uniqueKey: String
        get() = when (type) {
            TYPE_LAST_POSITION -> "l"
            TYPE_POSITION -> "p$startPos"
            TYPE_COMMENT -> "c$startPos-$endPos"
            TYPE_CORRECTION -> "r$startPos-$endPos"
            else -> "unknown"
        }

    private fun changed(v1: String?, v2: String?): Boolean {
        if (v1 == null && v2 == null)
            return false
        if (v1 == null || v2 == null)
            return true
        return v1 != v2
    }

    fun setCommentText(commentText: String?): Boolean {
        if (!changed(this.commentText, commentText))
            return false
        this.commentText = commentText
        return true
    }

    val isValid: Boolean
        get() {
            if (startPos == null || startPos!!.isEmpty())
                return false
            if (type < TYPE_LAST_POSITION || type > TYPE_CORRECTION)
                return false
            if ((endPos == null || endPos!!.isEmpty()) && (type == TYPE_COMMENT || type == TYPE_CORRECTION))
                return false
            return true
        }

    override fun toString(): String {
        return "Bookmark(t=$type, start=$startPos)"
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        if (id == null) {
            dest.writeByte(0.toByte())
        } else {
            dest.writeByte(1.toByte())
            dest.writeLong(id!!)
        }
        dest.writeInt(type)
        dest.writeInt(percent)
        dest.writeString(startPos)
        dest.writeString(endPos)
        dest.writeString(titleText)
        dest.writeString(posText)
        dest.writeString(commentText)
        dest.writeLong(timeStamp)
        dest.writeLong(readingTime)
    }

    companion object {
        const val TYPE_LAST_POSITION = 0
        const val TYPE_POSITION = 1
        const val TYPE_COMMENT = 2
        const val TYPE_CORRECTION = 3
        const val TYPE_UNSPECIFIED = -1

        @JvmField
        val CREATOR: Parcelable.Creator<Bookmark> = object : Parcelable.Creator<Bookmark> {
            override fun createFromParcel(parcel: Parcel): Bookmark {
                return Bookmark(parcel)
            }

            override fun newArray(size: Int): Array<Bookmark?> {
                return arrayOfNulls(size)
            }
        }
    }
}