/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Typeface
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Space
import android.widget.TextView
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener
import java.util.Stack

class DocContentsPopup(private val parent: View, contentView: View) {

    private class TocItemViewData {
        // Index in ListView
        var viewIndex: Int = -1

        // The status of this TOC element is either expanded (true) or collapsed (false).
        var expanded = false
    }

    interface OnTOCItemClickListener {
        fun onTOCItemClick(item: DocumentTOCItem)
    }

    private val mTocItemViewProps = HashMap<DocumentTOCItem, TocItemViewData>()

    // extension property for DocumentTOCItem
    private val DocumentTOCItem.viewProps: TocItemViewData
        @Synchronized
        get() {
            var viewProps = mTocItemViewProps[this]
            if (null == viewProps) {
                viewProps = TocItemViewData()
                mTocItemViewProps[this] = viewProps
            }
            return viewProps
        }

    var onTOCItemClickListener: OnTOCItemClickListener? = null
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mPopup: PopupWindow

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null
    private val mViewGroup: ViewGroup
    private val mToolbar: View
    private var mToolbarPosition: Int
    private val mBackButton: Button
    private val mBookNameView: TextView

    private val mRecyclerView: RecyclerView
    private var mLangTag: String? = null
    private var mRootItem: DocumentTOCItem? = null
    private var mCurrentChapter: DocumentTOCItem? = null
    private var mCurrentChapterPos = -1

    // Current page number (starting from 1)
    private var mCurrentPage: Int = -1
    private val mVisibleItems: ArrayList<DocumentTOCItem> = ArrayList()

    private inner class TocViewAdapter : RecyclerView.Adapter<TocViewAdapter.TocItemViewHolder>() {

        inner class TocItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val levelMarginSpace: Space = itemView.findViewById(R.id.level_margin)
            val treeItemStateImage: ImageView = itemView.findViewById(R.id.tree_item_state_imgview)
            val nameView: TextView = itemView.findViewById(R.id.name)
            val pageView: TextView = itemView.findViewById(R.id.page)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TocItemViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val itemView = layoutInflater.inflate(R.layout.toc_item, parent, false)
            return TocItemViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: TocItemViewHolder, position: Int) {
            val item: DocumentTOCItem? = if (position >= 0 && position < mVisibleItems.size)
                mVisibleItems[position]
            else
                null
            val isCurrentChapter = item === mCurrentChapter
            if (null != item) {
                var levelMargin = 0
                var i = 1
                while (i < item.level) {
                    levelMargin += (Utils.DPI_SCALE * 16f).toInt()
                    i++
                }
                holder.levelMarginSpace.layoutParams.width = levelMargin
                holder.levelMarginSpace.requestLayout()
                if (item.hasChildren) {
                    if (item.viewProps.expanded)
                        holder.treeItemStateImage.setImageResource(R.drawable.ic_listview_tree_emblem_expanded)
                    else
                        holder.treeItemStateImage.setImageResource(R.drawable.ic_listview_tree_emblem_collapsed)
                } else {
                    holder.treeItemStateImage.setImageResource(R.drawable.ic_listview_tree_emblem_normal)
                }
                holder.nameView.setHyphenatedText(mLangTag, item.name)
                holder.pageView.text = (item.page + 1).toString()
                holder.nameView.setTypeface(
                    null,
                    if (isCurrentChapter) Typeface.BOLD else Typeface.NORMAL
                )
                holder.pageView.setTypeface(
                    null,
                    if (isCurrentChapter) Typeface.BOLD else Typeface.NORMAL
                )
            }
        }

        override fun getItemCount(): Int {
            return mVisibleItems.size
        }
    }

    private val mAdapter = TocViewAdapter()

    fun show(title: String, langTag: String?, toc: DocumentTOCItem, currentPage: Int) {
        mBookNameView.text = title
        mLangTag = langTag
        mRootItem = toc
        mCurrentPage = currentPage
        val currentChapter = mRootItem?.getChapterAtPage(mCurrentPage - 1)
        mRootItem?.let {
            buildItemsList()
            currentChapter?.let {
                updateCurrentChapter()
                ensureVisible(it)
            }
        }
        // TODO: for tablet devices (or in landscape position) occupy only part of screen but not full
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    /**
     * Sets toolbar position. May be PropNames.App.TOOLBAR_LOCATION_TOP or PropNames.App.TOOLBAR_LOCATION_BOTTOM
     */
    fun setToolbarPosition(pos: Int) {
        if (pos != mToolbarPosition) {
            val insPos = when (pos) {
                PropNames.App.TOOLBAR_LOCATION_TOP -> {
                    0
                }

                PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                    1
                }

                else -> {
                    -1
                }
            }
            mToolbarPosition = pos
            if (insPos >= 0) {
                try {
                    mViewGroup.removeView(mToolbar)
                } catch (_: Exception) {
                }
                val layoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        0f
                    )
                mViewGroup.addView(mToolbar, insPos, layoutParams)
                mToolbar.visibility = View.VISIBLE
            } else {
                mToolbar.visibility = View.GONE
            }
        }
    }

    private fun updateCurrentChapter() {
        var chapter: DocumentTOCItem? = null
        var chapterPos = -1
        for ((i, item) in mVisibleItems.withIndex()) {
            if (item.page < mCurrentPage) {
                chapter = item
                chapterPos = i
            }
        }
        if (chapter != mCurrentChapter || chapterPos != mCurrentChapterPos) {
            val oldPos = mCurrentChapterPos
            mCurrentChapter = chapter
            mCurrentChapterPos = chapterPos
            if (oldPos >= 0)
                mAdapter.notifyItemChanged(oldPos)
            if (mCurrentChapterPos >= 0)
                mAdapter.notifyItemChanged(mCurrentChapterPos)
        }
    }

    private fun buildItemsList() {
        mVisibleItems.clear()
        mCurrentChapter = null
        mRootItem?.let { root ->
            recursivelyResetViewIndex(root)
            root.children?.let {
                for (child in it) {
                    recursivelyAddItem(child)
                }
            }
        }
        mAdapter.notifyItemRangeInserted(0, mVisibleItems.size)
    }

    private fun recursivelyAddItem(item: DocumentTOCItem) {
        // update index in list view
        item.viewProps.viewIndex = mVisibleItems.size
        mVisibleItems.add(item)
        if (item.hasChildren && item.viewProps.expanded) {
            item.children?.let {
                for (child in it)
                    recursivelyAddItem(child)
            }
        }
    }

    private fun recursivelyResetViewIndex(item: DocumentTOCItem) {
        item.viewProps.viewIndex = -1
        item.children?.let {
            for (child in it) {
                recursivelyResetViewIndex(child)
            }
        }
    }

    private fun expandItem(item: DocumentTOCItem) {
        if (item.viewProps.expanded || item === mRootItem)
            return
        // Ensure all parents expanded first
        val parents = Stack<DocumentTOCItem>()
        var parent = item.parent
        while (null != parent) {
            parents.push(parent)
            parent = parent.parent
        }
        while (!parents.empty()) {
            parent = parents.pop()
            expandItem(parent)
        }
        item.viewProps.expanded = true
        val startPos = item.viewProps.viewIndex + 1
        var insertPos = startPos
        item.children?.let {
            for (child in it) {
                child.viewProps.viewIndex = insertPos
                child.viewProps.expanded = false
                mVisibleItems.add(insertPos, child)
                insertPos++
            }
            mAdapter.notifyItemRangeInserted(startPos, insertPos - startPos)
        }
        mAdapter.notifyItemChanged(item.viewProps.viewIndex)
        for (i in item.viewProps.viewIndex + 1 until mVisibleItems.size)
            mVisibleItems[i].viewProps.viewIndex = i
        updateCurrentChapter()
    }

    private fun collapseItem(item: DocumentTOCItem) {
        if (!item.viewProps.expanded)
            return
        item.children?.let {
            var startPos = -1
            var endPos = -1
            // collapse all children first
            for (child in it) {
                if (child.viewProps.expanded)
                    collapseItem(child)
            }
            for (child in it) {
                val pos = child.viewProps.viewIndex
                if (-1 == startPos)
                    startPos = pos
                endPos = pos
                mVisibleItems.removeAt(startPos)
            }
            mAdapter.notifyItemRangeRemoved(startPos, endPos - startPos + 1)
        }
        item.viewProps.expanded = false
        mAdapter.notifyItemChanged(item.viewProps.viewIndex)
        for (i in item.viewProps.viewIndex + 1 until mVisibleItems.size)
            mVisibleItems[i].viewProps.viewIndex = i
        updateCurrentChapter()
    }

    private fun ensureVisible(item: DocumentTOCItem) {
        item.parent?.let { expandItem(it) }
        if (item.viewProps.viewIndex >= 0)
            mRecyclerView.smoothScrollToPosition(item.viewProps.viewIndex)
    }

    init {
        val context = contentView.context
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.START)
            mPopup.exitTransition = Slide(Gravity.START)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = true
        mPopup.isTouchable = true
        mPopup.isFocusable = false
        mViewGroup = contentView.findViewById(R.id.docContentContainer)
        mToolbar = contentView.findViewById(R.id.customToolbar)
        mToolbarPosition = PropNames.App.TOOLBAR_LOCATION_TOP
        mBackButton = contentView.findViewById(R.id.toolBack)
        mBackButton.setOnClickListener { hide() }
        mBookNameView = contentView.findViewById(R.id.bookName)
        mRecyclerView = contentView.findViewById(R.id.recyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        val rvPaddingLeft = mRecyclerView.paddingLeft
        val rvPaddingRight = mRecyclerView.paddingRight
        val rvPaddingTop = mRecyclerView.paddingTop
        val rvPaddingBottom = mRecyclerView.paddingBottom
        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            var tbInsets = insets
            var rvInsets = insets
            if (mToolbar.isVisible) {
                when (mToolbarPosition) {
                    PropNames.App.TOOLBAR_LOCATION_TOP -> {
                        tbInsets = Insets.of(insets.left, insets.top, insets.right, 0)
                        rvInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
                    }

                    PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                        tbInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
                        rvInsets = Insets.of(insets.left, insets.top, insets.right, 0)
                    }
                }
            }
            mToolbar.updatePadding(tbInsets.left, tbInsets.top, tbInsets.right, tbInsets.bottom)
            mRecyclerView.updatePadding(
                rvInsets.left + rvPaddingLeft,
                rvInsets.top + rvPaddingTop,
                rvInsets.right + rvPaddingRight,
                rvInsets.bottom + rvPaddingBottom
            )
            WindowInsetsCompat.CONSUMED
        }

        mRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                context,
                true
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val item =
                                if (index >= 0 && index < mVisibleItems.size) mVisibleItems[index] else null
                            if (null != item && null != onTOCItemClickListener) {
                                if (item.hasChildren) {
                                    if (item.viewProps.expanded)
                                        collapseItem(item)
                                    else
                                        expandItem(item)
                                    ensureVisible(item)
                                } else {
                                    onTOCItemClickListener?.onTOCItemClick(item)
                                    this@DocContentsPopup.hide()
                                }
                            }
                        }

                        override fun onLongPressed(index: Int) {
                            val item =
                                if (index >= 0 && index < mVisibleItems.size) mVisibleItems[index] else null
                            if (null != item && null != onTOCItemClickListener) {
                                onTOCItemClickListener?.onTOCItemClick(item)
                                this@DocContentsPopup.hide()
                                mRecyclerView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                            }
                        }
                    }
            })
    }
}
