/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import com.google.android.material.button.MaterialButton
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow
import java.text.NumberFormat
import kotlin.math.roundToInt

@SuppressLint("ClickableViewAccessibility")
class TTSPanelPopup(private val parent: View, contentView: View) {

    interface TTSPanelListener {
        fun onPlayPause()
        fun onClose()
        fun onPrev()
        fun onNext()
        fun onDecVolume()
        fun onIncVolume()
        fun onSetVolume(volume: Int)
        fun onDecSpeechRate()
        fun onIncSpeechRate()
        fun onSetSpeechRate(speechRate: Float)
        fun onOpenOptions()
    }

    var ttsPanelListener: TTSPanelListener? = null
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mIsSpeaking = false
    private var mMaxVolume = 100
    private var mVolume = 0
    private var mSpeechRate = 1f

    private var mPopup: PopupWindow

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null
    private val mButtonsPanel: ViewGroup
    private val mBtnPlayPause: MaterialButton
    private val mBtnDecVolume: Button
    private val mBtnIncVolume: Button
    private val mVolumeSlider: Slider
    private val mTvVolume: TextView
    private val mBtnDecRate: Button
    private val mBtnIncRate: Button
    private val mRateSlider: Slider
    private val mTvRate: TextView

    fun show() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
    }

    fun update(isSpeaking: Boolean, volume: Int, maxVolume: Int, speechRate: Float) {
        mIsSpeaking = isSpeaking
        mVolume = volume
        mMaxVolume = maxVolume
        mSpeechRate = speechRate
        updateImpl()
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    private fun updateImpl() {
        mBtnPlayPause.setIconResource(if (mIsSpeaking) R.drawable.ic_media_pause else R.drawable.ic_media_play)
        mVolumeSlider.valueTo = mMaxVolume.toFloat()
        mVolumeSlider.value = mVolume.toFloat()
        mBtnDecVolume.isEnabled = mVolume > 0
        mBtnIncVolume.isEnabled = mVolume < mMaxVolume
        mBtnDecRate.isEnabled = mSpeechRate > 0.3f
        mBtnIncRate.isEnabled = mSpeechRate < 3.5f
        mRateSlider.value = speechRateToPercent(mSpeechRate)
        val roundValue = (100.0 * mVolume / mMaxVolume).roundToInt()
        mTvVolume.text = parent.context.getString(R.string.volume_arg, roundValue)
        mTvRate.text = parent.context.getString(R.string.rate_arg, mSpeechRate)
    }

    /**
     * Convert speech speed percentage to speech rate value.
     * @param percent speech rate percentage
     * @return speech rate value
     *
     * 0%  - 0.30
     * 10% - 0.44
     * 20% - 0.58
     * 30% - 0.72
     * 40% - 0.86
     * 50% - 1.00
     * 60% - 1.50
     * 70% - 2.00
     * 80% - 2.50
     * 90% - 3.00
     * 100%- 3.50
     */
    private fun speechRateFromPercent(percent: Float): Float {
        val rate = if (percent < 50f)
            0.3f + 0.7f * percent / 50f
        else
            1.0f + 2.5f * (percent - 50f) / 50f
        return rate
    }

    private fun speechRateToPercent(rate: Float): Float {
        var percent = if (rate < 1f)
            50f * (rate - 0.3f) / 0.7f
        else
            50f + 50f * (rate - 1f) / 2.5f
        if (percent < 0f)
            percent = 0f
        if (percent > 100f)
            percent = 100f
        return percent
    }

    init {
        val context = contentView.context
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.BOTTOM)
            mPopup.exitTransition = Slide(Gravity.BOTTOM)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = false
        mPopup.isTouchable = true
        mPopup.isFocusable = false

        val glassView = contentView.findViewById<View>(R.id.glass)
        mButtonsPanel = contentView.findViewById(R.id.buttonsPanel)
        mBtnPlayPause = contentView.findViewById(R.id.btnPlayPause)
        mBtnDecVolume = contentView.findViewById(R.id.btnDecVolume)
        mBtnIncVolume = contentView.findViewById(R.id.btnIncVolume)
        mVolumeSlider = contentView.findViewById(R.id.volumeSlider)
        mTvVolume = contentView.findViewById(R.id.tvVolume)
        mBtnDecRate = contentView.findViewById(R.id.btnDecRate)
        mBtnIncRate = contentView.findViewById(R.id.btnIncRate)
        mTvRate = contentView.findViewById(R.id.tvSpeechRate)
        mRateSlider = contentView.findViewById(R.id.rateSlider)

        glassView.requestFocus()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed
            // TODO: Test on other API, new material design library (possible fixed)
            mVolumeSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mVolumeSlider.setLabelFormatter { value ->
                val numberFormat = NumberFormat.getPercentInstance()
                numberFormat.maximumFractionDigits = 0
                numberFormat.format(value / mVolumeSlider.valueTo)
            }
            mRateSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mRateSlider.setLabelFormatter { value ->
                val numberFormat = NumberFormat.getNumberInstance()
                numberFormat.maximumFractionDigits = 2
                numberFormat.format(speechRateFromPercent(value))
            }
        }

        val panel = contentView.findViewById<View>(R.id.panel)
        val panelPaddingLeft = panel.paddingLeft
        val panelPaddingRight = panel.paddingRight
        val panelPaddingBottom = panel.paddingBottom
        ViewCompat.setOnApplyWindowInsetsListener(panel) { v, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(
                left = insets.left + panelPaddingLeft,
                right = insets.right + panelPaddingRight,
                bottom = insets.bottom + panelPaddingBottom
            )
            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }

        mBtnPlayPause.setOnClickListener {
            ttsPanelListener?.onPlayPause()
        }
        contentView.findViewById<Button>(R.id.btnStopAndClose)
            .setOnClickListener {
                ttsPanelListener?.onClose()
                //hide()
            }
        contentView.findViewById<Button>(R.id.btnSkipToPrev)
            .setOnClickListener {
                ttsPanelListener?.onPrev()
            }
        contentView.findViewById<Button>(R.id.btnSkipToNext)
            .setOnClickListener {
                ttsPanelListener?.onNext()
            }
        contentView.findViewById<Button>(R.id.btnOptions)
            .setOnClickListener {
                ttsPanelListener?.onOpenOptions()
            }

        mBtnDecVolume.setOnTouchListener(RepeatOnTouchListener(500, 150))
        mBtnDecVolume.setOnClickListener {
            ttsPanelListener?.onDecVolume()
        }
        mBtnIncVolume.setOnTouchListener(RepeatOnTouchListener(500, 150))
        mBtnIncVolume.setOnClickListener {
            ttsPanelListener?.onIncVolume()
        }
        mVolumeSlider.addOnChangeListener { slider, value, fromUser ->
            if (fromUser)
                ttsPanelListener?.onSetVolume(value.roundToInt())
            val roundValue = (100.0 * value / slider.valueTo).roundToInt()
            mTvVolume.text = context.getString(R.string.volume_arg, roundValue)
        }

        mBtnDecRate.setOnTouchListener(RepeatOnTouchListener(500, 150))
        mBtnDecRate.setOnClickListener {
            ttsPanelListener?.onDecSpeechRate()
        }
        mBtnIncRate.setOnTouchListener(RepeatOnTouchListener(500, 150))
        mBtnIncRate.setOnClickListener {
            ttsPanelListener?.onIncSpeechRate()
        }
        mRateSlider.addOnChangeListener { _, value, fromUser ->
            val rateValue = speechRateFromPercent(value)
            if (fromUser)
                ttsPanelListener?.onSetSpeechRate(rateValue)
            mTvRate.text = context.getString(R.string.rate_arg, rateValue)
        }
    }
}