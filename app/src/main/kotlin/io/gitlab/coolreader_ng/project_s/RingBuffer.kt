/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

internal class RingBuffer constructor(size: Int, initialAvg: Long) {
    private val mSize: Int = size
    private val mArray: LongArray = LongArray(size)
    private var mSum: Long = 0
    private var mAvg: Long = initialAvg
    private var mPos: Int = 0
    private var mCount: Int = 0

    fun average(): Long {
        return mAvg
    }

    fun add(value: Long) {
        if (mCount < mSize) mCount++ else  // array is full
            mSum -= mArray[mPos] // subtract from sum the value to replace
        mArray[mPos] = value // write new value
        mSum += value // update sum
        mAvg = (mSum + mCount / 2) / mCount // calculate average value (with rounding)
        mPos++
        if (mPos >= mSize) mPos = 0
    }

    val isEmpty: Boolean
        get() = 0 == mCount

    fun fill(value: Long) {
        mPos = 0
        mCount = 0
        mSum = 0
        for (i in 0 until mSize) {
            add(value)
        }
    }
}