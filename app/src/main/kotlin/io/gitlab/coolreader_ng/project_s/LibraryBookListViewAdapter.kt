/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.DateFormat
import java.text.NumberFormat
import java.util.Date
import kotlin.math.roundToInt

internal class LibraryBookListViewAdapter(
    private val context: Context,
    private val bookCoverManager: BookCoverManager?,
    private val bookList: List<BookInfo>
) : RecyclerView.Adapter<LibraryBookListViewAdapter.BookItemViewHolder>() {

    interface BookRunnable {
        fun run(bookInfo: BookInfo)
    }

    interface OnSelectionModeChanged {
        fun onSelectionModeChanged(selectionMode: Boolean)
    }

    interface OnSelectionChanged {
        fun onSelectionChanged(selectedItems: Collection<BookInfo>)
    }

    inner class BookInfoViewData(private val ref: BookInfo) {
        // The status of this BookInfo element is either selected (true) or not (false).
        var selected = false
            set(value) {
                if (field != value) {
                    field = value
                    if (value)
                        mSelectedItems.add(ref)
                    else
                        mSelectedItems.remove(ref)
                    onSelectionChangedListener?.onSelectionChanged(mSelectedItems)
                }
            }
    }

    private val mBookViewProps = HashMap<BookInfo, BookInfoViewData>()
    private val mSelectedItems = ArrayList<BookInfo>()

    // extension property for Bookmark
    private val BookInfo.viewProps: BookInfoViewData
        @Synchronized
        get() {
            var viewProps = mBookViewProps[this]
            if (null == viewProps) {
                viewProps = BookInfoViewData(this)
                mBookViewProps[this] = viewProps
            }
            return viewProps
        }

    var onBookInfoRunnable: BookRunnable? = null
    var onCopiedInfoRunnable: Runnable? = null
    var onSelectionModeChangedListener: OnSelectionModeChanged? = null
    var onSelectionChangedListener: OnSelectionChanged? = null
    var isSelectionMode = false
        set(value) {
            if (field != value) {
                field = value
                if (!value) {
                    // deselect all items
                    for ((_, props) in mBookViewProps) {
                        if (props.selected)
                            props.selected = false
                    }
                }
                notifyItemRangeChanged(0, bookList.size)
                onSelectionModeChangedListener?.onSelectionModeChanged(field)
            }
        }

    val selectedItems: Collection<BookInfo>
        get() = mSelectedItems

    inner class BookItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: BookInfo? = null
        val cbSelected = itemView.findViewById<CheckBox>(R.id.cbSelected).also {
            it.setOnCheckedChangeListener { _, isChecked ->
                item?.viewProps?.selected = isChecked
            }
        }
        val ivBookCover = itemView.findViewById<ImageView>(R.id.ivBookCover)
        val tvAuthor = itemView.findViewById<TextView>(R.id.tvAuthor)
        val tvLastRead = itemView.findViewById<TextView>(R.id.tvLastRead)
        val tvTitle = itemView.findViewById<TextView>(R.id.tvTitle)
        val tvSeries = itemView.findViewById<TextView>(R.id.tvSeries)
        val tvStatus = itemView.findViewById<TextView>(R.id.tvStatus)
        val rbRating = itemView.findViewById<RatingBar>(R.id.rbRating)
        val tvProgress = itemView.findViewById<TextView>(R.id.tvProgress)
        val btnInfo = itemView.findViewById<Button>(R.id.btnInfo)
        val btnCopiedInfo = itemView.findViewById<Button>(R.id.btnCopiedInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.library_book_item, parent, false)
        return BookItemViewHolder(itemView)
    }

    override fun getItemCount(): Int = bookList.size

    override fun onBindViewHolder(holder: BookItemViewHolder, position: Int) {
        val item: BookInfo? = if (position >= 0 && position < bookList.size)
            bookList[position]
        else
            null
        holder.item = item
        if (null != item) {
            holder.tvAuthor.text = item.authorsToString(", ")
            if (item.title.isNullOrEmpty())
                holder.tvTitle.text = item.fileInfo.fileName
            else
                holder.tvTitle.text = item.title
            //holder.tvTitle.setHyphenatedText(SettingsManager.activeLang.langTag, item.title)
            holder.tvSeries.text = item.seriesToString()
            holder.tvSeries.visibility =
                if (holder.tvSeries.text.isNotEmpty()) View.VISIBLE else View.GONE
            val status = when (item.readingStatus) {
                BookInfo.ReadingStatus.NONE -> null
                BookInfo.ReadingStatus.IN_READING -> context.getString(R.string.in_reading)
                BookInfo.ReadingStatus.DONE -> context.getString(R.string.finished_reading)
                BookInfo.ReadingStatus.PLANNED -> context.getString(R.string.planned)
            }
            if (null != status) {
                holder.tvStatus.text = status
                holder.tvStatus.visibility = View.VISIBLE
            } else {
                holder.tvStatus.visibility = View.GONE
            }
            if (item.rating > 0) {
                holder.rbRating.rating = item.rating.toFloat()
                holder.rbRating.visibility = View.VISIBLE
            } else {
                holder.rbRating.visibility = View.GONE
            }
            var progress = 0f
            var ts = 0L
            item.lastPosition?.let {
                progress = it.percent.toFloat() / 100f
                ts = it.timeStamp
            }
            if (progress > 0) {
                val numberFormat =
                    NumberFormat.getPercentInstance(SettingsManager.activeLang.locale)
                numberFormat.minimumFractionDigits = 2
                holder.tvProgress.text = context.getString(
                    R.string.reading_progress_,
                    numberFormat.format(progress / 100)
                )
                holder.tvProgress.visibility = View.VISIBLE
            } else {
                holder.tvProgress.visibility = View.GONE
            }
            if (ts > 0) {
                val date = Date(ts)
                val dateFormat =
                    DateFormat.getDateInstance(DateFormat.SHORT, SettingsManager.activeLang.locale)
                holder.tvLastRead.text =
                    context.getString(R.string.last_read_at_, dateFormat.format(date))
                holder.tvLastRead.visibility = View.VISIBLE
            } else {
                holder.tvLastRead.visibility = View.GONE
            }
            if (null != onBookInfoRunnable) {
                holder.btnInfo.visibility = View.VISIBLE
                holder.btnInfo.setOnClickListener {
                    onBookInfoRunnable?.run(item)
                }
            } else {
                holder.btnInfo.visibility = View.GONE
            }
            if (null != onCopiedInfoRunnable) {
                if (Utils.isBookCopiedToInternalAppStorage(context, item))
                    holder.btnCopiedInfo.visibility = View.VISIBLE
                else
                    holder.btnCopiedInfo.visibility = View.GONE
                holder.btnCopiedInfo.setOnClickListener {
                    onCopiedInfoRunnable?.run()
                }
            } else {
                holder.btnCopiedInfo.visibility = View.GONE
            }
            // book cover
            bookCoverManager?.let {
                val maxWidth = Utils.convertUnits(80f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                val maxHeight =
                    Utils.convertUnits(120f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                it.getBookCoverDrawable(
                    item,
                    maxWidth,
                    maxHeight,
                    object : BookCoverManager.BookCoverDrawableResultCallback {
                        override fun onResult(drawable: Drawable?) {
                            BackgroundThread.postGUI {
                                holder.ivBookCover.setImageDrawable(drawable)
                            }
                        }
                    })
            }
            if (isSelectionMode) {
                holder.cbSelected.visibility = View.VISIBLE
                holder.cbSelected.isChecked = item.viewProps.selected
                holder.btnInfo.visibility = View.GONE
            } else {
                holder.cbSelected.visibility = View.GONE
                holder.btnInfo.visibility = View.VISIBLE
            }
        }
    }

    fun isItemSelected(index: Int): Boolean {
        val item: BookInfo? = if (index >= 0 && index < bookList.size)
            bookList[index]
        else
            null
        return item?.viewProps?.selected == true
    }

    fun setSelectedItem(index: Int, selected: Boolean) {
        val item: BookInfo? = if (index >= 0 && index < bookList.size)
            bookList[index]
        else
            null
        item?.let {
            it.viewProps.selected = selected
            notifyItemChanged(index)
        }
    }

    fun clearSelection() {
        for ((idx, item) in bookList.withIndex()) {
            if (item.viewProps.selected) {
                item.viewProps.selected = false
                notifyItemChanged(idx)
            }
        }
    }
}
