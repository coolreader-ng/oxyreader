/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

data class ReaderAction constructor(
    val id: String,
    val nameResId: Int,
    val cmd: ReaderCommand,
    val param: Int,
    val iconId: Int = 0,
    var canRepeat: Boolean = false,
    var mayAssignOnKey: Boolean = true,
    var mayAssignOnTap: Boolean = true,
    var activateWithLongMenuKey: Boolean = false
) {

    val isNone: Boolean
        get() = cmd === ReaderActionRegistry.NONE.cmd

    val isRepeat: Boolean
        get() = cmd === ReaderActionRegistry.REPEAT.cmd

    internal fun setActivateWithLongMenuKey(): ReaderAction {
        activateWithLongMenuKey = true
        return this
    }

    internal fun setCanRepeat(): ReaderAction {
        canRepeat = true
        return this
    }

    /*
    internal fun dontAssignOnKey(): ReaderAction {
        mayAssignOnKey = false
        return this
    }
     */

    internal fun dontAssignOnTap(): ReaderAction {
        mayAssignOnTap = false
        return this
    }

}
