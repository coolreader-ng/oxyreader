/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Canvas

abstract class AbstractAnimation {
    /**
     * Override to set automatically increase/decrease animation speed.
     * @param speed new animation speed.
     */
    abstract fun setSpeed(speed: Float)

    /**
     * Override to automatically increase animation progress.
     * @param acceleration position change acceleration, can be less than or equal to 0.
     */
    abstract fun autoIncrementPosition(acceleration: Float)

    /**
     * Override to automatically decrease animation progress.
     * @param acceleration position change acceleration, can be less than or equal to 0.
     */
    abstract fun autoDecrementPosition(acceleration: Float)

    /**
     * Override animation progress.
     * @param x
     * @param y
     *
     * Useful when implementing touch handlers.
     */
    abstract fun overridePosition(x: Int, y: Int)
    abstract fun resetPosition()
    abstract val direction: Int
    abstract val position: Int

    /**
     * Override to set the correct animation ending condition.
     */
    abstract val isAnimationComplete: Boolean

    /**
     * Render the frame on the canvas in the current animation state.
     *
     * @param canvas Canvas to draw.
     * @return true if the frame was successfully rendered, false otherwise.
     */
    abstract fun renderFrame(canvas: Canvas): Boolean
}
