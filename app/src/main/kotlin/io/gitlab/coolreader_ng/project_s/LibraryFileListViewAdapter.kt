/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.io.File
import java.text.DateFormat
import java.util.Date

internal class LibraryFileListViewAdapter(private val fileList: List<FileInfo>) :
    RecyclerView.Adapter<LibraryFileListViewAdapter.FileItemViewHolder>() {

    inner class FileItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: FileInfo? = null
        val ivFileTypeIcon: ImageView = itemView.findViewById(R.id.ivFileTypeIcon)
        val tvFileName: TextView = itemView.findViewById(R.id.tvFileName)
        val tvFileSize: TextView = itemView.findViewById(R.id.tvFileSize)
        val tvFileDate: TextView = itemView.findViewById(R.id.tvFileDate)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.library_file_item, parent, false)
        return FileItemViewHolder(itemView)
    }

    override fun getItemCount(): Int = fileList.size

    override fun onBindViewHolder(holder: FileItemViewHolder, position: Int) {
        val item: FileInfo? = if (position >= 0 && position < fileList.size)
            fileList[position]
        else
            null
        holder.item = item
        if (null != item) {
            val context = holder.tvFileName.context
            if (item.isDirectory) {
                holder.ivFileTypeIcon.setImageResource(R.mipmap.mime_folder)
                holder.tvFileName.text = item.fileName
                holder.tvFileSize.text = context.getString(R.string._dir_)
            } else {
                val format = item.fileName?.let { DocumentFormat.byExtension(it) }
                format?.let {
                    holder.ivFileTypeIcon.setImageResource(it.iconResourceId)
                } ?: run {
                    holder.ivFileTypeIcon.setImageResource(R.mipmap.mime_text_x_generic_template)
                }
                if (item.isArchive) {
                    val f = File(item.archiveName.toString())
                    holder.tvFileName.text = f.name
                    holder.tvFileSize.text =
                        Utils.getHumanReadableFileSize(
                            item.arcSize,
                            context,
                            SettingsManager.activeLang.locale
                        )
                } else {
                    holder.tvFileName.text = item.fileName
                    holder.tvFileSize.text = Utils.getHumanReadableFileSize(
                        item.size,
                        context,
                        SettingsManager.activeLang.locale
                    )
                }
            }
            val dateFormat = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.DEFAULT,
                SettingsManager.activeLang.locale
            )
            holder.tvFileDate.text = dateFormat.format(Date(item.modificationTime))
        }
    }
}
