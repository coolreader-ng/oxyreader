/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.TypedArray
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.activity.addCallback
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.NavUtils
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.preference.Preference
import androidx.preference.PreferenceDataStore
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.Stack

class SettingsActivity : AppCompatActivity(),
    PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    class CREPropsDataStorage(val properties: SRProperties) : PreferenceDataStore() {
        override fun putBoolean(key: String, value: Boolean) {
            val props = translatePrefKey(key, value.toString())
            for (prop in props)
                properties.setProperty(prop.first, prop.second ?: "")
        }

        override fun putInt(key: String, value: Int) {
            val props = translatePrefKey(key, value.toString())
            for (prop in props)
                properties.setProperty(prop.first, prop.second ?: "")
        }

        override fun putString(key: String, value: String?) {
            val props = translatePrefKey(key, value)
            for (prop in props)
                properties.setProperty(prop.first, prop.second ?: "")
        }

        override fun getBoolean(key: String, defValue: Boolean): Boolean {
            return Utils.parseBool(getPrefValue(key), defValue)
        }

        override fun getInt(key: String, defValue: Int): Int {
            return Utils.parseInt(getPrefValue(key), defValue)
        }

        override fun getString(key: String, defValue: String?): String? {
            return getPrefValue(key)
        }

        private fun getPrefValue(pref: String): String {
            val res: String
            when (pref) {
                PREF_KEY_THEME -> res = properties.getProperty(PropNames.App.THEME, "LIGHT")
                PREF_KEY_LANG -> res = properties.getProperty(PropNames.App.LOCALE, "SYSTEM")
                PREF_KEY_FULLSCREEN -> res = properties.getProperty(PropNames.App.FULLSCREEN, "0")
                PREF_KEY_TOOLBAR_POS -> res =
                    properties.getProperty(PropNames.App.TOOLBAR_LOCATION, "0")

                PREF_KEY_SCREEN_ORIENTATION -> res =
                    properties.getProperty(PropNames.App.SCREEN_ORIENTATION, "0")

                PREF_KEY_LANDSCAPE_PAGES -> res =
                    properties.getProperty(PropNames.Engine.PROP_LANDSCAPE_PAGES, "1")

                PREF_KEY_FALLBACK_FONTS -> res =
                    properties.getProperty(PropNames.Engine.PROP_FALLBACK_FONT_FACES, "")

                PREF_KEY_SHOW_BOOKMARKS -> res =
                    properties.getProperty(PropNames.Engine.PROP_HIGHLIGHT_COMMENT_BOOKMARKS, "1")

                PREF_KEY_VIEW_MODE -> res =
                    properties.getProperty(PropNames.Engine.PROP_PAGE_VIEW_MODE, "1")

                PREF_KEY_PAGE_FLIP_ANIM_TYPE -> res =
                    properties.getProperty(PropNames.App.PAGE_ANIM_TYPE, "0")

                PREF_KEY_DOUBLE_TAP_MODE -> {
                    val doubleTapActions = properties.getInt(PropNames.App.DOUBLE_TAP_ACTIONS, 0)
                    val doubleTapSelection =
                        properties.getInt(PropNames.App.DOUBLE_TAP_SELECTION, 0)
                    res = when (doubleTapActions.shl(1) or doubleTapSelection) {
                        0 -> "0"
                        1 -> "1"
                        2 -> "2"
                        else -> "0"   // unsupported
                    }
                }

                PREF_KEY_LONG_TAP_MODE -> {
                    val longTapActions = properties.getInt(PropNames.App.LONG_TAP_ACTIONS, 0)
                    val longTapSelection =
                        properties.getInt(PropNames.App.LONG_TAP_SELECTION, 0)
                    res = when (longTapActions.shl(1) or longTapSelection) {
                        0 -> "0"
                        1 -> "1"
                        2 -> "2"
                        else -> "0"   // unsupported
                    }
                }

                PREF_KEY_ALLOW_VOLUME_KEYS -> res =
                    properties.getProperty(PropNames.App.KEY_ACTIONS_ENABLE_VOLUME_KEYS, "1")

                PREF_KEY_HANDLE_LONG_KEY_PRESS -> res =
                    properties.getProperty(PropNames.App.KEY_ACTIONS_HANDLE_LONG_PRESS, "0")

                PREF_KEY_SCREEN_LOCK -> res =
                    properties.getProperty(PropNames.App.SCREEN_BACKLIGHT_LOCK, "1")

                PREF_KEY_GFF_SERIF_FONT -> res =
                    properties.getProperty(PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE, "")

                PREF_KEY_GFF_SANS_SERIF_FONT -> res =
                    properties.getProperty(PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE, "")

                PREF_KEY_GFF_CURSIVE_FONT -> res =
                    properties.getProperty(PropNames.Engine.PROP_GENERIC_CURSIVE_FONT_FACE, "")

                PREF_KEY_GFF_FANTASY_FONT -> res =
                    properties.getProperty(PropNames.Engine.PROP_GENERIC_FANTASY_FONT_FACE, "")

                PREF_KEY_GFF_MONOSPACE_FONT -> res =
                    properties.getProperty(PropNames.Engine.PROP_GENERIC_MONOSPACE_FONT_FACE, "")

                PREF_KEY_DEBUG_ENABLE_LOG -> res =
                    properties.getProperty(PropNames.App.LOGGING_ALWAYS, "0")

                PREF_KEY_DEBUG_EXPORT_LOGS -> res = "0" // no associated property

                PREF_KEY_TSA_ST_0 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "0",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_1 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "1",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_2 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "2",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_3 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "3",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_4 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "4",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_5 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "5",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_6 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "6",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_7 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "7",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_ST_8 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "8",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_0 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.0",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_1 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.1",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_2 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.2",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_3 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.3",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_4 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.4",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_5 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.5",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_6 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.6",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_7 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.7",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_LT_8 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.8",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_0 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.0",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_1 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.1",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_2 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.2",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_3 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.3",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_4 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.4",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_5 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.5",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_6 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.6",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_7 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.7",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_TSA_DT_8 -> res =
                    properties.getProperty(
                        PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.8",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_MENU -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_MENU}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_BACK -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_BACK}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_LEFT -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_LEFT}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_RIGHT -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_RIGHT}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_SEARCH -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_SEARCH}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_PAGE_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_PAGE_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_VOLUME_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_VOLUME_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_SP_CAMERA -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_CAMERA}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_MENU -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_MENU}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_BACK -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_BACK}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_LEFT -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_LEFT}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_RIGHT -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_RIGHT}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_SEARCH -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_SEARCH}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_PAGE_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_PAGE_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_VOLUME_UP -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_UP}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_VOLUME_DOWN -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_DOWN}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_KEYS_LP_CAMERA -> res =
                    properties.getProperty(
                        "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_CAMERA}",
                        ReaderActionRegistry.NONE.id
                    )

                PREF_KEY_DICTIONARY -> res =
                    properties.getProperty(
                        PropNames.App.DICTIONARY,
                        ExtDictionaryRegistry.DEFAULT.id
                    )

                PREF_KEY_TTS_ENGINE -> res = properties.getProperty(PropNames.App.TTS_ENGINE, "")

                PREF_KEY_TTS_AUTOSET_LANGUAGE -> res =
                    properties.getProperty(PropNames.App.TTS_USE_DOC_LANG, "1")

                PREF_KEY_TTS_LANGUAGE -> res =
                    properties.getProperty(PropNames.App.TTS_FORCE_LANGUAGE, "")

                PREF_KEY_TTS_VOICE -> res = properties.getProperty(PropNames.App.TTS_VOICE, "")
                PREF_KEY_TTS_END_SENTENCE_WORKAROUND -> res =
                    properties.getProperty(PropNames.App.TTS_GOOGLE_END_OF_SENTENCE_ABBR, "")

                PREF_KEY_TTS_USE_DICTIONARY -> res =
                    properties.getProperty(PropNames.App.TTS_USE_DICTIONARY, "")

                PREF_KEY_TTS_USE_IMMOBILITY_TIMEOUT -> res =
                    properties.getProperty(PropNames.App.TTS_USE_IMMOBILITY_TIMEOUT, "0")

                PREF_KEY_TTS_IMMOBILITY_TIMEOUT_VALUE -> res =
                    properties.getProperty(PropNames.App.TTS_IMMOBILITY_TIMEOUT_VALUE, "0")

                PREF_KEY_STARTUP_ACTION -> res =
                    properties.getProperty(PropNames.App.STARTUP_ACTION, "0")

                PREF_BRIGHTNESS_CONTROL_TYPE -> res =
                    properties.getProperty(PropNames.App.FLICK_BACKLIGHT_CONTROL, "0")

                // TODO: other prefs
                else -> res = ""
            }
            return res
        }

        private fun translatePrefKey(key: String, value: String?): Array<Pair<String, String?>> {
            val list = ArrayList<Pair<String, String?>>()
            when (key) {
                PREF_KEY_THEME -> list.add(Pair(PropNames.App.THEME, value))
                PREF_KEY_LANG -> list.add(Pair(PropNames.App.LOCALE, value))
                PREF_KEY_FULLSCREEN -> list.add(Pair(PropNames.App.FULLSCREEN, value))
                PREF_KEY_TOOLBAR_POS -> list.add(Pair(PropNames.App.TOOLBAR_LOCATION, value))
                PREF_KEY_SCREEN_ORIENTATION ->
                    list.add(Pair(PropNames.App.SCREEN_ORIENTATION, value))

                PREF_KEY_LANDSCAPE_PAGES ->
                    list.add(Pair(PropNames.Engine.PROP_LANDSCAPE_PAGES, value))

                PREF_KEY_FALLBACK_FONTS ->
                    list.add(Pair(PropNames.Engine.PROP_FALLBACK_FONT_FACES, value))

                PREF_KEY_SHOW_BOOKMARKS ->
                    list.add(Pair(PropNames.Engine.PROP_HIGHLIGHT_COMMENT_BOOKMARKS, value))

                PREF_KEY_VIEW_MODE -> list.add(Pair(PropNames.Engine.PROP_PAGE_VIEW_MODE, value))
                PREF_KEY_PAGE_FLIP_ANIM_TYPE -> list.add(Pair(PropNames.App.PAGE_ANIM_TYPE, value))
                PREF_KEY_DOUBLE_TAP_MODE -> {
                    when (Utils.parseInt(value, 0)) {
                        1 -> {
                            list.add(Pair(PropNames.App.DOUBLE_TAP_ACTIONS, "0"))
                            list.add(Pair(PropNames.App.DOUBLE_TAP_SELECTION, "1"))
                        }

                        2 -> {
                            list.add(Pair(PropNames.App.DOUBLE_TAP_ACTIONS, "1"))
                            list.add(Pair(PropNames.App.DOUBLE_TAP_SELECTION, "0"))
                        }

                        else -> {
                            list.add(Pair(PropNames.App.DOUBLE_TAP_ACTIONS, "0"))
                            list.add(Pair(PropNames.App.DOUBLE_TAP_SELECTION, "0"))
                        }
                    }
                }

                PREF_KEY_LONG_TAP_MODE -> {
                    when (Utils.parseInt(value, 0)) {
                        1 -> {
                            list.add(Pair(PropNames.App.LONG_TAP_ACTIONS, "0"))
                            list.add(Pair(PropNames.App.LONG_TAP_SELECTION, "1"))
                        }

                        2 -> {
                            list.add(Pair(PropNames.App.LONG_TAP_ACTIONS, "1"))
                            list.add(Pair(PropNames.App.LONG_TAP_SELECTION, "0"))
                        }

                        else -> {
                            list.add(Pair(PropNames.App.LONG_TAP_ACTIONS, "0"))
                            list.add(Pair(PropNames.App.LONG_TAP_SELECTION, "0"))
                        }
                    }
                }

                PREF_KEY_ALLOW_VOLUME_KEYS ->
                    list.add(Pair(PropNames.App.KEY_ACTIONS_ENABLE_VOLUME_KEYS, value))

                PREF_KEY_HANDLE_LONG_KEY_PRESS ->
                    list.add(Pair(PropNames.App.KEY_ACTIONS_HANDLE_LONG_PRESS, value))

                PREF_KEY_SCREEN_LOCK -> list.add(Pair(PropNames.App.SCREEN_BACKLIGHT_LOCK, value))
                PREF_KEY_GFF_SERIF_FONT ->
                    list.add(Pair(PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE, value))

                PREF_KEY_GFF_SANS_SERIF_FONT ->
                    list.add(Pair(PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE, value))

                PREF_KEY_GFF_CURSIVE_FONT ->
                    list.add(Pair(PropNames.Engine.PROP_GENERIC_CURSIVE_FONT_FACE, value))

                PREF_KEY_GFF_FANTASY_FONT ->
                    list.add(Pair(PropNames.Engine.PROP_GENERIC_FANTASY_FONT_FACE, value))

                PREF_KEY_GFF_MONOSPACE_FONT ->
                    list.add(Pair(PropNames.Engine.PROP_GENERIC_MONOSPACE_FONT_FACE, value))

                PREF_KEY_DEBUG_ENABLE_LOG -> list.add(Pair(PropNames.App.LOGGING_ALWAYS, value))
                PREF_KEY_DEBUG_EXPORT_LOGS -> { /* nothing to add */
                }

                PREF_KEY_TSA_ST_0 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "0", value))

                PREF_KEY_TSA_ST_1 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "1", value))

                PREF_KEY_TSA_ST_2 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "2", value))

                PREF_KEY_TSA_ST_3 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "3", value))

                PREF_KEY_TSA_ST_4 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "4", value))

                PREF_KEY_TSA_ST_5 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "5", value))

                PREF_KEY_TSA_ST_6 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "6", value))

                PREF_KEY_TSA_ST_7 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "7", value))

                PREF_KEY_TSA_ST_8 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "8", value))

                PREF_KEY_TSA_LT_0 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.0", value))

                PREF_KEY_TSA_LT_1 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.1", value))

                PREF_KEY_TSA_LT_2 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.2", value))

                PREF_KEY_TSA_LT_3 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.3", value))

                PREF_KEY_TSA_LT_4 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.4", value))

                PREF_KEY_TSA_LT_5 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.5", value))

                PREF_KEY_TSA_LT_6 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.6", value))

                PREF_KEY_TSA_LT_7 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.7", value))

                PREF_KEY_TSA_LT_8 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "long.8", value))

                PREF_KEY_TSA_DT_0 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.0", value))

                PREF_KEY_TSA_DT_1 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.1", value))

                PREF_KEY_TSA_DT_2 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.2", value))

                PREF_KEY_TSA_DT_3 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.3", value))

                PREF_KEY_TSA_DT_4 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.4", value))

                PREF_KEY_TSA_DT_5 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.5", value))

                PREF_KEY_TSA_DT_6 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.6", value))

                PREF_KEY_TSA_DT_7 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.7", value))

                PREF_KEY_TSA_DT_8 ->
                    list.add(Pair(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE + "double.8", value))

                PREF_KEY_KEYS_SP_MENU ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_MENU}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_BACK ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_BACK}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_LEFT ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_LEFT}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_RIGHT ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_RIGHT}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_SEARCH ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_SEARCH}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_PAGE_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_PAGE_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_VOLUME_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_VOLUME_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_SP_CAMERA ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_CAMERA}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_MENU ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_MENU}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_BACK ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_BACK}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_LEFT ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_LEFT}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_RIGHT ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_RIGHT}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_SEARCH ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_SEARCH}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_PAGE_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_PAGE_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_VOLUME_UP ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_UP}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_VOLUME_DOWN ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_DOWN}",
                            value
                        )
                    )

                PREF_KEY_KEYS_LP_CAMERA ->
                    list.add(
                        Pair(
                            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_CAMERA}",
                            value
                        )
                    )

                PREF_KEY_DICTIONARY ->
                    list.add(Pair(PropNames.App.DICTIONARY, value))

                PREF_KEY_TTS_ENGINE -> list.add(Pair(PropNames.App.TTS_ENGINE, value))
                PREF_KEY_TTS_AUTOSET_LANGUAGE ->
                    list.add(Pair(PropNames.App.TTS_USE_DOC_LANG, value))

                PREF_KEY_TTS_LANGUAGE -> list.add(Pair(PropNames.App.TTS_FORCE_LANGUAGE, value))
                PREF_KEY_TTS_VOICE -> list.add(Pair(PropNames.App.TTS_VOICE, value))
                PREF_KEY_TTS_END_SENTENCE_WORKAROUND ->
                    list.add(Pair(PropNames.App.TTS_GOOGLE_END_OF_SENTENCE_ABBR, value))

                PREF_KEY_TTS_USE_DICTIONARY ->
                    list.add(Pair(PropNames.App.TTS_USE_DICTIONARY, value))

                PREF_KEY_TTS_USE_IMMOBILITY_TIMEOUT ->
                    list.add(Pair(PropNames.App.TTS_USE_IMMOBILITY_TIMEOUT, value))

                PREF_KEY_TTS_IMMOBILITY_TIMEOUT_VALUE ->
                    list.add(Pair(PropNames.App.TTS_IMMOBILITY_TIMEOUT_VALUE, value))

                PREF_KEY_STARTUP_ACTION -> list.add(Pair(PropNames.App.STARTUP_ACTION, value))

                PREF_BRIGHTNESS_CONTROL_TYPE ->
                    list.add(Pair(PropNames.App.FLICK_BACKLIGHT_CONTROL, value))

                // TODO: add other prefs
                else -> list.add(Pair(key, value))
            }
            return list.toTypedArray()
        }
    }

    interface SettingsFragmentStorageHolder {
        fun setProperties(props: SRProperties)
        fun resetToDefaults()
    }

    private var mCurrentThemeResId = -1
    private var mProperties = SRProperties()
    private lateinit var mPrevProperties: SRProperties
    private var mParentActivityName: String? = null
    private lateinit var mMainLayout: View
    private lateinit var mTopAppBar: MaterialToolbar
    private val mTitleStack = Stack<String>()
    private val mAppSettingsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.getStringExtra("action")
            log.debug("received action: $action")
            when (action) {
                "finish" -> {
                    log.debug("finish this activity")
                    this@SettingsActivity.finish()
                }
            }
        }
    }

    override fun setTheme(resId: Int) {
        super.setTheme(resId)
        mCurrentThemeResId = resId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        var toolbarLocation = PropNames.App.TOOLBAR_LOCATION_NONE
        var goToFragment: String? = null
        intent?.let {
            mParentActivityName = it.getStringExtra("parent")
            val settings = it.getBundleExtra("settings")
            if (null != settings)
                mProperties = SRProperties(settings)
            val themeName = it.getStringExtra("theme")
            themeName?.let { name -> setActivityTheme(name) }
            toolbarLocation = it.getIntExtra("toolbar", PropNames.App.TOOLBAR_LOCATION_NONE)
            goToFragment = it.getStringExtra("fragment")
        }
        mPrevProperties = SRProperties(mProperties)

        enableEdgeToEdge()

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_settings)

        mMainLayout = findViewById<View>(R.id.mainLayout)

        val appBarLayout = findViewById<AppBarLayout>(R.id.appBarLayout)
        mTopAppBar = findViewById(R.id.topAppBar)
        val nestedScrollView = findViewById<NestedScrollView>(R.id.nestedScrollView)
        val bottomAppBar = findViewById<BottomAppBar>(R.id.bottomAppBar)

        val attrs = intArrayOf(android.R.attr.actionBarSize)
        val a: TypedArray = obtainStyledAttributes(attrs)
        val bottomToolbarSize = a.getDimensionPixelSize(0, 0)
        a.recycle()

        val mainFragment = SettingsFragmentMain()
        mainFragment.setProperties(mProperties)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.replace(R.id.settings_container, mainFragment)
        transaction.commit()
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount < mTitleStack.size && mTitleStack.isNotEmpty()) {
                // fragment popped from BackStack
                mTopAppBar.title = mTitleStack.pop()
            }
        }

        when (toolbarLocation) {
            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.VISIBLE
                nestedScrollView.setPadding(0, 0, 0, bottomToolbarSize)
            }

            else -> {
                // This activity is completely useless without the toolbar.
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }
        }

        updateSystemBars()
        ViewCompat.setOnApplyWindowInsetsListener(mMainLayout) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            if (appBarLayout.isVisible) {
                mTopAppBar.updatePadding(insets.left, insets.top, insets.right, 0)
                nestedScrollView.updatePadding(insets.left, 0, insets.right, insets.bottom)
            } else if (bottomAppBar.isVisible) {
                bottomAppBar.updatePadding(insets.left, 0, insets.right, insets.bottom)
                nestedScrollView.updatePadding(
                    insets.left,
                    insets.top,
                    insets.right,
                    bottomToolbarSize + insets.bottom
                )
            } else {
                nestedScrollView.updatePadding(insets.left, insets.top, insets.right, insets.bottom)
            }
            WindowInsetsCompat.CONSUMED
        }

        val backAction = Runnable {
            if (supportFragmentManager.backStackEntryCount > 0) {
                supportFragmentManager.popBackStack()
            } else {
                val diff = mProperties.diff(mPrevProperties)
                if (diff.isNotEmpty()) {
                    // Send broadcast only if user changed something
                    val data = Intent()
                    data.setPackage(packageName)
                    data.putExtra("settings", mProperties.toBundle())
                    data.action = "${SettingsActivity::class.qualifiedName}.get_settings"
                    sendBroadcast(data)
                }
                // The system may have cleared the activity stack, so a simple call to finish()
                // will simply terminate the application.
                // To avoid this, we must explicitly specify the activity to return to.
                mParentActivityName?.let {
                    NavUtils.navigateUpTo(this, Intent(it).apply { setPackage(packageName) })
                } ?: run { finish() }
            }
        }
        val onMenuItemClickListener = Toolbar.OnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.reset -> {
                    MaterialAlertDialogBuilder(this@SettingsActivity)
                        .setTitle(resources.getString(R.string.reset_to_default))
                        .setMessage(resources.getString(R.string.are_you_sure_you_want_to_reset_these_settings_))
                        .setNeutralButton(resources.getString(R.string.cancel)) { _, _ ->
                            // do nothing
                        }
                        .setPositiveButton(resources.getString(R.string.yes)) { _, _ ->
                            resetToDefaults()
                        }
                        .show()
                    return@OnMenuItemClickListener true
                }

                else -> return@OnMenuItemClickListener false
            }
        }

        mTopAppBar.setNavigationOnClickListener {
            backAction.run()
        }
        bottomAppBar.setNavigationOnClickListener {
            backAction.run()
        }
        onBackPressedDispatcher.addCallback {
            backAction.run()
        }
        mTopAppBar.setOnMenuItemClickListener(onMenuItemClickListener)
        bottomAppBar.setOnMenuItemClickListener(onMenuItemClickListener)

        goToFragment?.let { fragmentClassName ->
            if (fragmentClassName.isNotEmpty()) {
                val fragment = supportFragmentManager.fragmentFactory.instantiate(
                    classLoader,
                    fragmentClassName
                )
                if (fragment is SettingsFragmentStorageHolder)
                    fragment.setProperties(mProperties)
                // Replace the existing Fragment with the new Fragment
                val transaction = supportFragmentManager.beginTransaction()
                transaction.setReorderingAllowed(true)
                transaction.replace(R.id.settings_container, fragment)
                transaction.commit()
            }
        }

        // Register broadcast receiver for ReaderActivity
        ContextCompat.registerReceiver(
            this,
            mAppSettingsReceiver,
            IntentFilter("${ReaderActivity::class.qualifiedName}.action"),
            ContextCompat.RECEIVER_NOT_EXPORTED
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mAppSettingsReceiver)
    }

    override fun onPreferenceStartFragment(
        caller: PreferenceFragmentCompat,
        pref: Preference
    ): Boolean {
        // Instantiate the new Fragment
        val args = pref.extras
        val fragmentClassName = pref.fragment ?: return false
        val fragment = supportFragmentManager.fragmentFactory.instantiate(
            classLoader,
            fragmentClassName
        )
        fragment.arguments = args
        if (fragment is SettingsFragmentStorageHolder)
            fragment.setProperties(mProperties)
        // Replace the existing Fragment with the new Fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        transaction.setCustomAnimations(
            R.anim.show_translate_left,
            R.anim.hide_translate_left,
            R.anim.show_translate_right,
            R.anim.hide_translate_left
        )
        transaction.replace(R.id.settings_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        mTitleStack.push(mTopAppBar.title.toString())
        mTopAppBar.title = pref.title
        return true
    }

    private fun resetToDefaults() {
        val fragment = supportFragmentManager.findFragmentById(R.id.settings_container)
        val recreatedFragment: Fragment

        if (fragment is SettingsFragmentStorageHolder) {
            // Reset preferences
            fragment.resetToDefaults()
            // Recreate preference fragment
            val fragmentClassName = fragment::class.qualifiedName ?: return
            recreatedFragment = supportFragmentManager.fragmentFactory.instantiate(
                classLoader,
                fragmentClassName
            )
            if (recreatedFragment is SettingsFragmentStorageHolder) {
                recreatedFragment.setProperties(mProperties)
            }
            val emptyBackStack = (0 == supportFragmentManager.backStackEntryCount)
            if (!emptyBackStack)
                supportFragmentManager.popBackStack()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            if (!emptyBackStack) {
                // Set up only pop back animation for recreated fragment
                transaction.setCustomAnimations(
                    0,
                    0,
                    R.anim.show_translate_right,
                    R.anim.hide_translate_left
                )
            }
            transaction.replace(R.id.settings_container, recreatedFragment)
            if (!emptyBackStack)
                transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    private fun setActivityTheme(themeCode: String) {
        var theme = try {
            SettingsManager.Theme.valueOf(themeCode)
        } catch (e: Exception) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                SettingsManager.Theme.DAYNIGHT
            else
                SettingsManager.Theme.LIGHT
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (theme == SettingsManager.Theme.DAYNIGHT)
                theme = SettingsManager.Theme.LIGHT
        }
        if (mCurrentThemeResId != theme.themeId) {
            setTheme(theme.themeId)
        }
    }

    @SuppressLint("ResourceType")
    private fun updateSystemBars() {
        var windowLightStatusBar = false
        var windowLightNavigationBar = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(android.R.attr.windowLightStatusBar)
                )
                windowLightStatusBar = attrs.getBoolean(0, true)
                attrs.recycle()
            } else {
                // Force the status bar color to be black because the light background color
                // doesn't match the light color of the indicators, which we can't change.
                windowLightStatusBar = false
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(android.R.attr.windowLightNavigationBar)
                )
                windowLightNavigationBar = attrs.getBoolean(0, true)
                attrs.recycle()
            } else {
                windowLightNavigationBar = false
            }
            window.statusBarColor = Color.TRANSPARENT
        }
        val ctrl = WindowCompat.getInsetsController(window, mMainLayout)
        ctrl.isAppearanceLightStatusBars = windowLightStatusBar
        ctrl.isAppearanceLightNavigationBars = windowLightNavigationBar
    }

    companion object {
        private val log = SRLog.create("settings")

        const val PREF_KEY_THEME = "pref_theme"
        const val PREF_KEY_LANG = "pref_language"
        const val PREF_KEY_FULLSCREEN = "pref_fullscreen"
        const val PREF_KEY_TOOLBAR_POS = "pref_toolbar_position"
        const val PREF_KEY_SCREEN_ORIENTATION = "pref_screen_orientation"
        const val PREF_KEY_LANDSCAPE_PAGES = "pref_landscape_pages"
        const val PREF_KEY_GFF = "generic_font_families"
        const val PREF_KEY_GFF_SERIF_FONT = "pref_gff_serif_font"
        const val PREF_KEY_GFF_SANS_SERIF_FONT = "pref_gff_sans_serif_font"
        const val PREF_KEY_GFF_CURSIVE_FONT = "pref_gff_cursive_font"
        const val PREF_KEY_GFF_FANTASY_FONT = "pref_gff_fantasy_font"
        const val PREF_KEY_GFF_MONOSPACE_FONT = "pref_gff_monospace_font"
        const val PREF_KEY_FALLBACK_FONTS = "pref_fallback_fonts"
        const val PREF_KEY_SHOW_BOOKMARKS = "pref_show_bookmarks"
        const val PREF_KEY_VIEW_MODE = "pref_viewMode"
        const val PREF_KEY_PAGE_FLIP_ANIM_TYPE = "pref_page_flip_anim_type"
        const val PREF_KEY_DOUBLE_TAP_MODE = "pref_dbltap_mode"
        const val PREF_KEY_LONG_TAP_MODE = "pref_longtap_mode"
        const val PREF_KEY_ALLOW_VOLUME_KEYS = "pref_allow_volume_keys"
        const val PREF_KEY_HANDLE_LONG_KEY_PRESS = "pref_handle_long_key_press"
        const val PREF_KEY_SCREEN_LOCK = "pref_screen_lock_duration"
        const val PREF_KEY_TSA_ST = "touch_screen_actions_st"
        const val PREF_KEY_TSA_LT = "touch_screen_actions_lt"
        const val PREF_KEY_TSA_DT = "touch_screen_actions_dt"
        const val PREF_KEY_TSA_ST_0 = "pref_tsa_st_0"
        const val PREF_KEY_TSA_ST_1 = "pref_tsa_st_1"
        const val PREF_KEY_TSA_ST_2 = "pref_tsa_st_2"
        const val PREF_KEY_TSA_ST_3 = "pref_tsa_st_3"
        const val PREF_KEY_TSA_ST_4 = "pref_tsa_st_4"
        const val PREF_KEY_TSA_ST_5 = "pref_tsa_st_5"
        const val PREF_KEY_TSA_ST_6 = "pref_tsa_st_6"
        const val PREF_KEY_TSA_ST_7 = "pref_tsa_st_7"
        const val PREF_KEY_TSA_ST_8 = "pref_tsa_st_8"
        const val PREF_KEY_TSA_LT_0 = "pref_tsa_lt_0"
        const val PREF_KEY_TSA_LT_1 = "pref_tsa_lt_1"
        const val PREF_KEY_TSA_LT_2 = "pref_tsa_lt_2"
        const val PREF_KEY_TSA_LT_3 = "pref_tsa_lt_3"
        const val PREF_KEY_TSA_LT_4 = "pref_tsa_lt_4"
        const val PREF_KEY_TSA_LT_5 = "pref_tsa_lt_5"
        const val PREF_KEY_TSA_LT_6 = "pref_tsa_lt_6"
        const val PREF_KEY_TSA_LT_7 = "pref_tsa_lt_7"
        const val PREF_KEY_TSA_LT_8 = "pref_tsa_lt_8"
        const val PREF_KEY_TSA_DT_0 = "pref_tsa_dt_0"
        const val PREF_KEY_TSA_DT_1 = "pref_tsa_dt_1"
        const val PREF_KEY_TSA_DT_2 = "pref_tsa_dt_2"
        const val PREF_KEY_TSA_DT_3 = "pref_tsa_dt_3"
        const val PREF_KEY_TSA_DT_4 = "pref_tsa_dt_4"
        const val PREF_KEY_TSA_DT_5 = "pref_tsa_dt_5"
        const val PREF_KEY_TSA_DT_6 = "pref_tsa_dt_6"
        const val PREF_KEY_TSA_DT_7 = "pref_tsa_dt_7"
        const val PREF_KEY_TSA_DT_8 = "pref_tsa_dt_8"
        const val PREF_KEY_KEYS_SP = "keys_actions_sp"
        const val PREF_KEY_KEYS_LP = "keys_actions_lp"
        const val PREF_KEY_KEYS_SP_MENU = "pref_keys_sp_menu"
        const val PREF_KEY_KEYS_SP_BACK = "pref_keys_sp_back"
        const val PREF_KEY_KEYS_SP_LEFT = "pref_keys_sp_left"
        const val PREF_KEY_KEYS_SP_RIGHT = "pref_keys_sp_right"
        const val PREF_KEY_KEYS_SP_UP = "pref_keys_sp_up"
        const val PREF_KEY_KEYS_SP_DOWN = "pref_keys_sp_down"
        const val PREF_KEY_KEYS_SP_SEARCH = "pref_keys_sp_search"
        const val PREF_KEY_KEYS_SP_PAGE_UP = "pref_keys_sp_page_up"
        const val PREF_KEY_KEYS_SP_PAGE_DOWN = "pref_keys_sp_page_down"
        const val PREF_KEY_KEYS_SP_VOLUME_UP = "pref_keys_sp_volume_up"
        const val PREF_KEY_KEYS_SP_VOLUME_DOWN = "pref_keys_sp_volume_down"
        const val PREF_KEY_KEYS_SP_CAMERA = "pref_keys_sp_camera"
        const val PREF_KEY_KEYS_LP_MENU = "pref_keys_lp_menu"
        const val PREF_KEY_KEYS_LP_BACK = "pref_keys_lp_back"
        const val PREF_KEY_KEYS_LP_LEFT = "pref_keys_lp_left"
        const val PREF_KEY_KEYS_LP_RIGHT = "pref_keys_lp_right"
        const val PREF_KEY_KEYS_LP_UP = "pref_keys_lp_up"
        const val PREF_KEY_KEYS_LP_DOWN = "pref_keys_lp_down"
        const val PREF_KEY_KEYS_LP_SEARCH = "pref_keys_lp_search"
        const val PREF_KEY_KEYS_LP_PAGE_UP = "pref_keys_lp_page_up"
        const val PREF_KEY_KEYS_LP_PAGE_DOWN = "pref_keys_lp_page_down"
        const val PREF_KEY_KEYS_LP_VOLUME_UP = "pref_keys_lp_volume_up"
        const val PREF_KEY_KEYS_LP_VOLUME_DOWN = "pref_keys_lp_volume_down"
        const val PREF_KEY_KEYS_LP_CAMERA = "pref_keys_lp_camera"
        const val PREF_KEY_DICTIONARY = "pref_dictionary"
        const val PREF_KEY_DEBUG_ENABLE_LOG = "pref_enable_log"
        const val PREF_KEY_DEBUG_EXPORT_LOGS = "pref_export_logs"
        const val PREF_KEY_TTS = "pref_tts_entry"
        const val PREF_KEY_TTS_ENGINE = "pref_tts_engine"
        const val PREF_KEY_TTS_AUTOSET_LANGUAGE = "pref_tts_autoset_language"
        const val PREF_KEY_TTS_LANGUAGE = "pref_tts_language"
        const val PREF_KEY_TTS_VOICE = "pref_tts_voice"
        const val PREF_KEY_TTS_END_SENTENCE_WORKAROUND = "pref_tts_sentence_end_workaround"
        const val PREF_KEY_TTS_USE_DICTIONARY = "pref_tts_use_dictionary"
        const val PREF_KEY_TTS_IMPORT_DICT = "pref_tts_import_dict"
        const val PREF_KEY_TTS_USE_IMMOBILITY_TIMEOUT = "pref_tts_use_immobility_timeout"
        const val PREF_KEY_TTS_IMMOBILITY_TIMEOUT_VALUE = "pref_tts_immobility_timeout_value"
        const val PREF_KEY_STARTUP_ACTION = "pref_startup_action"
        const val PREF_BRIGHTNESS_CONTROL_TYPE = "pref_brightness_control_type"
    }
}