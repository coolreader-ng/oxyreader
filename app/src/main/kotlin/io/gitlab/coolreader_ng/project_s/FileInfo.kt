/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Parcel
import android.os.Parcelable
import java.io.File
import java.util.zip.ZipEntry

@UsedInJNI
class FileInfo : Parcelable {
    // path to directory where file is located
    @UsedInJNI
    var dirPath: String? = null

    // file name w/o path for normal file, with optional path for file inside archive
    @UsedInJNI
    var fileName: String? = null

    // full path+filename
    @UsedInJNI
    var pathName: String? = null

    // full file size
    @UsedInJNI
    var size: Long = 0

    // File modification timestamp
    var modificationTime: Long = 0

    // File last access timestamp
    var lastAccessTime: Long = 0

    @UsedInJNI
    var isArchive = false
    var isDirectory = false

    @UsedInJNI
    var fingerprint: String? = null

    // archive file name with path
    @UsedInJNI
    var archiveName: String? = null

    // file name inside archive, null if this object is not a file inside archive
    val archiveItemName: String?
        get() = if (isArchive && !isDirectory && pathName != null) pathName else null

    // The size of the archive, if the book file is in an archive. 0 otherwise.
    @UsedInJNI
    var arcSize: Long = 0

    val isHidden: Boolean
        get() = pathName!!.startsWith(".")

    /**
     * Get absolute path to file.
     * For plain files, returns /abs_path_to_file/filename.ext
     * For archives, returns /abs_path_to_archive/arc_file_name.zip@/filename_inside_archive.ext
     * @return full path + filename
     */
    val pathNameWA: String?
        get() = if (archiveName != null) archiveName + ARC_SEPARATOR + pathName else pathName
    val basePathWA: String?
        get() = if (archiveName != null) archiveName else pathName

    // Compressed size of the file in archive, if the book file is in an archive. 0 otherwise.
    @UsedInJNI
    var packSize: Long = 0

    // username for online catalogs
    var userName: String? = null

    // password for online catalogs
    var passWord: String? = null

    // some additional information
    var tag: Any? = null

    protected constructor(parcel: Parcel) {
        dirPath = parcel.readString()
        fileName = parcel.readString()
        pathName = parcel.readString()
        archiveName = parcel.readString()
        arcSize = parcel.readLong()
        userName = parcel.readString()
        passWord = parcel.readString()
        size = parcel.readLong()
        packSize = parcel.readLong()
        modificationTime = parcel.readLong()
        lastAccessTime = parcel.readLong()
        isArchive = parcel.readByte().toInt() != 0
        isDirectory = parcel.readByte().toInt() != 0
        fingerprint = parcel.readString()
    }

    constructor(pathName: String) {
        val parts = splitArcName(pathName)
        if (parts[1] != null) {
            // from archive
            isArchive = true
            archiveName = parts[1]
            this.pathName = parts[0]
            val f = File(this.pathName!!)
            fileName = f.name
            dirPath = f.path
            val arc = File(archiveName!!)
            if (arc.isFile && arc.exists()) {
                isArchive = true
                try {
                    arcSize = arc.length()
                    val entries: ArrayList<ZipEntry> =
                        CREngineNGBinding.getArchiveItems(archiveName)
                    for (entry in entries) {
                        val name = entry.name
                        if (!entry.isDirectory && this.pathName == name) {
                            val itemf = File(name)
                            fileName = itemf.name
                            dirPath = itemf.path
                            size = entry.size
                            packSize = entry.compressedSize
                            modificationTime = entry.time
                            break
                        }
                    }
                } catch (e: Exception) {
                    log.error("error while reading contents of $archiveName")
                }
            }
        } else {
            fromFile(File(pathName))
        }
    }

    /*
    val fileNameToDisplay: String
        get() {
            val isSingleFileArchive = isArchive && parent != null && !parent!!.isArchive && archiveName != null
            return if (isSingleFileArchive) File(archiveName).name else filename!!
        }
     */

    private fun fromFile(f: File) {
        modificationTime = f.lastModified()
        if (!f.isDirectory) {
            fileName = f.name
            dirPath = f.parent
            pathName = f.absolutePath
            size = f.length()
        } else {
            fileName = f.name
            dirPath = f.parent
            pathName = f.absolutePath
            isDirectory = true
        }
    }

    constructor(f: File) {
        fromFile(f)
    }

    constructor(v: FileInfo) {
        assign(v)
    }

    private fun assign(v: FileInfo) {
        dirPath = v.dirPath
        fileName = v.fileName
        pathName = v.pathName
        archiveName = v.archiveName
        arcSize = v.arcSize
        size = v.size
        packSize = v.packSize
        isArchive = v.isArchive
        isDirectory = v.isDirectory
        modificationTime = v.modificationTime
        lastAccessTime = v.lastAccessTime
        userName = v.userName
        passWord = v.passWord
        fingerprint = v.fingerprint
    }

    fun pathNameEquals(item: FileInfo): Boolean {
        return isDirectory == item.isDirectory && eq(archiveName, item.archiveName) && eq(
            pathName,
            item.pathName
        )
    }

    fun deleteFile(): Boolean {
        log.debug("deleting book file $this")
        var res = false
        if (isArchive) {
            if (isDirectory)
                return false
            archiveName?.let {
                val arcContents = CREngineNGBinding.getArchiveItems(it)
                if (arcContents.size <= 1) {
                    // Remove only archive with one file
                    // /somepath/somebook.fb2.zip@/somebook.fb2
                    // /somepath/somebook.fb2.zip :
                    //   somebook.fb2
                    // TODO: allow to remove archive with one html/md and multiple images, css, js, etc...
                    val file = File(it)
                    if (!file.exists()) {
                        log.debug(" file not exist!")
                    } else if (file.isDirectory) {
                        log.debug(" directory can't be deleted")
                    } else {
                        res = file.delete()
                    }
                } else {
                    log.debug("  We will not delete this archive file with many subfiles")
                }
            }
        } else {
            pathName?.let {
                val file = File(it)
                if (!file.exists()) {
                    log.debug(" file not exist!")
                } else if (file.isDirectory) {
                    log.debug(" directory can't be deleted")
                } else {
                    res = file.delete()
                }
            }
        }
        log.debug("  delete result: $res")
        return res
    }

    val isFileExists: Boolean
        get() {
            if (isDirectory)
                return false
            return exists()
        }

    /**
     * @return true if item (file, directory, or archive) exists
     */
    fun exists(): Boolean {
        if (isArchive) {
            archiveName?.let {
                // TODO: check file presence in archive
                return File(it).exists()
            }
            return false
        } else {
            pathName?.let {
                return File(it).exists()
            }
        }
        return false
    }

    val isFileReadable: Boolean
        get() {
            if (isDirectory)
                return false
            val file = if (isArchive) {
                File(archiveName ?: "")
            } else {
                File(pathName ?: "")
            }
            return file.canRead()
        }

    /**
     * @return true if item is a directory, which exists and can be written to
     */
    val isWritableDirectory: Boolean
        get() {
            if (!isDirectory || isArchive)
                return false
            val f = File(pathName)
            val isDir = f.isDirectory
            val canWr = f.canWrite()
            /*
            if (!canWr) {
                val testFile = File(f, "cr3test.tmp")
                try {
                    val os = FileOutputStream(testFile, false)
                    os.close()
                    testFile.delete()
                    canWr = true
                } catch (e: FileNotFoundException) {
                    Log.e(TAG, "cannot write $testFile, $e")
                } catch (e: IOException) {
                    Log.e(TAG, "cannot write $testFile, $e")
                }
            }
             */
            return isDir && canWr
        }

    /**
     * @return true if item is a directory, which exists and can be written to
     */
    val isReadableDirectory: Boolean
        get() {
            if (!isDirectory || isArchive)
                return false
            val f = File(pathName)
            val isDir = f.isDirectory
            val canRd = f.canRead()
            return isDir && canRd
        }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(dirPath)
        dest.writeString(fileName)
        dest.writeString(pathName)
        dest.writeString(archiveName)
        dest.writeLong(arcSize)
        dest.writeString(userName)
        dest.writeString(passWord)
        dest.writeLong(size)
        dest.writeLong(packSize)
        dest.writeLong(modificationTime)
        dest.writeLong(lastAccessTime)
        dest.writeByte((if (isArchive) 1 else 0).toByte())
        dest.writeByte((if (isDirectory) 1 else 0).toByte())
        dest.writeString(fingerprint)
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = archiveName?.hashCode() ?: 0
        result = prime * result + arcSize.hashCode()
        result = prime * result + packSize.hashCode()
        result = prime * result + modificationTime.hashCode()
        result = prime * result + (fileName?.hashCode() ?: 0)
        result = prime * result + isArchive.hashCode()
        result = prime * result + isDirectory.hashCode()
        result = prime * result + lastAccessTime.hashCode()
        result = prime * result + (dirPath?.hashCode() ?: 0)
        result = prime * result + (pathName?.hashCode() ?: 0)
        result = prime * result + size.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (other == null)
            return false
        if (javaClass != other.javaClass)
            return false
        val obj = other as FileInfo
        if (archiveName != obj.archiveName)
            return false
        if (arcSize != obj.arcSize)
            return false
        if (packSize != obj.packSize)
            return false
        if (modificationTime != obj.modificationTime)
            return false
        if (fileName != obj.fileName)
            return false
        if (isArchive != obj.isArchive)
            return false
        if (isDirectory != obj.isDirectory)
            return false
        if (lastAccessTime != obj.lastAccessTime)
            return false
        if (dirPath != obj.dirPath)
            return false
        if (pathName != obj.pathName)
            return false
        if (size != obj.size)
            return false
        // Key test: fingerprint
        if (fingerprint != obj.fingerprint)
            return false
        return true
    }

    override fun toString(): String {
        return pathName ?: "(none)"
    }

    companion object {
        private val log = SRLog.create("finfo")

        @JvmField
        val CREATOR: Parcelable.Creator<FileInfo?> = object : Parcelable.Creator<FileInfo?> {
            override fun createFromParcel(parcel: Parcel): FileInfo {
                return FileInfo(parcel)
            }

            override fun newArray(size: Int): Array<FileInfo?> {
                return arrayOfNulls(size)
            }
        }

        /**
         * To separate archive name from file name inside archive.
         */
        const val ARC_SEPARATOR = "@/"

        /**
         * Split archive + file path name by ARC_SEPARATOR
         * @param pathName is pathname like /arc_file_path@/filepath_inside_arc or /file_path
         * @return item[0] is pathname, item[1] is archive name (null if no archive)
         */
        fun splitArcName(pathName: String): Array<String?> {
            val res = arrayOfNulls<String>(2)
            val arcSeparatorPos = pathName.indexOf(ARC_SEPARATOR)
            if (arcSeparatorPos >= 0) {
                // from archive
                res[1] = pathName.substring(0, arcSeparatorPos)
                res[0] = pathName.substring(arcSeparatorPos + ARC_SEPARATOR.length)
            } else {
                res[0] = pathName
            }
            return res
        }

        fun eq(s1: String?, s2: String?): Boolean {
            return if (s1 == null) s2 == null else s1 == s2
        }
    }
}