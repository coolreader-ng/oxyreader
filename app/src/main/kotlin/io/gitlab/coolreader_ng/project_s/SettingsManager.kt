/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Build
import android.view.KeyEvent
import java.util.Locale


class SettingsManager(var context: Context) {

    constructor(context: Context, changeListener: OnSettingsChangeListener)
            : this(context) {
        onSettingsChangeListener = changeListener
    }

    interface OnSettingsChangeListener {
        fun onSettingsChanged(props: SRProperties, oldProps: SRProperties?)
    }

    enum class PressOrTapType {
        NORMAL,
        LONG,
        DOUBLE
    }

    class KeyAction {
        var keyCode = 0
        var pressType = PressOrTapType.NORMAL
        var action: ReaderAction? = null

        constructor(keyCode: Int, type: PressOrTapType, action: ReaderAction?) {
            this.keyCode = keyCode
            this.pressType = type
            this.action = action
        }
    }

    class TapAction {
        var zone = 0
        var tapType = PressOrTapType.NORMAL
        var action: ReaderAction? = null

        constructor(zone: Int, type: PressOrTapType, action: ReaderAction?) {
            this.zone = zone
            this.tapType = type
            this.action = action
        }
    }

    enum class Lang(val code: String, val nameResId: Int) {
        SYSTEM("system", R.string.as_in_the_system),
        EN("en", R.string.options_app_locale_en),
        RU("ru", R.string.options_app_locale_ru);

        val langTag: String
            get() {
                if (code == SYSTEM.code) {
                    val locale = Locale.getDefault()
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        return locale.toLanguageTag()
                    else
                        return locale.language
                } else {
                    return code
                }
            }

        val language: String
            get() {
                if (code == SYSTEM.code) {
                    return Locale.getDefault().language
                } else {
                    val delimiter: Char = if (code.contains('-'))
                        '-'
                    else
                        '_'
                    val parts = code.split(delimiter)
                    if (parts.isNotEmpty())
                        return parts[0]
                    return code
                }
            }

        val locale: Locale
            get() {
                if (code == SYSTEM.code) {
                    return Locale.getDefault()
                } else if (!code.contains('-') && !code.contains('_')) {
                    return Locale(code)
                } else {
                    val delimiter: Char = if (code.contains('-'))
                        '-'
                    else
                        '_'
                    val parts = code.split(delimiter)
                    if (parts.size > 1) {
                        return Locale(parts[0], parts[1])
                    }
                    return Locale(code)
                }
            }

        /*
        companion object {
            @JvmStatic
            fun byCode(code: String): Lang {
                for (lang in values()) {
                    if (lang.code == code)
                        return lang
                }
                if (code.length > 2 && (code.contains('-') || code.contains('_'))) {
                    val delimiter: Char = if (code.contains('-'))
                        '-'
                    else
                        '_'
                    val parts = code.split(delimiter)
                    if (parts.size > 1) {
                        for (lang in values())
                            if (lang.code == parts[0])
                                return lang
                    }
                }
                log.warn("language not found by code $code")
                return SYSTEM
            }
        }
         */
    }

    enum class Theme(val themeId: Int, val nameResId: Int) {
        @TargetApi(Build.VERSION_CODES.Q)
        DAYNIGHT(R.style.AppTheme_DayNight, R.string.options_app_ui_theme_day_night),
        WHITE(R.style.AppTheme_White, R.string.options_app_ui_theme_white),
        BLACK(R.style.AppTheme_Black, R.string.options_app_ui_theme_black),
        LIGHT(R.style.AppTheme_Light, R.string.options_app_ui_theme_light),
        DARK(R.style.AppTheme_Dark, R.string.options_app_ui_theme_dark)
    }

    enum class ScreenOrientation(val code: Int, val nameResId: Int) {
        // "portrait", ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        PORTRAIT(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT, R.string.portrait),

        // "landscape", ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        LANDSCAPE(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE, R.string.landscape),

        // reversePortrait, ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
        PORTRAIT_REVERSE(
            ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT,
            R.string.reverse_portrait
        ),

        // "reverseLandscape", ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
        LANDSCAPE_REVERSE(
            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE,
            R.string.reverse_landscape
        ),

        // "fullSensor", ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        SENSOR(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR, R.string.by_sensor),

        // "fullUser", ActivityInfo.SCREEN_ORIENTATION_FULL_USER, default
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        SYSTEM_SENSOR(ActivityInfo.SCREEN_ORIENTATION_FULL_USER, R.string.by_system);

        companion object {
            @JvmStatic
            fun byOrdinal(ord: Int): ScreenOrientation {
                if (ord >= 0 && ord < values().size)
                    return values()[ord]
                throw IllegalArgumentException("Invalid ordinal value=$ord")
            }

            @JvmStatic
            fun byCode(code: Int): ScreenOrientation {
                for (value in values()) {
                    if (value.code == code)
                        return value
                }
                throw IllegalArgumentException("Invalid code=$code")
            }
        }
    }

    data class PageBackgroundInfo(
        val id: String,
        val nameResId: Int = 0,
        val name: String? = null,
        val tiled: Boolean = false,
        val resId: Int = 0,
        val filePath: String? = null
    ) {
        constructor(id: String, nameResId: Int, tiled: Boolean, resId: Int)
                : this(id, nameResId, null, tiled, resId, null)

        fun isFile(): Boolean {
            return filePath?.isNotEmpty() == true
        }
    }

    var onSettingsChangeListener: OnSettingsChangeListener? = null
    var properties = SRProperties()
        private set

    fun reloadSettings(notify: Boolean) {
        val oldAppProperties = properties
        properties = SRProperties()
        loadSettings()
        if (notify)
            onSettingsChangeListener?.onSettingsChanged(properties, oldAppProperties)
    }

    private fun loadSettings() {
        val appPrefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE)
        properties.putAll(appPrefs.all)
    }

    private fun saveSettings(): Boolean {
        val appPrefs = context.getSharedPreferences("settings", Context.MODE_PRIVATE)
        val oldProps = SRProperties()
        oldProps.putAll(appPrefs.all)
        val editor = appPrefs.edit()
        var removedCount = 0
        var changedCount = 0
        // Find unused items to remove
        for (oldKey in oldProps.keys) {
            if (!properties.keys.contains(oldKey)) {
                editor.remove(oldKey.toString())
                removedCount++
            }
        }
        // Set changed props
        for (entry in properties.entries) {
            if (oldProps.getProperty(entry.key.toString()) != properties.getProperty(entry.key.toString())) {
                editor.putString(entry.key.toString(), entry.value.toString())
                changedCount++
            }
        }
        val res = editor.commit()
        if (res)
            log.verbose("Settings saved, removed $removedCount items, changed $changedCount items")
        else
            log.error("Failed to save settings")
        return res
    }

    /**
     * Set new settings, all old ones will be lost.
     * @param newSettings new option set
     * @param notify notify about changes via provided listener
     */
    fun setSettings(newSettings: SRProperties, notify: Boolean) {
        val diff = newSettings.diff(properties)
        if (diff.isEmpty)
            return
        val oldAppProperties: SRProperties = properties
        properties = SRProperties(newSettings)
        saveSettings()
        if (notify)
            onSettingsChangeListener?.onSettingsChanged(properties, oldAppProperties)
    }

    /**
     * Put option set.
     * If there are already such elements in the current set of options, they will be replaced,
     *  if they are missing, they will be added.
     * No items will be removed.
     * @param settings option set
     * @param notify notify about changes via provided listener
     */
    fun putSettings(settings: SRProperties, notify: Boolean) {
        val props = SRProperties(properties)
        props.setAll(settings)
        val diff = props.diff(properties)
        if (diff.isNotEmpty())
            setSettings(props, notify)
    }

    /**
     * Adding some defaults if they don't already exist.
     * If any of the specified options already exist in the set, they are ignored.
     * @param defaults defaults options set
     * @param notify notify about changes via provided listener
     */
    fun applyDefaults(defaults: SRProperties, notify: Boolean) {
        val props = SRProperties(properties)
        for ((key, value) in defaults) {
            props.applyDefault(key as String, value.toString())
        }
        val diff = props.diff(properties)
        if (diff.isNotEmpty())
            setSettings(props, notify)
    }

    fun setSetting(name: String, value: String, notify: Boolean) {
        if (value == properties.getProperty(name))
            return
        val props = SRProperties(properties)
        props.setProperty(name, value)
        setSettings(props, notify)
    }

    fun setSetting(name: String, value: Boolean, notify: Boolean) {
        setSetting(name, if (value) "1" else "0", notify)
    }

    fun setSetting(name: String, value: Int, notify: Boolean) {
        setSetting(name, value.toString(), notify)
    }

    fun setSetting(name: String, value: Long, notify: Boolean) {
        setSetting(name, value.toString(), notify)
    }

    fun getSetting(name: String): String? {
        return properties.getProperty(name)
    }

    fun getSetting(name: String, defaultValue: String): String {
        return properties.getProperty(name, defaultValue)
    }

    fun notifySettingsChanged() {
        onSettingsChangeListener?.onSettingsChanged(properties, null)
    }

    init {
        loadSettings()
    }

    object Defaults {
        val MAIN = mapOf(
            PropNames.App.THEME to
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                        Theme.DAYNIGHT.name
                    else
                        Theme.LIGHT.name,
            PropNames.App.LOCALE to Lang.SYSTEM.name,
            PropNames.App.FULLSCREEN to "0",
            PropNames.App.TOOLBAR_LOCATION to "${PropNames.App.TOOLBAR_LOCATION_TOP}",
            PropNames.App.SCREEN_ORIENTATION to
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                        "${ScreenOrientation.SYSTEM_SENSOR.ordinal}"
                    else
                        "${ScreenOrientation.SENSOR.ordinal}",
            PropNames.Engine.PROP_LANDSCAPE_PAGES to "2",
            PropNames.Engine.PROP_FALLBACK_FONT_FACES to "Noto Color Emoji; Droid Sans Fallback; Noto Sans CJK SC; Noto Sans Arabic UI; Noto Sans Devanagari UI; FreeSans; FreeSerif; Noto Serif; Noto Sans; Arial Unicode MS",
            PropNames.Engine.PROP_PAGE_VIEW_MODE to "1",
            PropNames.Engine.PROP_FOOTNOTES to "1",
            PropNames.Engine.PROP_HIGHLIGHT_COMMENT_BOOKMARKS to "1",
            PropNames.App.PAGE_ANIM_TYPE to "${ReaderView.PageAnimationType.Slide.ordinal}",
            PropNames.App.DOUBLE_TAP_SELECTION to "0",
            PropNames.App.DOUBLE_TAP_ACTIONS to "0",
            PropNames.App.LONG_TAP_SELECTION to "1",
            PropNames.App.LONG_TAP_ACTIONS to "0",
            PropNames.App.KEY_ACTIONS_ENABLE_VOLUME_KEYS to "1",
            PropNames.App.KEY_ACTIONS_HANDLE_LONG_PRESS to "0",
            PropNames.App.SCREEN_BACKLIGHT to "-1",
            PropNames.App.FLICK_BACKLIGHT_CONTROL to "${ReaderView.BrightnessControlType.None.ordinal}",
            PropNames.App.LOGGING_ALWAYS to "0",
            PropNames.App.NIGHT_MODE to "0",
            PropNames.App.DICTIONARY to ExtDictionaryRegistry.DEFAULT.id,
            PropNames.App.STARTUP_ACTION to "0"
        )
        var GFF: Map<String, String> = mapOf()
            private set
        val TAP_ACTIONS_ST = mapOf(
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}0" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}1" to ReaderActionRegistry.STYLE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}2" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}3" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}4" to ReaderActionRegistry.READER_MENU.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}5" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}6" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}7" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}8" to ReaderActionRegistry.PAGE_DOWN.id,
        )
        val TAP_ACTIONS_LT = mapOf(
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.0" to ReaderActionRegistry.GO_BACK.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.1" to ReaderActionRegistry.OPTIONS.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.2" to ReaderActionRegistry.TOC.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.3" to ReaderActionRegistry.PAGE_UP_10.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.4" to ReaderActionRegistry.TOGGLE_FULLSCREEN.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.5" to ReaderActionRegistry.PAGE_DOWN_10.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.6" to ReaderActionRegistry.PAGE_UP_10.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.7" to ReaderActionRegistry.BOOK_INFO.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}long.8" to ReaderActionRegistry.PAGE_DOWN_10.id,
        )
        val TAP_ACTIONS_DT = mapOf(
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.0" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.1" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.2" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.3" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.4" to ReaderActionRegistry.TOGGLE_FULLSCREEN.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.5" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.6" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.7" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}double.8" to ReaderActionRegistry.NONE.id,
        )
        val KEY_ACTIONS_ST = mapOf(
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_CENTER}" to ReaderActionRegistry.RECENT_BOOKS.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_UP}" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_DOWN}" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_LEFT}" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_DPAD_RIGHT}" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_UP}" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_VOLUME_DOWN}" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_MENU}" to ReaderActionRegistry.READER_MENU.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_CAMERA}" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_SEARCH}" to ReaderActionRegistry.SEARCH.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_UP}" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_PAGE_DOWN}" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_8}" to ReaderActionRegistry.PAGE_DOWN.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_2}" to ReaderActionRegistry.PAGE_UP.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}${KeyEvent.KEYCODE_ESCAPE}" to ReaderActionRegistry.PAGE_DOWN.id,
        )
        val KEY_ACTIONS_LT = mapOf(
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_CENTER}" to ReaderActionRegistry.BOOKMARKS.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_UP}" to ReaderActionRegistry.PAGE_UP_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_DOWN}" to ReaderActionRegistry.PAGE_DOWN_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_LEFT}" to ReaderActionRegistry.REPEAT.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_DPAD_RIGHT}" to ReaderActionRegistry.REPEAT.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_UP}" to ReaderActionRegistry.PAGE_UP_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_VOLUME_DOWN}" to ReaderActionRegistry.PAGE_DOWN_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_MENU}" to ReaderActionRegistry.STYLE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_CAMERA}" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_SEARCH}" to ReaderActionRegistry.TOGGLE_SELECTION_MODE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_UP}" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_PAGE_DOWN}" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_8}" to ReaderActionRegistry.PAGE_DOWN_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_2}" to ReaderActionRegistry.PAGE_UP_10.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}long.${KeyEvent.KEYCODE_ESCAPE}" to ReaderActionRegistry.REPEAT.id
        )
        val KEY_ACTIONS_DT = mapOf(
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}double.${KeyEvent.KEYCODE_MENU}" to ReaderActionRegistry.OPTIONS.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}double.${KeyEvent.KEYCODE_PAGE_UP}" to ReaderActionRegistry.NONE.id,
            "${PropNames.App.KEY_ACTIONS_PRESS_BASE}double.${KeyEvent.KEYCODE_PAGE_DOWN}" to ReaderActionRegistry.NONE.id,
        )
        val TTS = mapOf(
            PropNames.App.TTS_ENGINE to "com.google.android.tts",
            PropNames.App.TTS_SPEED to "1.0",
            PropNames.App.TTS_USE_DOC_LANG to "1",
            PropNames.App.TTS_FORCE_LANGUAGE to "",
            PropNames.App.TTS_VOICE to "",
            PropNames.App.TTS_USE_IMMOBILITY_TIMEOUT to "0",
            PropNames.App.TTS_IMMOBILITY_TIMEOUT_VALUE to "180"
        )
        var ALL_PREFS = MAIN + GFF + TAP_ACTIONS_ST + TAP_ACTIONS_LT + TAP_ACTIONS_DT +
                KEY_ACTIONS_ST + KEY_ACTIONS_LT + KEY_ACTIONS_DT + TTS
            private set

        fun updateGFFDefaults() {
            val fontFaceSerifCandidates = arrayOf(
                "Noto Serif", "FreeSerif", "DejaVu Serif",
                "Liberation Serif", "Droid Serif", "Georgia", "Times",
                "Times New Roman", "Marion", "PT Serif", "Palatino",
                "STSong"
            )
            val fontFaceSansCandidates = arrayOf(
                "Noto Sans", "FreeSans", "DejaVu Sans", "Droid Sans",
                "Liberation Sans", "Roboto", "Helvetica", "PT Sans",
                "Arial", "Verdana", "Tahoma"
            )
            val fontFaceCursiveCandidates = arrayOf(
                "Brush Script MT", "Brush Script Std", "Lucida Calligraphy",
                "Lucida Handwriting", "Dancing Script", "Dancing Script OT",
                "Corsiva", "Zapfino", "Pacifico", "Apple Chancery", "cursive"
            )
            val fontFaceFantasyCandidates = arrayOf(
                "Papyrus", "Herculanum", "Party LET", "Curlz MT",
                "Harrington", "Comic Sans MS", "fantasy"
            )
            val fontFaceMonospaceCandidates = arrayOf(
                "Noto Mono", "FreeMono", "Courier New", "Andale Mono",
                "Liberation Mono", "Ayuthaya", "DejaVu Sans Mono",
                "Droid Sans Mono", "Fira Code", "Inconsolata", "Monaco",
                "Monospaced", "Source Code Pro", "PT Mono", "Menlo"
            )
            var defaultSerifFont = "DejaVu Serif"
            var defaultSansFont = "Roboto"
            var defaultCursiveFont = "Dancing Script"
            var defaultFantasyFont = "Comic Sans MS"
            var defaultMonospaceFont = "Noto Mono"
            for (face in fontFaceSerifCandidates) {
                if (Utils.isFontAvailable(face)) {
                    defaultSerifFont = face
                    break
                }
            }
            for (face in fontFaceSansCandidates) {
                if (Utils.isFontAvailable(face)) {
                    defaultSansFont = face
                    break
                }
            }
            for (face in fontFaceCursiveCandidates) {
                if (Utils.isFontAvailable(face)) {
                    defaultCursiveFont = face
                    break
                }
            }
            for (face in fontFaceFantasyCandidates) {
                if (Utils.isFontAvailable(face)) {
                    defaultFantasyFont = face
                    break
                }
            }
            for (face in fontFaceMonospaceCandidates) {
                if (Utils.isFontAvailable(face)) {
                    defaultMonospaceFont = face
                    break
                }
            }
            val gff = HashMap<String, String>()
            gff[PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE] = defaultSerifFont
            gff[PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE] = defaultSansFont
            gff[PropNames.Engine.PROP_GENERIC_CURSIVE_FONT_FACE] = defaultCursiveFont
            gff[PropNames.Engine.PROP_GENERIC_FANTASY_FONT_FACE] = defaultFantasyFont
            gff[PropNames.Engine.PROP_GENERIC_MONOSPACE_FONT_FACE] = defaultMonospaceFont
            GFF = gff
            ALL_PREFS += GFF
        }
    }

    companion object {
        private val log = SRLog.create("sttgs")

        val INTERNAL_BACKGROUNDS: Array<PageBackgroundInfo> = arrayOf(
            PageBackgroundInfo("bg_paper2", R.string.old_paper2, false, R.raw.bg_paper2),
            PageBackgroundInfo(
                "bg_paper2_portrait",
                R.string.old_paper2_portrait,
                false,
                R.raw.bg_paper2_portrait
            )
        )
        val BACKGROUND_NONE = PageBackgroundInfo("id:none", nameResId = R.string.solid_color)

        val SUPPORTED_THEMES = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            arrayOf(Theme.DAYNIGHT, Theme.LIGHT, Theme.DARK, Theme.WHITE, Theme.BLACK)
        else
            arrayOf(Theme.LIGHT, Theme.DARK, Theme.WHITE, Theme.BLACK)

        val SUPPORTED_LANGUAGES = arrayOf(Lang.SYSTEM, Lang.RU, Lang.EN)

        val SUPPORTED_ORIENTATIONS =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                arrayOf(
                    ScreenOrientation.PORTRAIT, ScreenOrientation.LANDSCAPE,
                    ScreenOrientation.PORTRAIT_REVERSE, ScreenOrientation.LANDSCAPE_REVERSE,
                    ScreenOrientation.SENSOR, ScreenOrientation.SYSTEM_SENSOR
                )
            else
                arrayOf(
                    ScreenOrientation.PORTRAIT, ScreenOrientation.LANDSCAPE,
                    ScreenOrientation.PORTRAIT_REVERSE, ScreenOrientation.LANDSCAPE_REVERSE,
                    ScreenOrientation.SENSOR
                )

        var activeLang = Lang.SYSTEM
    }
}