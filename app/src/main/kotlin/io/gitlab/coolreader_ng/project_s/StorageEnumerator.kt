/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.os.Build
import android.os.Environment
import java.io.File

/**
 * External storage enumerator
 */
object StorageEnumerator {

    data class StorageEntry(val label: String, val shortcut: String, val path: String)

    val storageEntries: List<StorageEntry>
        get() = mStorageEntries

    private var mStorageEntries: ArrayList<StorageEntry> = ArrayList()

    fun scan(context: Context) {
        mStorageEntries.clear()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val externalDirs = context.getExternalFilesDirs(Environment.DIRECTORY_DOWNLOADS)
            var index = 0
            for (dir in externalDirs) {
                if (null != dir) {
                    val path = dir.absolutePath
                    val pos = path.indexOf("/Android/data/")
                    if (pos > 0) {
                        val storagePath = path.substring(0, pos)
                        addStorage(index++, storagePath)
                    }
                }
            }
        } else {
            val storage = Environment.getExternalStorageDirectory()
            addStorage(0, storage.absolutePath)
        }
    }

    /**
     * @param shortcut File system shortcut that return application "Files" by Google (package="com.android.externalstorage.documents")
     *                 "primary" for internal storage,
     *                 "XXXX-XXXX" (file system serial number) for any external storage like sdcard, usb disk, etc.
     * @return path to mount root for given file system if found, null otherwise.
     */
    fun byShortcut(shortcut: String): String? {
        for (entry in mStorageEntries) {
            if (shortcut == entry.shortcut)
                return entry.path
        }
        return null
    }

    private fun addStorage(index: Int, path: String) {
        var label = "?"
        var shortcut = "?"
        if (0 == index) {
            // Primary storage
            label = "Primary"
            shortcut = "primary"
        } else {
            // External SD-card, etc
            label = "External${index}"
            var mpath = path.trim()
            while (mpath.isNotEmpty() && mpath.last() == File.separatorChar)
                mpath = mpath.substring(0, mpath.length - 1)
            val pos = mpath.lastIndexOf(File.separatorChar)
            shortcut = if (pos > 0) mpath.substring(pos + 1) else mpath
        }
        mStorageEntries.add(StorageEntry(label, shortcut, path))
    }
}
