/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Color
import android.net.Uri
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.graphics.Insets
import androidx.core.os.LocaleListCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.core.view.updatePadding
import com.google.android.material.button.MaterialButton
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.tts.TTSControlServiceAccessor
import java.io.File
import java.io.InputStream
import java.util.Date
import java.util.regex.Pattern
import kotlin.math.abs


class ReaderActivity : AppCompatActivity() {

    private inner class ScreenBacklightControl {
        private var mmLastUserActivityTime: Long = 0
        private var mmBacklightTimerTask: Runnable? = null
        private var mmLastUpdateTimeStamp: Long = 0
        fun onUserActivity() {
            mmLastUserActivityTime = Utils.uptime()
            if (Utils.uptimeElapsed(mmLastUpdateTimeStamp) < 5000)
                return
            mmLastUpdateTimeStamp = Utils.uptime()
            if (mRequiredScreenLockTime <= 0) {
                if (isHeld)
                    release()
                return
            }
            if (!mInForeground) {
                log.debug("ScreenBacklightControl: user activity while not started")
                release()
                return
            }
            val wnd = window
            if (!isHeld && null != wnd) {
                log.debug("ScreenBacklightControl: add FLAG_KEEP_SCREEN_ON")
                wnd.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                isHeld = true
                if (mmBacklightTimerTask == null) {
                    log.verbose("ScreenBacklightControl: timer task started")
                    mmBacklightTimerTask = BacklightTimerTask()
                    BackgroundThread.postGUI(
                        mmBacklightTimerTask!!,
                        (mRequiredScreenLockTime / 10)
                    )
                }
            }
        }

        var isHeld: Boolean = false
            private set

        fun release() {
            window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            isHeld = false
            mmBacklightTimerTask = null
            mmLastUpdateTimeStamp = 0
        }

        private inner class BacklightTimerTask : Runnable {
            override fun run() {
                if (mmBacklightTimerTask == null)
                    return
                val elapsed = Utils.uptimeElapsed(mmLastUserActivityTime)
                //log.verbose("ScreenBacklightControl: timer task, elapsed = $elapsed")
                var nextTimerInterval = mRequiredScreenLockTime / 20
                var dim = false
                if (elapsed > mRequiredScreenLockTime * 8 / 10) {
                    nextTimerInterval /= 8
                    dim = true
                }
                if (elapsed > mRequiredScreenLockTime) {
                    log.verbose("ScreenBacklightControl: interval is expired")
                    release()
                } else {
                    BackgroundThread.postGUI(mmBacklightTimerTask!!, nextTimerInterval)
                    if (dim) {
                        updateBacklightBrightness(-0.9f) // reduce by 9%
                    }
                }
            }
        }
    }

    private inner class ReaderActivityControl : ActivityControl {
        private var mmOldOrientation: SettingsManager.ScreenOrientation =
            SettingsManager.ScreenOrientation.PORTRAIT

        /**
         * Update screen brightness lock
         */
        override fun onUserActivity() {
            mBacklightControl.onUserActivity()
            BackgroundThread.executeGUI {
                try {
                    val b: Float
                    // screenBacklightBrightness is 0..100
                    if (mScreenBacklightBrightness >= 0) {
                        val percent = mScreenBacklightBrightness
                        val minb = 8f
                        b = if (percent >= minb) percent / 100f else minb / 100f
                    } else {
                        // use system brightness value
                        b = -1.0f
                    }
                    //log.verbose("Brightness: $b")
                    updateBacklightBrightness(b)
                    // TODO: implement this
                    //updateButtonsBrightness(if (keyBacklightOff) 0.0f else -1.0f)
                } catch (e: Exception) {
                    // ignore
                }
            }
        }

        override fun isFullScreen(): Boolean {
            return mFullscreen
        }

        override fun setFullscreen(value: Boolean) {
            mSettingsManager?.setSetting(PropNames.App.FULLSCREEN, value, true)
        }

        override fun setSetting(name: String, value: Any, notify: Boolean) {
            mSettingsManager?.setSetting(name, value.toString(), notify)
        }

        override fun setSettings(props: SRProperties, notify: Boolean) {
            mSettingsManager?.setSettings(props, notify)
        }

        override fun getScreenBacklightLevel(): Int {
            return mScreenBacklightBrightness
        }

        override fun setScreenBacklightLevel(backlight: Int) {
            setScreenBacklightLevelImpl(backlight)
        }

        override fun showToast(message: String) {
            showToast(message, Toast.LENGTH_LONG)
        }

        override fun showToast(resId: Int) {
            showToast(getString(resId), Toast.LENGTH_LONG)
        }

        override fun showToast(resId: Int, vararg formatArgs: Any?) {
            showToast(getString(resId, *formatArgs), Toast.LENGTH_LONG)
        }

        override fun showBookInfo(bookInfo: BookInfo) {
            showBookInfoImpl(bookInfo)
        }

        override fun copyBookQuote(bookInfo: BookInfo, quote: String) {
            val title =
                bookInfo.title ?: (bookInfo.fileInfo.fileName ?: getString(R.string.untitled))
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(getString(R.string.quote_from__, title), quote)
            clipboard.setPrimaryClip(clip)
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                // Only show a toast for Android 12 and lower.
                showToast(R.string.quote_copied_from__, title)
            }
        }

        override fun shareBookQuote(bookInfo: BookInfo, quote: String) {
            val title =
                bookInfo.title ?: (bookInfo.fileInfo.fileName ?: getString(R.string.untitled))
            val authors = bookInfo.authors
            val subject = StringBuilder()
            if (!authors.isNullOrEmpty()) {
                val it = authors.iterator()
                while (it.hasNext()) {
                    subject.append(it.next())
                    if (it.hasNext())
                        subject.append(", ")
                }
                subject.append(" ")
            }
            subject.append(title)
            startActivity(Intent.createChooser(Intent(Intent.ACTION_SEND).apply {
                setType("text/plain")
                putExtra(Intent.EXTRA_SUBJECT, subject.toString())
                putExtra(Intent.EXTRA_TEXT, quote)
            }, null))
        }

        override fun showBookmarkEditFragment(
            bookmark: Bookmark,
            isNew: Boolean,
            actionsListener: BookmarkEditDialogFragment.OnActionsListener
        ) {
            showBookmarkEditFragmentImpl(bookmark, isNew, actionsListener)
        }

        override fun saveDataToFile(bookInfo: BookInfo, data: ByteArray): BookInfo? {
            val destDir = getDir("books", MODE_PRIVATE)
            return Utils.saveBookDataToFile(destDir, bookInfo, data)
        }

        override fun lockOrientation() {
            mmOldOrientation = mScreenOrientation
            when (mmOldOrientation) {
                SettingsManager.ScreenOrientation.SENSOR,
                SettingsManager.ScreenOrientation.SYSTEM_SENSOR -> {
                    // Get actual screen orientation
                    var orientation = SettingsManager.ScreenOrientation.PORTRAIT
                    try {
                        orientation =
                            SettingsManager.ScreenOrientation.byCode(resources.configuration.orientation)
                    } catch (_: Exception) {
                    }
                    setScreenOrientation(orientation)
                }

                else -> {}
            }
        }

        override fun unlockOrientation() {
            when (mmOldOrientation) {
                SettingsManager.ScreenOrientation.SENSOR,
                SettingsManager.ScreenOrientation.SYSTEM_SENSOR -> {
                    setScreenOrientation(mmOldOrientation)
                }

                else -> {}
            }
        }

        override fun openSettings(fragmentClassName: String?) {
            showSettingsImpl(fragmentClassName)
        }

        override fun openAboutDialog() {
            showAboutDialogImpl()
        }

        override fun openLibrary(pageNo: Int) {
            openLibraryImpl(pageNo, true)
        }

        override fun openFileSelector() {
            openFileSelectorImpl()
        }

        override fun openExternalDictionary(text: String) {
            openExternalDictionaryImpl(text)
        }

        override fun requestPostNotificationPermissions() {
            requestPostNotificationPermissions_impl()
        }

        override fun openURL(url: String, confirmation: Boolean) {
            openURLImpl(url, confirmation)
        }

        override fun openSearchFragment(bookInfo: BookInfo) {
            openSearchFragmentImpl(bookInfo)
        }

        override fun overrideToScrollViewMode() {
            if (!mOverriddenScrollMode) {
                mOverriddenScrollMode = true
                updateSystemBars()
            }
        }

        override fun restoreViewMode() {
            if (mOverriddenScrollMode) {
                mOverriddenScrollMode = false
                updateSystemBars()
            }
        }

        override fun exitReader() {
            //finish()
            finishAffinity()
            // Send broadcast to close all app activity
            val data = Intent()
            data.setPackage(packageName)
            data.putExtra("action", "finish")
            data.action = "${ReaderActivity::class.qualifiedName}.action"
            sendBroadcast(data)
        }
    }

    private var mVersion = "0.0"
    private var mJustCreated = false
    private var mInForeground = false
    private var mIsLargeLayout = false
    private var mCurrentThemeResId = -1
    private var mEngineObjects: ReaderEngineObjectsAccessor.EngineObjects? = null
    private var mReaderView: ReaderView? = null
    private lateinit var mReaderViewGroup: ViewGroup
    private lateinit var mTopReaderToolbarView: ViewGroup
    private lateinit var mBottomReaderToolbarView: ViewGroup
    private var mReaderOverflowMenu: PopupMenu? = null
    private var mSettingsManager: SettingsManager? = null
    private val settings: SRProperties
        get() {
            return mSettingsManager?.properties ?: SRProperties()
        }
    private var mFullscreen = false
    private var mInScrollViewMode = false
    private var mOverriddenScrollMode = false

    private val mBatteryChangeReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0)
            val plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0)
            val level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0)
            val charger = when (plugged) {
                BatteryManager.BATTERY_PLUGGED_AC -> ReaderView.BatteryCharger.AC
                BatteryManager.BATTERY_PLUGGED_USB -> ReaderView.BatteryCharger.USB
                BatteryManager.BATTERY_PLUGGED_WIRELESS -> ReaderView.BatteryCharger.Wireless
                else -> ReaderView.BatteryCharger.None
            }
            val batteryState = when (status) {
                BatteryManager.BATTERY_STATUS_CHARGING -> ReaderView.BatteryState.Charging
                BatteryManager.BATTERY_STATUS_DISCHARGING -> ReaderView.BatteryState.Discharging
                BatteryManager.BATTERY_STATUS_FULL -> ReaderView.BatteryState.Discharging
                BatteryManager.BATTERY_STATUS_NOT_CHARGING -> ReaderView.BatteryState.Discharging
                BatteryManager.BATTERY_STATUS_UNKNOWN -> ReaderView.BatteryState.Discharging
                else -> ReaderView.BatteryState.Discharging
            }
            mReaderView?.setBatteryState(batteryState, charger, level)
        }
    }
    private val mTimeTickReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            mReaderView?.onTimeTickReceived()
        }
    }
    private val mAppSettingsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val bundle = intent?.getBundleExtra("settings")
            val props = bundle?.let { SRProperties(it) } ?: SRProperties()
            if (props.isNotEmpty()) {
                // new settings received from Settings activity
                val changedProps = props.diff(mSettingsManager?.properties ?: SRProperties())
                log.debug("New settings received (size=${changedProps.size})")
                mSettingsManager?.putSettings(changedProps, true)
            } else {
                // Canceled, do nothing
                log.debug("Settings: canceled")
            }
        }
    }
    private var mBatteryChangeReceiverRegistered = false
    private var mTimeTickReceiverRegistered = false
    private var mScreenOrientation = SettingsManager.ScreenOrientation.SYSTEM_SENSOR
    private var mScreenBacklightBrightness = -1
    private var mScreenBacklightDuration = DEF_SCREEN_BACKLIGHT_TIMER_INTERVAL
    private var mScreenBacklightDurationSystem = -1
    private var mRequiredScreenLockTime = DEF_SCREEN_BACKLIGHT_TIMER_INTERVAL
    private val mBacklightControl = ScreenBacklightControl()
    private val mDBServiceAccessor = DBServiceAccessor(this)
    private val mTTSServiceAccessor: TTSControlServiceAccessor? = null
    private val mActivityControl = ReaderActivityControl()
    private var mODTOpenFileSelectorResultLauncher: ActivityResultLauncher<Intent>? = null
    private var mExtDictionaryResultLauncher: ActivityResultLauncher<Intent>? = null
    private var mPostNotificationResultLauncher: ActivityResultLauncher<String>? = null

    override fun setTheme(resId: Int) {
        super.setTheme(resId)
        mCurrentThemeResId = resId
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Get package version
        try {
            val pi =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                    packageManager.getPackageInfo(
                        packageName,
                        PackageManager.PackageInfoFlags.of(0)
                    )
                else
                    packageManager.getPackageInfo(packageName, 0)
            mVersion = pi.versionName
        } catch (_: Exception) {
            mVersion = "-0.0"
        }

        // Build SettingsManager, load settings, set OnSettingsChangeListener listener
        var engineDefaults: SRProperties? = null
        mSettingsManager = SettingsManager(this, object : SettingsManager.OnSettingsChangeListener {
            override fun onSettingsChanged(props: SRProperties, oldProps: SRProperties?) {
                // Firstly apply this activity options
                val changedProps = if (oldProps != null) props.diff(oldProps) else props
                for (prop in changedProps) {
                    onAppSettingChanged(prop.key.toString(), prop.value.toString())
                }
                // And only after that apply the ReaderView options
                //  since some ReaderView options depend on the state of this Activity
                //  for example, "Theme", "Locale", etc.
                mReaderView?.updateSettings(props)
            }
        })
        val themeCode = mSettingsManager?.properties?.getProperty(PropNames.App.THEME)
        themeCode?.let {
            val theme = SettingsManager.Theme.valueOf(themeCode)
            if (mCurrentThemeResId != theme.themeId) {
                setTheme(theme.themeId)
                application.setTheme(theme.themeId)
            }
        }
        val ifaceLangTag = mSettingsManager?.properties?.getProperty(PropNames.App.LOCALE)
        ifaceLangTag?.let { setLanguage(it) }

        enableEdgeToEdge()

        // Only after theme & language is set we can init UI
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reader)

        mJustCreated = true

        Utils.retrieveAndUseDPI(this)
        log.debug("DPI=${Utils.DPI}")
        log.debug("DPI_SCALE=${Utils.DPI_SCALE}")

        StorageEnumerator.scan(this)
        for (entry in StorageEnumerator.storageEntries)
            log.verbose("storage entry: $entry")

        // bind to DBService
        mDBServiceAccessor.bind()

        mIsLargeLayout = resources.getBoolean(R.bool.large_layout)

        mReaderViewGroup = findViewById(R.id.readerViewContainer)
        val topToolbarView = mReaderViewGroup.findViewById<ViewGroup>(R.id.customTopToolbar)
        val bottomToolbarView = mReaderViewGroup.findViewById<ViewGroup>(R.id.customBottomToolbar)
        val topOverflowButton = topToolbarView.findViewById<Button>(R.id.overflow)
        val bottomOverflowButton = bottomToolbarView.findViewById<Button>(R.id.overflow)
        // All other options menu items callbacks must bet set in ReaderView
        topOverflowButton.setOnClickListener { onOpenOverflowReaderMenu(topOverflowButton) }
        bottomOverflowButton.setOnClickListener { onOpenOverflowReaderMenu(bottomOverflowButton) }

        // get engine objects
        val engineObjects = ReaderEngineObjectsAccessor.leaseEngineObjects()

        // init objects this activity
        engineObjects.crEngineNGBinding?.setupEngineData(this)
        // Register non-system fonts in external storage
        for (entry in StorageEnumerator.storageEntries) {
            engineObjects.crEngineNGBinding?.initFontsFromDir(File(entry.path, "fonts"))
        }

        // We must create ReaderView instance ONLY after CREngineNGBinding instance creation
        val readerView = ReaderView(this).also {
            it.decorView = window.decorView
            it.activityControl = mActivityControl
            it.dbServiceAcc = mDBServiceAccessor
            it.genresCollection = GenresCollection.getInstance(this)
            it.requestFocus()
            engineObjects.addRecyclable(it)
            val layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    1f
                )
            val pos = mReaderViewGroup.indexOfChild(topToolbarView) + 1
            mReaderViewGroup.addView(it, pos, layoutParams)
            // Get engine defaults
            engineDefaults = it.getSettings()
        }
        readerView.setupReaderToolbar(topToolbarView)
        readerView.setupReaderToolbar(bottomToolbarView)
        mReaderView = readerView
        mTopReaderToolbarView = topToolbarView
        mBottomReaderToolbarView = bottomToolbarView

        // Cache engine objects
        mEngineObjects = engineObjects

        // Set defaults for omitted properties
        addDefaultsSettingsIfOmitted()
        // Add defaults from the engine for some properties if they are not already set in the main config file.
        engineDefaults?.let { mSettingsManager?.applyDefaults(it, false) }

        // Set DPI
        // Base (no scale) DPI in crengine-ng is 96, in Android - 160
        val engineDPI = (96.0f * Utils.DPI_SCALE).toInt()
        mSettingsManager?.setSetting(PropNames.Engine.PROP_RENDER_DPI, engineDPI, false)

        // validate settings (check compatibility of options)
        validateSettings()

        // Setting loaded in SettingsManager() constructor
        //  now just notify that the settings have changed to apply them.
        mSettingsManager?.notifySettingsChanged()

        // And finally set settings for ReaderView
        readerView.updateSettings(settings)

        // Get battery level
        // ACTION_BATTERY_CHANGED is a sticky broadcast & we pass null instead of receiver, then
        // no receiver is registered -- the function simply returns the sticky Intent that matches filter.
        val intent = registerReceiver(null, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        if (null != intent) {
            // and process this Intent: save received values
            mBatteryChangeReceiver.onReceive(null, intent)
        }

        supportFragmentManager.addFragmentOnAttachListener { _, fragment ->
            if (fragment is SearchDialogFragment) {
                fragment.dbServiceAccessor = mDBServiceAccessor
                fragment.docView = readerView.lvDocViewWrapper
                fragment.onActionsListener = object : SearchDialogFragment.OnActionsListener {
                    override fun onJumpTo(bookInfo: BookInfo, searchResult: SearchResultItem) {
                        readerView.selectSearchResult(searchResult)
                    }
                }
            }
        }

        // Register OnApplyWindowInsetsListener
        ViewCompat.setOnApplyWindowInsetsListener(mReaderViewGroup) { _, windowInsets ->
            var readerViewInsets: Insets
            var allowPageHeaderOverlap = false
            // We need to save window insets for popups for API < 30
            //  because for some reason on these APIs the insets for
            //  the view in PopupWindow are always 0.
            // TODO: find the best way
            var altPopupInsets: Insets? = null
            if (mFullscreen) {
                val insets = windowInsets.getInsets(WindowInsetsCompat.Type.displayCutout())
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
                    altPopupInsets = insets
                // In full screen mode we want to take up the entire screen,
                //  but we make sure that the text is not covered by the display cutouts.
                // (Toolbars are always hidden in full screen mode)
                mTopReaderToolbarView.updatePadding(0, 0, 0, 0)
                mBottomReaderToolbarView.updatePadding(0, 0, 0, 0)
                readerViewInsets = insets
                allowPageHeaderOverlap = true
            } else {
                val insets = windowInsets.getInsets(
                    WindowInsetsCompat.Type.systemBars() or
                            WindowInsetsCompat.Type.displayCutout()
                )
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R)
                    altPopupInsets = insets
                mTopReaderToolbarView.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    top = insets.top
                )
                mBottomReaderToolbarView.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    bottom = insets.bottom
                )
                readerViewInsets = insets
                if (View.VISIBLE == mTopReaderToolbarView.visibility) {
                    readerViewInsets = Insets.of(
                        readerViewInsets.left,
                        0,
                        readerViewInsets.right,
                        readerViewInsets.bottom
                    )
                }
                if (View.VISIBLE == mBottomReaderToolbarView.visibility) {
                    readerViewInsets = Insets.of(
                        readerViewInsets.left,
                        readerViewInsets.top,
                        readerViewInsets.right,
                        0
                    )
                }
            }
            readerView.setPageInsets(readerViewInsets, allowPageHeaderOverlap)
            altPopupInsets?.let { readerView.altPopupInsets = it }
            // Don't consume insets (WindowInsetsCompat.CONSUMED)
            //  since we use child fragments
            windowInsets
        }

        // Register OnBackPressedCallback
        onBackPressedDispatcher.addCallback(this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    var processed = false
                    val fragment =
                        supportFragmentManager.findFragmentById(R.id.substituteFragmentView)
                    fragment?.let {
                        if (it is AbleBackwards)
                            processed = it.onBackPressed()
                        if (!processed) {
                            if (supportFragmentManager.backStackEntryCount > 0) {
                                supportFragmentManager.popBackStack()
                                processed = true
                            }
                        }
                    }
                    if (!processed)
                        processed = mReaderView?.onBackPressed() ?: false
                    if (!processed)
                        this@ReaderActivity.finish()
                }
            }
        )

        // Register broadcast receiver for SettingsActivity
        ContextCompat.registerReceiver(
            this,
            mAppSettingsReceiver,
            IntentFilter("${SettingsActivity::class.qualifiedName}.get_settings"),
            ContextCompat.RECEIVER_NOT_EXPORTED
        )

        // Register a callback to request opening a file selector (API19+)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // (Copy file to library)
            mODTOpenFileSelectorResultLauncher =
                registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                    if (Activity.RESULT_OK == it.resultCode) {
                        val uri = it.data?.data
                        log.debug("activity ACTION_OPEN_DOCUMENT returns; file uri: $uri")
                        uri?.let { fileUri -> processUri(fileUri) }
                    }
                }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            // Register a callback for request post notification result
            mPostNotificationResultLauncher =
                registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
                    log.info("post notification request permission result: $result")
                }
        }

        // Register a callback to open external dictionary activity
        mExtDictionaryResultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                log.debug("external dictionary activity returns; resultCode=${it.resultCode}")
                // TODO: implement result processing
            }
    }

    override fun onPause() {
        log.debug("onPause()")
        // TODO: add required actions
        // Stop any running animation in ReaderView, etc...
        mReaderView?.onActivityPause()
        mInForeground = false
        mBacklightControl.release()
        unregisterReceivers()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        log.debug("onResume()")
        mInForeground = true
        registerReceivers()
        rereadSystemSettings()
        mSettingsManager?.reloadSettings(true)
        // TODO: add required actions
        // TODO: Find new fonts on this device and register them
        mBacklightControl.onUserActivity()
    }

    override fun onPostResume() {
        super.onPostResume()
        // TODO: add required actions
    }

    override fun onStart() {
        log.debug("onStart() enter")
        super.onStart()
        if (mJustCreated) {
            mJustCreated = false
            if (!processIntent(intent)) {
                // the intent can't be processed, running startup action
                var startupAction = settings.getInt(PropNames.App.STARTUP_ACTION, 0)
                if (startupAction < PropNames.App.STARTUP_ACTION_LAST_BOOK ||
                    startupAction > PropNames.App.STARTUP_ACTION_LIBRARY
                )
                    startupAction = PropNames.App.STARTUP_ACTION_LAST_BOOK
                when (startupAction) {
                    PropNames.App.STARTUP_ACTION_LIBRARY -> {
                        openLibraryImpl(-1, false)
                        finish()
                    }

                    PropNames.App.STARTUP_ACTION_LAST_BOOK -> {
                        mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                            override fun run(binder: DBServiceBinder) {
                                binder.getCurrentBook(object : DBService.StringResultCallback {
                                    override fun onResult(result: String) {
                                        if (result.isNotEmpty()) {
                                            binder.unsetCurrentBook()
                                            loadDocument(result, errorRunnable = {
                                                // Show reading menu
                                                mReaderView?.processReaderAction(
                                                    ReaderActionRegistry.READER_MENU
                                                )
                                            })
                                        } else {
                                            // Show reading menu
                                            mReaderView?.processReaderAction(ReaderActionRegistry.READER_MENU)
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            }
        }
        log.debug("onStart() leave")
    }

    override fun onStop() {
        log.debug("onStop() enter")
        super.onStop()
        // We can't save the last logcat write date/time in OnDestroy(),
        //  so we save it here
        if (mEngineObjects?.logcatWriter?.started == true)
            mSettingsManager?.setSetting(PropNames.App.LOGGING_LAST_WRITE_TIME, Date().time, false)
        log.debug("onStop() leave")
    }

    override fun onDestroy() {
        log.debug("onDestroy() enter")
        super.onDestroy()
        unregisterReceiver(mAppSettingsReceiver)
        mDBServiceAccessor.unbind()
        mTTSServiceAccessor?.unbind()
        // Even if the system kills this application without calling onDestroy(),
        // the child logcat process will also be killed.
        mEngineObjects?.logcatWriter?.stop()
        ReaderEngineObjectsAccessor.releaseEngineObject(mEngineObjects)
        log.debug("onDestroy() leave")
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus)
            applyWindowInsets()
    }

    // called that we set launchMode for this activity to "singleInstance"
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        processIntent(intent)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        if ((newConfig.uiMode and Configuration.UI_MODE_NIGHT_MASK) != 0) {
            updateSystemBars()
            updateToolbarColors()
            mReaderView?.onUiThemeOrLanguageChanged()
        }
        // Screen orientation change is handled in ReaderView as view resize
        super.onConfigurationChanged(newConfig)
    }

    private fun processIntent(intent: Intent?): Boolean {
        log.debug("received intent: $intent")
        var processed = false
        if (null == intent || Intent.ACTION_MAIN == intent.action)
            return false
        var fileToOpen: String? = null
        var uri: Uri? = null
        if (Intent.ACTION_VIEW == intent.action) {
            uri = intent.data
            intent.data = null
            if (null != uri)
                fileToOpen = filePathFromUri(uri)
        }
        if (null != intent.extras) {
            if (BuildConfig.BUILD_TYPE == "debug")
                dumpBundle(intent.extras!!)
            if (null == fileToOpen)
                fileToOpen = intent.extras!!.getString("FILE_TO_OPEN")
        }
        if (null != fileToOpen) {
            val pos = fileToOpen.indexOf(FileInfo.ARC_SEPARATOR)
            // Checking that the file exists and is readable
            val file = if (pos > 0) File(fileToOpen.substring(0, pos)) else File(fileToOpen)
            if (!file.exists() || !file.canRead())
                fileToOpen = null
        }
        // TODO: Check if fileToOpen is a font file (or archive with font files)
        //  install fonts into private directory
        if (null != fileToOpen) {
            // Less then Android API 29 and file name is decoded successfully
            // Or Android API 30+ with all files access (MANAGE_EXTERNAL_STORAGE permission)
            log.debug("FILE_TO_OPEN = $fileToOpen")
            loadDocument(fileToOpen)
            processed = true
        } else if (null != uri) {
            // Possible on Android API 29+ file name can't be decoded.
            //  On Android API 29+ even with external storage permissions,
            //  direct access to files is not allowed
            //  (excepting MANAGE_EXTERNAL_STORAGE permission)
            log.debug("URI_TO_OPEN = $uri")
            loadDocumentFromUri(uri)
            processed = true
        } else {
            log.debug("No file to open")
        }
        if (processed) {
            // Close any popups
            // TODO: replace with something one: onBackPressed() can navigate back
            mReaderView?.onBackPressed()
        }
        return processed
    }

    private fun processUri(uri: Uri) {
        // Close any popups
        // TODO: replace with something one: onBackPressed() can navigate back
        mReaderView?.onBackPressed()
        var fileToOpen = filePathFromUri(uri)
        if (null != fileToOpen) {
            val pos = fileToOpen.indexOf(FileInfo.ARC_SEPARATOR)
            // Checking that the file exists and is readable
            val file = if (pos > 0) File(fileToOpen.substring(0, pos)) else File(fileToOpen)
            if (!file.exists() || !file.canRead())
                fileToOpen = null
        }
        if (null != fileToOpen) {
            // Less then Android API 29 and file name is decoded successfully
            // Or Android API 30+ with all files access (MANAGE_EXTERNAL_STORAGE permission)
            log.debug("FILE_TO_OPEN = $fileToOpen")
            loadDocument(fileToOpen)
        } else {
            // Possible on Android API 29+ file name can't be decoded.
            //  On Android API 29+ even with external storage permissions,
            //  direct access to files is not allowed
            //  (excepting MANAGE_EXTERNAL_STORAGE permission)
            log.debug("URI_TO_OPEN = $uri")
            loadDocumentFromUri(uri)
        }
    }

    private fun dumpBundle(bundle: Bundle) {
        log.verbose("Bundle $bundle:")
        for (key in bundle.keySet()) {
            val value = bundle.getString(key)
            log.verbose("  $key=$value")
        }
    }

    private fun filePathFromUri(uri: Uri?): String? {
        if (null == uri)
            return null
        var filePath: String? = null
        val scheme = uri.scheme
        val host = uri.host
        if ("file" == scheme) {
            filePath = uri.path
            // patch for opening of books from ReLaunch (under Nook Simple Touch)
            if (null != filePath) {
                if (filePath.contains("%2F"))
                    filePath = filePath.replace("%2F", "/")
            }
        } else if ("content" == scheme) {
            filePath = if (uri.encodedPath!!.contains("%00")) uri.encodedPath else uri.path
            if (null != filePath) {
                // parse uri from system filemanager
                if (filePath.contains("%00")) {
                    // splitter between archive file name and inner file.
                    filePath = filePath.replace("%00", FileInfo.ARC_SEPARATOR)
                    filePath = Uri.decode(filePath)
                }
                if ("com.android.externalstorage.documents" == host) {
                    // application "Files" by Google, package="com.android.externalstorage.documents"
                    if (Pattern.matches("^/document/.*:.*$", filePath)) {
                        // decode special uri form: /document/primary:<somebody>
                        //                          /document/XXXX-XXXX:<somebody>
                        val shortcut =
                            filePath!!.replaceFirst("^/document/(.*):.*$".toRegex(), "$1")
                        val mountRoot: String? = StorageEnumerator.byShortcut(shortcut)
                        if (mountRoot != null)
                            filePath = filePath.replaceFirst(
                                "^/document/.*:(.*)$".toRegex(),
                                "$mountRoot/$1"
                            )
                    } else if (Pattern.matches("^/tree/.*:/document/.*:.*$", filePath)) {
                        // decode special uri form: /tree/XXXX-XXXX:/document/XXXX-XXXX:<somebody>
                        val shortcut = filePath!!.replaceFirst(
                            "^/tree/(.*):/document/(.*):.*$".toRegex(),
                            "$1"
                        )
                        val mountRoot: String? = StorageEnumerator.byShortcut(shortcut)
                        if (mountRoot != null)
                            filePath = filePath.replaceFirst(
                                "^/tree/.*:/document/.*:(.*)$".toRegex(),
                                "$mountRoot/$1"
                            )
                    }
                } else if ("com.google.android.apps.nbu.files.provider" == host) {
                    // application "Files" by Google, package="com.google.android.apps.nbu.files"
                    if (filePath!!.startsWith("/1////")) {
                        // skip "/1///"
                        filePath = filePath.substring(5)
                        filePath = Uri.decode(filePath)
                    } else if (filePath.startsWith("/1/file:///")) {
                        // skip "/1/file://"
                        filePath = filePath.substring(10)
                        filePath = Uri.decode(filePath)
                    }
                } else {
                    // Try some common conversions...
                    if (filePath!!.startsWith("/file%3A%2F%2F")) {
                        filePath = filePath.substring(14)
                        filePath = Uri.decode(filePath)
                        if (filePath.contains("%20")) {
                            filePath = filePath.replace("%20", " ")
                        }
                    }
                }
            }
        }
        if (null != filePath) {
            val file: File
            val pos = filePath.indexOf(FileInfo.ARC_SEPARATOR)
            file = if (pos > 0) File(filePath.substring(0, pos)) else File(filePath)
            if (!file.exists())
                filePath = null
        }
        return filePath
    }

    private fun loadDocument(
        fileName: String,
        doneRunnable: Runnable? = null,
        errorRunnable: Runnable? = null
    ) {
        // check permissions & file existence
        val pos = fileName.indexOf(FileInfo.ARC_SEPARATOR)
        val file = if (pos > 0) File(fileName.substring(0, pos)) else File(fileName)
        if (!file.exists()) {
            val message = getString(R.string.file_not_found, fileName)
            val dialog = MessageDialog(R.string.error).setMessage(message)
            dialog.showDialog(this)
            errorRunnable?.run()
        } else {
            if (file.canRead()) {
                mReaderView?.loadDocument(fileName, doneRunnable, errorRunnable)
            } else {
                val dialog = MessageDialog(R.string.error, R.string.no_read_permissions)
                dialog.showDialog(this)
                errorRunnable?.run()
            }
        }
    }

    private fun loadDocumentFromUri(
        uri: Uri,
        doneRunnable: Runnable? = null,
        errorRunnable: Runnable? = null
    ) {
        val inputStream: InputStream?
        try {
            inputStream = contentResolver.openInputStream(uri)
            inputStream?.let { stream ->
                uri.path?.let { path ->
                    mReaderView?.loadDocument(stream, path, doneRunnable, errorRunnable)
                } ?: run {
                    throw RuntimeException("failed to get path from URI $uri")
                }
            } ?: run {
                throw RuntimeException("failed to get stream from URI $uri")
            }
        } catch (e: Exception) {
            errorRunnable?.run()
        }
    }

    private fun addDefaultsSettingsIfOmitted() {
        val props = SRProperties(settings)
        // Defaults for application properties
        for ((key, value) in SettingsManager.Defaults.ALL_PREFS) {
            props.applyDefault(key, value)
        }

        props.applyDefault(PropNames.App.SCREEN_BACKLIGHT_LOCK, 1)
        props.applyDefault(PropNames.App.BOUNCE_TAP_INTERVAL, -1)
        //props.applyDefault(PropNames.App.FM_ENABLED, BuildConfig.ENABLE_INT_FM)
        props.applyDefault(PropNames.App.FM_BOOK_PROPERTY_SCAN_ENABLED, true)
        props.applyDefault(PropNames.App.FM_SHOW_COVERPAGES, true)
        props.applyDefault(PropNames.App.FM_COVERPAGE_SIZE, 1)
        // TODO: set defaults for this
        //props.applyDefault(PropNames.App.FM_BOOK_SORT_ORDER, ?)
        //props.applyDefault(PropNames.App.DICTIONARY, ?)
        props.applyDefault(PropNames.App.SELECTION_ACTION, 0)
        props.applyDefault(PropNames.App.MULTI_SELECTION_ACTION, 0)
        props.applyDefault(PropNames.App.SELECTION_PERSIST, 0)
        props.applyDefault(
            PropNames.App.BACKGROUND_IMAGE,
            SettingsManager.INTERNAL_BACKGROUNDS[0].id
        )
        // TODO: set defaults for this
        //props.applyDefault(PropNames.App.EINK_SCREEN_UPDATE_MODE, ?)
        //props.applyDefault(PropNames.App.EINK_SCREEN_UPDATE_INTERVAL, ?)
        //props.applyDefault(PropNames.App.GESTURE_PAGE_FLIPPING, ?)
        //props.applyDefault(PropNames.App.TTS_SPEED, ?)
        //props.applyDefault(PropNames.App.TTS_MOTION_TIMEOUT, ?)
        //props.applyDefault(PropNames.App.TTS_ENGINE, ?)
        //props.applyDefault(PropNames.App.TTS_FORCE_LANGUAGE, ?)
        //props.applyDefault(PropNames.App.TTS_VOICE, ?)
        //props.applyDefault(PropNames.App.TTS_USE_DOC_LANG, ?)
        //props.applyDefault(PropNames.App.TTS_GOOGLE_END_OF_SENTENCE_ABBR, ?)

        // Not needed?
        //props.applyDefault(PropNames.App.VIEW_ANIM_DURATION, ?)

        props.applyDefault(PropNames.App.LOGGING_LAST_WRITE_TIME, 0)

        props.applyDefault(PropNames.App.SCREEN_BACKLIGHT_DAY, -1)
        props.applyDefault(PropNames.App.SCREEN_BACKLIGHT_NIGHT, -1)
        props.applyDefault(PropNames.App.THEME_DAY, SettingsManager.Theme.LIGHT.name)
        props.applyDefault(PropNames.App.THEME_NIGHT, SettingsManager.Theme.DARK.name)

        // TODO: replace by any background image (resource?)
        props.applyDefault(PropNames.App.BACKGROUND_IMAGE_DAY, "")
        props.applyDefault(PropNames.App.BACKGROUND_IMAGE_NIGHT, "")
        props.applyDefault(PropNames.App.FONT_COLOR_DAY, "#000000")
        // TODO: For e-ink must be "#FFFFFF"
        props.applyDefault(PropNames.App.FONT_COLOR_NIGHT, "#D0B070")
        props.applyDefault(PropNames.App.BACKGROUND_COLOR_DAY, "#FFFFFF")
        // TODO: For e-ink must be "#000000"
        props.applyDefault(PropNames.App.BACKGROUND_COLOR_NIGHT, "#101010")
        props.applyDefault(PropNames.App.STATUS_FONT_COLOR_DAY, "#FF000000")
        props.applyDefault(PropNames.App.BACKGROUND_COLOR_NIGHT, "#80000000")
        props.applyDefault(PropNames.App.HIGHLIGHT_SELECTION_COLOR_DAY, "#AAAAAA")
        props.applyDefault(PropNames.App.HIGHLIGHT_BOOKMARK_COLOR_COMMENT_DAY, "#AAAA55")
        props.applyDefault(PropNames.App.HIGHLIGHT_BOOKMARK_COLOR_CORRECTION_DAY, "#C07070")
        props.applyDefault(PropNames.App.HIGHLIGHT_SELECTION_COLOR_NIGHT, "#808080")
        props.applyDefault(PropNames.App.HIGHLIGHT_BOOKMARK_COLOR_COMMENT_NIGHT, "#A09060")
        props.applyDefault(PropNames.App.HIGHLIGHT_BOOKMARK_COLOR_CORRECTION_NIGHT, "#906060")
        // TODO: this properties (above) for e-ink devices defaults

        // Some defaults for engine properties
        // Default values for most engine options applied in crengine-ng in LVDocView::propsUpdateDefaults()
        // We override only some defaults here
        SettingsManager.Defaults.updateGFFDefaults()
        SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE]?.let {
            props.applyDefault(PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE, it)
        }
        SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE]?.let {
            props.applyDefault(PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE, it)
        }
        SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_CURSIVE_FONT_FACE]?.let {
            props.applyDefault(PropNames.Engine.PROP_GENERIC_CURSIVE_FONT_FACE, it)
        }
        SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_FANTASY_FONT_FACE]?.let {
            props.applyDefault(PropNames.Engine.PROP_GENERIC_FANTASY_FONT_FACE, it)
        }
        SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_MONOSPACE_FONT_FACE]?.let {
            props.applyDefault(PropNames.Engine.PROP_GENERIC_MONOSPACE_FONT_FACE, it)
        }
        val defaultSerifFont =
            SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_SERIF_FONT_FACE]
                ?: "Noto Serif"
        val defaultSansFont =
            SettingsManager.Defaults.GFF[PropNames.Engine.PROP_GENERIC_SANS_SERIF_FONT_FACE]
                ?: "Roboto"
        props.applyDefault(PropNames.Engine.PROP_FONT_FACE, defaultSerifFont)
        props.applyDefault(
            PropNames.Engine.PROP_FONT_SIZE,
            Utils.convertUnits(10f, Utils.Units.PT, Utils.Units.PX).toInt()
        )
        props.applyDefault(PropNames.Engine.PROP_FONT_GAMMA, "1.0")
        props.applyDefault(PropNames.Engine.PROP_FONT_HINTING, 2)
        props.applyDefault(PropNames.Engine.PROP_FONT_SHAPING, 2)
        props.applyDefault(PropNames.Engine.PROP_FONT_KERNING_ENABLED, true)
        props.applyDefault(PropNames.Engine.PROP_FLOATING_PUNCTUATION, true)
        props.applyDefault(PropNames.Engine.PROP_FONT_COLOR, "0x00000000")
        props.applyDefault(PropNames.Engine.PROP_BACKGROUND_COLOR, "0x00FFFFFF")
        props.applyDefault(PropNames.Engine.PROP_FONT_ANTIALIASING, 3)
        props.applyDefault(
            PropNames.Engine.PROP_PAGE_MARGIN_TOP,
            Utils.convertUnits(10f, Utils.Units.DP, Utils.Units.PX).toInt()
        )
        props.applyDefault(
            PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM,
            Utils.convertUnits(10f, Utils.Units.DP, Utils.Units.PX).toInt()
        )
        props.applyDefault(
            PropNames.Engine.PROP_PAGE_MARGIN_LEFT,
            Utils.convertUnits(5f, Utils.Units.DP, Utils.Units.PX).toInt()
        )
        props.applyDefault(
            PropNames.Engine.PROP_PAGE_MARGIN_RIGHT,
            Utils.convertUnits(5f, Utils.Units.DP, Utils.Units.PX).toInt()
        )
        props.applyDefault(PropNames.Engine.PROP_FORMAT_SPACE_WIDTH_SCALE_PERCENT, 100)
        props.applyDefault(PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT, 75)
        props.applyDefault(PropNames.Engine.PROP_INTERLINE_SPACE, 100)
        props.applyDefault(PropNames.Engine.PROP_PAGE_VIEW_MODE, 1)
        props.applyDefault(PropNames.Engine.PROP_LANDSCAPE_PAGES, 2)

        props.applyDefault(PropNames.Engine.PROP_STATUS_LINE, 1)
        props.applyDefault(PropNames.Engine.PROP_STATUS_FONT_FACE, defaultSansFont)
        props.applyDefault(
            PropNames.Engine.PROP_STATUS_FONT_SIZE,
            Utils.convertUnits(5f, Utils.Units.PT, Utils.Units.PX).toInt()
        )
        props.applyDefault(PropNames.Engine.PROP_STATUS_FONT_COLOR, "0x00000000")

        props.applyDefault(PropNames.Engine.PROP_TEXTLANG_EMBEDDED_LANGS_ENABLED, true)
        props.applyDefault(PropNames.Engine.PROP_TEXTLANG_HYPHENATION_ENABLED, true)
        props.applyDefault(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, true)

        props.applyDefault(PropNames.Engine.PROP_CACHE_VALIDATION_ENABLED, true)
        props.applyDefault(PropNames.Engine.PROP_MIN_FILE_SIZE_TO_CACHE, 1 * 1024 * 1024)

        mSettingsManager?.setSettings(props, false)
    }

    /**
     * Check options compatibility. Fix incompatibilities.
     */
    private fun validateSettings() {
        // A single tap in the center of the screen should always open the reading menu
        mSettingsManager?.setSetting(
            "${PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE}4",
            ReaderActionRegistry.READER_MENU.id,
            false
        )
        if (settings.getBool(PropNames.App.DOUBLE_TAP_ACTIONS, false) &&
            settings.getBool(PropNames.App.DOUBLE_TAP_SELECTION, false)
        )
            mSettingsManager?.setSetting(PropNames.App.DOUBLE_TAP_SELECTION, false, notify = false)
        if (settings.getBool(PropNames.App.LONG_TAP_ACTIONS, false) &&
            settings.getBool(PropNames.App.LONG_TAP_SELECTION, false)
        )
            mSettingsManager?.setSetting(PropNames.App.LONG_TAP_ACTIONS, false, notify = false)
        // TODO: check other settings compatibility
    }

    private fun onAppSettingChanged(key: String, value: String) {
        when (key) {
            PropNames.App.THEME -> setAppTheme(value)
            PropNames.App.LOCALE -> setLanguage(value)
            PropNames.App.SCREEN_BACKLIGHT_LOCK -> setScreenBacklightDuration(
                Utils.parseInt(
                    value,
                    0
                )
            )

            PropNames.App.FULLSCREEN -> {
                val boolValue = Utils.parseBool(value)
                setFullscreenImpl(boolValue)
                val toolbarLocation = if (boolValue) PropNames.App.TOOLBAR_LOCATION_NONE
                else Utils.parseInt(
                    settings.getProperty(
                        PropNames.App.TOOLBAR_LOCATION,
                        "${PropNames.App.TOOLBAR_LOCATION_TOP}"
                    ),
                    PropNames.App.TOOLBAR_LOCATION_TOP,
                    PropNames.App.TOOLBAR_LOCATION_NONE,
                    PropNames.App.TOOLBAR_LOCATION_BOTTOM
                )
                setToolbarLocationImpl(toolbarLocation)
            }

            PropNames.App.SCREEN_ORIENTATION -> {
                var orientation = try {
                    SettingsManager.ScreenOrientation.byOrdinal(Utils.parseInt(value, 0))
                } catch (e: Exception) {
                    SettingsManager.ScreenOrientation.SYSTEM_SENSOR
                }
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    if (SettingsManager.ScreenOrientation.SYSTEM_SENSOR == orientation)
                        orientation = SettingsManager.ScreenOrientation.SENSOR
                }
                setScreenOrientation(orientation)
            }

            PropNames.App.SCREEN_BACKLIGHT -> setScreenBacklightLevelImpl(
                Utils.parseInt(
                    value,
                    -1,
                    -1,
                    100
                )
            )

            PropNames.App.LOGGING_ALWAYS -> {
                val on = Utils.parseBool(value)
                if (on) {
                    if (mEngineObjects?.logcatWriter?.started != true) {
                        // if logging is not started yet
                        val logsDir = File(filesDir, "logs")
                        LogcatWriter.rotateLogFiles(this, logsDir, "logcat-", 10)
                        val lastLogcatWriteDate =
                            Date(settings.getLong(PropNames.App.LOGGING_LAST_WRITE_TIME, 0))
                        mEngineObjects?.logcatWriter?.start(
                            lastLogcatWriteDate,
                            File(logsDir, "logcat-0.log")
                        )
                    }
                } else {
                    if (mEngineObjects?.logcatWriter?.started == true) {
                        // if logging already started
                        mEngineObjects?.logcatWriter?.stop()
                        mSettingsManager?.setSetting(
                            PropNames.App.LOGGING_LAST_WRITE_TIME,
                            Date().time,
                            false
                        )
                    }
                }
            }

            PropNames.App.TOOLBAR_LOCATION -> {
                val location = if (mFullscreen) PropNames.App.TOOLBAR_LOCATION_NONE
                else Utils.parseInt(
                    value,
                    PropNames.App.TOOLBAR_LOCATION_TOP,
                    PropNames.App.TOOLBAR_LOCATION_NONE,
                    PropNames.App.TOOLBAR_LOCATION_BOTTOM
                )
                setToolbarLocationImpl(location)
            }

            PropNames.App.DICTIONARY -> {
                ExtDictionaryRegistry.currentDictId = value
            }

            PropNames.Engine.PROP_PAGE_VIEW_MODE -> {
                val boolValue = Utils.parseInt(value, -1) == 0
                if (boolValue != mInScrollViewMode) {
                    mInScrollViewMode = boolValue
                    updateSystemBars()
                }
            }

            // TODO: implement other properties changes
        }
    }

    private fun setLanguage(langName: String) {
        val lang = try {
            SettingsManager.Lang.valueOf(langName)
        } catch (e: Exception) {
            SettingsManager.Lang.SYSTEM
        }
        setLanguage(lang)
    }

    private fun setLanguage(lang: SettingsManager.Lang) {
        if (lang != SettingsManager.activeLang) {
            log.debug("setLanguage(): lang=${lang}")
            val locales =
                if (lang == SettingsManager.Lang.SYSTEM)
                    LocaleListCompat.getEmptyLocaleList()
                else
                    LocaleListCompat.create(lang.locale)
            AppCompatDelegate.setApplicationLocales(locales)
            SettingsManager.activeLang = lang
            GenresCollection.reloadGenresFromResource(this)
            mReaderOverflowMenu = null
            mReaderView?.onUiThemeOrLanguageChanged()
        } else {
            log.debug("  ...skipping since this language already applied")
        }
    }

    private fun setAppTheme(themeCode: String) {
        var theme = try {
            SettingsManager.Theme.valueOf(themeCode)
        } catch (e: Exception) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                SettingsManager.Theme.DAYNIGHT
            else
                SettingsManager.Theme.LIGHT
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (theme == SettingsManager.Theme.DAYNIGHT)
                theme = SettingsManager.Theme.LIGHT
        }
        log.debug("setAppTheme(): theme=${theme}")
        if (mCurrentThemeResId != theme.themeId) {
            application.setTheme(theme.themeId)
            setTheme(theme.themeId)
            //updateBackground();
            updateSystemBars()
            updateToolbarColors()
            mReaderView?.onUiThemeOrLanguageChanged()
        } else {
            log.debug("  ...skipping since this theme already applied")
            // Force update system bars & toolbar
            updateSystemBars()
            updateToolbarColors()
        }
    }

    @SuppressLint("ResourceType")
    private fun updateSystemBars() {
        val statusBarColor: Int
        var windowLightStatusBar = false
        var windowLightNavigationBar = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(
                        android.R.attr.statusBarColor,
                        android.R.attr.windowLightStatusBar
                    )
                )
                statusBarColor = attrs.getColor(0, 0)
                windowLightStatusBar = attrs.getBoolean(1, true)
                attrs.recycle()
            } else {
                // Force the status bar color to be black because the light background color
                // doesn't match the light color of the indicators, which we can't change.
                statusBarColor = Color.BLACK
                windowLightStatusBar = false
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                val attrs = theme.obtainStyledAttributes(
                    intArrayOf(android.R.attr.windowLightNavigationBar)
                )
                windowLightNavigationBar = attrs.getBoolean(0, true)
                attrs.recycle()
            } else {
                windowLightNavigationBar = false
            }
            if ((mInScrollViewMode || mOverriddenScrollMode) && mReaderView?.pageInsets?.top != 0) {
                window.statusBarColor = statusBarColor
            } else {
                window.statusBarColor = Color.TRANSPARENT
            }
        }
        val ctrl = WindowCompat.getInsetsController(window, mReaderViewGroup)
        ctrl.isAppearanceLightStatusBars = windowLightStatusBar
        ctrl.isAppearanceLightNavigationBars = windowLightNavigationBar
    }

    @SuppressLint("ResourceType")
    private fun updateToolbarColors() {
        // Get colors from theme (reading theme attributes) & and apply to toolbar
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val a = theme.obtainStyledAttributes(
                intArrayOf(
                    R.attr.toolbarBackground,
                    R.attr.toolbarButtonColor
                )
            )
            val toolbarBackground = a.getColorStateList(0)
            val toolbarButtonColor = a.getColorStateList(1)
            a.recycle()
            val toolbars = arrayOf(mTopReaderToolbarView, mBottomReaderToolbarView)
            toolbarBackground?.let {
                for (toolbar in toolbars)
                    toolbar.backgroundTintList = it
            }
            toolbarButtonColor?.let { colorTint ->
                for (toolbar in toolbars) {
                    for (i in 0 until toolbar.childCount) {
                        val child = toolbar.getChildAt(i)
                        if (child is Button) {
                            child.setTextColor(colorTint)
                            if (child is MaterialButton) {
                                child.iconTint = colorTint
                            }
                        } else if (child is TextView) {
                            child.setTextColor(colorTint)
                        }
                    }
                }
            }
        } else {
            /*
            // < API21
            // Reload toolbar view from resources
            val toolbarView =
                layoutInflater.inflate(R.layout.reader_toolbar, mReaderViewGroup, false)
            mReaderToolbar = ReaderToolbar(
                toolbarView = toolbarView,
                backButton = toolbarView.findViewById(R.id.toolBack),
                contentButton = toolbarView.findViewById(R.id.toolContents),
                chapterView = toolbarView.findViewById(R.id.toolChapter),
                styleButton = toolbarView.findViewById(R.id.toolStyle),
                overflowButton = toolbarView.findViewById(R.id.overflow)
            )
            mReaderToolbar?.overflowButton?.setOnClickListener { onOpenOverflowReaderMenu() }
            mReaderView?.readerToolbar = mReaderToolbar
            // Apply toolbar appearance
            val toolbarLocation = if (mFullscreen) PropNames.App.TOOLBAR_LOCATION_NONE
            else Utils.parseInt(
                settings.getProperty(
                    PropNames.App.TOOLBAR_LOCATION,
                    "${PropNames.App.TOOLBAR_LOCATION_TOP}"
                ),
                PropNames.App.TOOLBAR_LOCATION_TOP,
                PropNames.App.TOOLBAR_LOCATION_NONE,
                PropNames.App.TOOLBAR_LOCATION_BOTTOM
            )
            setToolbarLocationImpl(toolbarLocation)
             */
        }
    }

    private fun setToolbarLocationImpl(location: Int) {
        when (location) {
            PropNames.App.TOOLBAR_LOCATION_NONE -> {
                mTopReaderToolbarView.visibility = View.GONE
                mBottomReaderToolbarView.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                mTopReaderToolbarView.visibility = View.VISIBLE
                mBottomReaderToolbarView.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                mTopReaderToolbarView.visibility = View.GONE
                mBottomReaderToolbarView.visibility = View.VISIBLE
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH)
            mReaderViewGroup.requestApplyInsets()
    }

    private fun setFullscreenImpl(fullscreen: Boolean) {
        mFullscreen = fullscreen
        applyWindowInsets()
    }

    private fun applyWindowInsets() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            val ctrl = WindowCompat.getInsetsController(window, mReaderViewGroup)
            val types = WindowInsetsCompat.Type.systemBars()
            if (mFullscreen)
                ctrl.hide(types)
            else
                ctrl.show(types)
            ctrl.systemBarsBehavior =
                WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        } else {
            // WindowCompat is not suitable here (API19)
            applyWindowInsetsAPILessThan20()
        }
    }

    @Suppress("deprecation")
    private fun applyWindowInsetsAPILessThan20() {
        val fsFlags = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // Flag View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY added in API 19 (KitKat)
            // without this flag  SYSTEM_UI_FLAG_HIDE_NAVIGATION will be force cleared by the system on any user interaction,
            // and SYSTEM_UI_FLAG_FULLSCREEN will be force-cleared by the system if the user swipes from the top of the screen.
            // So use this flags only on API >= 19
            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        } else {
            View.SYSTEM_UI_FLAG_LOW_PROFILE
        }
        val wnd = window ?: return
        if (mFullscreen) {
            wnd.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
            wnd.decorView.systemUiVisibility = wnd.decorView.systemUiVisibility or fsFlags
        } else {
            wnd.setFlags(0, WindowManager.LayoutParams.FLAG_FULLSCREEN)
            wnd.decorView.systemUiVisibility = wnd.decorView.systemUiVisibility and fsFlags.inv()
        }
    }

    private fun setScreenOrientation(orientation: SettingsManager.ScreenOrientation) {
        if (mScreenOrientation != orientation) {
            log.debug("setScreenOrientation(): orientation=$orientation")
            mScreenOrientation = orientation
            Utils.applyScreenOrientation(this, orientation.code)
        }
    }

    /**
     * @param duration 0 = system default, 1..5 == 1..5 minutes
     */
    private fun setScreenBacklightDuration(duration: Int) {
        if (mScreenBacklightDuration != duration * 60 * 1000L) {
            mScreenBacklightDuration = duration * 60 * 1000L
            if (mScreenBacklightDuration > 5 * 60 * 1000L)
                mScreenBacklightDuration = 5 * 60 * 1000L
            rereadSystemSettings()
        }
    }

    private fun setScreenBacklightLevelImpl(backlight: Int) {
        mScreenBacklightBrightness = if (backlight < -1 || backlight > 100)
            -1
        else
            backlight
        mActivityControl.onUserActivity()
        // TODO: for e-ink devices use alternative method
        log.debug("Set screen brightness to ${backlight}")
    }

    private fun showSettingsImpl(fragmentClassName: String? = null) {
        // We cant use startActivityForResult or registerForActivityResult()
        //  since with launchMode="singleInstance" the onActivityResult() callback (and ActivityResultContract family)
        //  is called right after startActivityForResult() on old APIs and we lost the result.
        // So we have to use broadcasts for this case.
        startActivity(Intent(applicationContext, SettingsActivity::class.java).apply {
            setPackage(packageName)
            putExtra("parent", ReaderActivity::class.qualifiedName)
            putExtra("settings", settings.toBundle())
            putExtra("theme", settings.getProperty(PropNames.App.THEME))
            putExtra(
                "toolbar",
                settings.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
            )
            fragmentClassName?.let { putExtra("fragment", fragmentClassName) }
        })
    }

    private fun showAboutDialogImpl() {
        val dialogFragment = AboutFragment()
        val args = Bundle()
        args.putInt(
            "toolbar",
            settings.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
        )
        args.putString("version", mVersion)
        dialogFragment.arguments = args
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setReorderingAllowed(true)
        // setTransition() is not used here as it don't work correctly in full screen mode.
        // TODO: Check it out in the latest version of the androidx.fragment library.
        //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction.add(R.id.substituteFragmentView, dialogFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun showBookInfoImpl(bookInfo: BookInfo) {
        val bookCoverManager = mEngineObjects?.crEngineNGBinding?.let {
            BookCoverManager(
                this,
                mDBServiceAccessor,
                it
            )
        }
        val dialogFragment = BookInfoDialogFragment(bookInfo, bookCoverManager)
        dialogFragment.onActionsListener = object : BookInfoDialogFragment.OnActionsListener {
            override fun onOpenBook(bookInfo: BookInfo) {
                mReaderView?.loadDocument(bookInfo.fileInfo, null, null)
                // TODO: check if it is all what we need
            }

            override fun onInfoChanged(bookInfo: BookInfo) {
                mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.saveBookInfo(bookInfo, object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                if (!result)
                                    showToast(
                                        getString(R.string.failed_to_save_info_),
                                        Toast.LENGTH_LONG
                                    )
                            }
                        })
                    }
                })
            }
        }
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        args.putInt(
            "toolbar",
            settings.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
        )
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(supportFragmentManager, "bookInfo")
        } else {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.add(R.id.substituteFragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun showBookmarkEditFragmentImpl(
        bookmark: Bookmark,
        isNew: Boolean,
        actionsListener: BookmarkEditDialogFragment.OnActionsListener
    ) {
        val dialogFragment = BookmarkEditDialogFragment(bookmark)
        dialogFragment.onActionsListener = actionsListener
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        args.putInt(
            "toolbar",
            settings.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
        )
        args.putBoolean("isEditMode", !isNew)
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(supportFragmentManager, "bookmark")
        } else {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.add(R.id.substituteFragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun openLibraryImpl(pageNo: Int, finishWhenOpeningBook: Boolean) {
        startActivity(Intent(applicationContext, LibraryActivity::class.java).apply {
            setPackage(packageName)
            putExtra("pageNo", pageNo)
            putExtra("finishWhenOpeningBook", finishWhenOpeningBook)
        })
    }

    private fun openFileSelectorImpl() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mODTOpenFileSelectorResultLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                // add all supported mime types
                val list = ArrayList<String>()
                for (format in DocumentFormat.entries) {
                    list.addAll(format.mimeFormats)
                }
                // Books inside zip-archives
                list.add("application/zip")
                // Android open document activity does not support all required mime types
                list.add("application/octet-stream")
                // Mime type replacement for ".chm"
                list.add("chemical/x-chemdraw")
                // Mime type replacement for ".pdb"
                list.add("chemical/x-pdb")
                putExtra(Intent.EXTRA_MIME_TYPES, list.toTypedArray())
                type = "*/*"
                addCategory(Intent.CATEGORY_OPENABLE)
            })
        } else {
            // TODO: implement something better than this for API<19
            startActivity(Intent(applicationContext, LibraryActivity::class.java).apply {
                setPackage(packageName)
                putExtra("parent", ReaderActivity::class.qualifiedName)
                putExtra("pageNo", 1)
            })
        }
    }

    private fun openExternalDictionaryImpl(text: String) {
        log.debug("openExternalDictionaryImpl(): text = $text")
        val dict = ExtDictionaryRegistry.currentDict
        try {
            val intent = ExtDictionaryRegistry.buildIntent(text)
            mExtDictionaryResultLauncher?.launch(intent)
        } catch (e: ActivityNotFoundException) {
            log.error("Dictionary is not installed", e)
            showToast(
                getString(R.string.dictionary___not_installed, dict.name),
                Toast.LENGTH_LONG
            )
        } catch (e: Exception) {
            log.error("Failed to run dictionary", e)
            showToast(
                getString(R.string.failed_to_start_dictionary__, dict.name),
                Toast.LENGTH_LONG
            )
        }
    }

    private fun openSearchFragmentImpl(bookInfo: BookInfo) {
        val dialogFragment = SearchDialogFragment(bookInfo)
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        args.putInt(
            "toolbar",
            settings.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
        )
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(supportFragmentManager, "search")
        } else {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.add(R.id.substituteFragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    fun requestPostNotificationPermissions_impl() {
        // Check or request permission to post notification
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            // before API 33 (before Android 13)
            // already allowed after application installation
        } else {
            // API 33+ (Android 13+)
            val postNotificationsPermissionCheck =
                checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS)
            var needPerm = ""
            if (PackageManager.PERMISSION_GRANTED != postNotificationsPermissionCheck) {
                needPerm = Manifest.permission.POST_NOTIFICATIONS
            } else {
                log.info("POST_NOTIFICATIONS permission already granted.")
            }
            if (needPerm.isNotEmpty()) {
                log.info("POST_NOTIFICATIONS DENIED, requesting from user...")
                // request permission from user
                mPostNotificationResultLauncher?.launch(needPerm)
            }
        }
    }

    fun openURLImpl(url: String, confirmation: Boolean) {
        val lambdaOpenURL = {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            } catch (e: Exception) {
                log.error("openURL(): url=$url; $e")
            }
        }
        if (confirmation) {
            val dialog = MessageDialog(R.string.confirmation)
            dialog.setMessage(getString(R.string.confirm_following_link__s, url))
            dialog.setButton(MessageDialog.StandardButtons.Cancel)
            dialog.setButton(MessageDialog.StandardButtons.Continue) {
                lambdaOpenURL()
            }
            dialog.showDialog(this)
        } else {
            lambdaOpenURL()
        }
    }

    private fun showToast(msg: String, duration: Int) {
        // TODO: Replace with Snackbar
        log.verbose("showing toast: $msg")
        val toast = Toast.makeText(this, msg, duration)
        toast.show()
    }

    private fun updateBacklightBrightness(b: Float) {
        window?.let { wnd ->
            val attrs = wnd.attributes
            var bb = b
            if (bb < 0 && attrs.screenBrightness < 0)
                return
            var changed = false
            if (bb > -0.99999f && bb < 0f) {
                log.debug("dimming screen by " + ((1 + bb) * 100).toInt() + "%")
                bb = -bb * attrs.screenBrightness
                if (bb < 0.1f)
                    return
            }
            val delta = abs(attrs.screenBrightness - bb)
            if (delta > 0.01) {
                attrs.screenBrightness = bb
                changed = true
            }
            if (changed) {
                log.debug("Window attribute changed: $attrs")
                wnd.attributes = attrs
            }
        }
    }

    private fun registerReceivers() {
        if (!mBatteryChangeReceiverRegistered) {
            try {
                // ACTION_BATTERY_CHANGED: This is a sticky broadcast containing the charging state, level, and other information about the battery.
                val intent1 = registerReceiver(
                    mBatteryChangeReceiver,
                    IntentFilter(Intent.ACTION_BATTERY_CHANGED)
                )
                if (null != intent1) {
                    // process this Intent
                    mBatteryChangeReceiver.onReceive(null, intent1)
                }
                mBatteryChangeReceiverRegistered = true
            } catch (_: Exception) {
            }
        }
        if (!mTimeTickReceiverRegistered) {
            try {
                // ACTION_TIME_TICK: The current time has changed. Sent every minute.
                val intent2 =
                    registerReceiver(mTimeTickReceiver, IntentFilter(Intent.ACTION_TIME_TICK))
                if (null != intent2) {
                    // process this Intent
                    mTimeTickReceiver.onReceive(null, intent2)
                }
                mTimeTickReceiverRegistered = true
            } catch (_: Exception) {
            }
        }
    }

    private fun unregisterReceivers() {
        if (mBatteryChangeReceiverRegistered) {
            try {
                unregisterReceiver(mBatteryChangeReceiver)
                mBatteryChangeReceiverRegistered = false
            } catch (e: IllegalArgumentException) {
                log.error("Failed to unregister receiver: $e")
            }
        }
        if (mTimeTickReceiverRegistered) {
            try {
                unregisterReceiver(mTimeTickReceiver)
                mTimeTickReceiverRegistered = false
            } catch (e: IllegalArgumentException) {
                log.error("Failed to unregister receiver: $e")
            }
        }
    }

    /**
     * Read & use various system settings
     */
    private fun rereadSystemSettings() {
        mScreenBacklightDurationSystem =
            Settings.System.getInt(contentResolver, Settings.System.SCREEN_OFF_TIMEOUT, -1)
        //log.debug("system screen timeout=$mScreenBacklightDurationSystem")
        mRequiredScreenLockTime = if (mScreenBacklightDurationSystem >= 0)
            mScreenBacklightDuration - mScreenBacklightDurationSystem
        else
            mScreenBacklightDuration
        if (mRequiredScreenLockTime <= 0) {
            log.debug("The system screen timeout ($mScreenBacklightDurationSystem ms) is greater than the screen backlight duration in this program, control of the screen backlight duration is not required")
            mBacklightControl.release()
        } else {
            mBacklightControl.onUserActivity()
        }
    }

    private fun onOpenOverflowReaderMenu(anchor: View) {
        var popupMenu = mReaderOverflowMenu
        if (null == popupMenu) {
            popupMenu = PopupMenu(this@ReaderActivity, anchor)
            popupMenu.inflate(R.menu.reader_menu)
            popupMenu.setOnMenuItemClickListener { item -> onOverflowReaderMenuItemSelected(item) }
            mReaderOverflowMenu = popupMenu
        }
        onPrepareOverflowReaderMenu(popupMenu.menu)
        popupMenu.show()
    }

    private fun onPrepareOverflowReaderMenu(menu: Menu) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            val toggleFullscreen = menu.findItem(R.id.toggle_fullscreen)
            toggleFullscreen.isVisible = true
        }
    }

    private fun onOverflowReaderMenuItemSelected(item: MenuItem): Boolean {
        var action: ReaderAction? = null
        when (item.itemId) {
            R.id.goto_ -> action = ReaderActionRegistry.SHOW_GO_PANEL
            R.id.bookmarks -> action = ReaderActionRegistry.BOOKMARKS
            R.id.toggle_fullscreen -> action = ReaderActionRegistry.TOGGLE_FULLSCREEN
            R.id.text_selection -> action = ReaderActionRegistry.START_SELECTION
            R.id.read_aloud -> action = ReaderActionRegistry.TTS_PLAY
            //R.id.search -> action = ReaderActionRegistry.SEARCH
            R.id.library -> {
                openLibraryImpl(1, true)
                return true
            }

            R.id.settings -> {
                showSettingsImpl()
                return true
            }

            R.id.exit -> action = ReaderActionRegistry.EXIT
        }
        if (null != action) {
            mReaderView?.processReaderAction(action)
            return true
        }
        return false
    }

    // TODO: when implementing internal file browser (to open books or documents)
    //  test mActivityState.haveExtStorageReadPerm and if it failed
    //  call requestStoragePermissions() to request required permissions.

    companion object {
        private val log = SRLog.create("main")
        private const val DEF_SCREEN_BACKLIGHT_TIMER_INTERVAL: Long = 3 * 60 * 1000
    }
}
