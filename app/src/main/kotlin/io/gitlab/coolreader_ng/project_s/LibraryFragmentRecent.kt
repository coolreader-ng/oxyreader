/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.extensions.disableAllMenuItems
import io.gitlab.coolreader_ng.project_s.extensions.enableAllMenuItems
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener

class LibraryFragmentRecent(
    dbServiceAccessor: DBServiceAccessor? = null,
    bookCoverManager: BookCoverManager? = null,
    settings: SRProperties? = null
) :
    LibraryFragmentBase(dbServiceAccessor, bookCoverManager, settings), AbleBackwards {

    private var mIsLargeLayout = false
    private val mBookList: ArrayList<BookInfo> = ArrayList()
    private lateinit var mAdapter: LibraryBookListViewAdapter
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mProgressView: View
    private lateinit var mProgressPopup: ProgressPopup
    private var mActionMode: ActionMode? = null

    override fun onUpdateViewImpl() {
        showProgress()
        dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.loadRecentBooks(100, object : DBService.BooksLoadingCallback {
                    override fun onBooksListLoaded(books: Collection<BookInfo>?) {
                        if (null != books) {
                            Utils.syncListByReference(mBookList, books, mAdapter)
                        } else {
                            if (mBookList.isNotEmpty()) {
                                val size = mBookList.size
                                mBookList.clear()
                                mAdapter.notifyItemRangeRemoved(0, size)
                            }
                        }
                        hideProgress()
                    }
                })
            }
        })
    }

    override fun onHideFragment() {
        mActionMode?.let {
            it.finish()
            mActionMode = null
        }
    }

    override fun onBackPressed(): Boolean {
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
            // Consume event
            return true
        }
        // We are saying that this event should be handled in the next chain item
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIsLargeLayout = resources.getBoolean(R.bool.large_layout)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mProgressView = inflater.inflate(R.layout.library_progress, container, false)
        return inflater.inflate(R.layout.library_recent, container, false)
    }

    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)

        mProgressPopup = ProgressPopup(contentView, mProgressView)

        var initialPosAtSelection = -1
        val actionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                mode?.menuInflater?.inflate(R.menu.library_contents_action_bar, menu)
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                menu?.let { Utils.applyToolBarColorForMenu(requireContext(), it) }
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.deselect -> {
                        mAdapter.clearSelection()
                        return true
                    }

                    R.id.delete -> {
                        val list = mAdapter.selectedItems
                        if (list.isNotEmpty()) {
                            val dialog =
                                MessageDialog(
                                    R.string.confirmation,
                                    R.string.are_you_sure_you_want_to_remove_these_books_from_recent_list_
                                )
                            dialog.setButton(MessageDialog.StandardButtons.No)
                            dialog.setButton(MessageDialog.StandardButtons.Cancel)
                            dialog.setButton(MessageDialog.StandardButtons.Yes) {
                                showProgress()
                                removeBooksFromRecent(list) {
                                    hideProgress()
                                    mode?.finish()
                                }
                            }
                            dialog.showDialog(requireContext())
                        }
                        return true
                    }
                }
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
                mAdapter.isSelectionMode = false
            }
        }

        mRecyclerView = contentView.findViewById(R.id.recyclerView)
        mAdapter = LibraryBookListViewAdapter(requireContext(), bookCoverManager, mBookList)
        mAdapter.onBookInfoRunnable = object : LibraryBookListViewAdapter.BookRunnable {
            override fun run(bookInfo: BookInfo) {
                showBookInfoImpl(bookInfo)
            }
        }
        mAdapter.onCopiedInfoRunnable = Runnable {
            // TODO: Replace with Snackbar or Tooltip
            val toast = Toast.makeText(
                requireContext(),
                R.string.this_file_has_been_copied_to_the_application_s_internal_storage,
                Toast.LENGTH_LONG
            )
            toast.show()
        }
        mAdapter.onSelectionModeChangedListener = object :
            LibraryBookListViewAdapter.OnSelectionModeChanged {
            override fun onSelectionModeChanged(selectionMode: Boolean) {
                if (selectionMode) {
                    val parentActivity =
                        if (activity is AppCompatActivity) (activity as AppCompatActivity) else null
                    if (null != parentActivity) {
                        mActionMode = parentActivity.startSupportActionMode(actionModeCallback)
                        mAdapter.setSelectedItem(initialPosAtSelection, true)
                    } else {
                        log.error("Invalid parent activity!")
                    }
                } else {
                    mActionMode = null
                    initialPosAtSelection = -1
                }
            }
        }
        mAdapter.onSelectionChangedListener = object :
            LibraryBookListViewAdapter.OnSelectionChanged {
            override fun onSelectionChanged(selectedItems: Collection<BookInfo>) {
                val count = selectedItems.size
                if (count > 0) {
                    mActionMode?.enableAllMenuItems()
                    mActionMode?.title = requireContext().getString(R.string._n_items, count)
                } else {
                    mActionMode?.disableAllMenuItems()
                    mActionMode?.title = requireContext().getString(R.string.no_selection)
                }
            }
        }
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        mRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                requireContext(),
                true
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val bookInfo =
                                if (index >= 0 && index < mBookList.size) mBookList[index] else null
                            bookInfo?.let { bi -> openBookInReaderViewImpl(bi) }
                        }

                        override fun onLongPressed(index: Int) {
                            initialPosAtSelection = index
                            mAdapter.isSelectionMode = true
                            mRecyclerView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                        }
                    }
            })

        val rvPaddingLeft = mRecyclerView.paddingLeft
        val rvPaddingRight = mRecyclerView.paddingRight
        val rvPaddingTop = mRecyclerView.paddingTop
        ViewCompat.setOnApplyWindowInsetsListener(mRecyclerView) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(
                left = insets.left + rvPaddingLeft,
                top = insets.top + rvPaddingTop,
                right = insets.right + rvPaddingRight
            )
            // Don't consume insets (WindowInsetsCompat.CONSUMED)
            //  since we use child fragments
            windowInsets
        }
    }

    private fun showBookInfoImpl(bookInfo: BookInfo) {
        val dialogFragment = BookInfoDialogFragment(bookInfo, bookCoverManager)
        dialogFragment.onActionsListener = object : BookInfoDialogFragment.OnActionsListener {
            override fun onOpenBook(bookInfo: BookInfo) {
                openBookInReaderViewImpl(bookInfo)
            }

            override fun onInfoChanged(bookInfo: BookInfo) {
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.saveBookInfo(bookInfo, object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                // TODO:
                                //if (!result)
                                //    showToast(getString(R.string.failed_to_save_info_), Toast.LENGTH_LONG)
                            }
                        })
                    }
                })
            }
        }
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        args.putInt(
            "toolbar",
            settings?.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
                ?: PropNames.App.TOOLBAR_LOCATION_NONE
        )
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(childFragmentManager, "bookInfo")
        } else {
            val transaction = childFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.replace(R.id.fragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun openBookInReaderViewImpl(bookInfo: BookInfo) {
        if (!bookInfo.isBookFileExists) {
            val dialog =
                MessageDialog(R.string.error, R.string.file_is_missing_it_may_have_been_deleted)
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else if (!bookInfo.isBookFileAvailable) {
            val dialog = MessageDialog(
                R.string.error,
                R.string.file_is_unavailable_may_not_have_read_permissions
            )
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else {
            // open book for reading
            startActivity(Intent(
                Intent.ACTION_VIEW,
                Uri.Builder().scheme("file").path(bookInfo.fileInfo.pathNameWA).build(),
                context,
                ReaderActivity::class.java
            ).apply {
                setPackage(requireContext().packageName)
            })
            if (finishWhenOpeningBook)
                requireActivity().finish()
        }
    }

    private fun removeBooksFromRecent(list: Collection<BookInfo>, onCompleteCallback: () -> Unit) {
        if (list.isNotEmpty()) {
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.removeMultipleBooksFromRecent(list,
                        object : DBService.BooksOperationCallback {
                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                BackgroundThread.postGUI {
                                    val removeRecentFailedList = ArrayList<BookInfo>()
                                    result?.let {
                                        for ((book, delRes) in it) {
                                            if (!delRes)
                                                removeRecentFailedList.add(book)
                                        }
                                    }
                                    if (removeRecentFailedList.isNotEmpty()) {
                                        val message2 =
                                            StringBuilder(
                                                requireContext().getString(
                                                    R.string.the_following_books_could_not_be_removed_from_the_recent_list_
                                                )
                                            )
                                        message2.append("\n")
                                        val iter = removeRecentFailedList.iterator()
                                        while (iter.hasNext()) {
                                            val book = iter.next()
                                            message2.append("\"${book.fileInfo.pathNameWA}\"")
                                            if (iter.hasNext())
                                                message2.append("\n")
                                        }
                                        val dialog2 = MessageDialog(R.string.error)
                                        dialog2.setMessage(message2.toString())
                                        dialog2.setButton(MessageDialog.StandardButtons.Ok)
                                        dialog2.showDialog(requireContext())
                                    }
                                    onCompleteCallback()
                                    // Update list if at least one file was deleted
                                    requestFragmentUpdate()
                                }
                            }
                        })
                }
            })
        }
    }

    private fun showProgress() {
        mProgressPopup.isIndeterminate = true
        mProgressPopup.x = 0
        mProgressPopup.y = 0
        mProgressPopup.width = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.height = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.show()
    }

    private fun hideProgress() {
        mProgressPopup.hide()
    }

    init {
        propertiesObserver = object : PropertiesObserver {
            override fun dbServiceAccessorIsSet(dbServiceAccessor: DBServiceAccessor?) {
            }

            override fun bookCoverManagerIsSet(bookCoverManager: BookCoverManager?) {
                // Find BookInfoDialogFragment & set bookCoverManager
                val bookInfoFragments =
                    childFragmentManager.fragments.filterIsInstance(BookInfoDialogFragment::class.java)
                for (fragment in bookInfoFragments) {
                    if (null == fragment.bookCoverManager)
                        fragment.bookCoverManager = bookCoverManager
                }
            }

            override fun settingsIsSet(settings: SRProperties?) {
            }
        }
    }

    companion object {
        private val log = SRLog.create("library.recent")
    }
}