/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.ProgressBar
import android.widget.ViewFlipper
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.gitlab.coolreader_ng.genrescollection.GenreRecord
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.db.Handbook
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewListAdapter
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener

internal class LibraryFilterFragment(
    private var filters: DBService.BookFilters? = null,
    internal var dbServiceAccessor: DBServiceAccessor? = null
) : DialogFragment(), AbleBackwards {
    interface OnActionsListener {
        fun onDismiss()
        fun onAccept(filters: DBService.BookFilters)
    }

    private enum class SelectionMode {
        Author,
        Series,
        Genre,
        Keyword,
        Language,
        Status,
        Rating
    }

    private class UiViewHolder {
        lateinit var viewFlipper: ViewFlipper
        lateinit var cbOnlyReferenced: CheckBox
        lateinit var btnCloseSelector: Button
        lateinit var btnClose: Button
        lateinit var cbAuthor: CheckBox
        lateinit var btnAuthor: Button
        lateinit var cbSeries: CheckBox
        lateinit var btnSeries: Button
        lateinit var cbGenres: CheckBox
        lateinit var btnGenres: Button
        lateinit var cbKeywords: CheckBox
        lateinit var btnKeywords: Button
        lateinit var cbLanguage: CheckBox
        lateinit var btnLanguage: Button
        lateinit var cbStatus: CheckBox
        lateinit var btnStatus: Button
        lateinit var cbRating: CheckBox
        lateinit var btnRating: Button
        lateinit var btnShow: Button
        lateinit var progressIndicator: ProgressBar
        lateinit var itemSelectionRecyclerView: RecyclerView
    }

    private lateinit var uiHolder: UiViewHolder

    private var mSuppressCheckboxCallbacks = false

    var onActionsListener: OnActionsListener? = null

    private var mShowAsDialog = false

    private val mAuthorsHandbook = Handbook()
    private val mSeriesHandbook = Handbook()
    private val mGenresHandbook = Handbook(allowDuplicateIds = true)
    private val mOnlyReferencedGenresHandbook = Handbook()
    private val mKeywordsHandbook = Handbook()
    private val mLanguagesHandbook = Handbook()
    private val mStatusesHandbook = Handbook()
    private val mRatingsHandbook = Handbook()
    private var mSelectedAuthorIndex = -1
    private var mSelectedSeriesIndex = -1
    private var mSelectedGenresIndex = -1
    private var mSelectedKeywordsIndex = -1
    private var mSelectedLanguageIndex = -1
    private var mSelectedStatusIndex = -1
    private var mSelectedRatingIndex = -1
    private var mSelectedAuthorId = -1L
    private var mSelectedSeriesId = -1L
    private var mSelectedGenresId = -1L
    private var mSelectedKeywordsId = -1L
    private var mSelectedLanguageId = -1L
    private var mSelectedStatusId = -1L
    private var mSelectedRatingId = -1L
    private var mAuthorsHandbookLoaded = false
    private var mSeriesHandbookLoaded = false
    private var mGenresHandbookLoaded = false
    private var mOnlyReferencedGenresHandbookLoaded = false
    private var mKeywordsHandbookLoaded = false
    private var mLanguagesHandbookLoaded = false
    private var mStatusesHandbookLoaded = false
    private var mRatingsHandbookLoaded = false

    private inner class ListSelector {
        var selectionMode: SelectionMode = SelectionMode.Author
        val handbook: Handbook
            get() {
                return when (selectionMode) {
                    SelectionMode.Author -> mAuthorsHandbook
                    SelectionMode.Series -> mSeriesHandbook
                    SelectionMode.Genre -> {
                        if (onlyReferenced) mOnlyReferencedGenresHandbook else mGenresHandbook
                    }

                    SelectionMode.Keyword -> mKeywordsHandbook
                    SelectionMode.Language -> mLanguagesHandbook
                    SelectionMode.Status -> mStatusesHandbook
                    SelectionMode.Rating -> mRatingsHandbook
                }
            }
        var selectedIndex: Int
            get() {
                return when (selectionMode) {
                    SelectionMode.Author -> mSelectedAuthorIndex
                    SelectionMode.Series -> mSelectedSeriesIndex
                    SelectionMode.Genre -> mSelectedGenresIndex
                    SelectionMode.Keyword -> mSelectedKeywordsIndex
                    SelectionMode.Language -> mSelectedLanguageIndex
                    SelectionMode.Status -> mSelectedStatusIndex
                    SelectionMode.Rating -> mSelectedRatingIndex
                }
            }
            set(value) {
                when (selectionMode) {
                    SelectionMode.Author -> mSelectedAuthorIndex = value
                    SelectionMode.Series -> mSelectedSeriesIndex = value
                    SelectionMode.Genre -> mSelectedGenresIndex = value
                    SelectionMode.Keyword -> mSelectedKeywordsIndex = value
                    SelectionMode.Language -> mSelectedLanguageIndex = value
                    SelectionMode.Status -> mSelectedStatusIndex = value
                    SelectionMode.Rating -> mSelectedRatingIndex = value
                }
            }
        var selectedId: Long
            get() {
                return when (selectionMode) {
                    SelectionMode.Author -> mSelectedAuthorId
                    SelectionMode.Series -> mSelectedSeriesId
                    SelectionMode.Genre -> mSelectedGenresId
                    SelectionMode.Keyword -> mSelectedKeywordsId
                    SelectionMode.Language -> mSelectedLanguageId
                    SelectionMode.Status -> mSelectedStatusId
                    SelectionMode.Rating -> mSelectedRatingId
                }
            }
            set(value) {
                when (selectionMode) {
                    SelectionMode.Author -> mSelectedAuthorId = value
                    SelectionMode.Series -> mSelectedSeriesId = value
                    SelectionMode.Genre -> mSelectedGenresId = value
                    SelectionMode.Keyword -> mSelectedKeywordsId = value
                    SelectionMode.Language -> mSelectedLanguageId = value
                    SelectionMode.Status -> mSelectedStatusId = value
                    SelectionMode.Rating -> mSelectedRatingId = value
                }
            }
        val selectedValue: String?
            get() = handbook.getById(selectedId)?.name
        var onlyReferenced = false
    }

    private fun Handbook.HandbookItem.displayedName(type: SelectionMode): String {
        return when (type) {
            SelectionMode.Language -> CREngineNGBinding.getHumanReadableLocaleName(name)
                .ifEmpty { name }

            else -> name
        }
    }

    private val mListSelector = ListSelector()
    private val mCommonItemList = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.library_filter, container, false)
    }

    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)

        uiHolder = UiViewHolder()
        uiHolder.viewFlipper = contentView.findViewById(R.id.viewFlipper)
        uiHolder.cbOnlyReferenced = contentView.findViewById(R.id.cbOnlyReferenced)
        uiHolder.btnCloseSelector = contentView.findViewById(R.id.btnCloseSelector)
        uiHolder.btnClose = contentView.findViewById(R.id.btnClose)
        uiHolder.cbAuthor = contentView.findViewById(R.id.cbAuthor)
        uiHolder.btnAuthor = contentView.findViewById(R.id.btnAuthor)
        uiHolder.cbSeries = contentView.findViewById(R.id.cbSeries)
        uiHolder.btnSeries = contentView.findViewById(R.id.btnSeries)
        uiHolder.cbGenres = contentView.findViewById(R.id.cbGenres)
        uiHolder.btnGenres = contentView.findViewById(R.id.btnGenres)
        uiHolder.cbKeywords = contentView.findViewById(R.id.cbKeywords)
        uiHolder.btnKeywords = contentView.findViewById(R.id.btnKeywords)
        uiHolder.cbLanguage = contentView.findViewById(R.id.cbLanguage)
        uiHolder.btnLanguage = contentView.findViewById(R.id.btnLanguage)
        uiHolder.cbStatus = contentView.findViewById(R.id.cbStatus)
        uiHolder.btnStatus = contentView.findViewById(R.id.btnStatus)
        uiHolder.cbRating = contentView.findViewById(R.id.cbRating)
        uiHolder.btnRating = contentView.findViewById(R.id.btnRating)
        uiHolder.btnShow = contentView.findViewById(R.id.btnShow)
        uiHolder.progressIndicator = contentView.findViewById(R.id.progressIndicator)
        uiHolder.itemSelectionRecyclerView =
            contentView.findViewById(R.id.itemSelectionRecyclerView)

        updateUiOnFilters()

        ViewCompat.setOnApplyWindowInsetsListener(contentView) { v, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(left = insets.left, right = insets.right, top = insets.top)
            WindowInsetsCompat.CONSUMED
        }

        // Callbacks
        val closeAction: () -> Unit = {
            if (mShowAsDialog) {
                dialog?.dismiss()
            } else {
                if (parentFragmentManager.backStackEntryCount > 0)
                    parentFragmentManager.popBackStack()
            }
        }
        uiHolder.btnCloseSelector.setOnClickListener {
            showFirstPage()
            updateUiOnFilters()
        }
        uiHolder.btnClose.setOnClickListener {
            closeAction()
        }
        uiHolder.cbOnlyReferenced.setOnCheckedChangeListener { _, isChecked ->
            mListSelector.onlyReferenced = isChecked
            if (1 == uiHolder.viewFlipper.displayedChild) {
                if (SelectionMode.Genre == mListSelector.selectionMode) {
                    openHandbook(SelectionMode.Genre, isChecked)
                }
            }
        }

        // Author
        uiHolder.cbAuthor.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Author
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterAuthorId == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterAuthorId, id.toString())
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Author)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterAuthorId || it.filterField == DBService.BookFilterType.FilterAuthor }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnAuthor.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Author
            openHandbook(SelectionMode.Author)
        }

        // Series
        uiHolder.cbSeries.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Series
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterSeriesId == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterSeriesId, id.toString())
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Series)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterSeriesId || it.filterField == DBService.BookFilterType.FilterSeries }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnSeries.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Series
            openHandbook(SelectionMode.Series)
        }

        // Genres
        uiHolder.cbGenres.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Genre
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterGenreId == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterGenreId, id.toString())
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Genre)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterGenreId }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnGenres.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Genre
            openHandbook(SelectionMode.Genre)
        }

        // Keywords
        uiHolder.cbKeywords.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Keyword
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterKeywordId == it.filterField }
                    val newFilter = DBService.BookFilter(
                        DBService.BookFilterType.FilterKeywordId,
                        id.toString()
                    )
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Keyword)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterKeywordId || it.filterField == DBService.BookFilterType.FilterKeyword }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnKeywords.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Keyword
            openHandbook(SelectionMode.Keyword)
        }

        // Languages
        uiHolder.cbLanguage.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Language
                val language = mListSelector.selectedValue
                if (null != language) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterLanguage == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterLanguage, language)
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Language)
                }
            } else {
                val oldFilter =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterLanguage }
                oldFilter?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnLanguage.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Language
            openHandbook(SelectionMode.Language)
        }

        // Statuses
        uiHolder.cbStatus.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Status
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterStatus == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterStatus, id.toString())
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Status)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterStatus }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnStatus.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Status
            openHandbook(SelectionMode.Status)
        }

        // Rating
        uiHolder.cbRating.setOnCheckedChangeListener { _, isChecked ->
            if (mSuppressCheckboxCallbacks)
                return@setOnCheckedChangeListener
            if (isChecked) {
                mListSelector.selectionMode = SelectionMode.Rating
                val id = mListSelector.selectedId
                if (id >= 0) {
                    // Already selected
                    val oldFilter =
                        filters?.find { DBService.BookFilterType.FilterRating == it.filterField }
                    val newFilter =
                        DBService.BookFilter(DBService.BookFilterType.FilterRating, id.toString())
                    if (oldFilter != newFilter) {
                        oldFilter?.let { toRemove -> filters?.remove(toRemove) }
                        filters?.add(newFilter)
                    }
                } else {
                    openHandbook(SelectionMode.Rating)
                }
            } else {
                val oldFilters =
                    filters?.filter { it.filterField == DBService.BookFilterType.FilterRating }
                oldFilters?.let { filters?.removeAll(it.toSet()) }
            }
        }
        uiHolder.btnRating.setOnClickListener {
            mListSelector.selectionMode = SelectionMode.Rating
            openHandbook(SelectionMode.Rating)
        }

        // RecyclerView
        uiHolder.itemSelectionRecyclerView.layoutManager = LinearLayoutManager(context)
        uiHolder.itemSelectionRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        uiHolder.itemSelectionRecyclerView.adapter = RecyclerViewListAdapter(mCommonItemList)
        uiHolder.itemSelectionRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(requireContext()).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val handbook = mListSelector.handbook
                            val listItem =
                                if (index >= 0 && index < handbook.size) handbook[index] else null
                            if (null != listItem) {
                                mListSelector.selectedIndex = index
                                mListSelector.selectedId = listItem.id
                                // Hide item selector
                                showFirstPage()
                                // Update filters
                                var filterType1ToRemove: DBService.BookFilterType? = null
                                var filterType2ToRemove: DBService.BookFilterType? = null
                                var filterToAdd: DBService.BookFilter? = null
                                when (mListSelector.selectionMode) {
                                    SelectionMode.Author -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterAuthor
                                        filterType2ToRemove =
                                            DBService.BookFilterType.FilterAuthorId
                                        filterToAdd =
                                            DBService.BookFilter(
                                                DBService.BookFilterType.FilterAuthorId,
                                                listItem.id.toString()
                                            )
                                    }

                                    SelectionMode.Series -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterSeries
                                        filterType2ToRemove =
                                            DBService.BookFilterType.FilterSeriesId
                                        filterToAdd =
                                            DBService.BookFilter(
                                                DBService.BookFilterType.FilterSeriesId,
                                                listItem.id.toString()
                                            )
                                    }

                                    SelectionMode.Genre -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterGenreId
                                        filterToAdd = DBService.BookFilter(
                                            DBService.BookFilterType.FilterGenreId,
                                            listItem.id.toString()
                                        )
                                    }

                                    SelectionMode.Keyword -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterKeyword
                                        filterType2ToRemove =
                                            DBService.BookFilterType.FilterKeywordId
                                        filterToAdd =
                                            DBService.BookFilter(
                                                DBService.BookFilterType.FilterKeywordId,
                                                listItem.id.toString()
                                            )
                                    }

                                    SelectionMode.Language -> {
                                        filterType1ToRemove =
                                            DBService.BookFilterType.FilterLanguage
                                        filterToAdd = DBService.BookFilter(
                                            DBService.BookFilterType.FilterLanguage,
                                            listItem.name
                                        )
                                    }

                                    SelectionMode.Status -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterStatus
                                        filterToAdd = DBService.BookFilter(
                                            DBService.BookFilterType.FilterStatus,
                                            listItem.id.toString()
                                        )
                                    }

                                    SelectionMode.Rating -> {
                                        filterType1ToRemove = DBService.BookFilterType.FilterRating
                                        filterToAdd = DBService.BookFilter(
                                            DBService.BookFilterType.FilterRating,
                                            listItem.id.toString()
                                        )
                                    }
                                }
                                val oldFilters =
                                    filters?.filter { filter -> filter.filterField == filterType1ToRemove || filter.filterField == filterType2ToRemove }
                                oldFilters?.let { toRemove -> filters?.removeAll(toRemove.toSet()) }
                                filters?.add(filterToAdd)
                                updateUiOnFilters()
                            }
                        }
                    }
            })

        uiHolder.btnShow.setOnClickListener {
            filters?.let { onActionsListener?.onAccept(it) }
            closeAction()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // Without Window.FEATURE_LEFT_ICON for some reason the bottom content of the dialog is cut off
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE or Window.FEATURE_LEFT_ICON)
        // Setting transparent background to remove padding
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (null == filters) {
            filters = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                savedInstanceState?.getSerializable("filters", DBService.BookFilters::class.java)
            else
                savedInstanceState?.getSerializable("filters") as DBService.BookFilters?
        }
        mShowAsDialog = arguments?.getBoolean("asDialog") ?: false
        // Note: onCreateView() function will be called later
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        filters?.let { outState.putSerializable("filters", it) }
    }

    override fun onStart() {
        super.onStart()
        if (mShowAsDialog) {
            // Increase dialog window width
            dialog?.let { dialog ->
                val metrics = resources.displayMetrics
                val displayWidth = metrics.widthPixels
                val displayHeight = metrics.heightPixels
                val layoutParams = WindowManager.LayoutParams()
                val oldParams = dialog.window?.attributes
                if (null != oldParams)
                    layoutParams.copyFrom(dialog.window?.attributes)
                else
                    layoutParams.height = 8 * displayHeight / 10
                layoutParams.width = 8 * displayWidth / 10
                dialog.window?.attributes = layoutParams
            }
        }
    }

    override fun onBackPressed(): Boolean {
        if (0 != uiHolder.viewFlipper.displayedChild) {
            showFirstPage()
            return true
        }
        return false
    }

    private fun showFirstPage() {
        if (0 != uiHolder.viewFlipper.displayedChild) {
            if (uiHolder.viewFlipper.displayedChild > 0)
                setViewFlipperBackwardAnimation()
            else
                setViewFlipperForwardAnimation()
            uiHolder.viewFlipper.displayedChild = 0
            uiHolder.btnClose.visibility = View.VISIBLE
            uiHolder.btnCloseSelector.visibility = View.GONE
            uiHolder.cbOnlyReferenced.visibility = View.GONE
            uiHolder.cbOnlyReferenced.isChecked = false
        }
    }

    private fun showSecondPage(showOnlyReferencedCheckBox: Boolean) {
        if (1 != uiHolder.viewFlipper.displayedChild) {
            if (uiHolder.viewFlipper.displayedChild < 1)
                setViewFlipperForwardAnimation()
            else
                setViewFlipperBackwardAnimation()
            uiHolder.viewFlipper.displayedChild = 1
            uiHolder.btnClose.visibility = View.GONE
            uiHolder.btnCloseSelector.visibility = View.VISIBLE
            uiHolder.cbOnlyReferenced.visibility =
                if (showOnlyReferencedCheckBox) View.VISIBLE else View.GONE
        }
    }

    private fun setViewFlipperForwardAnimation() {
        uiHolder.viewFlipper.setInAnimation(requireContext(), R.anim.show_translate_left)
        uiHolder.viewFlipper.setOutAnimation(requireContext(), R.anim.hide_translate_left)
    }

    private fun setViewFlipperBackwardAnimation() {
        uiHolder.viewFlipper.setInAnimation(requireContext(), R.anim.show_translate_right)
        uiHolder.viewFlipper.setOutAnimation(requireContext(), R.anim.hide_translate_right)
    }

    private fun openHandbook(type: SelectionMode, onlyReferenced: Boolean = false) {
        val showOnlyReferencedCheckBox: Boolean
        val useHandbookFunc: ((handbook: Handbook) -> Unit) -> Unit
        when (type) {
            SelectionMode.Author -> {
                useHandbookFunc = ::useAuthorsHandbook
                showOnlyReferencedCheckBox = false
            }

            SelectionMode.Series -> {
                useHandbookFunc = ::useSeriesHandbook
                showOnlyReferencedCheckBox = false
            }

            SelectionMode.Genre -> {
                useHandbookFunc =
                    if (onlyReferenced) ::useOnlyReferencedGenresHandbook else ::useGenresHandbook
                showOnlyReferencedCheckBox = true
            }

            SelectionMode.Keyword -> {
                useHandbookFunc = ::useKeywordsHandbook
                showOnlyReferencedCheckBox = false
            }

            SelectionMode.Language -> {
                useHandbookFunc = ::useLanguagesHandbook
                showOnlyReferencedCheckBox = false
            }

            SelectionMode.Status -> {
                useHandbookFunc = ::useStatusesHandbook
                showOnlyReferencedCheckBox = false
            }

            SelectionMode.Rating -> {
                useHandbookFunc = ::useRatingHandbook
                showOnlyReferencedCheckBox = false
            }
        }
        showSecondPage(showOnlyReferencedCheckBox)
        val oldSize = mCommonItemList.size
        mCommonItemList.clear()
        uiHolder.itemSelectionRecyclerView.adapter?.notifyItemRangeRemoved(0, oldSize)
        useHandbookFunc {
            for (item in it)
                mCommonItemList.add(item.displayedName(type))
            uiHolder.itemSelectionRecyclerView.adapter?.notifyItemRangeInserted(
                0,
                mCommonItemList.size
            )
            // TODO: scroll & select previously selected item
        }
    }

    private fun useAuthorsHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mAuthorsHandbookLoaded) {
            callback(mAuthorsHandbook)
        } else {
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookAuthor,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    mAuthorsHandbook.clear()
                                    mAuthorsHandbook.addAll(handbook.sortedBy { it.name })
                                    mAuthorsHandbookLoaded = true
                                }
                                callback(mAuthorsHandbook)
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    private fun useSeriesHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mSeriesHandbookLoaded) {
            callback(mSeriesHandbook)
        } else {
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookSeries,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    mSeriesHandbook.clear()
                                    mSeriesHandbook.addAll(handbook)
                                    mSeriesHandbookLoaded = true
                                }
                                callback(mSeriesHandbook)
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    private fun useGenresHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mGenresHandbookLoaded) {
            callback(mGenresHandbook)
        } else {
            showProgress()

            fun enumGenreFunc(genre: GenreRecord) {
                val prefix = "  ".repeat(genre.level)
                mGenresHandbook.addEntry(genre.id.toLong(), "${prefix}${genre.name}")
                if (genre.hasChilds()) {
                    for (child in genre.childs) {
                        enumGenreFunc(child)
                    }
                }
            }

            mGenresHandbook.clear()
            val genresCollection = GenresCollection.getInstance(requireContext())
            for ((_, genre) in genresCollection.collection) {
                enumGenreFunc(genre)
            }
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookUnsupportedGenres,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    for (genre in handbook) {
                                        mGenresHandbook.addEntry(
                                            genre.id + DBService.UnsupportedGenreIdBase,
                                            genre.name
                                        )
                                    }
                                    mGenresHandbookLoaded = true
                                }
                                callback(mGenresHandbook)
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    private fun useOnlyReferencedGenresHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mOnlyReferencedGenresHandbookLoaded) {
            callback(mOnlyReferencedGenresHandbook)
        } else {
            showProgress()
            mOnlyReferencedGenresHandbook.clear()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookReferencedGenre,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    for (genre in handbook) {
                                        mOnlyReferencedGenresHandbook.addEntry(genre.id, genre.name)
                                    }
                                }
                                binder.getHandbook(
                                    DBService.HandbookType.HandbookUnsupportedGenres,
                                    object : DBService.HandbookLoadingCallback {
                                        override fun onHandbookLoaded(handbook: Handbook?) {
                                            // Executes to the GUI thread
                                            if (null != handbook) {
                                                for (genre in handbook) {
                                                    mOnlyReferencedGenresHandbook.addEntry(
                                                        genre.id + DBService.UnsupportedGenreIdBase,
                                                        genre.name
                                                    )
                                                }
                                                mOnlyReferencedGenresHandbookLoaded = true
                                            }
                                            callback(mOnlyReferencedGenresHandbook)
                                            hideProgress()
                                        }
                                    })
                            }
                        })
                }
            })
        }
    }

    private fun useKeywordsHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mKeywordsHandbookLoaded) {
            callback(mKeywordsHandbook)
        } else {
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookKeywords,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    mKeywordsHandbook.clear()
                                    mKeywordsHandbook.addAll(handbook)
                                    mKeywordsHandbookLoaded = true
                                }
                                callback(mKeywordsHandbook)
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    private fun useLanguagesHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mLanguagesHandbookLoaded) {
            callback(mLanguagesHandbook)
        } else {
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.getHandbook(
                        DBService.HandbookType.HandbookLanguage,
                        object : DBService.HandbookLoadingCallback {
                            override fun onHandbookLoaded(handbook: Handbook?) {
                                // Executes to the GUI thread
                                if (null != handbook) {
                                    mLanguagesHandbook.clear()
                                    mLanguagesHandbook.addAll(handbook)
                                    mLanguagesHandbookLoaded = true
                                }
                                callback(mLanguagesHandbook)
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    private fun useStatusesHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mStatusesHandbookLoaded) {
            callback(mStatusesHandbook)
        } else {
            mStatusesHandbook.clear()
            mStatusesHandbook.addEntry(
                BookInfo.ReadingStatus.NONE.ordinal.toLong(),
                requireContext().getString(R.string.unspecified)
            )
            mStatusesHandbook.addEntry(
                BookInfo.ReadingStatus.IN_READING.ordinal.toLong(),
                requireContext().getString(R.string.in_reading)
            )
            mStatusesHandbook.addEntry(
                BookInfo.ReadingStatus.DONE.ordinal.toLong(),
                requireContext().getString(R.string.finished_reading)
            )
            mStatusesHandbook.addEntry(
                BookInfo.ReadingStatus.PLANNED.ordinal.toLong(),
                requireContext().getString(R.string.planned)
            )
            mStatusesHandbookLoaded = true
            callback(mStatusesHandbook)
        }
    }

    private fun useRatingHandbook(callback: (handbook: Handbook) -> Unit) {
        if (mRatingsHandbookLoaded) {
            callback(mRatingsHandbook)
        } else {
            mRatingsHandbook.clear()
            mRatingsHandbook.addEntry(0, requireContext().getString(R.string.unrated))
            mRatingsHandbook.addEntry(1, requireContext().getString(R.string.rating_arg, "1"))
            mRatingsHandbook.addEntry(2, requireContext().getString(R.string.rating_arg, "2"))
            mRatingsHandbook.addEntry(3, requireContext().getString(R.string.rating_arg, "3"))
            mRatingsHandbook.addEntry(4, requireContext().getString(R.string.rating_arg, "4"))
            mRatingsHandbook.addEntry(5, requireContext().getString(R.string.rating_arg, "5"))
            mRatingsHandbookLoaded = true
            callback(mRatingsHandbook)
        }
    }

    private fun updateUiOnFilters() {
        filters?.let { fs ->
            var haveAuthor = false
            var haveSeries = false
            var haveGenre = false
            var haveKeyword = false
            var haveLanguage = false
            var haveStatus = false
            var haveRating = false
            for (f in fs) {
                when (f.filterField) {
                    DBService.BookFilterType.FilterAuthor -> {
                        uiHolder.btnAuthor.text =
                            requireContext().getString(R.string.author_arg, f.filterValue)
                        uiHolder.btnAuthor.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        haveAuthor = true
                    }

                    DBService.BookFilterType.FilterAuthorId -> {
                        useAuthorsHandbook { handbook ->
                            var author = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == author)
                                author = "id=${f.filterValue}"
                            uiHolder.btnAuthor.text =
                                requireContext().getString(R.string.author_arg, author)
                            uiHolder.btnAuthor.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveAuthor = true
                    }

                    DBService.BookFilterType.FilterSeries -> {
                        uiHolder.btnSeries.text =
                            requireContext().getString(R.string.series_arg, f.filterValue)
                        uiHolder.btnSeries.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        haveSeries = true
                    }

                    DBService.BookFilterType.FilterSeriesId -> {
                        useSeriesHandbook { handbook ->
                            var series = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == series)
                                series = "id=${f.filterValue}"
                            uiHolder.btnSeries.text =
                                requireContext().getString(R.string.series_arg, series)
                            uiHolder.btnSeries.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveSeries = true
                    }

                    DBService.BookFilterType.FilterGenreId -> {
                        useGenresHandbook { handbook ->
                            var genre = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == genre)
                                genre = "id=${f.filterValue}"
                            uiHolder.btnGenres.text =
                                requireContext().getString(R.string.genre_arg, genre)
                            uiHolder.btnGenres.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveGenre = true
                    }

                    DBService.BookFilterType.FilterKeyword -> {
                        uiHolder.btnKeywords.text =
                            requireContext().getString(R.string.keyword_arg, f.filterValue)
                        uiHolder.btnKeywords.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        haveKeyword = true
                    }

                    DBService.BookFilterType.FilterKeywordId -> {
                        useKeywordsHandbook { handbook ->
                            var keyword = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == keyword)
                                keyword = "id=${f.filterValue}"
                            uiHolder.btnKeywords.text =
                                requireContext().getString(R.string.keyword_arg, keyword)
                            uiHolder.btnKeywords.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveKeyword = true
                    }

                    DBService.BookFilterType.FilterLanguage -> {
                        uiHolder.btnLanguage.text =
                            requireContext().getString(R.string.language_arg, f.filterValue)
                        uiHolder.btnLanguage.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        haveLanguage = true
                    }

                    DBService.BookFilterType.FilterStatus -> {
                        useStatusesHandbook { handbook ->
                            var status = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == status)
                                status = "id=${f.filterValue}"
                            uiHolder.btnStatus.text =
                                requireContext().getString(R.string.status_arg, status)
                            uiHolder.btnStatus.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveStatus = true
                    }

                    DBService.BookFilterType.FilterRating -> {
                        useRatingHandbook { handbook ->
                            var rating = try {
                                handbook.getById(f.filterValue.toLong())?.name
                            } catch (_: Exception) {
                                null
                            }
                            if (null == rating)
                                rating = "id=${f.filterValue}"
                            uiHolder.btnRating.text =
                                requireContext().getString(R.string.rating_arg, rating)
                            uiHolder.btnRating.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                        }
                        haveRating = true
                    }
                }
            }
            mSuppressCheckboxCallbacks = true
            uiHolder.cbAuthor.isChecked = haveAuthor
            uiHolder.cbSeries.isChecked = haveSeries
            uiHolder.cbGenres.isChecked = haveGenre
            uiHolder.cbKeywords.isChecked = haveKeyword
            uiHolder.cbLanguage.isChecked = haveLanguage
            uiHolder.cbStatus.isChecked = haveStatus
            uiHolder.cbRating.isChecked = haveRating
            mSuppressCheckboxCallbacks = false
        }
    }

    private fun showProgress() {
        uiHolder.progressIndicator.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        uiHolder.progressIndicator.visibility = View.GONE
    }

    companion object {
        private val log = SRLog.create("library.filter")
    }
}
