/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.material.progressindicator.BaseProgressIndicator

/**
 * Popup window with progress indicator
 *
 * Note that this implementation is not synchronized.
 * All properties and methods of this class must be called from the GUI thread.
 */
class ProgressPopup(private val parent: View, contentView: View) {

    private val mPopup: PopupWindow
    private val mProgressBar: ProgressBar
    private val mProgressValueText: TextView
    private val mProgressTaskText: TextView
    private val mContext: Context = parent.context
    private val mGuiHandler: Handler = Handler(Looper.getMainLooper())
    private var mLastRunnable: Runnable? = null
    private var mHaveDelayedTask = false
    private var mShowRequestTime = 0L

    val isShowing: Boolean
        get() = mPopup.isShowing

    val isNotShowing: Boolean
        get() = !mPopup.isShowing

    var x: Int = 0
    var y: Int = 0

    var width: Int
        get() = mPopup.width
        set(value) {
            mPopup.width = value
        }

    var height: Int
        get() = mPopup.height
        set(value) {
            mPopup.height = value
        }

    var popupGravity: Int = Gravity.NO_GRAVITY

    var min: Int
        get() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                return mProgressBar.min
            return 0
        }
        set(value) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                mProgressBar.min = value
        }

    var progress: Int
        get() = mProgressBar.progress
        set(value) {
            if (mProgressBar is BaseProgressIndicator<*>)
                mProgressBar.setProgressCompat(value, true)
            else
                mProgressBar.progress = value
            mProgressValueText.text = mContext.getString(R.string.progress_percents, value)
        }

    var progressTask: String
        get() = mProgressValueText.text.toString()
        set(value) {
            mProgressTaskText.text = value
        }

    var isIndeterminate: Boolean
        get() = mProgressBar.isIndeterminate
        set(value) {
            mProgressBar.isIndeterminate = value
            mProgressValueText.visibility = if (value) View.GONE else View.VISIBLE
        }

    fun show() {
        if (mPopup.isShowing) {
            mPopup.update()
        } else {
            mShowRequestTime = Utils.uptime()
            if (mHaveDelayedTask) {
                mLastRunnable?.let { mGuiHandler.removeCallbacks(it) }
                mHaveDelayedTask = false
            }
            mLastRunnable =
                Runnable {
                    mPopup.showAtLocation(parent, popupGravity, x, y)
                    mHaveDelayedTask = false
                }.also {
                    mGuiHandler.postDelayed(it, MAX_DELAY_TIME)
                    mHaveDelayedTask = true
                }
        }
    }

    fun hide() {
        if (mPopup.isShowing) {
            val elapsed = Utils.uptimeElapsed(mShowRequestTime)
            if (elapsed >= MIN_SHOW_TIME) {
                mPopup.dismiss()
            } else {
                if (mHaveDelayedTask) {
                    mLastRunnable?.let { mGuiHandler.removeCallbacks(it) }
                    mHaveDelayedTask = false
                }
                mLastRunnable = Runnable {
                    mPopup.dismiss()
                    mHaveDelayedTask = false
                }.also {
                    mGuiHandler.postDelayed(it, MIN_SHOW_TIME - elapsed)
                    mHaveDelayedTask = true
                }
            }
        } else {
            if (mHaveDelayedTask) {
                mLastRunnable?.let { mGuiHandler.removeCallbacks(it) }
                mLastRunnable = null
                mHaveDelayedTask = false
            }
        }
    }

    init {
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        /*
           Transition animation is disabled because it prevents the progress bar
           from being visible when using this popup again.
         */
        // TODO: check in new version Material Design Components library or AndroidX
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Fade(Fade.IN)
            mPopup.exitTransition = Fade(Fade.OUT)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        */
        mPopup.animationStyle = android.R.style.Animation_Toast
        mPopup.isTouchable = false
        mPopup.isFocusable = false
        mProgressBar = contentView.findViewById(R.id.progressIndicator)
        mProgressValueText = contentView.findViewById(R.id.progress_value_text)
        mProgressTaskText = contentView.findViewById(R.id.progress_task_text)
    }

    companion object {
        // Maximum show delay, ms
        const val MAX_DELAY_TIME = 300L

        // Minimum show time, ms
        const val MIN_SHOW_TIME = 500L
    }
}
