/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import androidx.core.graphics.Insets
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsCompat.Type.InsetsType

fun WindowInsetsCompat.reduce(@InsetsType typeMask: Int, insets: Insets): WindowInsetsCompat {
    val oldInsets = this.getInsets(typeMask)
    val left = if (oldInsets.left > insets.left) oldInsets.left - insets.left else 0
    val top = if (oldInsets.top > insets.top) oldInsets.top - insets.top else 0
    val right = if (oldInsets.right > insets.right) oldInsets.right - insets.right else 0
    val bottom = if (oldInsets.bottom > insets.bottom) oldInsets.bottom - insets.bottom else 0
    val newInsets = Insets.of(left, top, right, bottom)
    return WindowInsetsCompat.Builder(this).setInsets(typeMask, newInsets).build()
}
