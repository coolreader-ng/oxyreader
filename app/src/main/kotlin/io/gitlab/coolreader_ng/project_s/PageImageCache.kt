/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Bitmap

@UsedInJNI
class PageImageCache {
    @UsedInJNI
    @JvmField
    var bitmap: Bitmap? = null

    @JvmField
    var position: PositionProperties? = null

    fun recycle() {
        //factory.release(bitmap);
        bitmap?.recycle()
        bitmap = null
        position = null
    }

    val isReleased: Boolean
        get() {
            if (bitmap?.isRecycled == true)
                bitmap = null
            return bitmap === null
        }

    fun isCompatible(other: PageImageCache?): Boolean {
        if (null === other)
            return false
        return position?.isCompatible(other.position) ?: (null === other.position)
    }

    override fun toString(): String {
        return "PageImageCache(position=$position)"
    }
}
