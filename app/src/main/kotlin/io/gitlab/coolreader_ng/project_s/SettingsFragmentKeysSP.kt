/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.res.Configuration
import android.os.Bundle
import android.view.KeyCharacterMap
import android.view.KeyEvent
import android.view.ViewConfiguration
import androidx.annotation.Keep
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat

// Single key press (SP)
@Keep
class SettingsFragmentKeysSP : PreferenceFragmentCompat(),
    SettingsActivity.SettingsFragmentStorageHolder {

    private var mProperties = SRProperties()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = SettingsActivity.CREPropsDataStorage(mProperties)

        setPreferencesFromResource(R.xml.preferences_keys_sp, rootKey)

        // actions names & values
        val actionNames = Array(ReaderActionRegistry.AVAILABLE_ACTIONS.size) { i ->
            requireContext().getString(ReaderActionRegistry.AVAILABLE_ACTIONS[i].nameResId)
        }
        val actionIds = Array(ReaderActionRegistry.AVAILABLE_ACTIONS.size) { i ->
            ReaderActionRegistry.AVAILABLE_ACTIONS[i].id
        }

        val allowVolumeKeys =
            mProperties.getBool(PropNames.App.KEY_ACTIONS_ENABLE_VOLUME_KEYS, true)

        // Check available keyboards
        val configuration = requireContext().resources.configuration
        val hasPermanentMenuKey = ViewConfiguration.get(requireContext()).hasPermanentMenuKey()
        val hasHardwareKeyboard = when (configuration.keyboard) {
            Configuration.KEYBOARD_QWERTY,
            Configuration.KEYBOARD_12KEY -> true

            else -> false
        }
        val haveDpadKeys = Configuration.NAVIGATION_DPAD == configuration.navigation

        if (hasPermanentMenuKey) {
            val keyMenuPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_MENU)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_MENU)) {
                keyMenuPreference?.isVisible = true
                keyMenuPreference?.entries = actionNames
                keyMenuPreference?.entryValues = actionIds
            }
        }
        /*
         * Disabled explicitly
        val keyBackPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_BACK)
        if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK)) {
            keyBackPreference?.isVisible = true
            keyBackPreference?.entries = actionNames
            keyBackPreference?.entryValues = actionIds
        }
         */
        if (haveDpadKeys) {
            val keyLeftPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_LEFT)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_DPAD_LEFT)) {
                keyLeftPreference?.isVisible = true
                keyLeftPreference?.entries = actionNames
                keyLeftPreference?.entryValues = actionIds
            }
            val keyRightPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_RIGHT)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_DPAD_RIGHT)) {
                keyRightPreference?.isVisible = true
                keyRightPreference?.entries = actionNames
                keyRightPreference?.entryValues = actionIds
            }
            val keyUpPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_UP)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_DPAD_UP)) {
                keyUpPreference?.isVisible = true
                keyUpPreference?.entries = actionNames
                keyUpPreference?.entryValues = actionIds
            }
            val keyDownPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_DOWN)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_DPAD_DOWN)) {
                keyDownPreference?.isVisible = true
                keyDownPreference?.entries = actionNames
                keyDownPreference?.entryValues = actionIds
            }
        }
        val keySearchPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_SEARCH)
        if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_SEARCH)) {
            keySearchPreference?.isVisible = true
            keySearchPreference?.entries = actionNames
            keySearchPreference?.entryValues = actionIds
        }
        if (hasHardwareKeyboard) {
            val keyPageUpPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_PAGE_UP)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_PAGE_UP)) {
                keyPageUpPreference?.isVisible = true
                keyPageUpPreference?.entries = actionNames
                keyPageUpPreference?.entryValues = actionIds
            }
            val keyPageDownPreference =
                findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_PAGE_DOWN)
            if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_PAGE_DOWN)) {
                keyPageDownPreference?.isVisible = true
                keyPageDownPreference?.entries = actionNames
                keyPageDownPreference?.entryValues = actionIds
            }
        }
        val keyVolumeUpPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_VOLUME_UP)
        if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_VOLUME_UP)) {
            keyVolumeUpPreference?.isVisible = true
            keyVolumeUpPreference?.isEnabled = allowVolumeKeys
            keyVolumeUpPreference?.entries = actionNames
            keyVolumeUpPreference?.entryValues = actionIds
        }
        val keyVolumeDownPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_VOLUME_DOWN)
        if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_VOLUME_DOWN)) {
            keyVolumeDownPreference?.isVisible = true
            keyVolumeDownPreference?.isEnabled = allowVolumeKeys
            keyVolumeDownPreference?.entries = actionNames
            keyVolumeDownPreference?.entryValues = actionIds
        }
        val keyCameraPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_KEYS_SP_CAMERA)
        if (KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_CAMERA)) {
            keyCameraPreference?.isVisible = true
            keyCameraPreference?.entries = actionNames
            keyCameraPreference?.entryValues = actionIds
        }
    }

    override fun setProperties(props: SRProperties) {
        mProperties = props
    }

    override fun resetToDefaults() {
        for ((key, value) in SettingsManager.Defaults.TAP_ACTIONS_ST) {
            mProperties.setProperty(key, value)
        }
    }
}