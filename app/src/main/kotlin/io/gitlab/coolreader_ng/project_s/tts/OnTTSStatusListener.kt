/*
 * LxReader - book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader TTS module at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2015 by Vadim Lopatin <coolreader.org@gmail.com>
 * Copyright (C) 2020,2021 by Aleksey Chernov <valexlin@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s.tts

interface OnTTSStatusListener {
    /**
     * On utterance speech synthesis started.
     */
    fun onUtteranceStart()

    /**
     * On utterance speech synthesis completed.
     */
    fun onUtteranceDone()

    /**
     * On error occurred.
     * @param errorCode TTS error code
     */
    fun onError(errorCode: Int)

    /**
     * Playback state changed to state.
     */
    fun onStateChanged(state: TTSControlService.State)

    /**
     * On volume changed.
     * @param currentVolume current volume
     * @param maxVolume maximum volume
     */
    fun onVolumeChanged(currentVolume: Int, maxVolume: Int)

    /**
     * On speech rate changed.
     * @param speechRate current speech rate
     */
    fun onSpeechRateChanged(speechRate: Float)

    /**
     * On audio focus lost.
     */
    fun onAudioFocusLost()

    /**
     * On audio focus restored.
     */
    fun onAudioFocusRestored()

    /**
     * Current sentence requested.
     * @param ttsbinder
     */
    fun onCurrentSentenceRequested(ttsbinder: TTSControlBinder)

    /**
     * Next sentence requested.
     * @param ttsbinder
     */
    fun onNextSentenceRequested(ttsbinder: TTSControlBinder)

    /**
     * Previous sentence requested.
     * @param ttsbinder
     */
    fun onPreviousSentenceRequested(ttsbinder: TTSControlBinder)

    /**
     * Stop requested.
     * @param ttsbinder
     */
    fun onStopRequested(ttsbinder: TTSControlBinder)
}
