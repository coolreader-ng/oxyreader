/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import kotlin.math.roundToInt

class MessageDialog() {

    enum class StandardButtons {
        Ok,
        Cancel,
        Yes,
        Continue,
        No,
        Close,
        Apply,
        Reset
    }

    private data class ButtonRecord(val type: StandardButtons) {
        var runnable: Runnable? = null

        constructor(type: StandardButtons, runnable: Runnable?) : this(type) {
            this.runnable = runnable
        }
    }

    private var mMessage: String? = null
    private var mTitle: String? = null
    private var mMessageId: Int = 0
    private var mTitleId: Int = 0
    private val mButtons: ArrayList<ButtonRecord> = ArrayList()
    private var mOptions: ArrayList<String>? = null
    private var mOptionValues: ArrayList<Boolean>? = null
    private var mInputTextMode = false
    private var mInputHelperText: String? = null
    private var mInputHelperTextId: Int = 0
    private var mInputPlaceholder: String? = null
    private var mInputText: String? = null

    constructor(title: String, message: String? = null) : this() {
        mMessage = message
        mTitle = title
    }

    constructor(titleId: Int, messageId: Int = 0) : this() {
        mMessageId = messageId
        mTitleId = titleId
    }

    fun setMessage(message: String): MessageDialog {
        mMessage = message
        mMessageId = 0
        return this
    }

    fun setMessage(messageId: Int): MessageDialog {
        mMessageId = messageId
        mMessage = null
        return this
    }

    fun setButton(button: StandardButtons, runnable: Runnable? = null): MessageDialog {
        removeButtonIfExists(button)
        mButtons.add(ButtonRecord(button, runnable))
        return this
    }

    fun addOption(option: String): MessageDialog {
        if (null == mOptions)
            mOptions = ArrayList()
        if (null == mOptionValues)
            mOptionValues = ArrayList()
        mOptions?.add(option)
        mOptionValues?.add(false)
        return this
    }

    val optionValues: Array<Boolean>?
        get() = mOptionValues?.toTypedArray()

    fun setInputTextMode(prompt: String): MessageDialog {
        mMessage = prompt
        mInputTextMode = true
        return this
    }

    fun setInputTextMode(promptId: Int): MessageDialog {
        mMessageId = promptId
        mMessage = null
        mInputTextMode = true
        return this
    }

    fun setInputHelperText(helperText: String): MessageDialog {
        mInputHelperText = helperText
        mInputHelperTextId = 0
        return this
    }

    fun setInputHelperText(helperTextId: Int): MessageDialog {
        mInputHelperTextId = helperTextId
        mInputHelperText = null
        return this
    }

    fun setInputPlaceholder(placeholder: String): MessageDialog {
        mInputPlaceholder = placeholder
        return this
    }

    val inputText: String?
        get() = mInputText

    private fun removeButtonIfExists(button: StandardButtons) {
        var idx = -1
        for (i in mButtons.indices) {
            val btn = mButtons[i]
            if (btn.type == button) {
                idx = i
                break
            }
        }
        if (idx >= 0)
            mButtons.removeAt(idx)
    }

    private fun buildDialog(context: Context): Dialog {
        var bld = MaterialAlertDialogBuilder(context)
        if (null != mMessage)
            bld = bld.setMessage(mMessage)
        else if (0 != mMessageId)
            bld = bld.setMessage(mMessageId)
        if (null != mTitle)
            bld = bld.setTitle(mTitle)
        else if (0 != mTitleId)
            bld = bld.setTitle(mTitleId)
        var onCancelRunnable: Runnable? = null
        if (mButtons.isNotEmpty()) {
            for (button in mButtons) {
                when (button.type) {
                    StandardButtons.Ok -> bld =
                        bld.setPositiveButton(android.R.string.ok) { _, _ -> button.runnable?.run() }

                    StandardButtons.Cancel -> {
                        bld =
                            bld.setNegativeButton(android.R.string.cancel) { _, _ -> button.runnable?.run() }
                        onCancelRunnable = button.runnable
                    }

                    StandardButtons.Yes -> bld =
                        bld.setPositiveButton(R.string.yes) { _, _ -> button.runnable?.run() }

                    StandardButtons.Continue -> bld =
                        bld.setPositiveButton(R.string.continue_) { _, _ -> button.runnable?.run() }

                    StandardButtons.No -> bld =
                        bld.setNegativeButton(R.string.no) { _, _ -> button.runnable?.run() }

                    StandardButtons.Close -> bld =
                        bld.setNegativeButton(R.string.close) { _, _ -> button.runnable?.run() }

                    StandardButtons.Apply -> bld =
                        bld.setNeutralButton(R.string.apply) { _, _ -> button.runnable?.run() }

                    StandardButtons.Reset -> bld =
                        bld.setNeutralButton(R.string.reset) { _, _ -> button.runnable?.run() }
                }
            }
        } else {
            bld = bld.setPositiveButton(android.R.string.ok) { _, _ -> run { } }
        }
        bld = bld.setCancelable(true)
        bld = bld.setOnCancelListener { onCancelRunnable?.run() }
        var dialogView: ViewGroup? = null
        mOptions?.let { options ->
            if (options.isNotEmpty()) {
                val group = LinearLayout(context)
                group.orientation = LinearLayout.VERTICAL
                dialogView = group
                val layoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                val _8dp =
                    Utils.convertUnits(8f, from = Utils.Units.DP, to = Utils.Units.PX).roundToInt()
                layoutParams.setMargins(_8dp, 0, _8dp, 0)
                for ((idx, option) in options.withIndex()) {
                    val checkBox =
                        CheckBox(context, null, com.google.android.material.R.attr.checkboxStyle)
                    checkBox.text = option
                    checkBox.tag = idx
                    group.addView(checkBox, layoutParams)
                    checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                        val tagData = buttonView.tag
                        if (tagData is Int) {
                            mOptionValues?.set(tagData, isChecked)
                        }
                    }
                }
            }
        }
        if (mInputTextMode) {
            if (null == dialogView) {
                val group = LinearLayout(context)
                group.orientation = LinearLayout.VERTICAL
                dialogView = group
            }
            val layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            val _8dp =
                Utils.convertUnits(8f, from = Utils.Units.DP, to = Utils.Units.PX).roundToInt()
            layoutParams.setMargins(_8dp, 0, _8dp, 0)
            val layoutInflater = LayoutInflater.from(context)
            val textInputLayout =
                layoutInflater.inflate(R.layout.md_textfield, dialogView, false) as TextInputLayout
            if (null != mInputHelperText)
                textInputLayout.helperText = mInputHelperText
            else if (0 != mInputHelperTextId)
                textInputLayout.helperText = context.getString(mInputHelperTextId)
            dialogView?.addView(textInputLayout, layoutParams)
            val editText = textInputLayout.editText
            mInputPlaceholder?.let { editText?.hint = it }
            editText?.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

                override fun afterTextChanged(s: Editable?) {
                    mInputText = s.toString()
                }
            })
        }
        dialogView?.let { bld.setView(it) }
        return bld.create()
    }

    fun showDialog(context: Context) {
        val dialog = buildDialog(context)
        dialog.show()
    }

}
