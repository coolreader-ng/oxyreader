/*
 * LxReader - book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader TTS module at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2015 by Vadim Lopatin <coolreader.org@gmail.com>
 * Copyright (C) 2020,2021 by Aleksey Chernov <valexlin@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s.tts

import android.app.Notification
import android.app.Notification.MediaStyle
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ServiceInfo
import android.database.ContentObserver
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.AudioManager.OnAudioFocusChangeListener
import android.media.MediaMetadata
import android.media.MediaPlayer
import android.media.session.MediaSession
import android.media.session.PlaybackState
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.provider.Settings
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.EngineInfo
import android.speech.tts.TextToSpeech.OnInitListener
import android.speech.tts.UtteranceProgressListener
import android.speech.tts.Voice
import androidx.annotation.RequiresApi
import io.gitlab.coolreader_ng.project_s.AbstractService
import io.gitlab.coolreader_ng.project_s.AbstractTask
import io.gitlab.coolreader_ng.project_s.BookCoverManager
import io.gitlab.coolreader_ng.project_s.BookInfo
import io.gitlab.coolreader_ng.project_s.R
import io.gitlab.coolreader_ng.project_s.ReaderActivity
import io.gitlab.coolreader_ng.project_s.ReaderEngineObjectsAccessor
import io.gitlab.coolreader_ng.project_s.SRLog
import io.gitlab.coolreader_ng.project_s.Utils
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.io.InputStreamReader
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.util.Locale
import java.util.Timer
import java.util.TimerTask
import kotlin.math.abs

/**
 * This service implement TTS functionality.
 * This service keep the application in the foreground (for the target API >= 26),
 * even if the main activity is in the background, so that the system understands that the application
 * does not need to be unloaded from memory while TTS is running.
 * It also adds TTS control buttons to the notification area and lock screen.
 * It also declare media session and control audio focus.
 */
@Suppress("NOTHING_TO_INLINE")
class TTSControlService : AbstractService("tts") {

    enum class State {
        PLAYING,
        PAUSED,
        STOPPED
    }

    private var mChannelCreated = false
    private val mBinder = TTSControlBinder(this)
    private var mNotificationManager: NotificationManager? = null
    private val mDBServiceAccessor = DBServiceAccessor(this)

    private var mTTS: TextToSpeech? = null
    private var mTTSInitialized = false
    private var mTTSEnginePackage: String? = null
    private var mInitTTSTimer: Timer? = null
    private var mAuthors: String? = null
    private var mTitle: String? = null
    private var mCurrentUtterance: String? = null
    private var mCoverBitmap: Bitmap? = null
    private var mTTSParamsBundle: Bundle? = null // for API21+
    private var mTTSParamsMap: HashMap<String, String>? = null // for API<21
    private var mUseEndOfSentenceWorkaround = false
    private var mUseDictionary = false
    private val mReplaceDictionary = HashMap<String, String>()
    private var mUseStopTimeout = false
    private var mStopTimeoutValue: Long = -1
    private var mUseImmobilityTimeout = false
    private var mImmobilityTimeoutValue: Long = -1
    private var mPlaybackStartTime: Long = -1
    private var mPlaybackTime: Long = -1
    private var mImmobilityStartTime: Long = -1
    private var mImmobilityPlaybackTime: Long = -1
    private var mLocale: Locale? = null
    private var mVoice: Voice? = null
    private var mOnUtteranceStopOnce: Runnable? = null
    private var mStatusListener: OnTTSStatusListener? = null
    private var mState = State.STOPPED
    private var mPrevState = State.STOPPED
    private var mSpeechRate = 0f
    private var mContinuousErrors = 0
    private var mAudioManager: AudioManager? = null
    private var mAudioFocusRequest: AudioFocusRequest? = null
    private var mPlaybackNowAuthorized = false
    private var mPlaybackDelayed = false
    private var mResumeOnFocusGain = false
    private var mMediaSession: MediaSession? = null
    private var mMediaSessionCallback: MediaSession.Callback? = null
    private var mPlaybackStateBuilder: PlaybackState.Builder? = null
    private var mMediaPlayer: MediaPlayer? = null
    private var mVolumeSettingsContentObserver: VolumeSettingsContentObserver? = null
    private var mSensorManager: SensorManager? = null
    private var mAccelerometer: Sensor? = null
    private val mAccelerometerValues = floatArrayOf(0.0f, 0.0f, 0.0f)
    private var mVolumeFadingHandler: Handler? = null
    private var mFadingVolumeOrig = -1
    private var mFadingVolumeCurrent = -1
    private var mFadingVolumeThreshold = -1
    private var mFadingVolumeIsDone = false
    private val mLocker = Any()

    private interface OnBookCoverLoaded {
        fun onBookCoverLoaded(bitmap: Bitmap?)
    }

    private val mTTSControlActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            log.debug("received action: $action")
            if (null != action) {
                when (action) {
                    TTS_CONTROL_ACTION_PLAY_PAUSE -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            if (State.PLAYING == mState)
                                mMediaSessionCallback?.onPause()
                            else
                                mMediaSessionCallback?.onPlay()
                        } else {
                            if (State.PLAYING == mState)
                                pauseWrapper_api_less_than_21()
                            else
                                playWrapper_api_less_than_21()
                        }
                    }

                    TTS_CONTROL_ACTION_SKIP_NEXT -> {
                        if (State.PLAYING == mState) {
                            stopUtterance_impl {
                                mStatusListener?.onNextSentenceRequested(mBinder)
                            }
                        } else {
                            mStatusListener?.onNextSentenceRequested(mBinder)
                        }
                    }

                    TTS_CONTROL_ACTION_SKIP_PREV -> {
                        if (State.PLAYING == mState) {
                            stopUtterance_impl {
                                mStatusListener?.onPreviousSentenceRequested(mBinder)
                            }
                        } else {
                            mStatusListener?.onPreviousSentenceRequested(mBinder)
                        }
                    }

                    TTS_CONTROL_ACTION_STOP -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            mMediaSessionCallback?.onStop()
                        else
                            stopWrapper_api_less_than_21()
                        mStatusListener?.onStopRequested(mBinder)
                    }
                }
            }
        }
    }

    private val mBecomingNoisyReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // See https://developer.android.com/reference/android/media/AudioManager#ACTION_AUDIO_BECOMING_NOISY
            log.debug("audio is about to become 'noisy' due to a change in audio outputs")
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY == intent.action) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    mMediaSessionCallback?.onPause()
                else
                    pauseWrapper_api_less_than_21()
            }
        }
    }

    private val mAudioFocusChangeListener = OnAudioFocusChangeListener { focusChange: Int ->
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                log.debug("audio focus gain")
                mPlaybackNowAuthorized = true
                if (mPlaybackDelayed || mResumeOnFocusGain) {
                    mPlaybackDelayed = false
                    mResumeOnFocusGain = false
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        mMediaSessionCallback?.onPlay()
                    else
                        playWrapper_api_less_than_21()
                    mStatusListener?.onAudioFocusRestored()
                }
            }

            AudioManager.AUDIOFOCUS_LOSS -> {
                log.debug("audio focus is lost")
                mPlaybackNowAuthorized = false
                mResumeOnFocusGain = false
                mPlaybackDelayed = false
                if (State.PLAYING == mState) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mMediaSessionCallback?.onPause()
                    } else {
                        // Fallback for API<21, direct control
                        pauseWrapper_api_less_than_21()
                    }
                    mStatusListener?.onAudioFocusLost()
                }
            }

            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT,
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                log.debug("audio focus temporary lost")
                // set to pause, and schedule to resume when focus gain again
                mPlaybackNowAuthorized = false
                mResumeOnFocusGain = false
                mPlaybackDelayed = false
                if (State.PLAYING == mState) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                        mMediaSessionCallback?.onPause()
                    else
                        pauseWrapper_api_less_than_21()
                    mResumeOnFocusGain = true
                }
            }
        }
    }

    private val mAccelerometerEventListener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent?) {
            if (Sensor.TYPE_ACCELEROMETER == event?.sensor?.type) {
                val values = event.values
                synchronized(mLocker) {
                    if (State.PLAYING == mState) {
                        val delta = floatArrayOf(
                            abs(values[0] - mAccelerometerValues[0]),
                            abs(values[1] - mAccelerometerValues[1]),
                            abs(values[2] - mAccelerometerValues[2]),
                        )
                        if (delta[0] >= ACCELEROMETER_IMMOBILITY_THRESHOLD ||
                            delta[1] >= ACCELEROMETER_IMMOBILITY_THRESHOLD ||
                            delta[2] >= ACCELEROMETER_IMMOBILITY_THRESHOLD
                        ) {
                            //log.debug("sensor delta: (${delta[0]}; ${delta[1]}; ${delta[2]})")
                            // the device was not stationary
                            mImmobilityStartTime = Utils.uptime()
                            mImmobilityPlaybackTime = 0
                            // if volume fading in process, cancel it
                            cancelVolumeFading()
                        } else {
                            // the device was stationary
                            if (mImmobilityStartTime < 0)
                                mImmobilityStartTime = Utils.uptime()
                            mImmobilityPlaybackTime = Utils.uptimeElapsed(mImmobilityStartTime)
                        }
                    }
                }
                mAccelerometerValues[0] = values[0]
                mAccelerometerValues[1] = values[1]
                mAccelerometerValues[2] = values[2]
            }
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            //log.debug("Sensor accuracy changed: new value = $accuracy")
        }
    }

    private val mVolumeFadingHandlerRunnable = VolumeFadingHandlerRunnable()

    private inner class VolumeFadingHandlerRunnable : Runnable {
        override fun run() {
            if (mFadingVolumeCurrent > mFadingVolumeThreshold) {
                // set new volume and continue
                val volumeDecValue =
                    (((100 * mFadingVolumeOrig + 50) / FADING_VOLUME_STEPS) / 100).coerceAtLeast(1)
                mFadingVolumeCurrent -= volumeDecValue
                log.debug("Fading volume to $mFadingVolumeCurrent")
                setVolume_impl(mFadingVolumeCurrent)
                mVolumeFadingHandler?.postDelayed(
                    mVolumeFadingHandlerRunnable,
                    FADING_VOLUME_PERIOD.toLong()
                )
            } else {
                log.debug("Volume fading done (volume ${mFadingVolumeCurrent}; threshold $mFadingVolumeThreshold)")
                synchronized(mLocker) {
                    mFadingVolumeIsDone = true
                    mVolumeFadingHandler = null
                }
            }
        }
    }

    private inner class VolumeSettingsContentObserver(handler: Handler?) :
        ContentObserver(handler) {
        private var mmPreviousVolume: Int
        private var mmMaxVolume: Int

        init {
            mmPreviousVolume = mAudioManager?.getStreamVolume(AudioManager.STREAM_MUSIC) ?: 0
            mmMaxVolume = mAudioManager?.getStreamMaxVolume(AudioManager.STREAM_MUSIC) ?: 100
        }

        override fun deliverSelfNotifications(): Boolean {
            return false
        }

        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)
            mAudioManager?.let {
                mmMaxVolume = it.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                val currentVolume = it.getStreamVolume(AudioManager.STREAM_MUSIC)
                if (currentVolume != mmPreviousVolume) {
                    mStatusListener?.onVolumeChanged(currentVolume, mmMaxVolume)
                    mmPreviousVolume = currentVolume
                }
            }
        }
    }

    override fun onCreate() {
        log.debug("onCreate")
        super.onCreate()
        mDBServiceAccessor.bind()
        mState = State.STOPPED
        mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        mAudioManager = getSystemService(AUDIO_SERVICE) as AudioManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val aaBuilder = AudioAttributes.Builder().apply {
                    setUsage(AudioAttributes.USAGE_MEDIA)
                    setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                }
                val afrBuilder = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN).apply {
                    setAudioAttributes(aaBuilder.build())
                    setAcceptsDelayedFocusGain(false)
                    setWillPauseWhenDucked(true)
                    setOnAudioFocusChangeListener(mAudioFocusChangeListener)
                }
                mAudioFocusRequest = afrBuilder.build()
            }
            mMediaSessionCallback = object : MediaSession.Callback() {

                /*
                override fun onMediaButtonEvent(mediaButtonIntent: Intent): Boolean {
                    log.verbose("onMediaButtonEvent: $mediaButtonIntent")
                    val extras = mediaButtonIntent.extras
                    if (null != extras) {
                        val event =
                            mediaButtonIntent.getParcelableExtra<KeyEvent>(Intent.EXTRA_KEY_EVENT)
                        log.error("onMediaButtonEvent: event=$event")
                    }
                    return super.onMediaButtonEvent(mediaButtonIntent)
                }
                 */

                override fun onPlay() {
                    mCurrentUtterance?.let { utterance ->
                        // check or request audio focus
                        if (!mPlaybackNowAuthorized)
                            requestAudioFocusWrapper()
                        if (!mPlaybackNowAuthorized) {
                            log.error("Can't say anything, audio focus is locked by other app.")
                            return
                        }
                        if (mPlaybackStartTime >= 0)
                            mPlaybackTime = Utils.uptimeElapsed(mPlaybackStartTime)
                        if (mUseStopTimeout) {
                            if (mPlaybackTime >= mStopTimeoutValue) {
                                log.debug("Maximum reading time exceeded: playback time - ${mPlaybackTime / 1000}s, timeout = ${mStopTimeoutValue / 1000}s")
                                mMediaSessionCallback?.onPause()
                                return
                            }
                        }
                        if (mUseImmobilityTimeout) {
                            synchronized(mLocker) {
                                if (mImmobilityPlaybackTime >= mImmobilityTimeoutValue) {
                                    if (!isVolumeFadingStarted()) {
                                        log.debug("Maximum immobility time exceeded: playback time - ${mImmobilityPlaybackTime / 1000}s, timeout = ${mImmobilityTimeoutValue / 1000}s")
                                        startVolumeFadingAndPause()
                                    }
                                }
                            }
                        }
                        // start to speech
                        if (say_impl(utterance)) {
                            if (mPlaybackStartTime < 0)
                                mPlaybackStartTime = Utils.uptime()
                            // update media session
                            var builder = MediaMetadata.Builder()
                            if (!mAuthors.isNullOrEmpty()) {
                                builder =
                                    builder.putString(MediaMetadata.METADATA_KEY_AUTHOR, mAuthors)
                                builder =
                                    builder.putString(MediaMetadata.METADATA_KEY_ARTIST, mAuthors)
                            }
                            if (!mTitle.isNullOrEmpty())
                                builder =
                                    builder.putString(MediaMetadata.METADATA_KEY_TITLE, mTitle)
                            if (null != mCoverBitmap) {
                                builder = builder.putBitmap(
                                    MediaMetadata.METADATA_KEY_ALBUM_ART,
                                    mCoverBitmap
                                )
                                builder =
                                    builder.putBitmap(MediaMetadata.METADATA_KEY_ART, mCoverBitmap)
                            }
                            // set the value of METADATA_KEY_DURATION to -1,
                            //  since we do not know the real duration of the sound
                            builder = builder.putLong(MediaMetadata.METADATA_KEY_DURATION, -1)
                            mMediaSession?.setMetadata(builder.build())
                            mMediaSession?.isActive = true
                            mMediaSession?.setPlaybackState(
                                mPlaybackStateBuilder?.setState(
                                    PlaybackState.STATE_PLAYING,
                                    PlaybackState.PLAYBACK_POSITION_UNKNOWN, mSpeechRate
                                )?.build()
                            )
                            registerReceiver(
                                mBecomingNoisyReceiver,
                                IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
                            )
                            synchronized(mLocker) {
                                mState = State.PLAYING
                                if (mState != mPrevState) {
                                    mStatusListener?.onStateChanged(mState)
                                    mPrevState = mState
                                }
                            }
                            // update notification
                            val notification = buildNotification(utterance)
                            if (null != notification) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                                    startForeground(
                                        NOTIFICATION_ID,
                                        notification,
                                        ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                                    )
                                else
                                    startForeground(NOTIFICATION_ID, notification)
                            } else {
                                log.error("Failed to build notification!")
                            }
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                // Workaround for API26+ bug:
                                // https://stackoverflow.com/questions/45960265/android-o-oreo-8-and-higher-media-buttons-issue
                                // https://issuetracker.google.com/issues/65344811
                                synchronized(mLocker) {
                                    if (null == mMediaPlayer) {
                                        mMediaPlayer = MediaPlayer.create(
                                            this@TTSControlService,
                                            R.raw.silence_10_0
                                        ).also {
                                            it.setOnCompletionListener {
                                                synchronized(mLocker) {
                                                    it.reset()
                                                    it.release()
                                                    mMediaPlayer = null
                                                }
                                            }
                                            it.start()
                                        }
                                    }
                                }
                            }
                        }
                    } ?: run {
                        mStatusListener?.onCurrentSentenceRequested(mBinder)
                        return
                    }
                }

                override fun onPause() {
                    stopUtterance_impl(null)
                    synchronized(mLocker) {
                        mState = State.PAUSED
                        if (mState != mPrevState) {
                            mStatusListener?.onStateChanged(mState)
                            mPrevState = mState
                        }
                    }
                    mMediaSession?.setPlaybackState(
                        mPlaybackStateBuilder?.setState(
                            PlaybackState.STATE_PAUSED,
                            PlaybackState.PLAYBACK_POSITION_UNKNOWN, 0f
                        )?.build()
                    )
                    val notification = buildNotification(null)
                    if (null != notification) {
                        mNotificationManager?.notify(NOTIFICATION_ID, notification)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                            stopForeground(STOP_FOREGROUND_DETACH)
                        else
                            stopForeground(false)
                    } else {
                        log.error("Failed to build notification!")
                    }
                    try {
                        unregisterReceiver(mBecomingNoisyReceiver)
                    } catch (ignored: Exception) {
                    }
                    mPlaybackStartTime = -1
                    mPlaybackTime = 0
                    mImmobilityStartTime = -1
                    mImmobilityPlaybackTime = 0
                }

                override fun onStop() {
                    stopUtterance_impl(null)
                    synchronized(mLocker) {
                        mState = State.STOPPED
                        if (mState != mPrevState) {
                            mStatusListener?.onStateChanged(mState)
                            mPrevState = mState
                        }
                    }
                    mMediaSession?.setPlaybackState(
                        mPlaybackStateBuilder?.setState(
                            PlaybackState.STATE_STOPPED,
                            PlaybackState.PLAYBACK_POSITION_UNKNOWN, 0f
                        )?.build()
                    )
                    mNotificationManager?.cancel(NOTIFICATION_ID)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        stopForeground(STOP_FOREGROUND_REMOVE)
                    else
                        stopForeground(true)
                    abandonAudioFocusRequestWrapper()
                    mMediaSession?.isActive = false
                    try {
                        unregisterReceiver(mBecomingNoisyReceiver)
                    } catch (ignored: Exception) {
                    }
                    mPlaybackStartTime = -1
                    mPlaybackTime = 0
                    mImmobilityStartTime = -1
                    mImmobilityPlaybackTime = 0
                }

                override fun onSkipToNext() {
                    sendBroadcast(Intent(TTS_CONTROL_ACTION_SKIP_NEXT))
                }

                override fun onSkipToPrevious() {
                    sendBroadcast(Intent(TTS_CONTROL_ACTION_SKIP_PREV))
                }
            }
            mPlaybackStateBuilder = PlaybackState.Builder()
                .setActions(
                    PlaybackState.ACTION_PLAY
                            or PlaybackState.ACTION_STOP
                            or PlaybackState.ACTION_PAUSE
                            or PlaybackState.ACTION_PLAY_PAUSE
                            or PlaybackState.ACTION_SKIP_TO_NEXT
                            or PlaybackState.ACTION_SKIP_TO_PREVIOUS
                )
            mMediaSession = MediaSession(this, "LxReader TTS").apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    setFlags(0)
                } else {
                    // This constant was deprecated in API level 26.
                    // https://developer.android.com/reference/android/media/session/MediaSession#FLAG_HANDLES_MEDIA_BUTTONS
                    // https://developer.android.com/reference/android/media/session/MediaSession#FLAG_HANDLES_TRANSPORT_CONTROLS
                    setFlags(MediaSession.FLAG_HANDLES_MEDIA_BUTTONS or MediaSession.FLAG_HANDLES_TRANSPORT_CONTROLS)
                }
                setCallback(mMediaSessionCallback)
                setPlaybackState(
                    mPlaybackStateBuilder?.setState(
                        PlaybackState.STATE_PAUSED,
                        PlaybackState.PLAYBACK_POSITION_UNKNOWN,
                        0f
                    )?.build()
                )
                setPlaybackToLocal(
                    AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .build()
                )
            }
        }
        val filter = IntentFilter()
        filter.addAction(TTS_CONTROL_ACTION_PLAY_PAUSE)
        filter.addAction(TTS_CONTROL_ACTION_SKIP_NEXT)
        filter.addAction(TTS_CONTROL_ACTION_SKIP_PREV)
        filter.addAction(TTS_CONTROL_ACTION_STOP)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            registerReceiver(mTTSControlActionReceiver, filter, Context.RECEIVER_EXPORTED)
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            registerReceiver(mTTSControlActionReceiver, filter, RECEIVER_VISIBLE_TO_INSTANT_APPS)
        else
            registerReceiver(mTTSControlActionReceiver, filter)
        mVolumeSettingsContentObserver = VolumeSettingsContentObserver(Handler())
        applicationContext.contentResolver.registerContentObserver(
            Settings.System.CONTENT_URI,
            true,
            mVolumeSettingsContentObserver!!
        )
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        log.debug("Received start id $startId: $intent")
        var action: String? = null
        // According to the documentation, intent can be null:
        // https://developer.android.com/reference/android/app/Service#onStartCommand(android.content.Intent,%20int,%20int)
        // https://developer.android.com/reference/android/app/Service#START_STICKY
        if (null != intent)
            action = intent.action
        if (TTS_CONTROL_ACTION_PREPARE == action) {
            mCoverBitmap = null
            mAuthors = null
            mTitle = null
            val data = intent?.extras
            if (null != data) {
                val bookInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                    data.getParcelable("bookInfo", BookInfo::class.java)
                else
                    data.getParcelable("bookInfo")
                bookInfo?.let {
                    mTitle = it.title
                    mAuthors = getBookAuthors(it)
                    getBookCover(it, object : OnBookCoverLoaded {
                        override fun onBookCoverLoaded(bitmap: Bitmap?) {
                            mCoverBitmap = bitmap
                            // Update notification
                            val notification = buildNotification(null)
                            if (null != notification) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                                    startForeground(
                                        NOTIFICATION_ID,
                                        notification,
                                        ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                                    )
                                else
                                    startForeground(NOTIFICATION_ID, notification)
                            } else {
                                log.error("Failed to build notification!")
                            }
                        }
                    })
                }
            }
            synchronized(mLocker) {
                mState = State.PAUSED
            }
            // switch this service to foreground
            val notification = buildNotification(null)
            if (null != notification) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    startForeground(
                        NOTIFICATION_ID,
                        notification,
                        ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                    )
                else
                    startForeground(NOTIFICATION_ID, notification)
            } else {
                log.error("Failed to build notification!")
            }
        }
        // TODO: process media buttons events (android.intent.action.MEDIA_BUTTON)
        //  API<21
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        log.info("onBind(): $intent")
        return mBinder
    }

    override fun onDestroy() {
        log.debug("onDestroy")
        mVolumeSettingsContentObserver?.let { observer ->
            applicationContext.contentResolver.unregisterContentObserver(observer)
        }
        synchronized(mLocker) {
            mMediaPlayer?.let {
                if (it.isPlaying)
                    it.stop()
                it.reset()
                it.release()
            }
            mMediaPlayer = null
        }
        mStatusListener = null
        try {
            unregisterReceiver(mTTSControlActionReceiver)
        } catch (ignored: Exception) {
        }
        try {
            unregisterReceiver(mBecomingNoisyReceiver)
        } catch (ignored: Exception) {
        }
        if (mUseImmobilityTimeout)
            disableImmobilityTimeout_impl()
        stopUtterance_impl(null)
        abandonAudioFocusRequestWrapper()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mMediaSession?.isActive = false
            mMediaSession?.release()
        }
        mDBServiceAccessor.unbind()
        mNotificationManager?.cancel(NOTIFICATION_ID)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager?.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID)
        }
        if (null != mTTS) {
            mTTSEnginePackage = null
            try {
                mTTS?.shutdown()
            } catch (_: Exception) {
            }
            mTTS = null
            mTTSInitialized = false
        }
        super.onDestroy()
    }

    override fun onLowMemory() {
        log.debug("onLowMemory")
    }

    fun runOn(runnable: Runnable?) {
        postRunnable(runnable!!)
    }

    // Interfaces for returning the values of this service's operations

    interface RetrieveStateCallback {
        fun onResult(state: State?)
    }

    interface BooleanResultCallback {
        fun onResult(result: Boolean)
    }

    interface VolumeResultCallback {
        fun onResult(current: Int, max: Int)
    }

    interface IntResultCallback {
        fun onResult(result: Int)
    }

    interface FloatResultCallback {
        fun onResult(result: Float)
    }

    interface StringResultCallback {
        fun onResult(result: String?)
    }

    interface RetrieveEnginesListCallback {
        fun onResult(list: List<EngineInfo>?)
    }

    interface RetrieveLocalesListCallback {
        fun onResult(list: List<Locale>?)
    }

    interface RetrieveVoiceCallback {
        fun onResult(voice: Voice?)
    }

    interface RetrieveVoicesListCallback {
        fun onResult(list: List<Voice>?)
    }

    // specific functions of this service

    /**
     * Create TTS instance. Or recreate (to change engine).
     * @param engine engine package name
     * @param listener
     */
    private fun initTTS_impl(engine: String?, listener: OnTTSCreatedListener?) {
        if (mTTSInitialized && null != mTTS && null != mTTSEnginePackage && mTTSEnginePackage == engine) {
            listener?.onCreated()
            return
        }
        if (null != mTTS) {
            mTTS?.let {
                try {
                    it.stop()
                    it.shutdown()
                } catch (_: Exception) {
                }
            }
            mTTS = null
        }
        mTTSInitialized = false
        val isSpeaking = State.PLAYING == mState
        val onInitListener = OnInitListener { status: Int ->
            mInitTTSTimer?.cancel()
            mInitTTSTimer = null
            log.info("TTS init status: $status")
            if (status == TextToSpeech.SUCCESS) {
                mTTSInitialized = true
                mTTSEnginePackage = engine
                setupTTSHandlers()
                listener?.onCreated()
                if (isSpeaking)
                    say_impl(mCurrentUtterance)
            } else {
                try {
                    mTTS?.shutdown()
                } catch (ignored: Exception) {
                }
                mTTS = null
                listener?.onFailed()
            }
        }
        mInitTTSTimer = Timer().also {
            it.schedule(object : TimerTask() {
                override fun run() {
                    // TTS engine init hangs, call listener
                    log.error("TTS engine \"$engine\" init failure with timeout!")
                    listener?.onTimedOut()
                    it.cancel()
                    mInitTTSTimer = null
                    try {
                        mTTS?.shutdown()
                    } catch (ignored: Exception) {
                    }
                    mTTS = null
                }
            }, INIT_TTS_TIMEOUT)
        }
        mTTS = if (!engine.isNullOrEmpty())
            TextToSpeech(this, onInitListener, engine)
        else
            TextToSpeech(this, onInitListener)
    }

    /**
     * Set media info: authors, title and cover.
     * @param authors book's author(s)
     * @param title book's title
     * @param cover book's cover
     */
    private fun setMediaItemInfo_impl(bookInfo: BookInfo) {
        synchronized(mLocker) {
            mAuthors = getBookAuthors(bookInfo)
            mTitle = bookInfo.title
            getBookCover(bookInfo, object : OnBookCoverLoaded {
                override fun onBookCoverLoaded(bitmap: Bitmap?) {
                    mCoverBitmap = bitmap
                    // update notification
                    val notification = buildNotification(mCurrentUtterance)
                    if (null != notification) {
                        var isSpeaking: Boolean
                        synchronized(mLocker) {
                            isSpeaking = State.PLAYING == mState
                        }
                        if (isSpeaking) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                                startForeground(
                                    NOTIFICATION_ID,
                                    notification,
                                    ServiceInfo.FOREGROUND_SERVICE_TYPE_MEDIA_PLAYBACK
                                )
                            else
                                startForeground(NOTIFICATION_ID, notification)
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                stopForeground(STOP_FOREGROUND_DETACH)
                            else
                                stopForeground(false)
                            mNotificationManager?.notify(NOTIFICATION_ID, notification)
                        }
                    } else {
                        log.error("Failed to build notification!")
                    }
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                        var builder = MediaMetadata.Builder()
                        if (!mAuthors.isNullOrEmpty())
                            builder = builder.putString(MediaMetadata.METADATA_KEY_AUTHOR, mAuthors)
                        if (!mTitle.isNullOrEmpty())
                            builder = builder.putString(MediaMetadata.METADATA_KEY_TITLE, mTitle)
                        mCoverBitmap?.let {
                            builder = builder.putBitmap(MediaMetadata.METADATA_KEY_ALBUM_ART, it)
                        }
                        mMediaSession?.setMetadata(builder.build())
                    }
                }
            })
        }
    }

    private fun getState_impl(): State {
        synchronized(mLocker) {
            return mState
        }
    }

    /**
     * Start to speech specified text.
     * @param utterance Utterance to speech.
     */
    private fun say_impl(utterance: String?): Boolean {
        mTTS?.let { tts ->
            val ret: Int
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (null == mTTSParamsBundle) {
                    mTTSParamsBundle = Bundle().apply {
                        putInt(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_MUSIC)
                    }
                }
                ret = tts.speak(
                    utterance,
                    TextToSpeech.QUEUE_ADD,
                    mTTSParamsBundle,
                    APP_UTTERANCE_ID
                )
            } else {
                if (null == mTTSParamsMap) {
                    mTTSParamsMap = HashMap<String, String>().apply {
                        put(
                            TextToSpeech.Engine.KEY_PARAM_STREAM,
                            AudioManager.STREAM_MUSIC.toString()
                        )
                        put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, APP_UTTERANCE_ID)
                    }
                }
                ret = tts.speak(utterance, TextToSpeech.QUEUE_ADD, mTTSParamsMap)
            }
            return TextToSpeech.SUCCESS == ret
        }
        return false
    }

    /**
     * Stop to speech current utterance. Optional callback can be used to implement sentence switch.
     * @param callback runnable to executed after completion.
     */
    private fun stopUtterance_impl(callback: Runnable?): Boolean {
        mTTS?.let { tts ->
            mOnUtteranceStopOnce = callback
            if (tts.isSpeaking) {
                return tts.stop() == TextToSpeech.SUCCESS
            }
        }
        return false
    }

    private fun playWrapper_api_less_than_21() {
        mCurrentUtterance?.let { utterance ->
            if (!mPlaybackNowAuthorized)
                requestAudioFocusWrapper()
            if (mPlaybackNowAuthorized) {
                if (mPlaybackStartTime >= 0)
                    mPlaybackTime = Utils.uptimeElapsed(mPlaybackStartTime)
                if (mUseStopTimeout) {
                    if (mStopTimeoutValue >= mPlaybackTime) {
                        log.debug("Maximum reading time exceeded: playback time - ${mPlaybackTime / 1000}s, timeout = ${mStopTimeoutValue / 1000}s")
                        pauseWrapper_api_less_than_21()
                        return
                    }
                }
                if (mUseImmobilityTimeout) {
                    synchronized(mLocker) {
                        if (mImmobilityPlaybackTime >= mImmobilityTimeoutValue) {
                            if (!isVolumeFadingStarted()) {
                                log.debug("Maximum immobility time exceeded: playback time - ${mImmobilityPlaybackTime / 1000}s, timeout = ${mImmobilityTimeoutValue / 1000}s")
                                startVolumeFadingAndPause()
                            }
                        }
                    }
                }
                // start to speech
                if (say_impl(utterance)) {
                    if (mPlaybackStartTime < 0)
                        mPlaybackStartTime = Utils.uptime()
                    synchronized(mLocker) {
                        mState = State.PLAYING
                        if (mState != mPrevState) {
                            mStatusListener?.onStateChanged(mState)
                            mPrevState = mState
                        }
                    }
                    // update notification
                    val notification = buildNotification(utterance)
                    if (null != notification) {
                        startForeground(NOTIFICATION_ID, notification)
                    } else {
                        log.error("Failed to build notification!")
                    }
                    registerReceiver(
                        mBecomingNoisyReceiver,
                        IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
                    )
                } else {
                    log.error("tts failed to speak")
                }
            } else {
                log.error("Can't say anything, audio focus is locked by other app.")
            }
        } ?: run {
            mStatusListener?.onCurrentSentenceRequested(mBinder)
        }
    }

    private fun pauseWrapper_api_less_than_21() {
        stopUtterance_impl(null)
        synchronized(mLocker) {
            mState = State.PAUSED
            if (mState != mPrevState) {
                mStatusListener?.onStateChanged(mState)
                mPrevState = mState
            }
        }
        // update notification
        val notification = buildNotification(null)
        if (null != notification) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                stopForeground(STOP_FOREGROUND_DETACH)
            else
                stopForeground(false)
            mNotificationManager?.notify(NOTIFICATION_ID, notification)
        } else {
            log.error("Failed to build notification!")
        }
        try {
            unregisterReceiver(mBecomingNoisyReceiver)
        } catch (ignored: Exception) {
        }
        mPlaybackStartTime = -1
        mPlaybackTime = 0
        mImmobilityStartTime = -1
        mImmobilityPlaybackTime = 0
    }

    private fun stopWrapper_api_less_than_21() {
        stopUtterance_impl(null)
        synchronized(mLocker) {
            mState = State.STOPPED
            if (mState != mPrevState) {
                mStatusListener?.onStateChanged(mState)
                mPrevState = mState
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            stopForeground(STOP_FOREGROUND_REMOVE)
        else
            stopForeground(true)
        mNotificationManager?.cancel(NOTIFICATION_ID)
        abandonAudioFocusRequestWrapper()
        try {
            unregisterReceiver(mBecomingNoisyReceiver)
        } catch (ignored: Exception) {
        }
        mTitle = null
        mCoverBitmap = null
        mAuthors = null
        mPlaybackStartTime = -1
        mPlaybackTime = 0
        mImmobilityStartTime = -1
        mImmobilityPlaybackTime = 0
    }

    /**
     * Gets list of available engines.
     * @return list of available engines.
     */
    private fun getAvailableEngines_impl(): List<EngineInfo>? {
        return mTTS?.engines
    }

    /**
     * Gets list of available locales in current engine
     * @return list of available locales in current engine
     */
    private fun getAvailableLocales_impl(): List<Locale>? {
        mTTS?.let { tts ->
            val list = ArrayList<Locale>(0)
            var languages: Set<Locale>? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                try {
                    languages = tts.availableLanguages
                    languages?.let {
                        for (locale in it) {
                            list.add(locale)
                        }
                    }
                } catch (e: Exception) {
                    log.error("Failed to get available languages: TextToSpeech.getAvailableLanguages() failed: $e")
                }
            }
            if (null == languages) {
                // API < 21 or TextToSpeech.getAvailableLanguages() failed
                Locale.getAvailableLocales().let {
                    // Fill list without duplicates
                    val map = HashMap<Locale, Boolean>()
                    for (locale in it) {
                        when (tts.isLanguageAvailable(locale)) {
                            TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE ->
                                map[locale] = true

                            TextToSpeech.LANG_COUNTRY_AVAILABLE -> {
                                val newLocale = Locale(locale.language, locale.country)
                                map[newLocale] = true
                            }

                            TextToSpeech.LANG_AVAILABLE -> {
                                val newLocale = Locale(locale.language)
                                map[newLocale] = true
                            }
                        }
                    }
                    for (key in map.keys) {
                        list.add(key)
                    }
                }
            }
            // Sort lexicographically
            list.sortWith { o1: Locale, o2: Locale ->
                o1.toString().compareTo(o2.toString(), ignoreCase = true)
            }
            return list
        }
        return null
    }

    /**
     * Gets list of available voices in current engine for specified locale.
     * @param locale specific locale
     * @return list of available voices in current engine for specified locale.
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun getAvailableVoices_impl(locale: Locale): List<Voice>? {
        mTTS?.let { tts ->
            val list = ArrayList<Voice>().also {
                var voices: Set<Voice>? = null
                try {
                    voices = tts.voices
                } catch (e: Exception) {
                    log.error("Failed to get available voices: TextToSpeech.getVoices() failed: $e")
                }
                voices?.let { collection ->
                    for (voice in collection) {
                        if (voice.locale.toString().equals(locale.toString(), ignoreCase = true))
                            it.add(voice)
                    }
                }
                it.sortWith { o1: Voice, o2: Voice ->
                    o1.name.compareTo(o2.name)
                }
            }
            return list
        }
        return null
    }

    /**
     * Get current TTS language.
     * @return current TTS language.
     */
    private fun getLanguage_impl(): String? {
        synchronized(mLocker) {
            return mLocale?.let { Utils.localeToLangTag(it) }
        }
    }

    /**
     * Set TTS language. Voice can't be set with this function.
     * @param langTag language tag (IETF BCP 47)
     * @return
     */
    private fun setLanguage_impl(langTag: String): Boolean {
        // set language for TTS by locale name
        synchronized(mLocker) {
            val locale = Utils.langTagToLocale(langTag)
            when (mTTS?.setLanguage(locale)) {
                TextToSpeech.LANG_AVAILABLE,
                TextToSpeech.LANG_COUNTRY_AVAILABLE,
                TextToSpeech.LANG_COUNTRY_VAR_AVAILABLE -> {
                    mLocale = locale
                    return true
                }
            }
            mLocale = null
        }
        return false
    }

    /**
     * Get current TTS voice.
     * @return current TTS voice if set, otherwise null.
     */
    private fun getVoice_impl(): Voice? {
        synchronized(mLocker) {
            return mVoice
        }
    }

    /**
     * Set TTS voice. Override setLanguage() call.
     * @param voice specific voice
     * @return true if operation is successful, false otherwise.
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setVoice_impl(voice: Voice): Boolean {
        synchronized(mLocker) {
            mTTS?.let { tts ->
                if (TextToSpeech.SUCCESS == tts.setVoice(voice)) {
                    mVoice = voice
                    mLocale = voice.locale
                    return true
                }
            }
            mVoice = null
            mLocale = null
        }
        return false
    }

    /**
     * Set TTS voice. Override setLanguage() call.
     * @param voiceName specific voice name
     * @return true if operation is successful, false otherwise.
     */
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setVoice_impl(voiceName: String): Boolean {
        synchronized(mLocker) {
            mTTS?.let { tts ->
                tts.voices?.let { voices ->
                    for (voice in voices) {
                        if (voice.name == voiceName) {
                            if (TextToSpeech.SUCCESS == tts.setVoice(voice)) {
                                mVoice = voice
                                mLocale = voice.locale
                                return true
                            }
                        }
                    }
                }
            }
            mVoice = null
            mLocale = null
        }
        return false
    }

    private fun getSpeechRate_impl(): Float {
        synchronized(mLocker) {
            return mSpeechRate
        }
    }

    private fun setSpeechRate_impl(rate: Float): Boolean {
        synchronized(mLocker) {
            mTTS?.let { tts ->
                if (mSpeechRate != rate) {
                    if (TextToSpeech.SUCCESS == tts.setSpeechRate(rate)) {
                        mSpeechRate = rate
                        mStatusListener?.onSpeechRateChanged(mSpeechRate)
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun getMaxVolume_impl(): Int {
        return mAudioManager?.getStreamMaxVolume(AudioManager.STREAM_MUSIC) ?: -1
    }

    private fun getVolume_impl(): Int {
        return mAudioManager?.getStreamVolume(AudioManager.STREAM_MUSIC) ?: -1
    }

    private fun setVolume_impl(volume: Int) {
        mAudioManager?.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0)
    }

    private fun enableImmobilityTimeout_impl(timeout: Long) {
        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager?
        mAccelerometer = mSensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mAccelerometer?.let { sensor ->
            if (mUseImmobilityTimeout)
                mSensorManager?.unregisterListener(mAccelerometerEventListener)
            mSensorManager?.registerListener(
                mAccelerometerEventListener,
                sensor,
                SensorManager.SENSOR_DELAY_NORMAL
            )?.let { res ->
                mUseImmobilityTimeout = res
                mImmobilityTimeoutValue = timeout
                if (!res)
                    log.error("Failed to register accelerometer event listener.")
            }
        } ?: run {
            log.error("Failed to use accelerometer sensor.")
        }
    }

    private fun disableImmobilityTimeout_impl() {
        if (mUseImmobilityTimeout) {
            mUseImmobilityTimeout = false
            synchronized(mLocker) {
                cancelVolumeFading()
            }
            mSensorManager?.unregisterListener(mAccelerometerEventListener)
            mAccelerometer = null
            mSensorManager = null
        }
    }

    // Functions to access this service (wrappers)

    fun initTTS(engine: String?, listener: OnTTSCreatedListener?) {
        postTask(object : AbstractTask("initTTS") {
            override fun work() {
                initTTS_impl(engine, listener)
            }
        })
    }

    fun setMediaItemInfo(bookInfo: BookInfo) {
        postTask(object : AbstractTask("setMediaItemInfo") {
            override fun work() {
                setMediaItemInfo_impl(bookInfo)
            }
        })
    }

    fun retrieveState(callback: RetrieveStateCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveState") {
            override fun work() {
                val result = getState_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun say(utterance: String?, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("say") {
            override fun work() {
                mCurrentUtterance = preprocessUtterance(utterance)
                var result: Boolean
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    mMediaSessionCallback?.onPlay()
                else
                    playWrapper_api_less_than_21()
                synchronized(mLocker) {
                    result = State.PLAYING == mState
                }
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun pause(callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("pause") {
            override fun work() {
                var result: Boolean
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    mMediaSessionCallback?.onPause()
                else
                    pauseWrapper_api_less_than_21()
                synchronized(mLocker) {
                    result = State.PAUSED == mState
                }
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun stopUtterance(callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("stopUtterance") {
            override fun work() {
                val result = stopUtterance_impl(null)
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun stop(callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("stop") {
            override fun work() {
                var result: Boolean
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    mMediaSessionCallback?.onStop()
                else
                    stopWrapper_api_less_than_21()
                synchronized(mLocker) {
                    result = State.PAUSED == mState
                }
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveAvailableEngines(callback: RetrieveEnginesListCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveAvailableEngines") {
            override fun work() {
                val result = getAvailableEngines_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveAvailableLocales(callback: RetrieveLocalesListCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveAvailableLocales") {
            override fun work() {
                val result = getAvailableLocales_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun retrieveAvailableVoices(
        locale: Locale,
        callback: RetrieveVoicesListCallback,
        handler: Handler?
    ) {
        postTask(object : AbstractTask("retrieveAvailableVoices") {
            override fun work() {
                val result = getAvailableVoices_impl(locale)
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveLanguage(callback: StringResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveLanguage") {
            override fun work() {
                val result = getLanguage_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun setLanguage(langTag: String, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("setLanguage") {
            override fun work() {
                val result = setLanguage_impl(langTag)
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveVoice(callback: RetrieveVoiceCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveLanguage") {
            override fun work() {
                val result = getVoice_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setVoice(voice: Voice, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("setVoice") {
            override fun work() {
                val result = setVoice_impl(voice)
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setVoice(voiceName: String, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("setVoice(name)") {
            override fun work() {
                val result = setVoice_impl(voiceName)
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveSpeechRate(callback: FloatResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveSpeechRate") {
            override fun work() {
                val result = getSpeechRate_impl()
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun setSpeechRate(rate: Float, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("setSpeechRate") {
            override fun work() {
                val result = setSpeechRate_impl(rate)
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveVolume(callback: VolumeResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveVolume") {
            override fun work() {
                val maxVolume = getMaxVolume_impl()
                val result: Int = getVolume_impl()
                sendTask(handler) { callback.onResult(result, maxVolume) }
            }
        })
    }

    fun setVolume(volume: Int) {
        postTask(object : AbstractTask("setVolume") {
            override fun work() {
                setVolume_impl(volume)
            }
        })
    }

    fun getStatusListener(): OnTTSStatusListener? {
        return mStatusListener
    }

    fun setStatusListener(listener: OnTTSStatusListener?) {
        mStatusListener = listener
    }

    fun retrieveUseEndOfSentenceWorkaround(callback: BooleanResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveUseEndOfSentenceWorkaround") {
            override fun work() {
                val result = mUseEndOfSentenceWorkaround
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun setUseEndOfSentenceWorkaround(value: Boolean) {
        postTask(object : AbstractTask("setUseEndOfSentenceWorkaround") {
            override fun work() {
                mUseEndOfSentenceWorkaround = value
            }
        })
    }

    fun retrieveUseDictionary(callback: BooleanResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveUseDictionary") {
            override fun work() {
                val result = mUseDictionary
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun retrieveDictionarySize(callback: IntResultCallback, handler: Handler?) {
        postTask(object : AbstractTask("retrieveDictionarySize") {
            override fun work() {
                val result = mReplaceDictionary.size
                sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun setUseDictionary(value: Boolean) {
        postTask(object : AbstractTask("setUseDictionary") {
            override fun work() {
                mUseDictionary = value
                if (mUseDictionary)
                    loadDictionary()
                else
                    clearDictionary()
            }
        })
    }

    fun setDictionary(
        dictionary: HashMap<String, String>,
        callback: BooleanResultCallback?,
        handler: Handler?
    ) {
        postTask(object : AbstractTask("setDictionary") {
            override fun work() {
                mReplaceDictionary.clear()
                mReplaceDictionary.putAll(dictionary)
                val result = saveDictionary()
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun importDictionary(uri: Uri, callback: BooleanResultCallback?, handler: Handler?) {
        postTask(object : AbstractTask("importDictionary") {
            override fun work() {
                var result = false
                val dictionary = importDictionary_impl(uri)
                dictionary?.let {
                    if (it.isNotEmpty()) {
                        mReplaceDictionary.clear()
                        mReplaceDictionary.putAll(it)
                        result = saveDictionary()
                    }
                }
                if (null != callback)
                    sendTask(handler) { callback.onResult(result) }
            }
        })
    }

    fun setUseStopTimeout(useTimeout: Boolean, timeout: Long) {
        postTask(object : AbstractTask("setUseStopTimeout") {
            override fun work() {
                mUseStopTimeout = useTimeout
                mStopTimeoutValue = timeout
            }
        })
    }

    fun setUseImmobilityTimeout(useTimeout: Boolean, timeout: Long) {
        postTask(object : AbstractTask("setUseImmobilityTimeout") {
            override fun work() {
                if (useTimeout)
                    enableImmobilityTimeout_impl(timeout)
                else
                    disableImmobilityTimeout_impl()
            }
        })
    }

    // private helper functions

    private fun buildNotification(utterance: String?): Notification? {
        var title = mAuthors ?: ""
        if (!mTitle.isNullOrEmpty()) {
            title = if (title.isNotEmpty()) {
                "$title - $mTitle"
            } else {
                mTitle!!
            }
        }
        if (title.isEmpty())
            title = "LxReader"
        val notification: Notification
        val notificationIntent = Intent(this, ReaderActivity::class.java)
        val contentIntent =
            PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE)
        var builder: Notification.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
            // create notification channel
            if (!mChannelCreated) {
                val channel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    "LxReader TTS",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.description = "LxReader TTS control"
                channel.enableLights(false)
                channel.enableVibration(false)
                channel.setSound(null, null)
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                mNotificationManager?.let {
                    it.createNotificationChannel(channel)
                    mChannelCreated = true
                }
            }
            if (mChannelCreated)
                builder = builder.setChannelId(NOTIFICATION_CHANNEL_ID)
            else
                return null
        } else {
            builder = Notification.Builder(this)
            builder = builder.setDefaults(0)
            builder = builder.setPriority(Notification.PRIORITY_DEFAULT)
        }
        builder = builder.setSmallIcon(R.drawable.ic_logo)
        builder = builder.setContentTitle(title)
        builder = if (!utterance.isNullOrEmpty())
            builder.setContentText(utterance)
        else
            builder.setContentText("...")
        builder = builder.setOngoing(true)
        builder = builder.setAutoCancel(false)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            builder = builder.setShowWhen(false)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
                builder = builder.setLocalOnly(true)
                // add actions
                // play/pause
                val playPauseIntent = PendingIntent.getBroadcast(
                    this,
                    0,
                    Intent(TTS_CONTROL_ACTION_PLAY_PAUSE),
                    PendingIntent.FLAG_IMMUTABLE
                )
                var actionBld = Notification.Action.Builder(
                    if (mState == State.PAUSED) R.drawable.ic_media_play else R.drawable.ic_media_pause,
                    "",
                    playPauseIntent
                )
                val actionPlayPause = actionBld.build()
                builder = builder.addAction(actionPlayPause)
                // prev
                val prevIntent = PendingIntent.getBroadcast(
                    this,
                    0,
                    Intent(TTS_CONTROL_ACTION_SKIP_PREV),
                    PendingIntent.FLAG_IMMUTABLE
                )
                actionBld =
                    Notification.Action.Builder(R.drawable.ic_media_skip_prev, "", prevIntent)
                val actionPrev = actionBld.build()
                builder = builder.addAction(actionPrev)
                // next
                val nextIntent = PendingIntent.getBroadcast(
                    this,
                    0,
                    Intent(TTS_CONTROL_ACTION_SKIP_NEXT),
                    PendingIntent.FLAG_IMMUTABLE
                )
                actionBld =
                    Notification.Action.Builder(R.drawable.ic_media_skip_next, "", nextIntent)
                val actionNext = actionBld.build()
                builder = builder.addAction(actionNext)
                // stop
                val stopIntent = PendingIntent.getBroadcast(
                    this,
                    0,
                    Intent(TTS_CONTROL_ACTION_STOP),
                    PendingIntent.FLAG_IMMUTABLE
                )
                actionBld = Notification.Action.Builder(R.drawable.ic_media_stop, "", stopIntent)
                val actionStop = actionBld.build()
                builder = builder.addAction(actionStop)
                //
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = builder.setSound(null, null)
                    builder = builder.setStyle(
                        MediaStyle().setShowActionsInCompactView(0, 3).setMediaSession(
                            mMediaSession!!.sessionToken
                        )
                    )
                    builder = builder.setColor(Color.GRAY)
                    builder = builder.setVisibility(Notification.VISIBILITY_PUBLIC)
                    mCoverBitmap?.let {
                        builder = builder.setLargeIcon(it)
                    }
                }
            }
        } else {
            builder = builder.setWhen(System.currentTimeMillis())
        }
        // delete intent
        val delPendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            Intent(TTS_CONTROL_ACTION_STOP),
            PendingIntent.FLAG_IMMUTABLE
        )
        builder = builder.setDeleteIntent(delPendingIntent)
        builder = builder.setContentIntent(contentIntent)
        notification = builder.build()
        return notification
    }

    private fun getBookAuthors(bookInfo: BookInfo): String {
        val bookAuthors = StringBuilder()
        bookInfo.authors?.let { authors ->
            val iter = authors.iterator()
            while (iter.hasNext()) {
                bookAuthors.append(iter.next())
                if (iter.hasNext())
                    bookAuthors.append(", ")
            }
        }
        return bookAuthors.toString()
    }

    private fun getBookCover(
        bookInfo: BookInfo,
        resultCallback: OnBookCoverLoaded
    ) {
        postRunnable {
            val engineObjects = ReaderEngineObjectsAccessor.leaseEngineObjects()
            //engineObjects.crEngineNGBinding?.setupEngineData(this)
            engineObjects.crEngineNGBinding?.let { crEngineNGBinding ->
                val bookCoverManager =
                    BookCoverManager(this, mDBServiceAccessor, crEngineNGBinding)
                bookCoverManager.getBookCoverDrawable(
                    bookInfo,
                    MEDIA_COVER_WIDTH,
                    MEDIA_COVER_HEIGHT,
                    object : BookCoverManager.BookCoverDrawableResultCallback {
                        override fun onResult(drawable: Drawable?) {
                            if (drawable is BitmapDrawable)
                                resultCallback.onBookCoverLoaded(drawable.bitmap)
                            else
                                resultCallback.onBookCoverLoaded(null)
                        }
                    }
                )
            }
            ReaderEngineObjectsAccessor.releaseEngineObject(engineObjects)
        }
    }

    private fun setupTTSHandlers() {
        mTTS?.let { tts ->
            tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onStart(utteranceId: String) {
                }

                override fun onDone(utteranceId: String) {
                    var gotoNextSentence = true
                    synchronized(mLocker) {
                        if (mUseImmobilityTimeout && mFadingVolumeIsDone) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                mMediaSessionCallback?.onPause()
                            else
                                pauseWrapper_api_less_than_21()
                            mFadingVolumeIsDone = false
                            mImmobilityStartTime = -1
                            mImmobilityPlaybackTime = 0
                            // Restore original volume
                            setVolume_impl(mFadingVolumeOrig)
                            gotoNextSentence = false
                        }
                    }
                    mOnUtteranceStopOnce?.run()
                    mOnUtteranceStopOnce = null
                    mStatusListener?.let {
                        it.onUtteranceDone()
                        if (gotoNextSentence)
                            it.onNextSentenceRequested(mBinder)
                    }
                    synchronized(mLocker) {
                        mMediaPlayer?.let {
                            if (it.isPlaying)
                                it.stop()
                            it.reset()
                            it.release()
                            mMediaPlayer = null
                        }
                    }
                    mContinuousErrors = 0
                }

                @Deprecated("Deprecated in Java")
                override fun onError(utteranceId: String) {
                    log.error("TTS error")
                    mContinuousErrors++
                    if (mContinuousErrors >= MAX_CONTINUOUS_ERRORS) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            mMediaSessionCallback?.onStop()
                        else
                            stopWrapper_api_less_than_21()
                        mStatusListener?.onError(0)
                    } else {
                        // If error count is low - process as 'onDone' event
                        var gotoNextSentence = true
                        synchronized(mLocker) {
                            if (mUseImmobilityTimeout && mFadingVolumeIsDone) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                    mMediaSessionCallback?.onPause()
                                else
                                    pauseWrapper_api_less_than_21()
                                mFadingVolumeIsDone = false
                                mImmobilityStartTime = -1
                                mImmobilityPlaybackTime = 0
                                // Restore original volume
                                setVolume_impl(mFadingVolumeOrig)
                                gotoNextSentence = false
                            }
                        }
                        mOnUtteranceStopOnce?.run()
                        mOnUtteranceStopOnce = null
                        mStatusListener?.let {
                            it.onUtteranceDone()
                            if (gotoNextSentence)
                                it.onNextSentenceRequested(mBinder)
                        }
                        synchronized(mLocker) {
                            mMediaPlayer?.let {
                                if (it.isPlaying)
                                    it.stop()
                                it.reset()
                                it.release()
                                mMediaPlayer = null
                            }
                        }
                    }
                }

                // API 21
                override fun onError(utteranceId: String, errorCode: Int) {
                    log.error("TTS error, code=$errorCode")
                    mContinuousErrors++
                    if (mContinuousErrors >= MAX_CONTINUOUS_ERRORS) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            mMediaSessionCallback?.onStop()
                        else
                            stopWrapper_api_less_than_21()
                        mStatusListener?.onError(errorCode)
                    } else {
                        // If error count is low - process as 'onDone' event
                        var gotoNextSentence = true
                        synchronized(mLocker) {
                            if (mUseImmobilityTimeout && mFadingVolumeIsDone) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                                    mMediaSessionCallback?.onPause()
                                else
                                    pauseWrapper_api_less_than_21()
                                mFadingVolumeIsDone = false
                                mImmobilityStartTime = -1
                                mImmobilityPlaybackTime = 0
                                // Restore original volume
                                setVolume_impl(mFadingVolumeOrig)
                                gotoNextSentence = false
                            }
                        }
                        mOnUtteranceStopOnce?.run()
                        mOnUtteranceStopOnce = null
                        mStatusListener?.let {
                            it.onUtteranceDone()
                            if (gotoNextSentence)
                                it.onNextSentenceRequested(mBinder)
                        }
                        synchronized(mLocker) {
                            mMediaPlayer?.let {
                                if (it.isPlaying)
                                    it.stop()
                                it.reset()
                                it.release()
                                mMediaPlayer = null
                            }
                        }
                    }
                }

                // API 23
                override fun onStop(utteranceId: String, interrupted: Boolean) {
                    mOnUtteranceStopOnce?.run()
                    mOnUtteranceStopOnce = null
                    synchronized(mLocker) {
                        mMediaPlayer?.let {
                            if (it.isPlaying)
                                it.stop()
                            it.reset()
                            it.release()
                            mMediaPlayer = null
                        }
                    }
                }

                // API 24
                override fun onAudioAvailable(utteranceId: String, audio: ByteArray) {
                    // nothing...
                }

                // API 24
                override fun onBeginSynthesis(
                    utteranceId: String,
                    sampleRateInHz: Int,
                    audioFormat: Int,
                    channelCount: Int
                ) {
                    // nothing...
                }
            })
        }
    }

    private fun requestAudioFocusWrapper(): Boolean {
        mAudioManager?.let { audioManager ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mAudioFocusRequest?.let { afr ->
                    val res = audioManager.requestAudioFocus(afr)
                    when (res) {
                        AudioManager.AUDIOFOCUS_REQUEST_GRANTED -> {
                            mPlaybackNowAuthorized = true
                            mPlaybackDelayed = false
                            mResumeOnFocusGain = false
                        }

                        AudioManager.AUDIOFOCUS_REQUEST_DELAYED -> {
                            // Now this is dead code since we call
                            //  builder.setAcceptsDelayedFocusGain(false);
                            mPlaybackNowAuthorized = false
                            mPlaybackDelayed = true
                            mResumeOnFocusGain = false
                        }

                        AudioManager.AUDIOFOCUS_REQUEST_FAILED -> {
                            mPlaybackNowAuthorized = false
                            mPlaybackDelayed = false
                            mResumeOnFocusGain = false
                        }

                        else -> {
                            mPlaybackNowAuthorized = false
                            mPlaybackDelayed = false
                            mResumeOnFocusGain = false
                        }
                    }
                }
            } else {
                val res = audioManager.requestAudioFocus(
                    mAudioFocusChangeListener,
                    AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN
                )
                mPlaybackNowAuthorized = (AudioManager.AUDIOFOCUS_REQUEST_GRANTED == res)
                mPlaybackDelayed = false
                mResumeOnFocusGain = false
            }
        } ?: {
            mPlaybackNowAuthorized = true
        }
        return mPlaybackNowAuthorized
    }

    private fun abandonAudioFocusRequestWrapper() {
        mAudioManager?.let { audioManager ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mAudioFocusRequest?.let { afr ->
                    audioManager.abandonAudioFocusRequest(afr)
                }
            } else {
                audioManager.abandonAudioFocus(mAudioFocusChangeListener)
            }
        }
    }

    private fun loadDictionary() {
        // Load dictionary from file
        var res = false
        try {
            val ttsDir = File(filesDir, "tts")
            val ttsDictFile = File(ttsDir, "dictionary.map")
            val inputStream = FileInputStream(ttsDictFile)
            val objectInputStream = ObjectInputStream(inputStream)
            val map = objectInputStream.readObject()
            if (map is HashMap<*, *>) {
                mReplaceDictionary.clear()
                mReplaceDictionary.putAll(map as Map<out String, String>)
            }
            objectInputStream.close()
            inputStream.close()
            res = true
        } catch (e: Exception) {
            log.error("Failed to load tts dictionary: $e")
        }
        // Output to log dictionary statistics
        if (res) {
            log.debug("Successfully loaded tts dictionary file: ${mReplaceDictionary.size} items found.")
        }
    }

    private fun saveDictionary(): Boolean {
        var res = false
        try {
            val ttsDir = File(filesDir, "tts")
            val ttsDictFile = File(ttsDir, "dictionary.map")
            val outputStream = FileOutputStream(ttsDictFile)
            val objectOutputStream = ObjectOutputStream(outputStream)
            objectOutputStream.writeObject(mReplaceDictionary)
            objectOutputStream.close()
            outputStream.close()
            res = true
        } catch (e: Exception) {
            log.error("Failed to save tts dictionary: $e")
        }
        if (res)
            log.info("Successfully saved tts dictionary")
        return res
    }

    private fun clearDictionary() {
        mReplaceDictionary.clear()
    }

    private val IntRange.length
        get() = last - first + 1

    private fun preprocessUtterance(utterance: String?): String {
        var newUtterance = utterance ?: return ""
        if (mUseDictionary) {
            val savedStr = newUtterance
            newUtterance = replaceFromDictionary(newUtterance, mReplaceDictionary)
            if (savedStr != newUtterance)
                log.debug("utterance changed: \"${savedStr}\" -> \"${newUtterance}\"")
        }
        if (mUseEndOfSentenceWorkaround) {
            // Add space before last char if it's dot.
            val len = newUtterance.length
            if (len > 1) {
                if (newUtterance[len - 1] == '.') {
                    newUtterance = newUtterance.substring(0, len - 1)
                    newUtterance += " ."
                }
            }
        }
        return newUtterance
    }

    private fun replaceFromDictionary(utterance: String, dict: HashMap<String, String>): String {
        // Replace algorithm with exclusion of previously replaced fragments, optimized
        // The library search function is used
        // First find all the fragments, then replace them from the last to the first
        val searchResults = ArrayList<Pair<Int, String>>()
        val allowedSearchRanges = ArrayList<IntRange>()
        allowedSearchRanges.add(IntRange(0, utterance.length - 1))
        var repeat: Boolean
        for ((key, _) in dict) {
            var ri = 0
            while (ri < allowedSearchRanges.size) {
                repeat = false
                val range = allowedSearchRanges[ri]
                if (key.length > 1 && '^' == key[0]) {
                    val key1 = key.substring(1)
                    if (0 == range.first) {
                        if ((range.length >= key1.length)) {
                            if (utterance.startsWith(key1)) {
                                searchResults.add(Pair(0, key))
                                allowedSearchRanges.removeAt(ri)
                                // Add to allowed ranges list spaces around replaced fragment
                                var spacesEnd = 0
                                while (spacesEnd < key1.length && key1[key1.length - spacesEnd - 1].isWhitespace())
                                    spacesEnd++
                                val r = IntRange(key1.length - spacesEnd, range.last)
                                if (r.length > 0)
                                    allowedSearchRanges.add(ri, r)
                            }
                        }
                    }
                } else {
                    if (range.length >= key.length) {
                        val idx = utterance.indexOfInRange(key, range)
                        if (idx >= 0) {
                            searchResults.add(Pair(idx, key))
                            allowedSearchRanges.removeAt(ri)
                            // Add to allowed ranges list spaces around replaced fragment
                            var spacesStart = 0
                            while (spacesStart < key.length && key[spacesStart].isWhitespace())
                                spacesStart++
                            var spacesEnd = 0
                            while (spacesEnd < key.length && key[key.length - spacesEnd - 1].isWhitespace())
                                spacesEnd++
                            var insIdx = ri
                            if (idx > range.first) {
                                allowedSearchRanges.add(
                                    insIdx,
                                    IntRange(range.first, idx - 1 + spacesStart)
                                )
                                insIdx++
                            } else {
                                repeat = true
                            }
                            val r = IntRange(idx + key.length - spacesEnd, range.last)
                            if (r.length > 0)
                                allowedSearchRanges.add(insIdx, r)
                        }
                    }
                }
                if (!repeat)
                    ri++
            }
        }
        searchResults.sortBy { it.first }
        val stringBuilder = StringBuilder()
        var i = searchResults.size - 1
        var head = utterance
        while (i >= 0) {
            val pair = searchResults[i]
            val idx = pair.first
            var key = pair.second
            // value can't be null here
            val value = dict[key]
            if (null != value) {
                if (key.isNotEmpty() && key.startsWith('^'))
                    key = key.substring(1)
                val tailLength = head.length - idx - key.length
                if (tailLength > 0) {
                    stringBuilder.prepend(head.takeLast(tailLength))
                } else {
                    // Remove overlapping spaces
                    stringBuilder.delete(0, 0 - tailLength)
                }
                stringBuilder.prepend(value)
            }
            head = head.take(idx)
            i--
        }
        if (head.isNotEmpty())
            stringBuilder.prepend(head)
        return stringBuilder.toString()
    }

    // throws IndexOutOfBoundsException
    private inline fun String.indexOfInRange(str: String, range: IntRange): Int {
        val idx = substring(range).indexOf(str)
        if (idx >= 0)
            return range.first + idx
        return -1
    }

    private inline fun StringBuilder.prepend(str: String): StringBuilder = this.insert(0, str)

    private fun importDictionary_impl(uri: Uri): HashMap<String, String>? {
        var inputStream: InputStream? = null
        var inputStreamReader: InputStreamReader? = null
        var reader: BufferedReader? = null
        try {
            inputStream = contentResolver.openInputStream(uri)
            inputStreamReader = InputStreamReader(inputStream)
            reader = BufferedReader(inputStreamReader)
            var line: String? = reader.readLine()
            val dict = HashMap<String, String>()
            while (!line.isNullOrEmpty()) {
                val pair = splitTTSDictLine(line)
                pair?.let {
                    if (it.first.isNotEmpty() && it.second.isNotEmpty()) {
                        if (!dict.contains(it.first)) {
                            dict[it.first] = it.second
                        } else {
                            log.debug("duplicate dictionary item found: $it")
                        }
                    }
                }
                line = reader.readLine()
            }
            return dict
        } catch (e: Exception) {
            log.error("Failed to import dictionary: $e")
        } finally {
            reader?.close()
            inputStreamReader?.close()
            inputStream?.close()
        }
        return null
    }

    private fun splitTTSDictLine(line: String): Pair<String, String>? {
        // Split string to pair like this:
        //  first second -> { "first", "second" }
        //  first "second" -> { "first", "second" }
        //  "first" second -> { "first", "second" }
        //  "first" "second" -> { "first", "second" }
        //  ^"first" "second" -> { "^first", "second" }
        var key = ""
        var value = ""
        var state = 0
        var haveErrors = false
        // 0 -> initial state
        // 1 -> adding to key
        // 2 -> delimiter between key and value
        // 3 -> adding to value
        // 4 -> end parsing
        var keyInQuotes = false
        var valueInQuotes = false
        for (i in line.indices) {
            val c = line[i]
            when (state) {
                0 -> {
                    if (0xFEFF.toChar() == c) {
                        // Just skip it
                    } else if ('^' == c && key.isEmpty()) {
                        key += c
                    } else if ('"' == c) {
                        state = 1
                        keyInQuotes = true
                    } else if (!c.isWhitespace()) {
                        state = 1
                        keyInQuotes = false
                        key += c
                    }
                }

                1 -> {
                    if (keyInQuotes) {
                        if (c != '"')
                            key += c
                        else
                            state = 2
                    } else {
                        if (!c.isWhitespace())
                            key += c
                        else
                            state = 2
                    }
                }

                2 -> {
                    if (c.isWhitespace()) {
                        // Just skip it
                    } else if ('"' == c) {
                        valueInQuotes = true
                        state = 3
                    } else {
                        valueInQuotes = false
                        value += c
                        state = 3
                    }
                }

                3 -> {
                    if (valueInQuotes) {
                        if (c != '"')
                            value += c
                        else
                            state = 4
                    } else {
                        if ('"' == c) {
                            haveErrors = true
                            state = 4
                        }
                        if (!c.isWhitespace())
                            value += c
                        else
                            state = 4
                    }
                }

                4 -> {
                    break
                }

                else -> {
                    break
                }
            }
            if (haveErrors)
                break
        }
        if (3 == state && !valueInQuotes)
            state = 4
        if (4 != state)
            haveErrors = true
        if (haveErrors) {
            log.error("Broken dictionary line: $line")
        } else {
            if (key.isNotEmpty() && value.isNotEmpty())
                return Pair(key, value)
        }
        return null
    }

    private fun startVolumeFadingAndPause() {
        // Already in synchronized(mLocker)
        if (null == mVolumeFadingHandler) {
            Looper.myLooper()?.let {
                mVolumeFadingHandler = Handler(it)
            }
        }
        mFadingVolumeIsDone = false
        mFadingVolumeOrig = getVolume_impl()
        mFadingVolumeCurrent = mFadingVolumeOrig
        mFadingVolumeThreshold = (getMaxVolume_impl() / 20).coerceAtLeast(1)
        mVolumeFadingHandler?.postDelayed(
            mVolumeFadingHandlerRunnable,
            FADING_VOLUME_PERIOD.toLong()
        )
    }

    private fun isVolumeFadingStarted(): Boolean {
        return null != mVolumeFadingHandler
    }

    private fun cancelVolumeFading() {
        // Already in synchronized(mLocker)
        mVolumeFadingHandler?.let {
            log.debug("Cancel volume fading...")
            it.removeCallbacks(mVolumeFadingHandlerRunnable)
            // Restore volume
            setVolume(mFadingVolumeOrig)
            mVolumeFadingHandler = null
        }
    }

    companion object {
        private val log = SRLog.create("ttssvc")

        private const val APP_UTTERANCE_ID = "lxUtteranceId_740"
        private const val MAX_CONTINUOUS_ERRORS = 3
        private const val INIT_TTS_TIMEOUT = 10000L // 10 sec.
        private const val NOTIFICATION_ID = 1
        private const val NOTIFICATION_CHANNEL_ID = "LxReader TTS C1"

        const val TTS_CONTROL_ACTION_PREPARE = "io.gitlab.coolreader_ng.project_s.tts.prepare"
        const val TTS_CONTROL_ACTION_PLAY_PAUSE =
            "io.gitlab.coolreader_ng.project_s.tts.tts_play_pause"
        const val TTS_CONTROL_ACTION_SKIP_NEXT =
            "io.gitlab.coolreader_ng.project_s.tts.tts_skip_next"
        const val TTS_CONTROL_ACTION_SKIP_PREV =
            "io.gitlab.coolreader_ng.project_s.tts.tts_skip_prev"
        const val TTS_CONTROL_ACTION_STOP = "io.gitlab.coolreader_ng.project_s.tts.tts_stop"

        private const val MEDIA_COVER_WIDTH: Int = 300
        private const val MEDIA_COVER_HEIGHT: Int = 400

        // accelerometer immobility delta threshold
        private const val ACCELEROMETER_IMMOBILITY_THRESHOLD: Float = 0.5f

        private const val FADING_VOLUME_PERIOD = 1000   // 1s
        private const val FADING_VOLUME_TIME = 10000    // 10s
        private const val FADING_VOLUME_STEPS = FADING_VOLUME_TIME / FADING_VOLUME_PERIOD
    }
}
