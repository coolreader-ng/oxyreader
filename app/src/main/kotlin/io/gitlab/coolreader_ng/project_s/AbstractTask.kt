/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

abstract class AbstractTask(private val name: String) : Runnable {

    override fun toString(): String {
        return "Task[$name]"
    }

    override fun run() {
        val ts = Utils.uptime()
        log.verbose("$this started")
        try {
            work()
        } catch (e: Exception) {
            log.error("$this: exception while running task in background", e)
        }
        log.verbose("$this finished in " + Utils.uptimeElapsed(ts) + " ms")
    }

    abstract fun work()

    companion object {
        private val log = SRLog.create("abs_task")
    }
}