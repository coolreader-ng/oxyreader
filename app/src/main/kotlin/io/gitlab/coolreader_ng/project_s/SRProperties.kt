/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Bundle
import java.util.Properties

class SRProperties : Properties {
    constructor() : super()

    constructor(props: Properties) {
        synchronized(props) {
            setAll(props)
        }
    }

    constructor(bundle: Bundle) {
        synchronized(bundle) {
            setAll(bundle)
        }
    }

    @Synchronized
    fun setAll(props: Properties) {
        for ((key, value) in props) {
            setProperty(key as String, value.toString())
        }
    }

    @Synchronized
    fun setAll(bundle: Bundle) {
        for (key in bundle.keySet()) {
            setProperty(key, bundle.getString(key) ?: "")
        }
    }

    fun setColor(key: String, color: Int) {
        var value = Integer.toHexString(revBytes(color and 0xFFFFFF))
        while (value.length < 6)
            value = "0$value"
        value = "0x$value"
        setProperty(key, value)
    }

    fun getColor(key: String, defColor: Int): Int {
        val defc = revBytes(defColor)
        val value = getProperty(key)
        try {
            if (null != value) {
                if (value.length > 2 && value.startsWith("0x")) {
                    var cl = value.substring(2).toInt(16)
                    cl = revBytes(cl)
                    return cl or -0x1000000     // means 0xFF000000
                }
                if (value.length > 1 && value.startsWith("#")) {
                    var cl = value.substring(1).toInt(16)
                    cl = revBytes(cl)
                    return cl or -0x1000000     // means 0xFF000000
                }
            }
        } catch (_: Exception) {
        }
        return revBytes(defc) or -0x1000000
    }

    fun setInt(key: String, v: Int) {
        setProperty(key, v.toString())
    }

    fun getInt(key: String, def: Int): Int {
        val value = getProperty(key)
        var res = def
        try {
            if (null != value)
                res = Integer.valueOf(value)
        } catch (_: Exception) {
        }
        return res
    }

    fun setLong(key: String, v: Long) {
        setProperty(key, v.toString())
    }

    fun getLong(key: String, def: Long): Long {
        val value = getProperty(key)
        var res = def
        try {
            if (null != value)
                res = value.toLong()
        } catch (_: Exception) {
        }
        return res
    }

    fun setFloat(key: String, v: Float) {
        setProperty(key, v.toString())
    }

    fun getFloat(key: String, def: Float): Float {
        val value = getProperty(key)
        var res = def
        try {
            if (null != value)
                res = value.toFloat()
        } catch (_: Exception) {
        }
        return res
    }

    fun setBool(key: String, value: Boolean) {
        setProperty(key, if (value) "1" else "0")
    }

    fun getBool(key: String, defaultValue: Boolean): Boolean {
        val value = getProperty(key) ?: return defaultValue
        if (value == "1" || value == "true" || value == "yes")
            return true
        if (value == "0" || value == "false" || value == "no")
            return false
        return defaultValue
    }

    fun applyDefault(prop: String, defValue: String) {
        if (getProperty(prop) == null)
            setProperty(prop, defValue)
    }

    fun applyDefault(prop: String, defValue: Int) {
        if (getProperty(prop) == null)
            setInt(prop, defValue)
    }

    fun applyDefault(prop: String, defValue: Boolean) {
        if (getProperty(prop) == null)
            setBool(prop, defValue)
    }

    @Synchronized
    fun diff(oldValue: SRProperties): SRProperties {
        val res = SRProperties()
        for ((key, value) in entries) {
            if (!oldValue.containsKey(key) || !eq(value, oldValue[key])) {
                res.setProperty(key as String, value as String)
            }
        }
        return res
    }

    @Synchronized
    override fun getProperty(name: String, defaultValue: String): String {
        return super.getProperty(name, defaultValue)
    }

    @Synchronized
    override fun getProperty(name: String): String? {
        return super.getProperty(name)
    }

    @Synchronized
    fun haveProperty(name: String): Boolean {
        if (null == getProperty(name))
            return false
        return true
    }

    @Synchronized
    override fun setProperty(name: String, value: String): Any? {
        var res: Any? = null
        try {
            res = super.setProperty(name, value)
        } catch (e: NullPointerException) {
            log.error("PropertiesWrapper.setProperty(): value can't be null, name=$name")
        }
        return res
    }

    @Synchronized
    override fun remove(key: Any): Any? {
        return super.remove(key)
    }

    fun toBundle(): Bundle {
        val bundle = Bundle()
        for ((key, value) in entries) {
            /*
            if (value is Boolean) {
                bundle.putBoolean(skey, value)
            } else if (value is Int) {
                bundle.putInt(skey, value)
            } else if (value is Long) {
                bundle.putLong(skey, value)
            } else if (value is Float) {
                bundle.putFloat(skey, value)
            } else if (value is Double) {
                bundle.putDouble(skey, value)
            } else if (value is String) {
                bundle.putString(skey, value)
            } else {
                throw UnsupportedOperationException()
            }
             */
            bundle.putString(key as String, value.toString())
        }
        return bundle
    }

    override val entries: MutableSet<MutableMap.MutableEntry<Any, Any>>
        @Synchronized
        get() {
            return super.entries
        }

    companion object {
        private val log = SRLog.create("props")

        private fun revBytes(color: Int): Int {
            return color and 0xFFFFFF
            // return ((color & 0xFF)<<16)|((color & 0xFF00)<<0)|((color &
            // 0xFF0000)>>16);
        }

        fun eq(obj1: Any?, obj2: Any?): Boolean {
            if (null == obj1 && null == obj2) return true
            return if (null == obj1 || null == obj2) false else obj1 == obj2
        }
    }
}