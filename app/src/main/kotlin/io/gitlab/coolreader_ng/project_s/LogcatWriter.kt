/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.os.Build
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Arrays
import java.util.Date
import java.util.Locale

class LogcatWriter {

    class PipeWriterThread constructor(
        daemon: Boolean,
        since: Date,
        bufferSize: Int,
        outputStream: OutputStream
    ) : Thread() {

        private val mmDaemon = daemon
        private val mmSince = since
        private val mmBufferSize = bufferSize
        private val mmOutputStream = outputStream
        private var mmStopRequested = false
        private val mmLocker = Any()

        var exitCode: Int = EXIT_CODE_NOT_STARTED
            private set

        fun requestToStop() {
            synchronized(mmLocker) {
                mmStopRequested = true
            }
        }

        private fun logcatCmdFabric(
            daemonMode: Boolean = true,
            since: Date? = null,
            datePattern: String? = null
        ): List<String> {
            val list = ArrayList<String>()
            list.add("logcat")
            if (null != since && null != datePattern && datePattern.isNotEmpty()) {
                val dateFormat = SimpleDateFormat(datePattern, Locale.US)
                val dateString = dateFormat.format(since)
                list.add(if (daemonMode) "-T" else "-t")
                list.add(dateString)
            }
            if (!daemonMode)
                list.add("-d")
            return list
        }

        private fun isProcessAlive(process: Process): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                return process.isAlive
            } else {
                try {
                    process.exitValue()
                } catch (e: IllegalThreadStateException) {
                    return true
                }
                return false
            }
        }

        override fun run() {
            var exitCode: Int = EXIT_CODE_STILL_RUN
            this.exitCode = exitCode
            var process: Process?
            // 1. start process
            var cmdArgs = logcatCmdFabric(mmDaemon, mmSince, "yyyy-MM-dd HH:mm:ss.SSS")
            try {
                process = ProcessBuilder().command(cmdArgs).redirectErrorStream(true).start()
                try {
                    if (null != process) {
                        sleep(100)
                        exitCode = process.exitValue()
                        if (0 != exitCode) {
                            // the process terminated prematurely with bad exit code
                            process = null
                            log.debug("logcat date format \"yyyy-MM-dd HH:mm:ss.SSS\" is rejected")
                        }
                    }
                } catch (e: IllegalThreadStateException) {
                    // OK, process is still running
                }
            } catch (e: Exception) {
                process = null
                exitCode = EXIT_CODE_CANT_START
            }
            if (null == process) {
                // Possible invalid date format
                cmdArgs = logcatCmdFabric(mmDaemon, mmSince, "MM-dd HH:mm:ss.SSS")
                try {
                    process = ProcessBuilder().command(cmdArgs).redirectErrorStream(true).start()
                    try {
                        if (null != process) {
                            sleep(100)
                            exitCode = process.exitValue()
                            if (0 != exitCode) {
                                // the process terminated prematurely with bad exit code
                                process = null
                                log.debug("logcat date format \"MM-dd HH:mm:ss.SSS\" is rejected")
                            }
                        }
                    } catch (e: IllegalThreadStateException) {
                        // OK, process is still running
                    }
                } catch (e: Exception) {
                    process = null
                    exitCode = EXIT_CODE_CANT_START
                }
            }
            if (null == process) {
                // Possible invalid date format
                // trying without "-t" argument
                cmdArgs = logcatCmdFabric(mmDaemon, null, null)
                try {
                    process = ProcessBuilder().command(cmdArgs).redirectErrorStream(true).start()
                    try {
                        if (null != process) {
                            sleep(100)
                            exitCode = process.exitValue()
                            if (0 != exitCode) {
                                // the process terminated prematurely with bad exit code
                                process = null
                                log.error("logcat exited with code $exitCode")
                            }
                        }
                    } catch (e: IllegalThreadStateException) {
                        // OK, process is still running
                    }
                } catch (e: Exception) {
                    process = null
                    exitCode = EXIT_CODE_CANT_START
                    log.error("Failed to run logcat!", e)
                }
            }
            log.verbose("logcat started with command $cmdArgs")
            val inputStream: InputStream?
            if (null != process) {
                // 2. Setup I/O streams
                //    close the process input stream since we don't use that
                try {
                    process.outputStream?.close()
                } catch (ignored: IOException) {
                }
                //    close the process's error stream as we merge it with the standard output stream
                try {
                    process.errorStream?.close()
                } catch (ignored: IOException) {
                }
                //    get process standard output stream as our input stream
                inputStream = process.inputStream
                if (null != inputStream) {
                    // 3. start reading from pipe and send to output stream
                    var stopRequested = false
                    var rb: Int
                    val buffer = ByteArray(mmBufferSize)
                    while (!stopRequested) {
                        try {
                            rb = inputStream.read(buffer)
                            if (rb > 0) {
                                mmOutputStream.write(buffer, 0, rb);
                            } else {
                                if (-1 == rb) {
                                    log.debug("process pipe EOF")
                                }
                                /*
                                // check if process terminated
                                if (!isProcessAlive(process)) {
                                    log.verbose("process terminated")
                                    break
                                } else {
                                    sleep(100)
                                }
                                 */
                            }
                        } catch (e: Exception) {
                            log.error("I/O error", e)
                        }
                        // 4. logrotate by size
                        // TODO: implement logrotate
                        synchronized(mmLocker) {
                            stopRequested = mmStopRequested
                        }
                    }
                    try {
                        inputStream.close()
                    } catch (e: Exception) {
                    }
                } else {
                    log.error("Can't get process standard output stream")
                }
                // 5. Save process exit code
                var isStopped: Boolean
                try {
                    exitCode = process.exitValue()
                    isStopped = true
                } catch (e: Exception) {
                    isStopped = false
                }
                if (!isStopped) {
                    process.destroy()
                    exitCode = EXIT_CODE_TERMINATED
                }
            } else {
                log.error("Skip logcat output redirecting since logcat is not running.")
            }
            try {
                mmOutputStream.close()
            } catch (e: Exception) {
            }
            this.exitCode = exitCode
            log.verbose("PipeWriterThread finished")
        }
    }

    private var mWriteThread: PipeWriterThread? = null

    fun start(since: Date, outputFileName: String): Boolean {
        return start(since, File(outputFileName))
    }

    fun start(since: Date, outputFile: File): Boolean {
        if (null != mWriteThread && mWriteThread?.isAlive == false)
            mWriteThread = null
        if (mWriteThread?.isAlive == true) {
            log.error("Another logcat writer already running.")
            return false
        }
        try {
            val parent = outputFile.parentFile
            if (parent != null) {
                if (!parent.exists() && !parent.mkdirs())
                    throw IOException("Can't create parent directory!")
            }
            val outputStream = FileOutputStream(outputFile, true)
            mWriteThread = PipeWriterThread(true, since, 4096, outputStream)
            mWriteThread!!.start()
            return true
        } catch (e: Exception) {
            log.error("Failed to open file stream", e)
        }
        return false
    }

    fun stop(): Boolean {
        if (null != mWriteThread) {
            try {
                mWriteThread?.requestToStop()
                mWriteThread?.join()
                mWriteThread = null
                return true
            } catch (e: Exception) {
            }
        }
        return false
    }

    val started: Boolean
        get() {
            return mWriteThread?.isAlive ?: false
        }

    private class NaturalSortStringComparator : Comparator<String> {
        override fun compare(str1: String?, str2: String?): Int {
            if (null == str1)
                return if (null != str2) -1 else 0
            else if (null == str2)
                return 1
            var idx1 = 0
            var idx2 = 0
            while (idx1 < str1.length && idx2 < str2.length) {
                val char1 = str1[idx1]
                val char2 = str2[idx2]
                if (!char1.isDigit() || !char2.isDigit()) {
                    if (char1 != char2)
                        return char1 - char2
                    idx1++
                    idx2++
                } else {
                    var substr = ""
                    while (idx1 < str1.length && str1[idx1].isDigit()) {
                        substr += str1[idx1]
                        idx1++
                    }
                    val number1 = substr.toLong()
                    substr = ""
                    while (idx2 < str2.length && str2[idx2].isDigit()) {
                        substr += str2[idx2]
                        idx2++
                    }
                    val number2 = substr.toLong()
                    if (number1 != number2)
                        return (number1 - number2).toInt()
                }
            }
            return 0
        }
    }

    private class NaturalSortFileComparator : Comparator<File> {
        override fun compare(file1: File?, file2: File?): Int {
            if (null == file1)
                return if (null != file2) -1 else 0
            else if (null == file2)
                return 1
            return NaturalSortStringComparator().compare(file1.name, file2.name)
        }
    }

    companion object {
        private val log = SRLog.create("lcw")
        const val EXIT_CODE_NOT_STARTED: Int = -10000
        const val EXIT_CODE_STILL_RUN: Int = -10001
        const val EXIT_CODE_TERMINATED: Int = -10002
        const val EXIT_CODE_CANT_START: Int = -10003

        fun writeOnce(since: Date, outputFile: File): Boolean {
            try {
                return writeOnce(since, FileOutputStream(outputFile))
            } catch (e: Exception) {
                log.error("Failed to save logcat: $e")
            }
            return false
        }

        fun writeOnce(since: Date, outputStream: OutputStream): Boolean {
            try {
                val pipeWriterThread = PipeWriterThread(false, since, 4096, outputStream)
                pipeWriterThread.start()
                pipeWriterThread.join()
                return true
            } catch (e: Exception) {
                log.error("Failed to save logcat: $e")
            }
            return false
        }

        fun rotateLogFiles(ctx: Context, logsDir: File, baseName: String, maxCount: Int): Boolean {
            if (!logsDir.exists())
                return true
            var res = true
            val items = logsDir.listFiles()
            if (null != items && items.isNotEmpty()) {
                Arrays.sort(items, NaturalSortFileComparator())
                for (i in items.size - 1 downTo 0) {
                    val item = items[i]
                    val str = item.name
                    var idx: Int? = null
                    if (str.startsWith(baseName)) {
                        // drop extension
                        val dotPos = str.lastIndexOf('.')
                        val tail = str.substring(baseName.length, dotPos)
                        idx = tail.toIntOrNull()
                    }
                    if (null != idx && idx < maxCount - 1) {
                        idx++
                        val newFileName = File(item.parent, "$baseName$idx.log")
                        res = item.renameTo(newFileName)
                    } else {
                        // invalid file prefix, invalid number or max count exceeded
                        res = item.delete()
                    }
                    if (!res)
                        break
                }
            }
            return res
        }
    }
}