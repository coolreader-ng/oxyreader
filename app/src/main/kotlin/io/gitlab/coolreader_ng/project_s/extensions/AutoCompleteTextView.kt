/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.extensions

import android.os.Build
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView

fun AutoCompleteTextView.getItemPosition(value: String): Int {
    var idx = -1
    val adapter = this.adapter
    if (adapter is ArrayAdapter<*>) {
        try {
            val arrayAdapter = adapter as ArrayAdapter<String>
            idx = arrayAdapter.getPosition(value)
        } catch (_: Exception) {
        }
    } else {
        for (i in 0 until adapter.count) {
            val item = adapter.getItem(i).toString()
            if (item == value) {
                idx = i
                break
            }
        }
    }
    return idx
}

fun AutoCompleteTextView.getCurrentSelectedPosition(): Int {
    return getItemPosition(text.toString())
}

fun AutoCompleteTextView.setCurrentSelectedItem(idx: Int) {
    val adapter = this.adapter
    val text = if (idx >= 0 && idx < adapter.count) adapter.getItem(idx).toString() else ""
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        setText(text, false)
    } else {
        setText(text)
        try {
            val arrayAdapter = adapter as ArrayAdapter<*>
            arrayAdapter.filter.filter(null)
        } catch (_: Exception) {
        }
    }
    // No effect, but we set this
    listSelection = idx
}

fun AutoCompleteTextView.setCurrentSelectedItem(value: String) {
    val adapter = this.adapter
    val idx = getItemPosition(value)
    if (idx >= 0)
        setCurrentSelectedItem(idx)
    else {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setText(text, false)
        } else {
            setText(text)
            try {
                val arrayAdapter = adapter as ArrayAdapter<*>
                arrayAdapter.filter.filter(null)
            } catch (_: Exception) {
            }
        }
    }
}
