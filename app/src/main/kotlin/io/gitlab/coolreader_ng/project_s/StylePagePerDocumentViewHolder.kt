/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.view.View
import com.google.android.material.materialswitch.MaterialSwitch

class StylePagePerDocumentViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private val mEmbeddedStylesSwitch: MaterialSwitch
    private val mEmbeddedFontsSwitch: MaterialSwitch
    private val mLegacyRenderingSwitch: MaterialSwitch

    override fun onUpdateViewImpl() {
        mEmbeddedStylesSwitch.isChecked =
            mProps.getBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES, true)
        mEmbeddedFontsSwitch.isChecked =
            mProps.getBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS, true)
        mEmbeddedFontsSwitch.isEnabled = mEmbeddedStylesSwitch.isChecked
        val legacyRendering = mProps.getInt(
            PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
            CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
        ) == 0 ||
                mProps.getInt(
                    PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION,
                    CREngineNGBinding.DOM_VERSION_CURRENT
                ) < 20180524
        mLegacyRenderingSwitch.isChecked = legacyRendering
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    override fun onResetViewImpl() {
    }

    init {
        mEmbeddedStylesSwitch = itemView.findViewById(R.id.embeddedStylesSwitch)
        mEmbeddedFontsSwitch = itemView.findViewById(R.id.embeddedFontsSwitch)
        mLegacyRenderingSwitch = itemView.findViewById(R.id.legacyRenderingSwitch)
        mEmbeddedStylesSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES, isChecked)
            commitChanges()
        }
        mEmbeddedFontsSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS, isChecked)
            commitChanges()
        }
        mLegacyRenderingSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                mProps.setInt(
                    PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION,
                    CREngineNGBinding.DOM_VERSION_LEGACY
                )
                mProps.setInt(
                    PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
                    CREngineNGBinding.BLOCK_RENDERING_FLAGS_LEGACY
                )
            } else {
                mProps.setInt(
                    PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION,
                    CREngineNGBinding.DOM_VERSION_CURRENT
                )
                mProps.setInt(
                    PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
                    CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
                )
            }
            commitChanges()
        }
    }
}