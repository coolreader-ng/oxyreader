/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import io.gitlab.coolreader_ng.project_s.extensions.getCurrentSelectedPosition
import io.gitlab.coolreader_ng.project_s.extensions.setCurrentSelectedItem
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import io.gitlab.coolreader_ng.project_s.extensions.setSimpleItems
import java.text.NumberFormat
import kotlin.math.roundToInt

class StylePageText2ViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private val mFontGammaEdit: MaterialAutoCompleteTextView?
    private val mFontGammaSpinner: Spinner?
    private val mFontHintingEdit: MaterialAutoCompleteTextView?
    private val mFontHintingSpinner: Spinner?
    private val mTextShapingEdit: MaterialAutoCompleteTextView?
    private val mTextShapingSpinner: Spinner?
    private val mMinSpaceWidthSlider: Slider
    private val mMinSpaceValueView: TextView
    private val mBtnMinSpaceWidthDec: Button
    private val mBtnMinSpaceWidthInc: Button

    override fun onUpdateViewImpl() {
        // Font gamma
        val fontGamma = mProps.getFloat(PropNames.Engine.PROP_FONT_GAMMA, 1.0f)
        var fontGammaIdx = Utils.findNearestIndex(GAMMA_VALUES, fontGamma)
        if (fontGammaIdx < 0 || fontGammaIdx >= GAMMA_VALUES.size)
            fontGammaIdx = GAMMA_VALUE_1_0_INDEX
        // On layout for API26+
        mFontGammaEdit?.setCurrentSelectedItem(fontGammaIdx)
        // On layout for API less than 26
        mFontGammaSpinner?.setSelection(fontGammaIdx)

        // Font hinting
        val fontHintingIdx = mProps.getInt(PropNames.Engine.PROP_FONT_HINTING, 0)
        // On layout for API26+
        mFontHintingEdit?.setCurrentSelectedItem(fontHintingIdx)
        // On layout for API less than 26
        mFontHintingSpinner?.setSelection(fontHintingIdx)

        // Text shaping
        val textShapingIdx = when (mProps.getInt(PropNames.Engine.PROP_FONT_SHAPING, 0)) {
            0, 1 -> 0   // Simple, HarfBuzz light
            2 -> 1      // HarfBuzz full
            else -> 0
        }
        // On layout for API26+
        mTextShapingEdit?.setCurrentSelectedItem(textShapingIdx)
        // On layout for API less than 26
        mTextShapingSpinner?.setSelection(textShapingIdx)

        // Minimum space width
        val minSpaceWidth =
            mProps.getInt(PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT, 75)
        mMinSpaceWidthSlider.value = minSpaceWidth.toFloat()
        mMinSpaceValueView.text = context.getString(R.string.format_number_percent, minSpaceWidth)
        mBtnMinSpaceWidthDec.isEnabled = minSpaceWidth > MINIMUM_SPACE_WITH_MIN
        mBtnMinSpaceWidthInc.isEnabled = minSpaceWidth < MINIMUM_SPACE_WITH_MAX
    }

    override fun onResetViewImpl() {
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // On layout for API26+
            mFontGammaEdit = itemView.findViewById(R.id.fontGammaEdit)
            mFontHintingEdit = itemView.findViewById(R.id.fontHintingEdit)
            mTextShapingEdit = itemView.findViewById(R.id.textShapingEdit)
            mFontGammaSpinner = null
            mFontHintingSpinner = null
            mTextShapingSpinner = null
        } else {
            // On layout for API less than 26
            mFontGammaEdit = null
            mFontHintingEdit = null
            mTextShapingEdit = null
            mFontGammaSpinner = itemView.findViewById(R.id.fontGammaSpinner)
            mFontHintingSpinner = itemView.findViewById(R.id.fontHintingSpinner)
            mTextShapingSpinner = itemView.findViewById(R.id.textShapingSpinner)
        }
        mMinSpaceWidthSlider = itemView.findViewById(R.id.minSpaceWidthSlider)
        mMinSpaceValueView = itemView.findViewById(R.id.minSpaceValueView)
        mBtnMinSpaceWidthDec = itemView.findViewById(R.id.btnMinSpaceWidthDec)
        mBtnMinSpaceWidthInc = itemView.findViewById(R.id.btnMinSpaceWidthInc)

        // Font gamma
        val numberFormat = NumberFormat.getNumberInstance(SettingsManager.activeLang.locale)
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        val gammaLists = ArrayList<String>(GAMMA_VALUES.size)
        for (gamma in GAMMA_VALUES) {
            val label = numberFormat.format(gamma)
            gammaLists.add(label)
        }
        val gammaArray = gammaLists.toTypedArray()
        // On layout for API26+
        mFontGammaEdit?.setSimpleItems(gammaArray)
        mFontGammaEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevGamma = mProps.getFloat(PropNames.Engine.PROP_FONT_GAMMA, 1.0f)
                val selectedGammaIdx = mFontGammaEdit.getCurrentSelectedPosition()
                val selectedGamma =
                    if (selectedGammaIdx >= 0 && selectedGammaIdx < GAMMA_VALUES.size)
                        GAMMA_VALUES[selectedGammaIdx] else 1.0f
                if (prevGamma != selectedGamma) {
                    mProps.setFloat(PropNames.Engine.PROP_FONT_GAMMA, selectedGamma)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mFontGammaSpinner?.setSimpleItems(gammaArray)
        mFontGammaSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevGamma = mProps.getFloat(PropNames.Engine.PROP_FONT_GAMMA, 1.0f)
                val selectedGamma = if (position >= 0 && position < GAMMA_VALUES.size)
                    GAMMA_VALUES[position] else 1.0f
                if (prevGamma != selectedGamma) {
                    mProps.setFloat(PropNames.Engine.PROP_FONT_GAMMA, selectedGamma)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Font hinting
        // On layout for API26+
        mFontHintingEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevFontHintingCode = mProps.getInt(PropNames.Engine.PROP_FONT_HINTING, 0)
                // Font hinting engine's codes equals with R.array.font_hinting array
                val fontHintingCode = mFontHintingEdit!!.getCurrentSelectedPosition()
                if (prevFontHintingCode != fontHintingCode) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_HINTING, fontHintingCode)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mFontHintingSpinner?.setSimpleItems(R.array.font_hinting)
        mFontHintingSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevFontHintingCode = mProps.getInt(PropNames.Engine.PROP_FONT_HINTING, 0)
                // Font hinting engine's codes equals with R.array.font_hinting array
                if (prevFontHintingCode != position) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_HINTING, position)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Text shaping
        // On layout for API26+
        mTextShapingEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevTextShapingCode = mProps.getInt(PropNames.Engine.PROP_FONT_SHAPING, 0)
                val textShapingCode = when (mTextShapingEdit.getCurrentSelectedPosition()) {
                    0 -> 1  // HarfBuzz light
                    1 -> 2  // HarfBuzz full
                    else -> 2
                }
                if (prevTextShapingCode != textShapingCode) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_SHAPING, textShapingCode)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mTextShapingSpinner?.setSimpleItems(R.array.text_shaping)
        mTextShapingSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevTextShapingCode = mProps.getInt(PropNames.Engine.PROP_FONT_SHAPING, 0)
                val textShapingCode = when (position) {
                    0 -> 1  // HarfBuzz light
                    1 -> 2  // HarfBuzz full
                    else -> 2
                }
                if (prevTextShapingCode != textShapingCode) {
                    mProps.setInt(PropNames.Engine.PROP_FONT_SHAPING, textShapingCode)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Minimum space width
        val minSpaceLabel = itemView.findViewById<TextView>(R.id.minSpaceLabel)
        minSpaceLabel.setHyphenatedText(
            SettingsManager.activeLang.langTag,
            minSpaceLabel.text.toString()
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed (when slider is using on PopupWindow)
            // TODO: Test on other API, new material design library (possible fixed)
            mMinSpaceWidthSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mMinSpaceWidthSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_percent, value.roundToInt())
            }
        }
        mMinSpaceWidthSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val prevMinSpaceWidth =
                    mProps.getInt(PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT, 75)
                val newMinSpaceWidth = slider.value.roundToInt()
                if (prevMinSpaceWidth != newMinSpaceWidth) {
                    mProps.setInt(
                        PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT,
                        newMinSpaceWidth
                    )
                    commitChanges()
                }
            }
        })
        mBtnMinSpaceWidthDec.setOnClickListener {
            val prevMinSpaceWidth =
                mProps.getInt(PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT, 75)
            val newMinSpaceWidth = prevMinSpaceWidth - 1
            if (newMinSpaceWidth >= MINIMUM_SPACE_WITH_MIN) {
                mProps.setInt(
                    PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT,
                    newMinSpaceWidth
                )
                commitChanges()
            }
        }
        mBtnMinSpaceWidthInc.setOnClickListener {
            val prevMinSpaceWidth =
                mProps.getInt(PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT, 75)
            val newMinSpaceWidth = prevMinSpaceWidth + 1
            if (newMinSpaceWidth <= MINIMUM_SPACE_WITH_MAX) {
                mProps.setInt(
                    PropNames.Engine.PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT,
                    newMinSpaceWidth
                )
                commitChanges()
            }
        }
    }

    companion object {
        // Exactly as in crengine-ng
        private val GAMMA_VALUES = floatArrayOf(
            0.30f, 0.35f, 0.40f, 0.45f, 0.50f, 0.55f, 0.60f, 0.65f,
            0.70f, 0.75f, 0.80f, 0.85f, 0.90f, 0.95f, 0.98f, 1.00f,
            1.02f, 1.05f, 1.10f, 1.15f, 1.20f, 1.25f, 1.30f, 1.35f,
            1.40f, 1.45f, 1.50f, 1.60f, 1.70f, 1.80f, 1.90f, 2.00f,
            2.10f, 2.20f, 2.30f, 2.40f, 2.50f, 2.60f, 2.70f, 2.80f,
            2.90f, 3.00f, 3.10f, 3.20f, 3.40f, 3.60f, 3.80f, 4.00f
        )
        private const val GAMMA_VALUE_1_0_INDEX = 15
        private const val MINIMUM_SPACE_WITH_MIN = 25
        private const val MINIMUM_SPACE_WITH_MAX = 100
    }
}