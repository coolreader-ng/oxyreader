/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.app.Service
import android.os.Handler

abstract class AbstractService(name: String) : Service() {
    private var mThread = ServiceThread(name)

    val name: String
        get() = mThread.name

    override fun onCreate() {
        mThread.start()
    }

    override fun onDestroy() {
        mThread.stop(5000)
    }

    /**
     * Post a task (runnable) to run in a background service thread.
     * Exceptions will be ignored, just dumped into log.
     * @param task is task to execute
     */
    protected fun postTask(task: AbstractTask) {
        log.verbose("Posting task $task")
        mThread.post(task)
    }

    /**
     * Post a delayed task (runnable) to run in a background service thread.
     * Exceptions will be ignored, just dumped into log.
     * @param task is task to execute
     * @param delay delay in milliseconds
     */
    protected fun postTask(task: AbstractTask, delay: Long) {
        log.verbose("Posting task $task with delay $delay")
        mThread.postDelayed(task, delay)
    }

    /**
     * Post runnable to run in a background service thread.
     * @param runnable is runnable to execute
     */
    protected fun postRunnable(runnable: Runnable) {
        log.verbose("Posting runnable")
        mThread.post(runnable)
    }

    /**
     * Post delayed runnable to run in a background service thread.
     * Exceptions will be ignored, just dumped into log.
     * @param runnable is task to execute
     * @param delay delay in milliseconds
     */
    protected fun postRunnable(runnable: Runnable, delay: Long) {
        log.verbose("Posting runnable with delay $delay")
        mThread.postDelayed(runnable, delay)
    }

    /**
     * Send task to handler, if specified, otherwise run immediately.
     * Exceptions will be ignored, just dumped into log.
     * @param handler is handler to send task to, null to run immediately
     * @param task is Runnable to execute
     */
    protected fun sendTask(handler: Handler?, task: Runnable) {
        try {
            if (handler != null) {
                log.verbose("Sending task to $handler")
                handler.post(task)
            } else {
                log.verbose("No Handler provided: executing task in current thread")
                task.run()
            }
        } catch (e: Exception) {
            log.error("Exception in task", e)
        }
    }

    companion object {
        private val log = SRLog.create("bssvc")
    }
}