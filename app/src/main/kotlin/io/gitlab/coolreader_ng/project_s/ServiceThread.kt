/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.Message
import java.util.LinkedList

/**
 * Thread to run background tasks inside.
 */
class ServiceThread(name: String) : Thread(name) {

    class ServiceThreadHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message) {
            log.debug("message: $msg")
        }
    }

    private val mQueue = LinkedList<Runnable>()
    private var mHandler: Handler? = null
    private val mLocker = Any()

    /**
     * Post task for execution.
     * @param task is runnable to call
     */
    fun post(task: Runnable) {
        synchronized(mLocker) {
            if (mHandler == null) {
                log.warn("Thread is not yet started, just adding to queue $task")
                mQueue.addLast(task)
            } else {
                postQueuedTasks()
                mHandler!!.post(task)
            }
        }
    }

    /**
     * Post task for execution at front of queue.
     * @param task is runnable to call
     */
    fun postAtFrontOfQueue(task: Runnable) {
        synchronized(mLocker) {
            mHandler?.let {
                postQueuedTasks()
                it.postAtFrontOfQueue(task)
            } ?: run {
                mQueue.addLast(task)
            }
        }
    }

    /**
     * Post task for execution with delay.
     * @param task is runnable to call
     */
    fun postDelayed(task: Runnable, delayMillis: Long) {
        synchronized(mLocker) {
            if (mHandler == null)
                mQueue.addLast(task)
            else {
                postQueuedTasks()
                mHandler!!.postDelayed(task, delayMillis)
            }
        }
    }

    private fun postQueuedTasks() {
        // always emitted in critical section
        while (mQueue.size > 0) {
            val t = mQueue.removeFirst()
            log.warn("Executing queued task $t")
            mHandler!!.post(t)
        }
    }

    private fun waitForCompletion(timeout: Long): Boolean {
        val lock = Object()
        synchronized(lock) {
            synchronized(mLocker) {
                if (null != mHandler) {
                    mHandler!!.post {
                        synchronized(lock) {
                            lock.notify()
                        }
                    }
                    try {
                        lock.wait(timeout)
                        return true
                    } catch (e: InterruptedException) {
                        log.info("Waiting is interrupted")
                    }
                } else {
                    log.warn("thread already stopped")
                }
            }
        }
        return false
    }

    fun stop(timeout: Long): Boolean {
        log.info("Stop is called.")
        if (!waitForCompletion(timeout))
            return false
        synchronized(mLocker) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2)
                mHandler?.looper?.quitSafely()
            else
                mHandler?.looper?.quit()
        }
        return true
    }

    override fun run() {
        log.info("Running service thread")
        Looper.prepare()
        val looper = Looper.myLooper()
        if (null != looper) {
            synchronized(mLocker) {
                mHandler = ServiceThreadHandler(looper)
                log.info("Service thread handler is created")
                postQueuedTasks()
            }
            Looper.loop()
        } else {
            log.error("Failed to get service thread looper!")
        }
        synchronized(mLocker) {
            mHandler = null
        }
        log.info("Exiting background service thread")
    }

    companion object {
        private val log = SRLog.create("svcth")
    }
}
