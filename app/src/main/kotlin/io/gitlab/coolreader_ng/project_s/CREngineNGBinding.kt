/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Some code from the CoolReader project was used (https://github.com/buggins/coolreader)
 * Copyright (C) 2010-2016 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import java.io.File
import java.util.regex.Pattern
import java.util.zip.ZipEntry

@UsedInJNI
class CREngineNGBinding {

    enum class FontFamily(@UsedInJNI private val code: Int) {
        // In JNI, we cannot access the hidden "ordinal" field on all platforms,
        //  so we introduce our own "code" field to distinguish the values from each other.
        ANY(0),
        SERIF(1),
        SANS_SERIF(2),
        CURSIVE(3),
        FANTASY(4),
        MONOSPACE(5)
    }

    private var mContext: Context? = null
    private var mDataDir: File = File(".")
    private val mSyncObject = Object()

    fun setupEngineData(ctx: Context?) {
        mContext = ctx
        if (setupAppDataDir(mContext)) {
            // add hyphenation dictionaries found in application directory
            val added = LanguagesRegistry.addFromDir(File(mDataDir, "hyph"))
            log.verbose("added $added extra hyphenation dictionaries")
            // register hyphenation dictionaries (both files & builtins)
            setupHyphenations_native(LanguagesRegistry.dictionaries())
            // register app internal fonts
            initFontsFromDir(File(mDataDir, "fonts"))
        } else {
            log.error("Failed to initialize application data directory!")
        }
        // init cache directory
        if (!setupEngineCacheDir(mContext)) {
            log.error("Failed to initialize engine cache directory!")
        }
    }

    fun initFontsFromDir(dir: File): Boolean {
        return initFontsFromDir(dir.absolutePath)
    }

    fun initFontsFromDir(dir: String): Boolean {
        synchronized(mSyncObject) {
            return initFontsFromDir_native(dir)
        }
    }

    fun getFontFaceList(): Array<String> {
        synchronized(mSyncObject) {
            return getFontFaceList_native()
        }
    }

    fun getFontFaceListFiltered(family: FontFamily, langTag: String): Array<String> {
        synchronized(mSyncObject) {
            return getFontFaceListFiltered_native(family, langTag)
        }
    }

    private fun setupAppDataDir(ctx: Context?): Boolean {
        val dir = ctx?.filesDir
        var res = null != dir
        if (res) {
            mDataDir = dir!!
            res = mDataDir.canRead() && mDataDir.canWrite()
        }
        if (res) {
            val extra_css = File(mDataDir, "css")
            res = extra_css.exists() || extra_css.mkdirs()
        }
        if (res) {
            val extra_hyph = File(mDataDir, "hyph")
            res = extra_hyph.exists() || extra_hyph.mkdirs()
        }
        if (res) {
            val extra_backgrounds = File(mDataDir, "backgrounds")
            res = extra_backgrounds.exists() || extra_backgrounds.mkdirs()
        }
        if (res) {
            val extra_fonts = File(mDataDir, "fonts")
            res = extra_fonts.exists() || extra_fonts.mkdirs()
        }
        if (res) {
            val logs_dir = File(mDataDir, "logs")
            res = logs_dir.exists() || logs_dir.mkdirs()
        }
        if (res) {
            val tts_dir = File(mDataDir, "tts")
            res = tts_dir.exists() || tts_dir.mkdirs()
        }
        return res
    }

    private fun setupEngineCacheDir(ctx: Context?): Boolean {
        var dir = ctx!!.cacheDir
        var res = null != dir
        if (res) {
            dir = File(dir, "engineCache")
            res = dir.exists()
            if (!res)
                res = dir.mkdirs()
        }
        if (res)
            res = dir.canRead() && dir.canWrite()
        if (res)
            res = setCacheDir_native(dir.absolutePath, CACHE_DIR_SIZE)
        return res
    }

    @UsedInJNI
    @SuppressWarnings("unused")        // used in jni
    fun loadHyphDictData(code: String?): ByteArray? {
        var data: ByteArray? = null
        val dict = LanguagesRegistry.byCode(code)
        if (HyphenationDict.HYPH_DICT == dict.type) {
            val resId = dict.resourceId
            val file = dict.file
            if (0 != resId) {
                data = Utils.readRawResource(mContext!!, resId)
            } else if (null != file) {
                data = Utils.readFromFile(file)
            }
        }
        return data
    }

    fun getBookInfo(filePath: String): BookInfo? {
        BackgroundThread.ensureBackground()
        synchronized(mSyncObject) {
            return getBookInfo_native(filePath)
        }
    }

    fun getBookCoverData(filePath: String): ByteArray? {
        BackgroundThread.ensureBackground()
        synchronized(mSyncObject) {
            return getBookCoverData_native(filePath)
        }
    }

    /**
     * Draw book cover into bitmap buffer.
     * If cover image specified, this image will be drawn (resized to buffer size).
     * If no cover image, default cover will be drawn, with author, title, series.
     *
     * @param data         is coverpage image data bytes, or null or empty array if no cover image
     * @param maxSize      is maximum cover size
     * @param respectAspectRatio specify to respect aspect ration
     * @param fontFace     is font face to use.
     * @param title        is book title.
     * @param authors      is book authors list
     * @param seriesName   is series name
     * @param seriesNumber is series number
     * @param bpp          is bits per pixel (specify <=8 for eink grayscale dithering)
     * @return Bitmap with cover
     */
    fun drawBookCover(
        data: ByteArray?,
        maxWidth: Int,
        maxHeight: Int,
        respectAspectRatio: Boolean,
        fontFace: String?,
        title: String?,
        authors: String?,
        seriesName: String?,
        seriesNumber: Int,
        bpp: Int
    ): Bitmap? {
        BackgroundThread.ensureBackground()
        synchronized(mSyncObject) {
            return drawBookCover_native(
                data,
                maxWidth,
                maxHeight,
                respectAspectRatio,
                fontFace,
                title,
                authors,
                seriesName,
                seriesNumber,
                bpp
            )
        }
    }

    fun listFiles(dir: File): Array<File?>? {
        return listFiles_native(dir)
    }

    fun recycle() {
        engineCleanup_native()
    }

    private external fun engineInit_native(sdk_int: Int)
    private external fun engineCleanup_native()
    private external fun setCacheDir_native(path: String, size: Long): Boolean
    private external fun setupHyphenations_native(values: Array<HyphenationDict>): Boolean
    private external fun getFontFaceList_native(): Array<String>
    private external fun getFontFaceListFiltered_native(
        family: FontFamily,
        langTag: String
    ): Array<String>

    private external fun initFontsFromDir_native(dir: String): Boolean
    private external fun getBookInfo_native(path: String): BookInfo?
    private external fun getBookCoverData_native(filePath: String): ByteArray?
    private external fun drawBookCover_native(
        data: ByteArray?,
        maxWidth: Int,
        maxHeight: Int,
        respectAspectRatio: Boolean,
        fontFace: String?,
        title: String?,
        authors: String?,
        seriesName: String?,
        seriesNumber: Int,
        bpp: Int
    ): Bitmap?
    private external fun listFiles_native(dir: File): Array<File?>?

    companion object {
        private val log = SRLog.create("crbind")
        private const val CACHE_DIR_SIZE: Long = 524288000 // 500Mb
        private val mSyncObject: Any = Any()

        // See <crengine-ng>/include/lvrend.h
        const val BLOCK_RENDERING_FLAGS_LEGACY = 0
        const val BLOCK_RENDERING_FLAGS_WEB = 0x7FFFFFFF

        const val DOM_VERSION_LEGACY = 0

        // Current version of DOM parsing engine (See <crengine-ng>/include/lvtinydom_common.h)
        val DOM_VERSION_CURRENT: Int

        @UsedInJNI
        @JvmStatic
        fun getArchiveItems(zipFileName: String?): ArrayList<ZipEntry> {
            var array: Array<Triplet<String?, Long?, Long?>?>?
            synchronized(mSyncObject) {
                array = getArchiveItems_native(zipFileName)
            }
            val list: ArrayList<ZipEntry> = ArrayList()
            array?.let {
                for (item in it) {
                    if (item?.field1 != null) {
                        val e = ZipEntry(item.field1)
                        e.size = item.field2 ?: 0
                        e.compressedSize = item.field3 ?: 0
                        list.add(e)
                    }
                }
            }
            return list
        }

        /**
         * Adds hyphens to the specified string using a soft hyphen for the specified language
         * @param langTag language tag (ISO-639)
         * @param src source string
         * @return string with spaced hyphens if successful, null if the specified language is not supported
         */
        @JvmStatic
        fun hyphenate(langTag: String, src: String): String? {
            var startPos: Int
            var endPos: Int
            val delimiterPattern = Pattern.compile("(\\p{Punct}*[\\h\\s]+\\p{Punct}*|\\p{Punct}+)")
            var tail = src
            var word: String
            var hyphenated = ""
            var hyphenatedWord: String?
            var delimiter: String?
            var invalid = false
            while (tail.isNotEmpty()) {
                val matcher = delimiterPattern.matcher(tail)
                if (matcher.find()) {
                    startPos = matcher.start(1)
                    endPos = matcher.end(1)
                    if (startPos in 0..<endPos) {
                        delimiter = matcher.group(1)
                        if (!delimiter.isNullOrEmpty()) {
                            word = tail.substring(0, startPos)
                            if (word.isEmpty()) {
                                hyphenated += delimiter
                                tail = tail.substring(endPos)
                                continue
                            }
                            hyphenatedWord = hyphenateWord_native(langTag, word)
                            if (null != hyphenatedWord) {
                                hyphenated += hyphenatedWord + delimiter
                                tail = tail.substring(endPos)
                                continue
                            } else {
                                invalid = true
                            }
                        }
                    }
                }
                break
            }
            if (!invalid && tail.isNotEmpty()) {
                // hyphenate string tail
                hyphenatedWord = hyphenateWord_native(langTag, tail)
                if (null != hyphenatedWord) {
                    hyphenated += hyphenatedWord
                } else {
                    invalid = true
                }
            }
            return if (!invalid) hyphenated else null
        }

        @JvmStatic
        fun getAvailableFontWeight(fontFace: String): IntArray {
            synchronized(mSyncObject) {
                return getAvailableFontWeight_native(fontFace)
            }
        }

        @JvmStatic
        fun getAvailableSynthFontWeight(): IntArray {
            synchronized(mSyncObject) {
                return getAvailableSynthFontWeight_native()
            }
        }

        @JvmStatic
        fun getHumanReadableLocaleName(langTag: String): String {
            synchronized(mSyncObject) {
                return getHumanReadableLocaleName_native(langTag)
            }
        }

        @JvmStatic
        private external fun getArchiveItems_native(arcName: String?): Array<Triplet<String?, Long?, Long?>?>?

        @JvmStatic
        private external fun getDOMVersionCurrent_native(): Int

        /**
         * Adds hyphens to the specified word using a soft hyphen for the specified language
         * @param langTag language tag (ISO-639)
         * @param word source string
         * @return string with spaced hyphens if successful, null if the specified language is not supported
         */
        @JvmStatic
        private external fun hyphenateWord_native(langTag: String, word: String): String?

        @JvmStatic
        private external fun getAvailableFontWeight_native(fontFace: String): IntArray

        @JvmStatic
        private external fun getAvailableSynthFontWeight_native(): IntArray

        @JvmStatic
        private external fun getHumanReadableLocaleName_native(langTag: String): String

        init {
            System.loadLibrary("crengine_ng_jni")
            DOM_VERSION_CURRENT = getDOMVersionCurrent_native()
        }
    }

    init {
        engineInit_native(Build.VERSION.SDK_INT)
    }
}