/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.transition.Fade
import android.view.Gravity
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.widget.Button
import android.widget.PopupWindow
import io.gitlab.coolreader_ng.mygesturedetector.MyGestureDetector

internal class ImageViewer(
    private val mReaderView: ReaderView,
    private val mImage: PageImageCache,
    viewBar: View
) :
    MyGestureDetector.MyOnGestureListener,
    ScaleGestureDetector.OnScaleGestureListener {
    internal var onCloseListener: OnCloseListener? = null
    private var mX: Int = 0
    private var mY: Int = 0
    private val mWidth = mImage.bitmap!!.width
    private val mHeight = mImage.bitmap!!.height
    private val mBufWidth = mReaderView.mRenderWidth
    private val mBufHeight = mReaderView.mRenderHeight
    private var mScaledWidth = mWidth
    private var mScaledHeight = mHeight
    private var mScaleFactor = 1f
    private val mDetector = MyGestureDetector(mReaderView.context, this)
    private val mScaleDetector: ScaleGestureDetector =
        ScaleGestureDetector(mReaderView.context, this)
    private var mGestScaleActive = false
    private var mPopupWindow: PopupWindow? = null
    private val mHandler = Handler(Looper.getMainLooper())
    private val mHidePopupRunnable: Runnable = Runnable { mPopupWindow?.dismiss() }
    private val mFilterBitmapPaint: Paint
    private val mQuickPaint: Paint

    private fun lockOrientation() {
        mReaderView.activityControl?.lockOrientation()
    }

    private fun unlockOrientation() {
        mReaderView.activityControl?.unlockOrientation()
    }

    private fun centerIfLessThanScreen() {
        if (mScaledHeight < mBufHeight)
            mY = (mBufHeight - mScaledHeight) / 2
        if (mScaledWidth < mBufWidth)
            mX = (mBufWidth - mScaledWidth) / 2
    }

    private fun fixScreenBounds() {
        if (mScaledHeight >= mBufHeight) {
            if (mY < mBufHeight - mScaledHeight)
                mY = mBufHeight - mScaledHeight
            if (mY > 0)
                mY = 0
        }
        if (mScaledWidth >= mBufWidth) {
            if (mX < mBufWidth - mScaledWidth)
                mX = mBufWidth - mScaledWidth
            if (mX > 0)
                mX = 0
        }
    }

    private fun updatePosition() {
        centerIfLessThanScreen()
        fixScreenBounds()
        mReaderView.asyncDrawPage()
    }

    private fun showButtonBar() {
        if (null != mPopupWindow) {
            mHandler.removeCallbacks(mHidePopupRunnable)
            if (!mPopupWindow!!.isShowing) {
                val isFullScreen = mReaderView.activityControl?.isFullScreen() ?: false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                    // To take full screen in full screen mode on API 21-29
                    // On these APIs, without this, in full screen mode, there is an empty bar at the bottom of the screen,
                    //  the same size and position as the navigation bar
                    mPopupWindow!!.isClippingEnabled = !isFullScreen
                }
                mPopupWindow!!.showAtLocation(
                    mReaderView,
                    Gravity.FILL_HORIZONTAL or Gravity.BOTTOM,
                    0,
                    0
                )
            }
            // Hide this popup after 3 seconds
            mHandler.postDelayed(mHidePopupRunnable, 3000)
        }
    }

    private fun zoomIn() {
        zoomIn(mBufWidth / 2, mBufHeight / 2)
    }

    private fun zoomIn(refPointX: Int, refPointY: Int) {
        if (mScaleFactor < 4) {
            val lx: Float = (refPointX - mX) / mScaleFactor
            val ly: Float = (refPointY - mY) / mScaleFactor
            val oldScale = mScaleFactor
            mScaleFactor *= 2f
            mX += (lx * (oldScale - mScaleFactor)).toInt()
            mY += (ly * (oldScale - mScaleFactor)).toInt()
            mScaledHeight = (mHeight * mScaleFactor).toInt()
            mScaledWidth = (mWidth * mScaleFactor).toInt()
            updatePosition()
        }
    }

    private fun zoomOut() {
        zoomOut(mBufWidth / 2, mBufHeight / 2)
    }

    private fun zoomOut(refPointX: Int, refPointY: Int) {
        if (mScaledWidth >= mWidth || mScaledHeight >= mHeight) {
            val lx: Float = (refPointX - mX) / mScaleFactor
            val ly: Float = (refPointY - mY) / mScaleFactor
            val oldScale = mScaleFactor
            mScaleFactor /= 2f
            mX += (lx * (oldScale - mScaleFactor)).toInt()
            mY += (ly * (oldScale - mScaleFactor)).toInt()
            mScaledHeight = (mHeight * mScaleFactor).toInt()
            mScaledWidth = (mWidth * mScaleFactor).toInt()
            updatePosition()
        }
    }

    private fun zoom100() {
        zoom100(mBufWidth / 2, mBufHeight / 2)
    }

    private fun zoom100(refPointX: Int, refPointY: Int) {
        val lx: Float = (refPointX - mX) / mScaleFactor
        val ly: Float = (refPointY - mY) / mScaleFactor
        val oldScale = mScaleFactor
        mScaleFactor = 1f
        mX += (lx * (oldScale - mScaleFactor)).toInt()
        mY += (ly * (oldScale - mScaleFactor)).toInt()
        mScaledWidth = mWidth
        mScaledHeight = mHeight
        updatePosition()
    }

    private fun zoomToFit() {
        val xscale = mBufWidth.toFloat() / mWidth.toFloat()
        val yscale = mBufHeight.toFloat() / mHeight.toFloat()
        if (xscale < yscale) {
            mScaledWidth = mBufWidth
            mScaledHeight = (mHeight.toFloat() * xscale).toInt()
        } else {
            mScaledWidth = (mWidth.toFloat() * yscale).toInt()
            mScaledHeight = mBufHeight
        }
        mScaleFactor = mScaledWidth.toFloat() / mWidth.toFloat()
        updatePosition()
    }

    private val step: Int
        get() {
            var max = mBufHeight
            if (max < mBufWidth)
                max = mBufWidth
            return max / 10
        }

    private fun moveBy(dx: Int, dy: Int) {
        mX += dx
        mY += dy
        updatePosition()
    }

    fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        var code = keyCode
        if (code == 0)
            code = event.scanCode
        when (code) {
            KeyEvent.KEYCODE_VOLUME_UP -> {
                zoomIn()
                return true
            }

            KeyEvent.KEYCODE_VOLUME_DOWN -> {
                zoomOut()
                return true
            }

            KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_ENDCALL -> {
                close()
                return true
            }

            KeyEvent.KEYCODE_DPAD_LEFT -> {
                moveBy(step, 0)
                return true
            }

            KeyEvent.KEYCODE_DPAD_RIGHT -> {
                moveBy(-step, 0)
                return true
            }

            KeyEvent.KEYCODE_DPAD_UP -> {
                moveBy(0, step)
                return true
            }

            KeyEvent.KEYCODE_DPAD_DOWN -> {
                moveBy(0, -step)
                return true
            }
        }
        return false
    }

    fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {
        var code = keyCode
        if (code == 0)
            code = event.scanCode
        when (code) {
            KeyEvent.KEYCODE_BACK,
            KeyEvent.KEYCODE_ENDCALL -> {
                close()
                return true
            }
        }
        return false
    }

    fun onTouchEvent(event: MotionEvent): Boolean {
        val res1 = mDetector.onTouchEvent(event)
        val res2 = mScaleDetector.onTouchEvent(event)
        return res1 or res2
    }

    override fun onFling(
        e1: MotionEvent?,
        e2: MotionEvent,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        //Log.v(TAG, "onFling()")
        if (mGestScaleActive)
            return false
        return true
    }

    override fun onScroll(
        e1: MotionEvent?,
        e2: MotionEvent,
        distanceX: Float,
        distanceY: Float
    ): Boolean {
        //Log.v(TAG, "onScroll() $distanceX, $distanceY")
        if (mGestScaleActive)
            return false
        moveBy(-distanceX.toInt(), -distanceY.toInt())
        return true
    }

    override fun onLongPress(e: MotionEvent) {
        if (mGestScaleActive)
            return
        close()
    }

    override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        if (null != mPopupWindow) {
            if (mPopupWindow!!.isShowing)
                mPopupWindow!!.dismiss()
            else
                showButtonBar()
        }
        return true
    }

    override fun onDoubleTap(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        //if (null == e)
        //    return false
        // Zoom in on double tap
        zoomIn(e.x.toInt(), e.y.toInt())
        showButtonBar()
        return true
    }

    override fun onDoubleTapEvent(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        return true
    }

    override fun onUp(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        return true
    }

    override fun onDown(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        return true
    }

    override fun onShowPress(e: MotionEvent) {
    }

    override fun onSingleTapUp(e: MotionEvent): Boolean {
        if (mGestScaleActive)
            return false
        return true
    }

    override fun onScale(det: ScaleGestureDetector): Boolean {
        if (!mGestScaleActive)
            return false
        //if (null == det)
        //    return false
        if ((det.scaleFactor > 1f && mScaleFactor < 4f) ||
            (det.scaleFactor < 1f && (mScaledWidth >= mBufWidth || mScaledHeight >= mBufHeight))
        ) {
            val lx: Float = (det.focusX - mX) / mScaleFactor
            val ly: Float = (det.focusY - mY) / mScaleFactor
            val oldScale = mScaleFactor
            mScaleFactor *= det.scaleFactor
            mX += (lx * (oldScale - mScaleFactor)).toInt()
            mY += (ly * (oldScale - mScaleFactor)).toInt()
            mScaledWidth = (mScaleFactor * mWidth).toInt()
            mScaledHeight = (mScaleFactor * mHeight).toInt()
            updatePosition()
            log.debug("onScale(): new scale=$mScaleFactor")
        }
        return true
    }

    override fun onScaleBegin(det: ScaleGestureDetector): Boolean {
        mGestScaleActive = true
        return true
    }

    override fun onScaleEnd(det: ScaleGestureDetector) {
        mGestScaleActive = false
        showButtonBar()
        mReaderView.asyncDrawPage()
    }

    fun close() {
        mPopupWindow?.dismiss()
        mImage.recycle()
        unlockOrientation()
        onCloseListener?.onClose()
    }

    fun draw(canvas: Canvas) {
        // TODO: draw background
        val dst = Rect(mX, mY, mX + mScaledWidth, mY + mScaledHeight)
        val src = Rect(0, 0, mImage.bitmap!!.width, mImage.bitmap!!.height)
        if (dst.width() != canvas.width || dst.height() != canvas.height)
            canvas.drawColor(Color.rgb(32, 32, 32))
        canvas.drawBitmap(
            mImage.bitmap!!,
            src,
            dst,
            if (mGestScaleActive) mQuickPaint else mFilterBitmapPaint
        )
    }

    init {
        lockOrientation()
        zoomToFit()
        viewBar.measure(
            View.MeasureSpec.makeMeasureSpec(mReaderView.width, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(mReaderView.height / 8, View.MeasureSpec.AT_MOST)
        )
        val btnZoomOut: Button = viewBar.findViewById(R.id.btnZoomOut)
        btnZoomOut.setOnClickListener {
            zoomOut()
            mHandler.removeCallbacks(mHidePopupRunnable)
            mHandler.postDelayed(mHidePopupRunnable, 3000)
        }
        val btnZoomIn: Button = viewBar.findViewById(R.id.btnZoomIn)
        btnZoomIn.setOnClickListener {
            zoomIn()
            mHandler.removeCallbacks(mHidePopupRunnable)
            mHandler.postDelayed(mHidePopupRunnable, 3000)
        }
        val btnZoom100: Button = viewBar.findViewById(R.id.btnZoom100)
        btnZoom100.setOnClickListener {
            zoom100()
            mHandler.removeCallbacks(mHidePopupRunnable)
            mHandler.postDelayed(mHidePopupRunnable, 3000)
        }
        val btnZoomFit: Button = viewBar.findViewById(R.id.btnZoomFit)
        btnZoomFit.setOnClickListener {
            zoomToFit()
            mHandler.removeCallbacks(mHidePopupRunnable)
            mHandler.postDelayed(mHidePopupRunnable, 3000)
        }
        val btnClose: Button = viewBar.findViewById(R.id.btnClose)
        btnClose.setOnClickListener { close() }
        mPopupWindow = PopupWindow(viewBar, viewBar.measuredWidth, viewBar.measuredHeight)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopupWindow!!.enterTransition = Fade(Fade.IN)
            mPopupWindow!!.exitTransition = Fade(Fade.OUT)
        } else {
            mPopupWindow!!.animationStyle = android.R.style.Animation_Toast
        }
        mPopupWindow!!.isOutsideTouchable = false
        mPopupWindow!!.isTouchable = true
        mPopupWindow!!.isFocusable = false
        showButtonBar()
        mFilterBitmapPaint = Paint(Paint.FILTER_BITMAP_FLAG or Paint.DITHER_FLAG)
        mQuickPaint = Paint(0)
        updatePosition()
    }

    companion object {
        private val log = SRLog.create("imgview")
    }
}