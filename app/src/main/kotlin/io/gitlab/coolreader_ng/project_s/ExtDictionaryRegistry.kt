/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2015 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.content.ComponentName
import android.content.Intent

object ExtDictionaryRegistry {

    private val DICT_INVALID = ExtDictionaryDef(
        id = "Invalid",
        name = "Invalid",
        packageName = "com.example.-invalid-.-package-",
        className = "com.example.-invalid-.-package-.-Activity-",
        action = Intent.ACTION_SEND
    )

    private val DICT_GOOGLE_TRANSLATE = ExtDictionaryDef(
        id = "GoogleTranslate",
        name = "Google Translate",
        packageName = "com.google.android.apps.translate",
        className = "com.google.android.apps.translate.TranslateActivity",
        action = Intent.ACTION_SEND,
        dataKey = Intent.EXTRA_TEXT
    )

    private val DICT_YANDEX_TRANSLATE = ExtDictionaryDef(
        id = "YandexTranslate",
        name = "Yandex Translate",
        packageName = "ru.yandex.translate",
        className = "ru.yandex.translate.ui.activities.MainActivity",
        action = Intent.ACTION_SEND,
        dataKey = Intent.EXTRA_TEXT
    )

    private val DICT_AARD2 = ExtDictionaryDef(
        id = "aard2",
        name = "Aard 2",
        packageName = "itkach.aard2",
        className = "itkach.aard2.ArticleCollectionActivity",
        action = Intent.ACTION_SEND,
        dataKey = Intent.EXTRA_TEXT
    )

    val DEFAULT = DICT_GOOGLE_TRANSLATE

    val AVAILABLE_DICTIONARIES = arrayOf(
        DICT_GOOGLE_TRANSLATE,
        DICT_YANDEX_TRANSLATE,
        DICT_AARD2
    )

    fun buildIntent(text: String): Intent {
        // Find dictionary definition
        var dict: ExtDictionaryDef? = null
        for (ed in AVAILABLE_DICTIONARIES) {
            if (ed.id == currentDictId) {
                dict = ed
            }
        }
        dict?.let {
            // Common properties
            val intent = Intent(it.action)
            if (null != it.className)
                intent.setComponent(ComponentName(it.packageName, it.className))
            else
                intent.setPackage(it.packageName)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.putExtra(it.dataKey, text)
            // parameters specific to each dictionary
            when (it.id) {
                DICT_YANDEX_TRANSLATE.id -> {
                    intent.setType("text/plain");
                }

                DICT_AARD2.id -> {
                    intent.setType("text/plain");
                }
            }
            return intent
        }
        throw RuntimeException("Invalid/unsupported dictionary id $currentDictId")
    }

    var currentDictId: String = DEFAULT.id

    val currentDict: ExtDictionaryDef
        get() {
            for (dict in AVAILABLE_DICTIONARIES) {
                if (dict.id == currentDictId)
                    return dict
            }
            return DICT_INVALID
        }
}