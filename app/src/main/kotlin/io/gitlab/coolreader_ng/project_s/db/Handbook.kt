/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.db

import java.io.Serial
import java.io.Serializable

class Handbook(val allowDuplicateIds: Boolean = false) :
    AbstractMutableList<Handbook.HandbookItem>(), RandomAccess, Serializable, Cloneable {

    data class HandbookItem(var id: Long = 0, var name: String = "")

    private val mListImpl = ArrayList<HandbookItem>()
    private val mIdHash = HashMap<Long, HandbookItem>()
    private val mNameHash = HashMap<String, HandbookItem>()

    constructor(other: Handbook) : this() {
        // shallow copy of elements
        addAll(other)
    }

    override val size: Int
        get() = mListImpl.size

    @Throws(IndexOutOfBoundsException::class, DuplicateIdException::class)
    override fun add(index: Int, element: HandbookItem) {
        if (!allowDuplicateIds) {
            val test = mIdHash[element.id]
            if (null != test)
                throw DuplicateIdException("Already exist item with id=${element.id}: $test")
        }
        mListImpl.add(index, element)
        mIdHash[element.id] = element
        mNameHash[element.name] = element
    }

    @Throws(IndexOutOfBoundsException::class)
    override fun get(index: Int): HandbookItem {
        return mListImpl[index]
    }

    @Throws(IndexOutOfBoundsException::class)
    override fun removeAt(index: Int): HandbookItem {
        val removedItem = mListImpl.removeAt(index)
        mIdHash.remove(removedItem.id)
        mNameHash.remove(removedItem.name)
        // Handbook can have several elements with a non-unique name
        // By removing a name from the name hash, we will lose the index on the name if another element with the same name exists.
        return removedItem
    }

    @Throws(IndexOutOfBoundsException::class)
    override fun set(index: Int, element: HandbookItem): HandbookItem {
        mListImpl.add(HandbookItem(0, ""))
        val atItem = mListImpl[index]
        if (atItem == element)
            return atItem
        val prevItem = mListImpl.set(index, element)
        mIdHash.remove(prevItem.id)
        mNameHash.remove(prevItem.name)
        mIdHash[element.id] = element
        mNameHash[element.name] = element
        return prevItem
    }

    fun addEntry(id: Long, name: String) {
        add(HandbookItem(id, name))
    }

    fun getById(id: Long): HandbookItem? {
        return mIdHash[id]
    }

    fun getByName(name: String): HandbookItem? {
        return mNameHash[name]
    }

    override fun clone(): Any {
        return Handbook(this)
    }

    companion object {
        @Serial
        private const val serialVersionUID: Long = 4979023110316183900L
    }
}
