/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import android.widget.TextView
import com.google.android.material.materialswitch.MaterialSwitch
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import io.gitlab.coolreader_ng.project_s.extensions.getCurrentSelectedPosition
import io.gitlab.coolreader_ng.project_s.extensions.setCurrentSelectedItem
import io.gitlab.coolreader_ng.project_s.extensions.setSimpleItems

class StylePageLangViewHolder(itemView: View, props: SRProperties, var bookLang: String) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private val mLabelDocLanguage: TextView
    private val mMultiLangSupportSwitch: MaterialSwitch
    private val mLegacyRenderingLabel: TextView
    private val mLanguageViewGroup: ViewGroup
    private val mFallbackLangEdit: MaterialAutoCompleteTextView?
    private val mFallbackLangSpinner: Spinner?
    private val mAllowHyphenationSwitch: MaterialSwitch
    private val mHyphenationDictGroup: ViewGroup
    private val mHyphenDictEdit: MaterialAutoCompleteTextView?
    private val mHyphenDictSpinner: Spinner?
    private val mAllowedHyphenDicts: Array<HyphenationDict>
    private val mShowFootnotesSwitch: MaterialSwitch

    override fun onUpdateViewImpl() {
        val legacyRendering = mProps.getInt(
            PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
            CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
        ) == 0 ||
                mProps.getInt(
                    PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION,
                    CREngineNGBinding.DOM_VERSION_CURRENT
                ) < 20180524

        mLegacyRenderingLabel.visibility = if (legacyRendering) View.VISIBLE else View.GONE

        // Book's language
        mLabelDocLanguage.text = CREngineNGBinding.getHumanReadableLocaleName(bookLang)

        // Multi-language (embedded language) support
        mMultiLangSupportSwitch.isEnabled = !legacyRendering
        mMultiLangSupportSwitch.isChecked =
            mProps.getBool(PropNames.Engine.PROP_TEXTLANG_EMBEDDED_LANGS_ENABLED, true)
        if (mMultiLangSupportSwitch.isChecked && !legacyRendering) {
            mLanguageViewGroup.visibility = View.VISIBLE
            mAllowHyphenationSwitch.visibility = View.VISIBLE
            mHyphenationDictGroup.visibility = View.GONE
        } else {
            mLanguageViewGroup.visibility = View.GONE
            mAllowHyphenationSwitch.visibility = View.GONE
            mHyphenationDictGroup.visibility = View.VISIBLE
        }

        // Fallback main language autoset or override
        val textLangMainLangAutoset =
            mProps.getBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, true)
        val textLangMainLang = mProps.getProperty(PropNames.Engine.PROP_TEXTLANG_MAIN_LANG, "")
        if (textLangMainLangAutoset) {
            mFallbackLangEdit?.setCurrentSelectedItem(0)
            mFallbackLangSpinner?.setSelection(0)
        } else {
            mFallbackLangEdit?.setCurrentSelectedItem(textLangMainLang)
            mFallbackLangSpinner?.setCurrentSelectedItem(textLangMainLang)
        }

        // Allow hyphenations
        mAllowHyphenationSwitch.isChecked =
            mProps.getBool(PropNames.Engine.PROP_TEXTLANG_HYPHENATION_ENABLED, true)

        // Hyphenation dictionary
        val dictCode = mProps.getProperty(PropNames.Engine.PROP_HYPHENATION_DICT, "")
        val dict = LanguagesRegistry.byCode(dictCode)
        mHyphenDictEdit?.setCurrentSelectedItem(dict.name)
        mHyphenDictSpinner?.setCurrentSelectedItem(dict.name)

        // Show footnotes
        mShowFootnotesSwitch.isChecked = mProps.getBool(PropNames.Engine.PROP_FOOTNOTES, true)
    }

    override fun onResetViewImpl() {
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // On layout for API26+
            mFallbackLangEdit = itemView.findViewById(R.id.fallbackLangEdit)
            mHyphenDictEdit = itemView.findViewById(R.id.hyphenDictEdit)
            mFallbackLangSpinner = null
            mHyphenDictSpinner = null
        } else {
            // On layout for API less than 26
            mFallbackLangEdit = null
            mHyphenDictEdit = null
            mFallbackLangSpinner = itemView.findViewById(R.id.fallbackLangSpinner)
            mHyphenDictSpinner = itemView.findViewById(R.id.hyphenDictSpinner)
        }
        mLabelDocLanguage = itemView.findViewById(R.id.labelDocLanguage)
        mMultiLangSupportSwitch = itemView.findViewById(R.id.multiLangSupportSwitch)
        mLegacyRenderingLabel = itemView.findViewById(R.id.legacyRenderingLabel)
        mLanguageViewGroup = itemView.findViewById(R.id.languageGroup)
        mAllowHyphenationSwitch = itemView.findViewById(R.id.allowHyphenationSwitch)
        mHyphenationDictGroup = itemView.findViewById(R.id.hyphenationDictGroup)
        mShowFootnotesSwitch = itemView.findViewById(R.id.showFootnotesSwitch)

        // Multi-language (embedded) support
        mMultiLangSupportSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_TEXTLANG_EMBEDDED_LANGS_ENABLED, isChecked)
            commitChanges()
        }

        // Fallback main language autoset or override
        val langArrayList = ArrayList<String>(LanguagesRegistry.languages().size + 1)
        langArrayList.add(context.getString(R.string.as_book_s_language))
        for (lang in LanguagesRegistry.languages()) {
            langArrayList.add(lang.name)
        }
        val langArray = langArrayList.toTypedArray()
        // On layout for API26+
        mFallbackLangEdit?.setSimpleItems(langArray)
        mFallbackLangEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevTextLangMainLangAutoset =
                    mProps.getBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, true)
                val prevTextLangMainLang =
                    mProps.getProperty(PropNames.Engine.PROP_TEXTLANG_MAIN_LANG, "")
                // Skip "(do not change)" item
                val position = mFallbackLangEdit.getCurrentSelectedPosition()
                val textLangMainLangIdx = position - 1
                val supportedLanguages = LanguagesRegistry.languages()
                val textLangMainLang =
                    if (textLangMainLangIdx >= 0 && textLangMainLangIdx < supportedLanguages.size) supportedLanguages[textLangMainLangIdx].langTag else ""
                val textLangMainLangAutoset = (0 == position) || textLangMainLang.isEmpty()
                var haveChanges = false
                if (prevTextLangMainLangAutoset != textLangMainLangAutoset) {
                    mProps.setBool(
                        PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET,
                        textLangMainLangAutoset
                    )
                    haveChanges = true
                }
                if (!textLangMainLangAutoset) {
                    if (prevTextLangMainLang != textLangMainLang) {
                        mProps.setBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, false)
                        mProps.setProperty(
                            PropNames.Engine.PROP_TEXTLANG_MAIN_LANG,
                            textLangMainLang
                        )
                        haveChanges = true
                    }
                }
                if (haveChanges)
                    commitChanges()
            }
        })
        // On layout for API less than 26
        mFallbackLangSpinner?.setSimpleItems(langArray)
        mFallbackLangSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevTextLangMainLangAutoset =
                    mProps.getBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, true)
                val prevTextLangMainLang =
                    mProps.getProperty(PropNames.Engine.PROP_TEXTLANG_MAIN_LANG, "")
                // Skip "(do not change)" item
                val textLangMainLangIdx = position - 1
                val supportedLanguages = LanguagesRegistry.languages()
                val textLangMainLang =
                    if (textLangMainLangIdx >= 0 && textLangMainLangIdx < supportedLanguages.size) supportedLanguages[textLangMainLangIdx].langTag else ""
                val textLangMainLangAutoset = (0 == position) || textLangMainLang.isEmpty()
                var haveChanges = false
                if (prevTextLangMainLangAutoset != textLangMainLangAutoset) {
                    mProps.setBool(
                        PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET,
                        textLangMainLangAutoset
                    )
                    haveChanges = true
                }
                if (!textLangMainLangAutoset) {
                    if (prevTextLangMainLang != textLangMainLang) {
                        mProps.setBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, false)
                        mProps.setProperty(
                            PropNames.Engine.PROP_TEXTLANG_MAIN_LANG,
                            textLangMainLang
                        )
                        haveChanges = true
                    }
                }
                if (haveChanges)
                    commitChanges()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Allow hyphenation (in multilang support)
        mAllowHyphenationSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_TEXTLANG_HYPHENATION_ENABLED, isChecked)
            commitChanges()
        }

        // Hyphenation dictionary
        // Filter hidden items
        val allowedDictList = ArrayList<HyphenationDict>()
        val dictLabelsList = ArrayList<String>()
        for (dict in LanguagesRegistry.dictionaries()) {
            if (!dict.isHidden) {
                allowedDictList.add(dict)
                dictLabelsList.add(dict.name)
            }
        }
        val dictLabels = dictLabelsList.toTypedArray()
        mAllowedHyphenDicts = allowedDictList.toTypedArray()
        // On layout for API26+
        mHyphenDictEdit?.setSimpleItems(dictLabels)
        mHyphenDictEdit?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val prevHyphenDictCode =
                    mProps.getProperty(PropNames.Engine.PROP_HYPHENATION_DICT, "")
                val hyphenDictIdx = mHyphenDictEdit!!.getCurrentSelectedPosition()
                val hyphenDictCode =
                    if (hyphenDictIdx >= 0 && hyphenDictIdx < mAllowedHyphenDicts.size) mAllowedHyphenDicts[hyphenDictIdx].code else ""
                if (prevHyphenDictCode != hyphenDictCode && hyphenDictCode.isNotEmpty()) {
                    mProps.setProperty(PropNames.Engine.PROP_HYPHENATION_DICT, hyphenDictCode)
                    commitChanges()
                }
            }
        })
        // On layout for API less than 26
        mHyphenDictSpinner?.setSimpleItems(dictLabels)
        mHyphenDictSpinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val prevHyphenDictCode =
                    mProps.getProperty(PropNames.Engine.PROP_HYPHENATION_DICT, "")
                val hyphenDictCode =
                    if (position >= 0 && position < mAllowedHyphenDicts.size) mAllowedHyphenDicts[position].code else ""
                if (prevHyphenDictCode != hyphenDictCode && hyphenDictCode.isNotEmpty()) {
                    mProps.setProperty(PropNames.Engine.PROP_HYPHENATION_DICT, hyphenDictCode)
                    commitChanges()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }

        // Show footnotes
        mShowFootnotesSwitch.setOnCheckedChangeListener { _, isChecked ->
            mProps.setBool(PropNames.Engine.PROP_FOOTNOTES, isChecked)
            commitChanges()
        }
    }
}