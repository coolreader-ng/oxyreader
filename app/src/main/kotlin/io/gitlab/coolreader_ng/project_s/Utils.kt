/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.Manifest
import android.app.Activity
import android.app.Service
import android.content.ContentResolver
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.SystemClock
import android.util.DisplayMetrics
import android.view.Menu
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.forEach
import androidx.documentfile.provider.DocumentFile
import androidx.recyclerview.widget.RecyclerView
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.ObjectOutputStream
import java.io.OutputStream
import java.security.MessageDigest
import java.text.NumberFormat
import java.util.Locale
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.math.abs

object Utils {

    private val log = SRLog.create("utils")

    enum class Units {
        PX,
        PT,
        DP
    }

    data class FilePathInfo(
        val parentPath: String, // path to parent directory
        val basename: String,   // file name without path and extension
        val extension: String   // file extension
    ) {
        fun isEmpty(): Boolean {
            return basename.isEmpty()
        }
    }

    var DPI = DisplayMetrics.DENSITY_DEFAULT
        private set
    var DPI_SCALE: Float = 1.0f
        private set
    const val DPI_BASE = DisplayMetrics.DENSITY_DEFAULT.toFloat()

    private var mFontFaceList: Array<String>? = null

    private const val READ_BUF_SZ = 4096

    fun parseInt(str: String?, defValue: Int): Int {
        return parseInt(str, defValue, Int.MIN_VALUE, Int.MAX_VALUE)
    }

    fun parseInt(str: String?, defValue: Int, minValue: Int, maxValue: Int): Int {
        var res: Int
        if (null == str)
            return defValue
        res = try {
            str.toInt()
        } catch (e: NumberFormatException) {
            defValue
        }
        if (res < minValue)
            res = minValue
        else if (res > maxValue)
            res = maxValue
        return res
    }

    fun parseFloat(str: String?, defValue: Float): Float {
        return parseFloat(str, defValue, Float.MIN_VALUE, Float.MAX_VALUE)
    }

    fun parseFloat(str: String?, defValue: Float, minValue: Float, maxValue: Float): Float {
        var res: Float
        if (null == str)
            return defValue
        res = try {
            str.toFloat()
        } catch (e: NumberFormatException) {
            defValue
        }
        if (res < minValue)
            res = minValue
        else if (res > maxValue)
            res = maxValue
        return res
    }

    fun parseBool(str: String?, defValue: Boolean = false): Boolean {
        if (null == str)
            return defValue
        return when (str.lowercase()) {
            "1",
            "true",
            "t",
            "on",
            "yes",
            "y" -> true

            "0",
            "false",
            "f",
            "off",
            "no",
            "n" -> false

            else -> defValue
        }
    }

    fun retrieveAndUseDPI(activity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            DPI = activity.resources.configuration.densityDpi
        } else {
            val dm = DisplayMetrics()
            activity.windowManager.defaultDisplay.getMetrics(dm)
            DPI = dm.densityDpi
        }
        DPI_SCALE = DPI.toFloat() / DPI_BASE
    }

    fun convertUnits(value: Float, from: Units, to: Units): Float {
        // https://developer.android.com/guide/topics/resources/more-resources.html#Dimension
        // 72pt equals 1 inch
        // 160dp (DPI_BASE) equals 1 inch
        when (from) {
            Units.PX -> {
                return when (to) {
                    // px -> px
                    Units.PX -> value
                    // px -> dp
                    Units.DP -> value / DPI_SCALE
                    // px -> pt
                    Units.PT -> 72f * value / (DPI_SCALE * DPI_BASE)
                }
            }

            Units.DP -> {
                return when (to) {
                    // dp -> px
                    Units.PX -> value * DPI_SCALE
                    // dp -> dp
                    Units.DP -> value
                    // dp -> pt
                    Units.PT -> value * 72f / DPI_BASE
                }
            }

            Units.PT -> {
                return when (to) {
                    // pt -> px
                    Units.PX -> DPI_SCALE * value * DPI_BASE / 72
                    // pt -> pt
                    Units.PT -> value
                    // pt -> dp
                    Units.DP -> value * DPI_BASE / 72f
                }
            }
        }
    }

    /**
     * Convert color integer to hex string
     */
    fun colorToHtmlHexString(@ColorInt color: Int, withAlpha: Boolean): String {
        val a = Color.alpha(color)
        val r = Color.red(color)
        val g = Color.green(color)
        val b = Color.blue(color)
        return if (withAlpha)
            String.format(Locale.US, "#%02x%02x%02x%02x", r, g, b, a)
        else
            String.format(Locale.US, "#%02x%02x%02x", r, g, b)
    }

    /**
     * Convert color integer to string CSS3 RGBA representation
     */
    fun colorToCssRGBAString(@ColorInt color: Int): String {
        val a = Color.alpha(color).toFloat()
        val r = Color.red(color)
        val g = Color.green(color)
        val b = Color.blue(color)
        return String.format(Locale.US, "rgba(%d,%d,%d,%.2f)", r, g, b, a / 255f)
    }

    /**
     * Returns a string describing the weight of the font
     * @param weight font weight
     */
    fun fontWeightToName(weight: Int): String {
        val name: String
        if (weight < 100)
            name = "Thin-"
        else if (weight == 100)
            name = "Thin"
        else if (weight <= 150)
            name = "Thin+"
        else if (weight < 200)
            name = "Extra Light-"
        else if (weight == 200)
            name = "Extra Light"
        else if (weight <= 250)
            name = "Extra Light+"
        else if (weight < 300)
            name = "Light-"
        else if (weight == 300)
            name = "Light"
        else if (weight <= 350)
            name = "Light+"
        else if (weight < 400)
            name = "Normal-"
        else if (weight == 400)
            name = "Normal" // Regular
        else if (weight <= 450)
            name = "Normal+"
        else if (weight < 500)
            name = "Medium-"
        else if (weight == 500)
            name = "Medium"
        else if (weight <= 550)
            name = "Medium+"
        else if (weight < 600)
            name = "Semi Bold-"
        else if (weight == 600)
            name = "Semi Bold"  // Demi Bold
        else if (weight <= 650)
            name = "Semi Bold+"
        else if (weight < 700)
            name = "Bold-"
        else if (weight == 700)
            name = "Bold"
        else if (weight <= 750)
            name = "Bold+"
        else if (weight < 800)
            name = "Extra Bold-"
        else if (weight == 800)
            name = "Extra Bold"
        else if (weight <= 850)
            name = "Extra Bold+"
        else if (weight < 900)
            name = "Black-"
        else if (weight == 900)
            name = "Black"  // Heavy
        else if (weight <= 925)
            name = "Black+"
        else if (weight < 950)
            name = "Extra Black-"
        else if (weight == 950)
            name = "Extra Black"  // Ultra Black
        else
            name = "Extra Black+"
        return name
    }

    /**
     * Returns milliseconds since boot, not counting time spent in deep sleep.
     * @return milliseconds since boot
     *
     * @see android.os.SystemClock.uptimeMillis()
     */
    fun uptime(): Long {
        return SystemClock.uptimeMillis()
    }

    /**
     * Returns the current time in milliseconds.
     * @return current time in milliseconds (UTC)
     *
     * @see java.lang.System.currentTimeMillis()
     */
    fun timeStamp(): Long {
        return System.currentTimeMillis()
    }

    /**
     * Return elapsed milliseconds since specified uptime value.
     * @param startTime uptime value from the beginning of which to calculate the elapsed time
     * @return elapsed time in milliseconds
     */
    fun uptimeElapsed(startTime: Long): Long {
        return uptime() - startTime
    }

    fun readFromFile(f: File?): ByteArray? {
        if (f == null || !f.isFile || !f.exists())
            return null
        var data: ByteArray? = null
        try {
            val inputStream = FileInputStream(f)
            data = readFromStream(inputStream, f.length().toInt())
            inputStream.close()
        } catch (e: Exception) {
            log.error("Cannot read file \"$f\": $e")
        }
        return data
    }

    fun readFromDocFile(contentResolver: ContentResolver, docFile: DocumentFile): ByteArray? {
        return readFromUri(contentResolver, docFile.uri, docFile.length().toInt())
    }

    fun readFromUri(contentResolver: ContentResolver, fileUri: Uri, size: Int = -1): ByteArray? {
        var binaryData: ByteArray? = null
        try {
            val inputStream = contentResolver.openInputStream(fileUri)
                ?: throw RuntimeException("Failed to create input stream")
            binaryData = readFromStream(inputStream, size)
            inputStream.close()
        } catch (e: Exception) {
            log.error("Cannot read data from Uri \"$fileUri\": $e")
        }
        return binaryData
    }

    fun readFromStream(inputStream: InputStream, size: Int = -1): ByteArray? {
        var data: ByteArray? = null
        try {
            if (size > 0) {
                // We know the exact size
                val tmp = ByteArray(size)
                var readBytes: Int
                var pos = 0
                while (pos < size) {
                    val sz = if (size - pos >= READ_BUF_SZ) READ_BUF_SZ else size - pos
                    readBytes = inputStream.read(tmp, pos, sz)
                    if (readBytes > 0)
                        pos += readBytes
                    else
                        break
                }
                if (pos == size)
                    data = tmp
            } else {
                // Size unknown in advance
                // Not optimal memory usage
                val buf = ByteArray(READ_BUF_SZ)
                val outputStream = ByteArrayOutputStream()
                var readBytes: Int
                while (true) {
                    readBytes = inputStream.read(buf)
                    if (readBytes > 0)
                        outputStream.write(buf, 0, readBytes)
                    else
                        break
                }
                data = outputStream.toByteArray()
            }
        } catch (e1: IOException) {
            log.error("I/O error while copying content from input stream to buffer. Interrupted.")
        } catch (e2: OutOfMemoryError) {
            log.error("Out of memory while copying content from input stream to buffer. Interrupted.")
            log.error("$e2")
        } catch (e3: Exception) {
            log.error("failed to read data: $e3")
        }
        return data
    }

    fun copyStreamData(inputStream: InputStream, outputStream: OutputStream): Long {
        // There is no and should not be any exception handling here
        // Exception handling must be implemented on the caller side
        var totalBytes = 0L
        val buf = ByteArray(READ_BUF_SZ)
        var readBytes: Int
        while (true) {
            readBytes = inputStream.read(buf)
            if (readBytes > 0) {
                outputStream.write(buf, 0, readBytes)
                totalBytes += readBytes
            } else
                break
        }
        return totalBytes
    }

    fun getNativeFile(docFile: DocumentFile, checkPresence: Boolean = true): File? {
        return getNativeFile(docFile.uri, checkPresence)
    }

    fun getNativeFile(uri: Uri, checkPresence: Boolean = true): File? {
        if (uri.scheme == "file") {
            uri.path?.let {
                val file = File(it)
                if (checkPresence && (!file.exists() || !file.canRead()))
                    return null
                return file
            }
        }
        return null
    }

    fun readRawResource(context: Context, resId: Int): ByteArray? {
        var data: ByteArray? = null
        try {
            val inputStream = context.resources.openRawResource(resId)
            data = readFromStream(inputStream)
            inputStream.close()
        } catch (e: Exception) {
            log.error("Failed to open resource $resId")
        }
        return data
    }

    fun makeZipArchive(destFile: File, dataDir: File): Boolean {
        return makeZipArchive(FileOutputStream(destFile), dataDir)
    }

    fun makeZipArchive(
        contentResolver: ContentResolver,
        destFile: DocumentFile,
        dataDir: File
    ): Boolean {
        return makeZipArchive(contentResolver, destFile.uri, dataDir)
    }

    fun makeZipArchive(contentResolver: ContentResolver, fileUri: Uri, dataDir: File): Boolean {
        try {
            val outputStream = contentResolver.openOutputStream(fileUri)
                ?: throw RuntimeException("Failed to create output stream")
            return makeZipArchive(outputStream, dataDir)
        } catch (e: Exception) {
            log.error("Cannot read data from Uri \"$fileUri\": $e")
            return false
        }
    }

    fun makeZipArchive(outputStream: OutputStream, dataDir: File): Boolean {
        var res = false
        try {
            val zipOutputStream = ZipOutputStream(outputStream).apply {
                setMethod(ZipOutputStream.DEFLATED)
                setLevel(9)
            }
            val allFiles = dataDir.listFiles()
            val buf = ByteArray(READ_BUF_SZ)
            allFiles?.let {
                for (file in it) {
                    if (file.isFile) {
                        var readBytes: Int
                        val inputStream = FileInputStream(file)
                        val zipEntry = ZipEntry(file.name).apply {
                            method = ZipEntry.DEFLATED
                            time = file.lastModified()
                        }
                        zipOutputStream.putNextEntry(zipEntry)
                        while (true) {
                            readBytes = inputStream.read(buf)
                            if (readBytes > 0)
                                zipOutputStream.write(buf, 0, readBytes)
                            else
                                break
                        }
                        zipOutputStream.closeEntry()
                    } else {
                        // ignore inner directories
                    }
                }
                zipOutputStream.close()
                res = true
            }
        } catch (e: Exception) {
            log.error("Failed to create zip-archive", e)
        }
        return res
    }

    fun calcSHA256(file: File): String {
        try {
            val inputStream = FileInputStream(file)
            val hash = calcSHA256(inputStream)
            inputStream.close()
            return hash
        } catch (e: Exception) {
            log.error("calcSHA256() failed: $e")
        }
        return ""
    }

    fun calcSHA256(contentResolver: ContentResolver, docFile: DocumentFile): String {
        return calcSHA256(contentResolver, docFile.uri)
    }

    fun calcSHA256(contentResolver: ContentResolver, fileUri: Uri): String {
        try {
            val inputStream = contentResolver.openInputStream(fileUri)
                ?: throw RuntimeException("Failed to create input stream")
            val hash = calcSHA256(inputStream)
            inputStream.close()
            return hash
        } catch (e: Exception) {
            log.error("Cannot read data from Uri \"$fileUri\": $e")
        }
        return ""
    }

    fun calcSHA256(stream: InputStream): String {
        try {
            val messageDigest = MessageDigest.getInstance("SHA-256")
            val buf = ByteArray(READ_BUF_SZ)
            var readBytes: Int
            while (true) {
                readBytes = stream.read(buf)
                if (readBytes > 0)
                    messageDigest.update(buf, 0, readBytes)
                else
                    break
            }
            val hashData = messageDigest.digest()
            val builder = StringBuilder()
            for (b in hashData)
                builder.append("%02x".format(b))
            return builder.toString()
        } catch (e: Exception) {
            log.error("calcSHA256() failed: $e")
        }
        return ""
    }

    fun getAvailableTextures(context: Context): Array<SettingsManager.PageBackgroundInfo> {
        val list = ArrayList<SettingsManager.PageBackgroundInfo>()
        //list.add(SettingsManager.BACKGROUND_NONE)
        list.addAll(SettingsManager.INTERNAL_BACKGROUNDS)
        list.addAll(findExtraTextures(context))
        return list.toTypedArray()
    }

    private fun findExtraTextures(context: Context): Collection<SettingsManager.PageBackgroundInfo> {
        val resList = ArrayList<SettingsManager.PageBackgroundInfo>()
        val filesDir = context.filesDir
        val extraBackgrounds = File(filesDir, "backgrounds")
        if (extraBackgrounds.exists()) {
            val filesList = extraBackgrounds.listFiles()
            if (null != filesList) {
                for (f in filesList) {
                    if (f.isFile && f.exists()) {
                        val name = f.name
                        val lcName = name.lowercase()
                        if (lcName.endsWith(".png") ||
                            lcName.endsWith(".jpg") ||
                            lcName.endsWith(".jpeg") ||
                            lcName.endsWith(".gif")
                        ) {
                            val itemName = name.substring(0, name.lastIndexOf('.'))
                            var tiled = false
                            if (itemName.indexOf("tiled") != -1)
                                tiled = true
                            val item = SettingsManager.PageBackgroundInfo(
                                id = f.absolutePath,
                                name = itemName,
                                tiled = tiled,
                                filePath = f.absolutePath
                            )
                            resList.add(item)
                        }
                    }
                }
            }
        }
        return resList
    }

    fun isFontAvailable(fontFace: String): Boolean {
        if (null == mFontFaceList) {
            val engineAcc = ReaderEngineObjectsAccessor.leaseEngineObjects()
            mFontFaceList = engineAcc.crEngineNGBinding?.getFontFaceList()
            ReaderEngineObjectsAccessor.releaseEngineObject(engineAcc)
        }
        if (mFontFaceList?.isEmpty() == false) {
            for (face in mFontFaceList!!) {
                if (fontFace == face)
                    return true
            }
        }
        return false
    }

    fun findNearestIndex(array: FloatArray, value: Float): Int {
        var minDiff = abs(array[0] - value)
        var idx = 0
        for (i in 1 until array.size) {
            val diff = abs(array[i] - value)
            if (diff < minDiff) {
                idx = i
                minDiff = diff
            }
        }
        return idx
    }

    fun findNearestValue(array: FloatArray, value: Float): Float {
        return array[findNearestIndex(array, value)]
    }

    fun findNearestIndex(array: IntArray, value: Int): Int {
        var minDiff = abs(array[0] - value)
        var idx = 0
        for (i in 1 until array.size) {
            val diff = abs(array[i] - value)
            if (diff < minDiff) {
                idx = i
                minDiff = diff
            }
        }
        return idx
    }

    fun findNearestValue(array: IntArray, value: Int): Int {
        return array[findNearestIndex(array, value)]
    }

    fun getScreenOrientation(activity: Activity): Int {
        activity.window?.let {
            return it.attributes.screenOrientation
        }
        return ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
    }

    fun applyScreenOrientation(activity: Activity, screenOrientation: Int) {
        activity.requestedOrientation = screenOrientation
        val wnd = activity.window
        if (wnd != null) {
            val attrs: WindowManager.LayoutParams = wnd.attributes
            attrs.screenOrientation = screenOrientation
            wnd.attributes = attrs
        }
    }

    fun setupThemeForActivity(activity: Activity, themeCode: String) {
        val theme = try {
            SettingsManager.Theme.valueOf(themeCode)
        } catch (e: Exception) {
            SettingsManager.Theme.DAYNIGHT
        }
        log.debug("setupThemeForActivity(): theme=$theme; activity=${activity::class.qualifiedName}")
        activity.application.setTheme(theme.themeId)
        activity.setTheme(theme.themeId)
    }

    /**
     * Sync the contents of a list based on another collection.
     * @param target target data list
     * @param reference reference data collection
     * @param adapter adapter to notify about target collection changes
     *
     * After synchronization is completed, the target list fully matches the reference list.
     * Elements missing from the reference list are removed, missing ones are added.
     * Synchronization takes into account the order of elements.
     * If the contents of the lists are identical, no action is taken.
     */
    fun <E> syncListByReference(
        target: MutableList<E>,
        reference: Collection<E>,
        adapter: RecyclerView.Adapter<*>? = null
    ) {
        // Sync two lists (simple, without searching for gaps)
        var pos = 0
        val refListIter = reference.iterator()
        while (refListIter.hasNext()) {
            val refItem = refListIter.next()
            if (pos < target.size) {
                val item = target[pos]
                if (item != refItem) {
                    target[pos] = refItem
                    adapter?.notifyItemChanged(pos)
                }
            } else {
                target.add(refItem)
                adapter?.notifyItemInserted(pos)
            }
            pos++
        }
        if (target.size > pos) {
            var removedCount = 0
            while (target.size > pos) {
                target.removeAt(pos)
                removedCount++
            }
            adapter?.notifyItemRangeRemoved(pos, removedCount)
        }
    }

    fun checkStoragePermissions(context: Context): Boolean {
        // Check permission for external/shared storage
        val res: Boolean
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // before API 23 (before Android 6.0)
            // already allowed after application installation
            res = true
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            // API 23 (Android 6.0) - API 29 (Android 10)
            val readExtStoragePermissionCheck =
                context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            val writeExtStoragePermissionCheck =
                context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            res = (PackageManager.PERMISSION_GRANTED == readExtStoragePermissionCheck) &&
                    (PackageManager.PERMISSION_GRANTED == writeExtStoragePermissionCheck)
        } else {
            // API 30+ (Android 11+)
            res = Environment.isExternalStorageManager()
        }
        return res
    }

    fun checkPostNotificationPermissions(context: Context): Boolean {
        // Check permission to post notification
        val res: Boolean
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            // before API 33 (before Android 13)
            // already allowed after application installation
            res = true
        } else {
            val postNotificationsPermissionCheck =
                context.checkSelfPermission(Manifest.permission.POST_NOTIFICATIONS)
            // API 33+ (Android 13+)
            res = PackageManager.PERMISSION_GRANTED == postNotificationsPermissionCheck
        }
        return res
    }

    fun saveBookDataToFile(destDir: File, bookInfo: BookInfo, data: ByteArray): BookInfo? {
        val basePath: String
        if (destDir.isDirectory || destDir.mkdirs()) {
            basePath = destDir.absolutePath
        } else {
            log.error("Failed to obtain private books directory!")
            return null
        }
        var codebase = ""
        bookInfo.fileInfo.fingerprint?.let {
            var fp = it
            // "sha256:0820651b35febd5fe8fd5addaebcf7745fac6fb0c41c05327cfe0963b497e712" -> "0820651"
            // "0820651b35febd5fe8fd5addaebcf7745fac6fb0c41c05327cfe0963b497e712" -> "0820651"
            val pos = fp.indexOf(':')
            if (pos > 0)
                fp = fp.substring(pos + 1)
            if (fp.length > 7)
                codebase = fp.substring(0, 7)
        }
        if (codebase.isEmpty())
            codebase = "ts-${timeStamp()}"
        var extension = ""
        bookInfo.format.let {
            if (it.extensions.isNotEmpty())
                extension = it.extensions[0]
        }
        if (bookInfo.fileInfo.isArchive) {
            // No info about archive type
            // TODO: use real archive type extension
            extension += ".zip"
        }
        var filename = codebase + extension
        // Checking that such a file does not exist
        var checkFile = File(basePath, filename)
        var idx = 1
        while (checkFile.exists() && idx < 100) {
            filename = "${codebase}-${idx}${extension}"
            checkFile = File(basePath, filename)
            idx++
        }
        if (checkFile.exists()) {
            log.error("Failed to select a unique file name")
            return null
        }
        val file = checkFile
        try {
            val inputStream = ByteArrayInputStream(data)
            val outputStream = FileOutputStream(file)
            var totalSize: Long = 0
            val buf = ByteArray(4096)
            while (true) {
                val bytesRead = inputStream.read(buf)
                if (bytesRead <= 0)
                    break
                totalSize += bytesRead.toLong()
                outputStream.write(buf, 0, bytesRead)
            }
            outputStream.close()
            if (totalSize > 0) {
                val newBookInfo = BookInfo(bookInfo)
                // Set new path & name
                if (bookInfo.fileInfo.isArchive) {
                    newBookInfo.fileInfo.archiveName = file.absolutePath
                    newBookInfo.fileInfo.arcSize = totalSize
                } else {
                    newBookInfo.fileInfo.fileName = file.name
                    newBookInfo.fileInfo.dirPath = file.parent
                    newBookInfo.fileInfo.pathName = file.absolutePath
                    newBookInfo.fileInfo.modificationTime = file.lastModified()
                    newBookInfo.fileInfo.size = totalSize
                }
                // Save original file name to `extraData`
                bookInfo.fileInfo.fileName?.let { fileName ->
                    try {
                        val map = HashMap<String, String>(1)
                        map[BookInfo.EXTRA_ORIGINAL_FILENAME] = fileName
                        val byteStream = ByteArrayOutputStream()
                        val objectStream = ObjectOutputStream(byteStream)
                        objectStream.writeObject(map)
                        objectStream.close()
                        newBookInfo.extraData = byteStream.toByteArray().copyOf()
                        byteStream.close()
                    } catch (e: Exception) {
                        log.error(e.toString())
                    }
                }
                // Set maximum supported DOM version & block rendering flags
                newBookInfo.domVersion = CREngineNGBinding.DOM_VERSION_CURRENT
                newBookInfo.blockRenderingFlags = CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
                // TODO: calc sha256 for new file (if omitted in source book info)
                return newBookInfo
            }
        } catch (e: Exception) {
            log.error("Exception while saving stream data", e)
            file.delete()
        }
        return null
    }

    fun saveBookStreamToFile(
        destDir: File,
        bookInfo: BookInfo,
        inputStream: InputStream
    ): BookInfo? {
        val basePath: String
        if (destDir.isDirectory || destDir.mkdirs()) {
            basePath = destDir.absolutePath
        } else {
            log.error("Failed to obtain private books directory!")
            return null
        }
        var codebase = ""
        bookInfo.fileInfo.fingerprint?.let {
            var fp = it
            // "sha256:0820651b35febd5fe8fd5addaebcf7745fac6fb0c41c05327cfe0963b497e712" -> "0820651"
            // "0820651b35febd5fe8fd5addaebcf7745fac6fb0c41c05327cfe0963b497e712" -> "0820651"
            val pos = fp.indexOf(':')
            if (pos > 0)
                fp = fp.substring(pos + 1)
            if (fp.length > 7)
                codebase = fp.substring(0, 7)
        }
        if (codebase.isEmpty())
            codebase = "ts-${timeStamp()}"
        var extension = ""
        bookInfo.format.let {
            if (it.extensions.isNotEmpty())
                extension = it.extensions[0]
        }
        if (bookInfo.fileInfo.isArchive) {
            // TODO: use real archive type extension
            extension += ".zip"
        }
        var filename = codebase + extension
        // Checking that such a file does not exist
        var checkFile = File(basePath, filename)
        var idx = 1
        while (checkFile.exists() && idx < 100) {
            filename = "${codebase}-${idx}${extension}"
            checkFile = File(basePath, filename)
            idx++
        }
        if (checkFile.exists()) {
            log.error("Failed to select a unique file name")
            return null
        }
        val file = checkFile
        try {
            val outputStream = FileOutputStream(file)
            var totalSize: Long = 0
            val buf = ByteArray(4096)
            while (true) {
                val bytesRead = inputStream.read(buf)
                if (bytesRead <= 0)
                    break
                totalSize += bytesRead.toLong()
                outputStream.write(buf, 0, bytesRead)
            }
            outputStream.close()
            if (totalSize > 0) {
                val newBookInfo = BookInfo(bookInfo)
                // Set new path & name
                if (bookInfo.fileInfo.isArchive) {
                    newBookInfo.fileInfo.archiveName = file.absolutePath
                    newBookInfo.fileInfo.arcSize = totalSize
                } else {
                    newBookInfo.fileInfo.fileName = file.name
                    newBookInfo.fileInfo.dirPath = file.parent
                    newBookInfo.fileInfo.pathName = file.absolutePath
                    newBookInfo.fileInfo.modificationTime = file.lastModified()
                    newBookInfo.fileInfo.size = totalSize
                }
                // Save original file name to `extraData`
                bookInfo.fileInfo.fileName?.let { fileName ->
                    try {
                        val map = HashMap<String, String>(1)
                        map[BookInfo.EXTRA_ORIGINAL_FILENAME] = fileName
                        val byteStream = ByteArrayOutputStream()
                        val objectStream = ObjectOutputStream(byteStream)
                        objectStream.writeObject(map)
                        objectStream.close()
                        newBookInfo.extraData = byteStream.toByteArray().copyOf()
                        byteStream.close()
                    } catch (e: Exception) {
                        log.error(e.toString())
                    }
                }
                // Set maximum supported DOM version & block rendering flags
                newBookInfo.domVersion = CREngineNGBinding.DOM_VERSION_CURRENT
                newBookInfo.blockRenderingFlags = CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
                // TODO: calc sha256 for new file (if omitted in source book info)
                return BookInfo(newBookInfo)
            }
        } catch (e: Exception) {
            log.error("Exception while saving stream data", e)
            file.delete()
        }
        return null
    }

    fun parseFilePath(filepath: String): FilePathInfo {
        val dotPos = filepath.lastIndexOf('.')
        val separatorPos = filepath.lastIndexOf(File.separator)
        var parentPath = ""
        var basename = ""
        var extension = ""
        if (dotPos > 0) {
            basename = filepath.substring(0, dotPos)
            extension = filepath.substring(dotPos + 1)
        }
        if (separatorPos > 0) {
            parentPath = filepath.substring(0, separatorPos)
            basename = basename.substring(separatorPos + 1)
        }
        return FilePathInfo(parentPath, basename, extension)
    }

    fun scanDrawableForVisibleArea(drawable: Drawable): Rect {
        val w = drawable.intrinsicWidth
        val h = drawable.intrinsicHeight
        val area = Rect(w, h, 0, 0)
        val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val backupBounds = drawable.bounds
        drawable.bounds = Rect(0, 0, w, h)
        drawable.draw(canvas)
        for (y in 0 until h) {
            for (x in 0 until w) {
                val color = bitmap.getPixel(x, y)
                if (Color.alpha(color) != 0) {
                    area.union(x, y)
                }
            }
        }
        drawable.bounds = backupBounds
        return area
    }

    fun applyToolBarColorForMenu(context: Context, menu: Menu) {
        val a = context.obtainStyledAttributes(intArrayOf(R.attr.toolbarButtonColor))
        val toolbarButtonColor = a.getColorStateList(0)
        a.recycle()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            menu.forEach { item ->
                item.setIconTintList(toolbarButtonColor)
                item.subMenu?.let {
                    applyToolBarColorForMenu(context, it)
                }
            }
        } else {
            menu.forEach { item ->
                item.icon?.let {
                    val drawable = DrawableCompat.wrap(it)
                    DrawableCompat.setTintList(drawable, toolbarButtonColor)
                    item.icon = drawable
                }
                item.subMenu?.let {
                    applyToolBarColorForMenu(context, it)
                }
            }
        }
    }

    fun localeToLangTag(locale: Locale): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return locale.toLanguageTag()
        } else {
            val stringBuilder = StringBuilder()
            stringBuilder.append(locale.language)
            if (locale.country.isNotEmpty())
                stringBuilder.append("-${locale.country}")
            if (locale.variant.isNotEmpty())
                stringBuilder.append("-${locale.variant}")
            return stringBuilder.toString()
        }
    }

    fun langTagToLocale(langTag: String): Locale {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return Locale.forLanguageTag(langTag)
        } else {
            var language = ""
            var country = ""
            var variant = ""
            var tail = langTag
            if (tail.isNotEmpty()) {
                // Language
                var delimPos = tail.indexOf('-')
                if (delimPos < 0)
                    delimPos = tail.indexOf('_')
                if (delimPos > 0) {
                    language = tail.substring(0, delimPos)
                    tail = tail.substring(delimPos + 1)
                } else {
                    language = tail
                    tail = ""
                }
            }
            if (tail.isNotEmpty()) {
                // Country
                var delimPos = tail.indexOf('-')
                if (delimPos < 0)
                    delimPos = tail.indexOf('_')
                if (delimPos > 0) {
                    country = tail.substring(0, delimPos)
                    tail = tail.substring(delimPos)
                } else {
                    country = tail
                    tail = ""
                }
            }
            if (tail.isNotEmpty()) {
                // Variant
                variant = tail
            }
            return Locale(language, country, variant)
        }
    }

    /**
     * Determines whether the book file has been copied to the application's internal storage.
     * @param context Context instance
     * @param bookInfo book information instance
     * @return true if the book file is copied to the application's internal storage, false - otherwise.
     */
    fun isBookCopiedToInternalAppStorage(context: Context, bookInfo: BookInfo): Boolean {
        val dir = context.getDir("books", Service.MODE_PRIVATE).absolutePath
        return bookInfo.fileInfo.pathNameWA?.startsWith(dir) == true
    }

    fun getPseudoFilePath(docFile: DocumentFile): String? {
        // https://developer.android.com/reference/androidx/documentfile/provider/DocumentFile#getParentFile()
        // The underlying android.provider.DocumentsProvider only defines a forward mapping
        // from parent to child, so the reverse mapping of child to parent offered here is
        // purely a convenience method, and it may be incorrect if the underlying tree structure changes.
        val name = docFile.name
        docFile.parentFile?.let { return getPseudoFilePath(it) + File.separator + name }
        return name
    }

    fun byteArrayIsEquals(a1: ByteArray?, a2: ByteArray?): Boolean {
        if (null != a1)
            return a1.contentEquals(a2)
        else if (null == a2)
            return true
        return false
    }

    /**
     * Format the file size into a human-readable string.
     * @param size file size to format
     * @param context Context instance for retrieving string resources
     * @param locale Locale instance for number formatting
     */
    fun getHumanReadableFileSize(size: Long, context: Context, locale: Locale? = null): String {
        val maximumFractionDigits: Int
        val suffix: String
        val value: Double = if (size < 10240L) {
            // 0 <= x < 10kB
            maximumFractionDigits = 0
            suffix = context.getString(R.string.file_size_suffix_bytes)
            size.toDouble()
        } else if (size < 1048576L) {
            // 10kB <= x < 1MB
            maximumFractionDigits = 0
            suffix = context.getString(R.string.file_size_suffix_kilobytes)
            size.toDouble() / 1024.0
        } else if (size < 10485760L) {
            // 1MB <= x < 10MB
            maximumFractionDigits = 2
            suffix = context.getString(R.string.file_size_suffix_megabytes)
            size.toDouble() / 1048576.0
        } else if (size < 104857600L) {
            // 10MB <= x < 100MB
            maximumFractionDigits = 1
            suffix = context.getString(R.string.file_size_suffix_megabytes)
            size.toDouble() / 1048576.0
        } else if (size < 1073741824L) {
            // 100MB <= x < 1GB
            maximumFractionDigits = 0
            suffix = context.getString(R.string.file_size_suffix_megabytes)
            size.toDouble() / 1048576.0
        } else if (size < 10737418240L) {
            // 1GB <= x < 10GB
            maximumFractionDigits = 2
            suffix = context.getString(R.string.file_size_suffix_gigabytes)
            size.toDouble() / 1048576.0
        } else {
            // x >= 10GB
            maximumFractionDigits = 1
            suffix = context.getString(R.string.file_size_suffix_gigabytes)
            size.toDouble() / 1073741824.0
        }
        val numberFormat = if (null != locale)
            NumberFormat.getInstance(locale)
        else
            NumberFormat.getInstance()
        numberFormat.minimumFractionDigits = 0
        numberFormat.maximumFractionDigits = maximumFractionDigits
        return numberFormat.format(value) + " " + suffix
    }
}
