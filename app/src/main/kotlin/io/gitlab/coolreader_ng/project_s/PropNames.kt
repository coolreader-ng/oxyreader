/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

object PropNames {

    // application properties
    object App {
        // Fullscreen mode
        const val FULLSCREEN = "app.fullscreen"

        // Toolbar location
        const val TOOLBAR_LOCATION = "app.toolbar.location"

        // page orientation, possible values in SettingsManager.ScreenOrientation (use ordinal property)
        const val SCREEN_ORIENTATION = "app.screen.orientation"

        // Screen backlight brightness, integer (-1, 0 - 100)
        const val SCREEN_BACKLIGHT = "app.screen.backlight"

        // Currently unused: screen backlight brightness for warm component (if available)
        const val SCREEN_WARM_BACKLIGHT = "app.screen.warm.backlight"

        // Screen backlight duration
        const val SCREEN_BACKLIGHT_LOCK = "app.screen.backlight.lock"

        // Text selection with double tap
        const val DOUBLE_TAP_SELECTION = "app.controls.doubletap.selection"

        // Performing double tap actions
        const val DOUBLE_TAP_ACTIONS = "app.controls.doubletap.actions"

        // Text selection with double tap
        const val LONG_TAP_SELECTION = "app.controls.longtap.selection"

        // Performing double tap actions
        const val LONG_TAP_ACTIONS = "app.controls.longtap.actions"

        // Bounce filter
        const val BOUNCE_TAP_INTERVAL = "app.controls.bounce.interval"

        // Tap zone actions group, for example:
        //  app.tapzone.action.tap.3 = action for normal single tap for zone #3
        //  app.tapzone.action.tap.long.3 = action for long tap for zone #3
        //  app.tapzone.action.tap.double.3 = action for double tap for zone #3
        const val TAP_ZONE_ACTIONS_TAP_BASE = "app.tapzone.action.tap."

        // Key press actions group, for example:
        //  app.key.action.press.4 = action for normal single press of key with code 3
        //  app.key.action.press.long.82 = action for long press of key with code 82
        const val KEY_ACTIONS_PRESS_BASE = "app.key.action.press."

        // Whether or not it is possible to use the volume keys.
        // Boolean value
        const val KEY_ACTIONS_ENABLE_VOLUME_KEYS = "app.keys.volume.keys.enabled"

        // Handle long press of hardware keys
        // Boolean value
        const val KEY_ACTIONS_HANDLE_LONG_PRESS = "app.keys.handle.long.press"

        // Brightness control with a swipe gesture, possible values in ReaderView.BrightnessControlType (use ordinal property)
        const val FLICK_BACKLIGHT_CONTROL = "app.screen.backlight.control.flick"

        // Currently unused: warm backlight brightness control with a swipe gesture (if available)
        const val FLICK_WARMLIGHT_CONTROL = "app.screen.warmlight.control.flick"

        // Currently unused: simultaneously control warm and cold backlight
        const val FLICK_BACKLIGHT_CONTROL_TOGETHER = "app.screen.backlight.control.flick.together"

        //const val FM_ENABLED = "app.browser.enabled"

        // scan book properties in internal fm
        const val FM_BOOK_PROPERTY_SCAN_ENABLED = "app.browser.fileprops.scan.enabled"

        // show book cover in internal fm
        const val FM_SHOW_COVERPAGES = "app.browser.coverpages"

        // cover size: 0 -> small, 1 -> medium. 2 -> large
        const val FM_COVERPAGE_SIZE = "app.browser.coverpage.size"

        // Sorting order in internal fm
        const val FM_BOOK_SORT_ORDER = "app.browser.sort.order"

        // Dictionary name
        const val DICTIONARY = "app.dictionary.current"

        // Single selection action
        const val SELECTION_ACTION = "app.selection.action"

        // Multi Selection action
        const val MULTI_SELECTION_ACTION = "app.multiselection.action"

        // Keep selection after sending to dictionary
        const val SELECTION_PERSIST = "app.selection.persist"

        // Page background image (texture) Id
        const val BACKGROUND_IMAGE = "background.image.filename"

        const val NIGHT_MODE = "app.night.mode"

        // E-Ink screen update mode
        const val EINK_SCREEN_UPDATE_MODE = "app.screen.eink.update.mode"

        // Full update interval for E-Ink screen
        const val EINK_SCREEN_UPDATE_INTERVAL = "app.screen.eink.full_update.interval"

        // Page flipping with gestures
        const val GESTURE_PAGE_FLIPPING = "app.touch.gesture.page.flipping"

        // Interface Theme, possible values in SettingsManager.Theme (use Enum.name property)
        const val THEME = "app.ui.theme"

        // Interface language, possible values in SettingsManager.Lang (use Enum.name property)
        const val LOCALE = "app.locale.name"

        // Text reading speed using TTS
        const val TTS_SPEED = "app.tts.speed"

        // Stop reading aloud when there is no movement
        const val TTS_MOTION_TIMEOUT = "app.motion.timeout"

        // TTS Engine to use
        const val TTS_ENGINE = "app.tts.engine"

        // Force use specified TTS language (if automatic language set is unused)
        const val TTS_FORCE_LANGUAGE = "app.tts.force.lang"

        // Force use specific TTS voice (if automatic language set is unused)
        const val TTS_VOICE = "app.tts.voice"

        // Automatic setting of TTS language according to book language
        const val TTS_USE_DOC_LANG = "app.tts.use.doc.lang"

        // Use a workaround to disable processing of abbreviations at the end of a sentence when using "Google Speech Services"
        const val TTS_GOOGLE_END_OF_SENTENCE_ABBR =
            "app.tts.google.end-of-sentence-abbreviation.workaround"

        // Use the dictionary of substitutions and stresses
        const val TTS_USE_DICTIONARY = "app.tts.use.dictionary"

        // Use immobility timeout (boolean)
        const val TTS_USE_IMMOBILITY_TIMEOUT = "app.tts.immobility.timeout.enabled"

        // Immobility timeout value (seconds)
        const val TTS_IMMOBILITY_TIMEOUT_VALUE = "app.tts.immobility.timeout.value"

        // Average animation frame generation time
        const val VIEW_ANIM_DURATION = "app.view.anim.duration"

        // Page flip animation type: possible values in ReaderView.PageAnimationType (use ordinal property)
        const val PAGE_ANIM_TYPE = "app.view.anim.type"

        // Continuous program logging
        const val LOGGING_ALWAYS = "app.logging.always"

        // Last log write date
        const val LOGGING_LAST_WRITE_TIME = "app.logging.last.write.time"

        // Screen backlight brightness (for day mode)
        const val SCREEN_BACKLIGHT_DAY = "app.screen.backlight.day"

        // Screen backlight brightness (for night mode)
        const val SCREEN_BACKLIGHT_NIGHT = "app.screen.backlight.night"

        // Interface Theme (for day mode)
        const val THEME_DAY = "app.ui.theme.day"

        // Interface Theme (for night mode)
        const val THEME_NIGHT = "app.ui.theme.night"

        // Background image (texture) Id  (for day mode)
        const val BACKGROUND_IMAGE_DAY = "background.day.image.filename"

        // Background image (texture) Id  (for night mode)
        const val BACKGROUND_IMAGE_NIGHT = "background.night.image.filename"

        // Font color: integer, no alpha (for day mode)
        const val FONT_COLOR_DAY = "font.color.day"

        // Font color: integer, no alpha (for night mode)
        const val FONT_COLOR_NIGHT = "font.color.night"

        // Background color: integer, no alpha (for day mode)
        const val BACKGROUND_COLOR_DAY = "background.color.day"

        // Background color: integer, no alpha (for night mode)
        const val BACKGROUND_COLOR_NIGHT = "background.color.night"

        // Running title (status bar) text color: integer, no alpha (for day mode)
        const val STATUS_FONT_COLOR_DAY = "crengine.page.header.font.color.day"

        // Running title (status bar) text color: integer, no alpha (for night mode)
        const val STATUS_FONT_COLOR_NIGHT = "crengine.page.header.font.color.night"

        // Automatic setting of TEXTLANG_MAIN_LANG property by book language
        const val TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET =
            "app.crengine.textlang.fallback.main.lang.autoset"

        // Action of startup: 0 - open last book, 1 - open library
        const val STARTUP_ACTION = "app.startup.action"

        // Day/Night bookmarks/comments colors
        const val HIGHLIGHT_SELECTION_COLOR_DAY = "crengine.highlight.selection.color.day"
        const val HIGHLIGHT_BOOKMARK_COLOR_COMMENT_DAY =
            "crengine.highlight.bookmarks.color.comment.day"
        const val HIGHLIGHT_BOOKMARK_COLOR_CORRECTION_DAY =
            "crengine.highlight.bookmarks.color.correction.day"
        const val HIGHLIGHT_SELECTION_COLOR_NIGHT = "crengine.highlight.selection.color.night"
        const val HIGHLIGHT_BOOKMARK_COLOR_COMMENT_NIGHT =
            "crengine.highlight.bookmarks.color.comment.night"
        const val HIGHLIGHT_BOOKMARK_COLOR_CORRECTION_NIGHT =
            "crengine.highlight.bookmarks.color.correction.night"

        // available options for SELECTION_ACTION setting
        const val SELECTION_ACTION_TOOLBAR = 0
        const val SELECTION_ACTION_COPY = 1
        const val SELECTION_ACTION_DICTIONARY = 2
        const val SELECTION_ACTION_BOOKMARK = 3
        const val SELECTION_ACTION_FIND = 4

        // available options for SECONDARY_TAP_ACTION_TYPE setting
        const val TAP_ACTION_TYPE_LONGPRESS = 0
        const val TAP_ACTION_TYPE_DOUBLE = 1
        const val TAP_ACTION_TYPE_SHORT = 2

        // available options for TOOLBAR_LOCATION setting
        const val TOOLBAR_LOCATION_NONE = 0
        const val TOOLBAR_LOCATION_TOP = 1
        const val TOOLBAR_LOCATION_BOTTOM = 2

        // available options for STARTUP_ACTION
        const val STARTUP_ACTION_LAST_BOOK = 0
        const val STARTUP_ACTION_LIBRARY = 1
    }

    // per document properties
    object Document {
        // Allow to use embedded into document styles: boolean
        const val PROP_PER_DOC_EMBEDDED_STYLES = "app.per-doc.crengine.doc.embedded.styles.enabled"

        // Allow to use embedded into document fonts (epub): boolean
        const val PROP_PER_DOC_EMBEDDED_FONTS = "app.per-doc.crengine.doc.embedded.fonts.enabled"

        // Text format options: boolean: true - preformatted text, false - auto format (reflow)
        const val PROP_PER_DOC_TXT_OPTION_PREFORMATTED =
            "app.per-doc.crengine.file.txt.preformatted"

        // Block rendering flags: integer
        const val PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS =
            "app.per-doc.crengine.render.block.rendering.flags"

        // Requested DOM version: integer
        const val PROP_PER_DOC_REQUESTED_DOM_VERSION =
            "app.per-doc.crengine.render.requested_dom_version"
    }

    // crengine-ng properties
    object Engine {
        // Font smoothing gamma: float, currently supported: 0.30..4.00, see crengine/src/lvfont/lvgammatbl.c
        const val PROP_FONT_GAMMA = "font.gamma"

        // Font antialiasing: 0 - none, 1 - only big (gray), 2 - all (gray), 3 - all (gray), 4 - LCD RGB, 5 - LCD BGR, 8 - LCD RGB (vertical), 9 - LCD BGR (vertical)
        const val PROP_FONT_ANTIALIASING = "font.antialiasing.mode"

        // Font hinting: 0 - disabled, 1 - bytecode, 2 - autohinting
        const val PROP_FONT_HINTING = "font.hinting.mode"

        // Text shaping type: integer, 0 - simple shaping using FreeType only, 1 - Light HarfBuzz shaping without ligatures, 2 - HarfBuzz shaping with ligatures, RTL
        const val PROP_FONT_SHAPING = "font.shaping.mode"

        // Font kerning: boolean, 0 - disabled, 1 - enabled
        const val PROP_FONT_KERNING_ENABLED = "font.kerning.enabled"

        // Font color: integer, no alpha
        const val PROP_FONT_COLOR = "font.color.default"

        // Font Face Name: string
        const val PROP_FONT_FACE = "font.face.default"

        // Base font weight: 100, 200, 300, 400, 500, 600, 700, 800, 900
        const val PROP_FONT_BASE_WEIGHT = "font.face.base.weight"

        // Background color: integer, no alpha
        const val PROP_BACKGROUND_COLOR = "background.color.default"

        // Text format options: boolean
        const val PROP_TXT_OPTION_PREFORMATTED = "crengine.file.txt.preformatted"

        // Font size in pixels: integer
        const val PROP_FONT_SIZE = "crengine.font.size"

        // Fallback fonts list: string with a list of font faces separated by ';'
        const val PROP_FALLBACK_FONT_FACES = "crengine.font.fallback.faces"

        // Running title (status bar) text color: integer, no alpha
        const val PROP_STATUS_FONT_COLOR = "crengine.page.header.font.color"

        // Running title (status bar) Font Face: string
        const val PROP_STATUS_FONT_FACE = "crengine.page.header.font.face"

        // Running title (status bar) font size: integer
        const val PROP_STATUS_FONT_SIZE = "crengine.page.header.font.size"

        // Top page margin: integer
        const val PROP_PAGE_MARGIN_TOP = "crengine.page.margin.top"

        // Bottom page margin: integer
        const val PROP_PAGE_MARGIN_BOTTOM = "crengine.page.margin.bottom"

        // Left page margin: integer
        const val PROP_PAGE_MARGIN_LEFT = "crengine.page.margin.left"

        // Right page margin: integer
        const val PROP_PAGE_MARGIN_RIGHT = "crengine.page.margin.right"

        // View mode type: 0 - one canvas with per-pixel scrolling (scroll), 1 - page by page (pages)
        const val PROP_PAGE_VIEW_MODE = "crengine.page.view.mode"

        // Interline spacing: integer, in percents, 100% means default spacing
        const val PROP_INTERLINE_SPACE = "crengine.interline.space"

        // Allow to use embedded into document styles: boolean
        const val PROP_EMBEDDED_STYLES = "crengine.doc.embedded.styles.enabled"

        // Allow to use embedded into document fonts (epub): boolean
        const val PROP_EMBEDDED_FONTS = "crengine.doc.embedded.fonts.enabled"

        // Force page breaks on non-linear fragments (epub)
        const val PROP_NONLINEAR_PAGEBREAK = "crengine.doc.nonlinear.pagebreak.force"

        // Show footnotes at the bottom of the page: boolean
        const val PROP_FOOTNOTES = "crengine.footnotes"

        // Running title (status bar) position (only in page view mode): 0 - hide, 1 - page header, 2 - page footer
        const val PROP_STATUS_LINE = "window.status.line"

        // Show current time in running title: boolean
        const val PROP_SHOW_TIME = "window.status.clock"

        // Show current time in running title in 12 hours format: boolean
        const val PROP_SHOW_TIME_12HOURS = "window.status.clock.12hours"

        // Show book title in running title: boolean
        const val PROP_SHOW_TITLE = "window.status.title"

        // Show chapter marks in running title: boolean
        const val PROP_STATUS_CHAPTER_MARKS = "crengine.page.header.chapter.marks"

        // Show read position as a percentage in running title: boolean
        const val PROP_SHOW_POS_PERCENT = "window.status.pos.percent"

        // Show total number of pages in running title: boolean
        const val PROP_SHOW_PAGE_COUNT = "window.status.pos.page.count"

        // Show current reading page number in running title: boolean
        const val PROP_SHOW_PAGE_NUMBER = "window.status.pos.page.number"

        // Show battery level in running title: boolean
        const val PROP_SHOW_BATTERY = "window.status.battery"

        // Show battery level in percents (if enabled) in running title: boolean
        const val PROP_SHOW_BATTERY_PERCENT = "window.status.battery.percent"

        // Visible page count in landscape mode: integer, only 1 or 2.
        const val PROP_LANDSCAPE_PAGES = "window.landscape.pages"

        // Generic font families font faces
        const val PROP_GENERIC_SERIF_FONT_FACE = "crengine.generic.serif.font.face"
        const val PROP_GENERIC_SANS_SERIF_FONT_FACE = "crengine.generic.sans-serif.font.face"
        const val PROP_GENERIC_CURSIVE_FONT_FACE = "crengine.generic.cursive.font.face"
        const val PROP_GENERIC_FANTASY_FONT_FACE = "crengine.generic.fantasy.font.face"
        const val PROP_GENERIC_MONOSPACE_FONT_FACE = "crengine.generic.monospace.font.face"

        // Hyphenation dictionary id (for legacy rendering only): boolean
        const val PROP_HYPHENATION_DICT = "crengine.hyphenation.directory"

        // Allow to use soft hyphenation char for hyphenation: boolean
        const val PROP_HYPHENATION_TRUST_SOFT_HYPHENS = "crengine.hyphenation.trust.soft.hyphens"

        // Main language for hyphenation (for enhanced rendering only): string, language tag
        const val PROP_TEXTLANG_MAIN_LANG = "crengine.textlang.main.lang"

        // Support for several different languages in a document for hyphenation (for enhanced rendering only): boolean
        const val PROP_TEXTLANG_EMBEDDED_LANGS_ENABLED = "crengine.textlang.embedded.langs.enabled"

        // Allow to hyphenate words (for enhanced rendering only): boolean
        const val PROP_TEXTLANG_HYPHENATION_ENABLED = "crengine.textlang.hyphenation.enabled"

        // Use only soft hyphenation chars for hyphenation (for enhanced rendering only): boolean
        const val PROP_TEXTLANG_HYPH_SOFT_HYPHENS_ONLY =
            "crengine.textlang.hyphenation.soft.hyphens.only"

        // Force use only algorithmic hyphenation (for enhanced rendering only): boolean
        const val PROP_TEXTLANG_HYPH_FORCE_ALGORITHMIC =
            "crengine.textlang.hyphenation.force.algorithmic"

        // Use floating punctuation: boolean
        const val PROP_FLOATING_PUNCTUATION = "crengine.style.floating.punctuation.enabled"

        // Space width scaling (percentage): integer, minimum - 10%, maximum - 500%, default - 100%
        const val PROP_FORMAT_SPACE_WIDTH_SCALE_PERCENT = "crengine.style.space.width.scale.percent"

        // Minimum space width (percentage), (how much can the gap be reduced): integer, minimum - 25%, maximum - 100%, default - 50%
        const val PROP_FORMAT_MIN_SPACE_CONDENSING_PERCENT =
            "crengine.style.space.condensing.percent"

        // % of unused space on a line to trigger hyphenation, or addition of letter spacing for justification
        //  integer: minimum - 0%, maximum - 20%, default - 5%
        const val PROP_FORMAT_UNUSED_SPACE_THRESHOLD_PERCENT =
            "crengine.style.unused.space.threshold.percent"

        // Max allowed added letter spacing (% of font size)
        //  integer: minimum - 0%, maximum - 20%, default - 0%
        const val PROP_FORMAT_MAX_ADDED_LETTER_SPACING_PERCENT =
            "crengine.style.max.added.letter.spacing.percent"

        // Rendering DPI: integer, default is 96 (1 css px = 1 screen px)
        // use 0 for old crengine behaviour (no support for absolute units and 1css px = 1 screen px)
        const val PROP_RENDER_DPI = "crengine.render.dpi"

        // Scale font size for rendering DPI: boolean, false by default
        //  don't applicable if required convert fractional font size in pt to px, for example '7.2pt', since font size is always integer
        const val PROP_RENDER_SCALE_FONT_WITH_DPI = "crengine.render.scale.font.with.dpi"

        // Block rendering flags: integer
        const val PROP_RENDER_BLOCK_RENDERING_FLAGS = "crengine.render.block.rendering.flags"

        // Requested DOM version: integer
        const val PROP_REQUESTED_DOM_VERSION = "crengine.render.requested_dom_version"

        // Enable cache file contents validation: boolean, enabled by default
        const val PROP_CACHE_VALIDATION_ENABLED = "crengine.cache.validation.enabled"

        // Minimum file size when no cache is used: integer
        const val PROP_MIN_FILE_SIZE_TO_CACHE = "crengine.cache.filesize.min"

        // Minimum file size for which caching is forced to be disabled: integer, obsolete
        const val PROP_FORCED_MIN_FILE_SIZE_TO_CACHE = "crengine.cache.forced.filesize.min"

        // Bookmarks highlight mode: integer, 0 - none, 1 - solid color, 2 - underline
        const val PROP_HIGHLIGHT_COMMENT_BOOKMARKS = "crengine.highlight.bookmarks"

        // Selection color: integer, default 0xC0C0C0
        const val PROP_HIGHLIGHT_SELECTION_COLOR = "crengine.highlight.selection.color"

        // Bookmark (type=comment) color: integer, default 0xA08020
        const val PROP_HIGHLIGHT_BOOKMARK_COLOR_COMMENT =
            "crengine.highlight.bookmarks.color.comment"

        // Bookmark (type=correction) color: integer, default 0xA04040
        const val PROP_HIGHLIGHT_BOOKMARK_COLOR_CORRECTION =
            "crengine.highlight.bookmarks.color.correction"

        // image scaling settings
        // mode: 0=disabled, 1=integer scaling factors, 2=free scaling
        // scale: 0=auto based on font size, 1=no zoom, 2=scale up to *2, 3=scale up to *3
        const val PROP_IMG_SCALING_ZOOMIN_INLINE_MODE = "crengine.image.scaling.zoomin.inline.mode"
        const val PROP_IMG_SCALING_ZOOMIN_INLINE_SCALE =
            "crengine.image.scaling.zoomin.inline.scale"
        const val PROP_IMG_SCALING_ZOOMOUT_INLINE_MODE =
            "crengine.image.scaling.zoomout.inline.mode"
        const val PROP_IMG_SCALING_ZOOMOUT_INLINE_SCALE =
            "crengine.image.scaling.zoomout.inline.scale"
        const val PROP_IMG_SCALING_ZOOMIN_BLOCK_MODE = "crengine.image.scaling.zoomin.block.mode"
        const val PROP_IMG_SCALING_ZOOMIN_BLOCK_SCALE = "crengine.image.scaling.zoomin.block.scale"
        const val PROP_IMG_SCALING_ZOOMOUT_BLOCK_MODE = "crengine.image.scaling.zoomout.block.mode"
        const val PROP_IMG_SCALING_ZOOMOUT_BLOCK_SCALE =
            "crengine.image.scaling.zoomout.block.scale"
    }
}
