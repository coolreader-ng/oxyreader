/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Canvas
import android.graphics.Rect

internal class PageFlipAnimation constructor(
    dir: Int,
    page1: PageImageCache,
    page2: PageImageCache
) : AbstractAnimation() {

    private val mPage1: PageImageCache = page1
    private val mPage2: PageImageCache = page2
    private var mPos = 0
    private var mSpeed = 50f
    private val mSrcRect1 = Rect()
    private val mDstRect1 = Rect()
    private val mSrcRect2 = Rect()
    private val mDstRect2 = Rect()
    private var mDone: Boolean = false

    override val direction: Int = dir

    override val isAnimationComplete
        get() = mDone

    override val position: Int
        get() = if (direction > 0) mPos else -mPos

    override fun setSpeed(speed: Float) {
        mSpeed = speed
    }

    override fun autoIncrementPosition(acceleration: Float) {
        // TODO: use a physical model of smooth acceleration/deceleration
        if (mDone)
            return
        mPos = (mPos.toFloat() + mSpeed).toInt()
        if (mPos >= mPage1.position!!.pageWidth) {
            mPos = mPage1.position!!.pageWidth - 1
            mDone = true
        }
    }

    override fun autoDecrementPosition(acceleration: Float) {
        // TODO: use a physical model of smooth acceleration/deceleration
        if (mDone) return
        mPos = (mPos.toFloat() - mSpeed).toInt()
        if (mPos < 0) {
            mPos = 0
            mDone = true
        }
    }

    override fun overridePosition(x: Int, y: Int) {
        if (direction > 0)
            mPos = -x
        else
            mPos = x
        if (mPos < 0)
            mPos = 0
        else if (mPos >= mPage1.position!!.pageWidth)
            mPos = mPage1.position!!.pageWidth
    }

    override fun resetPosition() {
        mPos = 0
    }

    override fun renderFrame(canvas: Canvas): Boolean {
        if (mPage1.isReleased || mPage2.isReleased) return false
        if (direction > 0) {
            // from right to left
            mSrcRect1.set(mPos, 0, mPage1.position!!.pageWidth, mPage1.position!!.pageHeight)
            mDstRect1.set(0, 0, mPage1.position!!.pageWidth - mPos, mPage1.position!!.pageHeight)
            mSrcRect2.set(0, 0, mPos, mPage2.position!!.pageHeight)
            mDstRect2.set(
                mPage2.position!!.pageWidth - mPos,
                0,
                mPage2.position!!.pageWidth,
                mPage2.position!!.pageHeight
            )
        } else {
            // from left to right
            mSrcRect1.set(0, 0, mPage1.position!!.pageWidth - mPos, mPage1.position!!.pageHeight)
            mDstRect1.set(mPos, 0, mPage1.position!!.pageWidth, mPage1.position!!.pageHeight)
            mSrcRect2.set(
                mPage2.position!!.pageWidth - mPos,
                0,
                mPage2.position!!.pageWidth,
                mPage2.position!!.pageHeight
            )
            mDstRect2.set(0, 0, mPos, mPage2.position!!.pageHeight)
        }
        if (mSrcRect1.width() > 0)
            canvas.drawBitmap(mPage1.bitmap!!, mSrcRect1, mDstRect1, null)
        if (mSrcRect2.width() > 0)
            canvas.drawBitmap(mPage2.bitmap!!, mSrcRect2, mDstRect2, null)
        return true
    }
}
