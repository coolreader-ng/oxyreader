/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.util.Log

class SRLog private constructor(tag: String) {
    private var mTag: String = tag

    fun debug(msg: String): Int {
        return println(Log.DEBUG, mTag, msg);
    }

    fun debug(msg: String?, tr: Throwable?): Int {
        return println(Log.DEBUG, mTag, "$msg\n$tr");
    }

    fun error(msg: String): Int {
        return println(Log.ERROR, mTag, msg);
    }

    fun error(msg: String?, tr: Throwable?): Int {
        return println(Log.ERROR, mTag, "$msg\n$tr");
    }

    fun info(msg: String): Int {
        return println(Log.INFO, mTag, msg);
    }

    fun info(msg: String?, tr: Throwable?): Int {
        return println(Log.INFO, mTag, "$msg\n$tr");
    }

    fun verbose(msg: String): Int {
        return println(Log.VERBOSE, mTag, msg);
    }

    fun verbose(msg: String?, tr: Throwable?): Int {
        return println(Log.VERBOSE, mTag, "$msg\n$tr");
    }

    fun warn(msg: String): Int {
        return println(Log.WARN, mTag, msg);
    }

    fun warn(msg: String?, tr: Throwable?): Int {
        return println(Log.WARN, mTag, "$msg\n$tr");
    }

    companion object {
        private fun println(priority: Int, tag: String, msg: String): Int {
            return Log.println(priority, tag, msg)
        }

        fun create(tag: String): SRLog {
            return SRLog(tag);
        }

    }

}
