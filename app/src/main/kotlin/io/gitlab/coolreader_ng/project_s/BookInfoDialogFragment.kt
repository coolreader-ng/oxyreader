/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.DialogFragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.progressindicator.LinearProgressIndicator
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.extensions.inFullscreenWindow
import kotlin.math.roundToInt

class BookInfoDialogFragment(
    private var bookInfo: BookInfo? = null,
    internal var bookCoverManager: BookCoverManager? = null
) :
    DialogFragment() {

    interface OnActionsListener {
        fun onOpenBook(bookInfo: BookInfo)
        fun onInfoChanged(bookInfo: BookInfo)
    }

    var onActionsListener: OnActionsListener? = null

    private var mShowAsDialog = false

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mShowAsDialog = arguments?.getBoolean("asDialog") ?: false
        val toolbarLocation =
            arguments?.getInt("toolbar", PropNames.App.TOOLBAR_LOCATION_NONE)
                ?: PropNames.App.TOOLBAR_LOCATION_NONE

        val contentView = inflater.inflate(R.layout.bookinfo_panel, container, false)

        val appBarLayout = contentView.findViewById<AppBarLayout>(R.id.appBarLayout)
        val topAppBar = contentView.findViewById<MaterialToolbar>(R.id.topAppBar)
        val nestedScrollView = contentView.findViewById<NestedScrollView>(R.id.nestedScrollView)
        val bottomAppBar = contentView.findViewById<BottomAppBar>(R.id.bottomAppBar)
        val bookCover = contentView.findViewById<ImageView>(R.id.imgBookCover)
        val rgStatus = contentView.findViewById<RadioGroup>(R.id.rgStatus)
        val rbStatusNone = contentView.findViewById<RadioButton>(R.id.rbStatusNone)
        val rbStatusInReading = contentView.findViewById<RadioButton>(R.id.rbStatusInReading)
        val rbStatusFinished = contentView.findViewById<RadioButton>(R.id.rbStatusFinished)
        val rbStatusPlanned = contentView.findViewById<RadioButton>(R.id.rbStatusPlanned)
        val readingProgress =
            contentView.findViewById<LinearProgressIndicator>(R.id.readingProgress)
        val bookRating = contentView.findViewById<RatingBar>(R.id.bookRating)
        val titleTextEdit = contentView.findViewById<TextInputEditText>(R.id.titleTextEdit)
        val authorsLayout = contentView.findViewById<LinearLayout>(R.id.authorsLayout)
        val author1TextLayout = contentView.findViewById<TextInputLayout>(R.id.author1TextLayout)
        val author1TextEdit = contentView.findViewById<TextInputEditText>(R.id.author1TextEdit)
        val seriesTextEdit = contentView.findViewById<TextInputEditText>(R.id.seriesTextEdit)
        val serialNumTextEdit = contentView.findViewById<TextInputEditText>(R.id.serialNumTextEdit)
        val languageTextEdit = contentView.findViewById<TextInputEditText>(R.id.languageTextEdit)
        val subjectsLabel = contentView.findViewById<TextView>(R.id.subjectsLabel)
        val subjectsLayout = contentView.findViewById<LinearLayout>(R.id.subjectsLayout)
        val subject1TextLayout = contentView.findViewById<TextInputLayout>(R.id.subject1TextLayout)
        val subject1TextEdit = contentView.findViewById<TextInputEditText>(R.id.subject1TextEdit)
        val annotationTextEdit =
            contentView.findViewById<TextInputEditText>(R.id.annotationTextEdit)
        val locationTextEdit = contentView.findViewById<TextInputEditText>(R.id.locationTextEdit)

        val a: TypedArray = requireContext().obtainStyledAttributes(
            intArrayOf(android.R.attr.actionBarSize)
        )
        val bottomToolbarSize = a.getDimensionPixelSize(0, 0)
        a.recycle()

        when (toolbarLocation) {
            PropNames.App.TOOLBAR_LOCATION_NONE -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.VISIBLE
                nestedScrollView.setPadding(0, 0, 0, bottomToolbarSize)
            }

            else -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.GONE
            }
        }
        Utils.applyToolBarColorForMenu(requireContext(), topAppBar.menu)
        Utils.applyToolBarColorForMenu(requireContext(), bottomAppBar.menu)

        bookInfo?.let { bookInfo ->
            // Book cover
            bookCoverManager?.let {
                val maxWidth = Utils.convertUnits(200f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                val maxHeight =
                    Utils.convertUnits(300f, Utils.Units.DP, Utils.Units.PX).roundToInt()
                it.getBookCoverDrawable(
                    bookInfo,
                    maxWidth,
                    maxHeight,
                    object : BookCoverManager.BookCoverDrawableResultCallback {
                        override fun onResult(drawable: Drawable?) {
                            BackgroundThread.postGUI {
                                bookCover.setImageDrawable(drawable)
                            }
                        }
                    })
            }

            // Reading status
            when (bookInfo.readingStatus) {
                BookInfo.ReadingStatus.NONE -> rgStatus.check(R.id.rbStatusNone)
                BookInfo.ReadingStatus.IN_READING -> rgStatus.check(R.id.rbStatusInReading)
                BookInfo.ReadingStatus.DONE -> rgStatus.check(R.id.rbStatusFinished)
                BookInfo.ReadingStatus.PLANNED -> rgStatus.check(R.id.rbStatusPlanned)
            }
            // Reading progress
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                readingProgress.min = 0
            }
            readingProgress.max = 100
            var progress = 0
            bookInfo.lastPosition?.let {
                progress = (it.percent.toFloat() / 100f).roundToInt()
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                readingProgress.setProgress(progress, true)
            else
                readingProgress.progress = progress
            // Rating
            if (bookInfo.rating in 0..5)
                bookRating.rating = bookInfo.rating.toFloat()
            else
                bookRating.rating = 0f
            // Title
            titleTextEdit.setText(bookInfo.title)
            // Authors
            var authorEdit = author1TextEdit
            bookInfo.authors?.let { authors ->
                val layoutParams =
                    LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                val iter = authors.iterator()
                while (iter.hasNext()) {
                    val author = iter.next()
                    authorEdit.setText(author)
                    if (iter.hasNext()) {
                        val nextAuthorInputLayout =
                            layoutInflater.inflate(
                                R.layout.md_textfield,
                                authorsLayout,
                                false
                            ) as TextInputLayout
                        nextAuthorInputLayout.setHint(R.string.author)
                        val nextAuthorTextEdit = nextAuthorInputLayout.editText as TextInputEditText
                        nextAuthorTextEdit.inputType = EditorInfo.TYPE_NULL
                        authorsLayout.addView(nextAuthorInputLayout, layoutParams)
                        authorEdit = nextAuthorTextEdit
                    }
                }
            }
            // Series
            bookInfo.series?.let { series ->
                seriesTextEdit.setText(series)
                bookInfo.seriesNumber?.let { serialNumTextEdit.setText(it.toString()) }
            }
            // Book language
            bookInfo.language?.let { langTag ->
                val language = CREngineNGBinding.getHumanReadableLocaleName(langTag)
                languageTextEdit.setText(language)
            }
            // Subjects/Genres
            val isFB2 = bookInfo.format == DocumentFormat.FB2
            if (isFB2) {
                subjectsLabel.setText(R.string.genres)
                subject1TextLayout.setHint(R.string.genre)
            }
            var subjectEdit = subject1TextEdit
            var subjectEditAvailable = true
            val layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            bookInfo.genresIds?.let { genresIds ->
                val genresCollection = GenresCollection.getInstance(requireContext())
                val iter = genresIds.iterator()
                while (iter.hasNext()) {
                    val genre = genresCollection.byId(iter.next())
                    genre?.let {
                        val genreName = genresCollection.translate(it.code)
                        subjectEdit.setText(genreName)
                        subjectEditAvailable = false
                        if (iter.hasNext()) {
                            val nextSubjectInputLayout =
                                layoutInflater.inflate(
                                    R.layout.md_textfield,
                                    subjectsLayout,
                                    false
                                ) as TextInputLayout
                            nextSubjectInputLayout.setHint(R.string.genre)
                            val nextSubjectTextEdit =
                                nextSubjectInputLayout.editText as TextInputEditText
                            nextSubjectTextEdit.inputType = EditorInfo.TYPE_NULL
                            subjectsLayout.addView(nextSubjectInputLayout, layoutParams)
                            subjectEdit = nextSubjectTextEdit
                        }
                    }
                }
            }
            bookInfo.unsupportedGenres?.let { unGenres ->
                if (!subjectEditAvailable) {
                    // Create new TextField for unsupported genres (TextInputLayout & EditText)
                    val nextSubjectInputLayout =
                        layoutInflater.inflate(
                            R.layout.md_textfield,
                            subjectsLayout,
                            false
                        ) as TextInputLayout
                    nextSubjectInputLayout.setHint(R.string.genre)
                    val nextSubjectTextEdit = nextSubjectInputLayout.editText as TextInputEditText
                    nextSubjectTextEdit.inputType = EditorInfo.TYPE_NULL
                    subjectsLayout.addView(nextSubjectInputLayout, layoutParams)
                    subjectEdit = nextSubjectTextEdit
                    subjectEditAvailable = true
                }
                val iter = unGenres.iterator()
                while (iter.hasNext()) {
                    val unGenre = iter.next()
                    subjectEdit.setText(unGenre)
                    subjectEditAvailable = false
                    if (iter.hasNext()) {
                        val nextSubjectInputLayout =
                            layoutInflater.inflate(
                                R.layout.md_textfield,
                                subjectsLayout,
                                false
                            ) as TextInputLayout
                        nextSubjectInputLayout.setHint(R.string.genre)
                        val nextSubjectTextEdit =
                            nextSubjectInputLayout.editText as TextInputEditText
                        nextSubjectTextEdit.inputType = EditorInfo.TYPE_NULL
                        subjectsLayout.addView(nextSubjectInputLayout, layoutParams)
                        subjectEdit = nextSubjectTextEdit
                    }
                }
            }
            bookInfo.keywords?.let { keywords ->
                if (!subjectEditAvailable) {
                    // Create new TextField for unsupported genres (TextInputLayout & EditText)
                    val nextSubjectInputLayout =
                        layoutInflater.inflate(
                            R.layout.md_textfield,
                            subjectsLayout,
                            false
                        ) as TextInputLayout
                    nextSubjectInputLayout.setHint(R.string.subject)
                    val nextSubjectTextEdit = nextSubjectInputLayout.editText as TextInputEditText
                    nextSubjectTextEdit.inputType = EditorInfo.TYPE_NULL
                    subjectsLayout.addView(nextSubjectInputLayout, layoutParams)
                    subjectEdit = nextSubjectTextEdit
                    subjectEditAvailable = true
                }
                val iter = keywords.iterator()
                while (iter.hasNext()) {
                    val keyword = iter.next()
                    subjectEdit.setText(keyword)
                    if (iter.hasNext()) {
                        val nextSubjectInputLayout =
                            layoutInflater.inflate(
                                R.layout.md_textfield,
                                subjectsLayout,
                                false
                            ) as TextInputLayout
                        nextSubjectInputLayout.setHint(R.string.subject)
                        val nextSubjectTextEdit =
                            nextSubjectInputLayout.editText as TextInputEditText
                        nextSubjectTextEdit.inputType = EditorInfo.TYPE_NULL
                        subjectsLayout.addView(nextSubjectInputLayout, layoutParams)
                        subjectEdit = nextSubjectTextEdit
                    }
                }
            }
            // Annotation & other
            bookInfo.description?.let {
                annotationTextEdit.setText(it)
            }
            // File location
            if (Utils.isBookCopiedToInternalAppStorage(requireContext(), bookInfo))
                locationTextEdit.setText(R.string.internal_application_storage)
            else
                locationTextEdit.setText(bookInfo.fileInfo.basePathWA)
        }

        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val mask = if (inFullscreenWindow)
                WindowInsetsCompat.Type.displayCutout()
            else
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            val insets = windowInsets.getInsets(mask)
            var nsInsets = insets
            if (appBarLayout.isVisible) {
                topAppBar.updatePadding(left = insets.left, right = insets.right, top = insets.top)
                nsInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
            } else if (bottomAppBar.isVisible) {
                bottomAppBar.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    bottom = insets.bottom
                )
                nsInsets = Insets.of(
                    insets.left,
                    insets.top,
                    insets.right,
                    bottomToolbarSize + insets.bottom
                )
            }
            nestedScrollView.updatePadding(
                nsInsets.left,
                nsInsets.top,
                nsInsets.right,
                nsInsets.bottom
            )
            WindowInsetsCompat.CONSUMED
        }

        // Callbacks
        val closeAction: () -> Unit = {
            if (mShowAsDialog) {
                dialog?.dismiss()
            } else {
                if (parentFragmentManager.backStackEntryCount > 0)
                    parentFragmentManager.popBackStack()
            }
        }
        topAppBar.setNavigationOnClickListener {
            closeAction()
        }
        bottomAppBar.setNavigationOnClickListener {
            closeAction()
        }
        val onMenuItemClickListener = Toolbar.OnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.open -> {
                    bookInfo?.let { onActionsListener?.onOpenBook(it) }
                    closeAction()
                    true
                }

                else -> false
            }
        }
        topAppBar.setOnMenuItemClickListener(onMenuItemClickListener)
        bottomAppBar.setOnMenuItemClickListener(onMenuItemClickListener)
        rgStatus.setOnCheckedChangeListener { _, checkedId ->
            val newStatus = when (checkedId) {
                R.id.rbStatusNone -> BookInfo.ReadingStatus.NONE
                R.id.rbStatusInReading -> BookInfo.ReadingStatus.IN_READING
                R.id.rbStatusFinished -> BookInfo.ReadingStatus.DONE
                R.id.rbStatusPlanned -> BookInfo.ReadingStatus.PLANNED
                else -> BookInfo.ReadingStatus.NONE
            }
            bookInfo?.let {
                if (newStatus != it.readingStatus) {
                    it.readingStatus = newStatus
                    onActionsListener?.onInfoChanged(it)
                }
            }
        }
        val ratingTapHandler = Handler(Looper.getMainLooper())
        val resetRatingRunnable = Runnable {
            bookInfo?.let {
                if (it.rating != 0) {
                    bookRating.rating = 0f
                    it.rating = 0
                    onActionsListener?.onInfoChanged(it)
                }
            }
        }
        bookRating.setOnRatingBarChangeListener { _, rating, fromUser ->
            if (fromUser) {
                val newRating = rating.roundToInt()
                bookInfo?.let {
                    if (newRating != it.rating) {
                        it.rating = newRating
                        onActionsListener?.onInfoChanged(it)
                    }
                }
            }
            ratingTapHandler.removeCallbacks(resetRatingRunnable)
        }
        bookRating.setOnTouchListener { v, event ->
            if (MotionEvent.ACTION_UP == event.action) {
                ratingTapHandler.postDelayed(resetRatingRunnable, 200)
            }
            return@setOnTouchListener false
        }

        return contentView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        // Without Window.FEATURE_LEFT_ICON for some reason the bottom content of the dialog is cut off
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE or Window.FEATURE_LEFT_ICON)
        // Setting transparent background to remove padding
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (null == bookInfo) {
            bookInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                savedInstanceState?.getParcelable("bookInfo", BookInfo::class.java)
            else
                savedInstanceState?.getParcelable("bookInfo")
        }
        // Note: onCreateView() function will be called later
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        bookInfo?.let { outState.putParcelable("bookInfo", it) }
    }

    override fun onStart() {
        super.onStart()
        if (mShowAsDialog) {
            // Increase dialog window width
            dialog?.let { dialog ->
                val metrics = resources.displayMetrics
                val displayWidth = metrics.widthPixels
                val displayHeight = metrics.heightPixels
                val layoutParams = WindowManager.LayoutParams()
                val oldParams = dialog.window?.attributes
                if (null != oldParams)
                    layoutParams.copyFrom(dialog.window?.attributes)
                else
                    layoutParams.height = 8 * displayHeight / 10
                layoutParams.width = 8 * displayWidth / 10
                dialog.window?.attributes = layoutParams
            }
        }
    }
}
