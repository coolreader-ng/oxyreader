/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Point

@UsedInJNI
class Selection {
    // Coordinates of the left selection lever relative to the window
    @UsedInJNI
    val startHandleWndPos = Point()

    // Coordinates of the right selection lever relative to the window
    @UsedInJNI
    val endHandleWndPos = Point()

    // Coordinates of the left selection lever relative to the document
    @UsedInJNI
    val startHandleDocPos = Point()

    // Coordinates of the right selection lever relative to the document
    @UsedInJNI
    val endHandleDocPos = Point()

    @UsedInJNI
    var startXPtr: String? = null

    @UsedInJNI
    var endXPtr: String? = null

    @UsedInJNI
    var text: String? = null

    @UsedInJNI
    var chapter: String? = null

    @UsedInJNI
    var percent: Int = 0

    val isEmpty: Boolean
        get() = null == startXPtr || null == endXPtr
    val isNotEmpty: Boolean
        get() = null != startXPtr && null != endXPtr

    override fun toString(): String {
        return "Selection[startHandleWndPos=${startHandleWndPos}; endHandleWndPos=${endHandleWndPos}; startHandleDocPos=${startHandleDocPos}; endHandleDocPos=${endHandleDocPos}; startXPtr=${startXPtr}; endXPtr=${endXPtr}; text=${text}; chapter=${chapter}; percent=${percent}]"
    }
}
