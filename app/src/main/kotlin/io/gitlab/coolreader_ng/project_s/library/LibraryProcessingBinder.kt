/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.library

import android.net.Uri
import android.os.Binder
import android.os.Handler

/**
 * Class used for the client Binder.  Because we know this service always
 * runs in the same process as its clients, we don't need to deal with IPC.
 *
 * Most of the functions in this class use callbacks to return a result.
 * We use the caller's thread handle for this, so the callback is executed on the caller's thread.
 */
class LibraryProcessingBinder(val service: LibraryProcessingService) : Binder() {

    enum class ItemStatus {
        Ok,
        Unprocessed,
        UnsupportedType,
        Failed,
        SkippedCopy,
        Excluded
    }

    enum class ErrorStatus {
        None,
        Busy,
        Canceled,
        NoPermissions,
        StreamFailed,
        ReadDataFailed;
    }

    enum class OperationType {
        None,
        FolderCopy,
        FileCopy,
        FolderLink,
        ImportFonts;
    }

    enum class AppStorageType {
        AppLibrary,
        AppFonts
    }

    interface ProcessingProgressCallback {
        fun onStarted()
        fun onItemProcessing(item: String)
        fun onItemProcessed(item: String, status: ItemStatus)
        fun onProgress(percent: Int, addedCount: Int, skippedCount: Int, totalFiles: Int)
        fun onDone()
        fun onError(errorStatus: ErrorStatus)
    }

    interface EnumStorageResultListener {
        fun onResult(data: Collection<String>)
    }

    class ProcessStatus(
        val started: Boolean,
        val completed: Boolean,
        val canceled: Boolean,
        val isBusy: Boolean,
        val currentItem: String,
        val processedItems: Map<String, ItemStatus>,
        val percent: Int,
        val addedCount: Int,
        val skippedCount: Int,
        val totalFiles: Int,
        val lastOperation: OperationType,
        val lastError: ErrorStatus
    ) {
        constructor() : this(
            false,
            false,
            false,
            false,
            "",
            HashMap(),
            0,
            0,
            0,
            0,
            OperationType.None,
            ErrorStatus.None
        )
    }

    fun copyFolder(folderUri: Uri) {
        service.copyFolder(folderUri)
    }

    fun copyFile(fileUri: Uri) {
        service.copyFile(fileUri)
    }

    fun indexFolder(folderPath: String) {
        service.indexFolder(folderPath)
    }

    fun importFonts(folderUri: Uri) {
        service.importFonts(folderUri)
    }

    fun enumAppStorage(
        storageType: AppStorageType,
        listener: EnumStorageResultListener,
        handler: Handler
    ) {
        service.enumAppStorage(storageType, listener, handler)
    }

    fun registerProgressCallback(callback: ProcessingProgressCallback, handler: Handler) {
        service.registerProgressCallback(callback, handler)
    }

    fun unregisterProgressCallback() {
        service.unregisterProgressCallback()
    }

    fun requestStop() {
        service.requestStop()
    }

    fun isBusy(): Boolean {
        return service.isBusy()
    }

    fun getProcessStatus(): ProcessStatus {
        return service.processStatus
    }
}