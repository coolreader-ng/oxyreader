/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.documentfile.provider.DocumentFile
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class SettingsFragmentMain : PreferenceFragmentCompat(),
    SettingsActivity.SettingsFragmentStorageHolder {

    private var mProperties = SRProperties()
    private var mODTSaveLogsLauncher: ActivityResultLauncher<Intent>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mODTSaveLogsLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (Activity.RESULT_OK == it.resultCode) {
                    val uri = it.data?.data
                    log.debug("export log files: folder selected: $uri")
                    uri?.let { folderUri ->
                        var dir: DocumentFile? = null
                        if ("file" == folderUri.scheme) {
                            val path = folderUri.path
                            if (null != path)
                                dir = DocumentFile.fromFile(File(path))
                        }
                        if (null == dir)
                            dir = DocumentFile.fromTreeUri(requireContext(), folderUri)
                        var res = false
                        var fullFileName: String? = null
                        if (null != dir) {
                            val format = SimpleDateFormat(
                                "'lxreader-logs-'yyyy-MM-dd_HH-mm-ss'.zip'",
                                Locale.US
                            )
                            val fileName = format.format(Date())
                            val targetFile = dir.createFile("application/zip", fileName)
                            if (null != targetFile) {
                                fullFileName = Utils.getPseudoFilePath(targetFile)
                                val logsDir = File(requireContext().filesDir, "logs")
                                res = Utils.makeZipArchive(
                                    requireContext().contentResolver,
                                    targetFile,
                                    logsDir
                                )
                                if (res)
                                    log.verbose("zip archive with logs successfully created (${fullFileName})")
                                else
                                    log.error("Failed to write zip archive (${fullFileName})")
                            } else {
                                log.error("Failed to created new file in ${dir}!")
                            }
                        } else {
                            log.error("exporting log files: dir is null!")
                        }
                        val message = if (res)
                            requireContext().getString(
                                R.string.the_archive_with_log_files_was_saved_to_file__s,
                                fullFileName
                            )
                        else
                            requireContext().getString(R.string.failed_to_save_archive_with_log_files_)
                        val toast = Toast.makeText(
                            requireContext(),
                            message,
                            Toast.LENGTH_LONG
                        )
                        toast.show()
                    }
                }
            }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = SettingsActivity.CREPropsDataStorage(mProperties)

        setPreferencesFromResource(R.xml.preferences_main, rootKey)

        // Set entries & entryValues
        // Values must equal to associated crengine-ng properties
        val themePreference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_THEME)
        val themeEntries = arrayOfNulls<String>(SettingsManager.SUPPORTED_THEMES.size)
        val themeEntryValues = arrayOfNulls<String>(SettingsManager.SUPPORTED_THEMES.size)
        for ((i, theme) in SettingsManager.SUPPORTED_THEMES.withIndex()) {
            themeEntryValues[i] = theme.name
            themeEntries[i] = requireContext().getString(theme.nameResId)
        }
        themePreference?.entries = themeEntries
        themePreference?.entryValues = themeEntryValues

        val langPreference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_LANG)
        val langEntries = arrayOfNulls<String>(SettingsManager.SUPPORTED_LANGUAGES.size)
        val langEntryValues = arrayOfNulls<String>(SettingsManager.SUPPORTED_LANGUAGES.size)
        for ((i, lang) in SettingsManager.SUPPORTED_LANGUAGES.withIndex()) {
            langEntryValues[i] = lang.name
            langEntries[i] = requireContext().getString(lang.nameResId)
        }
        langPreference?.entries = langEntries
        langPreference?.entryValues = langEntryValues

        val toolbarPosPreference = findPreference<Preference>(SettingsActivity.PREF_KEY_TOOLBAR_POS)
        toolbarPosPreference?.isVisible = !mProperties.getBool(PropNames.App.FULLSCREEN, false)

        val screenOrientationPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_SCREEN_ORIENTATION)
        val orientationEntries = arrayOfNulls<String>(SettingsManager.SUPPORTED_ORIENTATIONS.size)
        val orientationEntryValues =
            arrayOfNulls<String>(SettingsManager.SUPPORTED_ORIENTATIONS.size)
        for ((i, orientation) in SettingsManager.SUPPORTED_ORIENTATIONS.withIndex()) {
            orientationEntryValues[i] = i.toString()
            orientationEntries[i] = requireContext().getString(orientation.nameResId)
        }
        screenOrientationPreference?.entries = orientationEntries
        screenOrientationPreference?.entryValues = orientationEntryValues

        val fullScreenPreference =
            findPreference<SwitchPreferenceCompat>(SettingsActivity.PREF_KEY_FULLSCREEN)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            fullScreenPreference?.isVisible = true
        fullScreenPreference?.setOnPreferenceChangeListener { _, newValue ->
            toolbarPosPreference?.isVisible = !Utils.parseBool(newValue.toString(), false)
            return@setOnPreferenceChangeListener true
        }

        val pageFlipAnimPreference =
            findPreference<Preference>(SettingsActivity.PREF_KEY_PAGE_FLIP_ANIM_TYPE)
        pageFlipAnimPreference?.isVisible =
            mProperties.getInt(PropNames.Engine.PROP_PAGE_VIEW_MODE, 1) == 1
        val viewModePreference = findPreference<Preference>(SettingsActivity.PREF_KEY_VIEW_MODE)
        viewModePreference?.setOnPreferenceChangeListener { _, newValue ->
            pageFlipAnimPreference?.isVisible = Utils.parseInt(newValue.toString(), 1) == 1
            return@setOnPreferenceChangeListener true
        }

        val tsaDTPreference = findPreference<Preference>(SettingsActivity.PREF_KEY_TSA_DT)
        tsaDTPreference?.isVisible = mProperties.getBool(PropNames.App.DOUBLE_TAP_ACTIONS, false)

        val doubleTapModePreference =
            findPreference<Preference>(SettingsActivity.PREF_KEY_DOUBLE_TAP_MODE)
        doubleTapModePreference?.setOnPreferenceChangeListener { _, newValue ->
            tsaDTPreference?.isVisible = Utils.parseInt(newValue.toString(), 0) == 2
            return@setOnPreferenceChangeListener true
        }

        val tsaLTPreference = findPreference<Preference>(SettingsActivity.PREF_KEY_TSA_LT)
        tsaLTPreference?.isVisible = mProperties.getBool(PropNames.App.LONG_TAP_ACTIONS, false)

        val longTapModePreference =
            findPreference<Preference>(SettingsActivity.PREF_KEY_LONG_TAP_MODE)
        longTapModePreference?.setOnPreferenceChangeListener { _, newValue ->
            tsaLTPreference?.isVisible = Utils.parseInt(newValue.toString(), 0) == 2
            return@setOnPreferenceChangeListener true
        }

        val kaLTPreference = findPreference<Preference>(SettingsActivity.PREF_KEY_KEYS_LP)
        kaLTPreference?.isVisible =
            mProperties.getBool(PropNames.App.KEY_ACTIONS_HANDLE_LONG_PRESS, false)

        val handleLongKeyPressPreference =
            findPreference<Preference>(SettingsActivity.PREF_KEY_HANDLE_LONG_KEY_PRESS)
        handleLongKeyPressPreference?.setOnPreferenceChangeListener { _, newValue ->
            kaLTPreference?.isVisible = Utils.parseBool(newValue.toString(), false)
            return@setOnPreferenceChangeListener true
        }

        val extDictPreference = findPreference<ListPreference>(SettingsActivity.PREF_KEY_DICTIONARY)
        val extDictEntries = Array(ExtDictionaryRegistry.AVAILABLE_DICTIONARIES.size) { i ->
            ExtDictionaryRegistry.AVAILABLE_DICTIONARIES[i].name
        }
        val extDictValues = Array(ExtDictionaryRegistry.AVAILABLE_DICTIONARIES.size) { i ->
            ExtDictionaryRegistry.AVAILABLE_DICTIONARIES[i].id
        }
        extDictPreference?.entries = extDictEntries
        extDictPreference?.entryValues = extDictValues

        val enableLogPreference =
            findPreference<SwitchPreferenceCompat>(SettingsActivity.PREF_KEY_DEBUG_ENABLE_LOG)
        val exportsLogPreference =
            findPreference<Preference>(SettingsActivity.PREF_KEY_DEBUG_EXPORT_LOGS)
        exportsLogPreference?.isEnabled = mProperties.getBool(PropNames.App.LOGGING_ALWAYS, false)
        enableLogPreference?.setOnPreferenceChangeListener { _, newValue ->
            exportsLogPreference?.isEnabled = Utils.parseBool(newValue.toString(), false)
            return@setOnPreferenceChangeListener true
        }
        exportsLogPreference?.setOnPreferenceClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val dialog =
                    MessageDialog(
                        R.string.export_program_log,
                        R.string.on_the_next_screen_select_the_directory_where_you_want_to_save_the_logs
                    )
                dialog.setButton(MessageDialog.StandardButtons.Cancel)
                dialog.setButton(MessageDialog.StandardButtons.Continue) {
                    mODTSaveLogsLauncher?.launch(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE))
                }
                dialog.showDialog(requireContext())
            } else {
                val format = SimpleDateFormat(
                    "'lxreader-logs-'yyyy-MM-dd_HH-mm-ss'.zip'",
                    Locale.US
                )
                val archiveLogName =
                    "${Environment.getExternalStorageDirectory().path}/Download/${format.format(Date())}.zip"
                val logsDir = File(requireContext().filesDir, "logs")
                val res = Utils.makeZipArchive(File(archiveLogName), logsDir)
                val message: String
                if (res) {
                    message = requireContext().getString(
                        R.string.the_archive_with_log_files_was_saved_to_file__s,
                        archiveLogName
                    )
                    log.verbose("zip archive with logs successfully created (${archiveLogName})")
                } else {
                    message =
                        requireContext().getString(R.string.failed_to_save_archive_with_log_files_)
                    log.error("Failed to write zip archive (${archiveLogName})")
                }
                val toast = Toast.makeText(
                    requireContext(),
                    message,
                    Toast.LENGTH_LONG
                )
                toast.show()
            }
            return@setOnPreferenceClickListener true
        }
    }

    override fun setProperties(props: SRProperties) {
        mProperties = props
    }

    override fun resetToDefaults() {
        for ((key, value) in SettingsManager.Defaults.ALL_PREFS) {
            mProperties.setProperty(key, value)
        }
    }

    companion object {
        private val log = SRLog.create("settings.main")
    }
}