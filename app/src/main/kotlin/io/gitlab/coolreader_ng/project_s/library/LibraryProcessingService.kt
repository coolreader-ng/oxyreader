/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s.library

import android.Manifest
import android.app.Notification
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.IBinder
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.documentfile.provider.DocumentFile
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.project_s.AbstractService
import io.gitlab.coolreader_ng.project_s.AbstractTask
import io.gitlab.coolreader_ng.project_s.BackgroundThread
import io.gitlab.coolreader_ng.project_s.BookInfo
import io.gitlab.coolreader_ng.project_s.Bookmark
import io.gitlab.coolreader_ng.project_s.CREngineNGBinding
import io.gitlab.coolreader_ng.project_s.DocumentFormat
import io.gitlab.coolreader_ng.project_s.FileInfo
import io.gitlab.coolreader_ng.project_s.LVDocViewWrapper
import io.gitlab.coolreader_ng.project_s.R
import io.gitlab.coolreader_ng.project_s.ReaderEngineObjectsAccessor
import io.gitlab.coolreader_ng.project_s.SRLog
import io.gitlab.coolreader_ng.project_s.Utils
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import java.io.File
import java.io.FileOutputStream
import java.util.regex.Pattern


class LibraryProcessingService : AbstractService("library") {

    private data class FileItem(
        val fileName: String,
        var status: LibraryProcessingBinder.ItemStatus
    )

    private data class DocfFileItem(
        val docFile: DocumentFile,
        val filePath: String,
        var status: LibraryProcessingBinder.ItemStatus
    )

    private class InternalProcessStatus {
        var started: Boolean
        var completed: Boolean
        var canceled: Boolean
        var currentItem: String
        var processedItems: MutableMap<String, LibraryProcessingBinder.ItemStatus>
        var percent: Int
        var addedCount: Int
        var skippedCount: Int
        var totalFiles: Int
        var lastOperation: LibraryProcessingBinder.OperationType
        var lastError: LibraryProcessingBinder.ErrorStatus

        fun reset() {
            started = false
            completed = false
            canceled = false
            currentItem = ""
            processedItems = HashMap()
            percent = 0
            addedCount = 0
            skippedCount = 0
            totalFiles = 0
            lastOperation = LibraryProcessingBinder.OperationType.None
            lastError = LibraryProcessingBinder.ErrorStatus.None
        }

        init {
            started = false
            completed = false
            canceled = false
            currentItem = ""
            processedItems = HashMap()
            percent = 0
            addedCount = 0
            skippedCount = 0
            totalFiles = 0
            lastOperation = LibraryProcessingBinder.OperationType.None
            lastError = LibraryProcessingBinder.ErrorStatus.None
        }
    }

    // Binder given to clients
    private val mBinder = LibraryProcessingBinder(this)

    private val mLocker = Any()
    private lateinit var mNotificationManagerCompat: NotificationManagerCompat
    private val mDBServiceAccessor = DBServiceAccessor(this)
    private var mBusy = false
    private var mStopRequested = false
    private var mProcessStatus = InternalProcessStatus()
    private var mProgressCallback: LibraryProcessingBinder.ProcessingProgressCallback? = null
    private var mCallbackHandler: Handler? = null

    private val mSyncActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            log.debug("received action: $action")
            if (LIB_ACTION_CANCEL == action) {
                requestStop()
            }
        }
    }


    fun runOn(runnable: Runnable) {
        postRunnable(runnable)
    }

    override fun onCreate() {
        super.onCreate()
        log.debug("library processing service created")
        mDBServiceAccessor.bind()
        mNotificationManagerCompat = NotificationManagerCompat.from(this)
        // Register the channel with the system
        // You can't change the importance or other notification behaviors after this (excluding channel ID change)
        mNotificationManagerCompat.createNotificationChannel(
            NotificationChannelCompat.Builder(
                NOTIFICATION_CHANNEL_ID,
                NotificationManagerCompat.IMPORTANCE_LOW
            )
                .setName("LtReader Library")
                .setDescription("LtReader Library processing")
                .build()
        )
        val filter = IntentFilter()
        filter.addAction(LIB_ACTION_CANCEL)
        ContextCompat.registerReceiver(
            this,
            mSyncActionReceiver,
            filter,
            ContextCompat.RECEIVER_EXPORTED
        )
        loadProcessState()
        mProcessStatus.started = false
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        // If the system kills the service after onStartCommand() returns, recreate the service and call onStartCommand(),
        //  but do not redeliver the last intent.
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onDestroy() {
        log.debug("LibraryProcessingService.Destroy()")
        saveProcessState()
        try {
            unregisterReceiver(mSyncActionReceiver)
        } catch (_: Exception) {
        }
        mDBServiceAccessor.unbind()
        mNotificationManagerCompat.cancel(NOTIFICATION_PROGRESS_ID)
        mNotificationManagerCompat.deleteNotificationChannel(NOTIFICATION_CHANNEL_ID)
        super.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        log.debug("LibraryProcessingService.onLowMemory()")
    }

    // service functions

    internal fun registerProgressCallback(
        callback: LibraryProcessingBinder.ProcessingProgressCallback,
        handler: Handler
    ) {
        mProgressCallback = callback
        mCallbackHandler = handler
    }

    internal fun unregisterProgressCallback() {
        mProgressCallback = null
        mCallbackHandler = null
    }

    internal fun copyFolder(folderUri: Uri) {
        postTask(object : AbstractTask("copyFolder") {
            override fun work() {
                synchronized(mLocker) {
                    mProcessStatus.reset()
                    if (mBusy) {
                        mProgressCallback?.let {
                            mCallbackHandler?.post {
                                it.onError(
                                    LibraryProcessingBinder.ErrorStatus.Busy
                                )
                            }
                        }
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Busy
                        return
                    }
                    mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.FolderCopy
                    mBusy = true
                }
                val pseudoFolderName = "${folderUri.path}"
                // enumerate files in folder
                val files = ArrayList<DocumentFile>()
                try {
                    var dir: DocumentFile? = null
                    if ("file" == folderUri.scheme) {
                        val path = folderUri.path
                        if (null != path)
                            dir = DocumentFile.fromFile(File(path))
                    }
                    if (null == dir)
                        dir = DocumentFile.fromTreeUri(this@LibraryProcessingService, folderUri)
                    if (null != dir) {
                        val enumRes = enumFilesInFolderUri(dir, files)
                        if (!enumRes) {
                            mProgressCallback?.let {
                                mCallbackHandler?.post {
                                    it.onError(
                                        LibraryProcessingBinder.ErrorStatus.NoPermissions
                                    )
                                }
                            }
                            synchronized(mLocker) {
                                mProcessStatus.lastError =
                                    LibraryProcessingBinder.ErrorStatus.NoPermissions
                            }
                        }
                    }
                } catch (e: Exception) {
                    log.error("Failed to create DocumentFile instance: $e")
                    mProgressCallback?.let {
                        mCallbackHandler?.post {
                            it.onError(
                                LibraryProcessingBinder.ErrorStatus.NoPermissions
                            )
                        }
                    }
                    synchronized(mLocker) {
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.NoPermissions
                    }
                }
                log.info("${files.size} files found")
                copyFilesImpl(pseudoFolderName, files)
                synchronized(mLocker) {
                    mBusy = false
                }
            }
        })
    }

    internal fun copyFile(fileUri: Uri) {
        postTask(object : AbstractTask("copyFile") {
            override fun work() {
                synchronized(mLocker) {
                    mProcessStatus.reset()
                    if (mBusy) {
                        mProgressCallback?.let {
                            mCallbackHandler?.post {
                                it.onError(
                                    LibraryProcessingBinder.ErrorStatus.Busy
                                )
                            }
                        }
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Busy
                        return
                    }
                    mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.FileCopy
                    mBusy = true
                }
                val pseudoFileName = "${fileUri.path}"
                val files = ArrayList<DocumentFile>(1)
                try {
                    val file = DocumentFile.fromSingleUri(this@LibraryProcessingService, fileUri)
                    file?.let { files.add(it) }
                } catch (e: Exception) {
                    log.error("Failed to create DocumentFile instance: $e")
                }
                copyFilesImpl(pseudoFileName, files)
                synchronized(mLocker) {
                    mBusy = false
                }
            }
        })
    }

    internal fun indexFolder(folderPath: String) {
        postTask(object : AbstractTask("indexFolder") {
            override fun work() {
                synchronized(mLocker) {
                    mProcessStatus.reset()
                    if (mBusy) {
                        mProgressCallback?.let {
                            mCallbackHandler?.post {
                                it.onError(
                                    LibraryProcessingBinder.ErrorStatus.Busy
                                )
                            }
                        }
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Busy
                        return
                    }
                    mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.FolderLink
                    mBusy = true
                }
                indexFolderImpl(folderPath)
                synchronized(mLocker) {
                    mBusy = false
                }
            }
        })
    }

    internal fun importFonts(folderUri: Uri) {
        postTask(object : AbstractTask("importFonts") {
            override fun work() {
                synchronized(mLocker) {
                    mProcessStatus.reset()
                    if (mBusy) {
                        mProgressCallback?.let {
                            mCallbackHandler?.post {
                                it.onError(
                                    LibraryProcessingBinder.ErrorStatus.Busy
                                )
                            }
                        }
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Busy
                        return
                    }
                    mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.ImportFonts
                    mBusy = true
                }
                val pseudoFolderName = "${folderUri.path}"
                // enumerate files in folder
                val files = ArrayList<DocumentFile>()
                try {
                    var dir: DocumentFile? = null
                    if ("file" == folderUri.scheme) {
                        val path = folderUri.path
                        if (null != path)
                            dir = DocumentFile.fromFile(File(path))
                    }
                    if (null == dir)
                        dir = DocumentFile.fromTreeUri(this@LibraryProcessingService, folderUri)
                    if (null != dir) {
                        val enumRes = enumFilesInFolderUri(dir, files)
                        if (!enumRes) {
                            mProgressCallback?.let {
                                mCallbackHandler?.post {
                                    it.onError(
                                        LibraryProcessingBinder.ErrorStatus.NoPermissions
                                    )
                                }
                            }
                            synchronized(mLocker) {
                                mProcessStatus.lastError =
                                    LibraryProcessingBinder.ErrorStatus.NoPermissions
                            }
                        }
                    }
                } catch (e: Exception) {
                    log.error("Failed to enumerate directory: $e")
                    mProgressCallback?.let {
                        mCallbackHandler?.post {
                            it.onError(
                                LibraryProcessingBinder.ErrorStatus.NoPermissions
                            )
                        }
                    }
                    synchronized(mLocker) {
                        mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.NoPermissions
                    }
                }
                log.info("${files.size} files found")
                importFontsImpl(pseudoFolderName, files)
                synchronized(mLocker) {
                    mBusy = false
                }
            }
        })
    }

    internal fun enumAppStorage(
        storageType: LibraryProcessingBinder.AppStorageType,
        listener: LibraryProcessingBinder.EnumStorageResultListener,
        handler: Handler
    ) {
        postTask(object : AbstractTask("enumAppStorage") {
            override fun work() {
                val dir = when (storageType) {
                    LibraryProcessingBinder.AppStorageType.AppLibrary -> getDir(
                        "books",
                        MODE_PRIVATE
                    )

                    LibraryProcessingBinder.AppStorageType.AppFonts -> File(
                        applicationContext.filesDir,
                        "fonts"
                    )
                }
                val files = ArrayList<DocumentFile>()
                var enumRes = false
                try {
                    val docf = DocumentFile.fromFile(dir)
                    enumRes = enumFilesInFolderUri(docf, files)
                } catch (e: Exception) {
                    log.error("Failed to create DocumentFile instance: $e")
                }
                val data = ArrayList<String>()
                if (enumRes) {
                    for (f in files) {
                        Utils.getPseudoFilePath(f)?.let { data.add(it) }
                    }
                }
                handler.post { listener.onResult(data) }
            }
        })
    }

    internal fun isBusy(): Boolean {
        synchronized(mLocker) {
            return mBusy
        }
    }

    internal val processStatus: LibraryProcessingBinder.ProcessStatus
        get() {
            synchronized(mLocker) {
                return LibraryProcessingBinder.ProcessStatus(
                    mProcessStatus.started,
                    mProcessStatus.completed,
                    mProcessStatus.canceled,
                    mBusy,
                    mProcessStatus.currentItem,
                    mProcessStatus.processedItems,
                    mProcessStatus.percent,
                    mProcessStatus.addedCount,
                    mProcessStatus.skippedCount,
                    mProcessStatus.totalFiles,
                    mProcessStatus.lastOperation,
                    mProcessStatus.lastError
                )
            }
        }

    internal fun requestStop() {
        synchronized(mLocker) {
            mStopRequested = true
        }
    }

    // private implementation

    private fun saveProcessState() {
        log.debug("saving process state")
        val prefs = getSharedPreferences("library", Context.MODE_PRIVATE)
        val editor = prefs.edit()
        editor.putBoolean("state.started", mProcessStatus.started)
        editor.putBoolean("state.completed", mProcessStatus.completed)
        editor.putBoolean("state.canceled", mProcessStatus.canceled)
        editor.putString("state.currentItem", mProcessStatus.currentItem)
        editor.putInt("state.percent", mProcessStatus.percent)
        editor.putInt("state.addedCount", mProcessStatus.addedCount)
        editor.putInt("state.skippedCount", mProcessStatus.skippedCount)
        editor.putInt("state.totalFiles", mProcessStatus.totalFiles)
        editor.putString("state.lastOperation", mProcessStatus.lastOperation.name)
        editor.putString("state.lastError", mProcessStatus.lastError.name)
        var i = 0
        for ((item, status) in mProcessStatus.processedItems) {
            editor.putString("state.item.$i.name", item)
            editor.putString("state.item.$i.status", status.name)
            i++
        }
        editor.apply()
    }

    private fun loadProcessState() {
        val prefs = getSharedPreferences("library", Context.MODE_PRIVATE)
        if (!prefs.contains("state.started")) {
            log.debug("the process state has not yet been saved")
            return
        }
        mProcessStatus.started = prefs.getBoolean("state.started", false)
        mProcessStatus.completed = prefs.getBoolean("state.completed", false)
        mProcessStatus.canceled = prefs.getBoolean("state.canceled", false)
        prefs.getString("state.currentItem", null)?.let { mProcessStatus.currentItem = it }
        mProcessStatus.percent = prefs.getInt("state.percent", 0)
        mProcessStatus.addedCount = prefs.getInt("state.addedCount", 0)
        mProcessStatus.skippedCount = prefs.getInt("state.skippedCount", 0)
        mProcessStatus.totalFiles = prefs.getInt("state.totalFiles", 0)
        prefs.getString("state.lastOperation", null)?.let {
            try {
                mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.valueOf(it)
            } catch (_: Exception) {
                mProcessStatus.lastOperation = LibraryProcessingBinder.OperationType.None
            }
        }
        prefs.getString("state.lastError", null)?.let {
            try {
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.valueOf(it)
            } catch (_: Exception) {
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.None
            }
        }
        var i = 0
        while (prefs.contains("state.item.$i.name")) {
            var item = ""
            var state = LibraryProcessingBinder.ItemStatus.Failed
            prefs.getString("state.item.$i.name", null)?.let { item = it }
            prefs.getString("state.item.$i.status", null)?.let {
                state = try {
                    LibraryProcessingBinder.ItemStatus.valueOf(it)
                } catch (_: Exception) {
                    LibraryProcessingBinder.ItemStatus.Failed
                }
            }
            if (item.isNotEmpty()) {
                mProcessStatus.processedItems[item] = state
            } else {
                break
            }
            i++
        }
    }

    private fun copyFilesImpl(name: String, files: List<DocumentFile>) {
        val allowNotify = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            ActivityCompat.checkSelfPermission(
                this@LibraryProcessingService,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        else true
        val operation =
            if (files.size > 1) LibraryProcessingBinder.OperationType.FolderCopy else LibraryProcessingBinder.OperationType.FileCopy
        if (allowNotify) {
            val notification = buildProgressNotification(name, operation, -1, -1)
            mNotificationManagerCompat.notify(NOTIFICATION_PROGRESS_ID, notification)
        }
        synchronized(mLocker) {
            mProcessStatus.started = true
        }
        mProgressCallback?.let { mCallbackHandler?.post { it.onStarted() } }

        val fileList = ArrayList<DocfFileItem>()
        // TODO: Check each file: if it is an archive, add each archive element to the list
        for (file in files) {
            val itemPath = Utils.getNativeFile(file, true)?.absolutePath ?: run {
                Utils.getPseudoFilePath(file)?.let { "stream:/${it}" } ?: run { "" }
            }
            fileList.add(
                DocfFileItem(
                    file,
                    itemPath,
                    LibraryProcessingBinder.ItemStatus.Unprocessed
                )
            )
        }
        synchronized(mLocker) {
            mProcessStatus.totalFiles = fileList.size
        }

        val exclPatterns = ArrayList<Pattern>().also {
            for (pattern in SCANDIR_IGNORE_FILE_PATTERNS)
                it.add(Pattern.compile(pattern, Pattern.CASE_INSENSITIVE))
        }

        var addedCount = 0
        var skippedCount = 0
        var canceled = false
        var docViewInstance: LVDocViewWrapper? = null
        val sema = Object()
        val contentResolver = applicationContext.contentResolver
        val genresCollection = GenresCollection.getInstance(this@LibraryProcessingService)

        // Split file processing into several parts to reduce memory consumption (for book covers)
        if (fileList.isNotEmpty()) {
            val partsCount = (fileList.size + MAX_PART_SIZE - 1) / MAX_PART_SIZE
            for (partNo in 0 until partsCount) {
                if (canceled)
                    break
                // process each new file
                val bookInfoToSave = ArrayList<BookInfo>()
                val booksToSave = HashMap<BookInfo, DocfFileItem>()
                val duplicatesToRemove = ArrayList<BookInfo>()
                val bookCovers = HashMap<BookInfo, ByteArray>()
                synchronized(sema) {
                    BackgroundThread.postBackground {
                        for (j in 0 until MAX_PART_SIZE) {
                            val i = partNo * MAX_PART_SIZE + j
                            val item = if (i < fileList.size) fileList[i] else break
                            if (item.filePath.isEmpty())
                                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
                            val nativeFile = Utils.getNativeFile(item.docFile, true)
                            val basename = nativeFile?.name ?: run { item.docFile.name ?: "" }
                            // Check if operation canceled, add file to process status
                            synchronized(mLocker) {
                                canceled = mStopRequested
                                mProcessStatus.processedItems[item.filePath] = item.status
                            }
                            if (canceled) {
                                log.info("Canceled")
                                break
                            }
                            // Check file extension & skip unsupported files
                            val docType = DocumentFormat.byExtension(basename)
                            if (null == docType || DocumentFormat.NONE == docType)
                                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
                            for (pattern in exclPatterns) {
                                if (pattern.matcher(basename).matches())
                                    item.status = LibraryProcessingBinder.ItemStatus.Excluded
                            }
                            if (LibraryProcessingBinder.ItemStatus.Unprocessed == item.status) {
                                // Try to open
                                log.debug("Trying to open file ${item.filePath}")
                                synchronized(mLocker) {
                                    mProcessStatus.currentItem = item.filePath
                                }
                                mProgressCallback?.let {
                                    mCallbackHandler?.post {
                                        it.onItemProcessing(
                                            item.filePath
                                        )
                                    }
                                }
                                val fileData = if (null == nativeFile) {
                                    //
                                    // LVDocView requires a stream to be seek able to open,
                                    //  but the external stream from the Android intent is not seek able.
                                    //  So, we copy the data from this input stream into an byte array
                                    // TODO: For big files (for example, >= 100MB)
                                    //  save this stream to temporary file instead of byte array
                                    Utils.readFromDocFile(contentResolver, item.docFile)
                                } else
                                    null
                                if (null != nativeFile || null != fileData) {
                                    val docView = docViewInstance ?: run {
                                        log.debug("Creating LVDocViewWrapper")
                                        docViewInstance =
                                            LVDocViewWrapper().apply { createDocView() }
                                        docViewInstance!!
                                    }
                                    val openRes = if (null != nativeFile)
                                        docView.loadDocument(nativeFile.absolutePath, true)
                                    else if (null != fileData)
                                        docView.loadDocument(fileData, item.filePath, true)
                                    else
                                        false
                                    if (openRes) {
                                        log.debug("  successfully")
                                        val bookInfo = BookInfo(item.filePath)
                                        docView.updateBookInfo(bookInfo, true, genresCollection)
                                        item.status = LibraryProcessingBinder.ItemStatus.Ok
                                        addedCount++
                                        // Add book to save list
                                        booksToSave[bookInfo] = item
                                        // Add book info to save to DB
                                        bookInfoToSave.add(bookInfo)
                                        // Retrieve book cover
                                        docView.getBookCoverData()
                                            ?.let { bookCovers.put(bookInfo, it) }
                                    } else {
                                        log.debug("  failed")
                                        item.status = LibraryProcessingBinder.ItemStatus.Failed
                                        skippedCount++
                                    }
                                } else {
                                    log.debug("${item.filePath}: data read failed")
                                    item.status = LibraryProcessingBinder.ItemStatus.Failed
                                    skippedCount++
                                }
                            } else {
                                skippedCount++
                            }
                            if (allowNotify) {
                                val notification =
                                    buildProgressNotification(name, operation, i + 1, fileList.size)
                                mNotificationManagerCompat.notify(
                                    NOTIFICATION_PROGRESS_ID,
                                    notification
                                )
                            }
                            synchronized(mLocker) {
                                mProcessStatus.processedItems[item.filePath] = item.status
                                mProcessStatus.percent = 100 * (i + 1) / fileList.size
                                mProcessStatus.addedCount = addedCount
                                mProcessStatus.skippedCount = skippedCount
                            }
                            // call callback in handler
                            mProgressCallback?.let {
                                mCallbackHandler?.post {
                                    it.onItemProcessed(item.filePath, item.status)
                                    it.onProgress(
                                        100 * (i + 1) / fileList.size,
                                        addedCount,
                                        skippedCount,
                                        fileList.size
                                    )
                                }
                            }
                        }
                        synchronized(sema) {
                            sema.notify()
                        }
                    }
                    sema.wait()
                }

                // Find duplicates
                if (bookInfoToSave.isNotEmpty()) {
                    // Find duplicates in bookInfoToSaved itself
                    log.debug("Finding duplicates")
                    val countBefore = bookInfoToSave.size
                    val newList = ArrayList<BookInfo>()
                    for ((i, bi) in bookInfoToSave.withIndex()) {
                        var isDuplicate = false
                        for (j in 0 until i) {
                            val item = bookInfoToSave[j]
                            if (item.fileInfo.fingerprint == bi.fileInfo.fingerprint) {
                                isDuplicate = true
                                break
                            }
                        }
                        if (!isDuplicate)
                            newList.add(bi)
                        else {
                            booksToSave.remove(bi)
                            log.debug("duplicate found (in the list itself): $bi")
                        }
                    }
                    if (newList.size != bookInfoToSave.size) {
                        bookInfoToSave.clear()
                        bookInfoToSave.addAll(newList)
                    }
                    newList.clear()
                    // Find duplicates in DB
                    val duplicatesToSkip = ArrayList<BookInfo>()
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            for ((i, bi) in bookInfoToSave.withIndex()) {
                                val fingerprint = bi.fileInfo.fingerprint
                                if (!fingerprint.isNullOrEmpty()) {
                                    mDBServiceAccessor.runWithService(object :
                                        DBServiceAccessor.Callback {
                                        override fun run(binder: DBServiceBinder) {
                                            binder.findBooksByFingerprint(
                                                fingerprint,
                                                object : DBService.BookSearchCallback {
                                                    override fun onBooksFound(books: Collection<BookInfo>?) {
                                                        if (!books.isNullOrEmpty()) {
                                                            val dubItem = books.iterator().next()
                                                            log.info("duplicate found (in DB): $dubItem")
                                                            if (dubItem.isBookFileExists && dubItem.isBookFileAvailable)
                                                                duplicatesToSkip.add(bi)
                                                            else {
                                                                log.info("but this duplicate does not exist or is not readable, copy its bookmarks")
                                                                val existBookmarks =
                                                                    ArrayList<Bookmark>()
                                                                for (bm in dubItem.allBookmarks) {
                                                                    // make a deep copy
                                                                    val newBm = Bookmark(bm)
                                                                    newBm.id = null
                                                                    existBookmarks.add(newBm)
                                                                }
                                                                bi.setBookmarks(existBookmarks)
                                                                duplicatesToRemove.add(dubItem)
                                                            }
                                                        }
                                                        if (i == bookInfoToSave.size - 1) {
                                                            // Last item
                                                            synchronized(sema) {
                                                                sema.notify()
                                                            }
                                                        }
                                                    }
                                                })
                                        }
                                    })
                                } else {
                                    if (i == bookInfoToSave.size - 1) {
                                        // Last item
                                        synchronized(sema) {
                                            sema.notify()
                                        }
                                    }
                                }
                            }
                        }
                        sema.wait()
                    }
                    if (duplicatesToSkip.isNotEmpty()) {
                        bookInfoToSave.removeAll(duplicatesToSkip.toSet())
                        for (book in duplicatesToSkip) {
                            booksToSave.remove(book)
                            bookCovers.remove(book)
                        }
                    }
                    val duplicatesCount = countBefore - bookInfoToSave.size
                    addedCount -= duplicatesCount
                    skippedCount += duplicatesCount
                    synchronized(mLocker) {
                        mProcessStatus.addedCount = addedCount
                        mProcessStatus.skippedCount = skippedCount
                    }
                    log.debug("Skipped $duplicatesCount duplicates")
                }

                if (duplicatesToRemove.isNotEmpty()) {
                    // Remove outdated duplicates
                    log.info("Remove outdated duplicates")
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                                override fun run(binder: DBServiceBinder) {
                                    binder.removeMultipleBookInfo(
                                        duplicatesToRemove,
                                        object : DBService.BooksOperationCallback {
                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                synchronized(sema) {
                                                    sema.notify()
                                                }
                                            }
                                        })
                                }
                            })
                        }
                        sema.wait()
                    }
                }

                if (booksToSave.isNotEmpty()) {
                    // Saving (copy) new books
                    log.debug("Saving (copy) new books to library...")
                    bookInfoToSave.clear()
                    val destDir = getDir("books", MODE_PRIVATE)
                    for ((bi, file) in booksToSave) {
                        try {
                            val inputStream = contentResolver.openInputStream(file.docFile.uri)
                            if (null != inputStream) {
                                val bookInfo = Utils.saveBookStreamToFile(destDir, bi, inputStream)
                                if (null != bookInfo) {
                                    log.info("data saved to $bookInfo")
                                    bookInfoToSave.add(bookInfo)
                                    val coverData = bookCovers[bi]
                                    bookCovers.remove(bi)
                                    coverData?.let { bookCovers[bookInfo] = it }
                                } else {
                                    bookCovers.remove(bi)
                                    file.status = LibraryProcessingBinder.ItemStatus.Failed
                                    log.error("${file.docFile.uri}: failed to save binary data to file")
                                    skippedCount++
                                    addedCount--
                                }
                                inputStream.close()
                            } else {
                                bookCovers.remove(bi)
                                file.status = LibraryProcessingBinder.ItemStatus.Failed
                                log.error("${file.docFile.uri}: failed to open stream data")
                                skippedCount++
                                addedCount--
                            }
                        } catch (e: Exception) {
                            bookCovers.remove(bi)
                            file.status = LibraryProcessingBinder.ItemStatus.Failed
                            log.error("${file.docFile.uri}: failed to open stream data")
                            log.error("$e")
                            skippedCount++
                            addedCount--
                        }
                        if (LibraryProcessingBinder.ItemStatus.Failed == file.status) {
                            synchronized(mLocker) {
                                mProcessStatus.processedItems[file.filePath] = file.status
                            }
                        }
                    }
                    synchronized(mLocker) {
                        mProcessStatus.addedCount = addedCount
                        mProcessStatus.skippedCount = skippedCount
                    }
                    log.info("Saved ${bookInfoToSave.size} files")
                }

                if (bookInfoToSave.isNotEmpty()) {
                    // Save to DB
                    log.debug("Saving new book info to DB...")
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                                override fun run(binder: DBServiceBinder) {
                                    binder.saveMultipleBookInfo(
                                        bookInfoToSave,
                                        object : DBService.BooksOperationCallback {
                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                // filter not saved books
                                                val covers = HashMap<BookInfo, ByteArray?>()
                                                result?.let {
                                                    for ((bi, res) in result) {
                                                        if (res)
                                                            bookCovers[bi]?.let { covers[bi] = it }
                                                    }
                                                }
                                                if (covers.isNotEmpty()) {
                                                    binder.saveMultipleBookCoverData(
                                                        bookCovers,
                                                        object : DBService.BooksOperationCallback {
                                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                                synchronized(sema) {
                                                                    sema.notify()
                                                                }
                                                            }
                                                        })
                                                } else {
                                                    synchronized(sema) {
                                                        sema.notify()
                                                    }
                                                }
                                            }
                                        })
                                }
                            })
                        }
                        sema.wait()
                    }
                    log.debug("Saving to DB complete")
                }
            }
        }

        // Recycle LVDocViewWrapper
        docViewInstance?.let {
            synchronized(sema) {
                BackgroundThread.postBackground {
                    log.debug("Recycling LVDocViewWrapper")
                    it.recycle()
                    synchronized(sema) {
                        sema.notify()
                    }
                }
                sema.wait()
            }
        }

        if (canceled) {
            mProgressCallback?.let { mCallbackHandler?.post { it.onError(LibraryProcessingBinder.ErrorStatus.Canceled) } }
            synchronized(mLocker) {
                mProcessStatus.canceled = true
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Canceled
                mStopRequested = false
            }
        }

        log.debug("document processing completed")
        log.debug("total files - ${fileList.size}; added - $addedCount; skipped - $skippedCount")

        synchronized(mLocker) {
            mProcessStatus.completed = true
        }
        if (allowNotify) {
            mNotificationManagerCompat.cancel(NOTIFICATION_PROGRESS_ID)
            val resultNotification = buildResultNotification(name, operation)
            mNotificationManagerCompat.notify(NOTIFICATION_RESULT_ID, resultNotification)
        }
        mProgressCallback?.let {
            mCallbackHandler?.post {
                if (!canceled)
                    it.onProgress(100, addedCount, skippedCount, fileList.size)
                it.onDone()
            }
        }
    }

    private fun indexFolderImpl(folderPath: String) {
        val allowNotify = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            ActivityCompat.checkSelfPermission(
                this@LibraryProcessingService,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        else true
        if (allowNotify) {
            val notification = buildProgressNotification(
                folderPath,
                LibraryProcessingBinder.OperationType.FolderLink,
                -1,
                -1
            )
            mNotificationManagerCompat.notify(NOTIFICATION_PROGRESS_ID, notification)
        }
        synchronized(mLocker) {
            mProcessStatus.started = true
        }
        mProgressCallback?.let { mCallbackHandler?.post { it.onStarted() } }

        // enumerate files in folder
        val files = ArrayList<String>()
        val enumRes = enumFilesInFolder(folderPath, files)
        if (!enumRes) {
            synchronized(mLocker) {
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.NoPermissions
            }
            mProgressCallback?.let { mCallbackHandler?.post { it.onError(LibraryProcessingBinder.ErrorStatus.NoPermissions) } }
        }
        if (allowNotify) {
            val notification =
                buildProgressNotification(
                    folderPath,
                    LibraryProcessingBinder.OperationType.FolderLink,
                    0,
                    files.size
                )
            mNotificationManagerCompat.notify(NOTIFICATION_PROGRESS_ID, notification)
        }
        log.info("${files.size} files found")

        // Check each file: if it is an archive, add each archive element to the list
        val fileList = ArrayList<FileItem>()
        for (file in files) {
            var isArchive = false
            if (file.lowercase().endsWith(".zip")) {
                val arcItems = CREngineNGBinding.getArchiveItems(file)
                if (arcItems.isNotEmpty()) {
                    // This is archive
                    for (item in arcItems) {
                        fileList.add(
                            FileItem(
                                file + FileInfo.ARC_SEPARATOR + item,
                                LibraryProcessingBinder.ItemStatus.Unprocessed
                            )
                        )
                    }
                    isArchive = true
                }
            }
            if (!isArchive)
                fileList.add(FileItem(file, LibraryProcessingBinder.ItemStatus.Unprocessed))
        }
        synchronized(mLocker) {
            mProcessStatus.totalFiles = files.size
        }
        log.info("${files.size} items including files inside archives")

        // find books in DB
        val sema = Object()
        if (fileList.isNotEmpty()) {
            log.info("search in DB previously added...")
            var existCount = 0
            synchronized(sema) {
                BackgroundThread.postBackground {
                    mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                        override fun run(binder: DBServiceBinder) {
                            for ((i, file) in fileList.withIndex()) {
                                binder.findBooksByPathName(
                                    file.fileName,
                                    object : DBService.BookSearchCallback {
                                        override fun onBooksFound(books: Collection<BookInfo>?) {
                                            if (null == books) {
                                                // this book not found
                                            } else {
                                                // We are searching for a book by an existing file name
                                                // If book info is found in the database, then the book file exists by definition
                                                file.status =
                                                    LibraryProcessingBinder.ItemStatus.SkippedCopy
                                                existCount++
                                            }
                                            if (i == fileList.size - 1) {
                                                // Last item processed
                                                synchronized(sema) {
                                                    sema.notify()
                                                }
                                            }
                                        }
                                    })
                            }
                        }
                    })
                }
                // wait DB operations complete
                sema.wait()
            }
            log.info("search complete, $existCount books already in DB (from ${fileList.size})")
        }

        val exclPatterns = ArrayList<Pattern>().also {
            for (pattern in SCANDIR_IGNORE_FILE_PATTERNS)
                it.add(Pattern.compile(pattern, Pattern.CASE_INSENSITIVE))
        }

        var addedCount = 0
        var skippedCount = 0
        var canceled = false
        var docViewInstance: LVDocViewWrapper? = null
        val genresCollection = GenresCollection.getInstance(this@LibraryProcessingService)

        // Split file processing into several parts to reduce memory consumption (for book covers)
        if (fileList.isNotEmpty()) {
            val partsCount = (fileList.size + MAX_PART_SIZE - 1) / MAX_PART_SIZE
            for (partNo in 0 until partsCount) {
                if (canceled)
                    break
                // process each new file
                val bookInfoToSave = ArrayList<BookInfo>()
                val duplicatesToRemove = ArrayList<BookInfo>()
                val bookCovers = HashMap<BookInfo, ByteArray>()
                synchronized(sema) {
                    BackgroundThread.postBackground {
                        for (j in 0 until MAX_PART_SIZE) {
                            val i = partNo * MAX_PART_SIZE + j
                            val item = if (i < fileList.size) fileList[i] else break
                            // Check if operation canceled, add file to process status
                            synchronized(mLocker) {
                                canceled = mStopRequested
                                mProcessStatus.processedItems[item.fileName] = item.status
                            }
                            if (canceled) {
                                log.info("Canceled")
                                break
                            }
                            val pos = item.fileName.lastIndexOf(File.separatorChar)
                            val basename = if (pos >= 0)
                                item.fileName.substring(pos + 1)
                            else
                                item.fileName
                            // Check file extension & skip unsupported files
                            val docType = DocumentFormat.byExtension(basename)
                            if (null == docType || DocumentFormat.NONE == docType)
                                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
                            for (pattern in exclPatterns) {
                                if (pattern.matcher(basename).matches())
                                    item.status = LibraryProcessingBinder.ItemStatus.Excluded
                            }
                            if (LibraryProcessingBinder.ItemStatus.Unprocessed == item.status) {
                                // Try to open
                                log.debug("Trying to open file ${item.fileName}")
                                synchronized(mLocker) {
                                    mProcessStatus.currentItem = item.fileName
                                }
                                mProgressCallback?.let {
                                    mCallbackHandler?.post {
                                        it.onItemProcessing(
                                            item.fileName
                                        )
                                    }
                                }
                                val docView = docViewInstance ?: run {
                                    log.debug("Creating LVDocViewWrapper")
                                    docViewInstance = LVDocViewWrapper().apply { createDocView() }
                                    docViewInstance!!
                                }
                                // No CSS applied, we just need to check if the document can be opened
                                if (docView.loadDocument(item.fileName, true)) {
                                    log.debug("  successfully")
                                    item.status = LibraryProcessingBinder.ItemStatus.Ok
                                    addedCount++
                                    // Add book info to save to DB
                                    val bookInfo = BookInfo(item.fileName)
                                    docView.updateBookInfo(bookInfo, true, genresCollection)
                                    bookInfoToSave.add(bookInfo)
                                    // Retrieve book cover
                                    docView.getBookCoverData()?.let { bookCovers.put(bookInfo, it) }
                                } else {
                                    log.debug("  failed")
                                    item.status = LibraryProcessingBinder.ItemStatus.Failed
                                    skippedCount++
                                }
                            } else {
                                skippedCount++
                            }
                            if (allowNotify) {
                                val notification =
                                    buildProgressNotification(
                                        folderPath,
                                        LibraryProcessingBinder.OperationType.FolderLink,
                                        i + 1,
                                        fileList.size
                                    )
                                mNotificationManagerCompat.notify(
                                    NOTIFICATION_PROGRESS_ID,
                                    notification
                                )
                            }
                            synchronized(mLocker) {
                                mProcessStatus.processedItems[item.fileName] = item.status
                                mProcessStatus.percent = 100 * (i + 1) / fileList.size
                                mProcessStatus.addedCount = addedCount
                                mProcessStatus.skippedCount = skippedCount
                            }
                            // call callback in handler
                            mProgressCallback?.let {
                                mCallbackHandler?.post {
                                    it.onItemProcessed(item.fileName, item.status)
                                    it.onProgress(
                                        100 * (i + 1) / fileList.size,
                                        addedCount,
                                        skippedCount,
                                        fileList.size
                                    )
                                }
                            }
                        }
                        synchronized(sema) {
                            sema.notify()
                        }
                    }
                    // wait doc view operations complete
                    sema.wait()
                }

                // Find duplicates
                if (bookInfoToSave.isNotEmpty()) {
                    // Find duplicates in bookInfoToSaved itself
                    log.debug("Finding duplicates")
                    val countBefore = bookInfoToSave.size
                    val newList = ArrayList<BookInfo>()
                    for ((i, bi) in bookInfoToSave.withIndex()) {
                        var duplicate: BookInfo? = null
                        for (j in 0 until i) {
                            val item = bookInfoToSave[j]
                            if (item.fileInfo.fingerprint == bi.fileInfo.fingerprint) {
                                duplicate = item
                                break
                            }
                        }
                        duplicate?.let {
                            log.debug("duplicate found (in the list itself): $bi")
                            it.fileInfo.pathNameWA?.let {
                                synchronized(mLocker) {
                                    mProcessStatus.processedItems[it] =
                                        LibraryProcessingBinder.ItemStatus.SkippedCopy
                                }
                            }
                        } ?: run {
                            newList.add(bi)
                        }
                    }
                    if (newList.size != bookInfoToSave.size) {
                        bookInfoToSave.clear()
                        bookInfoToSave.addAll(newList)
                    }
                    newList.clear()
                    // Find duplicates in DB
                    val duplicatesToSkip = ArrayList<BookInfo>()
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            for ((i, bi) in bookInfoToSave.withIndex()) {
                                val fingerprint = bi.fileInfo.fingerprint
                                if (!fingerprint.isNullOrEmpty()) {
                                    mDBServiceAccessor.runWithService(object :
                                        DBServiceAccessor.Callback {
                                        override fun run(binder: DBServiceBinder) {
                                            binder.findBooksByFingerprint(
                                                fingerprint,
                                                object : DBService.BookSearchCallback {
                                                    override fun onBooksFound(books: Collection<BookInfo>?) {
                                                        if (!books.isNullOrEmpty()) {
                                                            val dubItem = books.iterator().next()
                                                            log.info("duplicate found (in DB): $dubItem")
                                                            if (dubItem.isBookFileExists)
                                                                duplicatesToSkip.add(bi)
                                                            else {
                                                                log.info("but this duplicate does not exist, copy its bookmarks")
                                                                val existBookmarks =
                                                                    ArrayList<Bookmark>()
                                                                for (bm in dubItem.allBookmarks) {
                                                                    // make a deep copy
                                                                    val newBm = Bookmark(bm)
                                                                    newBm.id = null
                                                                    existBookmarks.add(newBm)
                                                                }
                                                                bi.setBookmarks(existBookmarks)
                                                                duplicatesToRemove.add(dubItem)
                                                            }
                                                        }
                                                        if (i == bookInfoToSave.size - 1) {
                                                            // Last item
                                                            synchronized(sema) {
                                                                sema.notify()
                                                            }
                                                        }
                                                    }
                                                })
                                        }
                                    })
                                } else {
                                    if (i == bookInfoToSave.size - 1) {
                                        // Last item
                                        synchronized(sema) {
                                            sema.notify()
                                        }
                                    }
                                }
                            }
                        }
                        sema.wait()
                    }
                    if (duplicatesToSkip.isNotEmpty()) {
                        bookInfoToSave.removeAll(duplicatesToSkip.toSet())
                        for (book in duplicatesToSkip) {
                            book.fileInfo.pathNameWA?.let {
                                synchronized(mLocker) {
                                    mProcessStatus.processedItems[it] =
                                        LibraryProcessingBinder.ItemStatus.SkippedCopy
                                }
                            }
                            bookCovers.remove(book)
                        }
                    }
                    val duplicatesCount = countBefore - bookInfoToSave.size
                    addedCount -= duplicatesCount
                    skippedCount += duplicatesCount
                    synchronized(mLocker) {
                        mProcessStatus.addedCount = addedCount
                        mProcessStatus.skippedCount = skippedCount
                    }
                    log.debug("Skipped $duplicatesCount duplicates")
                }

                if (duplicatesToRemove.isNotEmpty()) {
                    // Remove outdated duplicates
                    log.info("Remove outdated duplicates")
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                                override fun run(binder: DBServiceBinder) {
                                    binder.removeMultipleBookInfo(
                                        duplicatesToRemove,
                                        object : DBService.BooksOperationCallback {
                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                synchronized(sema) {
                                                    sema.notify()
                                                }
                                            }
                                        })
                                }
                            })
                        }
                        sema.wait()
                    }
                }

                if (bookInfoToSave.isNotEmpty()) {
                    // Save to DB
                    log.debug("Saving new book info to DB...")
                    synchronized(sema) {
                        BackgroundThread.postBackground {
                            mDBServiceAccessor.runWithService(object : DBServiceAccessor.Callback {
                                override fun run(binder: DBServiceBinder) {
                                    binder.saveMultipleBookInfo(
                                        bookInfoToSave,
                                        object : DBService.BooksOperationCallback {
                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                // filter not saved books
                                                val covers = HashMap<BookInfo, ByteArray?>()
                                                result?.let {
                                                    for ((bi, res) in result) {
                                                        if (res)
                                                            bookCovers[bi]?.let { covers[bi] = it }
                                                    }
                                                }
                                                if (covers.isNotEmpty()) {
                                                    binder.saveMultipleBookCoverData(
                                                        bookCovers,
                                                        object : DBService.BooksOperationCallback {
                                                            override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                                                synchronized(sema) {
                                                                    sema.notify()
                                                                }
                                                            }
                                                        })
                                                } else {
                                                    synchronized(sema) {
                                                        sema.notify()
                                                    }
                                                }
                                            }
                                        })
                                }
                            })
                        }
                        sema.wait()
                    }
                    log.debug("Saving to DB complete")
                }
            }
        }

        // Recycle LVDocViewWrapper
        docViewInstance?.let {
            synchronized(sema) {
                BackgroundThread.postBackground {
                    log.debug("Recycling LVDocViewWrapper")
                    it.recycle()
                    synchronized(sema) {
                        sema.notify()
                    }
                }
                sema.wait()
            }
        }

        if (canceled) {
            mProgressCallback?.let { mCallbackHandler?.post { it.onError(LibraryProcessingBinder.ErrorStatus.Canceled) } }
            synchronized(mLocker) {
                mProcessStatus.canceled = true
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Canceled
                mStopRequested = false
            }
        }

        log.debug("document processing completed")
        log.debug("total files - ${fileList.size}; added - $addedCount; skipped - $skippedCount")

        synchronized(mLocker) {
            mProcessStatus.completed = true
        }
        if (allowNotify) {
            mNotificationManagerCompat.cancel(NOTIFICATION_PROGRESS_ID)
            val resultNotification = buildResultNotification(
                folderPath,
                LibraryProcessingBinder.OperationType.FolderLink
            )
            mNotificationManagerCompat.notify(NOTIFICATION_RESULT_ID, resultNotification)
        }
        mProgressCallback?.let {
            mCallbackHandler?.post {
                if (!canceled)
                    it.onProgress(100, addedCount, skippedCount, fileList.size)
                it.onDone()
            }
        }
    }

    private fun importFontsImpl(name: String, files: List<DocumentFile>) {
        val allowNotify = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
            ActivityCompat.checkSelfPermission(
                this@LibraryProcessingService,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        else true
        val operation = LibraryProcessingBinder.OperationType.ImportFonts
        if (allowNotify) {
            val notification = buildProgressNotification(name, operation, -1, -1)
            mNotificationManagerCompat.notify(NOTIFICATION_PROGRESS_ID, notification)
        }
        synchronized(mLocker) {
            mProcessStatus.started = true
        }
        mProgressCallback?.let { mCallbackHandler?.post { it.onStarted() } }

        val fileList = ArrayList<DocfFileItem>()
        // TODO: Check each file: if it is an archive, add each archive element to the list
        for (file in files) {
            val itemPath = Utils.getNativeFile(file, true)?.absolutePath ?: run {
                Utils.getPseudoFilePath(file)?.let { "stream:/${it}" } ?: run { "" }
            }
            fileList.add(
                DocfFileItem(
                    file,
                    itemPath,
                    LibraryProcessingBinder.ItemStatus.Unprocessed
                )
            )
        }
        synchronized(mLocker) {
            mProcessStatus.totalFiles = fileList.size
        }

        val destFontsDir = File(applicationContext.filesDir, "fonts")
        var addedCount = 0
        var skippedCount = 0
        var canceled = false
        val contentResolver = applicationContext.contentResolver

        for ((i, item) in fileList.withIndex()) {
            // Check if operation canceled, add file to process status
            synchronized(mLocker) {
                canceled = mStopRequested
            }
            // Check if operation canceled, add file to process status
            if (canceled) {
                log.info("Canceled")
                mProcessStatus.processedItems[item.filePath] = item.status
                break
            }
            // process each new file
            val pathInfo = Utils.parseFilePath(item.filePath)
            if (pathInfo.isEmpty())
                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
            var extMatch = false
            if (pathInfo.basename.isNotEmpty() && pathInfo.extension.isNotEmpty()) {
                for (ext in ALLOWED_FONTS_EXT) {
                    if (0 == ext.compareTo(pathInfo.extension, true)) {
                        extMatch = true
                        break
                    }
                }
            }
            if (!extMatch)
                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
            if (item.docFile.length() > 200 * 1024 * 1204) {
                // The file is too large ( > 200 Mb ), most likely it is not a font
                item.status = LibraryProcessingBinder.ItemStatus.UnsupportedType
            }
            if (LibraryProcessingBinder.ItemStatus.Unprocessed == item.status) {
                // TODO: implement work with *.zip archives (extracting fonts from them)
                log.debug("Processing font file ${item.filePath}")
                synchronized(mLocker) {
                    mProcessStatus.currentItem = item.filePath
                }
                mProgressCallback?.let { mCallbackHandler?.post { it.onItemProcessing(item.filePath) } }
                var isDuplicate = false
                val srcHash = if (item.docFile.length() > 0)
                    Utils.calcSHA256(contentResolver, item.docFile)
                else {
                    log.debug("Source font file is empty, ignoring")
                    ""
                }
                if (srcHash.isNotEmpty()) {
                    var idx = 0
                    var targetName = "${pathInfo.basename}.${pathInfo.extension}"
                    var targetFile = File(destFontsDir, targetName)
                    while (targetFile.exists() && idx < 100) {
                        val destHash = Utils.calcSHA256(targetFile)
                        if (destHash == srcHash) {
                            isDuplicate = true
                            break
                        } else {
                            idx++
                            targetName = "${pathInfo.basename}-${idx}.${pathInfo.extension}"
                            targetFile = File(destFontsDir, targetName)
                        }
                    }
                    if (!isDuplicate) {
                        if (idx < 100) {
                            // Make file copy
                            try {
                                val inputStream =
                                    contentResolver.openInputStream(item.docFile.uri)!!
                                val outputStream = FileOutputStream(targetFile)
                                Utils.copyStreamData(inputStream, outputStream)
                                outputStream.close()
                                inputStream.close()
                                item.status = LibraryProcessingBinder.ItemStatus.Ok
                                addedCount++
                                log.info("Font $targetName copied successfully")
                            } catch (e: Exception) {
                                log.error("Failed to copy font file $targetName: $e")
                                targetFile.delete()
                                item.status = LibraryProcessingBinder.ItemStatus.Failed
                                skippedCount++
                            }
                        } else {
                            log.error("Can't make free file name for $targetFile")
                            item.status = LibraryProcessingBinder.ItemStatus.Failed
                            skippedCount++
                        }
                    } else {
                        log.info("A copy of file $targetName already exists")
                        item.status = LibraryProcessingBinder.ItemStatus.SkippedCopy
                        skippedCount++
                    }
                } else {
                    log.error("Failed to calculate hash for source font file ")
                    item.status = LibraryProcessingBinder.ItemStatus.Failed
                    skippedCount++
                }
            } else {
                log.debug("Skip unsupported file ${item.filePath}")
                skippedCount++
            }
            if (allowNotify) {
                val notification =
                    buildProgressNotification(name, operation, i + 1, fileList.size)
                mNotificationManagerCompat.notify(NOTIFICATION_PROGRESS_ID, notification)
            }
            synchronized(mLocker) {
                mProcessStatus.processedItems[item.filePath] = item.status
                mProcessStatus.percent = 100 * (i + 1) / fileList.size
                mProcessStatus.addedCount = addedCount
                mProcessStatus.skippedCount = skippedCount
            }
            // call callback in handler
            mProgressCallback?.let {
                mCallbackHandler?.post {
                    it.onItemProcessed(item.filePath, item.status)
                    it.onProgress(
                        100 * (i + 1) / fileList.size,
                        addedCount,
                        skippedCount,
                        fileList.size
                    )
                }
            }
        }

        if (canceled) {
            mProgressCallback?.let { mCallbackHandler?.post { it.onError(LibraryProcessingBinder.ErrorStatus.Canceled) } }
            synchronized(mLocker) {
                mProcessStatus.canceled = true
                mProcessStatus.lastError = LibraryProcessingBinder.ErrorStatus.Canceled
                mStopRequested = false
            }
        }

        log.info("document processing completed")
        log.info("total files - ${fileList.size}; added - $addedCount; skipped - $skippedCount")

        if (addedCount > 0) {
            // Register new fonts in CREngineNGBinding.
            // Once the ReaderActivity instance is created, we must register the new fonts with CREngineNGBinding to enable them.
            // If not, then it is useless, but it does not lead to anything bad.
            val engineObjects = ReaderEngineObjectsAccessor.leaseEngineObjects()
            val registerRes = engineObjects.crEngineNGBinding?.initFontsFromDir(destFontsDir)
            if (registerRes == true)
                log.info("Successful registration of new fonts")
            else
                log.info("Failed to register new fonts")
            ReaderEngineObjectsAccessor.releaseEngineObject(engineObjects)
        }

        synchronized(mLocker) {
            mProcessStatus.completed = true
        }
        if (allowNotify) {
            mNotificationManagerCompat.cancel(NOTIFICATION_PROGRESS_ID)
            val resultNotification = buildResultNotification(name, operation)
            mNotificationManagerCompat.notify(NOTIFICATION_RESULT_ID, resultNotification)
        }
        mProgressCallback?.let {
            mCallbackHandler?.post {
                if (!canceled)
                    it.onProgress(100, addedCount, skippedCount, fileList.size)
                it.onDone()
            }
        }
    }

    private fun enumFilesInFolder(path: String, list: MutableList<String>): Boolean {
        val dir = File(path)
        if (dir.canRead()) {
            dir.list()?.let { children ->
                for (child in children) {
                    val item = File(path, child)
                    if (item.isDirectory) {
                        if (!enumFilesInFolder(item.absolutePath, list))
                            return false
                    } else
                        list.add(item.absolutePath)
                }
            }
            return true
        }
        return false
    }

    private fun enumFilesInFolderUri(dir: DocumentFile, list: MutableList<DocumentFile>): Boolean {
        if (dir.canRead()) {
            val children = dir.listFiles()
            for (child in children) {
                if (child.isDirectory) {
                    if (!enumFilesInFolderUri(child, list))
                        return false
                } else
                    list.add(child)
            }
            return true
        }
        return false
    }

    private fun buildProgressNotification(
        subject: String,
        operation: LibraryProcessingBinder.OperationType,
        current: Int,
        total: Int
    ): Notification {
        val title = getString(R.string.app_name)
        var builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        builder = builder.setDefaults(0)
        builder = builder.setSmallIcon(R.drawable.ic_logo)
        builder = builder.setContentTitle(title)
        val contextText = when (operation) {
            LibraryProcessingBinder.OperationType.None -> ""
            LibraryProcessingBinder.OperationType.FolderCopy -> getString(
                R.string.copying_folder___to_the_program_library,
                subject
            )

            LibraryProcessingBinder.OperationType.FileCopy -> getString(
                R.string.copying_file___to_the_program_library,
                subject
            )

            LibraryProcessingBinder.OperationType.FolderLink -> getString(
                R.string.indexing_folder_,
                subject
            )

            LibraryProcessingBinder.OperationType.ImportFonts -> getString(
                R.string.import_fonts_from_folder_,
                subject
            )
        }
        builder = builder.setContentText(contextText)
        builder = if (current >= 0 && total > 0)
            builder.setProgress(total, current, false)
        else
            builder.setProgress(total, current, true)
        builder = builder.setOngoing(true)
        builder = builder.setAutoCancel(false)
        builder = builder.setPriority(NotificationCompat.PRIORITY_LOW)
        builder = builder.setShowWhen(false)
        builder = builder.setLocalOnly(false)
        //  add actions
        // cancel
        val cancelIntent = PendingIntent.getBroadcast(
            this,
            0,
            Intent(LIB_ACTION_CANCEL),
            PendingIntent.FLAG_IMMUTABLE
        )
        val actionBld = NotificationCompat.Action.Builder(
            R.drawable.ic_action_cancel,
            getString(R.string.cancel),
            cancelIntent
        )
        builder = builder.addAction(actionBld.build())
        builder = builder.setColor(Color.GRAY)
        builder = builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        // delete intent (no-op)
        val delPendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            Intent(LIB_ACTION_NOOP),
            PendingIntent.FLAG_IMMUTABLE
        )
        builder = builder.setDeleteIntent(delPendingIntent)
        return builder.build()
    }

    private fun buildResultNotification(
        subject: String,
        operation: LibraryProcessingBinder.OperationType
    ): Notification {
        val title = getString(R.string.app_name)
        var builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        builder = builder.setDefaults(0)
        builder = builder.setSmallIcon(R.drawable.ic_logo)
        builder = builder.setContentTitle(title)
        val contextText = when (operation) {
            LibraryProcessingBinder.OperationType.None -> ""
            LibraryProcessingBinder.OperationType.FolderCopy -> getString(
                R.string.copying_the_folder___to_the_program_library_is_complete,
                subject
            )

            LibraryProcessingBinder.OperationType.FileCopy -> getString(
                R.string.copying_file___to_the_program_library_is_complete,
                subject
            )

            LibraryProcessingBinder.OperationType.FolderLink -> getString(
                R.string.indexing_of_folder___is_complete,
                subject
            )

            LibraryProcessingBinder.OperationType.ImportFonts -> getString(
                R.string.import_fonts_from_folder___is_complete,
                subject
            )
        }
        builder = builder.setContentText(contextText)
        builder = builder.setOngoing(false)
        builder = builder.setAutoCancel(true)
        builder = builder.setPriority(NotificationCompat.PRIORITY_LOW)
        builder = builder.setShowWhen(true).setWhen(Utils.timeStamp())
        builder = builder.setLocalOnly(false)
        return builder.build()
    }

    companion object {
        const val LIB_ACTION_NOOP = "ltreader.library.progress.noop";
        private const val LIB_ACTION_CANCEL = "ltreader.library.progress.cancel";

        private val log = SRLog.create("libsvc")
        private const val NOTIFICATION_PROGRESS_ID = 1
        private const val NOTIFICATION_RESULT_ID = 2
        private const val NOTIFICATION_CHANNEL_ID = "LtReader Lib C1";
        private const val MAX_PART_SIZE = 10
        private val SCANDIR_IGNORE_FILE_PATTERNS = arrayOf(
            "^index\\.txt$",
            "^info\\.txt$",
            "^0_index\\.txt$",
            "^readme\\.txt$",
            "^.*\\.nfo$"
        )
        private val ALLOWED_FONTS_EXT = arrayOf(
            "ttf",
            "ttc",
            "otf",
            "pfa",
            "pfb"
        )
    }
}
