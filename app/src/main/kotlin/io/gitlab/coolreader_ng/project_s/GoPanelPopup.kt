/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow
import java.text.NumberFormat
import kotlin.math.roundToInt

class GoPanelPopup(private val parent: View, contentView: View) {

    interface GoPanelListener {
        fun onPrevChapterClick()
        fun onNextChapterClick()
        fun onPrevPageClick()
        fun onNextPageClick()
        fun jumpToPage(pageNo: Int)
        fun onShowTOC()
    }

    var onGoPanelClickListener: GoPanelListener? = null
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mPopup: PopupWindow

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null
    private val mBookNameView: TextView
    private val mPercentPosView: TextView
    private val mChapterNameView: TextView
    private val mPagePosView: TextView
    private val mPosSlider: Slider
    private var mPosSeekBarIsTracking = false
    private var mRootTocItem: DocumentTOCItem? = null
    private var mCurrentChapter: DocumentTOCItem? = null
    private var mCurrentPage = -1
    private var mPageCount = -1

    fun show(
        title: String,
        toc: DocumentTOCItem,
        currentPage: Int,
        pageCount: Int
    ) {
        mBookNameView.text = title
        mCurrentPage = currentPage
        mPageCount = pageCount
        mRootTocItem = toc
        updateImpl()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
    }

    fun update(currentPage: Int, pageCount: Int) {
        mCurrentPage = currentPage
        mPageCount = pageCount
        updateImpl()
    }

    private fun updateImpl() {
        mRootTocItem?.let {
            val percent: Double = (mCurrentPage - 1) / mPageCount.toDouble()
            val numberFormat = NumberFormat.getPercentInstance(SettingsManager.activeLang.locale)
            numberFormat.minimumFractionDigits = 2
            numberFormat.maximumIntegerDigits = 2
            mPercentPosView.text = numberFormat.format(percent)
            mPagePosView.text = mPopup.contentView.context.getString(
                R.string.go_panel_page_pos,
                mCurrentPage,
                mPageCount
            )
            if (!mPosSeekBarIsTracking) {
                // Assumed that this SeekBar started from 0
                mPosSlider.valueFrom = 1f
                mPosSlider.valueTo = mPageCount.toFloat()
                mPosSlider.value = mCurrentPage.toFloat()
            }
            mCurrentChapter = mRootTocItem?.getChapterAtPage(mCurrentPage - 1)
            mChapterNameView.text = mCurrentChapter?.name
        }
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    init {
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.BOTTOM)
            mPopup.exitTransition = Slide(Gravity.BOTTOM)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = true
        mPopup.isTouchable = true
        mPopup.isFocusable = false
        val glassView: View = contentView.findViewById(R.id.glass)
        mBookNameView = contentView.findViewById(R.id.bookName)
        mPercentPosView = contentView.findViewById(R.id.percentPos)
        mChapterNameView = contentView.findViewById(R.id.chapterName)
        mPagePosView = contentView.findViewById(R.id.pagePos)
        mPosSlider = contentView.findViewById(R.id.posSlider)
        val btnPrevChapter: Button = contentView.findViewById(R.id.btnPrevChapter)
        val btnNextChapter: Button = contentView.findViewById(R.id.btnNextChapter)
        val btnPrevPage: Button = contentView.findViewById(R.id.btnPrevPage)
        val btnNextPage: Button = contentView.findViewById(R.id.btnNextPage)

        val panel = contentView.findViewById<View>(R.id.panel)
        val panelPaddingLeft = panel.paddingLeft
        val panelPaddingRight = panel.paddingRight
        val panelPaddingBottom = panel.paddingBottom
        ViewCompat.setOnApplyWindowInsetsListener(panel) { v, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            v.updatePadding(
                left = insets.left + panelPaddingLeft,
                bottom = insets.bottom + panelPaddingBottom,
                right = insets.right + panelPaddingRight
            )
            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }

        glassView.setOnClickListener { hide() }
        btnPrevChapter.setOnClickListener { onGoPanelClickListener?.onPrevChapterClick() }
        btnNextChapter.setOnClickListener { onGoPanelClickListener?.onNextChapterClick() }
        btnPrevPage.setOnClickListener { onGoPanelClickListener?.onPrevPageClick() }
        btnNextPage.setOnClickListener { onGoPanelClickListener?.onNextPageClick() }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed
            // TODO: Test on other API, new material design library (possible fixed)
            mPosSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mPosSlider.setLabelFormatter { value ->
                val numberFormat = NumberFormat.getNumberInstance()
                numberFormat.format(value.roundToInt())
            }
        }
        mPosSlider.addOnChangeListener(Slider.OnChangeListener { _, value, fromUser ->
            if (fromUser)
                onGoPanelClickListener?.jumpToPage(value.roundToInt())
        })
        mPosSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
                mPosSeekBarIsTracking = true
            }

            override fun onStopTrackingTouch(slider: Slider) {
                mPosSeekBarIsTracking = false
                val seekPageNo = slider.value.roundToInt()
                if (seekPageNo != mCurrentPage)
                    onGoPanelClickListener?.jumpToPage(seekPageNo)
            }
        })
        mBookNameView.setOnClickListener { onGoPanelClickListener?.onShowTOC() }
        mChapterNameView.setOnClickListener { onGoPanelClickListener?.onShowTOC() }
    }
}
