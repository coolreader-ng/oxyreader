/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.android.material.slider.LabelFormatter
import com.google.android.material.slider.Slider
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import kotlin.math.roundToInt

class StylePageIndentsViewHolder(itemView: View, props: SRProperties) :
    StylePanelPopup.AbstractPageViewHolder(itemView, props) {

    private val mLeftIndentValueView: TextView
    private val mBtnLeftIndentDec: Button
    private val mBtnLeftIndentInc: Button
    private val mLeftIndentSlider: Slider
    private val mRightIndentValueView: TextView
    private val mBtnRightIndentDec: Button
    private val mBtnRightIndentInc: Button
    private val mRightIndentSlider: Slider
    private val mTopIndentValueView: TextView
    private val mBtnTopIndentDec: Button
    private val mBtnTopIndentInc: Button
    private val mTopIndentSlider: Slider
    private val mBottomIndentValueView: TextView
    private val mBtnBottomIndentDec: Button
    private val mBtnBottomIndentInc: Button
    private val mBottomIndentSlider: Slider
    private val mLineSpacingSlider: Slider
    private val mLineSpacingValueView: TextView
    private val mBtnLineSpacingDec: Button
    private val mBtnLineSpacingInc: Button

    override fun onUpdateViewImpl() {
        // Left indent
        val leftIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, 10)
        mLeftIndentValueView.text = context.getString(R.string.format_number_px, leftIndent)
        mLeftIndentSlider.value = leftIndent.toFloat()
        mBtnLeftIndentDec.isEnabled = leftIndent > INDENTS_VALUES.first()
        mBtnLeftIndentInc.isEnabled = leftIndent < INDENTS_VALUES.last()

        // Right indent
        val rightIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, 10)
        mRightIndentValueView.text = context.getString(R.string.format_number_px, rightIndent)
        mRightIndentSlider.value = rightIndent.toFloat()
        mBtnRightIndentDec.isEnabled = rightIndent > INDENTS_VALUES.first()
        mBtnRightIndentInc.isEnabled = rightIndent < INDENTS_VALUES.last()

        // Top indent
        val topIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, 10)
        mTopIndentValueView.text = context.getString(R.string.format_number_px, topIndent)
        mTopIndentSlider.value = topIndent.toFloat()
        mBtnTopIndentDec.isEnabled = topIndent > INDENTS_VALUES.first()
        mBtnTopIndentInc.isEnabled = topIndent < INDENTS_VALUES.last()

        // Bottom indent
        val bottomIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, 10)
        mBottomIndentValueView.text = context.getString(R.string.format_number_px, bottomIndent)
        mBottomIndentSlider.value = bottomIndent.toFloat()
        mBtnBottomIndentDec.isEnabled = bottomIndent > INDENTS_VALUES.first()
        mBtnBottomIndentInc.isEnabled = bottomIndent < INDENTS_VALUES.last()

        // Line spacing
        val lineSpacing = mProps.getInt(PropNames.Engine.PROP_INTERLINE_SPACE, 100)
        mLineSpacingSlider.value = lineSpacing.toFloat()
        mLineSpacingValueView.text = context.getString(R.string.format_number_percent, lineSpacing)
        mBtnLineSpacingDec.isEnabled = lineSpacing > LINE_SPACE_VALUES.first()
        mBtnLineSpacingInc.isEnabled = lineSpacing < LINE_SPACE_VALUES.last()
    }

    override fun onResetViewImpl() {
    }

    override fun onSetUserData(data: HashMap<String, Any?>) {
    }

    init {
        mLeftIndentValueView = itemView.findViewById(R.id.leftIndentValueView)
        mBtnLeftIndentDec = itemView.findViewById(R.id.btnLeftIndentDec)
        mBtnLeftIndentInc = itemView.findViewById(R.id.btnLeftIndentInc)
        mLeftIndentSlider = itemView.findViewById(R.id.leftIndentSlider)
        mRightIndentValueView = itemView.findViewById(R.id.rightIndentValueView)
        mBtnRightIndentDec = itemView.findViewById(R.id.btnRightIndentDec)
        mBtnRightIndentInc = itemView.findViewById(R.id.btnRightIndentInc)
        mRightIndentSlider = itemView.findViewById(R.id.rightIndentSlider)
        mTopIndentValueView = itemView.findViewById(R.id.topIndentValueView)
        mBtnTopIndentDec = itemView.findViewById(R.id.btnTopIndentDec)
        mBtnTopIndentInc = itemView.findViewById(R.id.btnTopIndentInc)
        mTopIndentSlider = itemView.findViewById(R.id.topIndentSlider)
        mBottomIndentValueView = itemView.findViewById(R.id.bottomIndentValueView)
        mBtnBottomIndentDec = itemView.findViewById(R.id.btnBottomIndentDec)
        mBtnBottomIndentInc = itemView.findViewById(R.id.btnBottomIndentInc)
        mBottomIndentSlider = itemView.findViewById(R.id.bottomIndentSlider)
        mLineSpacingSlider = itemView.findViewById(R.id.lineSpacingSlider)
        mLineSpacingValueView = itemView.findViewById(R.id.lineSpacingValueView)
        mBtnLineSpacingDec = itemView.findViewById(R.id.btnLineSpacingDec)
        mBtnLineSpacingInc = itemView.findViewById(R.id.btnLineSpacingInc)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed (when slider is using on PopupWindow)
            // TODO: Test on other API, new material design library (possible fixed)
            mLeftIndentSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mLeftIndentSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_px, value.roundToInt())
            }
            mRightIndentSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mRightIndentSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_px, value.roundToInt())
            }
            mTopIndentSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mTopIndentSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_px, value.roundToInt())
            }
            mBottomIndentSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mBottomIndentSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_px, value.roundToInt())
            }
        }

        // Left indent
        mLeftIndentSlider.valueFrom = INDENTS_VALUES.first().toFloat()
        mLeftIndentSlider.valueTo = INDENTS_VALUES.last().toFloat()
        mLeftIndentSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.roundToInt()
                val nearestValue = Utils.findNearestValue(INDENTS_VALUES, value)
                val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, 10)
                if (prevIndent != nearestValue) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, nearestValue)
                    commitChanges()
                } else if (value != nearestValue) {
                    mLeftIndentSlider.value = nearestValue.toFloat()
                }
            }
        })
        mBtnLeftIndentDec.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex - 1
            if (newIndentIndex >= 0) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, newIndent)
                    commitChanges()
                }
            }
        }
        mBtnLeftIndentInc.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex + 1
            if (newIndentIndex < INDENTS_VALUES.size) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_LEFT, newIndent)
                    commitChanges()
                }
            }
        }

        // Right indent
        mRightIndentSlider.valueFrom = INDENTS_VALUES.first().toFloat()
        mRightIndentSlider.valueTo = INDENTS_VALUES.last().toFloat()
        mRightIndentSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.roundToInt()
                val nearestValue = Utils.findNearestValue(INDENTS_VALUES, value)
                val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, 10)
                if (prevIndent != nearestValue) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, nearestValue)
                    commitChanges()
                } else if (value != nearestValue) {
                    mRightIndentSlider.value = nearestValue.toFloat()
                }
            }
        })
        mBtnRightIndentDec.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex - 1
            if (newIndentIndex >= 0) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, newIndent)
                    commitChanges()
                }
            }
        }
        mBtnRightIndentInc.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex + 1
            if (newIndentIndex < INDENTS_VALUES.size) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_RIGHT, newIndent)
                    commitChanges()
                }
            }
        }

        // Top indent
        mTopIndentSlider.valueFrom = INDENTS_VALUES.first().toFloat()
        mTopIndentSlider.valueTo = INDENTS_VALUES.last().toFloat()
        mTopIndentSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.roundToInt()
                val nearestValue = Utils.findNearestValue(INDENTS_VALUES, value)
                val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, 10)
                if (prevIndent != nearestValue) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, nearestValue)
                    commitChanges()
                } else if (value != nearestValue) {
                    mTopIndentSlider.value = nearestValue.toFloat()
                }
            }
        })
        mBtnTopIndentDec.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex - 1
            if (newIndentIndex >= 0) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, newIndent)
                    commitChanges()
                }
            }
        }
        mBtnTopIndentInc.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex + 1
            if (newIndentIndex < INDENTS_VALUES.size) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_TOP, newIndent)
                    commitChanges()
                }
            }
        }

        // Bottom indent
        mBottomIndentSlider.valueFrom = INDENTS_VALUES.first().toFloat()
        mBottomIndentSlider.valueTo = INDENTS_VALUES.last().toFloat()
        mBottomIndentSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.roundToInt()
                val nearestValue = Utils.findNearestValue(INDENTS_VALUES, value)
                val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, 10)
                if (prevIndent != nearestValue) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, nearestValue)
                    commitChanges()
                } else if (value != nearestValue) {
                    mBottomIndentSlider.value = nearestValue.toFloat()
                }
            }
        })
        mBtnBottomIndentDec.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex - 1
            if (newIndentIndex >= 0) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, newIndent)
                    commitChanges()
                }
            }
        }
        mBtnBottomIndentInc.setOnClickListener {
            val prevIndent = mProps.getInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, 10)
            val prevIndentIndex = Utils.findNearestIndex(INDENTS_VALUES, prevIndent)
            val newIndentIndex = prevIndentIndex + 1
            if (newIndentIndex < INDENTS_VALUES.size) {
                val newIndent = INDENTS_VALUES[newIndentIndex]
                if (prevIndent != newIndent) {
                    mProps.setInt(PropNames.Engine.PROP_PAGE_MARGIN_BOTTOM, newIndent)
                    commitChanges()
                }
            }
        }

        // Line spacing
        val lineSpacingLabel = itemView.findViewById<TextView>(R.id.lineSpacingLabel)
        lineSpacingLabel.setHyphenatedText(
            SettingsManager.activeLang.langTag,
            lineSpacingLabel.text.toString()
        )
        mLineSpacingSlider.valueFrom = LINE_SPACE_VALUES.first().toFloat()
        mLineSpacingSlider.valueTo = LINE_SPACE_VALUES.last().toFloat()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // On API 16 (and possible some one) using label program crashed (when slider is using on PopupWindow)
            // TODO: Test on other API, new material design library (possible fixed)
            mLineSpacingSlider.labelBehavior = LabelFormatter.LABEL_FLOATING
            mLineSpacingSlider.setLabelFormatter { value ->
                context.getString(R.string.format_number_percent, value.roundToInt())
            }
        }
        mLineSpacingSlider.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: Slider) {
            }

            override fun onStopTrackingTouch(slider: Slider) {
                val value = slider.value.roundToInt()
                val nearestValue = Utils.findNearestValue(LINE_SPACE_VALUES, value)
                val prevSpacing = mProps.getInt(PropNames.Engine.PROP_INTERLINE_SPACE, 100)
                if (prevSpacing != nearestValue) {
                    mProps.setInt(PropNames.Engine.PROP_INTERLINE_SPACE, nearestValue)
                    commitChanges()
                } else if (value != nearestValue) {
                    mLineSpacingSlider.value = nearestValue.toFloat()
                }
            }
        })
        mBtnLineSpacingDec.setOnClickListener {
            val prevSpacing = mProps.getInt(PropNames.Engine.PROP_INTERLINE_SPACE, 100)
            val prevSpacingIndex = Utils.findNearestIndex(LINE_SPACE_VALUES, prevSpacing)
            val newSpacingIndex = prevSpacingIndex - 1
            if (newSpacingIndex >= 0) {
                val newSpacing = LINE_SPACE_VALUES[newSpacingIndex]
                if (prevSpacing != newSpacing) {
                    mProps.setInt(PropNames.Engine.PROP_INTERLINE_SPACE, newSpacing)
                    commitChanges()
                }
            }
        }
        mBtnLineSpacingInc.setOnClickListener {
            val prevSpacing = mProps.getInt(PropNames.Engine.PROP_INTERLINE_SPACE, 100)
            val prevSpacingIndex = Utils.findNearestIndex(LINE_SPACE_VALUES, prevSpacing)
            val newSpacingIndex = prevSpacingIndex + 1
            if (newSpacingIndex < LINE_SPACE_VALUES.size) {
                val newSpacing = LINE_SPACE_VALUES[newSpacingIndex]
                if (prevSpacing != newSpacing) {
                    mProps.setInt(PropNames.Engine.PROP_INTERLINE_SPACE, newSpacing)
                    commitChanges()
                }
            }
        }
    }

    companion object {
        // Exactly as in crengine-ng
        private val INDENTS_VALUES = intArrayOf(
            0, 1, 2, 3, 4, 5, 8, 9,
            10, 11, 12, 13, 14, 15, 16,
            20, 25, 30, 40, 50, 60, 80,
            100, 130, 150, 200 /*,300*/
        )
        private val LINE_SPACE_VALUES = intArrayOf(
            80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
            90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
            100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
            110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
            120, 125, 130, 135, 140, 145, 150, 155, 160, 165,
            170, 175, 180, 185, 190, 195, 200
        )
    }
}