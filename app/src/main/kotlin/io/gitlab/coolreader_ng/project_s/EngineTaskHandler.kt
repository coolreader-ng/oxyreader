/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

class EngineTaskHandler constructor(val mTask: EngineTask) : Runnable {

    private inner class WaitThread : Thread("EngineTaskHandler.WaitThread") {
        override fun run() {
            if (mTask.requireWait.get()) {
                synchronized(mTask.waitObject) {
                    if (LOG_ENGINE_TASKS)
                        log.info("waiting for a task to complete on another thread: " + mTask.javaClass.name)
                    mTask.waitObject.wait()
                }
            }
            // post done callback
            BackgroundThread.postGUI {
                if (LOG_ENGINE_TASKS)
                    log.info("running task.done() " + mTask.javaClass.name + " in gui thread")
                mTask.done()
            }
        }
    }

    private var waitThread: WaitThread? = null

    companion object {
        private val log = SRLog.create("enth")
        const val LOG_ENGINE_TASKS = false
    }

    override fun toString(): String {
        return "[handler for $mTask]"
    }

    override fun run() {
        // Always runs on a background thread.
        try {
            if (LOG_ENGINE_TASKS)
                log.info("running task.work() " + mTask.javaClass.name)
            // run task
            mTask.work()
            if (LOG_ENGINE_TASKS)
                log.info("exited task.work() " + mTask.javaClass.name)
            if (mTask.requireWait.get()) {
                if (LOG_ENGINE_TASKS)
                    log.info("the task needs to be completed on another thread " + mTask.javaClass.name)
                // wait for the task to complete in a separate thread
                waitThread = WaitThread()
                waitThread!!.start()
            } else {
                // post success callback
                BackgroundThread.postGUI {
                    if (LOG_ENGINE_TASKS)
                        log.info("running task.done() " + mTask.javaClass.name + " in gui thread")
                    mTask.done()
                }
            }
        } catch (e: Exception) {
            log.error("exception while running task " + mTask.javaClass.name, e)
            // post error callback
            BackgroundThread.postGUI {
                log.error("running task.fail(" + e.message + ") " + mTask.javaClass.simpleName + " in gui thread ")
                mTask.fail(e)
            }
        }
    }

}