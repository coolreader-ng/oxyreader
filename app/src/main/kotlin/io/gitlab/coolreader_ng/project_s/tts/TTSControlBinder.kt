/*
 * LxReader - book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader TTS module at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2015 by Vadim Lopatin <coolreader.org@gmail.com>
 * Copyright (C) 2020,2021 by Aleksey Chernov <valexlin@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s.tts

import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.Handler
import android.speech.tts.Voice
import androidx.annotation.RequiresApi
import io.gitlab.coolreader_ng.project_s.BookInfo
import java.util.Locale

/**
 * Class used for the client Binder.  Because we know this service always
 * runs in the same process as its clients, we don't need to deal with IPC.
 *
 * Most of the functions in this class use callbacks to return a result.
 * We use the caller's thread handle for this, so the callback is executed on the caller's thread.
 */
class TTSControlBinder(val service: TTSControlService) : Binder() {

    fun initTTS(engine: String?, listener: OnTTSCreatedListener?) {
        service.initTTS(engine, listener)
    }

    fun setMediaItemInfo(bookInfo: BookInfo) {
        service.setMediaItemInfo(bookInfo)
    }

    fun getState(callback: TTSControlService.RetrieveStateCallback) {
        service.retrieveState(callback, Handler())
    }

    fun say(utterance: String?, callback: TTSControlService.BooleanResultCallback?) {
        service.say(utterance, callback, Handler())
    }

    fun pause(callback: TTSControlService.BooleanResultCallback?) {
        service.pause(callback, Handler())
    }

    fun stopUtterance(callback: TTSControlService.BooleanResultCallback?) {
        service.stopUtterance(callback, Handler())
    }

    fun stop(callback: TTSControlService.BooleanResultCallback?) {
        service.stop(callback, Handler())
    }

    fun retrieveAvailableEngines(callback: TTSControlService.RetrieveEnginesListCallback) {
        service.retrieveAvailableEngines(callback, Handler())
    }

    fun retrieveAvailableLocales(callback: TTSControlService.RetrieveLocalesListCallback) {
        service.retrieveAvailableLocales(callback, Handler())
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun retrieveAvailableVoices(
        locale: Locale,
        callback: TTSControlService.RetrieveVoicesListCallback
    ) {
        service.retrieveAvailableVoices(locale, callback, Handler())
    }

    fun retrieveLanguage(callback: TTSControlService.StringResultCallback) {
        service.retrieveLanguage(callback, Handler())
    }

    fun setLanguage(langTag: String, callback: TTSControlService.BooleanResultCallback?) {
        service.setLanguage(langTag, callback, Handler())
    }

    fun retrieveVoice(callback: TTSControlService.RetrieveVoiceCallback) {
        service.retrieveVoice(callback, Handler())
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setVoice(voice: Voice, callback: TTSControlService.BooleanResultCallback?) {
        service.setVoice(voice, callback, Handler())
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun setVoice(voiceName: String, callback: TTSControlService.BooleanResultCallback?) {
        service.setVoice(voiceName, callback, Handler())
    }

    fun retrieveSpeechRate(callback: TTSControlService.FloatResultCallback) {
        service.retrieveSpeechRate(callback, Handler())
    }

    fun setSpeechRate(rate: Float, callback: TTSControlService.BooleanResultCallback?) {
        service.setSpeechRate(rate, callback, Handler())
    }

    fun retrieveVolume(callback: TTSControlService.VolumeResultCallback) {
        service.retrieveVolume(callback, Handler())
    }

    fun setVolume(volume: Int) {
        service.setVolume(volume)
    }

    fun getStatusListener(): OnTTSStatusListener? {
        return service.getStatusListener()
    }

    fun setStatusListener(listener: OnTTSStatusListener?) {
        service.setStatusListener(listener)
    }

    fun retrieveUseEndOfSentenceWorkaround(callback: TTSControlService.BooleanResultCallback) {
        service.retrieveUseEndOfSentenceWorkaround(callback, Handler())
    }

    fun setUseEndOfSentenceWorkaround(value: Boolean) {
        service.setUseEndOfSentenceWorkaround(value)
    }

    fun retrieveUseDictionary(callback: TTSControlService.BooleanResultCallback) {
        service.retrieveUseDictionary(callback, Handler())
    }

    fun retrieveDictionarySize(callback: TTSControlService.IntResultCallback) {
        service.retrieveDictionarySize(callback, Handler())
    }

    fun setUseDictionary(value: Boolean) {
        service.setUseDictionary(value)
    }

    fun setDictionary(
        dictionary: HashMap<String, String>,
        callback: TTSControlService.BooleanResultCallback?
    ) {
        service.setDictionary(dictionary, callback, Handler())
    }

    fun importDictionary(uri: Uri, callback: TTSControlService.BooleanResultCallback?) {
        service.importDictionary(uri, callback, Handler())
    }

    fun setUseStopTimeout(useTimeout: Boolean, timeout: Long) {
        service.setUseStopTimeout(useTimeout, timeout)
    }

    fun setUseImmobilityTimeout(useTimeout: Boolean, timeout: Long) {
        service.setUseImmobilityTimeout(useTimeout, timeout)
    }
}
