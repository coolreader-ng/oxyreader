/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.content.res.TypedArray
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.updatePadding
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.bottomappbar.BottomAppBar
import io.gitlab.coolreader_ng.project_s.extensions.inFullscreenWindow
import java.io.ByteArrayInputStream
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.InputStreamReader


class AboutFragment : Fragment(), AbleBackwards {

    private inner class AboutWebViewClient : WebViewClient() {
        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldInterceptRequest(
            view: WebView?,
            request: WebResourceRequest?
        ): WebResourceResponse? {
            return processAboutRequest(request?.url?.toString())
        }

        @Deprecated(
            "Deprecated in Java",
            ReplaceWith("shouldInterceptRequest(view, request)", "android.webkit.WebViewClient")
        )
        override fun shouldInterceptRequest(view: WebView?, url: String?): WebResourceResponse? {
            return processAboutRequest(url)
        }
    }

    private fun processAboutRequest(url: String?): WebResourceResponse? {
        if (null != url && url.startsWith("help:///about/")) {
            val fileName = url.substring(14)
            val assetMan = resources.assets
            var data: String
            var reasonPhrase: String
            var istream: InputStream? = null
            var statusCode: Int
            try {
                val dataBuilder = StringBuilder()
                istream = assetMan.open(fileName)
                val isr = InputStreamReader(istream, "utf-8")
                val buffer = CharArray(4096)
                var ret: Int
                while (true) {
                    ret = isr.read(buffer, 0, buffer.size)
                    if (ret > 0)
                        dataBuilder.appendRange(buffer, 0, ret)
                    else
                        break
                }
                data = dataBuilder.toString()
                for ((rplKey, rplValue) in mReplacementMap) {
                    data = data.replace(rplKey, rplValue, false)
                }
                statusCode = 200
                reasonPhrase = "OK"
            } catch (e: FileNotFoundException) {
                log.error("File not found: $e")
                statusCode = 404
                reasonPhrase = "Not Found"
                data = e.toString()
            } catch (e: SecurityException) {
                log.error("SecurityException: $e")
                statusCode = 403
                reasonPhrase = "Forbidden"
                data = e.toString()
            } catch (e: Exception) {
                log.error("Failed to read asset: $e")
                statusCode = 500
                reasonPhrase = "Internal Server Error"
                data = e.toString()
            } finally {
                istream?.close()
            }
            val response = WebResourceResponse(
                "text/html",
                "utf-8",
                ByteArrayInputStream(data.toByteArray(Charsets.UTF_8))
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                response.setStatusCodeAndReasonPhrase(statusCode, reasonPhrase)
            return response
        }
        return null
    }

    private val mReplacementMap = HashMap<String, String>()
    private var mToolBarLocation = PropNames.App.TOOLBAR_LOCATION_NONE
    private var mVersion = "-0.0"

    private lateinit var mWebView: WebView

    override fun onBackPressed(): Boolean {
        if (mWebView.canGoBack()) {
            mWebView.goBack()
            return true
        }
        return false
    }

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mToolBarLocation = it.getInt("toolbar")
            mVersion = it.getString("version") ?: "-0.0"
        }

        val attrs = intArrayOf(
            android.R.attr.textColor,
            android.R.attr.textColorLink,
        )
        val a: TypedArray = requireContext().obtainStyledAttributes(attrs)
        val textColor = a.getColor(0, Color.BLACK)
        val linkColor = a.getColor(1, Color.BLUE)
        a.recycle()

        val appName = getString(R.string.app_name)

        mReplacementMap[RPL_TEXT_COLOR] = Utils.colorToHtmlHexString(textColor, false)
        mReplacementMap[RPL_LINK_COLOR] = Utils.colorToHtmlHexString(linkColor, false)
        mReplacementMap[RPL_APP_NAME] = appName
        mReplacementMap[RPL_APP_VERSION] = mVersion
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.about_panel, container, false)
        view.requestFocus()
        return view
    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val appBarLayout = view.findViewById<AppBarLayout>(R.id.appBarLayout)
        val topAppBar = view.findViewById<MaterialToolbar>(R.id.topAppBar)
        val bottomAppBar = view.findViewById<BottomAppBar>(R.id.bottomAppBar)
        val nestedScrollView = view.findViewById<NestedScrollView>(R.id.nestedScrollView)
        val viewVersion = view.findViewById<TextView>(R.id.viewProgVersion)
        val viewBuildType = view.findViewById<TextView>(R.id.viewBuildType)
        mWebView = view.findViewById(R.id.webView)

        val a: TypedArray = requireContext().obtainStyledAttributes(
            intArrayOf(android.R.attr.actionBarSize)
        )
        val bottomToolbarSize = a.getDimensionPixelSize(0, 0)
        a.recycle()
        when (mToolBarLocation) {
            PropNames.App.TOOLBAR_LOCATION_NONE -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_TOP -> {
                appBarLayout.visibility = View.VISIBLE
                bottomAppBar.visibility = View.GONE
            }

            PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.VISIBLE
                nestedScrollView.setPadding(0, 0, 0, bottomToolbarSize)
            }

            else -> {
                appBarLayout.visibility = View.GONE
                bottomAppBar.visibility = View.GONE
            }
        }
        viewVersion.text = getString(R.string.version_s, mReplacementMap[RPL_APP_VERSION])
        viewBuildType.text = getString(R.string.build_type_s, BuildConfig.BUILD_TYPE)

        mWebView.setBackgroundColor(Color.TRANSPARENT)
        mWebView.webViewClient = AboutWebViewClient()

        // Check localized version
        val candidates = arrayOf(
            "about_main_${SettingsManager.activeLang.langTag}.html",
            "about_main_${SettingsManager.activeLang.language}.html",
            "about_main_en.html"    // English as fallback
        )
        var mainFileName = ""
        for (candidate in candidates) {
            try {
                val stream = resources.assets.open(candidate)
                stream.close()
                mainFileName = candidate
                break
            } catch (_: Exception) {
            }
        }
        mWebView.loadUrl("help:///about/${mainFileName}")

        ViewCompat.setOnApplyWindowInsetsListener(view) { _, windowInsets ->
            val mask = if (inFullscreenWindow)
                WindowInsetsCompat.Type.displayCutout()
            else
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            val insets = windowInsets.getInsets(mask)
            var nsInsets = insets
            if (appBarLayout.isVisible) {
                topAppBar.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    top = insets.top
                )
                nsInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
            } else if (bottomAppBar.isVisible) {
                bottomAppBar.updatePadding(
                    left = insets.left,
                    right = insets.right,
                    bottom = insets.bottom
                )
                nsInsets = Insets.of(
                    insets.left,
                    insets.top,
                    insets.right,
                    bottomToolbarSize + insets.bottom
                )
            }
            nestedScrollView.updatePadding(
                nsInsets.left,
                nsInsets.top,
                nsInsets.right,
                nsInsets.bottom
            )
            WindowInsetsCompat.CONSUMED
        }

        val closeAction: () -> Unit = {
            if (!onBackPressed()) {
                if (parentFragmentManager.backStackEntryCount > 0)
                    parentFragmentManager.popBackStack()
            }
        }
        topAppBar.setNavigationOnClickListener {
            closeAction()
        }
        bottomAppBar.setNavigationOnClickListener {
            closeAction()
        }
    }

    companion object {
        private val log = SRLog.create("about")
        private const val RPL_TEXT_COLOR = "@TEXT_COLOR@"
        private const val RPL_LINK_COLOR = "@LINK_COLOR@"
        private const val RPL_APP_NAME = "@APP_NAME@"
        private const val RPL_APP_VERSION = "@APP_VERSION@"
    }
}
