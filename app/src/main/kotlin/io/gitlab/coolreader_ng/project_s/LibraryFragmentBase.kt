/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor

// Each fragment must have a default constructor
abstract class LibraryFragmentBase() : Fragment() {

    internal interface PropertiesObserver {
        fun dbServiceAccessorIsSet(dbServiceAccessor: DBServiceAccessor?)
        fun bookCoverManagerIsSet(bookCoverManager: BookCoverManager?)
        fun settingsIsSet(settings: SRProperties?)
    }

    internal var dbServiceAccessor: DBServiceAccessor? = null
        set(value) {
            field = value
            propertiesObserver?.dbServiceAccessorIsSet(field)
        }
    internal var bookCoverManager: BookCoverManager? = null
        set(value) {
            field = value
            propertiesObserver?.bookCoverManagerIsSet(field)
        }
    internal val crEngineNGBinding: CREngineNGBinding?
        get() = bookCoverManager?.crEngineNGBinding
    internal var settings: SRProperties? = null
        set(value) {
            field = value
            propertiesObserver?.settingsIsSet(field)
        }
    internal var customTag: Any? = null
    internal var finishWhenOpeningBook = false
    internal var propertiesObserver: PropertiesObserver? = null

    constructor(
        dbServiceAccessor: DBServiceAccessor? = null,
        bookCoverManager: BookCoverManager? = null,
        settings: SRProperties? = null
    ) : this() {
        this.dbServiceAccessor = dbServiceAccessor
        this.bookCoverManager = bookCoverManager
        this.settings = settings
    }

    /**
     * Called when the fragment needs to update its view.
     */
    protected abstract fun onUpdateViewImpl()

    /**
     * Called when the fragment becomes inactive.
     */
    internal abstract fun onHideFragment()

    /**
     * Force update of this fragment's view
     */
    fun requestFragmentUpdate() {
        BackgroundThread.executeGUI {
            onUpdateViewImpl()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            val keySet = it.keySet()
            if (keySet.contains("customTag")) {
                if (null == customTag)
                    customTag = it.getString("customTag")
            }
            if (keySet.contains("finishWhenOpeningBook"))
                finishWhenOpeningBook = it.getBoolean("finishWhenOpeningBook", false)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("customTag", customTag?.toString())
        outState.putBoolean("finishWhenOpeningBook", finishWhenOpeningBook)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH &&
            Build.VERSION.SDK_INT < Build.VERSION_CODES.R
        ) {
            // For some reason on child fragments inside ViewPager
            //  setOnApplyWindowInsetsListener() has no effect on API < 30.
            // So forcing this event
            view?.requestApplyInsets()
        }
        onUpdateViewImpl()
    }

}
