/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import kotlin.math.asin
import kotlin.math.roundToInt
import kotlin.math.sin

internal class NaturalPageFlipAnimation constructor(
    dir: Int,
    page1: PageImageCache,
    page2: PageImageCache,
    nightMode: Boolean
) :
    AbstractAnimation() {

    private val mPage1: PageImageCache = page1
    private val mPage2: PageImageCache = page2
    private val isNightMode: Boolean = nightMode
    private val mPageCount: Int = mPage1.position!!.pageMode
    private var mPos = 0
    private var mSpeed = 50f
    private val mSrcRect1 = Rect()
    private val mDstRect1 = Rect()
    private val mSrcRect2 = Rect()
    private val mDstRect2 = Rect()
    private val mSrcRect3 = Rect()
    private val mDstRect3 = Rect()
    private val mShadowRect = Rect()
    private val mWidth: Int = mPage1.position!!.pageWidth
    private val mHeight: Int = mPage1.position!!.fullHeight
    private var mDone: Boolean = false
    private val mShadePaints: Array<Paint?>
    private val mHilitePaints: Array<Paint?>

    override val direction: Int = dir

    override val isAnimationComplete: Boolean
        get() = mDone

    override val position: Int
        get() = if (direction > 0) mPos else -mPos

    override fun setSpeed(speed: Float) {
        mSpeed = speed
    }

    override fun autoIncrementPosition(acceleration: Float) {
        // TODO: use a physical model of smooth acceleration/deceleration
        if (mDone)
            return
        mPos = (mPos.toFloat() + mSpeed).toInt()
        if (mPos >= mWidth) {
            mPos = mWidth - 1
            mDone = true
        }
    }

    override fun autoDecrementPosition(acceleration: Float) {
        // TODO: use a physical model of smooth acceleration/deceleration
        if (mDone)
            return
        mPos = (mPos.toFloat() - mSpeed).toInt()
        if (mPos < 0) {
            mPos = 0
            mDone = true
        }
    }

    override fun overridePosition(x: Int, y: Int) {
        if (direction > 0)
            mPos = -x
        else
            mPos = x
        if (mPos < 0)
            mPos = 0
        else if (mPos >= mWidth)
            mPos = mWidth
    }

    override fun resetPosition() {
        mPos = 0
    }

    override fun renderFrame(canvas: Canvas): Boolean {
        if (mPage1.isReleased || mPage2.isReleased) return false
        val div: Int
        if (direction > 0) {
            // from right to left
            div = mWidth - mPos
            mShadowRect[div, 0, div + mWidth / 10] = mHeight
            if (2 == mPageCount) {
                val w2 = mWidth / 2
                if (div < w2) {
                    // left - part of old page
                    mSrcRect1.set(0, 0, div, mHeight)
                    mDstRect1.set(0, 0, div, mHeight)
                    canvas.drawBitmap(mPage1.bitmap!!, mSrcRect1, mDstRect1, null)
                    // left, resized part of new page
                    mSrcRect2.set(0, 0, w2, mHeight)
                    mDstRect2.set(div, 0, w2, mHeight)
                    drawDistorted(canvas, mPage2.bitmap, mSrcRect2, mDstRect2, -1)
                    // right, new page
                    mSrcRect3.set(w2, 0, mWidth, mHeight)
                    mDstRect3.set(w2, 0, mWidth, mHeight)
                    canvas.drawBitmap(mPage2.bitmap!!, mSrcRect3, mDstRect3, null)
                } else {
                    // left - old page
                    mSrcRect1.set(0, 0, w2, mHeight)
                    mDstRect1.set(0, 0, w2, mHeight)
                    canvas.drawBitmap(mPage1.bitmap!!, mSrcRect1, mDstRect1, null)
                    // right, resized old page
                    mSrcRect2.set(w2, 0, mWidth, mHeight)
                    mDstRect2.set(w2, 0, div, mHeight)
                    drawDistorted(canvas, mPage1.bitmap, mSrcRect2, mDstRect2, 1)
                    // right, new page
                    mSrcRect3.set(div, 0, mWidth, mHeight)
                    mDstRect3.set(div, 0, mWidth, mHeight)
                    canvas.drawBitmap(mPage2.bitmap!!, mSrcRect3, mDstRect3, null)

                    //if (div > 0 && div < m_width)
                    //    drawShadow(canvas, m_shadowRect);
                }
            } else {
                mSrcRect1.set(0, 0, mWidth, mHeight)
                mDstRect1.set(0, 0, mWidth - mPos, mHeight)
                mSrcRect2.set(mWidth - mPos, 0, mWidth, mHeight)
                mDstRect2.set(mWidth - mPos, 0, mWidth, mHeight)
                canvas.drawBitmap(mPage2.bitmap!!, mSrcRect2, mDstRect2, null)
                drawDistorted(canvas, mPage1.bitmap, mSrcRect1, mDstRect1, 1)
            }
        } else {
            // from left to right
            div = mPos
            mShadowRect[div, 0, div + 10] = mHeight
            if (2 == mPageCount) {
                val w2 = mWidth / 2
                if (div < w2) {
                    // left - part of old page
                    mSrcRect1.set(0, 0, div, mHeight)
                    mDstRect1.set(0, 0, div, mHeight)
                    canvas.drawBitmap(mPage2.bitmap!!, mSrcRect1, mDstRect1, null)
                    // left, resized part of new page
                    mSrcRect2.set(0, 0, w2, mHeight)
                    mDstRect2.set(div, 0, w2, mHeight)
                    drawDistorted(canvas, mPage1.bitmap, mSrcRect2, mDstRect2, -1)
                    // right, new page
                    mSrcRect3.set(w2, 0, mWidth, mHeight)
                    mDstRect3.set(w2, 0, mWidth, mHeight)
                    canvas.drawBitmap(mPage1.bitmap!!, mSrcRect3, mDstRect3, null)
                } else {
                    // left - old page
                    mSrcRect1.set(0, 0, w2, mHeight)
                    mDstRect1.set(0, 0, w2, mHeight)
                    canvas.drawBitmap(mPage2.bitmap!!, mSrcRect1, mDstRect1, null)
                    // right, resized old page
                    mSrcRect2.set(w2, 0, mWidth, mHeight)
                    mDstRect2.set(w2, 0, div, mHeight)
                    drawDistorted(canvas, mPage2.bitmap, mSrcRect2, mDstRect2, 1)
                    // right, new page
                    mSrcRect3.set(div, 0, mWidth, mHeight)
                    mDstRect3.set(div, 0, mWidth, mHeight)
                    canvas.drawBitmap(mPage1.bitmap!!, mSrcRect3, mDstRect3, null)

                    //if (div > 0 && div < m_width)
                    //    drawShadow(canvas, m_shadowRect);
                }
            } else {
                mSrcRect1.set(mPos, 0, mWidth, mHeight)
                mDstRect1.set(mPos, 0, mWidth, mHeight)
                mSrcRect2.set(0, 0, mWidth, mHeight)
                mDstRect2.set(0, 0, mPos, mHeight)
                canvas.drawBitmap(mPage1.bitmap!!, mSrcRect1, mDstRect1, null)
                drawDistorted(canvas, mPage2.bitmap, mSrcRect2, mDstRect2, 1)
            }
        }
        if (div in 1 until mWidth)
            drawShadow(canvas, mShadowRect)
        return true
    }

    private fun drawDistorted(canvas: Canvas, bmp: Bitmap?, src: Rect, dst: Rect, dir: Int) {
        val srcdx = src.width()
        val dstdx = dst.width()
        val dx = srcdx - dstdx
        val maxdistortdx = srcdx * DISTORT_PART_PERCENT / 100
        val maxdx = maxdistortdx * (PI_DIV_2 - SIN_TABLE_SCALE) / SIN_TABLE_SCALE
        val maxdistortsrc = maxdistortdx * PI_DIV_2 / SIN_TABLE_SCALE
        var distortdx = dx.coerceAtMost(maxdistortdx)
        var distortsrcstart = -1
        var distortsrcend = -1
        var distortdststart = -1
        var distortdstend = -1
        var distortanglestart = -1
        var distortangleend = -1
        var normalsrcstart = -1
        var normalsrcend = -1
        var normaldststart = -1
        var normaldstend = -1
        if (dx < maxdx) {
            // start
            var index = if (dx >= 0) dx * SIN_TABLE_SIZE / maxdx else 0
            if (index > DST_TABLE.size) index = DST_TABLE.size
            val dstv = DST_TABLE[index] * maxdistortdx / SIN_TABLE_SCALE
            distortsrcstart = dstdx - dstv
            distortdststart = distortsrcstart
            distortsrcend = srcdx
            distortdstend = dstdx
            normaldststart = 0
            normalsrcstart = normaldststart
            normalsrcend = distortsrcstart
            normaldstend = distortdststart
            distortanglestart = 0
            distortangleend = SRC_TABLE[index]
            distortdx = maxdistortdx
        } else if (dstdx > maxdistortdx) {
            // middle
            distortsrcstart = dstdx - maxdistortdx
            distortdststart = distortsrcstart
            distortsrcend = distortsrcstart + maxdistortsrc
            distortdstend = dstdx
            normaldststart = 0
            normalsrcstart = normaldststart
            normalsrcend = distortsrcstart
            normaldstend = distortdststart
            distortanglestart = 0
            distortangleend = PI_DIV_2
        } else {
            // end
            normaldstend = -1
            normalsrcend = normaldstend
            normaldststart = normalsrcend
            normalsrcstart = normaldststart
            distortdx = dstdx
            distortsrcstart = 0
            var n = if (maxdistortdx >= dstdx) maxdistortdx - dstdx else 0
            distortsrcend =
                ASIN_TABLE[SIN_TABLE_SIZE * n / maxdistortdx] * maxdistortsrc / SIN_TABLE_SCALE
            distortdststart = 0
            distortdstend = dstdx
            distortangleend = PI_DIV_2
            n = if (maxdistortdx >= distortdx) maxdistortdx - distortdx else 0
            distortanglestart =
                ASIN_TABLE[SIN_TABLE_SIZE * (maxdistortdx - distortdx) / maxdistortdx]
        }
        val srcrc = Rect(src)
        val dstrc = Rect(dst)
        if (normalsrcstart < normalsrcend) {
            if (dir > 0) {
                srcrc.left = src.left + normalsrcstart
                srcrc.right = src.left + normalsrcend
                dstrc.left = dst.left + normaldststart
                dstrc.right = dst.left + normaldstend
            } else {
                srcrc.right = src.right - normalsrcstart
                srcrc.left = src.right - normalsrcend
                dstrc.right = dst.right - normaldststart
                dstrc.left = dst.right - normaldstend
            }
            canvas.drawBitmap(bmp!!, srcrc, dstrc, null)
        }
        if (distortdststart < distortdstend) {
            val n = distortdx / 5 + 1
            val dst0 =
                SIN_TABLE[distortanglestart * SIN_TABLE_SIZE / PI_DIV_2] * maxdistortdx / SIN_TABLE_SCALE
            val src0 = distortanglestart * maxdistortdx / SIN_TABLE_SCALE
            for (i in 0 until n) {
                val angledelta = distortangleend - distortanglestart
                val startangle = distortanglestart + i * angledelta / n
                val endangle = distortanglestart + (i + 1) * angledelta / n
                val src1 = startangle * maxdistortdx / SIN_TABLE_SCALE - src0
                val src2 = endangle * maxdistortdx / SIN_TABLE_SCALE - src0
                val dst1 =
                    SIN_TABLE[startangle * SIN_TABLE_SIZE / PI_DIV_2] * maxdistortdx / SIN_TABLE_SCALE - dst0
                val dst2 =
                    SIN_TABLE[endangle * SIN_TABLE_SIZE / PI_DIV_2] * maxdistortdx / SIN_TABLE_SCALE - dst0
                val hiliteIndex = startangle * mHilitePaints.size / PI_DIV_2
                var paints: Array<Paint?>
                if (dir > 0) {
                    dstrc.left = dst.left + distortdststart + dst1
                    dstrc.right = dst.left + distortdststart + dst2
                    srcrc.left = src.left + distortsrcstart + src1
                    srcrc.right = src.left + distortsrcstart + src2
                    paints = mHilitePaints
                } else {
                    dstrc.right = dst.right - distortdststart - dst1
                    dstrc.left = dst.right - distortdststart - dst2
                    srcrc.right = src.right - distortsrcstart - src1
                    srcrc.left = src.right - distortsrcstart - src2
                    paints = mShadePaints
                }
                canvas.drawBitmap(bmp!!, srcrc, dstrc, null)
                canvas.drawRect(dstrc, paints[hiliteIndex]!!)
            }
        }
    }

    private fun drawShadow(canvas: Canvas, rc: Rect) {
        drawGradient(canvas, rc, mShadePaints, mShadePaints.size / 2, mShadePaints.size / 10)
    }

    private fun drawGradient(
        canvas: Canvas,
        rc: Rect,
        paints: Array<Paint?>,
        startIndex: Int,
        endIndex: Int
    ) {
        val n = if (startIndex < endIndex) endIndex - startIndex + 1 else startIndex - endIndex + 1
        val dir = if (startIndex < endIndex) 1 else -1
        val dx = rc.right - rc.left
        val rect = Rect(rc)
        for (i in 0 until n) {
            val index = startIndex + i * dir
            val x1 = rc.left + dx * i / n
            var x2 = rc.left + dx * (i + 1) / n
            if (x2 > rc.right) x2 = rc.right
            rect.left = x1
            rect.right = x2
            if (x2 > x1)
                canvas.drawRect(rect, paints[index]!!)
        }
    }

    companion object {
        private const val SIN_TABLE_SIZE = 1024
        private const val SIN_TABLE_SCALE = 0x10000
        private const val PI_DIV_2 = (Math.PI / 2 * SIN_TABLE_SCALE).toInt()

        /// sin table, for 0..PI/2
        private val SIN_TABLE = IntArray(SIN_TABLE_SIZE + 1)
        private val ASIN_TABLE = IntArray(SIN_TABLE_SIZE + 1)

        // mapping of 0..1 shift to angle
        private val SRC_TABLE = IntArray(SIN_TABLE_SIZE + 1)

        // mapping of 0..1 shift to sin(angle)
        private val DST_TABLE = IntArray(SIN_TABLE_SIZE + 1)
        private const val DISTORT_PART_PERCENT = 30

        // for dx=0..1 find such alpha (0..pi/2) that alpha - sin(alpha) = dx
        private fun shiftfn(dx: Double): Double {
            var a = 0.0
            var b = Math.PI / 2
            var c = 0.0
            for (i in 0..14) {
                c = (a + b) / 2
                val cq = c - sin(c)
                if (cq < dx) a = c else b = c
            }
            return c
        }

        init {
            for (i in 0..SIN_TABLE_SIZE) {
                var angle: Double = Math.PI / 2 * i / SIN_TABLE_SIZE
                var s: Int = (sin(angle) * SIN_TABLE_SCALE).roundToInt()
                SIN_TABLE[i] = s
                val x: Double = i.toDouble() / SIN_TABLE_SIZE
                s = (asin(x) * SIN_TABLE_SCALE).roundToInt()
                ASIN_TABLE[i] = s
                val dx: Double = i * (Math.PI / 2 - 1.0) / SIN_TABLE_SIZE
                angle = shiftfn(dx)
                SRC_TABLE[i] = (angle * SIN_TABLE_SCALE).roundToInt()
                DST_TABLE[i] = (sin(angle) * SIN_TABLE_SCALE).roundToInt()
            }
        }
    }

    init {
        val numPaints = 256
        mShadePaints = arrayOfNulls(numPaints)
        mHilitePaints = arrayOfNulls(numPaints)
        for (i in 0 until numPaints) {
            mShadePaints[i] = Paint()
            mShadePaints[i]!!.style = Paint.Style.FILL
            mHilitePaints[i] = Paint()
            mHilitePaints[i]!!.style = Paint.Style.FILL
            if (isNightMode) {
                mShadePaints[i]!!.color = Color.argb((i + 1) * 96 / numPaints, 0, 0, 0)
                mHilitePaints[i]!!.color = Color.argb((i + 1) * 96 / numPaints, 64, 64, 64)
            } else {
                mShadePaints[i]!!.color = Color.argb((i + 1) * 96 / numPaints, 0, 0, 0)
                mHilitePaints[i]!!.color = Color.argb((i + 1) * 96 / numPaints, 255, 255, 255)
            }
        }
    }
}