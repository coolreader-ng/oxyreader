/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s.db

import android.content.ComponentName
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import io.gitlab.coolreader_ng.project_s.SRLog


class DBServiceAccessor(private var contextWrapper: ContextWrapper) {

    interface Callback {
        fun run(binder: DBServiceBinder)
    }

    private var mBinder: DBServiceBinder? = null
    private var mServiceBound = false
    private var bindIsCalled = false
    private val onConnectCallbacks = ArrayList<Callback>()
    private val mLocker = Any()

    /**
     * Execute a runnable on the calling thread with a reference to the service attached.
     */
    fun runWithService(callback: Callback) {
        bind(callback)
    }

    /**
     * Post runnable to run in a background DB service thread.
     * @param runnable runnable to post
     */
    fun runOnService(runnable: Runnable) {
        synchronized(mLocker) {
            if (null != mBinder) {
                mBinder?.service?.runOn(runnable)
            }
        }
    }

    /**
     * Bind service to using initial contextWrapper
     */
    fun bind(boundCallback: Callback? = null) {
        synchronized(mLocker) {
            mBinder?.let {
                log.verbose("DBService is already bound")
                boundCallback?.run(it)
                return
            }
            //log.verbose("binding DBService");
            if (boundCallback != null) {
                synchronized(onConnectCallbacks) {
                    onConnectCallbacks.add(boundCallback)
                }
            }
            if (!bindIsCalled) {
                bindIsCalled = true
                if (contextWrapper.bindService(
                        Intent(contextWrapper, DBService::class.java),
                        mServiceConnection,
                        Context.BIND_AUTO_CREATE
                    )
                ) {
                    mServiceBound = true
                    log.verbose("binding DBService in progress...")
                } else {
                    log.error("cannot bind DBService")
                }
            }
        }
    }

    fun unbind() {
        log.verbose("unbinding DBService")
        synchronized(mLocker) {
            if (mServiceBound) {
                // Detach our existing connection.
                contextWrapper.unbindService(mServiceConnection)
                mServiceBound = false
                bindIsCalled = false
                mBinder = null
            }
        }
    }

    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            synchronized(mLocker) {
                mBinder = service as DBServiceBinder
                log.info("connected to DBService")
                synchronized(onConnectCallbacks) {
                    if (onConnectCallbacks.isNotEmpty()) {
                        // run once
                        for (callback in onConnectCallbacks)
                            callback.run(mBinder!!)
                        onConnectCallbacks.clear()
                    }
                }
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            synchronized(mLocker) {
                mServiceBound = false
                bindIsCalled = false
                mBinder = null
            }
            log.info("disconnected from DBService")
        }
    }

    companion object {
        private val log = SRLog.create("dbsvc")
    }
}
