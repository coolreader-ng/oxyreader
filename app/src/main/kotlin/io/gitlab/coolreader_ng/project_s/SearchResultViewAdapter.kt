/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat

internal class SearchResultViewAdapter(private val itemList: List<SearchResultItem>) :
    RecyclerView.Adapter<SearchResultViewAdapter.SearchResultItemViewHolder>() {

    inner class SearchResultItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: SearchResultItem? = null
        val tvChapter: TextView = itemView.findViewById(R.id.tvChapter)
        val tvPercentPos: TextView = itemView.findViewById(R.id.tvPercentPos)
        val tvText: TextView = itemView.findViewById(R.id.tvText)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchResultItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.text_search_result_item, parent, false)
        return SearchResultItemViewHolder(itemView)
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: SearchResultItemViewHolder, position: Int) {
        val item: SearchResultItem? = if (position >= 0 && position < itemList.size)
            itemList[position]
        else
            null
        holder.item = item
        item?.let {
            val context = holder.tvText.context
            holder.tvChapter.text = it.title
            val numberFormat = NumberFormat.getPercentInstance(SettingsManager.activeLang.locale)
            numberFormat.minimumFractionDigits = 0
            numberFormat.maximumFractionDigits = 2
            holder.tvPercentPos.text = numberFormat.format(it.percent.toFloat() / 10000f)
            holder.tvText.text = it.textFragment
        }
    }
}
