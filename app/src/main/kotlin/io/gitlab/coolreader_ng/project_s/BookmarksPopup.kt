/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Paint
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow
import io.gitlab.coolreader_ng.project_s.extensions.setHyphenatedText
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener
import java.text.NumberFormat

class BookmarksPopup(private val parent: View, contentView: View) {

    interface OnBookmarkClickListener {
        fun onBookmarkClick(item: Bookmark)
        fun onAddClicked()
        fun onBookmarkChanged(bookmarks: List<Bookmark>)
    }

    private inner class BookmarkViewData {
        // The status of this Bookmark element is either selected (true) or not (false).
        var selected = false
            set(value) {
                field = value
                mSTBDeleteButton.isEnabled = isHaveSelectedBookmarks
                if (isAllBookmarksSelected) {
                    mSTBSelectAllButton.visibility = View.GONE
                    mSTBDeselectButton.visibility = View.VISIBLE
                } else {
                    mSTBSelectAllButton.visibility = View.VISIBLE
                    mSTBDeselectButton.visibility = View.GONE
                }
            }
    }

    private val mBookmarkViewProps = HashMap<Bookmark, BookmarkViewData>()

    // extension property for Bookmark
    private val Bookmark.viewProps: BookmarkViewData
        @Synchronized
        get() {
            var viewProps = mBookmarkViewProps[this]
            if (null == viewProps) {
                viewProps = BookmarkViewData()
                mBookmarkViewProps[this] = viewProps
            }
            return viewProps
        }

    var onBookmarkClickListener: OnBookmarkClickListener? = null
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mPopup: PopupWindow

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null
    private val mViewGroup: ViewGroup
    private val mToolbar: View
    private val mToolbarSelection: View
    private var mToolbarPosition: Int
    private val mBackButton: Button
    private val mAddButton: Button
    private val mBookNameView: TextView
    private val mSTBBackButton: Button
    private val mSTBSelectAllButton: Button
    private val mSTBDeselectButton: Button
    private val mSTBDeleteButton: Button
    private val mRecyclerView: RecyclerView
    private var mLangTag: String? = null
    internal var isSelectionMode = false
        set(value) {
            if (field != value) {
                field = value
                mAdapter.notifyItemRangeChanged(0, mBookmarks.size)
                if (field) {
                    mToolbar.visibility = View.GONE
                    mToolbarSelection.visibility = View.VISIBLE
                } else {
                    mToolbar.visibility = View.VISIBLE
                    mToolbarSelection.visibility = View.GONE
                    deselectAllBookmarks()
                }
            }
        }
    private var mBookmarks: ArrayList<Bookmark> = ArrayList()
    private val isHaveSelectedBookmarks: Boolean
        get() {
            for (bk in mBookmarks)
                if (bk.viewProps.selected)
                    return true
            return false
        }
    private val isAllBookmarksSelected: Boolean
        get() {
            if (mBookmarks.isNotEmpty()) {
                for (bk in mBookmarks) {
                    if (!bk.viewProps.selected) {
                        return false
                    }
                }
                return true
            }
            return false
        }

    private inner class BookmarksViewAdapter :
        RecyclerView.Adapter<BookmarksViewAdapter.BookmarksViewHolder>() {

        inner class BookmarksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var item: Bookmark? = null
            val checkBox: CheckBox = itemView.findViewById<CheckBox?>(R.id.checkBox).also {
                it.setOnCheckedChangeListener { _, isChecked ->
                    item?.viewProps?.selected = isChecked
                }
            }
            val emblemImage: ImageView = itemView.findViewById(R.id.emblem)
            val titleTextView: TextView = itemView.findViewById(R.id.titleText)
            val posTextView: TextView = itemView.findViewById(R.id.posText)
            val commentView: TextView = itemView.findViewById(R.id.comment)
            val percentView: TextView = itemView.findViewById(R.id.percent)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookmarksViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val itemView = layoutInflater.inflate(R.layout.bookmark_item, parent, false)
            return BookmarksViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: BookmarksViewHolder, position: Int) {
            val item: Bookmark? = if (position >= 0 && position < mBookmarks.size)
                mBookmarks[position]
            else
                null
            holder.item = item
            if (null != item) {
                if (Bookmark.TYPE_POSITION == item.type || Bookmark.TYPE_COMMENT == item.type || Bookmark.TYPE_CORRECTION == item.type) {
                    holder.titleTextView.setHyphenatedText(mLangTag, item.titleText)
                    holder.posTextView.setHyphenatedText(mLangTag, item.posText)
                    if (Bookmark.TYPE_CORRECTION == item.type) {
                        holder.posTextView.visibility = View.VISIBLE
                        holder.posTextView.paintFlags =
                            holder.posTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        holder.commentView.visibility = View.VISIBLE
                        holder.commentView.setHyphenatedText(mLangTag, item.commentText)
                    } else if (Bookmark.TYPE_COMMENT == item.type) {
                        holder.posTextView.visibility = View.VISIBLE
                        holder.commentView.visibility = View.GONE
                    } else {
                        // position
                        holder.posTextView.visibility = View.GONE
                        holder.commentView.visibility = View.GONE
                    }
                } else {
                    holder.posTextView.visibility = View.GONE
                    holder.commentView.visibility = View.GONE
                }
                val percent: Double = item.percent / 10000.0
                val numberFormat =
                    NumberFormat.getPercentInstance(SettingsManager.activeLang.locale)
                numberFormat.minimumFractionDigits = 2
                numberFormat.maximumIntegerDigits = 2
                holder.percentView.text = numberFormat.format(percent)
                if (isSelectionMode) {
                    holder.checkBox.visibility = View.VISIBLE
                    holder.checkBox.isChecked = item.viewProps.selected
                } else {
                    holder.checkBox.visibility = View.GONE
                }
            }
        }

        override fun getItemCount(): Int {
            return mBookmarks.size
        }
    }

    private val mAdapter = BookmarksViewAdapter()

    fun show(bookInfo: BookInfo) {
        bookInfo.title?.let { mBookNameView.text = it }
        bookInfo.language?.let { mLangTag = it }
        isSelectionMode = false
        mBookmarks.clear()
        // We can't use bookInfo.allBookmarks, since we don't need the last reading position here
        for (i in 0 until bookInfo.bookmarkCount) {
            val bmk = bookInfo.getBookmark(i)
            bmk.viewProps.selected = false
            mBookmarks.add(bmk)
        }
        mAdapter.notifyItemRangeInserted(0, mBookmarks.size)
        // TODO: for tablet devices (or in landscape position) occupy only part of screen but not full
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    /**
     * Sets toolbar position. May be PropNames.App.TOOLBAR_LOCATION_TOP or PropNames.App.TOOLBAR_LOCATION_BOTTOM
     */
    fun setToolbarPosition(pos: Int) {
        if (pos != mToolbarPosition) {
            val tool1InsPos: Int
            val tool2InsPos: Int
            when (pos) {
                PropNames.App.TOOLBAR_LOCATION_TOP -> {
                    mToolbarPosition = pos
                    tool1InsPos = 0
                    tool2InsPos = 1
                }

                PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                    mToolbarPosition = pos
                    tool1InsPos = 1
                    tool2InsPos = 2
                }

                else -> {
                    // This popup is completely useless without the toolbar.
                    mToolbarPosition = PropNames.App.TOOLBAR_LOCATION_TOP
                    tool1InsPos = 0
                    tool2InsPos = 1
                }
            }
            try {
                mViewGroup.removeView(mToolbar)
                mViewGroup.removeView(mToolbarSelection)
            } catch (_: Exception) {
            }
            val layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    0f
                )
            mViewGroup.addView(mToolbar, tool1InsPos, layoutParams)
            mViewGroup.addView(mToolbarSelection, tool2InsPos, layoutParams)
        }
    }

    private fun selectAllBookmark() {
        for ((i, bk) in mBookmarks.withIndex()) {
            if (!bk.viewProps.selected) {
                bk.viewProps.selected = true
                mAdapter.notifyItemChanged(i)
            }
        }
    }

    private fun deselectAllBookmarks() {
        for ((i, bk) in mBookmarks.withIndex()) {
            if (bk.viewProps.selected) {
                bk.viewProps.selected = false
                mAdapter.notifyItemChanged(i)
            }
        }
    }

    private fun deleteSelectedBookmark() {
        var haveChanges = false
        var idx = mBookmarks.size - 1
        while (idx >= 0) {
            val bk = mBookmarks[idx]
            if (bk.viewProps.selected) {
                mBookmarks.removeAt(idx)
                mAdapter.notifyItemRemoved(idx)
                haveChanges = true
            }
            idx--
        }
        if (haveChanges) {
            if (null != onBookmarkClickListener) {
                onBookmarkClickListener?.onBookmarkChanged(mBookmarks)
            }
            isSelectionMode = false
        }
    }

    init {
        val context = contentView.context
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.START)
            mPopup.exitTransition = Slide(Gravity.START)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = true
        mPopup.isTouchable = true
        mPopup.isFocusable = false
        mViewGroup = contentView.findViewById(R.id.bookmarksContainer)
        mToolbarPosition = PropNames.App.TOOLBAR_LOCATION_TOP

        mToolbar = contentView.findViewById(R.id.customToolbar)
        mBackButton = contentView.findViewById(R.id.toolBack)
        mBackButton.setOnClickListener { hide() }
        mAddButton = contentView.findViewById(R.id.toolAdd)
        mAddButton.setOnClickListener { onBookmarkClickListener?.onAddClicked() }

        mToolbarSelection = contentView.findViewById(R.id.customToolbarSelection)
        mSTBBackButton = contentView.findViewById(R.id.toolSelectBack)
        mSTBBackButton.setOnClickListener { isSelectionMode = false }
        mSTBSelectAllButton = contentView.findViewById(R.id.toolSelectAll)
        mSTBSelectAllButton.setOnClickListener { selectAllBookmark() }
        mSTBDeselectButton = contentView.findViewById(R.id.toolDeselect)
        mSTBDeselectButton.setOnClickListener { deselectAllBookmarks() }
        mSTBDeleteButton = contentView.findViewById(R.id.toolDeleteSelected)
        mSTBDeleteButton.setOnClickListener {
            MaterialAlertDialogBuilder(context)
                .setTitle(context.getString(R.string.confirmation))
                .setMessage(context.getString(R.string.are_you_sure_you_want_to_delete_these_bookmarks_))
                .setNeutralButton(context.getString(R.string.cancel), null)
                .setPositiveButton(context.getString(R.string.yes)) { _, _ ->
                    deleteSelectedBookmark()
                }
                .show()
        }

        mBookNameView = contentView.findViewById(R.id.bookName)
        mRecyclerView = contentView.findViewById(R.id.recyclerView)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.adapter = mAdapter
        mRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        val rvPaddingLeft = mRecyclerView.paddingLeft
        val rvPaddingRight = mRecyclerView.paddingRight
        val rvPaddingTop = mRecyclerView.paddingTop
        val rvPaddingBottom = mRecyclerView.paddingBottom
        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            var toolbarInsets = insets
            var recyclerInsets = insets
            when (mToolbarPosition) {
                PropNames.App.TOOLBAR_LOCATION_TOP -> {
                    toolbarInsets = Insets.of(insets.left, insets.top, insets.right, 0)
                    recyclerInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
                }

                PropNames.App.TOOLBAR_LOCATION_BOTTOM -> {
                    toolbarInsets = Insets.of(insets.left, 0, insets.right, insets.bottom)
                    recyclerInsets = Insets.of(insets.left, insets.top, insets.right, 0)
                }

                else -> {
                    log.error("error: invalid toolbar pos = $mToolbarPosition")
                }
            }
            mToolbar.updatePadding(
                toolbarInsets.left,
                toolbarInsets.top,
                toolbarInsets.right,
                toolbarInsets.bottom
            )
            mToolbarSelection.updatePadding(
                toolbarInsets.left,
                toolbarInsets.top,
                toolbarInsets.right,
                toolbarInsets.bottom
            )
            mRecyclerView.updatePadding(
                recyclerInsets.left + rvPaddingLeft,
                recyclerInsets.top + rvPaddingTop,
                recyclerInsets.right + rvPaddingRight,
                recyclerInsets.bottom + rvPaddingBottom
            )
            WindowInsetsCompat.CONSUMED
        }

        mRecyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                context,
                true
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val item =
                                if (index >= 0 && index < mBookmarks.size) mBookmarks[index] else null
                            if (null != item) {
                                if (isSelectionMode) {
                                    item.viewProps.selected = !item.viewProps.selected
                                    mAdapter.notifyItemChanged(index)
                                } else {
                                    if (null != onBookmarkClickListener) {
                                        onBookmarkClickListener?.onBookmarkClick(item)
                                        this@BookmarksPopup.hide()
                                    }
                                }
                            }
                        }

                        override fun onLongPressed(index: Int) {
                            // Toggle selection mode
                            isSelectionMode = !isSelectionMode
                            if (isSelectionMode) {
                                val item =
                                    if (index >= 0 && index < mBookmarks.size) mBookmarks[index] else null
                                if (null != item) {
                                    item.viewProps.selected = true
                                    mAdapter.notifyItemChanged(index)
                                }
                            }
                            mRecyclerView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                        }
                    }
            })
    }

    companion object {
        private val log = SRLog.create("bkpop")
    }
}
