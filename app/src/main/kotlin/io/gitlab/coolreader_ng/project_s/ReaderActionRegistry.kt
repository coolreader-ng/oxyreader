/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

object ReaderActionRegistry {
    @JvmField
    val NONE = ReaderAction("NONE", R.string.action_none, ReaderCommand.DCMD_NONE, 0)

    @JvmField
    val REPEAT = ReaderAction("REPEAT", R.string.action_repeat, ReaderCommand.DCMD_REPEAT, 0)

    @JvmField
    val PAGE_DOWN = ReaderAction(
        "PAGE_DOWN",
        R.string.action_pagedown,
        ReaderCommand.DCMD_PAGEDOWN,
        1
    ).setCanRepeat()

    @JvmField
    val PAGE_DOWN_10 =
        ReaderAction(
            "PAGE_DOWN_10",
            R.string.action_pagedown_10,
            ReaderCommand.DCMD_PAGEDOWN,
            10
        ).setCanRepeat()

    @JvmField
    val PAGE_UP =
        ReaderAction("PAGE_UP", R.string.action_pageup, ReaderCommand.DCMD_PAGEUP, 1).setCanRepeat()

    @JvmField
    val PAGE_UP_10 = ReaderAction(
        "PAGE_UP_10",
        R.string.action_pageup_10,
        ReaderCommand.DCMD_PAGEUP,
        10
    ).setCanRepeat()

    @JvmField
    val ZOOM_IN =
        ReaderAction("ZOOM_IN", R.string.action_font_size_increase, ReaderCommand.DCMD_ZOOM_IN, 1)

    @JvmField
    val ZOOM_OUT =
        ReaderAction("ZOOM_OUT", R.string.action_font_size_decrease, ReaderCommand.DCMD_ZOOM_OUT, 1)

    @JvmField
    val DOCUMENT_STYLES =
        ReaderAction(
            "DOCUMENT_STYLES",
            R.string.action_toggle_document_styles,
            ReaderCommand.DCMD_TOGGLE_DOCUMENT_STYLES,
            0
        )

    @JvmField
    val TEXT_AUTOFORMAT =
        ReaderAction(
            "TEXT_AUTOFORMAT",
            R.string.action_toggle_text_autoformat,
            ReaderCommand.DCMD_TOGGLE_TEXT_AUTOFORMAT,
            0
        )

    @JvmField
    val BOOKMARKS = ReaderAction(
        "BOOKMARKS",
        R.string.action_bookmarks,
        ReaderCommand.DCMD_BOOKMARKS,
        0,
        R.drawable.ic_action_bookmarks
    )

    @JvmField
    val NEW_BOOKMARK_PAGE =
        ReaderAction(
            "NEW_BOOKMARKS_PAGE",
            R.string.add_bookmark_to_page,
            ReaderCommand.DCMD_NEW_BOOKMARK,
            0,
            R.drawable.ic_action_bookmark_add
        )

    @JvmField
    val ABOUT =
        ReaderAction(
            "ABOUT",
            R.string.dlg_about,
            ReaderCommand.DCMD_ABOUT,
            0,
            R.drawable.ic_action_info
        )

    @JvmField
    val BOOK_INFO =
        ReaderAction(
            "BOOK_INFO",
            R.string.dlg_book_info,
            ReaderCommand.DCMD_BOOK_INFO,
            0,
            R.drawable.ic_action_bookinfo
        )

    @JvmField
    val TOC =
        ReaderAction(
            "TOC",
            R.string.action_toc,
            ReaderCommand.DCMD_SHOW_TOC,
            0,
            R.drawable.ic_action_contents
        )

    @JvmField
    val SEARCH = ReaderAction(
        "SEARCH",
        R.string.action_search,
        ReaderCommand.DCMD_SEARCH,
        0,
        R.drawable.ic_action_search
    )

    @JvmField
    val SHOW_GO_PANEL = ReaderAction(
        "SHOW_GO_PANEL",
        R.string.action_goto,
        ReaderCommand.DCMD_SHOW_GO_PANEL,
        0,
        R.drawable.ic_action_go
    )

    @JvmField
    val FIRST_PAGE =
        ReaderAction("FIRST_PAGE", R.string.action_go_first_page, ReaderCommand.DCMD_BEGIN, 0)

    @JvmField
    val LAST_PAGE =
        ReaderAction("LAST_PAGE", R.string.action_go_last_page, ReaderCommand.DCMD_END, 0)

    @JvmField
    val STYLE = ReaderAction(
        "STYLE",
        R.string.action_style,
        ReaderCommand.DCMD_OPTIONS_STYLE,
        0,
        R.drawable.ic_action_style
    )

    @JvmField
    val OPTIONS = ReaderAction(
        "OPTIONS",
        R.string.action_options,
        ReaderCommand.DCMD_OPTIONS_DIALOG,
        0,
        R.drawable.ic_action_preferences
    )

    @JvmField
    val READER_MENU =
        ReaderAction("READER_MENU", R.string.action_reader_menu, ReaderCommand.DCMD_READER_MENU, 0)

    @JvmField
    val TOGGLE_DAY_NIGHT = ReaderAction(
        "TOGGLE_DAY_NIGHT",
        R.string.action_toggle_day_night,
        ReaderCommand.DCMD_TOGGLE_DAY_NIGHT_MODE,
        0,
        0 /*R.id.cr3_mi_toggle_day_night*/
    ) //.setIconId(R.drawable.cr3_option_night);

    @JvmField
    val NEXT_CHAPTER = ReaderAction(
        "NEXT_CHAPTER",
        R.string.action_chapter_next,
        ReaderCommand.DCMD_MOVE_BY_CHAPTER,
        1
    )

    @JvmField
    val PREV_CHAPTER = ReaderAction(
        "PREV_CHAPTER",
        R.string.action_chapter_prev,
        ReaderCommand.DCMD_MOVE_BY_CHAPTER,
        -1
    )

    @JvmField
    val GO_BACK = ReaderAction(
        "GO_BACK",
        R.string.action_go_back,
        ReaderCommand.DCMD_LINK_BACK,
        0,
        R.drawable.ic_action_back
    )

    @JvmField
    val GO_FORWARD = ReaderAction(
        "GO_FORWARD",
        R.string.action_go_forward,
        ReaderCommand.DCMD_LINK_FORWARD,
        0,
        R.drawable.ic_action_forward
    )

    @JvmField
    val TOGGLE_FULLSCREEN =
        ReaderAction(
            "TOGGLE_FULLSCREEN",
            R.string.action_toggle_fullscreen,
            ReaderCommand.DCMD_TOGGLE_FULLSCREEN,
            0,
            R.drawable.ic_action_fullscreen
        )

    @JvmField
    val START_SELECTION =
        ReaderAction(
            "START_SELECTION",
            R.string.action_select_text,
            ReaderCommand.DCMD_START_SELECTION,
            0,
            R.drawable.ic_action_select_text
        )

    @JvmField
    val TOGGLE_SELECTION_MODE = ReaderAction(
        "TOGGLE_SELECTION_MODE",
        R.string.action_toggle_selection_mode,
        ReaderCommand.DCMD_TOGGLE_SELECTION_MODE,
        0,
        R.drawable.ic_action_select_text
    )

    @JvmField
    val EXIT = ReaderAction(
        "EXIT",
        R.string.action_exit,
        ReaderCommand.DCMD_EXIT,
        0,
        R.drawable.ic_action_exit
    )

    @JvmField
    val OPEN_BOOK =
        ReaderAction(
            "OPEN_FILE",
            R.string.action_open_book,
            ReaderCommand.DCMD_OPEN_FILE,
            0,
            R.drawable.ic_action_file_open
        )

    @JvmField
    val RECENT_BOOKS = ReaderAction(
        "RECENT_BOOKS",
        R.string.action_recent_books_list,
        ReaderCommand.DCMD_RECENT_BOOKS_LIST,
        0,
        R.drawable.ic_action_library
    )

    @JvmField
    val LIBRARY =
        ReaderAction(
            "LIBRARY",
            R.string.action_open_library,
            ReaderCommand.DCMD_OPEN_LIBRARY,
            0,
            R.drawable.ic_action_library
        )

    @JvmField
    val TTS_PLAY =
        ReaderAction(
            "TTS_PLAY",
            R.string.action_read_aloud,
            ReaderCommand.DCMD_TTS_PLAY,
            0,
            R.drawable.ic_action_read_aloud
        )

    @JvmField
    val MENU_MORE =
        ReaderAction(
            "MENU_MORE",
            R.string.action_more,
            ReaderCommand.DCMD_NONE,
            0,
            R.drawable.ic_action_more
        )

    val AVAILABLE_ACTIONS: Array<ReaderAction>

    fun findById(id: String?): ReaderAction {
        if (id == null)
            return NONE
        for (a in AVAILABLE_ACTIONS) {
            if (id == a.id)
                return a
        }
        if (id == REPEAT.id)
            return REPEAT
        return NONE
    }

    init {
        val baseActions = arrayOf(
            NONE,
            PAGE_DOWN,
            PAGE_UP,
            PAGE_DOWN_10,
            PAGE_UP_10,
            GO_BACK,
            GO_FORWARD,
            FIRST_PAGE,
            LAST_PAGE,
            NEXT_CHAPTER,
            PREV_CHAPTER,
            TOC,
            TTS_PLAY,
            SHOW_GO_PANEL,
            BOOKMARKS,
            NEW_BOOKMARK_PAGE,
            SEARCH,
            STYLE,
            OPTIONS,
            EXIT,
            RECENT_BOOKS,
            LIBRARY,
            READER_MENU,
            ZOOM_IN,
            ZOOM_OUT,
            DOCUMENT_STYLES,
            ABOUT,
            BOOK_INFO,
            TEXT_AUTOFORMAT,
            TOGGLE_SELECTION_MODE,
            TOGGLE_FULLSCREEN,
            TOGGLE_DAY_NIGHT
        )
        // TODO: add some other actions for some specific devices and/or platforms
        AVAILABLE_ACTIONS = baseActions
    }
}
