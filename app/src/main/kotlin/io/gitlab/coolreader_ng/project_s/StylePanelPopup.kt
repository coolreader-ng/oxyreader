/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Context
import android.os.Build
import android.transition.Slide
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import io.gitlab.coolreader_ng.project_s.extensions.isFullscreenWindow


class StylePanelPopup(private val parent: View, contentView: View) {

    interface StylePanelListener {
        fun onPropertiesChanged(props: SRProperties)
    }

    abstract class AbstractPageViewHolder(itemView: View, var mProps: SRProperties) :
        RecyclerView.ViewHolder(itemView) {
        val context: Context = itemView.context
        var onStylePanelListener: StylePanelListener? = null
        abstract fun onUpdateViewImpl()
        abstract fun onResetViewImpl()
        abstract fun onSetUserData(data: HashMap<String, Any?>)
        fun commitChanges() {
            onUpdateViewImpl()
            onStylePanelListener?.onPropertiesChanged(mProps)
        }
    }

    private inner class PageCollectionAdapter : RecyclerView.Adapter<AbstractPageViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractPageViewHolder {
            // Here viewType is equals to position: see getItemViewType()
            val layoutInflater = LayoutInflater.from(parent.context)
            val viewHolder: AbstractPageViewHolder = when (viewType) {
                0 -> {
                    // tab "Common"
                    val itemView =
                        layoutInflater.inflate(R.layout.style_panel_common, parent, false)
                    StylePageCommonViewHolder(itemView, mProps)
                }

                1 -> {
                    // tab "Font"
                    val itemView = layoutInflater.inflate(R.layout.style_panel_text1, parent, false)
                    StylePageText1ViewHolder(itemView, mProps)
                }

                2 -> {
                    // tab "Indents"
                    val itemView =
                        layoutInflater.inflate(R.layout.style_panel_indents, parent, false)
                    StylePageIndentsViewHolder(itemView, mProps)
                }

                3 -> {
                    // tab "Other"
                    val itemView = layoutInflater.inflate(R.layout.style_panel_text2, parent, false)
                    StylePageText2ViewHolder(itemView, mProps)
                }

                4 -> {
                    // tab "Header"
                    val itemView =
                        layoutInflater.inflate(R.layout.style_panel_header, parent, false)
                    StylePageHeaderViewHolder(itemView, mProps)
                }

                5 -> {
                    // tab "Language"
                    val itemView = layoutInflater.inflate(R.layout.style_panel_lang, parent, false)
                    StylePageLangViewHolder(itemView, mProps, mBookLang)
                }

                6 -> {
                    // tab "Per Document"
                    val itemView =
                        layoutInflater.inflate(R.layout.style_panel_per_document, parent, false)
                    StylePagePerDocumentViewHolder(itemView, mProps)
                }

                else -> throw IllegalArgumentException("Invalid view type $viewType")
            }
            viewHolder.onStylePanelListener = this@StylePanelPopup.onStylePanelListener
            viewHolder.onSetUserData(mTabsData)
            viewHolder.onUpdateViewImpl()
            mTabHolders[viewType] = viewHolder
            return viewHolder
        }

        override fun onBindViewHolder(holder: AbstractPageViewHolder, position: Int) {
            if (holder is StylePageLangViewHolder)
                holder.bookLang = mBookLang
            holder.onUpdateViewImpl()
        }

        override fun getItemCount(): Int = TABS_COUNT

        override fun getItemViewType(position: Int): Int {
            // Each page have your own type
            return position
        }
    }

    var decorView: View? = null
        set(value) {
            field = value
            mTabsData["decorView"] = value
            setUserData(mTabsData)
        }
    var onStylePanelListener: StylePanelListener? = null
        set(value) {
            field = value
            for (tab in mTabHolders)
                tab.value.onStylePanelListener = value
        }
    val isShowing: Boolean
        get() = mPopup.isShowing

    private var mPopup: PopupWindow

    // External alternative insets for API < 30
    internal var altInsets: Insets? = null
    private val mPageCollectionAdapter: PageCollectionAdapter
    private val mViewPager: ViewPager2
    private val mTabLayout: TabLayout
    private val mTabHolders = HashMap<Int, AbstractPageViewHolder>()
    private val mTabsData = HashMap<String, Any?>()
    private var mProps = SRProperties()
    private var mBookLang = ""
    private var mMaxPageHeight = -1

    fun show(props: SRProperties, bookLang: String) {
        mBookLang = bookLang
        mProps = SRProperties(props)
        for (tab in mTabHolders) {
            tab.value.mProps = mProps
            tab.value.onResetViewImpl()
            tab.value.onUpdateViewImpl()
        }
        mTabLayout.selectTab(mTabLayout.getTabAt(0))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH || mPopup.isFullscreenWindow)
            mPopup.isClippingEnabled = false
        mPopup.showAtLocation(parent, Gravity.FILL, 0, 0)
    }

    fun update(props: SRProperties) {
        mProps = SRProperties(props)
        for (tab in mTabHolders) {
            tab.value.mProps = mProps
            tab.value.onUpdateViewImpl()
        }
    }

    fun hide() {
        if (mPopup.isShowing)
            mPopup.dismiss()
    }

    private fun setUserData(data: HashMap<String, Any?>) {
        for (tab in mTabHolders)
            tab.value.onSetUserData(data)
    }

    init {
        mPopup = PopupWindow(
            contentView,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mPopup.enterTransition = Slide(Gravity.BOTTOM)
            mPopup.exitTransition = Slide(Gravity.BOTTOM)
        } else {
            mPopup.animationStyle = android.R.style.Animation_Toast
        }
        mPopup.isOutsideTouchable = true
        mPopup.isTouchable = true
        mPopup.isFocusable = false

        mPageCollectionAdapter = PageCollectionAdapter()
        mViewPager = contentView.findViewById(R.id.viewPager)
        mTabLayout = contentView.findViewById(R.id.tabLayout)
        mViewPager.adapter = mPageCollectionAdapter
        TabLayoutMediator(
            mTabLayout, mViewPager
        ) { tab, position ->
            tab.setIcon(
                when (position) {
                    0 -> R.drawable.ic_tab_style        // tab "Common"
                    1 -> R.drawable.ic_tab_font         // tab "Font"
                    2 -> R.drawable.ic_tab_indents      // tab "Indents"
                    3 -> R.drawable.ic_tab_font2        // tab "Font (other)"
                    4 -> R.drawable.ic_tab_header       // tab "Header"
                    5 -> R.drawable.ic_tab_lang         // tab "Language"
                    6 -> R.drawable.ic_tab_per_document // tab "Per Document"
                    else -> throw IllegalArgumentException("Invalid position $position")
                }
            )
        }.attach()
        mViewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                if (position in 0..<TABS_COUNT) {
                    val holder = mTabHolders[position]
                    holder?.onUpdateViewImpl()
                    val pageView = holder?.itemView
                    pageView?.post {
                        val wMeasureSpec = View.MeasureSpec.makeMeasureSpec(
                            pageView.width,
                            View.MeasureSpec.EXACTLY
                        )
                        val hMeasureSpec =
                            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                        pageView.measure(wMeasureSpec, hMeasureSpec)
                        if (mMaxPageHeight < pageView.measuredHeight)
                            mMaxPageHeight = pageView.measuredHeight
                        if (mViewPager.layoutParams.height < mMaxPageHeight) {
                            mViewPager.layoutParams.height = mMaxPageHeight
                            mViewPager.requestLayout()
                        }
                    }
                }
            }
        })

        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val insets = altInsets ?: windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            mTabLayout.updatePadding(bottom = insets.bottom)
            mViewPager.updatePadding(left = insets.left, right = insets.right)
            // Return CONSUMED if you don't want want the window insets to keep passing
            // down to descendant views.
            WindowInsetsCompat.CONSUMED
        }

        val glassView: View = contentView.findViewById(R.id.glass)
        glassView.setOnClickListener { hide() }
    }

    companion object {
        const val TABS_COUNT = 7
    }
}
