/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

/**
 * Interface for processing the "Back" press.
 *
 * If a fragment's implementation requires a response to a back press, then
 * the fragment can implement this interface, and then the activity that owns the fragment
 * will call the onBackPressed() function when Back is pressed for the active fragment.
 */
internal interface AbleBackwards {
    /**
     * Called by the activity when the user presses Back.
     * @return true if this fragment processed and consumed the event;
     *  false - if this fragment ignores this event and it must be handled in the next element of the chain.
     */
    fun onBackPressed(): Boolean
}