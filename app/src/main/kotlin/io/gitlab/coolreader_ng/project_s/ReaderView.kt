/*
 * book reader based on crengine-ng
 * Copyright (C) 2024,2025 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Resources.NotFoundException
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.AttributeSet
import android.view.Gravity
import android.view.HapticFeedbackConstants
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.graphics.Insets
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.graphics.minus
import androidx.core.view.ViewCompat
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import io.gitlab.coolreader_ng.mygesturedetector.MyGestureDetector
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.tts.OnTTSCreatedListener
import io.gitlab.coolreader_ng.project_s.tts.OnTTSStatusListener
import io.gitlab.coolreader_ng.project_s.tts.TTSControlBinder
import io.gitlab.coolreader_ng.project_s.tts.TTSControlService
import io.gitlab.coolreader_ng.project_s.tts.TTSControlServiceAccessor
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.io.UnsupportedEncodingException
import java.text.DecimalFormatSymbols
import kotlin.math.abs
import kotlin.math.roundToInt


class ReaderView : SurfaceView, Recyclable {

    constructor(context: Context?) :
            super(context) {
        setup()
    }

    constructor(context: Context?, attrs: AttributeSet?) :
            super(context, attrs) {
        setup()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        setup()
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) :
            super(context, attrs, defStyleAttr, defStyleRes) {
        setup()
    }

    enum class PageAnimationType {
        None, Paper, Slide;

        companion object {
            fun byOrdinal(ord: Int): PageAnimationType {
                if (ord >= 0 && ord < values().size)
                    return values()[ord]
                return None
            }
        }
    }

    enum class BatteryCharger(val code: Int) {
        None(1), AC(2), USB(3), Wireless(4)
    }

    enum class BatteryState(val code: Int) {
        NoBattery(-2), Discharging(-3), Charging(-1)
    }

    enum class BrightnessControlType {
        None,
        LeftEdge,
        RightEdge;

        companion object {
            fun byOrdinal(ord: Int): BrightnessControlType {
                if (ord >= 0 && ord < entries.size)
                    return entries[ord]
                return None
            }
        }
    }

    // public properties
    var activityControl: ActivityControl? = null
    var decorView: View? = null
        set(value) {
            field = value
            mStylePanelPopup.decorView = value
        }
    var dbServiceAcc: DBServiceAccessor? = null
    var genresCollection: GenresCollection? = null
    var batteryState = BatteryState.NoBattery
        private set
    var batteryChargingConnection = BatteryCharger.None
        private set
    var batteryChargeLevel = 0
        private set
    val isBookLoaded
        get() = mBookOpened
    val isAnimationAllowed: Boolean
        get() = mAnimationAllowed || mForceUseAnimation
    val pageInsets: Insets
        get() = mPageInsets
    internal val lvDocViewWrapper: LVDocViewWrapper
        get() = mDoc

    // Hack: alternative insets for popups (API < 30)
    // TODO: find the best way
    var altPopupInsets: Insets? = null
        set(value) {
            field = value
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
                mDocContentPopup.altInsets = field
                mGoPanelPopup.altInsets = field
                mBookmarksPopup.altInsets = field
                mStylePanelPopup.altInsets = field
                mReaderMenuPopup.altInsets = field
                mTTSPanelPopup.altInsets = field
            }
        }

    // private & internal properties
    private val mDoc = LVDocViewWrapper()
    private lateinit var mGestureDetector: MyGestureDetector
    private var mInitialized = false
    private var mBookInfo: BookInfo? = null
    private var mBookOpened = false
    private var mToc: DocumentTOCItem? = null
    private var mCurrentChapter: String? = null
    private var mLastSavedBookmark: Bookmark? = null
    private val mRenderBpp: Byte = 32
    internal var mRenderWidth = 100 // internal document page width (LVDocView page render width)
        private set
    internal var mRenderHeight = 100 // internal document page height (LVDocView page render height)
        private set
    private var mRequestedWidth = 0
    private var mRequestedHeight = 0
    private var mResizeAllowed = true
    private var mLastRequestedWidth = 0
    private var mLastRequestedHeight = 0
    private var mPageInsets = Insets.NONE
    private var mAllowPageHeaderOverlap = false
    private var mDPI = 160
    private var mSurfaceCreated = false
    private var mCurrentPageCache: PageImageCache? = null
    private var mNextPageCache: PageImageCache? = null
    private var mCurrentPos: PositionProperties? = null
    private var mInvalidateImages = true
    private val mPageImageLocker = Any()
    private val mGestureListener = GestureListener()
    private lateinit var mAnimationThread: AnimationThread
    private var mCurrentAnimation: AbstractAnimation? = null
    private val mAnimationLocker = Any()
    private var mAnimAvgFrameTime: Long = 17 // animation frame generation time
    private var mLastDrawTaskId: Long = 0
    private var mLastSavePositionTaskId: Long = 0
    private var mLastResizeTaskId: Long = 0
    private var mLastUpdateTaskId: Long = 0
    private var mCurrentUpdateCacheTask: UpdateCacheTask? = null
    private val mUpdateCacheLocker = Any()
    private var mImageViewer: ImageViewer? = null
    private var mChapterViews: ArrayList<TextView> = ArrayList()
    private lateinit var mProgressPopup: ProgressPopup
    private var mCurrentProgressTask: String? = null
    private var mCurrentProgressTaskId = 0
    private var mCurrentProgressPosition = 0
    private lateinit var mDocContentPopup: DocContentsPopup
    private lateinit var mGoPanelPopup: GoPanelPopup
    private lateinit var mBookmarksPopup: BookmarksPopup
    private lateinit var mStylePanelPopup: StylePanelPopup
    private lateinit var mReaderMenuPopup: ReaderMenuPopup
    private lateinit var mSelectionPanelPopup: SelectionPanelPopup
    private lateinit var mTTSPanelPopup: TTSPanelPopup
    private var mShowBattery = true
    private var mShowTime = true
    private var mOverriddenScrollMode = false
    private var mSensorBrightnessControlType = BrightnessControlType.None
    private var mCurrentBrightnessValue = -1
    private var mCurrentBrightnessPrevYPos = -1

    private var mSettings = SRProperties()
    private val haveAnyOpenPopups: Boolean
        get() {
            return mDocContentPopup.isShowing ||
                    mGoPanelPopup.isShowing ||
                    mBookmarksPopup.isShowing ||
                    mStylePanelPopup.isShowing ||
                    mReaderMenuPopup.isShowing ||
                    mSelectionPanelPopup.isShowing ||
                    mTTSPanelPopup.isShowing
        }

    /// The animation is ready, it can be either running or not
    private val isAnimationUsed: Boolean
        get() {
            synchronized(mAnimationLocker) {
                if (null != mCurrentAnimation) {
                    return true
                }
            }
            return false
        }

    /// Animation is in progress
    private val isAnimationInProgress: Boolean
        get() = mAnimationThread.isAnimationActive

    // Reader View customizable options
    private var mAnimationAllowed = true
    private var mForceUseAnimation = false
    private var mPageAnimationType = PageAnimationType.Slide
    private var mHorizDeadIndent = 0
    private var mVertDeadIndent = 0
    private var isDoubleTapForActionsAllowed = false
        set(value) {
            mGestureDetector.setOnDoubleTapListener(if (value) mGestureListener else null)
            field = value
        }
    private var isDoubleTapForSelectionStart = false
        set(value) {
            mGestureDetector.setOnDoubleTapListener(if (value) mGestureListener else null)
            field = value
        }
    private var mIsLongTapForActionsAllowed = false
    private var mIsLongTapForSelectionStart = false
    private val mSingleTapActions = arrayOfNulls<ReaderAction?>(9)
    private val mLongTapActions = arrayOfNulls<ReaderAction?>(9)
    private val mDoubleTapActions = arrayOfNulls<ReaderAction?>(9)
    private val mKeySinglePressActions = HashMap<Int, ReaderAction?>()
    private val mKeyLongPressActions = HashMap<Int, ReaderAction?>()
    private var mAllowVolumeKeys = true
    private var mHandleLongKeyPress = false
    private var mBookReadingStartTime = 0L
    private var bookReadingDuration = 0L
        get() {
            field += Utils.uptimeElapsed(mBookReadingStartTime)
            return field
        }
    private var mCurrentBackgroundInfo: SettingsManager.PageBackgroundInfo? = null
    private var mCurrentBackgroundColor: Int = 0
    private var mCanGoBack = false

    // Text selection stuff
    private var mInStartSelectionMode = false
    private var mSelectionStart = Point()
    private var mSelectionEnd = Point()
    private var mSelectionStickyWordBoundaries = false
    private var mSelectionGestureCompleted = false
    private var mCurrentSelection: Selection? = null
    private var mTextSelectHandleLeftDrawable: Drawable? = null
    private var mTextSelectHandleRightDrawable: Drawable? = null
    private var mTextSelectHandleLeftBounds = Rect()
    private var mTextSelectHandleLeftPadding = Rect()
    private var mTextSelectHandleRightBounds = Rect()
    private var mTextSelectHandleRightPadding = Rect()
    private var mTextSelectionIsDraggingLeftHandle = false
    private var mTextSelectionIsDraggingRightHandle = false
    private var mTextSelectionLeftHandleStartPos = Point()
    private var mTextSelectionRightHandleStartPos = Point()
    private var mSystemGestureExclusionRects = ArrayList<Rect>(2)

    // TTS stuff
    private var mTTSControlServiceAccessor: TTSControlServiceAccessor? = null
    private var mTTSIsSpeaking = false
    private var mTTSEngine = ""
    private var mTTSVoice = ""
    private var mTTSForceLang = ""
    private var mTTSCurrentLanguage = ""
    private var mTTSCurrentVoiceName = ""
    private var mTTSUseDocLang = true
    private var mTTSSpeechRate = 1f
    private var mTTSSpeechRateIdx = 23
    private var mTTSUseEndOfSentenceWorkaround = false
    private var mTTSUseDictionary = false
    private var mTTSUseImmobilityTimeout = false
    private var mTTSImmobilityTimeoutValue = 60
    private var mTTSState = TTSControlService.State.STOPPED
    private var mTTSVolume = 0
    private var mTTSMaxVolume = 100
    private var mTTSSelection: Selection? = null
    private val mTTSIsActive
        get() = mTTSPanelPopup.isShowing
    private val mTTSStatusListener = object : OnTTSStatusListener {
        override fun onUtteranceStart() {
            mTTSIsSpeaking = true
        }

        override fun onUtteranceDone() {
        }

        override fun onError(errorCode: Int) {
            BackgroundThread.postGUI { activityControl?.showToast(R.string.tts_failed) }
        }

        override fun onStateChanged(state: TTSControlService.State) {
            mTTSState = state
            mTTSIsSpeaking = when (state) {
                TTSControlService.State.PLAYING -> {
                    true
                }

                TTSControlService.State.PAUSED,
                TTSControlService.State.STOPPED -> {
                    false
                }
            }
            BackgroundThread.postGUI {
                mTTSPanelPopup.update(
                    mTTSIsSpeaking,
                    mTTSVolume,
                    mTTSMaxVolume,
                    mTTSSpeechRate
                )
            }
        }

        override fun onVolumeChanged(currentVolume: Int, maxVolume: Int) {
            mTTSVolume = currentVolume
            mTTSMaxVolume = maxVolume
            BackgroundThread.postGUI {
                mTTSPanelPopup.update(
                    isSpeaking = mTTSIsSpeaking,
                    mTTSVolume,
                    mTTSMaxVolume,
                    mTTSSpeechRate
                )
            }
        }

        override fun onSpeechRateChanged(speechRate: Float) {
            mTTSSpeechRate = speechRate
            mTTSSpeechRateIdx = Utils.findNearestIndex(TTS_SPEECH_RATE_VALUES, mTTSSpeechRate)
            BackgroundThread.postGUI {
                activityControl?.setSetting(PropNames.App.TTS_SPEED, mTTSSpeechRate, false)
                mTTSPanelPopup.update(
                    isSpeaking = mTTSIsSpeaking,
                    mTTSVolume,
                    mTTSMaxVolume,
                    mTTSSpeechRate
                )
            }
        }

        override fun onAudioFocusLost() {
        }

        override fun onAudioFocusRestored() {
        }

        override fun onCurrentSentenceRequested(ttsbinder: TTSControlBinder) {
            mTTSSelection?.let {
                ttsbinder.say(it.text, null)
            } ?: run {
                processEngineCommand(
                    ReaderCommand.DCMD_SELECT_FIRST_SENTENCE,
                    ReaderCommand.DCMD_SELECT_FLAG_DONT_CHANGE_POS,
                    false
                ) {
                    BackgroundThread.postBackground {
                        mTTSSelection = mDoc.getSelection()
                        mTTSSelection?.let {
                            BackgroundThread.postGUI {
                                goToSelection(it)
                            }
                            ttsbinder.say(it.text, null)
                        }
                    }
                }
            }
        }

        override fun onNextSentenceRequested(ttsbinder: TTSControlBinder) {
            processEngineCommand(
                ReaderCommand.DCMD_SELECT_NEXT_SENTENCE,
                ReaderCommand.DCMD_SELECT_FLAG_DONT_CHANGE_POS,
                false
            ) {
                BackgroundThread.postBackground {
                    mTTSSelection = mDoc.getSelection()
                    mTTSSelection?.let {
                        BackgroundThread.postGUI {
                            if (isAnimationAllowed && mResizeAllowed)
                                animateScrollTo(it)
                            else
                                goToSelection(it)
                        }
                        if (mTTSIsSpeaking) {
                            ttsbinder.say(it.text, null)
                        }
                    }
                }
            }
        }

        override fun onPreviousSentenceRequested(ttsbinder: TTSControlBinder) {
            processEngineCommand(
                ReaderCommand.DCMD_SELECT_PREV_SENTENCE,
                ReaderCommand.DCMD_SELECT_FLAG_DONT_CHANGE_POS,
                false
            ) {
                BackgroundThread.postBackground {
                    mTTSSelection = mDoc.getSelection()
                    mTTSSelection?.let {
                        BackgroundThread.postGUI {
                            if (isAnimationAllowed && mResizeAllowed)
                                animateScrollTo(it)
                            else
                                goToSelection(it)
                        }
                        if (mTTSIsSpeaking) {
                            ttsbinder.say(it.text, null)
                        }
                    }
                }
            }
        }

        override fun onStopRequested(ttsbinder: TTSControlBinder) {
            stopTTSAndClosePanel()
        }

    }

    private inner class SurfaceCallback : SurfaceHolder.Callback {
        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            log.debug("surfaceChanged(): width=$width; height=$height")
            if (mResizeAllowed) {
                BackgroundThread.postGUI({
                    if (mResizeAllowed) {
                        requestResize(width, height)
                    } else {
                        mLastRequestedWidth = width
                        mLastRequestedHeight = height
                    }
                }, 500)
            } else {
                mLastRequestedWidth = width
                mLastRequestedHeight = height
            }
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            log.info("surfaceCreated()")
            mSurfaceCreated = true
            asyncDrawPage()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            log.info("surfaceDestroyed()")
            mSurfaceCreated = false
        }
    }

    private inner class GestureListener : MyGestureDetector.MyOnGestureListener {
        private var mmInScrollEvent = false
        private var mmIsPageMode = false
        private var mmLastDir = 0
        private var mmScrollSpeedX = 0f
        private var mmScrollSpeedY = 0f
        private var mmInBrightnessControl = false

        private fun isInAllowedArea(e: MotionEvent): Boolean {
            return (e.x >= mHorizDeadIndent && e.x < (this@ReaderView.width - mHorizDeadIndent)) &&
                    (e.y >= mVertDeadIndent && e.y < (this@ReaderView.height - mVertDeadIndent))
        }

        private fun isInBrightnessControlArea(e: MotionEvent): Boolean {
            when (mSensorBrightnessControlType) {
                BrightnessControlType.None ->
                    return false

                BrightnessControlType.LeftEdge -> {
                    val xLimit = this@ReaderView.width / 3
                    return (e.x >= mHorizDeadIndent && e.x < xLimit) &&
                            (e.y >= mVertDeadIndent && e.y < (this@ReaderView.height - mVertDeadIndent))
                }

                BrightnessControlType.RightEdge -> {
                    val xLimit = 2 * this@ReaderView.width / 3
                    return (e.x >= xLimit && e.x < this@ReaderView.width - mHorizDeadIndent) &&
                            (e.y >= mVertDeadIndent && e.y < (this@ReaderView.height - mVertDeadIndent))
                }
            }
        }

        private fun singleTapImpl(e: MotionEvent) {
            if (isInAllowedArea(e)) {
                if (mInStartSelectionMode) {
                    if (!mTextSelectionIsDraggingLeftHandle && !mTextSelectionIsDraggingRightHandle) {
                        mSelectionEnd.x = e.x.toInt()
                        mSelectionEnd.y = e.y.toInt()
                        updateSelection(mSelectionStart, mSelectionEnd, true, true)
                        mSelectionGestureCompleted = true
                    }
                } else {
                    val hasSelection = mCurrentSelection?.isNotEmpty == true
                    // Call whether or not text is selected to force hide selection panel
                    clearSelection()
                    if (!hasSelection) {
                        // TODO: check link before executing action (or better in onLongPress()?)
                        val region = getActionNoByCoord(e.x.toInt(), e.y.toInt())
                        val action = getRegionAction(SettingsManager.PressOrTapType.NORMAL, region)
                        if (null != action)
                            processReaderAction(action)
                    }
                }
            }
        }

        // GestureDetector.OnDoubleTapListener
        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            // Notified when a single-tap occurs.
            // Unlike OnGestureListener#onSingleTapUp(MotionEvent), this will only be called after
            // the detector is confident that the user's first tap is not followed by a second tap
            // leading to a double-tap gesture.
            log.debug("onSingleTapConfirmed(): e=$e")
            singleTapImpl(e)
            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            // Notified when a double-tap occurs.
            // Triggered on the down event of second tap.
            if (!isInAllowedArea(e))
                return true
            log.debug("onDoubleTap(): e=$e")
            if (isDoubleTapForActionsAllowed) {
                val region = getActionNoByCoord(e.x.toInt(), e.y.toInt())
                val action = getRegionAction(SettingsManager.PressOrTapType.DOUBLE, region)
                if (null != action)
                    processReaderAction(action)
            } else if (isDoubleTapForSelectionStart) {
                processCommand(ReaderCommand.DCMD_TOGGLE_SELECTION_MODE, 0, null)
            }
            return true
        }

        override fun onDoubleTapEvent(e: MotionEvent): Boolean {
            // Notified when an event within a double-tap gesture occurs, including the down, move, and up events.
            return isInAllowedArea(e)
        }

        // GestureDetector.OnGestureListener
        override fun onDown(e: MotionEvent): Boolean {
            // Notified when a tap occurs with the down MotionEvent that triggered it.
            // This will be triggered immediately for every down event.
            // All other events should be preceded by this.
            // return false to ignore entire gesture

            // Don't filter events by coordinates, since links processing will be broken
            if (mInStartSelectionMode) {
                if (!mTextSelectionIsDraggingLeftHandle && !mTextSelectionIsDraggingRightHandle) {
                    mSelectionStart.x = e.x.toInt()
                    mSelectionStart.y = e.y.toInt()
                    mSelectionGestureCompleted = false
                }
                mmInBrightnessControl = false
            } else {
                mCurrentSelection?.let {
                    if (it.isNotEmpty) {
                        mTextSelectionIsDraggingLeftHandle =
                            mTextSelectHandleLeftBounds.contains(e.x.toInt(), e.y.toInt())
                        mTextSelectionIsDraggingRightHandle =
                            mTextSelectHandleRightBounds.contains(e.x.toInt(), e.y.toInt())
                        if (mTextSelectionIsDraggingLeftHandle) {
                            mTextSelectionLeftHandleStartPos.x = it.startHandleWndPos.x
                            mTextSelectionLeftHandleStartPos.y = it.startHandleWndPos.y
                        } else if (mTextSelectionIsDraggingRightHandle) {
                            mTextSelectionRightHandleStartPos.x = it.endHandleWndPos.x
                            mTextSelectionRightHandleStartPos.y = it.endHandleWndPos.y
                        }
                    }
                    mmInBrightnessControl = false
                } ?: run {
                    mmInBrightnessControl = isInBrightnessControlArea(e)
                    if (mmInBrightnessControl)
                        startBrightnessControl(e.x.toInt(), e.y.toInt())
                }
            }
            if (isAnimationAllowed && isAnimationInProgress) {
                log.debug("onDown(): animation thread in progress...")
            }
            return true
        }

        override fun onUp(e: MotionEvent): Boolean {
            if (mInStartSelectionMode) {
                if (!mTextSelectionIsDraggingLeftHandle && !mTextSelectionIsDraggingRightHandle) {
                    if (!mSelectionGestureCompleted) {
                        mSelectionEnd.x = e.x.toInt()
                        mSelectionEnd.y = e.y.toInt()
                        updateSelection(mSelectionStart, mSelectionEnd, true, true)
                        mSelectionGestureCompleted = true
                        mInStartSelectionMode = false
                        return true
                    }
                }
            }
            if (mmInBrightnessControl) {
                stopBrightnessControl(e.x.toInt(), e.y.toInt())
                return true
            }
            if (isAnimationAllowed || !mmIsPageMode) {
                if (mmInScrollEvent) {
                    mmInScrollEvent = false
                    synchronized(mAnimationLocker) {
                        if (null != mCurrentAnimation) {
                            if (mCurrentAnimation is ScrollAnimation) {
                                val offset = mCurrentAnimation!!.position
                                mCurrentAnimation = null
                                processCommand(ReaderCommand.DCMD_SCROLL_BY, offset, null)
                            } else {
                                val forward = abs(mCurrentAnimation!!.position) > mRenderWidth / 2
                                //log.d("onUp(): forward=$forward");
                                mAnimationThread.startAnimation(mCurrentAnimation!!,
                                    forward,
                                    50f,
                                    0f,
                                    if (forward) {
                                        {
                                            synchronized(mAnimationLocker) {
                                                if (null != mCurrentAnimation) {
                                                    val curr_dir = mCurrentAnimation!!.direction
                                                    val cmd: ReaderCommand
                                                    val param: Int
                                                    val isPageMode =
                                                        mCurrentAnimation is PageFlipAnimation || mCurrentAnimation is NaturalPageFlipAnimation
                                                    if (isPageMode) {
                                                        cmd =
                                                            if (curr_dir > 0) ReaderCommand.DCMD_PAGEDOWN else ReaderCommand.DCMD_PAGEUP
                                                        param = 1
                                                    } else {
                                                        cmd = ReaderCommand.DCMD_SCROLL_BY
                                                        param = mCurrentAnimation!!.position
                                                    }
                                                    mCurrentAnimation =
                                                        null // nullify to not prevent asyncDraw()
                                                    processEngineCommand(cmd, param)
                                                }
                                            }
                                        }
                                    } else {
                                        {
                                            synchronized(mAnimationLocker) {
                                                // nullify to not prevent asyncDraw()
                                                mCurrentAnimation = null
                                            }
                                            mCurrentSelection?.let {
                                                if (it.isNotEmpty)
                                                    asyncDrawPage()
                                            }
                                        }
                                    })
                            }
                        }
                    }
                }
            } else {
                if (mmInScrollEvent) {
                    mmInScrollEvent = false
                    val cmd =
                        if (mmLastDir > 0) ReaderCommand.DCMD_PAGEDOWN else ReaderCommand.DCMD_PAGEUP
                    processEngineCommand(cmd, 1)
                }
            }
            return true
        }

        override fun onShowPress(e: MotionEvent) {
            // The user has performed a down MotionEvent and not performed a move or up yet.
            // This event is commonly used to provide visual feedback to the user to let them
            // know that their action has been recognized i.e. highlight an element.
            log.debug("onShowPress(): e=$e")
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            // Notified when a tap occurs with the up MotionEvent that triggered it.
            log.debug("onSingleTapUp(): e=$e")
            if (!isDoubleTapForActionsAllowed && !isDoubleTapForSelectionStart)
                singleTapImpl(e)
            return true
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            // Notified when a scroll occurs with the initial on down MotionEvent and the current move MotionEvent.
            // The distance in x and y is also supplied for convenience.
            // e1: The first down motion event that started the scrolling.
            // e2: The move motion event that triggered the current onScroll. This value cannot be null.
            // distanceX: The distance along the X axis that has been scrolled since the last call to onScroll. This is NOT the distance between e1 and e2.
            // distanceY: The distance along the Y axis that has been scrolled since the last call to onScroll. This is NOT the distance between e1 and e2.
            //log.debug("onScroll(): e1=$e1")
            //log.debug("onScroll(): e2=$e2")
            //log.debug("onScroll(): distanceX=$distanceX; distanceY=$distanceY")
            if (null == e1)
                return true
            if (!isInAllowedArea(e1) || !isInAllowedArea(e2))
                return true
            val dX = (e2.x - e1.x).roundToInt()
            val dY = (e2.y - e1.y).roundToInt()
            if (mCurrentSelection?.isNotEmpty == true) {
                if (mTextSelectionIsDraggingLeftHandle) {
                    val newStartX =
                        e2.x.toInt() + (mTextSelectionLeftHandleStartPos.x - e1.x).roundToInt()
                    mSelectionStart.x = newStartX
                    val newStartY =
                        e2.y.toInt() - (e1.y - mTextSelectionLeftHandleStartPos.y + 3).roundToInt()
                    mSelectionStart.y = newStartY
                    updateSelection(
                        mSelectionStart,
                        mSelectionEnd,
                        mSelectionStickyWordBoundaries,
                        false
                    )
                    return true
                } else if (mTextSelectionIsDraggingRightHandle) {
                    val newEndX =
                        e2.x.toInt() - (e1.x - mTextSelectionRightHandleStartPos.x).roundToInt()
                    val newEndY =
                        e2.y.toInt() - (e1.y - mTextSelectionRightHandleStartPos.y + 3).roundToInt()
                    mSelectionEnd.x = newEndX
                    mSelectionEnd.y = newEndY
                    updateSelection(
                        mSelectionStart,
                        mSelectionEnd,
                        mSelectionStickyWordBoundaries,
                        false
                    )
                    return true
                }
            }
            if (mInStartSelectionMode) {
                if (!mSelectionGestureCompleted) {
                    //mSelectionStart.x = e1.x.toInt()
                    //mSelectionStart.y = e1.y.toInt()
                    mSelectionEnd.x = e2.x.toInt()
                    mSelectionEnd.y = e2.y.toInt()
                    updateSelection(mSelectionStart, mSelectionEnd, true, false)
                    return true
                }
            }
            //log.debug("onScroll(): dX=$dX; dY=$dY")
            if (mmInBrightnessControl)
                mmInBrightnessControl = isInBrightnessControlArea(e2)
            if (mmInBrightnessControl) {
                updateBrightnessControl(e2.x.toInt(), e2.y.toInt())
                return true
            }
            mmIsPageMode = isInPageViewMode()
            if (isAnimationAllowed || !mmIsPageMode) {
                if (!mmInScrollEvent) {
                    mmScrollSpeedX = 50.0f
                    mmScrollSpeedY = 50.0f
                    mmInScrollEvent = true
                }
                if (isAnimationInProgress) {
                    log.debug("onScroll(): animation thread in progress...")
                    return true
                }
                val elapsedTime: Long
                val offset_x: Float
                val offset_y: Float
                if (e2.historySize > 1) {
                    var pos = 2
                    if (pos >= e2.historySize) pos = e2.historySize - 1
                    elapsedTime = e2.eventTime - e2.getHistoricalEventTime(pos)
                    offset_x = e2.x - e2.getHistoricalX(pos)
                    offset_y = e2.y - e2.getHistoricalY(pos)
                } else {
                    elapsedTime = e2.eventTime - e1.eventTime
                    offset_x = dX.toFloat()
                    offset_y = dY.toFloat()
                }
                mmScrollSpeedX = abs(mAnimAvgFrameTime * offset_x / elapsedTime.toFloat())
                mmScrollSpeedY = abs(mAnimAvgFrameTime * offset_y / elapsedTime.toFloat())
                synchronized(mAnimationLocker) {
                    if (null == mCurrentAnimation) {
                        // 1. Checking if we have images of both pages
                        val page1 = getPageImage(0)
                        if (null != page1?.bitmap && !page1.isReleased) {
                            mmIsPageMode = 0 != page1.position?.pageMode
                            if (mmIsPageMode) {
                                if (abs(dX) < 10) {
                                    log.verbose("Ignoring too small x offset; dX=$dX")
                                    return true
                                }
                                val dir = if (dX < 0) 1 else -1
                                // check/prepare next page image
                                val offset = if (dir > 0) 1 else -1
                                val page2 = getPageImage(offset)
                                // check cached page2
                                if (page2?.bitmap == null || null == page2.position) {
                                    log.error("page2 is null or incomplete!")
                                } else if (!page2.isCompatible(page1)) {
                                    log.error("page1 & page2 is not compatible!")
                                } else {
                                    // Create animation object
                                    mCurrentAnimation = when (mPageAnimationType) {
                                        PageAnimationType.Paper -> NaturalPageFlipAnimation(
                                            dir,
                                            page1,
                                            page2,
                                            false
                                        )

                                        PageAnimationType.Slide -> PageFlipAnimation(
                                            dir,
                                            page1,
                                            page2
                                        )

                                        else -> PageFlipAnimation(dir, page1, page2)
                                    }
                                    log.debug("animation created using cached images")
                                }
                            } else {
                                if (abs(dY) < 10) {
                                    log.verbose("Ignoring too small y offset; dY=$dY")
                                    return true
                                }
                                val dir = if (dY < 0) 1 else -1
                                // check next page image
                                val offset = if (dir > 0) mRenderHeight else -mRenderHeight
                                val page2 = getPageImage(offset)
                                // check cached page2
                                if (page2?.bitmap == null || null == page2.position) {
                                    log.error("page2 is null or incomplete!")
                                } else if (!page2.isCompatible(page1)) {
                                    log.error("page1 & page2 is not compatible!")
                                } else {
                                    // Create animation object
                                    mCurrentAnimation = ScrollAnimation(dir, page1, page2)
                                    log.debug("animation created using cached images")
                                }
                            }
                        }
                        if (null == mCurrentAnimation) {
                            // 2. If there are no cached images of both pages, then
                            // build animation instance in background thread since we use DocViewWrapper
                            // to prepare pages image
                            BackgroundThread.postBackground {
                                synchronized(mAnimationLocker) {
                                    if (null == mCurrentAnimation) {
                                        val page1 = preparePageImage(0)
                                        // check cached page1
                                        if (page1?.bitmap == null || null == page1.position) {
                                            log.error("page1 is null or incomplete!")
                                            return@postBackground
                                        }
                                        mmIsPageMode = 0 != page1.position?.pageMode
                                        if (mmIsPageMode) {
                                            if (abs(dX) < 10) {
                                                log.verbose("Ignoring too small x offset; dX=$dX")
                                                return@postBackground
                                            }
                                            val dir = if (dX < 0) 1 else -1
                                            // check/prepare next page image
                                            val offset = if (dir > 0) 1 else -1
                                            val page2 = preparePageImage(offset)
                                            // check cached page2
                                            if (page2?.bitmap == null || null == page2.position) {
                                                log.error("page2 is null or incomplete!")
                                                return@postBackground
                                            }
                                            if (!page2.isCompatible(page1)) {
                                                log.error("page1 & page2 is not compatible!")
                                                return@postBackground
                                            }
                                            // Create animation object
                                            mCurrentAnimation = when (mPageAnimationType) {
                                                PageAnimationType.Paper -> NaturalPageFlipAnimation(
                                                    dir,
                                                    page1,
                                                    page2,
                                                    false
                                                )

                                                PageAnimationType.Slide -> PageFlipAnimation(
                                                    dir,
                                                    page1,
                                                    page2
                                                )

                                                else -> PageFlipAnimation(dir, page1, page2)
                                            }
                                        } else {
                                            if (abs(dY) < 10) {
                                                log.verbose("Ignoring too small y offset; dY=$dY")
                                                return@postBackground
                                            }
                                            val dir = if (dY < 0) 1 else -1
                                            // check/prepare next page image
                                            val offset =
                                                if (dir > 0) mRenderHeight else -mRenderHeight
                                            val page2 = preparePageImage(offset)
                                            // check cached page2
                                            if (page2?.bitmap == null || null == page2.position) {
                                                log.error("page2 is null or incomplete!")
                                                return@postBackground
                                            }
                                            if (!page2.isCompatible(page1)) {
                                                log.error("page1 & page2 is not compatible!")
                                                return@postBackground
                                            }
                                            log.debug("onScroll(): dir=$dir; dY=$dY")
                                            // Create animation object
                                            mCurrentAnimation = ScrollAnimation(dir, page1, page2)
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        mCurrentAnimation?.overridePosition(dX, dY)
                        drawOnSurfaceUsingCallback { c -> mCurrentAnimation?.renderFrame(c) }
                        if (mCurrentAnimation?.isAnimationComplete == true) {
                            mCurrentAnimation = null
                            asyncDrawPage()
                        }
                        // Save this direction for onFling() method
                        mmLastDir =
                            if (mmIsPageMode) (if (distanceX > 0) 1 else -1) else (if (distanceY > 0) 1 else -1)
                    }
                }
            } else {
                // TODO: filter small offset (test dX, dY)
                if (null != mCurrentPageCache && null != mCurrentPageCache!!.position) {
                    mmIsPageMode = mCurrentPageCache!!.position!!.pageMode != 0
                    // Save this direction for onFling() method
                    if (mmIsPageMode) {
                        if (abs(dX) < abs(3 * dY)) {
                            // filter non horizontal scroll
                            return true
                        }
                        mmLastDir = if (distanceX > 0) 1 else -1
                    } else {
                        if (abs(dY) < abs(3 * dX)) {
                            // filter non vertical scroll
                            return true
                        }
                        mmLastDir = if (distanceY > 0) 1 else -1
                    }
                    mmInScrollEvent = true
                }
            }
            return true
        }

        override fun onLongPress(e: MotionEvent) {
            // Notified when a long press occurs with the initial on down MotionEvent that trigged it.
            //log.debug("onLongPress(): e=$e")
            // Examine current page at event coordinates for links, images or bookmarks
            val x: Int = e.x.toInt()
            val y: Int = e.y.toInt()
            BackgroundThread.postEngineTask(object : EngineTask() {
                private var mmmLink: String? = null
                private var mmmImage: PageImageCache? = null
                private var mmmBookmark: Bookmark? = null

                override fun work() {
                    val imageCache = PageImageCache()
                    if (mDoc.getEmbeddedImage(x, y, imageCache, mRenderBpp)) {
                        mmmImage = imageCache
                        return
                    }
                    imageCache.recycle()
                    val link = mDoc.checkLink(x, y, mDPI / 10)
                    if (link != null) {
                        mmmLink = link
                        return
                    }
                    val bookmark = mDoc.checkBookmark(x, y)
                    if (bookmark != null && bookmark.type != Bookmark.TYPE_POSITION)
                        mmmBookmark = bookmark
                }

                override fun done() {
                    var completed = false
                    if (null != mmmImage) {
                        startImageViewer(mmmImage!!)
                        completed = true
                    } else if (null != mmmBookmark) {
                        mmmBookmark?.let { bm ->
                            mBookInfo?.let { bi ->
                                var bi_bm: Bookmark? = null
                                for (i in 0 until bi.bookmarkCount) {
                                    val ibm = bi.getBookmark(i)
                                    if (bm.equalUniqueKey(ibm))
                                        bi_bm = ibm
                                }
                                if (null == bi_bm) {
                                    bi.addBookmark(bm)
                                    bi_bm = bm
                                }
                                val savedBm = Bookmark(bi_bm)
                                activityControl?.showBookmarkEditFragment(bi_bm, false,
                                    object : BookmarkEditDialogFragment.OnActionsListener {
                                        override fun onAddBookmark(bookmark: Bookmark) {
                                            if (savedBm != bi_bm) {
                                                saveCurrentBookInfo()
                                                BackgroundThread.postBackground {
                                                    mDoc.setBookmarks(bi)
                                                    invalidateImageCache()
                                                    asyncDrawPage()
                                                }
                                            }
                                        }

                                        override fun onRemoveBookmark(bookmark: Bookmark) {
                                            val removed = bi.removeBookmark(bookmark) != null
                                            if (removed) {
                                                saveCurrentBookInfo()
                                                BackgroundThread.postBackground {
                                                    mDoc.setBookmarks(bi)
                                                    invalidateImageCache()
                                                    asyncDrawPage()
                                                }
                                            }
                                        }
                                    })
                                completed = true
                            }
                        }
                    } else if (null != mmmLink) {
                        mmmLink?.let { link ->
                            if (link.startsWith("#")) {
                                log.debug("go to $link")
                                BackgroundThread.postBackground {
                                    mDoc.goLink(link)
                                    mCanGoBack = mDoc.canGoBack()
                                    asyncDrawPage()
                                }
                                completed = true
                            } else {
                                // Follow external link
                                if (link.startsWith("http://") || link.startsWith("https://")) {
                                    activityControl?.openURL(link, true)
                                    completed = true
                                }
                            }
                        }
                    } else {
                        if (isInAllowedArea(e)) {
                            if (mIsLongTapForActionsAllowed) {
                                val region = getActionNoByCoord(e.x.toInt(), e.y.toInt())
                                val action =
                                    getRegionAction(SettingsManager.PressOrTapType.LONG, region)
                                if (null != action)
                                    processReaderAction(action)
                                completed = true
                            } else if (mIsLongTapForSelectionStart && mBookOpened) {
                                mCurrentSelection = Selection()
                                mSelectionStart.x = e.x.toInt()
                                mSelectionStart.y = e.y.toInt()
                                mSelectionEnd.x = e.x.toInt()
                                mSelectionEnd.y = e.y.toInt()
                                updateSelection(mSelectionStart, mSelectionEnd, true, true)
                                mSelectionGestureCompleted = true
                                mInStartSelectionMode = false
                                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1) {
                                    // On Android 8.1+ haptic feedback performed in updateSelection()
                                    completed = true
                                }
                            }
                        }
                    }
                    if (completed)
                        performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                }

                override fun fail(e: Exception) {
                    log.error("examine page failed: $e")
                }
            })
        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            // Notified of a fling event when it occurs with the initial on down MotionEvent and the matching up MotionEvent.
            // The calculated velocity is supplied along the x and y axis in pixels per second.
            //log.debug("onFling(): e1=$e1")
            //log.debug("onFling(): e2=$e2")
            //log.debug("onFling(): velocityX=$velocityX; velocityY=$velocityY")
            if (mInStartSelectionMode && !mSelectionGestureCompleted) {
                return true
            }
            if (isAnimationAllowed || !mmIsPageMode) {
                if (mmInScrollEvent) {
                    mmInScrollEvent = false
                    synchronized(mAnimationLocker) {
                        if (null != mCurrentAnimation) {
                            val forward = mmLastDir == mCurrentAnimation!!.direction
                            var acceleration = 0.0f
                            var speed = 50.0f
                            val isPageMode =
                                mCurrentAnimation is PageFlipAnimation || mCurrentAnimation is NaturalPageFlipAnimation
                            if (isPageMode) {
                                acceleration = 0.0f
                                speed = mmScrollSpeedX
                                if (speed < 20.0f)
                                    speed = 20.0f
                            } else {
                                //acceleration = -0.6f
                                //acceleration = -1.0f
                                acceleration = -0.8f
                                speed = mmScrollSpeedY
                                if (speed < 5.0f)
                                    speed = 5.0f
                            }
                            //log.d("onFling(): speed=" + speed)
                            mAnimationThread.startAnimation(
                                mCurrentAnimation!!,
                                forward,
                                speed,
                                acceleration,
                                if (forward) {
                                    {
                                        synchronized(mAnimationLocker) {
                                            if (null != mCurrentAnimation) {
                                                val curr_dir = mCurrentAnimation!!.direction
                                                val cmd: ReaderCommand
                                                var param = 0
                                                if (isPageMode) {
                                                    cmd =
                                                        if (curr_dir > 0) ReaderCommand.DCMD_PAGEDOWN else ReaderCommand.DCMD_PAGEUP
                                                    param = 1
                                                } else {
                                                    cmd = ReaderCommand.DCMD_SCROLL_BY
                                                    param = mCurrentAnimation!!.position
                                                }
                                                mCurrentAnimation =
                                                    null // nullify to not prevent asyncDraw()
                                                processEngineCommand(cmd, param)
                                                scheduleSaveCurrentPositionBookmark(
                                                    DEF_SAVE_POSITION_INTERVAL
                                                )
                                            }
                                        }
                                    }
                                } else {
                                    {
                                        synchronized(mAnimationLocker) {
                                            // nullify to not prevent asyncDraw()
                                            mCurrentAnimation = null
                                        }
                                        mCurrentSelection?.let {
                                            if (it.isNotEmpty)
                                                asyncDrawPage()
                                        }
                                    }
                                })
                        }
                    }
                }
            } else {
                if (mmInScrollEvent) {
                    mmInScrollEvent = false
                    val cmd =
                        if (mmLastDir > 0) ReaderCommand.DCMD_PAGEDOWN else ReaderCommand.DCMD_PAGEUP
                    processEngineCommand(cmd, 1)
                }
            }
            return true
        }
    } // class GestureListener

    private inner class MyDocViewCallback : DocViewCallback {
        override fun onLoadFileStart(filename: String?) {
            cancelUpdateCacheTask()
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onLoadFileStart $filename")
            showProgress(10, R.string.progress_task_loading)
        }

        override fun onLoadFileFormatDetected(fileFormat: DocumentFormat?): String? {
            BackgroundThread.ensureBackground()
            log.info("readerCallback.onLoadFileFormatDetected $fileFormat")
            return fileFormat?.let { getCSSContentsForFormat(it) }
        }

        override fun onLoadFileEnd() {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onLoadFileEnd")
            if (mRenderWidth == 0 && mRenderHeight == 0) {
                mRenderWidth = mRequestedWidth
                mRenderHeight = mRequestedHeight
                log.debug("onLoadFileEnd: resize($mRenderWidth,$mRenderHeight)")
                mDoc.resize(mRenderWidth, mRenderHeight)
            }
        }

        override fun onLoadFileFirstPagesReady() {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onLoadFileFirstPagesReady")
        }

        override fun onLoadFileProgress(percent: Int) {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onLoadFileProgress $percent")
            showProgress(percent * 4 / 10 + 10, R.string.progress_task_loading)
        }

        override fun onFormatStart() {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onFormatStart")
            showProgress(50, R.string.progress_task_formatting)
        }

        override fun onFormatEnd() {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onFormatEnd")
            hideProgress()
            asyncDrawPage()
            scheduleUpdateCacheTask()
            mToc = mDoc.getTOC()
        }

        override fun onFormatProgress(percent: Int) {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onFormatProgress $percent")
            showProgress(percent / 2 + 50, R.string.progress_task_formatting)
        }

        override fun onExportProgress(percent: Int) {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onExportProgress $percent")
        }

        override fun onLoadFileError(message: String?) {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onLoadFileError($message)")
        }

        override fun onExternalLink(url: String?, nodeXPath: String?) {
            BackgroundThread.ensureBackground()
        }

        @Synchronized
        override fun onImageCacheClear() {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onImageCacheClear")
            invalidateImageCache()
        }

        override fun onRequestReload(): Boolean {
            BackgroundThread.ensureBackground()
            log.debug("readerCallback.onRequestReload")
            // TODO: implement this...
            return false
        }

        fun finalize() {
            log.debug("readerCallback.finalize()")
        }

        init {
            log.debug("readerCallback.MyDocViewCallback()")
        }
    } // class MyDocViewCallback

    private inner class DrawPageTask(var doneCallback: (() -> Unit)?) : EngineTask() {
        val id: Long
        var pic: PageImageCache? = null
        override fun work() {
            BackgroundThread.ensureBackground()
            if (id != mLastDrawTaskId) {
                log.debug("skipping duplicate drawPage request")
                return
            }
            synchronized(mAnimationLocker) {
                if (mCurrentAnimation != null) {
                    log.debug("skipping drawPage request while animation is in progress")
                    return
                }
                log.debug("DrawPageTask.work(): " + mRenderWidth + "x" + mRenderHeight)
                pic = preparePageImage(0)
                if (null != pic) {
                    mCurrentSelection?.let {
                        if (it.isNotEmpty) {
                            val llp = Point(it.startHandleWndPos)
                            mDoc.docToWindowPoint(it.startHandleWndPos, it.startHandleDocPos)
                            mDoc.docToWindowPoint(it.endHandleWndPos, it.endHandleDocPos)
                            val offset = it.startHandleWndPos.minus(llp)
                            mSelectionStart.offset(offset.x, offset.y)
                            mSelectionEnd.offset(offset.x, offset.y)
                        }
                    }
                    updateTextSelectHandleBounds()
                    drawOnSurface()
                    updateCurrentPositionStatus()
                } else {
                    log.error("Failed to get page image!")
                }
            }
        }

        override fun done() {
            //BackgroundThread.ensureGUI()
            doneCallback?.invoke()
        }

        override fun fail(e: Exception) {
            hideProgress()
        }

        init {
            mLastDrawTaskId++
            id = mLastDrawTaskId
            //cancelGc()
        }
    } // class DrawPageTask

    private inner class UpdateCacheTask : EngineTask() {
        private var mmTimeout = 10000L
        private var mmIsTimeout = false

        fun reschedule() {
            synchronized(mUpdateCacheLocker) {
                if (this !== mCurrentUpdateCacheTask)
                    return
                BackgroundThread.postGUI({
                    synchronized(mUpdateCacheLocker) {
                        if (null == mCurrentUpdateCacheTask)
                        // update cache task was canceled
                            return@postGUI
                        BackgroundThread.postEngineTask(this@UpdateCacheTask)
                    }
                }, 2000)
            }
        }

        fun cancel() {
            BackgroundThread.postBackground { mDoc.cancelLongCachingOperation() }
        }

        override fun work() {
            if (this !== mCurrentUpdateCacheTask)
                return
            val startTime = Utils.uptime()
            val res = mDoc.updateCache(mmTimeout)
            mmIsTimeout = res == LVDocViewWrapper.ContinuousOperationResult.CR_TIMEOUT
            val duration = Utils.uptimeElapsed(startTime)
            if (!mmIsTimeout) {
                log.info("updateCache is finished with result $res in $duration ms")
            } else {
                log.debug("updateCache exited by TIMEOUT in $duration ms: rescheduling")
            }
        }

        override fun done() {
            if (mmIsTimeout) {
                // increase timeout and retry
                mmTimeout *= 2
                reschedule()
            }
        }

        override fun fail(e: Exception) {
            log.error("UpdateCacheTask failed", e)
        }
    } // class UpdateCacheTask

    private inner class LoadDocumentTask(
        bookInfo: BookInfo,
        private val mmInputStream: InputStream?,
        private val mmDoneRunnable: Runnable?,
        private val mmErrorRunnable: Runnable?
    ) : EngineTask() {
        private val mmPathNameWA: String
        private var mmDocBinaryData: ByteArray? = null
        private var mmStartPos: String? = null

        override fun work() {
            BackgroundThread.ensureBackground()
            val bookInfo = mBookInfo as BookInfo
            log.info("Loading document $mmPathNameWA")
            mDoc.processCommand(ReaderCommand.DCMD_CLOSE_BOOK.nativeId, 0)
            showProgress(0, R.string.progress_task_loading)
            var success = false
            var isFromStream = false
            if (null != mmInputStream) {
                isFromStream = true
                // When loading from a memory stream, we don't yet have information about document flags
                //  So let's skip them, this will be done after opening the document
                // LVDocView requires a stream to be seekable to open,
                //  but the external stream from the Android intent is not seekable.
                //  So, we copy the data from this input stream into an byte array
                // TODO: For big files (for example, >= 100MB)
                //  save this stream to temporary file instead of byte array
                val outputStream = ByteArrayOutputStream()
                var copyOk = false
                try {
                    val buf = ByteArray(4096)
                    var readBytes: Int
                    while (true) {
                        readBytes = mmInputStream.read(buf)
                        if (readBytes > 0)
                            outputStream.write(buf, 0, readBytes)
                        else
                            break
                    }
                    copyOk = true
                    mmInputStream.close()
                } catch (e1: IOException) {
                    log.error("I/O error while copying content from input stream to buffer. Interrupted.")
                } catch (e2: OutOfMemoryError) {
                    log.error("Out of memory while copying content from input stream to buffer. Interrupted.")
                }
                if (copyOk)
                    mmDocBinaryData = outputStream.toByteArray()
            } else {
                // Firstly set document flags, DOM level, block rendering flags
                mDoc.processCommand(
                    ReaderCommand.DCMD_SET_INTERNAL_STYLES.nativeId,
                    if (bookInfo.isFlagSet(BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG)) 0 else 1
                )
                mDoc.processCommand(
                    ReaderCommand.DCMD_SET_DOC_FONTS.nativeId,
                    if (bookInfo.isFlagSet(BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG)) 0 else 1
                )
                mDoc.processCommand(
                    ReaderCommand.DCMD_SET_TEXT_FORMAT.nativeId,
                    if (bookInfo.isFlagSet(BookInfo.DONT_REFLOW_TXT_FILES_FLAG)) 0 else 1
                )
                log.info("Processing DCMD_SET_REQUESTED_DOM_VERSION to ${bookInfo.domVersion}")
                mDoc.processCommand(
                    ReaderCommand.DCMD_SET_REQUESTED_DOM_VERSION.nativeId,
                    bookInfo.domVersion
                )
                if (0 == bookInfo.domVersion) {
                    log.info("Processing DCMD_SET_RENDER_BLOCK_RENDERING_FLAGS to 0")
                    mDoc.processCommand(
                        ReaderCommand.DCMD_SET_RENDER_BLOCK_RENDERING_FLAGS.nativeId,
                        0
                    )
                } else {
                    log.info("Processing DCMD_SET_RENDER_BLOCK_RENDERING_FLAGS to ${bookInfo.blockRenderingFlags}")
                    mDoc.processCommand(
                        ReaderCommand.DCMD_SET_RENDER_BLOCK_RENDERING_FLAGS.nativeId,
                        bookInfo.blockRenderingFlags
                    )
                }
            }
            if (isFromStream) {
                mmDocBinaryData?.let {
                    success = mDoc.loadDocument(it, mmPathNameWA)
                }
            } else {
                success = mDoc.loadDocument(mmPathNameWA)
            }
            if (success) {
                mDoc.setBookmarks(bookInfo)
                // get title, authors, genres, language, etc.
                log.verbose("updating loaded book info")
                updateBookInfo(true)
                mCanGoBack = mDoc.canGoBack()
                // Reapply settings to:
                //  set main text language for enhanced rendering (PROP_TEXTLANG_MAIN_LANG), etc...
                val currSettings = SRProperties(mSettings)
                applySettings(currSettings)
                mDoc.requestRender()
                mToc = mDoc.getTOC()
                log.verbose("requesting page image, to render")
                preparePageImage(0)
                if (null == mmDocBinaryData) {
                    log.info("Document $mmPathNameWA is loaded successfully")
                    if (mmStartPos != null) {
                        log.info("Restoring position : $mmStartPos")
                        restorePositionBackground(mmStartPos)
                    }
                    updateCurrentPositionStatus()
                } else {
                    log.info("Stream $mmPathNameWA loaded successfully")
                    // In this case, we are delegating part of the work to the database service,
                    // which is running on a different thread, so we need to tell the task handler about it.
                    setTaskRequireWait()
                    convertDocBinaryDataToFile(
                        bookInfo,
                        mmDocBinaryData!!,
                        object : DBService.BookInfoLoadingCallback {
                            override fun onBooksInfoLoaded(bookInfo: BookInfo?) {
                                if (null != bookInfo) {
                                    // Now we have all info about document file
                                    //  we can apply document flags & other options
                                    mmStartPos = null
                                    val initBookInfo = mBookInfo as BookInfo
                                    mBookInfo = bookInfo
                                    // The database may not contain up-to-date data for some properties,
                                    //  so we use the properties that were obtained when opening the document file.
                                    mBookInfo?.authors = initBookInfo.authors
                                    mBookInfo?.keywords = initBookInfo.keywords
                                    mBookInfo?.lastPosition?.let {
                                        mmStartPos = it.startPos
                                        bookReadingDuration = it.readingTime
                                    } ?: run {
                                        bookReadingDuration = 0
                                    }
                                    applyPerDocumentPropertiesByEngine(initBookInfo, bookInfo)
                                    BackgroundThread.postBackground {
                                        if (null != mmStartPos) {
                                            log.info("Restoring position : $mmStartPos")
                                            restorePositionBackground(mmStartPos)
                                        }
                                        updateCurrentPositionStatus()
                                        // this task is done in another thread
                                        setTaskDone()
                                        log.debug("LoadDocumentTask, find/create book for stream completed successfully")
                                    }
                                } else {
                                    // this task is done in another thread
                                    setTaskDone()
                                    throw RuntimeException("Failed to convert stream $mBookInfo to file")
                                }
                            }
                        })
                }
            } else {
                log.error("Error occurred while trying to load document $mmPathNameWA")
                throw IOException("Cannot read document")
            }
        }

        override fun done() {
            BackgroundThread.ensureGUI()
            log.debug("LoadDocumentTask, book download completed successfully")
            mBookInfo!!.updateAccess()
            val infoToSave = BookInfo(mBookInfo!!)
            dbServiceAcc?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.saveBookInfo(infoToSave)
                    infoToSave.fileInfo.pathNameWA?.let {
                        binder.setCurrentBook(
                            it,
                            Utils.timeStamp()
                        )
                    }
                }
            })
            // TODO: set book cover as screensaver on e-ink
            hideProgress()
            asyncDrawPage()
            //mActivity.showReader()
            mmDoneRunnable?.run()
            mBookOpened = true
        }

        override fun fail(e: Exception) {
            BackgroundThread.ensureGUI()
            close()
            log.verbose("LoadDocumentTask failed for $mmPathNameWA", e)
            val failedPath = mBookInfo!!.fileInfo.pathNameWA
            mBookInfo = null
            mBookOpened = false
            mCanGoBack = false
            val errorTitle = context.resources.getString(R.string.error)
            val errorString = context.resources.getString(R.string.error_while_opening, failedPath)
            BackgroundThread.executeBackground {
                mDoc.createDefaultDocument(
                    errorTitle,
                    errorString
                )
                mDoc.requestRender()
                asyncDrawPage()
            }
            hideProgress()
            activityControl?.showToast(R.string.error_while_opening, e.toString())
            mmErrorRunnable?.let {
                log.error("LoadDocumentTask: Calling error handler")
                it.run()
            }
        }

        init {
            val name = bookInfo.fileInfo.pathNameWA
            if (name != null)
                mmPathNameWA = name
            else
                throw RuntimeException("LoadDocumentTask: fileInfo can't be null or empty!")
            mBookInfo = BookInfo(bookInfo)
            bookInfo.lastPosition?.let {
                mmStartPos = it.startPos
                bookReadingDuration = it.readingTime
            } ?: run {
                bookReadingDuration = 0
            }
            log.verbose("LoadDocumentTask : book info $mBookInfo")
            log.verbose("LoadDocumentTask : last position = $mmStartPos")
        }
    }

    @Throws(RuntimeException::class)
    private fun convertDocBinaryDataToFile(
        bookInfo: BookInfo,
        docBinaryData: ByteArray,
        foundCallback: DBService.BookInfoLoadingCallback
    ) {
        BackgroundThread.ensureBackground()
        // Find existing book in DB with the same fingerprint
        //  or by book metadata (if fingerprint not specified)
        //  or save this byte array as file
        dbServiceAcc?.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.findBooks(bookInfo, object : DBService.BookSearchCallback {
                    override fun onBooksFound(books: Collection<BookInfo>?) {
                        var bi: BookInfo? = null
                        var existsBook: BookInfo? = null
                        var moreRecentBook: BookInfo? = null
                        books?.let {
                            for (book in it) {
                                if (null == existsBook && book.isBookFileExists) {
                                    existsBook = book
                                }
                                if ((book.lastPosition?.percent
                                        ?: 0) > (moreRecentBook?.lastPosition?.percent ?: 0)
                                )
                                    moreRecentBook = book
                            }
                        }
                        if (null != existsBook) {
                            log.info("existing book found: $existsBook")
                            bi = BookInfo(existsBook!!)
                            moreRecentBook?.let {
                                bi!!.setBookmarks(it.allBookmarks)
                            }
                        } else {
                            // save docBinaryData to file
                            //  and create `bi`
                            val savedBi = activityControl?.saveDataToFile(bookInfo, docBinaryData)
                            if (null != savedBi) {
                                log.info("Created new book file: $savedBi")
                                bi = BookInfo(savedBi)
                                bi.updateAccess()
                                val biToSave = BookInfo(bi)
                                binder.saveBookInfo(biToSave)
                            } else {
                                log.error("Failed to save document memory buffer to file!")
                                activityControl?.showToast(R.string.failed_to_save_memory_stream)
                                throw RuntimeException("Failed to save document memory buffer to file!")
                            }
                        }
                        foundCallback.onBooksInfoLoaded(bi)
                    }
                })
            }
        })
    }

    private fun showProgress(position: Int, titleResource: Int) {
        log.verbose("showProgress($position)")
        var update = false
        if (null == mCurrentProgressTask || mCurrentProgressTaskId != titleResource) {
            mCurrentProgressTaskId = titleResource
            mCurrentProgressTask = context.resources.getString(mCurrentProgressTaskId)
            update = true
        }
        if (mCurrentProgressPosition != position) {
            mCurrentProgressPosition = position
            update = true
        }
        if (update) {
            BackgroundThread.executeGUI {
                if (mProgressPopup.isNotShowing) {
                    mProgressPopup.x = 0
                    mProgressPopup.y = 0
                    mProgressPopup.width = ViewGroup.LayoutParams.MATCH_PARENT
                    mProgressPopup.height = ViewGroup.LayoutParams.MATCH_PARENT
                    mProgressPopup.show()
                }
                mProgressPopup.progressTask = mCurrentProgressTask as String
                mProgressPopup.progress = mCurrentProgressPosition
            }
        }
    }

    private fun hideProgress() {
        log.verbose("hideProgress()")
        BackgroundThread.executeGUI {
            mProgressPopup.hide()
        }
        // TODO: for e-ink make full refresh after progress popup gone
    }

    private fun isProgressActive(): Boolean {
        return mProgressPopup.isShowing
    }

    private fun startBrightnessControl(x: Int, y: Int) {
        activityControl?.let {
            mCurrentBrightnessValue = it.getScreenBacklightLevel()
        }
        mCurrentBrightnessPrevYPos = y
        updateBrightnessControl(x, y)
    }

    private fun updateBrightnessControl(x: Int, y: Int) {
        val diff = 100 * (mCurrentBrightnessPrevYPos - y) / this@ReaderView.height
        var newValue = mCurrentBrightnessValue + diff
        if (newValue < -1)
            newValue = -1
        else if (newValue > 100)
            newValue = 100
        if (newValue != mCurrentBrightnessValue) {
            mCurrentBrightnessValue = newValue
            activityControl?.setScreenBacklightLevel(mCurrentBrightnessValue)
            mCurrentBrightnessPrevYPos = y
        }
    }

    private fun stopBrightnessControl(x: Int, y: Int) {
        if (mCurrentBrightnessPrevYPos > -1) {
            updateBrightnessControl(x, y)
            activityControl?.setSetting(
                PropNames.App.SCREEN_BACKLIGHT,
                mCurrentBrightnessValue,
                false
            )
            mCurrentBrightnessPrevYPos = -1
            mCurrentBrightnessValue = -1
        }
    }

    private fun startImageViewer(image: PageImageCache) {
        if (null != mImageViewer)
            mImageViewer?.close()
        if (null == image.bitmap)
            return
        // Recycle & nullify mCurrentPageCache, mNextPageCache to save memory
        mCurrentPageCache?.recycle()
        mCurrentPageCache = null
        mNextPageCache?.recycle()
        mNextPageCache = null
        val layoutInflater = LayoutInflater.from(context)
        val imageViewBarView = layoutInflater.inflate(R.layout.imageview_bar, null)
        mImageViewer = ImageViewer(this, image, imageViewBarView)
        mImageViewer!!.onCloseListener = object : OnCloseListener {
            override fun onClose() {
                mImageViewer = null
                asyncDrawPage()
            }
        }
        asyncDrawPage()
    }

    private fun closeImageViewer() {
        if (null != mImageViewer) {
            mImageViewer?.close()
            mImageViewer = null
        }
    }

    private fun applyPerDocumentProperties(props: SRProperties) {
        if (mBookOpened) {
            mBookInfo?.let {
                val oldBookInfo = BookInfo(it)
                if (props.haveProperty(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES)) {
                    it.flags =
                        if (!props.getBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES, true))
                            it.flags or BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG
                        else
                            it.flags and BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG.inv()
                }
                if (props.haveProperty(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS)) {
                    it.flags =
                        if (props.getBool(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS, true))
                            it.flags and BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG.inv()
                        else
                            it.flags or BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG
                }
                if (props.haveProperty(PropNames.Document.PROP_PER_DOC_TXT_OPTION_PREFORMATTED)) {
                    it.flags = if (props.getBool(
                            PropNames.Document.PROP_PER_DOC_TXT_OPTION_PREFORMATTED,
                            false
                        )
                    )
                        it.flags or BookInfo.DONT_REFLOW_TXT_FILES_FLAG
                    else
                        it.flags and BookInfo.DONT_REFLOW_TXT_FILES_FLAG.inv()
                }
                if (props.haveProperty(PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION)) {
                    it.domVersion = props.getInt(
                        PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION,
                        CREngineNGBinding.DOM_VERSION_CURRENT
                    )
                }
                if (props.haveProperty(PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS)) {
                    it.blockRenderingFlags = props.getInt(
                        PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
                        CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
                    )
                }
                val haveChanges: Boolean =
                    (oldBookInfo.flags != it.flags) || (oldBookInfo.domVersion != it.domVersion) || (oldBookInfo.blockRenderingFlags != it.blockRenderingFlags)
                if (haveChanges) {
                    applyPerDocumentPropertiesByEngine(oldBookInfo, it)
                    saveCurrentBookInfo()
                }
            }
        }
    }

    private fun applyPerDocumentPropertiesByEngine(oldBookInfo: BookInfo, newBookInfo: BookInfo) {
        val changedFlags = oldBookInfo.flags xor newBookInfo.flags
        if ((changedFlags and BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG) != 0) {
            val useDocStyles =
                if (!newBookInfo.isFlagSet(BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG)) 1 else 0
            processEngineCommand(ReaderCommand.DCMD_SET_INTERNAL_STYLES, useDocStyles)
        }
        if ((changedFlags and BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG) != 0) {
            val useDocFonts =
                if (!newBookInfo.isFlagSet(BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG)) 1 else 0
            processEngineCommand(ReaderCommand.DCMD_SET_DOC_FONTS, useDocFonts)
        }
        if ((changedFlags and BookInfo.DONT_REFLOW_TXT_FILES_FLAG) != 0) {
            val autoTextFormat =
                if (!newBookInfo.isFlagSet(BookInfo.DONT_REFLOW_TXT_FILES_FLAG)) 1 else 0
            processEngineCommand(ReaderCommand.DCMD_SET_TEXT_FORMAT, autoTextFormat)
        }
        if (oldBookInfo.domVersion != newBookInfo.domVersion) {
            processEngineCommand(
                ReaderCommand.DCMD_SET_REQUESTED_DOM_VERSION,
                newBookInfo.domVersion
            )
        }
        if (oldBookInfo.blockRenderingFlags != newBookInfo.blockRenderingFlags) {
            processEngineCommand(
                ReaderCommand.DCMD_SET_RENDER_BLOCK_RENDERING_FLAGS,
                newBookInfo.blockRenderingFlags
            )
        }
    }

    private fun setup() {
        mDPI = Utils.DPI
        mGestureDetector = MyGestureDetector(context, mGestureListener)
        mGestureDetector.setIsLongpressEnabled(true)
        holder.setSizeFromLayout()
        holder.addCallback(SurfaceCallback())
        isFocusable = true
        isFocusableInTouchMode = true
        mDoc.setDocViewCallback(MyDocViewCallback())
        val symbols = DecimalFormatSymbols(SettingsManager.activeLang.locale)
        BackgroundThread.postBackground {
            mDoc.createDocView()
            mDoc.createDefaultDocument(
                context.getString(R.string.there_is_no_open_book),
                context.getString(R.string.app_welcome_message)
            )
            mDoc.setDecimalPointChar(symbols.decimalSeparator)
            mInitialized = true
        }
        updateTextSelectHandleAssets()
        requestResize(100, 100)

        // Create internal views

        val layoutInflater = LayoutInflater.from(context)

        val progressContentView = layoutInflater.inflate(R.layout.reader_progress, null)
        mProgressPopup = ProgressPopup(this, progressContentView)
        mProgressPopup.popupGravity = Gravity.FILL
        mProgressPopup.isIndeterminate = false

        val docContentsView = layoutInflater.inflate(R.layout.doc_contents_panel, null)
        mDocContentPopup = DocContentsPopup(this, docContentsView)
        mDocContentPopup.onTOCItemClickListener = object : DocContentsPopup.OnTOCItemClickListener {
            override fun onTOCItemClick(item: DocumentTOCItem) {
                goToPage(item.page + 1)
            }
        }

        val goPanelView = layoutInflater.inflate(R.layout.go_panel, null)
        mGoPanelPopup = GoPanelPopup(this, goPanelView)
        mGoPanelPopup.onGoPanelClickListener = object : GoPanelPopup.GoPanelListener {
            override fun onPrevChapterClick() {
                processCommand(ReaderCommand.DCMD_MOVE_BY_CHAPTER, -1, null)
            }

            override fun onNextChapterClick() {
                processCommand(ReaderCommand.DCMD_MOVE_BY_CHAPTER, 1, null)
            }

            override fun onPrevPageClick() {
                processCommand(ReaderCommand.DCMD_PAGEUP, 1, null)
            }

            override fun onNextPageClick() {
                processCommand(ReaderCommand.DCMD_PAGEDOWN, 1, null)
            }

            override fun jumpToPage(pageNo: Int) {
                goToPage(pageNo)
            }

            override fun onShowTOC() {
                showDocContents()
            }
        }

        val bookmarksView = layoutInflater.inflate(R.layout.bookmarks_panel, null)
        mBookmarksPopup = BookmarksPopup(this, bookmarksView)
        mBookmarksPopup.onBookmarkClickListener = object : BookmarksPopup.OnBookmarkClickListener {
            override fun onBookmarkClick(item: Bookmark) {
                goToBookmark(item)
            }

            override fun onAddClicked() {
                processReaderAction(ReaderActionRegistry.NEW_BOOKMARK_PAGE)
                mBookmarksPopup.hide()
            }

            override fun onBookmarkChanged(bookmarks: List<Bookmark>) {
                // Sync new bookmarks with mBookInfo
                mBookInfo?.let {
                    val lastPos = it.lastPosition
                    it.setBookmarks(bookmarks)
                    if (null != lastPos)
                        it.addBookmark(lastPos)
                    BackgroundThread.postBackground {
                        mDoc.setBookmarks(it)
                        invalidateImageCache()
                        asyncDrawPage()
                    }
                }
                saveCurrentBookInfo()
            }
        }

        val stylePanelView = layoutInflater.inflate(R.layout.style_panel, null)
        mStylePanelPopup = StylePanelPopup(this, stylePanelView)
        mStylePanelPopup.onStylePanelListener = object : StylePanelPopup.StylePanelListener {
            override fun onPropertiesChanged(props: SRProperties) {
                // filter per-document options
                applyPerDocumentProperties(props)
                val newProps = SRProperties(props)
                newProps.remove(PropNames.Document.PROP_PER_DOC_TXT_OPTION_PREFORMATTED)
                newProps.remove(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES)
                newProps.remove(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS)
                newProps.remove(PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION)
                newProps.remove(PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS)
                activityControl?.setSettings(newProps, true)
            }
        }
        mStylePanelPopup.decorView = decorView
        val readerMenuView = layoutInflater.inflate(R.layout.reader_menu, null)
        mReaderMenuPopup = ReaderMenuPopup(this, readerMenuView)

        val selectionPanelView = layoutInflater.inflate(R.layout.selection_panel, null)
        mSelectionPanelPopup = SelectionPanelPopup(this, selectionPanelView)
        mSelectionPanelPopup.selectionPanelListener =
            object : SelectionPanelPopup.SelectionPanelListener {
                override fun onStickToWordBoundary(value: Boolean) {
                    mSelectionStickyWordBoundaries = value
                }

                override fun onCopyToClipboard() {
                    mCurrentSelection?.let { sel ->
                        mBookInfo?.let { bi ->
                            val text = sel.text
                            if (!text.isNullOrEmpty()) {
                                activityControl?.copyBookQuote(bi, text)
                            }
                        }
                    }
                    clearSelection()
                }

                override fun onShare() {
                    mCurrentSelection?.let { sel ->
                        mBookInfo?.let { bi ->
                            val text = sel.text
                            if (!text.isNullOrEmpty()) {
                                activityControl?.shareBookQuote(bi, text)
                            }
                        }
                    }
                    clearSelection()
                }

                override fun onNewBookmark() {
                    mCurrentSelection?.let { sel ->
                        mBookInfo?.let { bi ->
                            val text = sel.text
                            if (!text.isNullOrEmpty()) {
                                val bm = Bookmark()
                                bm.type = Bookmark.TYPE_COMMENT
                                bm.startPos = sel.startXPtr
                                bm.endPos = sel.endXPtr
                                bm.titleText = sel.chapter
                                bm.posText = text
                                bm.percent = sel.percent
                                bm.readingTime = bookReadingDuration
                                activityControl?.showBookmarkEditFragment(bm, true,
                                    object : BookmarkEditDialogFragment.OnActionsListener {
                                        override fun onAddBookmark(bookmark: Bookmark) {
                                            if (bi.addBookmark(bm)) {
                                                saveCurrentBookInfo()
                                                BackgroundThread.postBackground {
                                                    mDoc.setBookmarks(bi)
                                                    invalidateImageCache()
                                                    asyncDrawPage()
                                                }
                                            }
                                        }

                                        override fun onRemoveBookmark(bookmark: Bookmark) {
                                        }
                                    })
                            }
                        }
                    }
                    clearSelection()
                }

                override fun onOpenDictionary() {
                    mCurrentSelection?.let { sel ->
                        val text = sel.text
                        if (!text.isNullOrEmpty()) {
                            activityControl?.openExternalDictionary(text)
                        }
                    }
                    clearSelection()
                }

                override fun onCancel() {
                    clearSelection()
                }
            }

        val ttsPanelView = layoutInflater.inflate(R.layout.tts_panel, null)
        mTTSPanelPopup = TTSPanelPopup(this, ttsPanelView)
        mTTSPanelPopup.ttsPanelListener =
            object : TTSPanelPopup.TTSPanelListener {
                override fun onPlayPause() {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.getState(object :
                                    TTSControlService.RetrieveStateCallback {
                                    override fun onResult(state: TTSControlService.State?) {
                                        if (TTSControlService.State.PLAYING != state) {
                                            mTTSSelection?.let {
                                                ttsbinder.say(it.text, null)
                                            }
                                        } else {
                                            ttsbinder.pause(null)
                                        }
                                    }
                                })
                            }
                        }
                    )
                }

                override fun onClose() {
                    stopTTSAndClosePanel()
                }

                override fun onPrev() {
                    context.sendBroadcast(Intent(TTSControlService.TTS_CONTROL_ACTION_SKIP_PREV).apply {
                        setPackage(context.packageName)
                    })
                }

                override fun onNext() {
                    context.sendBroadcast(Intent(TTSControlService.TTS_CONTROL_ACTION_SKIP_NEXT).apply {
                        setPackage(context.packageName)
                    })
                }

                override fun onDecVolume() {
                    if (mTTSVolume > 0) {
                        mTTSControlServiceAccessor?.runWithService(
                            object : TTSControlServiceAccessor.Callback {
                                override fun run(ttsbinder: TTSControlBinder) {
                                    ttsbinder.setVolume(mTTSVolume - 1)
                                }
                            })
                    }
                }

                override fun onIncVolume() {
                    if (mTTSVolume < mTTSMaxVolume) {
                        mTTSControlServiceAccessor?.runWithService(
                            object : TTSControlServiceAccessor.Callback {
                                override fun run(ttsbinder: TTSControlBinder) {
                                    ttsbinder.setVolume(mTTSVolume + 1)
                                }
                            })
                    }
                }

                override fun onSetVolume(volume: Int) {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.setVolume(volume)
                            }
                        })
                }

                override fun onDecSpeechRate() {
                    val idx = mTTSSpeechRateIdx - 1
                    if (idx >= 0)
                        onSetSpeechRate(TTS_SPEECH_RATE_VALUES[idx])
                }

                override fun onIncSpeechRate() {
                    val idx = mTTSSpeechRateIdx + 1
                    if (idx < TTS_SPEECH_RATE_VALUES.size)
                        onSetSpeechRate(TTS_SPEECH_RATE_VALUES[idx])
                }

                override fun onSetSpeechRate(speechRate: Float) {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.setSpeechRate(
                                    Utils.findNearestValue(
                                        TTS_SPEECH_RATE_VALUES,
                                        speechRate
                                    ), null
                                )
                            }
                        })
                }

                override fun onOpenOptions() {
                    activityControl?.openSettings("io.gitlab.coolreader_ng.project_s.SettingsFragmentTTS")
                }
            }
        mAnimationThread = AnimationThread(this@ReaderView.holder).apply {
            setOnStopCallback {
                log.debug("animation stopped.")
                synchronized(mAnimationLocker) {
                    mAnimAvgFrameTime = avgFrameTime
                    mCurrentAnimation = null
                }
            }
            start()
        }
        // Setting up some default properties
        mPageAnimationType = PageAnimationType.Paper
        mHorizDeadIndent = 0
        mVertDeadIndent = 0
    }

    private fun clearSelection() {
        BackgroundThread.ensureGUI()
        if (mBookInfo == null || !mBookOpened)
            return
        BackgroundThread.postEngineTask(object : EngineTask() {
            override fun work() {
                BackgroundThread.ensureBackground()
                if (mBookOpened) {
                    mDoc.clearSelection()
                    invalidateImageCache()
                    mCurrentSelection = null
                    asyncDrawPage()
                }
            }

            override fun done() {
                synchronized(mSystemGestureExclusionRects) {
                    mSystemGestureExclusionRects.clear()
                }
                updateSystemGestureExclusionRects()
                mInStartSelectionMode = false
                mTextSelectionIsDraggingLeftHandle = false
                mTextSelectionIsDraggingRightHandle = false
                mSelectionPanelPopup.hide()
                restoreViewMode()
            }

            override fun fail(e: Exception) {
                log.error("clearSelection(): $e")
            }
        })
    }

    private fun updateSelection(
        startPt: Point,
        endPt: Point,
        stickyBounds: Boolean,
        isUpdateEnd: Boolean
    ) {
        mLastUpdateTaskId++
        val myId = mLastUpdateTaskId
        val prevText = mCurrentSelection?.text ?: ""
        BackgroundThread.postEngineTask(object : EngineTask() {
            override fun work() {
                if (myId != mLastUpdateTaskId && !isUpdateEnd)
                    return
                val res = mDoc.selectText(
                    startPt.x,
                    startPt.y,
                    endPt.x,
                    endPt.y,
                    stickyBounds,
                    mCurrentSelection
                )
                if (res) {
                    invalidateImageCache()
                    asyncDrawPage()
                }
            }

            override fun done() {
                var active = false
                var needScrollViewMode = false
                val maxMarginY = mRenderHeight / 7
                mCurrentSelection?.let {
                    if (it.isNotEmpty) {
                        if (it.startHandleWndPos.y < maxMarginY || it.endHandleWndPos.y > mRenderHeight - maxMarginY)
                            needScrollViewMode = true
                        mSelectionStart.x = it.startHandleWndPos.x
                        mSelectionEnd.x = it.endHandleWndPos.x
                        mSelectionEnd.y = it.endHandleWndPos.y - 3
                        updateTextSelectHandleBounds()
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                            if (prevText != it.text)
                                performHapticFeedback(HapticFeedbackConstants.TEXT_HANDLE_MOVE)
                        }
                        active = true
                    }
                }
                if (needScrollViewMode)
                    overrideToScrollViewMode()
                showSelectionPanel(active)
                if (isUpdateEnd) {
                    val text = mCurrentSelection?.text
                    if (text?.isNotEmpty() == true) {
                        log.debug("selected text: $text")
                        // TODO: onSelectionComplete(selection)
                    } else {
                        clearSelection()
                    }
                }
            }

            override fun fail(e: Exception) {
                log.error("updateSelection(): $e")
            }
        })
    }

    @SuppressLint("ResourceType")
    private fun updateTextSelectHandleAssets() {
        val attrs = context.obtainStyledAttributes(
            intArrayOf(
                android.R.attr.textSelectHandleLeft,
                android.R.attr.textSelectHandleRight,
                android.R.attr.textColorHighlight
            )
        )
        val color = attrs.getColor(2, Color.BLUE)
        var drawable = attrs.getDrawable(0)
        if (null != drawable) {
            mTextSelectHandleLeftPadding.set(Utils.scanDrawableForVisibleArea(drawable))
            drawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(drawable, color)
            mTextSelectHandleLeftBounds.set(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
            mTextSelectHandleLeftDrawable = drawable
        }
        drawable = attrs.getDrawable(1)
        attrs.recycle()
        if (null != drawable) {
            drawable = DrawableCompat.wrap(drawable)
            mTextSelectHandleRightPadding.set(Utils.scanDrawableForVisibleArea(drawable))
            DrawableCompat.setTint(drawable, color)
            mTextSelectHandleRightBounds.set(
                0,
                0,
                drawable.intrinsicWidth,
                drawable.intrinsicHeight
            )
            mTextSelectHandleRightDrawable = drawable
        }
    }

    private fun updateTextSelectHandleBounds() {
        mCurrentSelection?.let { it ->
            if (it.isNotEmpty) {
                val leftHandleWidth = mTextSelectHandleLeftBounds.width()
                val leftHandleHeight = mTextSelectHandleLeftBounds.height()
                val rightHandleWidth = mTextSelectHandleRightBounds.width()
                val rightHandleHeight = mTextSelectHandleRightBounds.height()
                mTextSelectHandleLeftBounds.set(
                    it.startHandleWndPos.x - mTextSelectHandleLeftPadding.right,
                    it.startHandleWndPos.y + mTextSelectHandleLeftPadding.top,
                    it.startHandleWndPos.x + leftHandleWidth - mTextSelectHandleLeftPadding.right,
                    it.startHandleWndPos.y + leftHandleHeight - mTextSelectHandleLeftPadding.top
                )
                mTextSelectHandleRightBounds.set(
                    it.endHandleWndPos.x - mTextSelectHandleRightPadding.left,
                    it.endHandleWndPos.y + mTextSelectHandleRightPadding.top,
                    it.endHandleWndPos.x - mTextSelectHandleRightPadding.left + rightHandleWidth,
                    it.endHandleWndPos.y + rightHandleHeight - mTextSelectHandleRightPadding.top
                )
                mTextSelectHandleLeftDrawable?.bounds = mTextSelectHandleLeftBounds
                mTextSelectHandleRightDrawable?.bounds = mTextSelectHandleRightBounds
                synchronized(mSystemGestureExclusionRects) {
                    while (mSystemGestureExclusionRects.size < 2) {
                        mSystemGestureExclusionRects.add(Rect())
                    }
                    mSystemGestureExclusionRects[0].set(mTextSelectHandleLeftBounds)
                    mSystemGestureExclusionRects[1].set(mTextSelectHandleRightBounds)
                }
                updateSystemGestureExclusionRects()
            }
        }
    }

    private fun updateSystemGestureExclusionRects() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            BackgroundThread.executeGUI { requestLayout() }
        }
    }

    private fun updatePopupsForTheme() {
        val layoutInflater = LayoutInflater.from(context)

        val oldProgressPopup = mProgressPopup
        val progressContentView = layoutInflater.inflate(R.layout.reader_progress, null)
        mProgressPopup = ProgressPopup(this, progressContentView)
        mProgressPopup.popupGravity = Gravity.FILL
        mProgressPopup.isIndeterminate = false
        if (oldProgressPopup.isShowing) {
            oldProgressPopup.hide()
            mProgressPopup.show()
        }

        val oldDocContentsPopup = mDocContentPopup
        val onTOCItemClickListener = mDocContentPopup.onTOCItemClickListener
        val docContentsView = layoutInflater.inflate(R.layout.doc_contents_panel, null)
        mDocContentPopup = DocContentsPopup(this, docContentsView)
        mDocContentPopup.setToolbarPosition(
            mSettings.getInt(
                PropNames.App.TOOLBAR_LOCATION,
                PropNames.App.TOOLBAR_LOCATION_TOP
            )
        )
        mDocContentPopup.onTOCItemClickListener = onTOCItemClickListener
        if (oldDocContentsPopup.isShowing) {
            oldDocContentsPopup.hide()
            // No info to show new popup
        }

        val oldGoPanelPopup = mGoPanelPopup
        val onGoPanelClickListener = mGoPanelPopup.onGoPanelClickListener
        val goPanelView = layoutInflater.inflate(R.layout.go_panel, null)
        mGoPanelPopup = GoPanelPopup(this, goPanelView)
        mGoPanelPopup.onGoPanelClickListener = onGoPanelClickListener
        if (oldGoPanelPopup.isShowing) {
            oldGoPanelPopup.hide()
            // No info to show new popup
        }

        val oldBookmarksPopup = mBookmarksPopup
        val onBookmarkClickListener = mBookmarksPopup.onBookmarkClickListener
        val bookmarksView = layoutInflater.inflate(R.layout.bookmarks_panel, null)
        mBookmarksPopup = BookmarksPopup(this, bookmarksView)
        mBookmarksPopup.setToolbarPosition(
            mSettings.getInt(
                PropNames.App.TOOLBAR_LOCATION,
                PropNames.App.TOOLBAR_LOCATION_TOP
            )
        )
        mBookmarksPopup.onBookmarkClickListener = onBookmarkClickListener
        if (oldBookmarksPopup.isShowing) {
            oldBookmarksPopup.hide()
            mBookInfo?.let { mBookmarksPopup.show(it) }
        }

        val oldStylePanelPopup = mStylePanelPopup
        val onStylePanelListener = mStylePanelPopup.onStylePanelListener
        val stylePanelView = layoutInflater.inflate(R.layout.style_panel, null)
        mStylePanelPopup = StylePanelPopup(this, stylePanelView)
        mStylePanelPopup.onStylePanelListener = onStylePanelListener
        mStylePanelPopup.decorView = decorView
        if (oldStylePanelPopup.isShowing) {
            oldStylePanelPopup.hide()
            // No info to show new popup
        }

        val oldReaderMenuPopup = mReaderMenuPopup
        val readerMenuView = layoutInflater.inflate(R.layout.reader_menu, null)
        mReaderMenuPopup = ReaderMenuPopup(this, readerMenuView)
        if (oldReaderMenuPopup.isShowing) {
            oldReaderMenuPopup.hide()
            mReaderMenuPopup.show()
        }

        val oldSelectionPanelPopup = mSelectionPanelPopup
        val selectionPanelListener = oldSelectionPanelPopup.selectionPanelListener
        val selectionPanelView = layoutInflater.inflate(R.layout.selection_panel, null)
        mSelectionPanelPopup = SelectionPanelPopup(this, selectionPanelView)
        mSelectionPanelPopup.selectionPanelListener = selectionPanelListener
        if (oldSelectionPanelPopup.isShowing) {
            oldSelectionPanelPopup.hide()
            mSelectionPanelPopup.show()
        }

        val oldTTSPanelPopup = mTTSPanelPopup
        val ttsPanelListener = oldTTSPanelPopup.ttsPanelListener
        val ttsPanelView = layoutInflater.inflate(R.layout.tts_panel, null)
        mTTSPanelPopup = TTSPanelPopup(this, ttsPanelView)
        mTTSPanelPopup.ttsPanelListener = ttsPanelListener
        if (oldTTSPanelPopup.isShowing) {
            oldTTSPanelPopup.hide()
            mTTSPanelPopup.show()
            mTTSPanelPopup.update(mTTSIsSpeaking, mTTSVolume, mTTSMaxVolume, mTTSSpeechRate)
        }
    }

    fun getSettings(): SRProperties {
        BackgroundThread.ensureGUI()
        val engineSettings = SRProperties()
        // Run on a background thread and wait for completion
        val syncObject = Object()
        synchronized(syncObject) {
            BackgroundThread.postBackground {
                mDoc.getSettings(engineSettings)
                synchronized(syncObject) {
                    syncObject.notify()
                }
            }
            try {
                syncObject.wait()
            } catch (_: InterruptedException) {
            }
        }
        return engineSettings
    }

    fun setPageInsets(insets: Insets, allowPageHeaderOverlap: Boolean) {
        if (mPageInsets != insets || mAllowPageHeaderOverlap != allowPageHeaderOverlap) {
            mPageInsets = insets
            mAllowPageHeaderOverlap = allowPageHeaderOverlap
            BackgroundThread.executeBackground {
                mDoc.applyPageInsets(insets, allowPageHeaderOverlap)
                invalidateImageCache()
                asyncDrawPage()
            }
        }
    }

    /**
     * Change settings.
     *
     * @param settings are new settings
     */
    fun updateSettings(settings: SRProperties) {
        log.verbose("updateSettings() $settings")
        BackgroundThread.ensureGUI()
        val changedProps = settings.diff(mSettings)
        val currSettings = SRProperties(mSettings)
        var ttsVoiceChanges = false
        for (prop in changedProps) {
            onAppSettingChanged(prop.key.toString(), prop.value.toString())
            when (prop.key.toString()) {
                PropNames.App.TTS_USE_DOC_LANG,
                PropNames.App.TTS_FORCE_LANGUAGE,
                PropNames.App.TTS_VOICE ->
                    ttsVoiceChanges = true
            }
        }
        val changedSettings = settings.diff(currSettings)
        currSettings.setAll(changedSettings)
        // make a deep copy
        mSettings = SRProperties(settings)
        BackgroundThread.postBackground { applySettings(currSettings) }
        if (mTTSIsActive && ttsVoiceChanges)
            setupTTSVoice()
    }

    private fun applySettings(props_: SRProperties) {
        log.verbose("applySettings()")
        BackgroundThread.ensureBackground()
        val props = SRProperties(props_) // make a copy

        // Remove per-document properties
        // Instead before opening document use DCMD_* equivalent command
        props.remove(PropNames.Engine.PROP_TXT_OPTION_PREFORMATTED)
        props.remove(PropNames.Engine.PROP_EMBEDDED_STYLES)
        props.remove(PropNames.Engine.PROP_EMBEDDED_FONTS)
        props.remove(PropNames.Engine.PROP_REQUESTED_DOM_VERSION)
        props.remove(PropNames.Engine.PROP_RENDER_BLOCK_RENDERING_FLAGS)
        props.remove(PropNames.Document.PROP_PER_DOC_TXT_OPTION_PREFORMATTED)
        props.remove(PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES)
        props.remove(PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS)
        props.remove(PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION)
        props.remove(PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS)

        val isFullScreen = props.getBool(PropNames.App.FULLSCREEN, false)
        mShowBattery = props.getBool(PropNames.Engine.PROP_SHOW_BATTERY, true) && isFullScreen
        mShowTime = props.getBool(PropNames.Engine.PROP_SHOW_TIME, true) && isFullScreen
        val scrollMode = props.getInt(PropNames.Engine.PROP_PAGE_VIEW_MODE, 1) == 0
        // Force use scroll animation in scroll view mode
        mForceUseAnimation = scrollMode
        props.setBool(PropNames.Engine.PROP_SHOW_BATTERY, mShowBattery)
        props.setBool(PropNames.Engine.PROP_SHOW_TIME, mShowTime)
        val backgroundImageId: String = props.getProperty(PropNames.App.BACKGROUND_IMAGE, "")
        val backgroundColor: Int = props.getColor(PropNames.Engine.PROP_BACKGROUND_COLOR, 0xFFFFFF)
        setBackgroundTexture(backgroundImageId, backgroundColor)
        // TODO: set e-ink update mode & update interval
        // Set fallback main text language for enhanced rendering (PROP_TEXTLANG_MAIN_LANG)
        // Has no effect when using legacy rendering
        val fallbackLangAutoSet =
            props.getBool(PropNames.App.TEXTLANG_FALLBACK_MAIN_LANG_AUTOSET, true)
        if (fallbackLangAutoSet) {
            val bookLanguage = mBookInfo?.language
            if (!bookLanguage.isNullOrEmpty()) {
                props.setProperty(PropNames.Engine.PROP_TEXTLANG_MAIN_LANG, bookLanguage)
            }
        }
        // TODO: checkFontLanguageCompatibility
        mDoc.applySettings(props)
        invalidateImageCache()
        asyncDrawPage()
    }

    private fun onAppSettingChanged(key: String, value: String) {
        if (key.startsWith(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE)) {
            var tapType = SettingsManager.PressOrTapType.NORMAL
            var tapZoneS = key.substring(PropNames.App.TAP_ZONE_ACTIONS_TAP_BASE.length)
            if (tapZoneS.startsWith("long.")) {
                tapType = SettingsManager.PressOrTapType.LONG
                tapZoneS = tapZoneS.substring(5)
            } else if (tapZoneS.startsWith("double.")) {
                tapType = SettingsManager.PressOrTapType.DOUBLE
                tapZoneS = tapZoneS.substring(7)
            }
            val tapZone = try {
                tapZoneS.toInt()
            } catch (e: Exception) {
                -1
            }
            if (tapZone in 0..8) {
                when (tapType) {
                    SettingsManager.PressOrTapType.NORMAL ->
                        mSingleTapActions[tapZone] = ReaderActionRegistry.findById(value)

                    SettingsManager.PressOrTapType.LONG ->
                        mLongTapActions[tapZone] = ReaderActionRegistry.findById(value)

                    SettingsManager.PressOrTapType.DOUBLE ->
                        mDoubleTapActions[tapZone] = ReaderActionRegistry.findById(value)
                }
            }
            return
        }
        if (key.startsWith(PropNames.App.KEY_ACTIONS_PRESS_BASE)) {
            var keyType = SettingsManager.PressOrTapType.NORMAL
            var keyCodeS = key.substring(PropNames.App.KEY_ACTIONS_PRESS_BASE.length)
            if (keyCodeS.startsWith("long.")) {
                keyType = SettingsManager.PressOrTapType.LONG
                keyCodeS = keyCodeS.substring(5)
            }
            val keyCode = try {
                keyCodeS.toInt()
            } catch (e: Exception) {
                -1
            }
            if (keyCode > 0) {
                when (keyType) {
                    SettingsManager.PressOrTapType.NORMAL ->
                        mKeySinglePressActions[keyCode] = ReaderActionRegistry.findById(value)

                    SettingsManager.PressOrTapType.LONG ->
                        mKeyLongPressActions[keyCode] = ReaderActionRegistry.findById(value)

                    else -> {}
                }
            }
            return
        }
        when (key) {
            PropNames.App.DOUBLE_TAP_ACTIONS -> isDoubleTapForActionsAllowed =
                Utils.parseBool(value)

            PropNames.App.DOUBLE_TAP_SELECTION -> isDoubleTapForSelectionStart =
                Utils.parseBool(value)

            PropNames.App.LONG_TAP_ACTIONS -> mIsLongTapForActionsAllowed =
                Utils.parseBool(value)

            PropNames.App.LONG_TAP_SELECTION -> mIsLongTapForSelectionStart =
                Utils.parseBool(value)

            PropNames.App.KEY_ACTIONS_ENABLE_VOLUME_KEYS -> mAllowVolumeKeys =
                Utils.parseBool(value)

            PropNames.App.KEY_ACTIONS_HANDLE_LONG_PRESS -> mHandleLongKeyPress =
                Utils.parseBool(value)

            PropNames.App.VIEW_ANIM_DURATION -> mAnimAvgFrameTime =
                Utils.parseInt(value, 17).toLong()

            PropNames.App.TOOLBAR_LOCATION -> {
                mDocContentPopup.setToolbarPosition(
                    Utils.parseInt(value, PropNames.App.TOOLBAR_LOCATION_TOP)
                )
                mBookmarksPopup.setToolbarPosition(
                    Utils.parseInt(value, PropNames.App.TOOLBAR_LOCATION_TOP)
                )
            }

            PropNames.App.PAGE_ANIM_TYPE -> {
                mPageAnimationType = PageAnimationType.byOrdinal(Utils.parseInt(value, 0))
                mAnimationAllowed = mPageAnimationType != PageAnimationType.None
            }

            PropNames.App.FLICK_BACKLIGHT_CONTROL -> {
                mSensorBrightnessControlType =
                    BrightnessControlType.byOrdinal(Utils.parseInt(value, 0))
                if (BrightnessControlType.None == mSensorBrightnessControlType) {
                    // Reset screen brightness to the value corresponding to the system level
                    activityControl?.setSetting(PropNames.App.SCREEN_BACKLIGHT, -1, true)
                }
            }

            PropNames.App.TTS_ENGINE -> {
                mTTSEngine = value
                if (mTTSIsActive) {
                    // Apply TTS engine if changed
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.stop(null)
                                ttsbinder.initTTS(mTTSEngine, object : OnTTSCreatedListener {
                                    override fun onCreated() {
                                        log.info("TTS engine changed to $mTTSEngine")
                                    }

                                    override fun onFailed() {
                                        log.error("Failed to change TTS engine $mTTSEngine")
                                        stopTTSAndClosePanel()
                                    }

                                    override fun onTimedOut() {
                                        log.error("Failed to change TTS engine $mTTSEngine: timeout")
                                        stopTTSAndClosePanel()
                                    }
                                })
                            }
                        })
                }
            }

            PropNames.App.TTS_USE_DOC_LANG -> mTTSUseDocLang = Utils.parseBool(value)
            PropNames.App.TTS_FORCE_LANGUAGE -> mTTSForceLang = value
            PropNames.App.TTS_VOICE -> mTTSVoice = value
            PropNames.App.TTS_GOOGLE_END_OF_SENTENCE_ABBR -> {
                mTTSUseEndOfSentenceWorkaround = Utils.parseBool(value)
                if (mTTSIsActive) {
                    mTTSControlServiceAccessor?.runWithService(object :
                        TTSControlServiceAccessor.Callback {
                        override fun run(ttsbinder: TTSControlBinder) {
                            ttsbinder.setUseEndOfSentenceWorkaround(mTTSUseEndOfSentenceWorkaround)
                        }
                    })
                }
            }

            PropNames.App.TTS_USE_DICTIONARY -> {
                mTTSUseDictionary = Utils.parseBool(value)
                if (mTTSIsActive) {
                    mTTSControlServiceAccessor?.runWithService(object :
                        TTSControlServiceAccessor.Callback {
                        override fun run(ttsbinder: TTSControlBinder) {
                            ttsbinder.setUseDictionary(mTTSUseDictionary)
                        }
                    })
                }
            }

            PropNames.App.TTS_SPEED -> {
                mTTSSpeechRate = Utils.parseFloat(value, 1.0f, 0.3f, 3.5f)
                if (mTTSIsActive) {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.setSpeechRate(mTTSSpeechRate, null)
                            }
                        })
                }
            }

            PropNames.App.TTS_USE_IMMOBILITY_TIMEOUT -> {
                mTTSUseImmobilityTimeout = Utils.parseBool(value, false)
                if (mTTSIsActive) {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.setUseImmobilityTimeout(
                                    mTTSUseImmobilityTimeout,
                                    mTTSImmobilityTimeoutValue * 1000L
                                )
                            }
                        })
                }
            }

            PropNames.App.TTS_IMMOBILITY_TIMEOUT_VALUE -> {
                mTTSImmobilityTimeoutValue = Utils.parseInt(value, 180, 60, 3600)
                if (mTTSIsActive) {
                    mTTSControlServiceAccessor?.runWithService(
                        object : TTSControlServiceAccessor.Callback {
                            override fun run(ttsbinder: TTSControlBinder) {
                                ttsbinder.setUseImmobilityTimeout(
                                    mTTSUseImmobilityTimeout,
                                    mTTSImmobilityTimeoutValue * 1000L
                                )
                            }
                        })
                }
            }

            // TODO: implement other property changes
        }
    }

    fun loadDocument(
        fileName: String,
        doneRunnable: Runnable? = null,
        errorRunnable: Runnable? = null
    ): Boolean {
        BackgroundThread.ensureGUI()
        log.info("loadDocument($fileName)")
        return loadDocument(FileInfo(fileName), doneRunnable, errorRunnable)
    }

    fun loadDocument(
        stream: InputStream,
        streamName: String,
        doneRunnable: Runnable? = null,
        errorRunnable: Runnable? = null
    ): Boolean {
        BackgroundThread.ensureGUI()
        log.info("loadDocument($streamName)")
        return loadDocument(stream, FileInfo(streamName), doneRunnable, errorRunnable)
    }

    fun loadDocument(
        fileInfo: FileInfo,
        doneRunnable: Runnable?,
        errorRunnable: Runnable?
    ): Boolean {
        log.verbose("loadDocument(file: ${fileInfo.pathNameWA})")
        saveCurrentBookInfo()
        mBookInfo?.let {
            if (mBookOpened && it.fileInfo.pathNameWA == fileInfo.pathNameWA) {
                log.debug("trying to load already opened document, skipping")
                doneRunnable?.run()
                asyncDrawPage()
                return false
            }
        }
        dbServiceAcc?.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.loadBookInfo(fileInfo, object : DBService.BookInfoLoadingCallback {
                    override fun onBooksInfoLoaded(bookInfo: BookInfo?) {
                        var bi: BookInfo? = null
                        if (bookInfo?.isBookFileExists == true) {
                            // file found in DB & exists
                            bi = BookInfo(bookInfo)
                        } else {
                            // file omitted in DB or not exists
                            bi = BookInfo(fileInfo)
                            // Set maximum supported DOM version & block rendering flags
                            bi.domVersion = CREngineNGBinding.DOM_VERSION_CURRENT
                            bi.blockRenderingFlags = CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
                        }
                        BackgroundThread.postEngineTask(
                            LoadDocumentTask(
                                bi,
                                null,
                                doneRunnable,
                                errorRunnable
                            )
                        )
                    }
                })
            }
        })
        return true
    }

    fun loadDocument(
        stream: InputStream,
        fileInfo: FileInfo,
        doneRunnable: Runnable? = null,
        errorRunnable: Runnable? = null
    ): Boolean {
        log.verbose("loadDocument(stream: ${fileInfo.pathNameWA})")
        saveCurrentBookInfo()
        // When the document is opened from the stream at this moment,
        // we do not know the real path to the file, since it will be
        // changed after the successful opening of the document,
        // so here we cannot compare the path to the document currently
        // open with the fileInfo argument.
        // TODO: calculate fingerprint for this memory stream to
        //  find existing file in DB
        val bi = BookInfo(fileInfo)
        // Set maximum supported DOM version & block rendering flags
        bi.domVersion = CREngineNGBinding.DOM_VERSION_CURRENT
        bi.blockRenderingFlags = CREngineNGBinding.BLOCK_RENDERING_FLAGS_WEB
        BackgroundThread.postEngineTask(LoadDocumentTask(bi, stream, doneRunnable, errorRunnable))
        return true
    }

    fun setupReaderToolbar(toolbarView: View) {
        // TODO: setup callbacks for other buttons
        val backButton = toolbarView.findViewById<Button>(R.id.toolBack)
        val contentButton = toolbarView.findViewById<Button>(R.id.toolContents)
        val chapterView = toolbarView.findViewById<TextView>(R.id.toolChapter)
        val styleButton = toolbarView.findViewById<Button>(R.id.toolStyle)

        backButton?.setOnClickListener { onBackPressed() }
        contentButton?.setOnClickListener {
            processCommand(
                ReaderCommand.DCMD_SHOW_TOC,
                0,
                null
            )
        }
        chapterView?.setOnClickListener {
            processCommand(
                ReaderCommand.DCMD_SHOW_TOC,
                0,
                null
            )
        }
        styleButton?.setOnClickListener { showStylePanel() }
        mCurrentChapter?.let { chapterView?.text = it }
        mChapterViews.add(chapterView)
    }

    /**
     * Save current open book to database
     */
    private fun saveCurrentBookInfo() {
        if (mBookOpened) {
            updateBookReadingStats()
            mBookInfo?.let {
                log.verbose("saving last immediately")
                it.updateAccess()
                it.updateReadingTime(bookReadingDuration)
                dbServiceAcc?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.saveBookInfo(it)
                    }
                })
            }
        }
    }

    fun close() {
        BackgroundThread.ensureGUI()
        log.info("ReaderView.close() is called")
        if (!mBookOpened)
            return
        closeImageViewer()
        cancelUpdateCacheTask()
        saveCurrentBookInfo()
        BackgroundThread.postEngineTask(object : EngineTask() {
            override fun work() {
                BackgroundThread.ensureBackground()
                if (mBookOpened) {
                    mBookOpened = false
                    log.info("ReaderView().close() : closing current document")
                    mDoc.processCommand(ReaderCommand.DCMD_CLOSE_BOOK.nativeId, 0)
                }
            }

            override fun done() {
                BackgroundThread.ensureGUI()
                mAnimationThread.stopAnimation()
                // TODO: clear pages image cache (in background thread?)
                //factory.compact()
            }

            override fun fail(e: Exception) {
                log.error("close(): $e")
            }
        })
    }

    private fun animatePageTurn(dir: Int, onCompleteCallback: (() -> Unit)?) {
        BackgroundThread.ensureGUI()
        if (!mBookOpened)
            return
        log.debug("animatePageTurn(): dir=$dir")
        if (isAnimationUsed) {
            log.debug("other animation already in progress!")
            return
        }
        BackgroundThread.postBackground {
            // check/prepare current page image
            val page1 = preparePageImage(0)
            // check cached page1
            if (page1?.bitmap == null || null == page1.position) {
                log.error("page1 is null or incomplete!")
                return@postBackground
            }
            val isPageMode = 0 != page1.position!!.pageMode
            // check/prepare next page image
            var offset: Int
            val finalOffset: Int
            val scrollLimit: Int
            if (isPageMode) {
                // page mode
                offset = if (dir > 0) 1 else -1
                finalOffset = offset
                scrollLimit = 0 // ignored
            } else {
                // scroll mode
                offset = mRenderHeight
                if (dir < 0) {
                    offset = -offset
                    finalOffset = offset + (mPageInsets.top + mPageInsets.bottom)
                    scrollLimit = -finalOffset
                } else {
                    finalOffset = offset - (mPageInsets.top + mPageInsets.bottom)
                    scrollLimit = finalOffset
                }
            }
            val page2 = preparePageImage(offset)
            // check cached page2
            if (page2?.bitmap == null || null == page2.position) {
                log.error("page2 is null or incomplete!")
                return@postBackground
            }
            if (!page1.isCompatible(page2)) {
                log.error("page1 & page2 not compatible!")
                return@postBackground
            }
            if (page1 == page2 || page2.position == page1.position) {
                log.error("page1 & page2 is equal!")
                return@postBackground
            }
            synchronized(mAnimationLocker) {
                val speed: Float
                if (isPageMode) {
                    mCurrentAnimation = when (mPageAnimationType) {
                        PageAnimationType.Paper -> NaturalPageFlipAnimation(
                            dir,
                            page1,
                            page2,
                            false
                        )

                        PageAnimationType.Slide -> PageFlipAnimation(dir, page1, page2)
                        else -> PageFlipAnimation(dir, page1, page2)
                    }
                    speed =
                        (mAnimAvgFrameTime * page1.position!!.pageWidth).toFloat() / DEF_ANIMATION_DURATION.toFloat()
                } else {
                    mCurrentAnimation = ScrollAnimation(dir, page1, page2, scrollLimit)
                    speed =
                        (mAnimAvgFrameTime * page1.position!!.pageHeight).toFloat() / DEF_ANIMATION_DURATION.toFloat()
                }
                //log.debug("speed=$speed")
                mAnimationThread.startAnimation(mCurrentAnimation!!, true, speed, 0f) {
                    val cmd: ReaderCommand
                    synchronized(mAnimationLocker) {
                        mCurrentAnimation = null
                    }
                    if (isPageMode) {
                        cmd =
                            if (dir > 0) ReaderCommand.DCMD_PAGEDOWN else ReaderCommand.DCMD_PAGEUP
                        processEngineCommand(cmd, 1, true, onCompleteCallback)
                    } else {
                        cmd = ReaderCommand.DCMD_SCROLL_BY
                        processEngineCommand(cmd, finalOffset, true, onCompleteCallback)
                    }
                }
            }
        }
    }

    private fun getCSSContentsForFormat(fileFormat: DocumentFormat): String? {
        // TODO: load css from application data directory
        // TODO: implement '@import' support in css
        try {
            val stream = resources.openRawResource(fileFormat.cssResourceId)
            val reader = InputStreamReader(stream, "UTF8")
            val stringBuilder = StringBuilder()
            val buff = CharArray(4096)
            var res: Int
            while (reader.read(buff, 0, 4096).also { res = it } != -1) {
                stringBuilder.appendRange(buff, 0, res)
            }
            return stringBuilder.toString()
        } catch (e: NotFoundException) {
            log.error(e.toString())
        } catch (e: UnsupportedEncodingException) {
            log.error(e.toString())
        } catch (e: IOException) {
            log.error(e.toString())
        }
        return null
    }

    private fun requestResize(width: Int, height: Int) {
        mLastResizeTaskId++
        val thisId = mLastResizeTaskId
        mRequestedWidth = width
        mRequestedHeight = height
        if (mRequestedWidth < 100)
            mRequestedWidth = 100
        if (mRequestedHeight < 100)
            mRequestedHeight = 100
        val isChanged = mRequestedWidth != mRenderWidth || mRequestedHeight != mRenderHeight
        if (isChanged) {
            BackgroundThread.postEngineTask(object : EngineTask() {
                @Throws(Exception::class)
                override fun work() {
                    if (thisId != mLastResizeTaskId) {
                        log.debug("skipping duplicate resize request in GUI thread")
                        return
                    }
                    mDoc.resize(mRequestedWidth, mRequestedHeight)
                    mRenderWidth = mRequestedWidth
                    mRenderHeight = mRequestedHeight
                    invalidateImageCache()
                }

                override fun done() {
                    invalidate()
                    asyncDrawPage {
                        // Additional redraw required on hardware accelerated canvas
                        drawOnSurface()
                    }
                }

                override fun fail(e: Exception) {
                    log.error(e.toString())
                }
            })
        } else {
            redraw()
        }
    }

    fun invalidateImageCache() {
        mInvalidateImages = true
    }

    /**
     * Prepare and cache page image.
     * Cache is represented by two slots: mCurrentPageInfo and mNextPageInfo.
     * If page already exists in cache, returns it (if current page requested,
     * ensures that it became stored as mCurrentPageInfo; if another page requested,
     * no mCurrentPageInfo/mNextPageInfo reordering made).
     *
     * @param offset is kind of page: 0==current, -1=previous, 1=next page
     * @return page image and properties, null if requested page is unavailable (e.g. requested next/prev page is out of document range)
     */
    private fun preparePageImage(offset: Int): PageImageCache? {
        var off: Int = offset
        BackgroundThread.ensureBackground()
        log.verbose("preparePageImage(): offset=$offset (${mRenderWidth}x${mRenderHeight})")
        if (isAnimationUsed) {
            log.error("preparePageImage(): Trying to update the page image cache while it is in the animation object!")
            log.error("preparePageImage():  This is prohibited, as it will lead to an emergency stop of the program. Skipping.")
            //throw RuntimeException("Trying to update the page image cache while it is in the animation object! This is prohibited, as it will lead to an emergency stop of the program.")
            return null
        }
        synchronized(mPageImageLocker) {
            if (mInvalidateImages) {
                mCurrentPageCache?.recycle()
                mCurrentPageCache = null
                mNextPageCache?.recycle()
                mNextPageCache = null
                mInvalidateImages = false
            }
            if (mRenderWidth == 0 || mRenderHeight == 0) {
                if (mRequestedWidth > 0 && mRequestedHeight > 0) {
                    mRenderWidth = mRequestedWidth
                    mRenderHeight = mRequestedHeight
                } else {
                    mRenderWidth = this@ReaderView.width
                    mRenderHeight = this@ReaderView.height
                }
                mDoc.resize(mRenderWidth, mRenderHeight)
            }

            val currPos = mDoc.getPositionProps(null) ?: return null
            mCurrentPos = currPos
            val isPageView = currPos.pageMode != 0
            var currPosPageInCache: PageImageCache? = null
            // Here currPos can't be null
            if (mCurrentPageCache?.position == currPos)
                currPosPageInCache = mCurrentPageCache
            else if (mNextPageCache?.position == currPos)
                currPosPageInCache = mNextPageCache
            if (off == 0) {
                // Current page requested
                if (null != currPosPageInCache) {
                    if (mNextPageCache == currPosPageInCache) {
                        // reorder pages
                        val tmp = mNextPageCache
                        mNextPageCache = mCurrentPageCache
                        mCurrentPageCache = tmp
                    }
                    // prepared page image found
                    return currPosPageInCache
                }
                mCurrentPageCache?.recycle()
                mCurrentPageCache = null
                val pageImageCache = PageImageCache()
                pageImageCache.position = currPos
                //pageImageCache.bitmap = factory.get(m_renderWidth, m_renderHeight)
                pageImageCache.bitmap =
                    Bitmap.createBitmap(mRenderWidth, mRenderHeight, Bitmap.Config.ARGB_8888)
                mDoc.setBatteryState(batteryState, batteryChargingConnection, batteryChargeLevel)
                mDoc.renderPageBitmap(pageImageCache.bitmap!!, mRenderBpp)
                mCurrentPageCache = pageImageCache
                //log.verbose("Prepared new current page image $mCurrentPageCache")
                return mCurrentPageCache
            }
            if (isPageView) {
                // PAGES: one of next or prev pages requested, offset is specified as param
                val cmd =
                    if (off > 0) ReaderCommand.DCMD_PAGEDOWN.nativeId else ReaderCommand.DCMD_PAGEUP.nativeId
                val cmd_post =
                    if (off > 0) ReaderCommand.DCMD_PAGEUP.nativeId else ReaderCommand.DCMD_PAGEDOWN.nativeId
                if (off < 0)
                    off = -off
                if (mDoc.processCommand(cmd, off)) {
                    // can move to next page
                    val nextPos = mDoc.getPositionProps(null)
                    var nextPosPageInCache: PageImageCache? = null
                    if (null != nextPos) {
                        if (mCurrentPageCache?.position == nextPos)
                            nextPosPageInCache = mCurrentPageCache
                        else if (mNextPageCache?.position == nextPos)
                            nextPosPageInCache = mNextPageCache
                    }
                    if (nextPosPageInCache == null) {
                        // existing image not found in cache, overriding mNextPageInfo
                        mNextPageCache?.recycle()
                        mNextPageCache = null
                        val pic = PageImageCache()
                        pic.position = nextPos
                        //pic.bitmap = factory.get(m_renderWidth, m_renderHeight);
                        pic.bitmap = Bitmap.createBitmap(
                            mRenderWidth,
                            mRenderHeight,
                            Bitmap.Config.ARGB_8888
                        )
                        mDoc.setBatteryState(
                            batteryState,
                            batteryChargingConnection,
                            batteryChargeLevel
                        )
                        mDoc.renderPageBitmap(pic.bitmap!!, mRenderBpp)
                        mNextPageCache = pic
                        nextPosPageInCache = pic
                        //log.v("Prepared new current page image $mNextPageCache")
                    }
                    // return back to previous page
                    mDoc.processCommand(cmd_post, off)
                    return nextPosPageInCache
                }
            } else {
                // SCROLL: next or prev page requested, with pixel offset specified
                val y = currPos.y + off
                if (mDoc.processCommand(ReaderCommand.DCMD_GO_POS.nativeId, y)) {
                    val nextPos = mDoc.getPositionProps(null)
                    var nextPosPageInCache: PageImageCache? = null
                    if (null != nextPos) {
                        if (mCurrentPageCache?.position == nextPos)
                            nextPosPageInCache = mCurrentPageCache
                        else if (mNextPageCache?.position == nextPos)
                            nextPosPageInCache = mNextPageCache
                    }
                    if (nextPosPageInCache == null) {
                        // existing image not found in cache, overriding mNextPageInfo
                        mNextPageCache?.recycle()
                        mNextPageCache = null
                        val pic = PageImageCache()
                        pic.position = nextPos
                        //pic.bitmap = factory.get(m_renderWidth, m_renderHeight)
                        pic.bitmap = Bitmap.createBitmap(
                            mRenderWidth,
                            mRenderHeight,
                            Bitmap.Config.ARGB_8888
                        )
                        mDoc.setBatteryState(
                            batteryState,
                            batteryChargingConnection,
                            batteryChargeLevel
                        )
                        mDoc.renderPageBitmap(pic.bitmap!!, mRenderBpp)
                        mNextPageCache = pic
                        nextPosPageInCache = pic
                    }
                    // return back to prev position
                    mDoc.processCommand(ReaderCommand.DCMD_GO_POS.nativeId, currPos.y)
                    return nextPosPageInCache
                }
            }
        }
        // cannot move to page: out of document range
        return null
    }

    /**
     * Returns a pre-rendered page image.
     * Cache is represented by two slots: mCurrentPageInfo and mNextPageInfo.
     * If page already exists in cache, returns it (if current page requested,
     * ensures that it became stored as mCurrentPageInfo; if another page requested,
     * no mCurrentPageInfo/mNextPageInfo reordering made).
     *
     * @param offset is kind of page: 0==current, -1=previous, 1=next page
     * @return page image and properties, null if requested image page cache isn't available
     */
    private fun getPageImage(offset: Int): PageImageCache? {
        log.verbose("getPageImage(): offset=$offset (${mRenderWidth}x${mRenderHeight})")
        synchronized(mPageImageLocker) {
            if (mInvalidateImages)
                return null
            val currpos = mCurrentPos ?: return null
            var currposPageImage: PageImageCache? = null
            // Here currpos can't be null
            if (mCurrentPageCache?.position != null && mCurrentPageCache?.position == currpos)
                currposPageImage = mCurrentPageCache
            else if (mNextPageCache?.position != null && mNextPageCache?.position == currpos)
                currposPageImage = mNextPageCache
            if (offset == 0) {
                // Current page requested
                currposPageImage?.let {
                    if (mNextPageCache == currposPageImage) {
                        // reorder pages
                        val tmp: PageImageCache? = mNextPageCache
                        mNextPageCache = mCurrentPageCache
                        mCurrentPageCache = tmp
                    }
                    // found ready page image
                    return mCurrentPageCache
                }
            } else if (offset < 0) {
                mNextPageCache?.let { image ->
                    image.position?.let { pos ->
                        if (pos.percent < currpos.percent && pos.isCompatible(currpos))
                            return image
                    }
                }
                mCurrentPageCache?.let { image ->
                    image.position?.let { pos ->
                        if (pos.percent < currpos.percent && pos.isCompatible(currpos))
                            return image
                    }
                }
            } else {
                mNextPageCache?.let { image ->
                    image.position?.let { pos ->
                        if (pos.percent > currpos.percent && pos.isCompatible(currpos))
                            return image
                    }
                }
                mCurrentPageCache?.let { image ->
                    image.position?.let { pos ->
                        if (pos.percent > currpos.percent && pos.isCompatible(currpos))
                            return image
                    }
                }
            }
        }
        // No suitable prepared images
        return null
    }

    private fun isInPageViewMode(): Boolean {
        var result = true
        synchronized(mPageImageLocker) {
            mCurrentPageCache?.position?.let {
                result = it.pageMode != 0
            } ?: run {
                mNextPageCache?.position?.let {
                    result = it.pageMode != 0
                }
            }
        }
        return result
    }

    internal fun asyncDrawPage(doneCallback: (() -> Unit)? = null) {
        if (!mInitialized)
            return
        log.verbose("asyncDrawPage(): submitting DrawPageTask")
        if (isAnimationUsed) {
            log.error("asyncDrawPage(): can't draw page while animation is used!")
            doneCallback?.let {
                BackgroundThread.postBackground {
                    it.invoke()
                }
            }
            return
        }
        if (mBookOpened)
            scheduleSaveCurrentPositionBookmark(DEF_SAVE_POSITION_INTERVAL)
        if (null != mImageViewer)
            BackgroundThread.postEngineTask(object : EngineTask() {
                override fun work() {
                    drawOnSurfaceUsingCallback { c -> mImageViewer?.draw(c) }
                }

                override fun done() {
                    doneCallback?.invoke()
                }

                override fun fail(e: Exception) {
                    log.error("image draw failed!: $e")
                    hideProgress()
                }
            })
        else
            BackgroundThread.postEngineTask(DrawPageTask(doneCallback))
    }

    private fun drawOnSurface() {
        drawOnSurfaceUsingCallback { c -> drawPage_impl(c) }
    }

    private fun drawPage_impl(canvas: Canvas) {
        try {
            log.debug("drawPage_impl() called")
            if (!mInitialized) {
                log.debug("not initialized yet!")
                return
            }
            synchronized(mPageImageLocker) {
                if (null != mCurrentPageCache && false == mCurrentPageCache?.isReleased && null != mCurrentPageCache?.bitmap) {
                    val bitmap = mCurrentPageCache!!.bitmap!!
                    log.debug("drawPage_impl() -- drawing page image, canvas size=${canvas.width}x${canvas.height}")
                    // TODO: autoscroll related implementation
                    /*
                    if (currentAutoScrollAnimation != null) {
                        currentAutoScrollAnimation.draw(canvas)
                    } else {
                     */
                    val dst = Rect(0, 0, canvas.width, canvas.height)
                    val src = Rect(0, 0, bitmap.width, bitmap.height)
                    if (DONT_STRETCH_WHILE_DRAWING) {
                        if (dst.right > src.right) dst.right = src.right
                        if (dst.bottom > src.bottom) dst.bottom = src.bottom
                        if (src.right > dst.right) src.right = dst.right
                        if (src.bottom > dst.bottom) src.bottom = dst.bottom
                        if (CENTER_PAGE_INSTEAD_OF_RESIZING) {
                            val ddx = (canvas.width - dst.width()) / 2
                            val ddy = (canvas.height - dst.height()) / 2
                            dst.left += ddx
                            dst.right += ddx
                            dst.top += ddy
                            dst.bottom += ddy
                        }
                    }
                    if (dst.width() != canvas.width || dst.height() != canvas.height) {
                        if (null != mCurrentBackgroundInfo) {
                            // TODO: draw background
                        } else {
                            canvas.drawColor(mCurrentBackgroundColor)
                        }
                    }
                    canvas.drawBitmap(bitmap, src, dst, null)
                } else {
                    log.debug("drawPage_impl() -- drawing empty screen")
                    // TODO: implement & uncomment:
                    //drawPageBackground(canvas);
                }
                mCurrentSelection?.let {
                    if (it.isNotEmpty) {
                        mTextSelectHandleLeftDrawable?.draw(canvas)
                        mTextSelectHandleRightDrawable?.draw(canvas)
                    }
                }
            }
        } catch (e: Exception) {
            log.error("drawPage_impl(): exception while drawing", e)
        }
    }

    private fun drawOnSurfaceUsingCallback(callback: (Canvas) -> Unit) {
        if (!mSurfaceCreated)
            return
        //log.verbose("drawOnSurfaceUsingCallback() - in thread ${Thread.currentThread().name}")
        val holder = holder
        if (holder != null) {
            var canvas: Canvas? = null
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                try {
                    canvas = holder.lockHardwareCanvas()
                } catch (e: Exception) {
                    log.error("drawOnSurfaceUsingCallback() -> lockHardwareCanvas() failed: $e")
                }
            }
            try {
                if (canvas == null)
                    canvas = holder.lockCanvas(null)
                //log.verbose("before callback(canvas) $canvas");
                canvas?.let {
                    // TODO: uncomment next lines:
                    /*
                    if (DeviceInfo.EINK_SCREEN) {
                        // pre draw update
                        //BackgroundThread.instance().executeGUI(() -> EinkScreen.PrepareController(surface, isPartially));
                        mEinkScreen.prepareController(surface, isPartially);
                    }
                     */
                    callback(it)
                }
            } finally {
                //log.verbose("exiting finally");
                canvas?.let {
                    //log.verbose("before unlockCanvasAndPost");
                    holder.unlockCanvasAndPost(it)
                    // TODO: uncomment next lines:
                    /*
                    if (DeviceInfo.EINK_SCREEN) {
                        // post draw update
                        mEinkScreen.updateController(surface, isPartially);
                    }
                     */
                    //log.verbose("after unlockCanvasAndPost");
                }
            }
        }
        //log.verbose("exiting drawOnSurfaceUsingCallback()");
    }

    fun setBatteryState(state: BatteryState, chargingConn: BatteryCharger, level: Int) {
        var needUpdate = false
        if (state != batteryState) {
            log.info("Battery state changed: $state")
            batteryState = state
            needUpdate = true
        }
        if (chargingConn != batteryChargingConnection) {
            log.info("Battery charging connection changed: $chargingConn")
            batteryChargingConnection = chargingConn
            needUpdate = true
        }
        if (level != batteryChargeLevel) {
            log.info("Battery charging level changed: $level")
            batteryChargeLevel = level
            needUpdate = true
        }
        // TODO: Don't update on e-ink devices or autoscroll active
        if (mShowBattery && needUpdate)
            redraw()
    }

    fun onTimeTickReceived() {
        if (mShowTime) {
            // TODO: Don't update on e-ink devices or autoscroll active
            BackgroundThread.postBackground {
                if (mDoc.isTimeChanged()) {
                    log.info("The current time has been changed (minutes), redrawing is scheduled.")
                    redraw()
                }
            }
        }
    }

    /**
     * Called on the ReaderActivity to handle the navigation bar's back press events.
     * See https://developer.android.com/reference/android/app/Activity#onBackPressed()
     * Also called when the back button on the toolbar is clicked.
     * @return true if back event processed, false otherwise.
     *
     * When this function return false, client (ReaderActivity) call super method, i.e. finishes the current activity.
     */
    fun onBackPressed(): Boolean {
        if (null != mImageViewer) {
            closeImageViewer()
            return true
        }
        if (mDocContentPopup.isShowing) {
            mDocContentPopup.hide()
            return true
        }
        if (mGoPanelPopup.isShowing) {
            mGoPanelPopup.hide()
            return true
        }
        if (mBookmarksPopup.isShowing) {
            if (mBookmarksPopup.isSelectionMode) {
                mBookmarksPopup.isSelectionMode = false
            } else {
                mBookmarksPopup.hide()
            }
            return true
        }
        if (mStylePanelPopup.isShowing) {
            mStylePanelPopup.hide()
            return true
        }
        if (mReaderMenuPopup.isShowing) {
            mReaderMenuPopup.hide()
            return true
        }
        if (mTTSPanelPopup.isShowing) {
            mTTSPanelPopup.hide()
            stopTTSAndClosePanel()
            return true
        }
        // TODO: close/hide other popup windows (style dialog, etc.)
        if (mInStartSelectionMode || null != mCurrentSelection || mSelectionPanelPopup.isShowing) {
            clearSelection()
            return true
        }
        if (mCanGoBack) {
            navigateByHistory(ReaderCommand.DCMD_LINK_BACK)
            return true
        }
        return false
    }

    fun onUiThemeOrLanguageChanged() {
        // Recreate all popups to use the new theme on them
        updatePopupsForTheme()
        updateTextSelectHandleAssets()
        val symbols = DecimalFormatSymbols(SettingsManager.activeLang.locale)
        BackgroundThread.postBackground {
            mDoc.setDecimalPointChar(symbols.decimalSeparator)
        }
    }

    fun selectSearchResult(searchResultItem: SearchResultItem) {
        BackgroundThread.postEngineTask(object : EngineTask() {
            override fun work() {
                mDoc.selectSearchResult(searchResultItem)
            }

            override fun done() {
                invalidate()
                asyncDrawPage()
            }

            override fun fail(e: Exception) {
                log.error(e.toString())
            }
        })
    }

    /**
     * Gets information about the open book and stores it in mBookInfo.
     * @param updatePath also update book path components
     */
    private fun updateBookInfo(updatePath: Boolean) {
        BackgroundThread.ensureBackground()
        val bookInfo: BookInfo = mBookInfo ?: return
        mDoc.updateBookInfo(bookInfo, updatePath, genresCollection)
    }

    private fun scheduleUpdateCacheTask() {
        log.verbose("scheduleUpdateCacheTask() : submitting UpdateCacheTask")
        synchronized(mUpdateCacheLocker) {
            mCurrentUpdateCacheTask = UpdateCacheTask()
            BackgroundThread.postGUI({
                synchronized(mUpdateCacheLocker) {
                    mCurrentUpdateCacheTask?.let { BackgroundThread.postEngineTask(it) }
                }
            }, 2000)
        }
    }

    private fun cancelUpdateCacheTask() {
        synchronized(mUpdateCacheLocker) {
            mCurrentUpdateCacheTask?.cancel()
            mCurrentUpdateCacheTask = null
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        log.debug("onSizeChanged(): w=$w, h=$h")
        super.onSizeChanged(w, h, oldw, oldh)
        if (mResizeAllowed) {
            BackgroundThread.postGUI({
                if (mResizeAllowed) {
                    requestResize(w, h)
                } else {
                    mLastRequestedWidth = w
                    mLastRequestedHeight = h
                }
            }, 500)
        } else {
            mLastRequestedWidth = w
            mLastRequestedHeight = h
        }
    }

    override fun onWindowVisibilityChanged(visibility: Int) {
        if (visibility == VISIBLE) {
            // TODO: e-ink: refresh screen
            // TODO: investigate if we need to resize here...
            startBookReadingStats()
            BackgroundThread.postGUI({ enableResizeEvents(true) }, 500)
        } else {
            stopBookReadingStats()
            enableResizeEvents(false)
        }
        super.onWindowVisibilityChanged(visibility)
    }

    override fun onWindowFocusChanged(hasWindowFocus: Boolean) {
        if (hasWindowFocus) {
            // TODO: e-ink: refresh screen
            // TODO: investigate if we need to resize here...
            startBookReadingStats()
            BackgroundThread.postGUI({ enableResizeEvents(true) }, 500)
            requestFocus()
        } else {
            stopBookReadingStats()
            enableResizeEvents(false)
        }
        super.onWindowFocusChanged(hasWindowFocus)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            synchronized(mSystemGestureExclusionRects) {
                ViewCompat.setSystemGestureExclusionRects(this, mSystemGestureExclusionRects)
            }
        }
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onDraw(canvas: Canvas) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            synchronized(mSystemGestureExclusionRects) {
                ViewCompat.setSystemGestureExclusionRects(this, mSystemGestureExclusionRects)
            }
        }
        asyncDrawPage()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        activityControl?.onUserActivity()
        mImageViewer?.let { return it.onTouchEvent(event) }
        return mGestureDetector.onTouchEvent(event)
    }

    private fun isVolumeKey(keyCode: Int): Boolean {
        return when (keyCode) {
            KeyEvent.KEYCODE_VOLUME_UP,
            KeyEvent.KEYCODE_VOLUME_DOWN -> true

            else -> false
        }
    }

    @SuppressLint("GestureBackNavigation")
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (null == event)
            return false
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            // We should not intercept KeyEvent.KEYCODE_BACK events
            // Just skip this, it processed by activity's onBackPressedDispatcher
            return super.onKeyUp(keyCode, event)
        }
        activityControl?.onUserActivity()
        mImageViewer?.let { return it.onKeyDown(keyCode, event) }
        if (haveAnyOpenPopups)
            return false
        if (isVolumeKey(keyCode) && !mAllowVolumeKeys)
            return false
        val action = mKeySinglePressActions[keyCode]
        if (null != action) {
            if (mHandleLongKeyPress)
                event.startTracking()
            else if (event.repeatCount > 0)
                processReaderAction(action)
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    @SuppressLint("GestureBackNavigation")
    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (null == event)
            return false
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            // We should not intercept KeyEvent.KEYCODE_BACK events
            // Just skip this, it processed by activity's onBackPressedDispatcher
            return super.onKeyUp(keyCode, event)
        }
        mImageViewer?.let { return it.onKeyUp(keyCode, event) }
        if (haveAnyOpenPopups)
            return false
        if (isVolumeKey(keyCode) && !mAllowVolumeKeys)
            return false
        if (mHandleLongKeyPress) {
            if ((event.isTracking && !event.isCanceled)) {
                val action = mKeySinglePressActions[keyCode]
                if (null != action) {
                    processReaderAction(action)
                    return true
                }
            }
        } else {
            if (0 == event.repeatCount) {
                val action = mKeySinglePressActions[keyCode]
                if (null != action) {
                    processReaderAction(action)
                    return true
                }
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onKeyLongPress(keyCode: Int, event: KeyEvent?): Boolean {
        if (null == event)
            return false
        activityControl?.onUserActivity()
        mImageViewer?.let { return false }
        if (haveAnyOpenPopups)
            return false
        if (isVolumeKey(keyCode) && !mAllowVolumeKeys)
            return false
        val action = mKeyLongPressActions[keyCode]
        if (null != action) {
            processReaderAction(action)
            return true
        }
        return false
    }

    private fun getActionNoByCoord(x: Int, y: Int): Int {
        var pos: Int = -1
        val ww = width - 2 * mHorizDeadIndent
        val hh = height - 2 * mVertDeadIndent
        val xx = x - mHorizDeadIndent
        val yy = y - mVertDeadIndent
        if (xx in 0 until ww && yy in 0 until hh) {
            pos = if (xx >= 0 && xx < ww / 3)
                0
            else if (xx >= ww / 3 && xx < 2 * ww / 3)
                1
            else
                2
            pos += if (yy >= 0 && yy < hh / 3)
                0
            else if (yy >= hh / 3 && yy < 2 * hh / 3)
                3
            else
                6
        }
        return pos
    }

    private fun getRegionAction(
        type: SettingsManager.PressOrTapType,
        regionNo: Int
    ): ReaderAction? {
        var action: ReaderAction? = null
        if (regionNo in 0..8) {
            action = when (type) {
                SettingsManager.PressOrTapType.NORMAL -> mSingleTapActions[regionNo]
                SettingsManager.PressOrTapType.LONG -> mLongTapActions[regionNo]
                SettingsManager.PressOrTapType.DOUBLE -> mDoubleTapActions[regionNo]
            }
        }
        return action
    }

    internal fun processReaderAction(action: ReaderAction, onComplete: (() -> Unit)? = null) {
        BackgroundThread.ensureGUI()
        if (!action.isNone)
            processCommand(action.cmd, action.param, onComplete)
    }

    private fun processCommand(cmd: ReaderCommand, param: Int, onComplete: (() -> Unit)?) {
        BackgroundThread.ensureGUI()
        log.info("process command " + cmd + if (param != 0) " ($param)" else " ")
        when (cmd) {
            ReaderCommand.DCMD_FILE_BROWSER_ROOT -> {}  // TODO: implement this
            ReaderCommand.DCMD_ABOUT -> activityControl?.openAboutDialog()
            ReaderCommand.DCMD_TOGGLE_AUTOSCROLL -> {}  // TODO: implement this
            ReaderCommand.DCMD_AUTOSCROLL_SPEED_INCREASE -> {}  // TODO: implement this
            ReaderCommand.DCMD_AUTOSCROLL_SPEED_DECREASE -> {}  // TODO: implement this
            ReaderCommand.DCMD_SHOW_DICTIONARY -> {}    // TODO: implement this
            ReaderCommand.DCMD_OPEN_PREVIOUS_BOOK -> {} // TODO: implement this
            ReaderCommand.DCMD_BOOK_INFO -> mBookInfo?.let {
                if (mBookOpened)
                    activityControl?.showBookInfo(it)
            }

            ReaderCommand.DCMD_USER_MANUAL -> {}    // TODO: implement this
            ReaderCommand.DCMD_TTS_PLAY -> startTTS()

            ReaderCommand.DCMD_TOGGLE_DOCUMENT_STYLES -> {
                if (mBookOpened)
                    toggleDocumentStyles()
            }

            ReaderCommand.DCMD_SHOW_HOME_SCREEN -> {}   // TODO: implement this
            ReaderCommand.DCMD_TOGGLE_ORIENTATION -> {} // TODO: implement this
            ReaderCommand.DCMD_TOGGLE_FULLSCREEN -> {
                var fs = activityControl?.isFullScreen()
                if (null != fs) {
                    fs = !fs
                    activityControl?.setFullscreen(fs)
                }
            }

            ReaderCommand.DCMD_TOGGLE_TITLEBAR -> {}    // TODO: implement this
            ReaderCommand.DCMD_SHOW_POSITION_INFO_POPUP -> {}   // TODO: implement this
            ReaderCommand.DCMD_TOGGLE_SELECTION_MODE -> {
                if (mBookOpened) {
                    val hasSelection = mCurrentSelection?.isNotEmpty == true
                    if (!hasSelection) {
                        // To avoid disrupting the sequence of actions when calling functions clearSelection() and showSelectionPanel(),
                        //  both functions must add actions to the same queue.
                        BackgroundThread.postEngineTask(object : EngineTask() {
                            override fun work() {}

                            override fun done() {
                                mCurrentSelection = Selection()
                                mInStartSelectionMode = true
                                showSelectionPanel(false)
                            }

                            override fun fail(e: Exception) {}
                        })
                    } else {
                        clearSelection()
                    }
                }
            }

            ReaderCommand.DCMD_START_SELECTION -> {
                if (mBookOpened) {
                    val hasSelection = mCurrentSelection?.isNotEmpty == true
                    if (!hasSelection) {
                        // To avoid disrupting the sequence of actions when calling functions clearSelection() and showSelectionPanel(),
                        //  both functions must add actions to the same queue.
                        BackgroundThread.postEngineTask(object : EngineTask() {
                            override fun work() {}

                            override fun done() {
                                mCurrentSelection = Selection()
                                mInStartSelectionMode = true
                                showSelectionPanel(false)
                            }

                            override fun fail(e: Exception) {}
                        })
                    }
                }
            }

            ReaderCommand.DCMD_TOGGLE_TOUCH_SCREEN_LOCK -> {}   // TODO: implement this
            ReaderCommand.DCMD_LINK_BACK,
            ReaderCommand.DCMD_LINK_FORWARD -> navigateByHistory(cmd)

            ReaderCommand.DCMD_ZOOM_OUT -> processEngineCommand(
                ReaderCommand.DCMD_ZOOM_OUT,
                param,
                true,
                onComplete
            )

            ReaderCommand.DCMD_ZOOM_IN -> processEngineCommand(
                ReaderCommand.DCMD_ZOOM_IN,
                param,
                true,
                onComplete
            )

            ReaderCommand.DCMD_PAGEDOWN -> {
                if (mBookOpened) {
                    if (isAnimationAllowed && param == 1)
                        animatePageTurn(1, onComplete)
                    else
                        processEngineCommand(cmd, param, true, onComplete)
                }
            }

            ReaderCommand.DCMD_PAGEUP -> {
                if (mBookOpened) {
                    if (isAnimationAllowed && param == 1)
                        animatePageTurn(-1, onComplete)
                    else
                        processEngineCommand(cmd, param, true, onComplete)
                }
            }

            ReaderCommand.DCMD_MOVE_BY_CHAPTER,
            ReaderCommand.DCMD_BEGIN,
            ReaderCommand.DCMD_END,
            ReaderCommand.DCMD_SCROLL_BY,
            ReaderCommand.DCMD_GO_POS -> {
                if (mBookOpened)
                    processEngineCommand(cmd, param, true, onComplete)
            }

            ReaderCommand.DCMD_GO_PAGE -> {
                if (mBookOpened)
                    processEngineCommand(cmd, param, true, onComplete)
            }

            ReaderCommand.DCMD_OPEN_FILE -> activityControl?.openFileSelector()
            ReaderCommand.DCMD_RECENT_BOOKS_LIST -> activityControl?.openLibrary(0)
            ReaderCommand.DCMD_OPEN_LIBRARY -> activityControl?.openLibrary(1)
            ReaderCommand.DCMD_SEARCH -> {
                mBookInfo?.let {
                    activityControl?.openSearchFragment(it)
                }
            }

            ReaderCommand.DCMD_EXIT -> activityControl?.exitReader()
            ReaderCommand.DCMD_BOOKMARKS -> showBookmarks()
            ReaderCommand.DCMD_NEW_BOOKMARK -> {
                if (mBookOpened) {
                    BackgroundThread.postBackground {
                        getCurrentPositionBookmark()?.let { bm ->
                            bm.type = Bookmark.TYPE_POSITION
                            mBookInfo?.let { bi ->
                                if (bi.addBookmark(bm))
                                    saveCurrentBookInfo()
                            }
                        }
                    }
                }
            }

            ReaderCommand.DCMD_SHOW_GO_PANEL -> showGoPanel()
            ReaderCommand.DCMD_SHOW_TOC -> showDocContents()
            ReaderCommand.DCMD_FILE_BROWSER -> {}   // TODO: implement this
            ReaderCommand.DCMD_CURRENT_BOOK_DIRECTORY -> {} // TODO: implement this
            ReaderCommand.DCMD_OPTIONS_STYLE -> showStylePanel()
            ReaderCommand.DCMD_OPTIONS_DIALOG -> activityControl?.openSettings()
            ReaderCommand.DCMD_READER_MENU -> showReaderMenuPanel()
            ReaderCommand.DCMD_TOGGLE_DAY_NIGHT_MODE -> {}  // TODO: implement this
            ReaderCommand.DCMD_BACKLIGHT_SET_DEFAULT -> {}  // TODO: implement this
            ReaderCommand.DCMD_SHOW_SYSTEM_BACKLIGHT_DIALOG -> {}   // TODO: implement this
            ReaderCommand.DCMD_GOOGLEDRIVE_SYNC -> {}   // TODO: implement this
            else -> {}
        }
    }

    private fun processEngineCommand(
        cmd: ReaderCommand,
        param: Int,
        invalidate: Boolean = true,
        doneCallback: (() -> Unit)? = null
    ) {
        log.debug("processEngineCommand($cmd, $param)")
        BackgroundThread.postEngineTask(object : EngineTask() {
            var res = false
            var isMoveCommand = false

            @Throws(Exception::class)
            override fun work() {
                res = mDoc.processCommand(cmd.nativeId, param)
                if (res) {
                    when (cmd) {
                        ReaderCommand.DCMD_BEGIN,
                        ReaderCommand.DCMD_LINEUP,
                        ReaderCommand.DCMD_PAGEUP,
                        ReaderCommand.DCMD_PAGEDOWN,
                        ReaderCommand.DCMD_LINEDOWN,
                        ReaderCommand.DCMD_LINK_FORWARD,
                        ReaderCommand.DCMD_LINK_BACK,
                        ReaderCommand.DCMD_LINK_NEXT,
                        ReaderCommand.DCMD_LINK_PREV,
                        ReaderCommand.DCMD_LINK_GO,
                        ReaderCommand.DCMD_END,
                        ReaderCommand.DCMD_GO_POS,
                        ReaderCommand.DCMD_GO_PAGE,
                        ReaderCommand.DCMD_MOVE_BY_CHAPTER,
                        ReaderCommand.DCMD_GO_SCROLL_POS,
                        ReaderCommand.DCMD_LINK_FIRST,
                        ReaderCommand.DCMD_SCROLL_BY ->
                            isMoveCommand = true

                        else -> {}
                    }
                    if (isMoveCommand && mBookOpened) {
                        mCanGoBack = mDoc.canGoBack()
                        synchronized(mAnimationLocker) {
                            if (null != mCurrentAnimation) {
                                log.debug("Stopping animation, as this blocks engine commands...")
                                mAnimationThread.stopAnimation()
                                mCurrentAnimation = null
                            }
                        }
                        if (invalidate)
                            invalidateImageCache()
                    }
                }
            }

            override fun done() {
                if (res) {
                    if (isMoveCommand) {
                        BackgroundThread.postBackground {
                            updateCurrentPositionStatus()
                        }
                    }
                    if (mResizeAllowed && invalidate) {
                        asyncDrawPage(doneCallback)
                        /*
                        if (isMoveCommand) {
                            BackgroundThread.postBackground {
                                // prepare the image of the next page for smooth scrolling
                                mCurrentPos?.let {
                                    val isPageView = it.pageMode != 0
                                    val offset = if (isPageView) 1 else mRenderHeight
                                    preparePageImage(offset)
                                }
                            }
                        }
                         */
                    } else {
                        log.debug("Skip redrawing as it's not requested")
                        // Will be redrawn when resizing is allowed
                        doneCallback?.invoke()
                    }
                    if (isMoveCommand && mBookOpened)
                        scheduleSaveCurrentPositionBookmark(DEF_SAVE_POSITION_INTERVAL)
                }
            }

            override fun fail(e: Exception) {
                log.error(
                    "Task ${this.javaClass.simpleName} is failed with exception ${e.message}",
                    e
                )
            }
        })
    }

    private fun navigateByHistory(cmd: ReaderCommand) {
        when (cmd) {
            ReaderCommand.DCMD_LINK_BACK,
            ReaderCommand.DCMD_LINK_FORWARD -> {
                BackgroundThread.postBackground {
                    val res: Boolean = mDoc.processCommand(cmd.nativeId, 0)
                    BackgroundThread.postGUI {
                        if (res) {
                            // successful
                            BackgroundThread.postBackground { mCanGoBack = mDoc.canGoBack() }
                            asyncDrawPage()
                            scheduleSaveCurrentPositionBookmark(DEF_SAVE_POSITION_INTERVAL)
                        } else {
                            // cannot navigate - no data in navigation history
                        }
                    }
                }
            }

            else -> {}
        }
    }

    private fun restorePositionBackground(pos: String?) {
        BackgroundThread.ensureBackground()
        if (pos != null) {
            mDoc.goToPosition(pos, false)
            asyncDrawPage()
            scheduleSaveCurrentPositionBookmark(DEF_SAVE_POSITION_INTERVAL)
        }
    }

    /**
     * Jump to specified page
     * @param page page number (starting from 1)
     */
    private fun goToPage(page: Int) {
        BackgroundThread.ensureGUI()
        val currPageNo = mCurrentPageCache?.position?.pageNo?.plus(1) ?: -1
        when (page) {
            currPageNo -> return
            currPageNo + 1 -> processCommand(ReaderCommand.DCMD_PAGEDOWN, 1, null)
            currPageNo - 1 -> processCommand(ReaderCommand.DCMD_PAGEUP, 1, null)
            else -> processCommand(ReaderCommand.DCMD_GO_PAGE, page - 1, null)
        }
    }

    /**
     * Jump to specified bookmark
     * @param bm bookmark to jump to
     */
    private fun goToBookmark(bm: Bookmark) {
        BackgroundThread.ensureGUI()
        val pos = bm.startPos
            ?: return
        BackgroundThread.postEngineTask(object : EngineTask() {
            override fun work() {
                mDoc.goToPosition(pos, true)
            }

            override fun done() {
                invalidate()
                asyncDrawPage()
            }

            override fun fail(e: Exception) {
                log.error(e.toString())
            }
        })
    }

    private fun setBackgroundTexture(backgroundImageId: String, color: Int) {
        val backgrounds = Utils.getAvailableTextures(context)
        for (info in backgrounds) {
            if (info.id == backgroundImageId) {
                setBackgroundTexture(info, color)
                return
            }
        }
        setBackgroundTexture(SettingsManager.BACKGROUND_NONE, color)
    }

    private fun setBackgroundTexture(background: SettingsManager.PageBackgroundInfo, color: Int) {
        BackgroundThread.ensureBackground()
        if (background != mCurrentBackgroundInfo || color != mCurrentBackgroundColor) {
            // mCurrentBackgroundColor just saved for future functions
            // background color is set via mDoc.applySettings(props)
            mCurrentBackgroundInfo = background
            mCurrentBackgroundColor = color
            var imageData: ByteArray? = null
            if (!background.isFile() && 0 != background.resId)
                imageData = Utils.readRawResource(context, background.resId)
            else if (background.filePath?.isNotEmpty() == true)
                imageData = Utils.readFromFile(File(background.filePath))
            mDoc.setPageBackgroundTexture(imageData, background.tiled)
        }
    }

    // update book and position info in status bar
    private fun updateCurrentPositionStatus() {
        BackgroundThread.ensureBackground()
        var chapterText: String? = null
        val pageNo = mCurrentPageCache?.position?.pageNo?.plus(1)
        val pageCount = mCurrentPageCache?.position?.pageCount
        val toc = mToc
        if (null != toc && null != pageNo) {
            val chapter = toc.getChapterAtPage(pageNo - 1)
            chapter?.name?.let {
                chapterText = it
            }
        }
        if (null == chapterText)
            chapterText = mBookInfo?.title
        chapterText?.let {
            if (it != mCurrentChapter) {
                BackgroundThread.postGUI {
                    for (view in mChapterViews)
                        view.text = it
                }
                mCurrentChapter = it
            }
        }
        if (null != pageNo && null != pageCount) {
            BackgroundThread.postGUI {
                if (mGoPanelPopup.isShowing)
                    mGoPanelPopup.update(pageNo, pageCount)
            }
        }
        // TODO: update current position in UI
        //  required only if the current position is displayed not only in this view
    }

    private fun scheduleSaveCurrentPositionBookmark(delayMillis: Long) {
        // Background thread required
        BackgroundThread.postBackground {
            mLastSavePositionTaskId++
            val mylastSavePositionTaskId = mLastSavePositionTaskId
            if (mBookOpened && mBookInfo != null) {
                val bmk = getCurrentPositionBookmark()
                    ?: return@postBackground
                val bookInfo = mBookInfo
                if (delayMillis <= 1) {
                    if (bookInfo != null) {
                        log.verbose("saving last position immediately")
                        savePositionBookmark(bmk)
                    }
                } else {
                    BackgroundThread.postBackground({
                        // if after this task scheduled new task - skip this
                        if (mylastSavePositionTaskId == mLastSavePositionTaskId) {
                            if (bookInfo != null) {
                                log.verbose("saving last position")
                                savePositionBookmark(bmk)
                            }
                        }
                    }, delayMillis)
                }
            }
        }
    }

    private fun getCurrentPositionBookmark(): Bookmark? {
        BackgroundThread.ensureBackground()
        if (!mBookOpened)
            return null
        val bmk = mDoc.getCurrentPageBookmark() // mDoc.getCurrentPageBookmarkNoRender()
        if (null != bmk) {
            updateBookReadingStats()
            bmk.timeStamp = System.currentTimeMillis()
            bmk.type = Bookmark.TYPE_LAST_POSITION
            bmk.readingTime = bookReadingDuration
        }
        return bmk
    }

    private fun savePositionBookmark(bmk: Bookmark?) {
        if (null != bmk && mBookOpened) {
            mBookInfo?.let {
                if (mLastSavedBookmark?.startPos != bmk.startPos) {
                    it.setLastPosition(bmk)
                    dbServiceAcc?.runWithService(object : DBServiceAccessor.Callback {
                        override fun run(binder: DBServiceBinder) {
                            binder.saveBookInfo(it)
                            mLastSavedBookmark = bmk
                        }
                    })
                }
            }
        }
    }

    private fun overrideToScrollViewMode() {
        BackgroundThread.ensureGUI()
        if (!mOverriddenScrollMode) {
            mOverriddenScrollMode = true
            BackgroundThread.postBackground {
                mDoc.setForceScrollViewMode(true)
                invalidateImageCache()
                asyncDrawPage { mCurrentSelection?.let { ensureSelectionVisible(it) } }
            }
            activityControl?.overrideToScrollViewMode()
        }
    }

    private fun restoreViewMode() {
        BackgroundThread.ensureGUI()
        if (mOverriddenScrollMode) {
            mOverriddenScrollMode = false
            BackgroundThread.postBackground {
                mDoc.setForceScrollViewMode(false)
                invalidateImageCache()
                asyncDrawPage()
            }
            activityControl?.restoreViewMode()
        }
    }

    private fun ensureSelectionVisible(sel: Selection) {
        var visible = false
        synchronized(mPageImageLocker) {
            mCurrentPos?.let {
                if ((sel.startHandleDocPos.y >= it.y && sel.startHandleDocPos.y < (it.y + it.pageHeight)) ||
                    (sel.endHandleDocPos.y >= it.y && sel.endHandleDocPos.y < (it.y + it.pageHeight))
                )
                    visible = true
            }
        }
        if (!visible) {
            //log.debug("Selection outside of visible area, scrolling to it...")
            val pos = sel.startHandleDocPos.y - mRenderHeight / 10
            processEngineCommand(ReaderCommand.DCMD_GO_POS, pos)
        }
    }

    private fun goToSelection(sel: Selection) {
        val pos = sel.startHandleDocPos.y - mRenderHeight / 10
        processEngineCommand(ReaderCommand.DCMD_GO_POS, pos)
    }

    private fun animateScrollTo(sel: Selection) {
        log.debug("animateScrollTo(): sel=$sel")
        BackgroundThread.ensureGUI()
        if (!mBookOpened)
            return
        if (!isAnimationAllowed) {
            log.debug("animation disabled")
            goToSelection(sel)
            return
        }
        synchronized(mAnimationLocker) {
            if (null != mCurrentAnimation) {
                log.debug("other animation already in progress! Stopping it...")
                mAnimationThread.stopAnimation()
                mCurrentAnimation = null
                goToSelection(sel)
                return
            }
        }
        val newY = sel.startHandleDocPos.y - mRenderHeight / 10
        BackgroundThread.postBackground {
            // check/prepare current page image
            val page1 = preparePageImage(0)
            // check cached page1
            if (page1?.bitmap == null || null == page1.position) {
                log.error("page1 is null or incomplete!")
                goToSelection(sel)
                return@postBackground
            }
            // check/prepare next page image
            val offset: Int
            val dir: Int
            val dY = newY - page1.position!!.y
            if (0 != page1.position!!.pageMode) {
                // page mode
                log.error("page view != scroll!")
                return@postBackground
            } else {
                // scroll mode
                dir = if (dY < 0) -1 else 1
                offset = if (dir > 0) mRenderHeight else -mRenderHeight
            }
            if (abs(dY) < mRenderHeight / 4 && newY >= page1.position!!.y) {
                log.debug("animateScrollTo(): too small offset (dY=$dY), keeping as is")
                invalidateImageCache()
                asyncDrawPage()
                return@postBackground
            }
            if (abs(dY) >= 2 * mRenderHeight / 3) {
                log.debug("animateScrollTo(): too big offset, scrolling without animation...")
                goToSelection(sel)
                return@postBackground
            }
            val page2 = preparePageImage(offset)
            // check cached page2
            if (page2?.bitmap == null || null == page2.position) {
                log.error("page2 is null or incomplete!")
                goToSelection(sel)
                return@postBackground
            }
            if (!page1.isCompatible(page2)) {
                log.error("page1 & page2 not compatible!")
                goToSelection(sel)
                return@postBackground
            }
            if (page1 == page2 || page2.position == page1.position) {
                log.error("page1 & page2 is equal!")
                goToSelection(sel)
                return@postBackground
            }
            synchronized(mAnimationLocker) {
                mCurrentAnimation = ScrollAnimation(dir, page1, page2, abs(dY))
                //val speed =
                //    (mAnimAvgFrameTime * page1.position!!.pageHeight).toFloat() / DEF_ANIMATION_DURATION.toFloat()
                //log.debug("speed=$speed")
                val speed = mRenderHeight / 240f
                mAnimationThread.startAnimation(mCurrentAnimation!!, true, speed, 0f) {
                    synchronized(mAnimationLocker) {
                        mCurrentAnimation = null
                    }
                    processEngineCommand(ReaderCommand.DCMD_SCROLL_BY, dY)
                }
            }
        }
    }

    private fun showDocContents() {
        // 1. Firstly update contents
        BackgroundThread.postBackground {
            mToc = mDoc.getTOC()
            if (null != mToc) {
                BackgroundThread.postGUI {
                    // 2. And finally show TOC
                    val page = mCurrentPageCache?.position?.pageNo?.plus(1) ?: -1
                    mDocContentPopup.show(
                        mBookInfo?.title ?: context.getString(R.string.untitled),
                        mBookInfo?.language,
                        mToc!!,
                        page
                    )
                }
            } else {
                log.warn("Document contents is null!")
            }
        }
    }

    private fun showGoPanel() {
        // 1. Firstly update contents
        BackgroundThread.postBackground {
            mToc = mDoc.getTOC()
            if (null != mToc) {
                BackgroundThread.postGUI {
                    // 2. And finally show Go panel
                    val page = mCurrentPageCache?.position?.pageNo?.plus(1) ?: -1
                    val pageCount = mCurrentPageCache?.position?.pageCount ?: -1
                    mGoPanelPopup.show(
                        mBookInfo?.title ?: context.getString(R.string.untitled),
                        mToc!!,
                        page,
                        pageCount
                    )
                }
            } else {
                log.warn("Document contents is null!")
            }
        }
    }

    private fun showBookmarks() {
        mBookInfo?.let { mBookmarksPopup.show(it) }
            ?: run { log.error("No opened documents!") }
    }

    private fun showStylePanel() {
        val props = SRProperties(mSettings)
        var bookLang = context.getString(R.string.unknown)
        mBookInfo?.let {
            it.language?.let { lang ->
                bookLang = lang
            }
            props.setBool(
                PropNames.Document.PROP_PER_DOC_TXT_OPTION_PREFORMATTED,
                it.isFlagSet(BookInfo.DONT_REFLOW_TXT_FILES_FLAG)
            )
            props.setBool(
                PropNames.Document.PROP_PER_DOC_EMBEDDED_STYLES,
                !it.isFlagSet(BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG)
            )
            props.setBool(
                PropNames.Document.PROP_PER_DOC_EMBEDDED_FONTS,
                !it.isFlagSet(BookInfo.DONT_USE_DOCUMENT_FONTS_FLAG)
            )
            props.setInt(PropNames.Document.PROP_PER_DOC_REQUESTED_DOM_VERSION, it.domVersion)
            props.setInt(
                PropNames.Document.PROP_PER_DOC_RENDER_BLOCK_RENDERING_FLAGS,
                it.blockRenderingFlags
            )
        }
        mStylePanelPopup.show(props, bookLang)
    }

    private fun showReaderMenuPanel() {
        mReaderMenuPopup.show()
    }

    private fun showSelectionPanel(active: Boolean) {
        mSelectionPanelPopup.show()
        mSelectionPanelPopup.isSelectionActive = active
    }

    private fun showTTSPanel() {
        mTTSPanelPopup.show()
    }

    private fun startTTSControlService() {
        if (null == mTTSControlServiceAccessor)
            mTTSControlServiceAccessor = TTSControlServiceAccessor(context)
        // Start the foreground service to make this app also foreground,
        // even if the main activity is in the background.
        // https://developer.android.com/about/versions/oreo/background#services
        val intent = Intent(
            TTSControlService.TTS_CONTROL_ACTION_PREPARE,
            Uri.EMPTY,
            context,
            TTSControlService::class.java
        )
        intent.putExtras(Bundle().apply {
            putParcelable("bookInfo", mBookInfo)
        })
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            context.startForegroundService(intent)
        else
            context.startService(intent)
    }

    private fun startTTS() {
        if (!mBookOpened)
            return
        BackgroundThread.executeGUI {
            activityControl?.showToast(R.string.initializing_TTS_)
            activityControl?.lockOrientation()
            activityControl?.requestPostNotificationPermissions()
        }
        startTTSControlService()
        mTTSControlServiceAccessor?.bind(object : TTSControlServiceAccessor.Callback {
            override fun run(ttsbinder: TTSControlBinder) {
                ttsbinder.initTTS(mTTSEngine, object : OnTTSCreatedListener {
                    override fun onCreated() {
                        BackgroundThread.postGUI {
                            overrideToScrollViewMode()
                            showTTSPanel()
                        }
                        processEngineCommand(
                            ReaderCommand.DCMD_SELECT_FIRST_SENTENCE,
                            ReaderCommand.DCMD_SELECT_FLAG_DONT_CHANGE_POS,
                            false
                        ) {
                            BackgroundThread.postBackground {
                                mTTSSelection = mDoc.getSelection()
                                mTTSSelection?.let {
                                    BackgroundThread.postGUI {
                                        goToSelection(it)
                                    }
                                }
                            }
                        }
                        ttsbinder.setStatusListener(mTTSStatusListener)
                        ttsbinder.setUseEndOfSentenceWorkaround(mTTSUseEndOfSentenceWorkaround)
                        ttsbinder.setUseDictionary(mTTSUseDictionary)
                        ttsbinder.setUseImmobilityTimeout(
                            mTTSUseImmobilityTimeout,
                            mTTSImmobilityTimeoutValue * 1000L
                        )
                        ttsbinder.setSpeechRate(mTTSSpeechRate, null)
                        ttsbinder.retrieveVolume(
                            object : TTSControlService.VolumeResultCallback {
                                override fun onResult(current: Int, max: Int) {
                                    mTTSVolume = current
                                    mTTSMaxVolume = max
                                    mTTSPanelPopup.update(
                                        mTTSIsSpeaking,
                                        mTTSVolume,
                                        mTTSMaxVolume,
                                        mTTSSpeechRate
                                    )
                                }
                            })
                        setupTTSVoice()
                    }

                    override fun onFailed() {
                        BackgroundThread.postGUI {
                            activityControl?.showToast(R.string.tts_init_failure)
                            stopTTSAndClosePanel()
                        }
                    }

                    override fun onTimedOut() {
                        log.error("TTS engine \"$mTTSEngine\" init failure, disabling!")
                        BackgroundThread.postGUI {
                            activityControl?.showToast(
                                R.string.tts_init_engine_failure,
                                mTTSEngine
                            )
                            activityControl?.setSetting(PropNames.App.TTS_ENGINE, "", false)
                            mTTSEngine = ""
                            stopTTSAndClosePanel()
                        }
                    }
                })
            }
        })
    }

    private fun setupTTSVoice() {
        if (mTTSUseDocLang) {
            val bookLang = mBookInfo?.language ?: ""
            // set language for TTS based on book's language
            if (bookLang.isNotEmpty()) {
                log.debug("trying to set TTS language to \"$bookLang\"")
                mTTSControlServiceAccessor?.runWithService(
                    object : TTSControlServiceAccessor.Callback {
                        override fun run(ttsbinder: TTSControlBinder) {
                            ttsbinder.setLanguage(
                                bookLang,
                                object : TTSControlService.BooleanResultCallback {
                                    override fun onResult(result: Boolean) {
                                        mTTSCurrentLanguage = bookLang
                                        mTTSCurrentVoiceName = ""
                                        if (result)
                                            log.debug("setting TTS language to \"$bookLang\" successful.");
                                        else
                                            log.error("Failed to set TTS language to \"$bookLang\".");
                                    }
                                })
                        }
                    }
                )
            } else {
                log.error("Failed to detect book's language, will be used system default!")
            }
        } else {
            val selectVoiceLambda =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    {
                        if (mTTSVoice.isNotEmpty() && mTTSVoice != mTTSCurrentVoiceName) {
                            mTTSControlServiceAccessor?.runWithService(
                                object : TTSControlServiceAccessor.Callback {
                                    override fun run(ttsbinder: TTSControlBinder) {
                                        ttsbinder.setVoice(
                                            mTTSVoice,
                                            object : TTSControlService.BooleanResultCallback {
                                                override fun onResult(result: Boolean) {
                                                    mTTSCurrentVoiceName = mTTSVoice
                                                    if (result) {
                                                        log.debug("Set voice \"$mTTSVoice\" successful")
                                                    } else {
                                                        log.error("Failed to set voice \"$mTTSVoice\"!")
                                                    }
                                                }
                                            })
                                    }
                                }
                            )
                        }
                    }
                } else {
                    {}
                }
            if (mTTSForceLang.isNotEmpty() && mTTSForceLang != mTTSCurrentLanguage) {
                mTTSControlServiceAccessor?.runWithService(
                    object : TTSControlServiceAccessor.Callback {
                        override fun run(ttsbinder: TTSControlBinder) {
                            // Select language
                            ttsbinder.setLanguage(
                                mTTSForceLang,
                                object : TTSControlService.BooleanResultCallback {
                                    override fun onResult(result: Boolean) {
                                        mTTSCurrentLanguage = mTTSForceLang
                                        mTTSCurrentVoiceName = ""
                                        if (result) {
                                            log.debug("Set language \"$mTTSForceLang\" successful")
                                        } else {
                                            log.error("Failed to set language \"$mTTSForceLang\"!")
                                        }
                                        // Then select Voice
                                        selectVoiceLambda()
                                    }
                                })
                        }
                    }
                )
            } else {
                selectVoiceLambda()
            }
        }
    }

    private fun stopTTSAndClosePanel() {
        mTTSControlServiceAccessor?.runWithService(object : TTSControlServiceAccessor.Callback {
            override fun run(ttsbinder: TTSControlBinder) {
                ttsbinder.stop(object : TTSControlService.BooleanResultCallback {
                    override fun onResult(result: Boolean) {
                        BackgroundThread.postBackground {
                            mTTSSelection = null
                        }
                        BackgroundThread.postGUI {
                            mTTSControlServiceAccessor?.unbind()
                            val intent = Intent(context, TTSControlService::class.java)
                            context.stopService(intent)
                            restoreViewMode()
                            clearSelection()
                            mTTSIsSpeaking = false
                            mTTSPanelPopup.hide()
                            saveCurrentBookInfo()
                            mTTSCurrentLanguage = ""
                            mTTSCurrentVoiceName = ""
                            activityControl?.unlockOrientation()
                        }
                    }
                })
            }
        })
    }

    fun redraw() {
        if (mResizeAllowed) {
            BackgroundThread.executeGUI {
                invalidate()
                invalidateImageCache()
                asyncDrawPage()
            }
        }
    }

    private fun startBookReadingStats() {
        if (mBookReadingStartTime == 0L) {
            mBookReadingStartTime = Utils.uptime()
            log.debug("stats: started reading")
        }
    }

    private fun stopBookReadingStats() {
        if (mBookReadingStartTime > 0L) {
            bookReadingDuration += Utils.uptimeElapsed(mBookReadingStartTime)
            mBookReadingStartTime = 0L
            log.debug("stats: stopped reading")
        }
    }

    private fun updateBookReadingStats() {
        if (mBookReadingStartTime > 0L) {
            bookReadingDuration += Utils.uptimeElapsed(mBookReadingStartTime)
        }
    }

    private fun toggleDocumentStyles() {
        if (mBookOpened) {
            mBookInfo?.let {
                log.debug("toggleDocumentStyles()")
                var disableEmbeddedStyles = it.isFlagSet(BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG)
                disableEmbeddedStyles = !disableEmbeddedStyles
                BackgroundThread.postEngineTask(object : EngineTask() {
                    override fun work() {
                        mDoc.processCommand(
                            ReaderCommand.DCMD_SET_INTERNAL_STYLES.nativeId,
                            if (disableEmbeddedStyles) 0 else 1
                        )
                    }

                    override fun done() {
                        if (disableEmbeddedStyles)
                            it.flags = it.flags or BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG
                        else
                            it.flags = it.flags and BookInfo.DONT_USE_DOCUMENT_STYLES_FLAG.inv()
                        saveCurrentBookInfo()
                    }

                    override fun fail(e: Exception) {
                        log.error("$e")
                    }
                })
            }
        }
    }


    /**
     * Stop (and wait stopping) any animation threads
     */
    fun onActivityPause() {
        mAnimationThread.stopAnimation()
        BackgroundThread.postBackground {
            getCurrentPositionBookmark()?.let { savePositionBookmark(it) }
        }
        activityControl?.setSetting(PropNames.App.VIEW_ANIM_DURATION, mAnimAvgFrameTime, false)
    }

    private fun enableResizeEvents(value: Boolean) {
        mResizeAllowed = value
        if (mResizeAllowed) {
            log.debug("Enable resize events processing")
            if (mLastRequestedWidth > 0 && mLastRequestedHeight > 0) {
                log.debug("process delayed resize event: ${mLastRequestedWidth}x${mLastRequestedHeight}")
                requestResize(mLastRequestedWidth, mLastRequestedHeight)
                mLastRequestedWidth = 0
                mLastRequestedHeight = 0
            }
        } else {
            log.debug("Disabling resize events temporary")
        }
    }

    override fun recycle() {
        BackgroundThread.postBackground {
            if (mInitialized) {
                mInitialized = false
                mDoc.recycle()
            }
        }
        mAnimationThread.shutdown()
        if (mCurrentPageCache != null) {
            mCurrentPageCache!!.recycle()
            mCurrentPageCache = null
        }
        if (mNextPageCache != null) {
            mNextPageCache!!.recycle()
            mNextPageCache = null
        }
    }

    companion object {
        private val log = SRLog.create("creview")
        private const val DEF_SAVE_POSITION_INTERVAL = 60000L // 1 minute
        private const val DEF_ANIMATION_DURATION = 300 // animation duration (ms)
        private const val DONT_STRETCH_WHILE_DRAWING = true
        private const val CENTER_PAGE_INSTEAD_OF_RESIZING = true
        private val TTS_SPEECH_RATE_VALUES = floatArrayOf(
            0.30f, 0.33f, 0.36f, 0.39f, 0.42f, 0.45f, 0.48f, 0.51f, 0.54f, 0.57f,
            0.60f, 0.63f, 0.66f, 0.69f, 0.72f, 0.75f, 0.78f, 0.81f, 0.84f, 0.87f,
            0.90f, 0.93f, 0.96f, 1.00f, 1.10f, 1.20f, 1.30f, 1.40f, 1.50f, 1.60f,
            1.70f, 1.80f, 1.90f, 2.00f, 2.10f, 2.20f, 2.30f, 2.40f, 2.50f, 2.60f,
            2.70f, 2.80f, 2.90f, 3.00f, 3.10f, 3.20f, 3.30f, 3.40f, 3.50f
        )
    }
}
