/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

class ReaderEngineObjectsAccessor {

    class EngineObjects {
        var logcatWriter: LogcatWriter? = null
        var isBackgroundThreadInitialized: Boolean = false
        var crEngineNGBinding: CREngineNGBinding? = null
        var recyclableList: ArrayList<Recyclable>? = null

        fun addRecyclable(recyclable: Recyclable) {
            recyclableList?.add(recyclable)
        }
    }

    companion object {
        private val log = SRLog.create("lifecycle")

        private val mLocker = Any()
        private var mData: EngineObjects = EngineObjects()
        private var mInstanceCount = 0

        private fun initData(data: EngineObjects) {
            log.verbose("Build main application objects: enter...")
            if (!data.isBackgroundThreadInitialized) {
                if (BackgroundThread.bootstrap())
                    data.isBackgroundThreadInitialized = true
                else
                    log.error("BackgroundThread bootstrap failed!")
            }
            if (null == data.logcatWriter)
                data.logcatWriter = LogcatWriter()
            if (null == data.crEngineNGBinding)
                data.crEngineNGBinding = CREngineNGBinding()
            if (null == data.recyclableList)
                data.recyclableList = ArrayList()
            log.verbose("Build main application objects: leave...")
        }

        private fun recycleData(data: EngineObjects) {
            log.verbose("Recycle main application objects: enter...")
            data.recyclableList?.let { list ->
                for (recyclable in list)
                    recyclable.recycle()
            }
            data.recyclableList = null
            data.crEngineNGBinding?.let { engineBinding ->
                val sema = Object()
                synchronized(sema) {
                    BackgroundThread.postBackground {
                        engineBinding.recycle()
                        synchronized(sema) {
                            sema.notify()
                        }
                    }
                    sema.wait()
                }
            }
            data.crEngineNGBinding = null
            if (data.isBackgroundThreadInitialized) {
                BackgroundThread.gentlyQuit()
                data.isBackgroundThreadInitialized = false
            }
            data.logcatWriter?.stop()
            data.logcatWriter = null
            log.verbose("Recycle main application objects: leave.")
        }

        fun leaseEngineObjects(): EngineObjects {
            synchronized(mLocker) {
                if (0 == mInstanceCount) {
                    initData(mData)
                }
                mInstanceCount++
                return mData
            }
        }

        fun releaseEngineObject(data: EngineObjects?) {
            synchronized(mLocker) {
                if (data === mData) {
                    if (mInstanceCount > 0) {
                        mInstanceCount--
                        if (0 == mInstanceCount) {
                            recycleData(mData)
                        } else {
                        }
                    } else {
                        log.error("Program error: invalid instance counter ($mInstanceCount)!")
                    }
                } else {
                    log.error("Program error: Attempt to release unrelated data!")
                }
            }
        }
    }
}
