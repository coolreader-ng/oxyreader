/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

@UsedInJNI
class PositionProperties() : Cloneable {

    // this position x-coordinate
    @UsedInJNI
    var x: Int = 0
        private set

    // this position y-coordinate
    @UsedInJNI
    var y: Int = 0
        private set

    // full document height
    @UsedInJNI
    var fullHeight: Int = 0
        private set

    // page width
    @UsedInJNI
    var pageWidth: Int = 0
        private set

    // page height
    @UsedInJNI
    var pageHeight: Int = 0
        private set

    // Current page number (starting from 0)
    @UsedInJNI
    var pageNo: Int = 0
        private set

    // Total page count
    @UsedInJNI
    var pageCount: Int = 0
        private set

    // 1, 2 for page mode, 0 for scroll mode
    @UsedInJNI
    var pageMode: Int = 0
        private set

    // number of non-whitespace characters per page
    @UsedInJNI
    var charCount: Int = 0
        private set

    // number of images per page
    @UsedInJNI
    var imageCount: Int = 0
        private set

    @UsedInJNI
    constructor(v: PositionProperties) : this() {
        x = v.x
        y = v.y
        fullHeight = v.fullHeight
        pageHeight = v.pageHeight
        pageWidth = v.pageWidth
        pageNo = v.pageNo
        pageCount = v.pageCount
        pageMode = v.pageMode
        charCount = v.charCount
        imageCount = v.imageCount
    }

    val percent: Int
        get() {
            if (fullHeight - pageHeight <= 0)
                return 0
            var p = 10000 * y / (fullHeight - pageHeight)
            if (p < 0)
                p = 0
            if (p > 10000)
                p = 10000
            return p
        }

    val canMoveToNextPage: Boolean
        get() {
            if (0 == pageMode)  // scroll mode
                return fullHeight > pageHeight && y < fullHeight - pageHeight
            // page mode
            return pageNo < pageCount - pageMode
        }

    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Any {
        return super.clone()
    }

    override fun toString(): String {
        return ("PositionProperties(pageMode=" + pageMode + ", pageNo="
                + pageNo + ", pageCount=" + pageCount + ", x=" + x + ", y="
                + y + ", pageHeight=" + pageHeight + ", pageWidth=" + pageWidth
                + ", fullHeight=" + fullHeight + ")")
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + fullHeight
        result = prime * result + pageCount
        result = prime * result + pageHeight
        result = prime * result + pageMode
        result = prime * result + pageNo
        result = prime * result + pageWidth
        result = prime * result + x
        result = prime * result + y
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other)
            return true
        if (other == null)
            return false
        if (javaClass != other.javaClass)
            return false
        val obj = other as PositionProperties
        return fullHeight == obj.fullHeight &&
                pageCount == obj.pageCount &&
                pageWidth == obj.pageWidth &&
                pageHeight == obj.pageHeight &&
                pageNo == obj.pageNo &&
                pageMode == obj.pageMode &&
                x == obj.x &&
                y == obj.y
    }

    fun isCompatible(other: PositionProperties?): Boolean {
        if (this === other)
            return true
        if (null == other)
            return false
        return fullHeight == other.fullHeight &&
                pageCount == other.pageCount &&
                pageWidth == other.pageWidth &&
                pageHeight == other.pageHeight &&
                pageMode == other.pageMode
    }
}
