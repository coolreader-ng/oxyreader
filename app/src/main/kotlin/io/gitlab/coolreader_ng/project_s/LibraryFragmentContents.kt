/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.search.SearchBar
import com.google.android.material.search.SearchView
import io.gitlab.coolreader_ng.project_s.db.DBService
import io.gitlab.coolreader_ng.project_s.db.DBServiceAccessor
import io.gitlab.coolreader_ng.project_s.db.DBServiceBinder
import io.gitlab.coolreader_ng.project_s.extensions.disableAllMenuItems
import io.gitlab.coolreader_ng.project_s.extensions.enableAllMenuItems
import io.gitlab.coolreader_ng.project_s.recyclerview.utils.RecyclerViewSimpleItemOnClickListener
import java.io.Serial
import kotlin.math.roundToInt

class LibraryFragmentContents(
    dbServiceAccessor: DBServiceAccessor? = null,
    bookCoverManager: BookCoverManager? = null,
    settings: SRProperties? = null
) :
    LibraryFragmentBase(dbServiceAccessor, bookCoverManager, settings), AbleBackwards {

    private class SearchHistory : ArrayList<DBService.SearchHistoryItem>() {

        fun haveSearchQuery(query: String): Boolean {
            var res = false
            for (item in this) {
                if (item.searchQuery == query) {
                    res = true
                    break
                }
            }
            return res
        }

        fun prepend(item: DBService.SearchHistoryItem) {
            add(0, item)
        }

        fun append(item: DBService.SearchHistoryItem) {
            add(item)
        }

        fun removeSearchQuery(query: String): Boolean {
            var idx = -1
            for (i in 0 until size) {
                if (get(i).searchQuery == query) {
                    idx = i
                    break
                }
            }
            if (idx >= 0) {
                removeAt(idx)
                return true
            }
            return false
        }

        companion object {
            @Serial
            private const val serialVersionUID: Long = -430898399294081615L
        }

    }

    private var mIsLargeLayout = false
    private var mBookSorting = DBService.BooksSorting()
    private val mBookFilters = DBService.BookFilters()
    private var mSearchText: String = ""
    private val mBookList: ArrayList<BookInfo> = ArrayList()
    private lateinit var mContentsAdapter: LibraryBookListViewAdapter
    private val mSearchHistory = SearchHistory()
    private lateinit var mBtnSort: MaterialButton
    private lateinit var mTvSearchingResult: TextView
    private lateinit var mBtnFilterText: Button
    private lateinit var mBtnFilterIcon: Button
    private lateinit var mFilterBadge: BadgeDrawable
    private lateinit var mSearchBar: SearchBar
    private lateinit var mSearchView: SearchView
    private var mActionMode: ActionMode? = null
    private lateinit var mCgHistoryItems: ChipGroup
    private lateinit var mBtnHistoryClear: Button
    private lateinit var mProgressView: View
    private lateinit var mProgressPopup: ProgressPopup

    override fun onUpdateViewImpl() {
        if (mSearchText.isNotEmpty()) {
            mBtnFilterText.visibility = View.GONE
            mBtnFilterIcon.visibility = View.GONE
            mTvSearchingResult.visibility = View.VISIBLE
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.searchBooks(
                        mSearchText,
                        mBookSorting,
                        object : DBService.BookSearchCallback {
                            override fun onBooksFound(books: Collection<BookInfo>?) {
                                if (null != books) {
                                    Utils.syncListByReference(mBookList, books, mContentsAdapter)
                                } else {
                                    if (mBookList.isNotEmpty()) {
                                        val size = mBookList.size
                                        mBookList.clear()
                                        mContentsAdapter.notifyItemRangeRemoved(0, size)
                                    }
                                }
                                hideProgress()
                            }
                        })
                }
            })
        } else {
            mBtnFilterText.visibility = View.VISIBLE
            mBtnFilterIcon.visibility = View.VISIBLE
            mTvSearchingResult.visibility = View.GONE
            showProgress()
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.loadBookList(
                        mBookSorting,
                        mBookFilters,
                        object : DBService.BooksLoadingCallback {
                            override fun onBooksListLoaded(books: Collection<BookInfo>?) {
                                if (null != books) {
                                    Utils.syncListByReference(mBookList, books, mContentsAdapter)
                                } else {
                                    if (mBookList.isNotEmpty()) {
                                        val size = mBookList.size
                                        mBookList.clear()
                                        mContentsAdapter.notifyItemRangeRemoved(0, size)
                                    }
                                }
                                hideProgress()
                            }
                        })
                }
            })
        }
    }

    override fun onHideFragment() {
        mActionMode?.let {
            it.finish()
            mActionMode = null
        }
    }

    override fun onBackPressed(): Boolean {
        if (mSearchView.isShowing) {
            mSearchView.hide()
            return true
        }
        val fragment = childFragmentManager.findFragmentById(R.id.fragmentView)
        val processed = if (fragment is AbleBackwards) fragment.onBackPressed() else false
        if (processed)
            return true
        if (childFragmentManager.backStackEntryCount > 0) {
            childFragmentManager.popBackStack()
            // Consume event
            return true
        }
        // We are saying that this event should be handled in the next chain item
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mIsLargeLayout = resources.getBoolean(R.bool.large_layout)

        val savedSorting: DBService.BooksSorting?
        val savedFilters: DBService.BookFilters?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            savedSorting =
                savedInstanceState?.getSerializable("sorting", DBService.BooksSorting::class.java)
            savedFilters =
                savedInstanceState?.getSerializable("filters", DBService.BookFilters::class.java)
        } else {
            savedSorting = savedInstanceState?.getSerializable("sorting") as DBService.BooksSorting?
            // TODO: exception:
            //  Caused by: java.lang.ClassCastException: java.util.ArrayList cannot be cast to io.gitlab.coolreader_ng.lxreader.db.DBService$BookFilters
            savedFilters = savedInstanceState?.getSerializable("filters") as DBService.BookFilters?
        }
        val savedSearchText = savedInstanceState?.getString("searchText")
        if (null != savedSorting) {
            mBookSorting = savedSorting
        } else {
            mBookSorting.sortType = DBService.BookSortType.SortByAuthor
            mBookSorting.sortDesc = false
        }
        if (mBookFilters.isEmpty() && savedFilters?.isNotEmpty() == true)
            mBookFilters.addAll(savedFilters)
        savedSearchText?.let { mSearchText = it }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("sorting", mBookSorting)
        outState.putSerializable("filters", mBookFilters)
        outState.putString("searchText", mSearchText)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mProgressView = inflater.inflate(R.layout.library_progress, container, false)
        return inflater.inflate(R.layout.library_contents, container, false)
    }

    @androidx.annotation.OptIn(com.google.android.material.badge.ExperimentalBadgeUtils::class)
    override fun onViewCreated(contentView: View, savedInstanceState: Bundle?) {
        super.onViewCreated(contentView, savedInstanceState)

        val appBarLayout = contentView.findViewById<View>(R.id.appBarLayout)
        val recyclerView = contentView.findViewById<RecyclerView>(R.id.contentRecyclerView)
        mBtnSort = contentView.findViewById(R.id.btnSort)
        mTvSearchingResult = contentView.findViewById(R.id.tvSearchingResults)
        mBtnFilterText = contentView.findViewById(R.id.btnFilterText)
        mBtnFilterIcon = contentView.findViewById(R.id.btnFilterIcon)
        val buttonsLayout = contentView.findViewById<ViewGroup>(R.id.sortNFilterToolbar)
        mSearchBar = contentView.findViewById(R.id.searchBar)
        mSearchView = contentView.findViewById(R.id.searchView)
        mBtnHistoryClear = contentView.findViewById(R.id.btnHistoryClear)
        mCgHistoryItems = contentView.findViewById(R.id.cgHistoryItems)

        mProgressPopup = ProgressPopup(contentView, mProgressView)

        var initialPosAtSelection = -1
        val actionModeCallback = object : ActionMode.Callback {
            override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                mode?.menuInflater?.inflate(R.menu.library_contents_action_bar, menu)
                return true
            }

            override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
                menu?.let { Utils.applyToolBarColorForMenu(requireContext(), it) }
                return false
            }

            override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
                when (item?.itemId) {
                    R.id.deselect -> {
                        mContentsAdapter.clearSelection()
                        return true
                    }

                    R.id.delete -> {
                        val list = mContentsAdapter.selectedItems
                        if (list.isNotEmpty()) {
                            val dialog =
                                MessageDialog(
                                    R.string.confirmation,
                                    R.string.are_you_sure_you_want_to_delete_these_books_
                                )
                            dialog.setButton(MessageDialog.StandardButtons.No)
                            dialog.setButton(MessageDialog.StandardButtons.Cancel)
                            dialog.addOption(requireContext().getString(R.string.save_book_information))
                            dialog.setButton(MessageDialog.StandardButtons.Yes) {
                                showProgress()
                                val options = dialog.optionValues
                                val saveBookInfo =
                                    if (options.isNullOrEmpty()) false else options[0]
                                deleteBooks(list, saveBookInfo) {
                                    hideProgress()
                                    mode?.finish()
                                }
                            }
                            dialog.showDialog(requireContext())
                        }
                        return true
                    }
                }
                return false
            }

            override fun onDestroyActionMode(mode: ActionMode?) {
                mContentsAdapter.isSelectionMode = false
            }
        }

        mContentsAdapter = LibraryBookListViewAdapter(requireContext(), bookCoverManager, mBookList)
        mContentsAdapter.onBookInfoRunnable = object : LibraryBookListViewAdapter.BookRunnable {
            override fun run(bookInfo: BookInfo) {
                showBookInfoImpl(bookInfo)
            }
        }
        mContentsAdapter.onCopiedInfoRunnable = Runnable {
            // TODO: Replace with Snackbar or Tooltip
            val toast = Toast.makeText(
                requireContext(),
                R.string.this_file_has_been_copied_to_the_application_s_internal_storage,
                Toast.LENGTH_LONG
            )
            toast.show()
        }
        mContentsAdapter.onSelectionModeChangedListener = object :
            LibraryBookListViewAdapter.OnSelectionModeChanged {
            override fun onSelectionModeChanged(selectionMode: Boolean) {
                if (selectionMode) {
                    val parentActivity =
                        if (activity is AppCompatActivity) (activity as AppCompatActivity) else null
                    if (null != parentActivity) {
                        mActionMode = parentActivity.startSupportActionMode(actionModeCallback)
                        mContentsAdapter.setSelectedItem(initialPosAtSelection, true)
                        mSearchBar.visibility = View.GONE
                        buttonsLayout.visibility = View.GONE
                    } else {
                        log.error("Invalid parent activity!")
                    }
                } else {
                    mSearchBar.visibility = View.VISIBLE
                    buttonsLayout.visibility = View.VISIBLE
                    mActionMode = null
                    initialPosAtSelection = -1
                }
            }
        }
        mContentsAdapter.onSelectionChangedListener = object :
            LibraryBookListViewAdapter.OnSelectionChanged {
            override fun onSelectionChanged(selectedItems: Collection<BookInfo>) {
                val count = selectedItems.size
                if (count > 0) {
                    mActionMode?.enableAllMenuItems()
                    mActionMode?.title = requireContext().getString(R.string._n_items, count)
                } else {
                    mActionMode?.disableAllMenuItems()
                    mActionMode?.title = requireContext().getString(R.string.no_selection)
                }
            }
        }
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = mContentsAdapter
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerView.addOnItemTouchListener(
            RecyclerViewSimpleItemOnClickListener(
                requireContext(),
                true
            ).also {
                it.onItemSelectedListener =
                    object : RecyclerViewSimpleItemOnClickListener.OnItemSelectedListener() {
                        override fun onItemSelected(index: Int) {
                            val bookInfo =
                                if (index >= 0 && index < mBookList.size) mBookList[index] else null
                            if (!mContentsAdapter.isSelectionMode)
                                bookInfo?.let { bi -> openBookInReaderViewImpl(bi) }
                            else
                                mContentsAdapter.setSelectedItem(
                                    index,
                                    !mContentsAdapter.isItemSelected(index)
                                )
                        }

                        override fun onLongPressed(index: Int) {
                            initialPosAtSelection = index
                            mContentsAdapter.isSelectionMode = true
                            recyclerView.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS)
                        }
                    }
            })

        val rvPaddingLeft = recyclerView.paddingLeft
        val rvPaddingRight = recyclerView.paddingRight
        ViewCompat.setOnApplyWindowInsetsListener(contentView) { _, windowInsets ->
            val insets = windowInsets.getInsets(
                WindowInsetsCompat.Type.systemBars() or
                        WindowInsetsCompat.Type.displayCutout()
            )
            appBarLayout.updatePadding(left = insets.left, right = insets.right, top = insets.top)
            recyclerView.updatePadding(
                left = insets.left + rvPaddingLeft,
                right = insets.right + rvPaddingRight
            )
            // Don't consume insets (WindowInsetsCompat.CONSUMED)
            //  since we use child fragments
            windowInsets
        }

        mFilterBadge = BadgeDrawable.create(requireContext())
        mFilterBadge.verticalOffset =
            Utils.convertUnits(24f, from = Utils.Units.DP, to = Utils.Units.PX).roundToInt()

        mBtnSort.setOnClickListener { v ->
            showSortMenu(v)
        }

        mBtnFilterText.setOnClickListener {
            showFilters()
        }
        mBtnFilterText.viewTreeObserver.addOnGlobalLayoutListener {
            BadgeUtils.attachBadgeDrawable(mFilterBadge, mBtnFilterText)
        }
        mBtnFilterIcon.setOnClickListener {
            showFilters()
        }

        mBtnHistoryClear.setOnClickListener {
            val dialog = MessageDialog(
                R.string.confirmation,
                R.string.are_you_sure_you_want_to_clear_your_search_history_
            )
            dialog.setButton(MessageDialog.StandardButtons.No)
            dialog.setButton(MessageDialog.StandardButtons.Cancel)
            dialog.setButton(MessageDialog.StandardButtons.Yes) {
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.clearBookSearchHistoryItem(object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                if (result) {
                                    mSearchHistory.clear()
                                    updateSearchHistoryView()
                                } else {
                                    log.error("Failed to clear search history!")
                                }
                            }
                        })
                    }
                })
            }
            dialog.showDialog(requireContext())
        }

        mSearchView.addTransitionListener { searchView, previousState, newState ->
            if (SearchView.TransitionState.HIDDEN == newState) {
                val text = mSearchView.text ?: ""
                if (text.isEmpty() && mSearchText.isNotEmpty()) {
                    mSearchText = ""
                    mSearchBar.setText("")
                    requestFragmentUpdate()
                }
            }
        }
        mSearchView.editText.setOnEditorActionListener { _, _, _ ->
            startToSearch(mSearchView.text.toString())
            return@setOnEditorActionListener true
        }

        updateSortButton()
        updateFilterButton()
    }

    override fun onResume() {
        super.onResume()
        dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
            override fun run(binder: DBServiceBinder) {
                binder.loadBookSearchHistory(40, object : DBService.SearchHistoryLoadingCallback {
                    override fun onHistoryLoaded(history: Collection<DBService.SearchHistoryItem>?) {
                        if (null != history) {
                            mSearchHistory.clear()
                            mSearchHistory.addAll(history)
                            // We cannot call updateSearchHistoryView() in onViewCreated() because
                            //  it may result in an IllegalStateException when calling
                            //  getLayoutInflater() or requireContext().
                            // Well, we call this in onResume() when this fragment is unconditionally
                            //  attached to the activity.
                            updateSearchHistoryView()
                        }
                    }
                })
            }
        })
    }

    private fun updateSearchHistoryView() {
        val existChips = ArrayList<Chip>()
        for (i in 0 until mCgHistoryItems.childCount) {
            val child = mCgHistoryItems.getChildAt(i)
            if (child is Chip)
                existChips.add(child)
        }
        for ((idx, item) in mSearchHistory.withIndex()) {
            val chip = if (idx < existChips.size) existChips[idx] else {
                (layoutInflater.inflate(
                    R.layout.search_item_chip,
                    mCgHistoryItems,
                    false
                ) as Chip).also { chip ->
                    mCgHistoryItems.addView(chip)
                    chip.setOnClickListener { _ ->
                        mSearchView.setText(chip.text)
                        startToSearch(chip.text.toString())
                    }
                    chip.setOnCloseIconClickListener {
                        val historyItem =
                            DBService.SearchHistoryItem(chip.tag as Long, chip.text.toString())
                        dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                            override fun run(binder: DBServiceBinder) {
                                binder.removeBookSearchHistoryItem(
                                    historyItem,
                                    object : DBService.BooleanResultCallback {
                                        override fun onResult(result: Boolean) {
                                            if (result) {
                                                if (mSearchHistory.removeSearchQuery(historyItem.searchQuery))
                                                    updateSearchHistoryView()
                                            } else {
                                                log.error("Failed to remove search query from history!")
                                            }
                                        }
                                    })
                            }
                        })
                    }
                }
            }
            chip.text = item.searchQuery
            chip.tag = item.timeStamp
        }
        for (i in mSearchHistory.size until existChips.size) {
            mCgHistoryItems.removeView(existChips[i])
        }
        mBtnHistoryClear.isEnabled = mSearchHistory.isNotEmpty()
    }

    private fun startToSearch(searchQuery: String) {
        log.debug("startToSearch(): query=$searchQuery")
        mSearchText = searchQuery.trim()
        mSearchBar.setText(mSearchText)
        log.debug("startToSearch(): mSearchBar.text=${mSearchBar.text}")
        mSearchView.hide()
        requestFragmentUpdate()
        if (mSearchText.isNotEmpty() && !mSearchHistory.haveSearchQuery(mSearchText)) {
            // Save search query to DB
            val searchQueryToSave = DBService.SearchHistoryItem(Utils.timeStamp(), mSearchText)
            dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                override fun run(binder: DBServiceBinder) {
                    binder.saveBookSearchHistoryItem(
                        searchQueryToSave,
                        object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                if (result) {
                                    mSearchHistory.prepend(searchQueryToSave)
                                    updateSearchHistoryView()
                                } else {
                                    log.error("Failed to save history item!")
                                }
                            }
                        })
                }
            })
        }
    }

    private fun showSortMenu(anchor: View) {
        val popup = PopupMenu(requireContext(), anchor)
        popup.menuInflater.inflate(R.menu.library_sort_menu, popup.menu)
        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.sort_by_author_asc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByAuthor, false)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_author_desc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByAuthor, true)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_title_asc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByTitle, false)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_title_desc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByTitle, true)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_series_asc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortBySeries, false)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_series_desc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortBySeries, true)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_reading_date_asc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByLastReadTime, false)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                R.id.sort_by_reading_date_desc -> {
                    val newSorting =
                        DBService.BooksSorting(DBService.BookSortType.SortByLastReadTime, true)
                    if (mBookSorting != newSorting) {
                        mBookSorting = newSorting
                        updateSortButton()
                        requestFragmentUpdate()
                    }
                }

                else -> {}
            }
            return@setOnMenuItemClickListener true
        }
        popup.show()
    }

    private fun showFilters() {
        val dialogFragment = LibraryFilterFragment(mBookFilters.copy(), dbServiceAccessor)
        dialogFragment.onActionsListener = object : LibraryFilterFragment.OnActionsListener {
            override fun onDismiss() {
            }

            override fun onAccept(filters: DBService.BookFilters) {
                log.debug("accepting new filters: $filters")
                mBookFilters.replace(filters)
                updateFilterButton()
                requestFragmentUpdate()
            }
        }
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(childFragmentManager, "libraryFilters")
        } else {
            val transaction = childFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.replace(R.id.fragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun updateSortButton() {
        var stringResId = -1
        when (mBookSorting.sortType) {
            DBService.BookSortType.SortByAuthor -> {
                stringResId = if (!mBookSorting.sortDesc)
                    R.string.by_author_ascending
                else
                    R.string.by_author_descending
            }

            DBService.BookSortType.SortByTitle -> {
                stringResId = if (!mBookSorting.sortDesc)
                    R.string.by_title_ascending
                else
                    R.string.by_title_descending
            }

            DBService.BookSortType.SortBySeries -> {
                stringResId = if (!mBookSorting.sortDesc)
                    R.string.by_series_ascending
                else
                    R.string.by_series_descending
            }

            DBService.BookSortType.SortByLastReadTime -> {
                stringResId = if (!mBookSorting.sortDesc)
                    R.string.by_reading_date_ascending
                else
                    R.string.by_reading_date_descending
            }

            else -> {}
        }
        val iconResId = if (!mBookSorting.sortDesc)
            R.drawable.ic_action_sort_asc
        else
            R.drawable.ic_action_sort_desc
        mBtnSort.setText(stringResId)
        mBtnSort.setIconResource(iconResId)
    }

    @androidx.annotation.OptIn(com.google.android.material.badge.ExperimentalBadgeUtils::class)
    private fun updateFilterButton() {
        if (mBookFilters.isNotEmpty()) {
            mFilterBadge.number = mBookFilters.size
            mFilterBadge.isVisible = true
        } else {
            mFilterBadge.isVisible = false
        }
        // TODO: check if it's really needed?
        //BadgeUtils.attachBadgeDrawable(mFilterBadge, mBtnFilterText)
    }

    private fun showBookInfoImpl(bookInfo: BookInfo) {
        val dialogFragment = BookInfoDialogFragment(bookInfo, bookCoverManager)
        dialogFragment.onActionsListener = object : BookInfoDialogFragment.OnActionsListener {
            override fun onOpenBook(bookInfo: BookInfo) {
                openBookInReaderViewImpl(bookInfo)
            }

            override fun onInfoChanged(bookInfo: BookInfo) {
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.saveBookInfo(bookInfo, object : DBService.BooleanResultCallback {
                            override fun onResult(result: Boolean) {
                                // TODO:
                                //if (!result)
                                //    showToast(getString(R.string.failed_to_save_info_), Toast.LENGTH_LONG)
                            }
                        })
                    }
                })
            }
        }
        val args = Bundle()
        args.putBoolean("asDialog", mIsLargeLayout)
        args.putInt(
            "toolbar",
            settings?.getInt(PropNames.App.TOOLBAR_LOCATION, PropNames.App.TOOLBAR_LOCATION_NONE)
                ?: PropNames.App.TOOLBAR_LOCATION_NONE
        )
        dialogFragment.arguments = args
        if (mIsLargeLayout) {
            dialogFragment.show(childFragmentManager, "bookInfo")
        } else {
            val transaction = childFragmentManager.beginTransaction()
            transaction.setReorderingAllowed(true)
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            transaction.replace(R.id.fragmentView, dialogFragment)
                .addToBackStack(null)
            transaction.commit()
        }
    }

    private fun openBookInReaderViewImpl(bookInfo: BookInfo) {
        if (!bookInfo.isBookFileExists) {
            val dialog =
                MessageDialog(R.string.error, R.string.file_is_missing_it_may_have_been_deleted)
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else if (!bookInfo.isBookFileAvailable) {
            val dialog = MessageDialog(
                R.string.error,
                R.string.file_is_unavailable_may_not_have_read_permissions
            )
            dialog.setButton(MessageDialog.StandardButtons.Ok)
            dialog.showDialog(requireContext())
        } else {
            // open book for reading
            startActivity(Intent(
                Intent.ACTION_VIEW,
                Uri.Builder().scheme("file").path(bookInfo.fileInfo.pathNameWA).build(),
                context,
                ReaderActivity::class.java
            ).apply {
                setPackage(requireContext().packageName)
            })
            if (finishWhenOpeningBook)
                requireActivity().finish()
        }
    }

    private fun deleteBooks(
        list: Collection<BookInfo>,
        saveBookInfo: Boolean,
        onCompleteCallback: () -> Unit
    ) {
        BackgroundThread.postBackground {
            val toRemoveInfoList = ArrayList<BookInfo>()
            val deleteFailedList = ArrayList<BookInfo>()
            var deletedCount = 0
            for (book in list) {
                val res = if (book.isBookFileExists) book.deleteBookFile() else true
                if (res) {
                    deletedCount++
                    if (!saveBookInfo)
                        toRemoveInfoList.add(book)
                } else {
                    deleteFailedList.add(book)
                }
            }
            if (toRemoveInfoList.isNotEmpty()) {
                dbServiceAccessor?.runWithService(object : DBServiceAccessor.Callback {
                    override fun run(binder: DBServiceBinder) {
                        binder.removeMultipleBookInfo(toRemoveInfoList,
                            object : DBService.BooksOperationCallback {
                                override fun onBooksProcessed(result: Map<BookInfo, Boolean>?) {
                                    BackgroundThread.postGUI {
                                        val removeInfoFailedList = ArrayList<BookInfo>()
                                        result?.let {
                                            for ((book, delRes) in it) {
                                                if (!delRes)
                                                    removeInfoFailedList.add(book)
                                            }
                                        }
                                        if (deleteFailedList.isNotEmpty()) {
                                            val message =
                                                StringBuilder(requireContext().getString(R.string.following_files_could_not_be_deleted_))
                                            message.append("\n")
                                            val iter = deleteFailedList.iterator()
                                            while (iter.hasNext()) {
                                                val book = iter.next()
                                                message.append("\"${book.fileInfo.pathNameWA}\"")
                                                if (iter.hasNext())
                                                    message.append(";\n")
                                            }
                                            val dialog = MessageDialog(R.string.error)
                                            dialog.setMessage(message.toString())
                                            dialog.setButton(MessageDialog.StandardButtons.Ok)
                                            dialog.showDialog(requireContext())
                                        }
                                        if (removeInfoFailedList.isNotEmpty()) {
                                            val message2 =
                                                StringBuilder(requireContext().getString(R.string.failed_to_delete_info_about_following_files_))
                                            message2.append("\n")
                                            val iter = removeInfoFailedList.iterator()
                                            while (iter.hasNext()) {
                                                val book = iter.next()
                                                message2.append("\"${book.fileInfo.pathNameWA}\"")
                                                if (iter.hasNext())
                                                    message2.append("\n")
                                            }
                                            val dialog2 = MessageDialog(R.string.error)
                                            dialog2.setMessage(message2.toString())
                                            dialog2.setButton(MessageDialog.StandardButtons.Ok)
                                            dialog2.showDialog(requireContext())
                                        }
                                        onCompleteCallback()
                                        if (deletedCount > 0) {
                                            // Update list if at least one file was deleted
                                            requestFragmentUpdate()
                                        }
                                    }
                                }
                            })
                    }
                })
            } else {
                BackgroundThread.postGUI {
                    if (deleteFailedList.isNotEmpty()) {
                        val message =
                            StringBuilder(requireContext().getString(R.string.following_files_could_not_be_deleted_))
                        message.append("\n")
                        val iter = deleteFailedList.iterator()
                        while (iter.hasNext()) {
                            val book = iter.next()
                            message.append("\"${book.fileInfo.pathNameWA}\"")
                            if (iter.hasNext())
                                message.append(";\n")
                        }
                        val dialog = MessageDialog(R.string.error)
                        dialog.setMessage(message.toString())
                        dialog.setButton(MessageDialog.StandardButtons.Ok)
                        dialog.showDialog(requireContext())
                    }
                    if (deletedCount > 0)
                        requestFragmentUpdate()
                    onCompleteCallback()
                }
            }
        }
    }

    private fun showProgress() {
        mProgressPopup.isIndeterminate = true
        mProgressPopup.x = 0
        mProgressPopup.y = 0
        mProgressPopup.width = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.height = ViewGroup.LayoutParams.MATCH_PARENT
        mProgressPopup.show()
    }

    private fun hideProgress() {
        mProgressPopup.hide()
    }

    init {
        propertiesObserver = object : PropertiesObserver {
            override fun dbServiceAccessorIsSet(dbServiceAccessor: DBServiceAccessor?) {
                // Find LibraryFilterFragment & set dbServiceAccessor
                val filterFragments = childFragmentManager.fragments.filterIsInstance(
                    LibraryFilterFragment::class.java
                )
                for (fragment in filterFragments) {
                    if (null == fragment.dbServiceAccessor)
                        fragment.dbServiceAccessor = dbServiceAccessor
                }
            }

            override fun bookCoverManagerIsSet(bookCoverManager: BookCoverManager?) {
            }

            override fun settingsIsSet(settings: SRProperties?) {
            }
        }
    }

    companion object {
        private val log = SRLog.create("library.contents")
    }
}
