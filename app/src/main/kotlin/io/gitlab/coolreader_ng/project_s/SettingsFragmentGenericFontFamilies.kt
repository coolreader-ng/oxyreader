/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.project_s

import android.os.Bundle
import androidx.annotation.Keep
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat

@Keep
class SettingsFragmentGenericFontFamilies : PreferenceFragmentCompat(),
    SettingsActivity.SettingsFragmentStorageHolder {

    private var mProperties = SRProperties()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.preferenceDataStore = SettingsActivity.CREPropsDataStorage(mProperties)

        setPreferencesFromResource(R.xml.preferences_gff, rootKey)

        // Font faces
        val engineAcc = ReaderEngineObjectsAccessor.leaseEngineObjects()
        val fontFaces = engineAcc.crEngineNGBinding?.getFontFaceList()
        // For now crengine-ng can't filter font faces by family other than "monospace"
        val monospaceFontFaces = engineAcc.crEngineNGBinding?.getFontFaceListFiltered(
            CREngineNGBinding.FontFamily.MONOSPACE,
            ""
        )
        ReaderEngineObjectsAccessor.releaseEngineObject(engineAcc)

        val serifFontPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_GFF_SERIF_FONT)
        serifFontPreference?.entries = fontFaces
        serifFontPreference?.entryValues = fontFaces

        val sansSerifFontPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_GFF_SANS_SERIF_FONT)
        sansSerifFontPreference?.entries = fontFaces
        sansSerifFontPreference?.entryValues = fontFaces

        val cursiveFontPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_GFF_CURSIVE_FONT)
        cursiveFontPreference?.entries = fontFaces
        cursiveFontPreference?.entryValues = fontFaces

        val fantasyFontPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_GFF_FANTASY_FONT)
        fantasyFontPreference?.entries = fontFaces
        fantasyFontPreference?.entryValues = fontFaces

        val monospaceFontPreference =
            findPreference<ListPreference>(SettingsActivity.PREF_KEY_GFF_MONOSPACE_FONT)
        monospaceFontPreference?.entries = monospaceFontFaces
        monospaceFontPreference?.entryValues = monospaceFontFaces
    }

    override fun setProperties(props: SRProperties) {
        mProperties = props
    }

    override fun resetToDefaults() {
        SettingsManager.Defaults.updateGFFDefaults()
        for ((key, value) in SettingsManager.Defaults.GFF) {
            mProperties.setProperty(key, value)
        }
    }
}