/*
 * book reader based on crengine-ng
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Based on CoolReader project code at https://github.com/buggins/coolreader
 * Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>
 */

package io.gitlab.coolreader_ng.project_s

import android.graphics.Bitmap
import android.graphics.Point
import androidx.core.graphics.Insets
import io.gitlab.coolreader_ng.genrescollection.GenresCollection
import java.util.Properties

/**
 * Accessor for crengine-ng' LVDocView instance.
 * All operations on this LVDocView instance must be performed on a special 'BackgroundThread' thread.
 */
@UsedInJNI
class LVDocViewWrapper {

    enum class ContinuousOperationResult {
        CR_DONE,    ///< operation is finished successfully
        CR_TIMEOUT, ///< operation is incomplete - interrupted by timeout
        CR_ERROR    ///< error while executing operation
    }

    // internal native data
    private var mNativeDataPointer: Long = 0
    private val mSyncObject = Object()
    private var mWidth = 0
    private var mHeight = 0

    @UsedInJNI
    @SuppressWarnings("unused") // used in jni
    private var jni_docviewCallback: DocViewCallback? = null

    /**
     * Create new LVDocView instance.
     */
    fun createDocView() {
        BackgroundThread.ensureBackground()
        synchronized(mSyncObject) {
            mNativeDataPointer = newLVDocView_native()
        }
    }

    fun recycle() {
        BackgroundThread.ensureBackground()
        synchronized(mSyncObject) {
            if (0L != mNativeDataPointer) {
                deleteLVDocView_native(mNativeDataPointer)
                mNativeDataPointer = 0
            }
        }
    }

    /**
     * Set document callback.
     * Since all operations on this LVDocView instance are performed on a special background thread,
     * all functions in this callback will also be called on the same background thread.
     *
     * @param docviewCallback is callback to set
     */
    fun setDocViewCallback(docviewCallback: DocViewCallback?) {
        jni_docviewCallback = docviewCallback
    }

    fun loadDocument(filename: String, metadataOnly: Boolean = false): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return loadDocument_native(mNativeDataPointer, filename, metadataOnly)
        }
    }

    fun loadDocument(
        docBinaryData: ByteArray,
        streamName: String,
        metadataOnly: Boolean = false
    ): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return loadDocumentBuff_native(
                mNativeDataPointer,
                docBinaryData,
                streamName,
                metadataOnly
            )
        }
    }

    fun renderPageBitmap(bitmap: Bitmap, render_bpp: Byte): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            renderPageBitmap_native(mNativeDataPointer, bitmap, render_bpp)
        }
        return true
    }

    fun resize(width: Int, height: Int): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        mWidth = width
        mHeight = height
        synchronized(mSyncObject) {
            resize_native(mNativeDataPointer, mWidth, mHeight)
        }
        return true
    }

    fun processCommand(command: Int, param: Int): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return processCommand_native(mNativeDataPointer, command, param)
        }
    }

    fun requestRender(): Boolean {
        BackgroundThread.ensureBackground()
        return processCommand(ReaderCommand.DCMD_REQUEST_RENDER.nativeId, 0)
    }

    fun getCurrentPageBookmark(): Bookmark? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            return getCurrentPageBookmark_native(mNativeDataPointer)
        }
    }

    /**
     * Get position properties by xPath.
     *
     * @param xPath   if not null, path to a specific document tag. Null is interpreted as the current position.
     * @param extInfo Specifies whether the fields 'charCount' and 'imageCount' should be filed.
     * For page turn command 'extInfo' must be 'false' for maximum speed.
     * For bookmarking 'extInfo' must be 'true' for more accurate result (y position).
     * @return position properties as a new object if successful, null otherwise.
     */
    fun getPositionProps(xPath: String?, extInfo: Boolean = false): PositionProperties? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            return getPositionProps_native(mNativeDataPointer, xPath, extInfo)
        }
    }

    fun getAvgTextLineHeight(): Int {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return -1
        }
        synchronized(mSyncObject) {
            return getAvgTextLineHeight_native(mNativeDataPointer)
        }
    }

    /**
     * Find a link near to specified window coordinates.
     * @param x
     * @param y
     * @param delta
     * @return
     */
    fun checkLink(x: Int, y: Int, delta: Int): String? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            return checkLink_native(mNativeDataPointer, x, y, delta)
        }
    }

    /**
     * Check whether point of current document belongs to bookmark.
     *
     * @param x is X coordinate in document window
     * @param y is Y coordinate in document window
     * @return bookmark if point belongs to bookmark, null otherwise
     */
    fun checkBookmark(x: Int, y: Int): Bookmark? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            val dstBookmark = Bookmark()
            if (checkBookmark_native(mNativeDataPointer, x, y, dstBookmark))
                return dstBookmark
            return null
        }
    }

    /**
     * Returns an image at the specified window coordinates.
     *
     * @param x is X coordinate in document window
     * @param y is Y coordinate in document window
     * @param dstImage is to place found image to
     * @return true if point belongs to image
     */
    fun getEmbeddedImage(x: Int, y: Int, dstImage: PageImageCache, render_bpp: Byte): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return getEmbeddedImage_native(mNativeDataPointer, x, y, dstImage, render_bpp)
        }
    }

    /**
     * Follow link.
     * @param link
     * @return
     */
    fun goLink(link: String): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return goLink_native(mNativeDataPointer, link)
        }
    }

    /**
     * Move reading position to specified xPath.
     * @param xPath
     * @return
     */
    fun goToPosition(xPath: String, saveToHistory: Boolean): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return goToPosition_native(mNativeDataPointer, xPath, saveToHistory)
        }
    }

    /**
     * Get engine settings
     * @param props object to save settings
     */
    fun getSettings(props: Properties): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return getSettings_native(mNativeDataPointer, props)
        }
    }

    /**
     * Apply settings
     * @param props settings to apply
     */
    fun applySettings(props: Properties): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return applySettings_native(mNativeDataPointer, props)
        }
    }

    /**
     * Set battery state, type of battery charger, battery charge level.
     * This is necessary to update the battery information in running title.
     *
     * @param batteryState battery state
     * @param batteryCharger battery charger
     * @param chargeLevel battery charge level
     */
    fun setBatteryState(
        batteryState: ReaderView.BatteryState,
        batteryCharger: ReaderView.BatteryCharger,
        chargeLevel: Int
    ) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            setBatteryState_native(
                mNativeDataPointer,
                batteryState.code,
                batteryCharger.code,
                chargeLevel
            )
        }
    }

    /**
     * Checking if the time has changed since the clock was last drawn
     * @return true if time has changed, false otherwise.
     */
    fun isTimeChanged(): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return isTimeChanged_native(mNativeDataPointer)
        }
    }

    /**
     * Sets the view mode for scrolling regardless of settings
     * @param value true to set the scrolling mode, false to restore if it was previously changed
     * @return true if view mode is changed
     */
    fun setForceScrollViewMode(value: Boolean): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        return setForceScrollViewMode_native(mNativeDataPointer, value)
    }

    /**
     * If document uses cache file, save all data to it.
     * @return either CR_DONE, CR_TIMEOUT, CR_ERROR
     */
    fun updateCache(timeout: Long): ContinuousOperationResult {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return ContinuousOperationResult.CR_ERROR
        }
        synchronized(mSyncObject) {
            return when (updateCache_native(mNativeDataPointer, timeout)) {
                0 -> ContinuousOperationResult.CR_DONE
                1 -> ContinuousOperationResult.CR_TIMEOUT
                2 -> ContinuousOperationResult.CR_ERROR
                else -> ContinuousOperationResult.CR_ERROR
            }
        }
    }

    /**
     * Cancel document caching operation.
     */
    fun cancelLongCachingOperation() {
        // This function can be called from any thread
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        // we can't use `synchronized` here to avoid blocking the calling thread
        cancelLongCachingOperation_native(mNativeDataPointer)
    }

    /**
     * Fill book info fields using metadata from current book.
     * @param info
     * @param updatePath
     */
    fun updateBookInfo(
        info: BookInfo,
        updatePath: Boolean,
        genresCollection: GenresCollection? = null
    ) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            updateBookInfo_native(mNativeDataPointer, info, updatePath)
            // Apply extra data again as some properties may have changed
            info.reApplyExtraData()
            if (DocumentFormat.FB2 == info.format) {
                // TODO: investigate what needed for fb3 format
                // Now we need to convert the "keywords" field into genre codes
                genresCollection?.let { genres ->
                    // FB2 genre codes are written in the "keywords" field in the JNI code.
                    //  see CRJNIEnv::updateBookInfo()
                    val keywords = info.keywords
                    if (keywords?.isNotEmpty() == true) {
                        val unknownGenresFab = HashSet<String>()
                        val genresFab = HashSet<Int>()
                        for (genreCode in keywords) {
                            val code = genreCode.trim()
                            val genre = genres.byCode(code)
                            if (null != genre)
                                genresFab.add(genre.id)
                            else
                                unknownGenresFab.add(code)
                        }
                        if (genresFab.isNotEmpty())
                            info.genresIds = genresFab
                        else
                            info.genresIds = null
                        if (unknownGenresFab.isNotEmpty())
                            info.unsupportedGenres = unknownGenresFab
                        else
                            info.unsupportedGenres = null
                        // nullify after processing, since genres are stored in keywords temporarily
                        info.keywords = null
                    }
                }
            }
        }
    }

    /**
     * create empty document with specified message (e.g. to show errors)
     * @param title document's title
     * @param message document's body
     */
    fun createDefaultDocument(title: String, message: String) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            createDefaultDocument_native(mNativeDataPointer, title, message)
        }
    }

    /**
     * Set background texture for this docview
     * @param imageData
     * @param tiled
     */
    fun setPageBackgroundTexture(imageData: ByteArray?, tiled: Boolean) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            setPageBackgroundTexture_native(mNativeDataPointer, imageData, tiled)
        }
    }

    fun getTOC(): DocumentTOCItem? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            return getTOC_native(mNativeDataPointer)
        }
    }

    fun canGoBack(): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            return canGoBack_native(mNativeDataPointer)
        }
    }

    fun getBookCoverData(): ByteArray? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            return getBookCoverData_native(mNativeDataPointer)
        }
    }

    /**
     * Gets the current selection.
     * @param selection selection object where the result will be written
     * @return true if the selection is found in the document and is not empty, otherwise false
     */
    fun getSelection(selection: Selection): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        var res: Boolean
        synchronized(mSyncObject) {
            res = getSelection_native(mNativeDataPointer, selection)
            if (res && selection.isEmpty)
                res = false
        }
        return res
    }

    /**
     * Gets the current selection.
     * @return selection object, or null if there is no current selection or an error occurred
     */
    fun getSelection(): Selection? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            val selection = Selection()
            val res = getSelection_native(mNativeDataPointer, selection)
            if (res && selection.isNotEmpty)
                return selection
        }
        return null
    }

    /**
     * Selects text in the specified rectangle.
     * @param startX x coordinate of the start of the selection
     * @param startY Y coordinate of the start of the selection
     * @param endX x coordinate of the end of the selection
     * @param endY y coordinate of the end of the selection
     * @param stickyBounds selection bounds stick to word bounds
     * @param selection selection object where the result will be written (may be null if the result is not needed)
     * @return selection object, or null if there is no current selection or an error occurred
     */
    fun selectText(
        startX: Int,
        startY: Int,
        endX: Int,
        endY: Int,
        stickyBounds: Boolean,
        selection: Selection? = null
    ): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return false
        }
        synchronized(mSyncObject) {
            var res =
                selectText_native(mNativeDataPointer, startX, startY, endX, endY, stickyBounds)
            if (res && null != selection)
                res = getSelection_native(mNativeDataPointer, selection)
            return res
        }
    }

    fun clearSelection() {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            clearSelection_native(mNativeDataPointer)
        }
    }

    fun docToWindowPoint(dst: Point, src: Point) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            docToWindowPoint_native(mNativeDataPointer, dst, src)
        }
    }

    /**
     * Sets bookmarks for the currently open document/book.
     * @param bookInfo book info with bookmarks that need to be set
     */
    fun setBookmarks(bookInfo: BookInfo) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        val count = bookInfo.bookmarkCount
        val bookmarks = if (count > 0) {
            Array(count) { i -> bookInfo.getBookmark(i) }
        } else {
            null
        }
        synchronized(mSyncObject) {
            setBookmarks_native(mNativeDataPointer, bookmarks)
        }
    }

    /**
     * Set decimal separator char.
     * @param decimalPointChar decimal separator char
     */
    fun setDecimalPointChar(decimalPointChar: Char) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            setDecimalPointChar_native(mNativeDataPointer, decimalPointChar)
        }
    }

    fun applyPageInsets(insets: Insets, allowPageHeaderOverlap: Boolean) {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return
        }
        synchronized(mSyncObject) {
            applyPageInsets_native(
                mNativeDataPointer,
                insets.left,
                insets.top,
                insets.right,
                insets.bottom,
                allowPageHeaderOverlap
            )
        }
    }

    /**
     * Find all occurrences of text within a document
     * @param text search text
     * @param caseSensitivity case sensitive search
     * @param maxCount maximum results count
     * @return array of search result items, null if any errors occurred.
     *
     * If nothing is found, a non-null empty array is returned.
     */
    fun findTextAll(
        text: String,
        caseSensitivity: Boolean,
        maxCount: Int
    ): List<SearchResultItem>? {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
            return null
        }
        synchronized(mSyncObject) {
            val results = findTextAll_native(mNativeDataPointer, text, caseSensitivity, maxCount)
            if (null != results) {
                val list = ArrayList<SearchResultItem>()
                for (item in results)
                    item?.let { list.add(it) }
                return list
            }
            return null
        }
    }

    /**
     * Select search result item and scroll to it. Clears existing selection.
     * @param searchResultItem search result item
     */
    fun selectSearchResult(searchResultItem: SearchResultItem): Boolean {
        BackgroundThread.ensureBackground()
        if (0L == mNativeDataPointer) {
            log.error("Attempt to call a native function with an uninitialized object.")
        }
        synchronized(mSyncObject) {
            return selectSearchResult_native(mNativeDataPointer, searchResultItem)
        }
    }

    private external fun newLVDocView_native(): Long
    private external fun deleteLVDocView_native(instance: Long)

    private external fun loadDocument_native(
        instance: Long,
        filename: String,
        metadataOnly: Boolean
    ): Boolean

    private external fun loadDocumentBuff_native(
        instance: Long,
        data: ByteArray,
        streamName: String,
        metadataOnly: Boolean
    ): Boolean

    private external fun renderPageBitmap_native(instance: Long, bitmap: Bitmap, render_bpp: Byte)
    private external fun resize_native(instance: Long, width: Int, height: Int)
    private external fun processCommand_native(instance: Long, command: Int, param: Int): Boolean
    private external fun getCurrentPageBookmark_native(instance: Long): Bookmark?
    private external fun getPositionProps_native(
        instance: Long,
        xPath: String?,
        extInfo: Boolean
    ): PositionProperties?

    private external fun getAvgTextLineHeight_native(instance: Long): Int
    private external fun checkLink_native(instance: Long, x: Int, y: Int, delta: Int): String?
    private external fun checkBookmark_native(
        instance: Long,
        x: Int,
        y: Int,
        dstBookmark: Bookmark
    ): Boolean

    private external fun getEmbeddedImage_native(
        instance: Long,
        x: Int,
        y: Int,
        dstImage: PageImageCache,
        render_bpp: Byte
    ): Boolean

    private external fun goLink_native(instance: Long, link: String): Boolean
    private external fun goToPosition_native(
        instance: Long,
        xPath: String,
        saveToHistory: Boolean
    ): Boolean

    private external fun getSettings_native(instance: Long, props: Properties): Boolean
    private external fun applySettings_native(instance: Long, props: Properties): Boolean
    private external fun setBatteryState_native(
        instance: Long,
        batteryState: Int,
        batteryCharger: Int,
        chargeLevel: Int
    )

    private external fun isTimeChanged_native(instance: Long): Boolean
    private external fun updateCache_native(instance: Long, timeout: Long): Int
    private external fun cancelLongCachingOperation_native(instance: Long)
    private external fun updateBookInfo_native(instance: Long, info: BookInfo, updatePath: Boolean)
    private external fun createDefaultDocument_native(
        instance: Long,
        title: String,
        message: String
    )

    private external fun setPageBackgroundTexture_native(
        instance: Long,
        imageData: ByteArray?,
        tiled: Boolean
    )

    private external fun getTOC_native(instance: Long): DocumentTOCItem?
    private external fun canGoBack_native(instance: Long): Boolean
    private external fun getBookCoverData_native(instance: Long): ByteArray?
    private external fun getSelection_native(instance: Long, selection: Selection): Boolean
    private external fun selectText_native(
        instance: Long,
        startX: Int,
        startY: Int,
        endX: Int,
        endY: Int,
        stickyBounds: Boolean
    ): Boolean

    private external fun clearSelection_native(instance: Long)
    private external fun docToWindowPoint_native(instance: Long, dst: Point, src: Point)
    private external fun setBookmarks_native(instance: Long, bookmarks: Array<Bookmark>?)
    private external fun setForceScrollViewMode_native(instance: Long, value: Boolean): Boolean
    private external fun setDecimalPointChar_native(instance: Long, decimalPointChar: Char)
    private external fun applyPageInsets_native(
        instance: Long,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int,
        allowPageHeaderOverlap: Boolean
    )

    private external fun findTextAll_native(
        instance: Long,
        text: String,
        caseSensitivity: Boolean,
        maxCount: Int
    ): Array<SearchResultItem?>?

    private external fun selectSearchResult_native(
        instance: Long,
        searchResultItem: SearchResultItem
    ): Boolean

    companion object {
        private val log = SRLog.create("doc")
    }

    init {
        synchronized(mSyncObject) {
            mNativeDataPointer = 0
        }
    }
}