/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "crengine-ng-binding.h"

#include <crhyphman.h>
#include <lvfntman.h>
#include <lvstreamutils.h>
#include <lvcontaineriteminfo.h>
#include <ldomdoccache.h>
#include <lvtinydom_common.h>
#include <lvfnt.h>
#include <crlocaledata.h>
#include <lvdocview.h>
#include <lvgraydrawbuf.h>
#include <crlog.h>

#include <android/log.h>

#include "crengine-ng-jni.h"
#include "crjnienv.h"
#include "crlog-jnibridge.h"
#include "crjni-accessors.h"
#include "hyphdataloaderproxy.h"
#include "jnigraphicslib.h"

#define LOG_TAG "cre-ng"
#define SYSTEM_FONTS_DIR    "/system/fonts/"

bool InitFontsFromDir(const lString32 &fontDir);

bool getDirectoryFonts(const lString32 &fontDir, lString32Collection &ext,
                       lString32Collection &fonts, bool absPath, bool recursively);

void myFatalErrorHandler(int errorCode, const char *errorText);

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    engineInit_native
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineInit_1native
        (JNIEnv *, jobject, jint sdk_int) {
    CRJNIEnv::sdk_int = sdk_int;
    // to catch crashes and remove current cache file on crash (SIGSEGV, etc.)
    crSetSignalHandler();
    // set fatal error handler
    crSetFatalErrorHandler(&myFatalErrorHandler);
    // set logger to logcat
    CRLog::setLogger(new CRLogJNIBridge);
    CRLog::setLogLevel(CRLog::LL_TRACE);
    // init engine: font manager, etc
    // Don't enumerate installed system fonts.
    InitFontManager(lString8::empty_str, false);
    // Use only one platform independent font set
    bool fonts_ok = InitFontsFromDir(cs32(SYSTEM_FONTS_DIR));
    if (!fonts_ok)
        CRLog::error("Cannot init crengine-ng (fonts)\n");
    // Initializing hyphenation manager without any dictionaries
    CRLog::info("initializing hyphenation manager");
    HyphMan::initDictionaries(lString32::empty_str); //don't look for dictionaries
    HyphMan::activateDictionary(lString32(HYPH_DICT_ID_NONE));
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    engineCleanup_native
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineCleanup_1native
        (JNIEnv *, jobject) {
    HyphMan::uninit();
    ShutdownFontManager();
    CRLog::debug("engineCleanup_native(): resources released");
    CRLog::setLogger(nullptr);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    setCacheDir_native
 * Signature: (Ljava/lang/String;J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setCacheDir_1native
        (JNIEnv *env_, jobject, jstring path, jlong size) {
    CRJNIEnv env(env_);
    return ldomDocCache::init(env.fromJavaString(path), size);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    setupHyphenations_native
 * Signature: ([Lio/gitlab/coolreader_ng/project_s/HyphenationDict;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setupHyphenations_1native
        (JNIEnv *env_, jobject creEnv, jobjectArray dictArray) {
    CRJNIEnv env(env_);
    jclass pjcHyphDict = env->FindClass(PACKAGE_SIG "/HyphenationDict");
    if (nullptr == pjcHyphDict)
        return JNI_FALSE;
    jclass pjcLanguageItem = env->FindClass(PACKAGE_SIG "/LanguageItem");
    if (nullptr == pjcLanguageItem)
        return JNI_FALSE;
    jfieldID pjfHyphDict_code = env->GetFieldID(pjcHyphDict, "code", "Ljava/lang/String;");
    if (nullptr == pjfHyphDict_code)
        return JNI_FALSE;
    jfieldID pjfHyphDict_type = env->GetFieldID(pjcHyphDict, "type", "I");
    if (nullptr == pjfHyphDict_type)
        return JNI_FALSE;
    jfieldID pjfHyphDict_languages = env->GetFieldID(pjcHyphDict, "languages",
                                                     "[L" PACKAGE_SIG "/LanguageItem;");
    if (nullptr == pjfHyphDict_languages)
        return JNI_FALSE;
    jfieldID pjfLanguageItem_langTag = env->GetFieldID(pjcLanguageItem, "langTag",
                                                       "Ljava/lang/String;");
    if (nullptr == pjfLanguageItem_langTag)
        return JNI_FALSE;

    int len = env->GetArrayLength(dictArray);
    HyphDictionary *dict;
    HyphDictType dict_type;
    for (int i = 0; i < len; i++) {
        jobject obj = env->GetObjectArrayElement(dictArray, i);
        int type = env->GetIntField(obj, pjfHyphDict_type);
        auto code = (jstring) env->GetObjectField(obj, pjfHyphDict_code);
        auto languages = (jobjectArray) env->GetObjectField(obj, pjfHyphDict_languages);
        auto languages_size = env->GetArrayLength(languages);
        lString32 dict_langTags;
        for (int j = 0; j < languages_size; j++) {
            jobject lang = env->GetObjectArrayElement(languages, j);
            auto jLangTag = (jstring) env->GetObjectField(lang, pjfLanguageItem_langTag);
            lString32 langTag = env.fromJavaString(jLangTag);
            if (dict_langTags.empty())
                dict_langTags = langTag;
            else
                dict_langTags = dict_langTags + "," + langTag;
            env->DeleteLocalRef(lang);
        }
        switch (type) {     // convert HyphenationDict.m_type into HyphDictType
            case 0:         // HyphenationDict$HYPH_NONE
                dict_type = HDT_NONE;
                break;
            case 1:         // HyphenationDict$HYPH_ALGO
                dict_type = HDT_ALGORITHM;
                break;
            case 3:         // HyphenationDict$HYPH_DICT
                dict_type = HDT_DICT_TEX;
                break;
            default:
                dict_type = HDT_NONE;
                break;
        }
        lString32 dict_code = env.fromJavaString(code);
        dict = new HyphDictionary(dict_type, dict_code, dict_code, dict_langTags, dict_code);
        if (!HyphMan::addDictionaryItem(dict))
            delete dict;
        env->DeleteLocalRef(obj);
    }
    JavaVM *jvm;
    env->GetJavaVM(&jvm);
    HyphMan::setDataLoader(new HyphDataLoaderProxy(jvm, creEnv));
    return JNI_TRUE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    initFontsFromDir_native
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_initFontsFromDir_1native(
        JNIEnv *env_, jobject, jstring jdir) {
    CRJNIEnv env(env_);
    return InitFontsFromDir(env.fromJavaString(jdir)) ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getBookInfo_native
 * Signature: (Ljava/lang/String;)Lio/gitlab/coolreader_ng/project_s/BookInfo;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookInfo_1native(
        JNIEnv *env_, jobject, jstring jpath) {
    CRJNIEnv env(env_);
    // open document (only metadata)
    auto *doc = new LVDocView(32, true);
    lString32 filePath = env.fromJavaString(jpath);
    bool res = doc->LoadDocument(filePath.c_str(), true);
    jobject jbookinfo = nullptr;
    if (res) {
        // Create BookInfo instance
        jclass pjc_BookInfo = env->FindClass(PACKAGE_SIG "/BookInfo");
        jmethodID pjm_BookInfo_Ctor = env->GetMethodID(pjc_BookInfo, "<init>",
                                                       "(Ljava/lang/String;)V");
        jbookinfo = env->NewObject(pjc_BookInfo, pjm_BookInfo_Ctor, jpath);
        // Get metadata & set to object
        if (env.updateBookInfo(jbookinfo, doc, false)) {
            jbookinfo = env->NewGlobalRef(jbookinfo);
        } else {
            env->DeleteLocalRef(jbookinfo);
            jbookinfo = nullptr;
        }
    }
    delete doc;
    return jbookinfo;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getBookCoverData_native
 * Signature: (Ljava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookCoverData_1native(
        JNIEnv *env_, jobject, jstring jFilePath) {
    CRJNIEnv env(env_);
    // open document (only metadata)
    jbyteArray array = nullptr;
    auto *view = new LVDocView(32, true);
    lString32 filePath = env.fromJavaString(jFilePath);
    bool res = view->LoadDocument(filePath.c_str(), true);
    if (res) {
        // Get cover image
        LVStreamRef imgStream = view->getBookCoverImageStream();
        if (!imgStream.isNull())
            array = env.streamToJByteArray(imgStream);
    }
    delete view;
    return array;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    drawBookCover_native
 * Signature: ([BIIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_drawBookCover_1native(
        JNIEnv *env_,
        jobject,
        jbyteArray jdata,
        jint maxWidth,
        jint maxHeight,
        jboolean respectAspectRatio,
        jstring jfontFace,
        jstring jtitle,
        jstring jauthors,
        jstring jseriesName,
        jint seriesNumber,
        jint bpp) {
    CRJNIEnv env(env_);
    CRLog::debug("drawBookCover_native() called");
    lString8 fontFace = UnicodeToUtf8(env.fromJavaString(jfontFace));
    lString32 title = env.fromJavaString(jtitle);
    lString32 authors = env.fromJavaString(jauthors);
    lString32 seriesName = env.fromJavaString(jseriesName);
    LVStreamRef stream;

    // Create image
    LVImageSourceRef image;
    int width;
    int height;
    if (jdata != nullptr && env->GetArrayLength(jdata) > 0) {
        CRLog::debug("drawBookCover_native(): cover image from array");
        stream = env.jbyteArrayToStream(jdata);
        if (!stream.isNull()) {
            image = LVCreateStreamImageSource(stream);
            width = image->GetWidth();
            height = image->GetHeight();
        }
    }
    if (image.isNull()) {
        // TODO: move to function arguments
        width = 300;
        height = 450;
    }

    // Limit size with max size
    if (respectAspectRatio) {
        if (width > maxWidth || height > maxHeight) {
            // TODO: rewrite without floating point calculations
            double k;
            if (maxWidth < width)
                k = ((double) maxWidth) / (double) width;
            else
                k = ((double) maxHeight) / (double) height;
            width = (int) (((double) width) * k);
            height = (int) (((double) height) * k);
        }
    } else {
        if (width > maxWidth)
            width = maxWidth;
        if (height > maxHeight)
            height = maxHeight;
    }

    // Create Bitmap object
    jclass pjcBitmap = env->FindClass("android/graphics/Bitmap");
    if (nullptr == pjcBitmap)
        return nullptr;
    jmethodID pjcBitmap_createBitmap = env_->GetStaticMethodID(pjcBitmap, "createBitmap",
                                                               "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    if (nullptr == pjcBitmap_createBitmap)
        return nullptr;
    jmethodID pjcBitmap_recycle = env_->GetMethodID(pjcBitmap, "recycle",
                                                    "()V");
    if (nullptr == pjcBitmap_recycle)
        return nullptr;
    jclass pjc_BitmapConfig = env_->FindClass("android/graphics/Bitmap$Config");
    if (nullptr == pjc_BitmapConfig)
        return nullptr;
    jfieldID pjf_RGB_565 = env_->GetStaticFieldID(pjc_BitmapConfig, "RGB_565",
                                                  "Landroid/graphics/Bitmap$Config;");
    jfieldID pjf_ARGB_8888 = env_->GetStaticFieldID(pjc_BitmapConfig, "ARGB_8888",
                                                    "Landroid/graphics/Bitmap$Config;");
    jobject bitmap_config = env_->GetStaticObjectField(pjc_BitmapConfig,
                                                       (bpp == 32) ? pjf_ARGB_8888
                                                                   : pjf_RGB_565);
    jobject jbitmap = env->CallStaticObjectMethod(pjcBitmap, pjcBitmap_createBitmap, width, height,
                                                  bitmap_config);
    if (nullptr != jbitmap) {
        LVDrawBuf *drawbuf = BitmapAccessorInterface::getInstance()->lock(env_, jbitmap);
        if (drawbuf != nullptr) {
            if (bpp >= 16) {
                // native color resolution
                CRLog::debug("drawBookCover_native(): calling LVDrawBookCover");
                LVDrawBookCover(*drawbuf, image, respectAspectRatio, fontFace, title, authors,
                                seriesName, seriesNumber);
                image.Clear();
            } else {
                LVGrayDrawBuf grayBuf(drawbuf->GetWidth(), drawbuf->GetHeight(), bpp);
                LVDrawBookCover(grayBuf, image, respectAspectRatio, fontFace, title, authors,
                                seriesName, seriesNumber);
                image.Clear();
                grayBuf.DrawTo(drawbuf, 0, 0, 0, nullptr);
            }
            BitmapAccessorInterface::getInstance()->unlock(env_, jbitmap, drawbuf);
        } else {
            CRLog::error("jbitmap accessor is invalid");
            env->CallVoidMethod(jbitmap, pjcBitmap_recycle);
            jbitmap = nullptr;
        }
    } else {
        CRLog::error("Failed to build Bitmap object!");
    }
    CRLog::debug("drawBookCover_native() finished");
    return jbitmap;
}


/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getArchiveItems_native
 * Signature: (Ljava/lang/String;)[Lio/gitlab/coolreader_ng/project_s/Triplet;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getArchiveItems_1native(
        JNIEnv *env_, jclass, jstring jarcName) {
    CRJNIEnv env(env_);
    lString32 arcName = env.fromJavaString(jarcName);
    jobjectArray jarray = nullptr;

    jclass pjcTriplet = env->FindClass(PACKAGE_SIG "/Triplet");
    if (nullptr == pjcTriplet)
        return nullptr;
    // Getting Triplet<String, Long, Long>(String, Long, Long) constructor method id
    // Note: the generics are erased in JNI signature (replaced with java.lang.Object)
    jmethodID pjmTriplet_Ctor = env->GetMethodID(pjcTriplet, "<init>",
                                                 "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V");
    if (nullptr == pjmTriplet_Ctor)
        return nullptr;

    LVStreamRef stream = LVOpenFileStream(arcName.c_str(), LVOM_READ);
    if (!stream.isNull()) {
        LVContainerRef arc = LVOpenArchieve(stream);
        if (!arc.isNull()) {
            jarray = env->NewObjectArray(arc->GetObjectCount(), pjcTriplet, nullptr);
            if (nullptr != jarray) {
                // convert
                for (int i = 0; i < arc->GetObjectCount(); i++) {
                    const LVContainerItemInfo *item = arc->GetObjectInfo(i);
                    //if ( item->IsContainer())
                    //    continue;
                    if (nullptr != item && item->GetName()) {
                        lString32 fileName = item->GetName();
                        jstring jFileName = env.toJavaString(fileName);
                        jobject jSize = env.toJavaLong((lInt64) item->GetSize());
                        jobject jPackSize = env.toJavaLong((lInt64) item->GetPackSize());
                        if (nullptr != jFileName) {
                            // TODO: uncomment exceptions handling after testing...
                            //env->ExceptionClear();
                            jobject jtriplet = env->NewObject(
                                    pjcTriplet,
                                    pjmTriplet_Ctor,
                                    jFileName,
                                    jSize,
                                    jPackSize
                            );
                            //if (env->ExceptionCheck() == JNI_TRUE)
                            //    env->ExceptionClear();
                            //else {
                            if (nullptr != jtriplet) {
                                env->SetObjectArrayElement(jarray, i, jtriplet);
                                env->DeleteLocalRef(jtriplet);
                            }
                            //}
                            env->DeleteLocalRef(jFileName);
                            env->DeleteLocalRef(jSize);
                            env->DeleteLocalRef(jPackSize);
                        }
                    }
                }
            }
        }
    }
    return jarray;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getFontFaceList_native
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceList_1native(
        JNIEnv *env_, jobject) {
    CRJNIEnv env(env_);
    lString32Collection faceList;
    fontMan->getFaceList(faceList);
    return env.toJavaStringArray(faceList);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getFontFaceListFiltered_native
 * Signature: (Lio/gitlab/coolreader_ng/project_s/CREngineNGBinding$FontFamily;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceListFiltered_1native(
        JNIEnv *env_, jobject, jobject jFontFamily, jstring jLangTag) {
    CRJNIEnv env(env_);
    jclass pjcFontFamily = env->GetObjectClass(jFontFamily);
    if (nullptr == pjcFontFamily)
        return nullptr;
    jfieldID pjfFontFamily_code = env->GetFieldID(pjcFontFamily, "code", "I");
    jint familyCode = env->GetIntField(jFontFamily, pjfFontFamily_code);
    css_font_family_t family;
    switch (familyCode) {
        case 0: // CREngineNGBinding$FontFamily.ANY
            family = css_ff_inherit;
            break;
        case 1: // CREngineNGBinding$FontFamily.SERIF
            family = css_ff_serif;
            break;
        case 2: // CREngineNGBinding$FontFamily.SANS_SERIF
            family = css_ff_sans_serif;
            break;
        case 3: // CREngineNGBinding$FontFamily.CURSIVE
            family = css_ff_cursive;
            break;
        case 4: // CREngineNGBinding$FontFamily.FANTASY
            family = css_ff_fantasy;
            break;
        case 5: // CREngineNGBinding$FontFamily.MONOSPACE
            family = css_ff_monospace;
            break;
        default:
            // invalid
            family = css_ff_inherit;
    }
    lString8 langTag = UnicodeToLocal(env.fromJavaString(jLangTag));
    lString32Collection faceList;
    if (css_ff_inherit == family) {
        // Assume 'any' font family
        if (langTag.empty()) {
            fontMan->getFaceList(faceList);
        } else {
#if USE_LOCALE_DATA == 1
            CRLocaleData loc(langTag);
            if (loc.isValid()) {
                lString32Collection tmpList;
                fontMan->getFaceList(tmpList);
                for (int i = 0; i < tmpList.length(); i++) {
                    if (font_lang_compat_full ==
                        fontMan->checkFontLangCompat(UnicodeToUtf8(tmpList[i]), langTag))
                        faceList.add(tmpList[i]);
                }
            }
#else
            fontMan->getFaceList(faceList);
#endif
        }
    } else {
        fontMan->getFaceListFiltered(faceList, family, langTag);
    }
    return env.toJavaStringArray(faceList);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getDOMVersionCurrent_native
 * Signature: ()I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getDOMVersionCurrent_1native(
        JNIEnv *, jclass) {
    return gDOMVersionCurrent;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    hyphenateWord_native
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_hyphenateWord_1native(
        JNIEnv *env_, jclass, jstring jLangTag, jstring jWord) {
    CRJNIEnv env(env_);
    lString32 langTag = env.fromJavaString(jLangTag);
    lString32 word = env.fromJavaString(jWord);
    HyphMethod *method = HyphMan::getHyphMethodForLang(langTag);
    if (nullptr != method && method->getPatternsCount() > 0) {
        // the hyphenation method is a patterns dictionary and is not empty
#define MAX_WORD_SIZE 64
        lUInt16 widths[MAX_WORD_SIZE + 1];
        lUInt16 flags[MAX_WORD_SIZE + 1];
        const lUInt16 hyphCharWidth = 5;
        const lUInt16 maxWidth = 10000;
        memset(widths, 0, sizeof(widths));
        memset(flags, 0, sizeof(flags));

        int len = word.length();
        if (len <= MAX_WORD_SIZE) {
            for (int i = 0; i < len; i++) {
                widths[i] = 5;
                flags[i] = 0;
            }
            if (method->hyphenate(word.c_str(), len, widths, (lUInt8 *) flags, hyphCharWidth,
                                  maxWidth, 2)) {
                lChar32 hyphenated[MAX_WORD_SIZE * 2 + 1];
                memset(hyphenated, 0, sizeof(hyphenated));
                int idx = 0;
                for (int i = 0; i < len; i++) {
                    hyphenated[idx] = word[i];
                    idx++;
                    if (flags[i] & LCHAR_ALLOW_HYPH_WRAP_AFTER) {
                        hyphenated[idx] = UNICODE_SOFT_HYPHEN_CODE;
                        idx++;
                    }
                }
                hyphenated[idx] = 0;
                return env.toJavaString(hyphenated);
            }
            return env.toJavaString(word);
        }
    }
    return nullptr;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getAvailableFontWeight_native
 * Signature: (Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableFontWeight_1native(
        JNIEnv *env_, jclass clazz, jstring font_face) {
    CRJNIEnv env(env_);
    lString32 fontFace32 = env.fromJavaString(font_face);
    LVArray<int> weights;
    fontMan->GetAvailableFontWeights(weights, UnicodeToUtf8(fontFace32));
    int len = weights.length();
    jintArray array = env->NewIntArray(len);
    if (nullptr != array)
        env->SetIntArrayRegion(array, 0, len, weights.get());
    return array;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getAvailableSynthFontWeight_native
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableSynthFontWeight_1native(
        JNIEnv *env_, jclass clazz) {
    CRJNIEnv env(env_);
#if USE_FT_EMBOLDEN
    int available_synth_weight[] = {
            300, 400, 450, 500, 550, 600, 700, 800, 900
    };
#else
    int available_synth_weight[] = {
            600
    };
#endif
    int len = sizeof(available_synth_weight) / sizeof(int);
    jintArray array = env->NewIntArray(len);
    if (nullptr != array)
        env->SetIntArrayRegion(array, 0, len, available_synth_weight);
    return array;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getHumanReadableLocaleName_native
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getHumanReadableLocaleName_1native(
        JNIEnv *env_, jclass, jstring lang_tag) {
    CRJNIEnv env(env_);
    lString32 res;
#if USE_LOCALE_DATA == 1
    CRLocaleData loc(UnicodeToUtf8(env.fromJavaString(lang_tag)));
    if (loc.isValid()) {
        res = loc.langName().c_str();
        if (loc.scriptNumeric() > 0) {
            res.append("-");
            res.append(loc.scriptName().c_str());
        }
        if (loc.regionNumeric() > 0) {
            res.append(" (");
            res.append(loc.regionAlpha3().c_str());
            res.append(")");
        }
    }
#else
    res = cs32("Undetermined");
#endif
    return env.toJavaString(res);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    listFiles_native
 * Signature: (Ljava/io/File;)[Ljava/io/File;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_listFiles_1native(
        JNIEnv *env_, jobject, jobject jdir) {
    CRJNIEnv env(env_);
    if (nullptr == jdir)
        return nullptr;
    jclass pjcFile = env->GetObjectClass(jdir);
    if (nullptr == pjcFile)
        return nullptr;
    jmethodID pjmFile_GetAbsolutePath = env->GetMethodID(pjcFile, "getAbsolutePath",
                                                         "()Ljava/lang/String;");
    if (nullptr == pjmFile_GetAbsolutePath)
        return nullptr;
    jmethodID pjmFile_ctor = env->GetMethodID(pjcFile, "<init>", "(Ljava/lang/String;)V");
    if (nullptr == pjmFile_ctor)
        return nullptr;
    auto jpathname = (jstring) env->CallObjectMethod(jdir, pjmFile_GetAbsolutePath);
    if (nullptr == jpathname)
        return nullptr;
    lString32 path = env.fromJavaString(jpathname);
    jobjectArray jarray = nullptr;
    LVContainerRef dir = LVOpenDirectory(path);
    if (!dir.isNull()) {
        jstring emptyString = env->NewStringUTF("");
        jobject emptyFile = env->NewObject(pjcFile, pjmFile_ctor, emptyString);
        jarray = env->NewObjectArray(dir->GetObjectCount(), pjcFile, emptyFile);
        if (nullptr != jarray) {
            for (int i = 0; i < dir->GetObjectCount(); i++) {
                const LVContainerItemInfo *item = dir->GetObjectInfo(i);
                if (item && item->GetName()) {
                    lString32 fileName = path + "/" + item->GetName();
                    jstring jfilename = env.toJavaString(fileName);
                    if (nullptr != jfilename) {
                        env->ExceptionClear();
                        jobject jfile = env->NewObject(pjcFile, pjmFile_ctor, jfilename);
                        if (JNI_TRUE == env->ExceptionCheck())
                            env->ExceptionClear();
                        else {
                            if (nullptr != jfile)
                                env->SetObjectArrayElement(jarray, i, jfile);
                        }
                        env->DeleteLocalRef(jfile);
                        env->DeleteLocalRef(jfilename);
                    }
                }
            }
        }
    }
    return jarray;
}

#ifdef __cplusplus
}
#endif

bool InitFontsFromDir(const lString32 &fontDir) {
    CRLog::trace("InitFontsFromDir()");
    // Load font definitions into font manager
#if USE_FREETYPE == 1
    lString32Collection fontExt;
    fontExt.add(cs32(".ttf"));
    fontExt.add(cs32(".ttc"));
    fontExt.add(cs32(".otf"));
    fontExt.add(cs32(".pfa"));
    fontExt.add(cs32(".pfb"));

    lString32Collection fonts;
    getDirectoryFonts(fontDir, fontExt, fonts, true, true);

    // load fonts from file
    CRLog::debug("%d font files found in '%s'", fonts.length(), LCSTR(fontDir));
    for (int fi = 0; fi < fonts.length(); fi++) {
        lString8 fn = UnicodeToLocal(fonts[fi]);
        CRLog::trace("loading font: %s", fn.c_str());
        if (!fontMan->RegisterFont(fn)) {
            CRLog::trace("    failed\n");
        }
    }
#endif // USE_FREETYPE==1
    if (!fontMan->GetFontCount()) {
        //error
#if (USE_FREETYPE == 1)
        CRLog::error("Fatal Error: Cannot open font file(s) .ttf \nCannot work without font");
#else
        CRLog::error("Fatal Error: Cannot open font file(s) font#.lbf \nCannot work without font\nUse FontConv utility to generate .lbf fonts from TTF\n");
#endif
        return false;
    }
    CRLog::info("%d fonts loaded.\n", fontMan->GetFontCount());
    return true;
}

#if USE_FREETYPE == 1

bool getDirectoryFonts(const lString32 &fontDir, lString32Collection &ext,
                       lString32Collection &fonts, bool absPath, bool recursively) {
    int foundCount = 0;
    LVContainerRef dir = LVOpenDirectory(fontDir.c_str());
    if (!dir.isNull()) {
        CRLog::trace("Checking directory %s", UnicodeToUtf8(fontDir).c_str());
        for (int i = 0; i < dir->GetObjectCount(); i++) {
            const LVContainerItemInfo *item = dir->GetObjectInfo(i);
            //lString8 fn = UnicodeToLocal(fileName);
            //printf(" test(%s) ", fn.c_str() );
            if (item->IsContainer()) {
                if (recursively) {
                    lString32 childDir = fontDir;
                    childDir = LVCombinePaths(childDir, item->GetName());
                    getDirectoryFonts(childDir, ext, fonts, absPath, recursively);
                }
            } else {
                lString32 fileName = item->GetName();
                bool found = false;
                lString32 lc = fileName;
                lc.lowercase();
                for (int j = 0; j < ext.length(); j++) {
                    if (lc.endsWith(ext[j])) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    continue;
                lString32 fn;
                if (absPath) {
                    fn = fontDir;
                    LVAppendPathDelimiter(fn);
                }
                fn << fileName;
                foundCount++;
                fonts.add(fn);
            }
        }
    }
    return foundCount > 0;
}

void myFatalErrorHandler(int errorCode, const char *errorText) {
    __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, "program fatal error #%d: %s", errorCode,
                        errorText);
    __android_log_assert("Fatal Error", LOG_TAG, "program fatal error #%d: %s", errorCode,
                         errorText);
}

#endif // USE_FREETYPE == 1
