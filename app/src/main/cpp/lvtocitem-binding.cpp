/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "lvtocitem-binding.h"
#include "crengine-ng-jni.h"

#include <lvtocitem.h>

LVTocItemBinding::LVTocItemBinding(CRJNIEnv &env)
        : m_env(env) {
    m_clazz = env->FindClass(PACKAGE_SIG "/DocumentTOCItem");
    m_methodID_ctor = m_env->GetMethodID(m_clazz, "<init>", "()V");
    m_methodID_addEmptyChild = m_env->GetMethodID(m_clazz, "addEmptyChild",
                                                  "()L" PACKAGE_SIG "/DocumentTOCItem;");
    m_fieldID_level = m_env->GetFieldID(m_clazz, "level", "I");
    m_fieldID_page = m_env->GetFieldID(m_clazz, "page", "I");
    m_fieldID_percent = m_env->GetFieldID(m_clazz, "percent", "I");
    m_fieldID_name = m_env->GetFieldID(m_clazz, "name", "Ljava/lang/String;");
    m_fieldID_path = m_env->GetFieldID(m_clazz, "path", "Ljava/lang/String;");
}

void LVTocItemBinding::set(jobject obj, LVTocItem *item) {
    m_env->SetIntField(obj, m_fieldID_level, item->getLevel());
    m_env->SetIntField(obj, m_fieldID_page, item->getPage());
    m_env->SetIntField(obj, m_fieldID_percent, item->getPercent());
    jstring str1 = m_env.toJavaString(item->getName());
    m_env->SetObjectField(obj, m_fieldID_name, str1);
    m_env->DeleteLocalRef(str1);
    jstring str2 = m_env.toJavaString(item->getPath());
    m_env->SetObjectField(obj, m_fieldID_path, str2);
    m_env->DeleteLocalRef(str2);
}

void LVTocItemBinding::add(jobject obj, LVTocItem *child) {
    jobject jchild = m_env->CallObjectMethod(obj, m_methodID_addEmptyChild);
    set(jchild, child);
    for (int i = 0; i < child->getChildCount(); i++) {
        add(jchild, child->getChild(i));
    }
    m_env->DeleteLocalRef(jchild);
}

jobject LVTocItemBinding::newJavaObject() {
    return m_env->NewObject(m_clazz, m_methodID_ctor);
}
