/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#ifndef LVDOCVIEW_WRAPPER_PRIVATE_H
#define LVDOCVIEW_WRAPPER_PRIVATE_H

#include <lvdocview.h>
#include <crprops.h>
#include <crtimerutil.h>

class LVDocViewWrapperPrivate {
public:
    LVDocViewWrapperPrivate();

    virtual ~LVDocViewWrapperPrivate();

    LVDocView *getDocView() {
        return m_doc;
    }

    CRPropRef getProps() {
        return m_props;
    }

    lString32 getLink(int x, int y);

    lString32 getLink(int x, int y, int r);

    bool closeBook();

    int processCommand(LVDocCmd command, int param);

    void updateBatteryIcons();

    ContinuousOperationResult updateCache(long timeout);

    void cancelLongCachingOperation();

    bool isForcedScrollViewMode() {
        return m_forcedScrollViewMode;
    }

    bool setForceScrollViewMode(bool value);

    void setPendingViewMode(LVDocViewMode mode) {
        m_pendingViewMode = mode;
    }

private:
    LVDocView *m_doc;
    CRPropRef m_props;
    lUInt32 m_batteryIconColor;
    int m_batteryIconSize;
    CRTimerUtil m_timeoutControl;
    bool m_forcedScrollViewMode;
    LVDocViewMode m_pendingViewMode;
};

#endif // LVDOCVIEW_WRAPPER_PRIVATE_H
