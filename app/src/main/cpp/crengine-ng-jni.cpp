/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

#include <jni.h>

#include "crengine-ng-jni.h"
#include "crengine-ng-binding.h"
#include "lvdocview_wrapper.h"

JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    // Most likely all actual versions of Android support JNI 1.6 or higher.
    // https://developer.android.com/training/articles/perf-jni.html#unsupported-featuresbackwards-compatibility
    // Android API 4 (Android 1.6) already support JNI 1.6
    // https://android.googlesource.com/platform/dalvik.git/+/refs/tags/android-1.6_r1/vm/Jni.c
    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR;
    }

    // Registering native methods is not required, but is recommended for
    // "get up-front checking that the symbols exist" and for building
    // "smaller and faster shared libraries".
    // See https://developer.android.com/training/articles/perf-jni#native-libraries

    // Find CREngineNGBinding class. JNI_OnLoad is called from the correct class loader context for this to work.
    jclass clazz1 = env->FindClass(PACKAGE_SIG "/CREngineNGBinding");
    if (clazz1 == nullptr)
        return JNI_ERR;
    // Find LVDocViewWrapper class. JNI_OnLoad is called from the correct class loader context for this to work.
    jclass clazz2 = env->FindClass(PACKAGE_SIG "/LVDocViewWrapper");
    if (clazz2 == nullptr)
        return JNI_ERR;

    // CREngineNGBinding class' native methods.
    static const JNINativeMethod methods1[] = {
            {"engineInit_native",                  "(I)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineInit_1native)},
            {"engineCleanup_native",               "()V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineCleanup_1native)},
            {"setCacheDir_native",                 "(Ljava/lang/String;J)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setCacheDir_1native)},
            {"setupHyphenations_native",           "([L" PACKAGE_SIG "/HyphenationDict;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setupHyphenations_1native)},
            {"initFontsFromDir_native",            "(Ljava/lang/String;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_initFontsFromDir_1native)},
            {"getBookInfo_native",                 "(Ljava/lang/String;)L" PACKAGE_SIG "/BookInfo;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookInfo_1native)},
            {"getBookCoverData_native",            "(Ljava/lang/String;)[B",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookCoverData_1native)},
            {"drawBookCover_native",               "([BIIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_drawBookCover_1native)},
            {"getArchiveItems_native",             "(Ljava/lang/String;)[L" PACKAGE_SIG "/Triplet;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getArchiveItems_1native)},
            {"getFontFaceList_native",             "()[Ljava/lang/String;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceList_1native)},
            {"getFontFaceListFiltered_native",     "(L" PACKAGE_SIG "/CREngineNGBinding$FontFamily;Ljava/lang/String;)[Ljava/lang/String;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceListFiltered_1native)},
            {"getDOMVersionCurrent_native",        "()I",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getDOMVersionCurrent_1native)},
            {"hyphenateWord_native",               "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_hyphenateWord_1native)},
            {"getAvailableFontWeight_native",      "(Ljava/lang/String;)[I",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableFontWeight_1native)},
            {"getAvailableSynthFontWeight_native", "()[I",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableSynthFontWeight_1native)},
            {"getHumanReadableLocaleName_native",  "(Ljava/lang/String;)Ljava/lang/String;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getHumanReadableLocaleName_1native)},
            {"listFiles_native",                   "(Ljava/io/File;)[Ljava/io/File;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_listFiles_1native)},
    };
    // LVDocViewWrapper class' native methods.
    static const JNINativeMethod methods2[] = {
            {"newLVDocView_native",               "()J",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_newLVDocView_1native)},
            {"deleteLVDocView_native",            "(J)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_deleteLVDocView_1native)},
            {"loadDocument_native",               "(JLjava/lang/String;Z)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocument_1native)},
            {"loadDocumentBuff_native",           "(J[BLjava/lang/String;Z)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocumentBuff_1native)},
            {"renderPageBitmap_native",           "(JLandroid/graphics/Bitmap;B)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_renderPageBitmap_1native)},
            {"resize_native",                     "(JII)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_resize_1native)},
            {"processCommand_native",             "(JII)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_processCommand_1native)},
            {"getCurrentPageBookmark_native",     "(J)L" PACKAGE_SIG "/Bookmark;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getCurrentPageBookmark_1native)},
            {"getPositionProps_native",           "(JLjava/lang/String;Z)L" PACKAGE_SIG "/PositionProperties;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getPositionProps_1native)},
            {"getAvgTextLineHeight_native",       "(J)I",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getAvgTextLineHeight_1native)},
            {"checkLink_native",                  "(JIII)Ljava/lang/String;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkLink_1native)},
            {"checkBookmark_native",              "(JIIL" PACKAGE_SIG "/Bookmark;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkBookmark_1native)},
            {"getEmbeddedImage_native",           "(JIIL" PACKAGE_SIG "/PageImageCache;B)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getEmbeddedImage_1native)},
            {"goLink_native",                     "(JLjava/lang/String;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goLink_1native)},
            {"goToPosition_native",               "(JLjava/lang/String;Z)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goToPosition_1native)},
            {"getSettings_native",                "(JLjava/util/Properties;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSettings_1native)},
            {"applySettings_native",              "(JLjava/util/Properties;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applySettings_1native)},
            {"setBatteryState_native",            "(JIII)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBatteryState_1native)},
            {"isTimeChanged_native",              "(J)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_isTimeChanged_1native)},
            {"updateCache_native",                "(JJ)I",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateCache_1native)},
            {"cancelLongCachingOperation_native", "(J)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_cancelLongCachingOperation_1native)},
            {"updateBookInfo_native",             "(JL" PACKAGE_SIG "/BookInfo;Z)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateBookInfo_1native)},
            {"createDefaultDocument_native",      "(JLjava/lang/String;Ljava/lang/String;)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_createDefaultDocument_1native)},
            {"setPageBackgroundTexture_native",   "(J[BZ)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setPageBackgroundTexture_1native)},
            {"getTOC_native",                     "(J)L" PACKAGE_SIG "/DocumentTOCItem;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getTOC_1native)},
            {"canGoBack_native",                  "(J)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_canGoBack_1native)},
            {"getBookCoverData_native",           "(J)[B",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getBookCoverData_1native)},
            {"getSelection_native",               "(JL" PACKAGE_SIG "/Selection;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSelection_1native)},
            {"selectText_native",                 "(JIIIIZ)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectText_1native)},
            {"clearSelection_native",             "(J)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_clearSelection_1native)},
            {"docToWindowPoint_native",           "(JLandroid/graphics/Point;Landroid/graphics/Point;)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_docToWindowPoint_1native)},
            {"setBookmarks_native",               "(J[Lio/gitlab/coolreader_ng/project_s/Bookmark;)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBookmarks_1native)},
            {"setForceScrollViewMode_native",     "(JZ)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setForceScrollViewMode_1native)},
            {"setDecimalPointChar_native",        "(JC)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setDecimalPointChar_1native)},
            {"applyPageInsets_native",            "(JIIIIZ)V",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applyPageInsets_1native)},
            {"findTextAll_native",                "(JLjava/lang/String;ZI)[Lio/gitlab/coolreader_ng/project_s/SearchResultItem;",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_findTextAll_1native)},
            {"selectSearchResult_native",         "(JLio/gitlab/coolreader_ng/project_s/SearchResultItem;)Z",
                    reinterpret_cast<void *>(Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectSearchResult_1native)},
    };
    // Register CREngineNGBinding class' native methods.
    int ret = env->RegisterNatives(clazz1, methods1, sizeof(methods1) / sizeof(JNINativeMethod));
    if (ret != JNI_OK)
        return ret;
    // Register LVDocViewWrapper class' native methods.
    ret = env->RegisterNatives(clazz2, methods2, sizeof(methods2) / sizeof(JNINativeMethod));
    if (ret != JNI_OK)
        return ret;

    return JNI_VERSION_1_6;
}