/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "lvdocviewcallback-jnibridge.h"
#include "crengine-ng-jni.h"

#include <lvdocview.h>

LVDocViewCallbackJNIBridge::LVDocViewCallbackJNIBridge(JNIEnv *env, LVDocView *docview,
                                                       jobject jdocview)
        : m_env(env), m_docview(docview) {
    jclass objclass = m_env->GetObjectClass(jdocview);  // LVDocViewWrapper
    jfieldID fid = m_env->GetFieldID(objclass, "jni_docviewCallback",
                                     "L" PACKAGE_SIG "/DocViewCallback;");
    m_callbackObj = m_env->GetObjectField(jdocview, fid);
    if (nullptr != m_callbackObj) {
        jclass callback_class = m_env->GetObjectClass(m_callbackObj);
        m_methodID_onLoadFileStart = m_env->GetMethodID(callback_class, "onLoadFileStart",
                                                        "(Ljava/lang/String;)V");
        m_methodID_onLoadFileFormatDetected = m_env->GetMethodID(callback_class,
                                                                 "onLoadFileFormatDetected",
                                                                 "(L" PACKAGE_SIG "/DocumentFormat;)Ljava/lang/String;");
        m_methodID_onLoadFileEnd = m_env->GetMethodID(callback_class, "onLoadFileEnd", "()V");
        m_methodID_onLoadFileFirstPagesReady = m_env->GetMethodID(callback_class,
                                                                  "onLoadFileFirstPagesReady",
                                                                  "()V");
        m_methodID_onLoadFileProgress = m_env->GetMethodID(callback_class, "onLoadFileProgress",
                                                           "(I)V");
        m_methodID_onFormatStart = m_env->GetMethodID(callback_class, "onFormatStart", "()V");
        m_methodID_onFormatEnd = m_env->GetMethodID(callback_class, "onFormatEnd", "()V");
        m_methodID_onFormatProgress = m_env->GetMethodID(callback_class, "onFormatProgress",
                                                         "(I)V");
        m_methodID_onExportProgress = m_env->GetMethodID(callback_class, "onExportProgress",
                                                         "(I)V");
        m_methodID_onRequestReload = m_env->GetMethodID(callback_class, "onRequestReload", "()Z");
        m_methodID_onLoadFileError = m_env->GetMethodID(callback_class, "onLoadFileError",
                                                        "(Ljava/lang/String;)V");
        m_methodID_onExternalLink = m_env->GetMethodID(callback_class, "onExternalLink",
                                                       "(Ljava/lang/String;Ljava/lang/String;)V");
        m_methodID_onImageCacheClear = m_env->GetMethodID(callback_class, "onImageCacheClear",
                                                          "()V");
        m_oldCallback = m_docview->setCallback(this);
        m_callbackIsSet = true;
    } else {
        m_methodID_onLoadFileStart = nullptr;
        m_methodID_onLoadFileFormatDetected = nullptr;
        m_methodID_onLoadFileEnd = nullptr;
        m_methodID_onLoadFileFirstPagesReady = nullptr;
        m_methodID_onLoadFileProgress = nullptr;
        m_methodID_onFormatStart = nullptr;
        m_methodID_onFormatEnd = nullptr;
        m_methodID_onFormatProgress = nullptr;
        m_methodID_onExportProgress = nullptr;
        m_methodID_onRequestReload = nullptr;
        m_methodID_onLoadFileError = nullptr;
        m_methodID_onExternalLink = nullptr;
        m_methodID_onImageCacheClear = nullptr;
        m_oldCallback = nullptr;
        m_callbackIsSet = false;
    }
}

LVDocViewCallbackJNIBridge::~LVDocViewCallbackJNIBridge() {
    if (m_callbackIsSet)
        m_docview->setCallback(m_oldCallback);
}

void LVDocViewCallbackJNIBridge::OnLoadFileStart(lString32 filename) {
    CRLog::info("DocViewCallback::OnLoadFileStart() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileStart)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onLoadFileStart,
                              m_env.toJavaString(filename));
}

void LVDocViewCallbackJNIBridge::OnLoadFileFormatDetected(doc_format_t fileFormat) {
    CRLog::info("DocViewCallback::OnLoadFileFormatDetected() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileFormatDetected) {
        jobject jFormat = nullptr;
        jclass cl = m_env->FindClass(PACKAGE_SIG "/DocumentFormat");
        if (cl) {
            jmethodID method = m_env->GetStaticMethodID(cl, "byId",
                                                        "(I)L" PACKAGE_SIG "/DocumentFormat;");
            if (method) {
                jFormat = m_env->CallStaticObjectMethod(cl, method, (jint) fileFormat);
            }
        }
        auto css = (jstring) m_env->CallObjectMethod(m_callbackObj,
                                                     m_methodID_onLoadFileFormatDetected, jFormat);
        if (nullptr != css) {
            lString32 s = m_env.fromJavaString(css);
            CRLog::info("OnLoadFileFormatDetected: setting CSS for format %d", (int) fileFormat);
            m_docview->setStyleSheet(UnicodeToUtf8(s));
        }
    }
}

void LVDocViewCallbackJNIBridge::OnLoadFileEnd() {
    CRLog::info("DocViewCallback::OnLoadFileEnd() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileEnd)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onLoadFileEnd);
}

void LVDocViewCallbackJNIBridge::OnLoadFileFirstPagesReady() {
    CRLog::info("DocViewCallback::OnLoadFileFirstPagesReady() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileFirstPagesReady)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onLoadFileFirstPagesReady);
}

void LVDocViewCallbackJNIBridge::OnLoadFileProgress(int percent) {
    CRLog::info("DocViewCallback::OnLoadFileProgress() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileProgress)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onLoadFileProgress, (jint) percent);
}

void LVDocViewCallbackJNIBridge::OnFormatStart() {
    CRLog::info("DocViewCallback::OnFormatStart() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onFormatStart)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onFormatStart);
}

void LVDocViewCallbackJNIBridge::OnFormatEnd() {
    CRLog::info("DocViewCallback::OnFormatEnd() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onFormatEnd)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onFormatEnd);
}

void LVDocViewCallbackJNIBridge::OnFormatProgress(int percent) {
    CRLog::info("DocViewCallback::OnFormatProgress() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onFormatProgress)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onFormatProgress, (jint) percent);
}

void LVDocViewCallbackJNIBridge::OnExportProgress(int percent) {
    CRLog::info("DocViewCallback::OnExportProgress() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onExportProgress)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onExportProgress, (jint) percent);
}

void LVDocViewCallbackJNIBridge::OnLoadFileError(lString32 message) {
    CRLog::info("DocViewCallback::OnLoadFileError() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onLoadFileError)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onLoadFileError,
                              m_env.toJavaString(message));
}

void LVDocViewCallbackJNIBridge::OnExternalLink(lString32 url, ldomNode *node) {
    CRLog::info("DocViewCallback::OnExternalLink() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onExternalLink) {
        lString32 path = ldomXPointer(node, 0).toString();
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onExternalLink, m_env.toJavaString(url),
                              m_env.toJavaString(path));
    }
}

void LVDocViewCallbackJNIBridge::OnImageCacheClear() {
    CRLog::info("DocViewCallback::OnImageCacheClear() called");
    if (nullptr != m_callbackObj && nullptr != m_methodID_onImageCacheClear)
        m_env->CallVoidMethod(m_callbackObj, m_methodID_onImageCacheClear);
}

bool LVDocViewCallbackJNIBridge::OnRequestReload() {
    bool res = false;
    if (nullptr != m_callbackObj && nullptr != m_methodID_onRequestReload)
        res = m_env->CallBooleanMethod(m_callbackObj, m_methodID_onRequestReload);
    return res;
}
