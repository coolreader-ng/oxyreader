/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#ifndef LVDOCVIEWCALLBACK_JNIBRIDGE_H
#define LVDOCVIEWCALLBACK_JNIBRIDGE_H

#include <lvdocviewcallback.h>

#include "crjnienv.h"

class LVDocView;

/**
 * JNI bridge for java DocViewCallback derivatives.
 *
 * Instances of this class can't be shared across multiple threads
 * as it cache JNIEnv instance:
 * https://developer.android.com/training/articles/perf-jni#javavm-and-jnienv
 */
class LVDocViewCallbackJNIBridge : public LVDocViewCallback {
public:
    LVDocViewCallbackJNIBridge(JNIEnv *env, LVDocView *docview, jobject jdocview);

    virtual ~LVDocViewCallbackJNIBridge();

    /// on starting file loading
    virtual void OnLoadFileStart(lString32 filename);

    /// format detection finished
    virtual void OnLoadFileFormatDetected(doc_format_t fileFormat);

    /// file loading is finished successfully - drawCoveTo() may be called there
    virtual void OnLoadFileEnd();

    /// first page is loaded from file an can be formatted for preview
    virtual void OnLoadFileFirstPagesReady();

    /// file progress indicator, called with values 0..100
    virtual void OnLoadFileProgress(int percent);

    /// document formatting started
    virtual void OnFormatStart();

    /// document formatting finished
    virtual void OnFormatEnd();

    /// format progress, called with values 0..100
    virtual void OnFormatProgress(int percent);

    /// format progress, called with values 0..100
    virtual void OnExportProgress(int percent);

    /// file load finiished with error
    virtual void OnLoadFileError(lString32 message);

    /// Override to handle external links
    virtual void OnExternalLink(lString32 url, ldomNode *node);

    virtual void OnImageCacheClear();

    /// return true if reload will be processed by external code, false to let internal code process it
    virtual bool OnRequestReload();

private:
    CRJNIEnv m_env;
    LVDocView *m_docview;
    LVDocViewCallback *m_oldCallback;
    bool m_callbackIsSet;
    jobject m_callbackObj;
    jmethodID m_methodID_onLoadFileStart;
    jmethodID m_methodID_onLoadFileFormatDetected;
    jmethodID m_methodID_onLoadFileEnd;
    jmethodID m_methodID_onLoadFileFirstPagesReady;
    jmethodID m_methodID_onLoadFileProgress;
    jmethodID m_methodID_onFormatStart;
    jmethodID m_methodID_onFormatEnd;
    jmethodID m_methodID_onFormatProgress;
    jmethodID m_methodID_onExportProgress;
    jmethodID m_methodID_onLoadFileError;
    jmethodID m_methodID_onExternalLink;
    jmethodID m_methodID_onImageCacheClear;
    jmethodID m_methodID_onRequestReload;
};

#endif  // LVDOCVIEWCALLBACK_JNIBRIDGE_H
