/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#ifndef LVTOCITEM_BINDING_H
#define LVTOCITEM_BINDING_H

#include <jni.h>
#include "crjnienv.h"

/**
 * Binding class between `LVTocItem` and `DocumentTOCItem`
 */
class LVTocItemBinding {
    CRJNIEnv &m_env;
    jclass m_clazz;
    jmethodID m_methodID_ctor;
    jmethodID m_methodID_addEmptyChild;
    jfieldID m_fieldID_level;
    jfieldID m_fieldID_page;
    jfieldID m_fieldID_percent;
    jfieldID m_fieldID_name;
    jfieldID m_fieldID_path;
public:
    LVTocItemBinding(CRJNIEnv &env);

    void set(jobject obj, LVTocItem *item);

    void add(jobject obj, LVTocItem *child);

    jobject newJavaObject();
};

#endif  // LVTOCITEM_BINDING_H
