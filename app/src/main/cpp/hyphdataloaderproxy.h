/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

#ifndef HYPHDATALOADERPROXY_H
#define HYPHDATALOADERPROXY_H

#include <jni.h>
#include <crhyphman.h>

class HyphDataLoaderProxy : public HyphDataLoader {
    JavaVM *m_javaVM;
    jobject m_creBinding;
public:
    HyphDataLoaderProxy(JavaVM *jvm, jobject crebinding);

    virtual ~HyphDataLoaderProxy();

    virtual LVStreamRef loadData(lString32 code);
};

#endif  // HYPHDATALOADERPROXY_H
