/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "crlog-jnibridge.h"

#include <android/log.h>

#include <stdio.h>
#include <string.h>

#define LOG_TAG "cre-ng"
#define MAX_LOG_MSG_SIZE 1024

CRLogJNIBridge::CRLogJNIBridge() : CRLog() {
    m_currLevel = CRLog::LL_DEBUG;
}

void CRLogJNIBridge::log(log_level lvl, const char *msg, va_list args) {
    static char buffer[MAX_LOG_MSG_SIZE + 1];
    vsnprintf(buffer, MAX_LOG_MSG_SIZE, msg, args);
    int level;
    switch (lvl) {
        case LL_FATAL:
            level = ANDROID_LOG_FATAL;
            break;
        case LL_ERROR:
            level = ANDROID_LOG_ERROR;
            break;
        case LL_WARN:
            level = ANDROID_LOG_WARN;
            break;
        case LL_INFO:
            level = ANDROID_LOG_INFO;
            break;
        case LL_DEBUG:
            level = ANDROID_LOG_DEBUG;
            break;
        case LL_TRACE:
            level = ANDROID_LOG_VERBOSE;
            break;
        default:
            level = ANDROID_LOG_DEBUG;
    }
    __android_log_write(level, LOG_TAG, buffer);
}
