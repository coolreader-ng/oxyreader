/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "crjnienv.h"
#include "crjni-accessors.h"
#include "lvtocitem-binding.h"
#include "crengine-ng-jni.h"

#include <lvtocitem.h>
#include <lvstreamutils.h>
#include <crlog.h>
#include <lvdocview.h>

uint8_t CRJNIEnv::sdk_int = 0;

lString32 CRJNIEnv::fromJavaString(jstring str) {
    if (!str)
        return lString32::empty_str;
    jboolean iscopy;
    const char *s = env->GetStringUTFChars(str, &iscopy);
    lString32 res;
    if (CRJNIEnv::sdk_int >= __ANDROID_API_M__)
        res = Utf8ToUnicode(s);
    else
        res = Wtf8ToUnicode(s);
    env->ReleaseStringUTFChars(str, s);
    return res;
}

jstring CRJNIEnv::toJavaString(const lString32 &str) {
    if (CRJNIEnv::sdk_int >= __ANDROID_API_M__)
        return env->NewStringUTF(UnicodeToUtf8(str).c_str());
    // To support 4-byte UTF-8 sequence on Android older that 6.0 (API 23),
    // we encode characters with codes >= 0x10000 to WTF-8.
    // Otherwise, we have crash with following message:
    //   "input is not valid Modified UTF-8: illegal start byte 0xf0"
    return env->NewStringUTF(UnicodeToWtf8(str).c_str());
}

lInt64 CRJNIEnv::fromJavaLong(jobject value) {
    // an exception will be thrown if the value is of type other than java.lang.Long
    jmethodID pjmLong_longValue = env->GetMethodID(env->GetObjectClass(value), "longValue", "()J");
    return (lInt64) env->CallLongMethod(value, pjmLong_longValue);
}

jobject CRJNIEnv::toJavaLong(lInt64 value) {
    jclass pjcLong = env->FindClass("java/lang/Long");
    if (nullptr != pjcLong) {
        jmethodID pjmLong_valueOf = env->GetStaticMethodID(pjcLong, "valueOf",
                                                           "(J)Ljava/lang/Long;");
        return env->CallStaticObjectMethod(pjcLong, pjmLong_valueOf, value);
    }
    return nullptr;
}

void CRJNIEnv::fromJavaStringArray(jobjectArray array, lString32Collection &dst) {
    dst.clear();
    if (!array)
        return;
    int len = env->GetArrayLength(array);
    for (int i = 0; i < len; i++) {
        jstring str = (jstring) env->GetObjectArrayElement(array, i);
        dst.add(fromJavaString(str));
        env->DeleteLocalRef(str);
    }
}

jobjectArray CRJNIEnv::toJavaStringArray(lString32Collection &src) {
    int len = src.length();
    jobjectArray array = env->NewObjectArray(len, env->FindClass("java/lang/String"),
                                             env->NewStringUTF(""));
    for (int i = 0; i < len; i++) {
        jstring local = toJavaString(src[i]);
        env->SetObjectArrayElement(array, i, local);
        env->DeleteLocalRef(local);
    }
    return array;
}

LVStreamRef CRJNIEnv::jbyteArrayToStream(jbyteArray array) {
    if (!array)
        return LVStreamRef();
    int len = env->GetArrayLength(array);
    if (!len)
        return LVStreamRef();
    auto *data = (lUInt8 *) env->GetByteArrayElements(array, nullptr);
    LVStreamRef res = LVCreateMemoryStream(data, len, true, LVOM_READ);
    env->ReleaseByteArrayElements(array, (jbyte *) data, 0);
    return res;
}

jbyteArray CRJNIEnv::streamToJByteArray(LVStreamRef stream) {
    if (stream.isNull())
        return NULL;
    lvsize_t sz = stream->GetSize();
    if (sz < 10 || sz > 2000000)
        return NULL;
    jbyteArray array = env->NewByteArray((jsize) sz);
    lUInt8 *array_data = (lUInt8 *) env->GetByteArrayElements(array, 0);
    lvsize_t bytesRead = 0;
    stream->Read(array_data, sz, &bytesRead);
    env->ReleaseByteArrayElements(array, (jbyte *) array_data, 0);
    if (bytesRead != sz)
        return NULL;
    return array;
}

CRPropRef CRJNIEnv::fromJavaProperties(jobject jprops) {
    CRPropRef props = LVCreatePropsContainer();
    CRObjectAccessor jp(env, jprops);
    CRMethodAccessor p_getProperty(jp, "getProperty", "(Ljava/lang/String;)Ljava/lang/String;");
    jobject enums = CRMethodAccessor(jp, "propertyNames", "()Ljava/util/Enumeration;").callObj();
    CRObjectAccessor jenums(env, enums);
    CRMethodAccessor jenums_hasMoreElements(jenums, "hasMoreElements", "()Z");
    CRMethodAccessor jenums_nextElement(jenums, "nextElement", "()Ljava/lang/Object;");
    while (jenums_hasMoreElements.callBool()) {
        auto key = (jstring) jenums_nextElement.callObj();
        auto value = (jstring) p_getProperty.callObj(key);
        props->setString(LCSTR(fromJavaString(key)), LCSTR(fromJavaString(value)));
        env->DeleteLocalRef(key);
        env->DeleteLocalRef(value);
    }
    return props;
}

jobject CRJNIEnv::toJavaProperties(CRPropRef props) {
    jclass cls = env->FindClass("java/util/Properties");
    jmethodID mid = env->GetMethodID(cls, "<init>", "()V");
    jobject obj = env->NewObject(cls, mid);
    CRObjectAccessor jp(env, obj);
    CRMethodAccessor p_setProperty(jp, "setProperty",
                                   "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;");
    for (int i = 0; i < props->getCount(); i++) {
        jstring key = toJavaString(lString32(props->getName(i)));
        jstring value = toJavaString(lString32(props->getValue(i)));
        p_setProperty.callObj(key, value);
        env->DeleteLocalRef(key);
        env->DeleteLocalRef(value);
    }
    return obj;
}

bool CRJNIEnv::toJavaProperties(jobject jprops, CRPropRef props) {
    if (nullptr == jprops)
        return false;
    jclass properties_clazz = env->FindClass("java/util/Properties");
    if (!env->IsAssignableFrom(env->GetObjectClass(jprops), properties_clazz))
        return false;
    CRObjectAccessor jp(env, jprops);
    CRMethodAccessor p_setProperty(jp, "setProperty",
                                   "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;");
    for (int i = 0; i < props->getCount(); i++) {
        jstring key = toJavaString(lString32(props->getName(i)));
        jstring value = toJavaString(lString32(props->getValue(i)));
        p_setProperty.callObj(key, value);
        env->DeleteLocalRef(key);
        env->DeleteLocalRef(value);
    }
    return true;
}

void CRJNIEnv::dumpPropsToDebugLog(CRPropRef props) {
    CRLog::debug("dump properties:");
    int count = props->getCount();
    CRLog::debug("  PRPS:    count=%d", count);
    for (int i = 0; i < count; i++) {
        CRLog::debug("  PRPS: %d: %s : %s", i, props->getName(i), LCSTR(props->getValue(i)));
    }
}

jobject CRJNIEnv::toJavaHashSetString(lString32Collection &src) {
    jclass hashset_clazz = env->FindClass("java/util/HashSet");
    jmethodID ctor_mid = env->GetMethodID(hashset_clazz, "<init>", "()V");
    jobject jset = env->NewObject(hashset_clazz, ctor_mid);
    jmethodID add_mid = env->GetMethodID(hashset_clazz, "add", "(Ljava/lang/Object;)Z");
    for (int i = 0; i < src.length(); i++) {
        jstring javaString = toJavaString(src[i]);
        env->CallBooleanMethod(jset, add_mid, javaString);
        env->DeleteLocalRef(javaString);
    }
    return jset;
}

jobject CRJNIEnv::toJavaTOCItem(LVTocItem *toc) {
    if (nullptr == toc)
        return nullptr;
    LVTocItemBinding toc_binding(*this);
    jobject jroot = toc_binding.newJavaObject();
    for (int i = 0; i < toc->getChildCount(); i++) {
        toc_binding.add(jroot, toc->getChild(i));
    }
    return jroot;
}

bool CRJNIEnv::updateBookInfo(jobject jbookinfo, LVDocView *docview, jboolean updatePath) {
    if (jbookinfo == nullptr) {
        CRLog::error("CRJNIEnv::updateBookInfo(): destination object is NULL!");
        return false;
    }
    jclass jbookinfo_class = env->GetObjectClass(jbookinfo);
    jclass pjc_BookInfo = env->FindClass(PACKAGE_SIG "/BookInfo");
    if (!env->IsAssignableFrom(jbookinfo_class, pjc_BookInfo)) {
        CRLog::error("CRJNIEnv::updateBookInfo(): invalid object type!");
        return false;
    }
    if (nullptr == docview || !docview->isDocumentOpened()) {
        CRLog::error("CRJNIEnv::updateBookInfo(): document is not open!");
        return false;
    }
    CRObjectAccessor bookinfo(env, jbookinfo);
    CRStringField titleField(bookinfo, "title");
    titleField.set(docview->getTitle());
    // authors separated by ", ", see
    //  lvtinydomutils.cpp: extractDocKeywords()
    //  epubfmt.cpp: ImportEpubDocument()
    // Split this string & write it to java Set
    CRObjectField authorsField(bookinfo, "authors", "Ljava/util/Set;");
    lString32Collection authors;
    lString32 authorsStr = docview->getAuthors();
    lString32Collection list;
    list.split(authorsStr, cs32(","));
    for (int i = 0; i < list.length(); i++) {
        lString32 author = list[i];
        author = author.trim();
        if (!author.empty())
            authors.add(author);
    }
    list.clear();
    if (authors.length() != 0)
        authorsField.set(toJavaHashSetString(authors));
    else
        authorsField.set(nullptr);
    CRStringField seriesField(bookinfo, "series");
    CRIntegerField seriesNumberField(bookinfo, "seriesNumber");
    lString32 seriesName = docview->getSeriesName();
    int seriesNumber = docview->getSeriesNumber();
    if (!seriesName.empty())
        seriesField.set(seriesName);
    else
        seriesField.setObject(nullptr);
    if (!seriesName.empty() && seriesNumber != 0)
        seriesNumberField.set(seriesNumber);
    else {
        // series number 0 is generally considered invalid
        seriesNumberField.setObject(nullptr);
    }
    CRStringField languageField(bookinfo, "language");
    languageField.set(docview->getLanguage());

    CRObjectField keywordsField(bookinfo, "keywords", "Ljava/util/Set;");
    // keywords separated by ", ", see
    //  lvtinydomutils.cpp: extractDocKeywords()
    //  epubfmt.cpp: ImportEpubDocument()
    lString32Collection keywords;
    lString32 keywordsStr = docview->getKeywords();
    list.split(keywordsStr, cs32(","));
    for (int i = 0; i < list.length(); i++) {
        lString32 keyword = list[i];
        keyword = keyword.trim();
        if (!keyword.empty())
            keywords.add(keyword);
    }
    list.clear();
    if (keywords.length() != 0)
        keywordsField.set(toJavaHashSetString(keywords));
    else
        keywordsField.set(nullptr);
    CRStringField descriptionField(bookinfo, "description");
    descriptionField.set(docview->getDescription());
    CRObjectField formatField(bookinfo, "format", "L" PACKAGE_SIG "/DocumentFormat;");
    jclass format_clazz = env->GetObjectClass(formatField.getObject());
    jmethodID methodId_byId = env->GetStaticMethodID(format_clazz, "byId",
                                                     "(I)L" PACKAGE_SIG "/DocumentFormat;");
    jobject jformat = env->CallStaticObjectMethod(format_clazz, methodId_byId,
                                                  docview->getDocFormat());
    formatField.set(jformat);
    CRObjectAccessor fileinfo(env, CRFieldAccessor(bookinfo, "fileInfo",
                                                   "L" PACKAGE_SIG "/FileInfo;").getObject());
    CRStringField fingerPrintField(fileinfo, "fingerprint");
#if (USE_SHASUM == 1)
    fingerPrintField.set(docview->getFileHash());
#else
    fingerPrintField.set(lString32::empty_str);
#endif
    if (updatePath) {
        CRPropRef doc_props = docview->getDocProps();
        if (!doc_props.isNull()) {
            CRStringField arcnameField(fileinfo, "archiveName");
            CRLongField arcsizeField(fileinfo, "arcSize");
            CRLongField packsizeField(fileinfo, "packSize");
            CRBooleanField isArchiveField(fileinfo, "isArchive");
            CRStringField pathNameField(fileinfo, "pathName");
            CRStringField dirPathField(fileinfo, "dirPath");
            CRStringField fileNameField(fileinfo, "fileName");
            CRLongField sizeField(fileinfo, "size");
            bool isArchive = !doc_props->getStringDef(DOC_PROP_ARC_NAME, "").empty();
            if (isArchive) {
                lString32 arcname = doc_props->getStringDef(DOC_PROP_ARC_NAME, "");
                lString32 arcpath = doc_props->getStringDef(DOC_PROP_ARC_PATH, "");
                lInt64 arcsize = doc_props->getInt64Def(DOC_PROP_ARC_SIZE, 0);
                lInt64 packsize = doc_props->getInt64Def(DOC_PROP_FILE_PACK_SIZE, 0);
                arcnameField.set(LVCombinePaths(arcpath, arcname));
                arcsizeField.set(arcsize);
                packsizeField.set(packsize);
            } else {
                arcnameField.setObject(nullptr);
                arcsizeField.set(0);
                packsizeField.set(0);
            }
            lString32 path = doc_props->getStringDef(DOC_PROP_FILE_PATH, "");
            LVRemovePathDelimiter(path);
            lString32 filename = doc_props->getStringDef(DOC_PROP_FILE_NAME, "");
            isArchiveField.set(isArchive);
            pathNameField.set(LVCombinePaths(path, filename));
            dirPathField.set(path);
            fileNameField.set(filename);
            sizeField.set(doc_props->getInt64Def(DOC_PROP_FILE_SIZE, 0));
        }
    }
    return true;
}
