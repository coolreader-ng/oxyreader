/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#ifndef CRJNIENV_H
#define CRJNIENV_H

#include <jni.h>

#include <lvstring.h>
#include <lvstring32collection.h>
#include <crprops.h>
#include <lvstream.h>

class LVTocItem;

class LVDocView;

class CRJNIEnv {
public:
    CRJNIEnv(JNIEnv *pEnv) : env(pEnv) {}

    JNIEnv *operator->() { return env; }

    lString32 fromJavaString(jstring str);

    jstring toJavaString(const lString32 &str);

    lInt64 fromJavaLong(jobject value);

    jobject toJavaLong(lInt64 value);

    void fromJavaStringArray(jobjectArray array, lString32Collection &dst);

    jobjectArray toJavaStringArray(lString32Collection &src);

    LVStreamRef jbyteArrayToStream(jbyteArray array);

    jbyteArray streamToJByteArray(LVStreamRef stream);

    //jobject enumByNativeId(const char *classname, int id);

    CRPropRef fromJavaProperties(jobject jprops);

    jobject toJavaProperties(CRPropRef props);

    bool toJavaProperties(jobject jprops, CRPropRef props);

    void dumpPropsToDebugLog(CRPropRef props);

    jobject toJavaHashSetString(lString32Collection &src);

    jobject toJavaTOCItem(LVTocItem *toc);

    bool updateBookInfo(jobject jbookinfo, LVDocView *docview, jboolean updatePath);

public:
    static uint8_t sdk_int;
protected:
    JNIEnv *env;
};

#endif // CRJNIENV_H
