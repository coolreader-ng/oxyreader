/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/* Header for native methods of java class LVDocViewWrapper */

#ifndef LVDOCVIEW_WRAPPER_H
#define LVDOCVIEW_WRAPPER_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    newLVDocView_native
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_newLVDocView_1native
        (JNIEnv *, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    deleteLVDocView_native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_deleteLVDocView_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    loadDocument_native
 * Signature: (JLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocument_1native
        (JNIEnv *, jobject, jlong, jstring, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    loadDocumentBuff_native
 * Signature: (J[BLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocumentBuff_1native
        (JNIEnv *, jobject, jlong, jbyteArray, jstring, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    renderPageBitmap_native
 * Signature: (JLandroid/graphics/Bitmap;B)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_renderPageBitmap_1native
        (JNIEnv *, jobject, jlong, jobject, jbyte);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    resize_native
 * Signature: (JII)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_resize_1native
        (JNIEnv *, jobject, jlong, jint, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    processCommand_native
 * Signature: (JII)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_processCommand_1native
        (JNIEnv *, jobject, jlong, jint, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getCurrentPageBookmark_native
 * Signature: (J)Lio/gitlab/coolreader_ng/project_s/Bookmark;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getCurrentPageBookmark_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getPositionProps_native
 * Signature: (Ljava/lang/String;Z)Lio/gitlab/coolreader_ng/project_s/PositionProperties;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getPositionProps_1native
        (JNIEnv *, jobject, jlong, jstring, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getAvgTextLineHeight_native
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getAvgTextLineHeight_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    checkLink_native
 * Signature: (JIII)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkLink_1native
        (JNIEnv *, jobject, jlong, jint, jint, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    checkBookmark_native
 * Signature: (JIILio/gitlab/coolreader_ng/project_s/Bookmark;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkBookmark_1native
        (JNIEnv *, jobject, jlong, jint, jint, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getEmbeddedImage_native
 * Signature: (JIILio/gitlab/coolreader_ng/project_s/PageImageCache;B)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getEmbeddedImage_1native
        (JNIEnv *, jobject, jlong, jint, jint, jobject, jbyte);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    goLink_native
 * Signature: (JLjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goLink_1native
        (JNIEnv *, jobject, jlong, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    goToPosition_native
 * Signature: (JLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goToPosition_1native
        (JNIEnv *, jobject, jlong, jstring, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getSettings_native
 * Signature: (JLjava/util/Properties;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSettings_1native
        (JNIEnv *, jobject, jlong, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    applySettings_native
 * Signature: (JLjava/util/Properties;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applySettings_1native
        (JNIEnv *, jobject, jlong, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setBatteryState_native
 * Signature: (JIII)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBatteryState_1native
        (JNIEnv *, jobject, jlong, jint, jint, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    isTimeChanged_native
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_isTimeChanged_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    updateCache_native
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateCache_1native
        (JNIEnv *, jobject, jlong, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    cancelLongCachingOperation_native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_cancelLongCachingOperation_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    updateBookInfo_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/BookInfo;Z)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateBookInfo_1native
        (JNIEnv *, jobject, jlong, jobject, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    createDefaultDocument_native
 * Signature: (JLjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_createDefaultDocument_1native
        (JNIEnv *, jobject, jlong, jstring, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setPageBackgroundTexture_native
 * Signature: (J[BZ)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setPageBackgroundTexture_1native
        (JNIEnv *, jobject, jlong, jbyteArray, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getTOC_native
 * Signature: (J)Lio/gitlab/coolreader_ng/project_s/DocumentTOCItem;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getTOC_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    canGoBack_native
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_canGoBack_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getBookCoverData_native
 * Signature: (J)[B
 */
JNIEXPORT jbyteArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getBookCoverData_1native(
        JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getSelection_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/Selection;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSelection_1native
        (JNIEnv *, jobject, jlong, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    selectText_native
 * Signature: (JIIIIZ)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectText_1native
        (JNIEnv *, jobject, jlong, jint, jint, jint, jint, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    clearSelection_native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_clearSelection_1native
        (JNIEnv *, jobject, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    docToWindowPoint_native
 * Signature: (JLandroid/graphics/Point;Landroid/graphics/Point;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_docToWindowPoint_1native
        (JNIEnv *, jobject, jlong, jobject, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setBookmarks_native
 * Signature: (J[Lio/gitlab/coolreader_ng/project_s/Bookmark;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBookmarks_1native
        (JNIEnv *, jobject, jlong, jobjectArray);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setForceScrollViewMode_native
 * Signature: (JZ)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setForceScrollViewMode_1native
        (JNIEnv *, jobject, jlong, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setDecimalPointChar_native
 * Signature: (JC)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setDecimalPointChar_1native
        (JNIEnv *, jobject, jlong, jchar);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    applyPageInsets_native
 * Signature: (JIIIIZ)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applyPageInsets_1native
        (JNIEnv *env, jobject, jlong, jint, jint, jint, jint, jboolean);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    findTextAll_native
 * Signature: (JLjava/lang/String;ZI)[Lio/gitlab/coolreader_ng/project_s/SearchResultItem;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_findTextAll_1native
        (JNIEnv *, jobject, jlong, jstring, jboolean, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    selectSearchResult_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/SearchResultItem;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectSearchResult_1native
        (JNIEnv *, jobject, jlong, jobject);

#ifdef __cplusplus
}
#endif

#endif  // LVDOCVIEW_WRAPPER_H
