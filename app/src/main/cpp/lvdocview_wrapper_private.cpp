/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "lvdocview_wrapper_private.h"
#include "lvdocviewcmd_extra.h"

#include <ldomdocument.h>
#include <lvrend.h>

static void s_getBatteryIcons(LVRefVec<LVImageSource> &icons, lUInt32 color, int size);

LVDocViewWrapperPrivate::LVDocViewWrapperPrivate() {
    m_doc = new LVDocView(32, false);
    m_batteryIconColor = 0x000000;
    m_batteryIconSize = -1;
    m_doc->setMinFontSize(8);
    m_doc->setMaxFontSize(320);
    m_doc->Resize(360, 640);
    m_forcedScrollViewMode = false;
    m_pendingViewMode = DVM_PAGES;  // doesn't affect anything here
}

LVDocViewWrapperPrivate::~LVDocViewWrapperPrivate() {
    closeBook();
    delete m_doc;
}

lString32 LVDocViewWrapperPrivate::getLink(int x, int y) {
    ldomXPointer p = m_doc->getNodeByPoint(lvPoint(x, y));
    if (!p.isNull())
        return p.getHRef();
    return lString32::empty_str;
}

lString32 LVDocViewWrapperPrivate::getLink(int x, int y, int r) {
    const int step = 5;
    int n = r / step;
    r = n * step;
    if (r == 0)
        return getLink(x, y);
    lString32 link;
    for (int xx = -r; xx <= r; xx += step) {
        link = getLink(x + xx, y - r);
        if (!link.empty())
            return link;
        link = getLink(x + xx, y + r);
        if (!link.empty())
            return link;
    }
    for (int yy = -r + step; yy <= r - step; yy += step) {
        link = getLink(x + r, y + yy);
        if (!link.empty())
            return link;
        link = getLink(x - r, y + yy);
        if (!link.empty())
            return link;
    }
    return lString32::empty_str;
}

bool LVDocViewWrapperPrivate::closeBook() {
    if (m_doc->isDocumentOpened()) {
        m_doc->savePosition();
        m_doc->getDocument()->updateMap();
        //saveHistory(lString32::empty_str);
        m_doc->close();
        return true;
    }
    return false;
}

int LVDocViewWrapperPrivate::processCommand(LVDocCmd command, int param) {
    switch (command) {
        case DCMD_CLOSE_BOOK:
            return closeBook();
        case DCMD_PAGEUP:
            if (m_doc->getViewMode() == DVM_SCROLL) {
                int offset = -m_doc->GetHeight() + m_doc->getAvgTextLineHeight();
                return m_doc->doCommand(DCMD_SCROLL_BY, offset);
            }
            return m_doc->doCommand(command, param);
        case DCMD_PAGEDOWN:
            if (m_doc->getViewMode() == DVM_SCROLL) {
                int offset = m_doc->GetHeight() - m_doc->getAvgTextLineHeight();
                return m_doc->doCommand(DCMD_SCROLL_BY, offset);
            }
            return m_doc->doCommand(command, param);
        default:
            return m_doc->doCommand(command, param);
    }
}

void LVDocViewWrapperPrivate::updateBatteryIcons() {
    CRPropRef props = m_doc->propsGetCurrent();
    lUInt32 textColor = props->getColorDef(PROP_FONT_COLOR, 0x000000);
    lUInt32 statusColor = props->getColorDef(PROP_STATUS_FONT_COLOR, 0xFF000000);
    lUInt32 batteryColor = statusColor;
    if (batteryColor == 0xFF000000)
        batteryColor = textColor;
    int newSize = 28;
    int w = m_doc->GetWidth();
    int h = m_doc->GetHeight();
    if (w > h)
        w = h;
    if (w > 700)
        newSize = 56;
    if (m_batteryIconColor != batteryColor ||
        m_batteryIconSize != newSize) { //oldNightMode!=newNightMode
        m_batteryIconColor = batteryColor;
        m_batteryIconSize = newSize;
        //CRLog::debug("Setting Battery icon color = #%06x", batteryColor);
        LVRefVec<LVImageSource> icons;
        s_getBatteryIcons(icons, m_batteryIconColor, m_batteryIconSize);
        //CRLog::debug("Setting list of Battery icon bitmaps, size=%d, w=%d", m_batteryIconSize, w);
        m_doc->setBatteryIcons(icons);
        //CRLog::debug("Setting list of Battery icon bitmats - done");
    }
}

ContinuousOperationResult LVDocViewWrapperPrivate::updateCache(long timeout) {
    CRTimerUtil timer(timeout);
    // TODO: use mutex to lock critical section
    m_timeoutControl = timer;
    return m_doc->updateCache(m_timeoutControl);
}

void LVDocViewWrapperPrivate::cancelLongCachingOperation() {
    // TODO: use mutex to lock critical section since this function called from another thread
    m_timeoutControl.cancel();
}

bool LVDocViewWrapperPrivate::setForceScrollViewMode(bool value) {
    if (m_forcedScrollViewMode == value)
        return false;
    bool res = false;
    if (value) {
        if (DVM_SCROLL != m_doc->getViewMode()) {
            m_pendingViewMode = DVM_PAGES;
            m_doc->setViewMode(DVM_SCROLL);
            res = true;
        } else {
            m_pendingViewMode = DVM_SCROLL;
        }
    } else {
        if (m_pendingViewMode != m_doc->getViewMode()) {
            m_doc->setViewMode(m_pendingViewMode);
            res = true;
        }
    }
    m_forcedScrollViewMode = value;
    return res;
}

static void s_replaceColor(char *str, lUInt32 color) {
    // in line like "0 c #80000000",
    // replace value of color
    for (int i = 0; i < 8; i++) {
        str[i + 5] = toHexDigit((color >> 28) & 0xF);
        color <<= 4;
    }
}

static void s_getBatteryIcons(LVRefVec<LVImageSource> &icons, lUInt32 color, int size) {
    CRLog::debug("Making list of Battery icon bitmaps");

    lUInt32 cl1 = 0x00000000 | (color & 0xFFFFFF);
    lUInt32 cl2 = 0x40000000 | (color & 0xFFFFFF);
    lUInt32 cl3 = 0x80000000 | (color & 0xFFFFFF);
    lUInt32 cl4 = 0xF0000000 | (color & 0xFFFFFF);

    static char color1[] = "0 c #80000000";
    static char color2[] = "X c #80000000";
    static char color3[] = "o c #80AAAAAA";
    static char color4[] = ". c #80FFFFFF";

    if (size <= 28) {

#undef BATTERY_HEADER
#define BATTERY_HEADER \
                "28 15 5 1", \
                color1, \
                color2, \
                color3, \
                color4, \
                "  c None",

        static const char *battery8[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0000.XXXX.XXXX.XXXX.XXXX.0.",
                ".0..0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0..0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0..0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0..0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0..0.XXXX.XXXX.XXXX.XXXX.0.",
                ".0000.XXXX.XXXX.XXXX.XXXX.0.",
                "....0.XXXX.XXXX.XXXX.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery7[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0.oooo.XXXX.XXXX.XXXX.0.",
                ".0000.oooo.XXXX.XXXX.XXXX.0.",
                ".0..0.oooo.XXXX.XXXX.XXXX.0.",
                ".0..0.oooo.XXXX.XXXX.XXXX.0.",
                ".0..0.oooo.XXXX.XXXX.XXXX.0.",
                ".0..0.oooo.XXXX.XXXX.XXXX.0.",
                ".0..0.oooo.XXXX.XXXX.XXXX.0.",
                ".0000.oooo.XXXX.XXXX.XXXX.0.",
                "....0.oooo.XXXX.XXXX.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery6[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0......XXXX.XXXX.XXXX.0.",
                ".0000......XXXX.XXXX.XXXX.0.",
                ".0..0......XXXX.XXXX.XXXX.0.",
                ".0..0......XXXX.XXXX.XXXX.0.",
                ".0..0......XXXX.XXXX.XXXX.0.",
                ".0..0......XXXX.XXXX.XXXX.0.",
                ".0..0......XXXX.XXXX.XXXX.0.",
                ".0000......XXXX.XXXX.XXXX.0.",
                "....0......XXXX.XXXX.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery5[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0......oooo.XXXX.XXXX.0.",
                ".0000......oooo.XXXX.XXXX.0.",
                ".0..0......oooo.XXXX.XXXX.0.",
                ".0..0......oooo.XXXX.XXXX.0.",
                ".0..0......oooo.XXXX.XXXX.0.",
                ".0..0......oooo.XXXX.XXXX.0.",
                ".0..0......oooo.XXXX.XXXX.0.",
                ".0000......oooo.XXXX.XXXX.0.",
                "....0......oooo.XXXX.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery4[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0...........XXXX.XXXX.0.",
                ".0000...........XXXX.XXXX.0.",
                ".0..0...........XXXX.XXXX.0.",
                ".0..0...........XXXX.XXXX.0.",
                ".0..0...........XXXX.XXXX.0.",
                ".0..0...........XXXX.XXXX.0.",
                ".0..0...........XXXX.XXXX.0.",
                ".0000...........XXXX.XXXX.0.",
                "....0...........XXXX.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery3[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0...........oooo.XXXX.0.",
                ".0000...........oooo.XXXX.0.",
                ".0..0...........oooo.XXXX.0.",
                ".0..0...........oooo.XXXX.0.",
                ".0..0...........oooo.XXXX.0.",
                ".0..0...........oooo.XXXX.0.",
                ".0..0...........oooo.XXXX.0.",
                ".0000...........oooo.XXXX.0.",
                "....0...........oooo.XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery2[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0................XXXX.0.",
                ".0000................XXXX.0.",
                ".0..0................XXXX.0.",
                ".0..0................XXXX.0.",
                ".0..0................XXXX.0.",
                ".0..0................XXXX.0.",
                ".0..0................XXXX.0.",
                ".0000................XXXX.0.",
                "....0................XXXX.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery1[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "   .0................oooo.0.",
                ".0000................oooo.0.",
                ".0..0................oooo.0.",
                ".0..0................oooo.0.",
                ".0..0................oooo.0.",
                ".0..0................oooo.0.",
                ".0..0................oooo.0.",
                ".0000................oooo.0.",
                "   .0................oooo.0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery0[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "   .0.....................0.",
                ".0000.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0000.....................0.",
                "....0.....................0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        //#endif

        static const char *battery_charge[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0.....................0.",
                ".0000............XX.......0.",
                ".0..0...........XXXX......0.",
                ".0..0..XX......XXXXXX.....0.",
                ".0..0...XXX...XXXX..XX....0.",
                ".0..0....XXX..XXXX...XX...0.",
                ".0..0.....XXXXXXX.....XX..0.",
                ".0000.......XXXX..........0.",
                "....0........XX...........0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };
        static const char *battery_frame[] = {
                BATTERY_HEADER
                "   .........................",
                "   .00000000000000000000000.",
                "   .0.....................0.",
                "....0.....................0.",
                ".0000.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0..0.....................0.",
                ".0000.....................0.",
                "....0.....................0.",
                "   .0.....................0.",
                "   .00000000000000000000000.",
                "   .........................",
        };

        const char **icon_bpm[] = {
                battery_charge,
                battery0,
                battery1,
                battery2,
                battery3,
                battery4,
                battery5,
                battery6,
                battery7,
                battery8,
                battery_frame,
                NULL
        };

        s_replaceColor(color1, cl1);
        s_replaceColor(color2, cl2);
        s_replaceColor(color3, cl3);
        s_replaceColor(color4, cl4);

        for (int i = 0; icon_bpm[i]; i++)
            icons.add(LVCreateXPMImageSource(icon_bpm[i]));

    } else {

#undef BATTERY_HEADER
#define BATTERY_HEADER \
                "56 30 5 1", \
                color1, \
                color2, \
                color3, \
                color4, \
                "  c None",

        static const char *battery8[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..XXXXXXXX..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery7[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00..oooooooo..XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery6[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "........00............XXXXXXXX..XXXXXXXX..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery5[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "........00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00....00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "..00000000............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "........00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "........00............oooooooo..XXXXXXXX..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery4[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00......................XXXXXXXX..XXXXXXXX..00..",
                "........00......................XXXXXXXX..XXXXXXXX..00..",
                "..00000000......................XXXXXXXX..XXXXXXXX..00..",
                "..00000000......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00....00......................XXXXXXXX..XXXXXXXX..00..",
                "..00000000......................XXXXXXXX..XXXXXXXX..00..",
                "..00000000......................XXXXXXXX..XXXXXXXX..00..",
                "........00......................XXXXXXXX..XXXXXXXX..00..",
                "........00......................XXXXXXXX..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery3[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00......................oooooooo..XXXXXXXX..00..",
                "........00......................oooooooo..XXXXXXXX..00..",
                "..00000000......................oooooooo..XXXXXXXX..00..",
                "..00000000......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00....00......................oooooooo..XXXXXXXX..00..",
                "..00000000......................oooooooo..XXXXXXXX..00..",
                "..00000000......................oooooooo..XXXXXXXX..00..",
                "........00......................oooooooo..XXXXXXXX..00..",
                "........00......................oooooooo..XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery2[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00................................XXXXXXXX..00..",
                "........00................................XXXXXXXX..00..",
                "..00000000................................XXXXXXXX..00..",
                "..00000000................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00....00................................XXXXXXXX..00..",
                "..00000000................................XXXXXXXX..00..",
                "..00000000................................XXXXXXXX..00..",
                "........00................................XXXXXXXX..00..",
                "........00................................XXXXXXXX..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery1[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00................................oooooooo..00..",
                "........00................................oooooooo..00..",
                "..00000000................................oooooooo..00..",
                "..00000000................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00....00................................oooooooo..00..",
                "..00000000................................oooooooo..00..",
                "..00000000................................oooooooo..00..",
                "........00................................oooooooo..00..",
                "........00................................oooooooo..00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery0[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00..........................................00..",
                "........00..........................................00..",
                "..00000000..........................................00..",
                "..00000000..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00000000..........................................00..",
                "..00000000..........................................00..",
                "........00..........................................00..",
                "........00..........................................00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        //#endif

        static const char *battery_charge[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00..........................................00..",
                "........00..........................X...............00..",
                "..00000000.........................XXX..............00..",
                "..00000000........................XXXXX.............00..",
                "..00....00...X...................XXXXXXX............00..",
                "..00....00...XXX................XXXXXXXXX...........00..",
                "..00....00....XXXX.............XXXXXXXXXXX..........00..",
                "..00....00....XXXXX...........XXXXXXX.XXXXX.........00..",
                "..00....00.....XXXXXX........XXXXXXX....XXXX........00..",
                "..00....00......XXXXXX.......XXXXXXX.....XXXX.......00..",
                "..00....00.......XXXXXX.....XXXXXXX.......XXXX......00..",
                "..00....00........XXXXXX....XXXXXX.........XXXX.....00..",
                "..00....00.........XXXXXX.XXXXXXX...........XXXX....00..",
                "..00....00..........XXXXXXXXXXXXX............XXXX...00..",
                "..00000000............XXXXXXXXXX...............XX...00..",
                "..00000000..............XXXXXXX.....................00..",
                "........00...............XXXXX......................00..",
                "........00................XXX.......................00..",
                "      ..00.................X........................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };
        static const char *battery_frame[] = {
                BATTERY_HEADER
                "      ..................................................",
                "      ..................................................",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "........00..........................................00..",
                "........00..........................................00..",
                "..00000000..........................................00..",
                "..00000000..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00....00..........................................00..",
                "..00000000..........................................00..",
                "..00000000..........................................00..",
                "........00..........................................00..",
                "........00..........................................00..",
                "      ..00..........................................00..",
                "      ..00..........................................00..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..0000000000000000000000000000000000000000000000..",
                "      ..................................................",
                "      ..................................................",
        };

        const char **icon_bpm[] = {
                battery_charge,
                battery0,
                battery1,
                battery2,
                battery3,
                battery4,
                battery5,
                battery6,
                battery7,
                battery8,
                battery_frame,
                NULL
        };

        s_replaceColor(color1, cl1);
        s_replaceColor(color2, cl2);
        s_replaceColor(color3, cl3);
        s_replaceColor(color4, cl4);

        for (int i = 0; icon_bpm[i]; i++)
            icons.add(LVCreateXPMImageSource(icon_bpm[i]));

    }
}
