/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#ifndef CRJNI_ACCESSORS_H
#define CRJNI_ACCESSORS_H

#include "crjnienv.h"

class CRClassAccessor : public CRJNIEnv {
protected:
    jclass cls;
public:
    jclass getClass() { return cls; }

    CRClassAccessor(JNIEnv *pEnv, jclass _class) : CRJNIEnv(pEnv) {
        cls = _class;
    }

    CRClassAccessor(JNIEnv *pEnv, const char *className) : CRJNIEnv(pEnv) {
        cls = env->FindClass(className);
    }
};

class CRObjectAccessor : public CRClassAccessor {
    jobject obj;
public:
    jobject getObject() { return obj; }

    CRObjectAccessor(JNIEnv *pEnv, jobject _obj)
            : CRClassAccessor(pEnv, pEnv->GetObjectClass(_obj)) {
        obj = _obj;
    }
};

class CRFieldAccessor {
protected:
    CRObjectAccessor &objacc;
    jfieldID fieldid;
public:
    CRFieldAccessor(CRObjectAccessor &acc, const char *fieldName, const char *fieldType)
            : objacc(acc) {
        fieldid = objacc->GetFieldID(objacc.getClass(), fieldName, fieldType);
    }

    jobject getObject() {
        return objacc->GetObjectField(objacc.getObject(), fieldid);
    }

    void setObject(jobject obj) {
        return objacc->SetObjectField(objacc.getObject(), fieldid, obj);
    }
};

class CRMethodAccessor {
protected:
    CRObjectAccessor &objacc;
    jmethodID methodid;
public:
    CRMethodAccessor(CRObjectAccessor &acc, const char *methodName, const char *signature)
            : objacc(acc) {
        methodid = objacc->GetMethodID(objacc.getClass(), methodName, signature);
    }

    jobject callObj() {
        return objacc->CallObjectMethod(objacc.getObject(), methodid);
    }

    jobject callObj(jobject obj) {
        return objacc->CallObjectMethod(objacc.getObject(), methodid, obj);
    }

    jobject callObj(jobject obj1, jobject obj2) {
        return objacc->CallObjectMethod(objacc.getObject(), methodid, obj1, obj2);
    }

    jboolean callBool() {
        return objacc->CallBooleanMethod(objacc.getObject(), methodid);
    }

    jint callInt() {
        return objacc->CallIntMethod(objacc.getObject(), methodid);
    }
};

class CRStringField : public CRFieldAccessor {
public:
    CRStringField(CRObjectAccessor &acc, const char *fieldName)
            : CRFieldAccessor(acc, fieldName, "Ljava/lang/String;") {
    }

    lString32 get() {
        jstring str = (jstring) objacc->GetObjectField(objacc.getObject(), fieldid);
        lString32 res = objacc.fromJavaString(str);
        objacc->DeleteLocalRef(str);
        return res;
    }

    void set(const lString32 &str) {
        objacc->SetObjectField(objacc.getObject(), fieldid, objacc.toJavaString(str));
    }
};

class CRIntField : public CRFieldAccessor {
public:
    CRIntField(CRObjectAccessor &acc, const char *fieldName)
            : CRFieldAccessor(acc, fieldName, "I") {
    }

    int get() { return objacc->GetIntField(objacc.getObject(), fieldid); }

    void set(int v) { objacc->SetIntField(objacc.getObject(), fieldid, v); }
};

class CRLongField : public CRFieldAccessor {
public:
    CRLongField(CRObjectAccessor &acc, const char *fieldName)
            : CRFieldAccessor(acc, fieldName, "J") {
    }

    lInt64 get() { return objacc->GetLongField(objacc.getObject(), fieldid); }

    void set(lInt64 v) { objacc->SetLongField(objacc.getObject(), fieldid, v); }
};

class CRBooleanField : public CRFieldAccessor {
public:
    CRBooleanField(CRObjectAccessor &acc, const char *fieldName)
            : CRFieldAccessor(acc, fieldName, "Z") {
    }

    bool get() { return objacc->GetBooleanField(objacc.getObject(), fieldid); }

    void set(bool v) { objacc->SetBooleanField(objacc.getObject(), fieldid, v); }
};

class CRObjectField : public CRFieldAccessor {
public:
    CRObjectField(CRObjectAccessor &acc, const char *fieldName, const char *signature)
            : CRFieldAccessor(acc, fieldName, signature) {
    }

    jobject get() { return objacc->GetObjectField(objacc.getObject(), fieldid); }

    void set(jobject v) { objacc->SetObjectField(objacc.getObject(), fieldid, v); }
};

class CRIntegerField : public CRObjectField {
    jclass _clazz;
    jmethodID _mid_intValue;
    jmethodID _mid_ctor_int;
public:
    CRIntegerField(CRObjectAccessor &acc, const char *fieldName)
            : CRObjectField(acc, fieldName, "Ljava/lang/Integer;") {
        _clazz = objacc->FindClass("java/lang/Integer");
        _mid_intValue = objacc->GetMethodID(_clazz, "intValue", "()I");
        _mid_ctor_int = objacc->GetMethodID(_clazz, "<init>", "(I)V");
    }

    int intValue(bool *isNull) {
        jobject obj = get();
        if (nullptr == obj) {
            if (isNull)
                *isNull = true;
            return 0;
        } else {
            if (isNull)
                *isNull = false;
            return objacc->CallIntMethod(obj, _mid_intValue);
        }
    }

    void set(int v) {
        CRObjectField::set(objacc->NewObject(_clazz, _mid_ctor_int, v));
    }
};

#endif // CRJNI_ACCESSORS_H
