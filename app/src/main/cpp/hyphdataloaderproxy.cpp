/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

#include "hyphdataloaderproxy.h"

#include "crengine-ng-jni.h"
#include "crjnienv.h"

HyphDataLoaderProxy::HyphDataLoaderProxy(JavaVM *jvm, jobject crebinding) :
        HyphDataLoader() {
    m_javaVM = jvm;
    JNIEnv *penv = nullptr;
    bool attached = false;
    m_javaVM->GetEnv((void **) &penv, JNI_VERSION_1_6);
    if (nullptr == penv) {
        // caller thread is not attached yet
        m_javaVM->AttachCurrentThread(&penv, nullptr);
        attached = true;
    }
    m_creBinding = penv->NewGlobalRef(crebinding);
    if (attached)
        m_javaVM->DetachCurrentThread();
}

HyphDataLoaderProxy::~HyphDataLoaderProxy() {
    JNIEnv *penv = nullptr;
    bool attached = false;
    m_javaVM->GetEnv((void **) &penv, JNI_VERSION_1_6);
    if (nullptr == penv) {
        // caller thread is not attached yet
        m_javaVM->AttachCurrentThread(&penv, nullptr);
        attached = true;
    }
    penv->DeleteGlobalRef(m_creBinding);
    if (attached)
        m_javaVM->DetachCurrentThread();
}

LVStreamRef HyphDataLoaderProxy::loadData(lString32 code) {
    JNIEnv *penv = nullptr;
    bool attached = false;
    m_javaVM->GetEnv((void **) &penv, JNI_VERSION_1_6);
    if (nullptr == penv) {
        // caller thread is not attached yet
        m_javaVM->AttachCurrentThread(&penv, nullptr);
        attached = true;
    }
    LVStreamRef stream = LVStreamRef();
    jclass clazz = penv->GetObjectClass(m_creBinding);
    if (nullptr != clazz) {
        jmethodID mid_loadHyphDictData = penv->GetMethodID(clazz,
                                                           "loadHyphDictData",
                                                           "(Ljava/lang/String;)[B");
        if (nullptr != mid_loadHyphDictData) {
            CRJNIEnv env(penv);
            jstring jcode = env.toJavaString(code);
            auto data = (jbyteArray) penv->CallObjectMethod(m_creBinding, mid_loadHyphDictData,
                                                            jcode);
            stream = env.jbyteArrayToStream(data);
        }
    }
    if (attached)
        m_javaVM->DetachCurrentThread();
    return stream;
}
