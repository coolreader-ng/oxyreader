/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/***************************************************************************
 *   Based on CoolReader project code at                                   *
 *   https://github.com/buggins/coolreader                                 *
 *   Copyright (C) 2010-2021 by Vadim Lopatin <coolreader.org@gmail.com>   *
 ***************************************************************************/

#include "lvdocview_wrapper.h"
#include "crengine-ng-jni.h"
#include "lvdocview_wrapper_private.h"
#include "crjnienv.h"
#include "crjni-accessors.h"
#include "jnigraphicslib.h"
#include "lvdocviewcallback-jnibridge.h"

#include <lvgraydrawbuf.h>
#include <lvstreamutils.h>
#include <ldomdocument.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    newLVDocView_native
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_newLVDocView_1native
        (JNIEnv *, jobject) {
    auto *wrapper = new LVDocViewWrapperPrivate;
    return (jlong) wrapper;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    deleteLVDocView_native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_deleteLVDocView_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    delete wrapper;
    CRLog::debug("deleteLVDocView_native(): resources released");
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    loadDocument_native
 * Signature: (JLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocument_1native
        (JNIEnv *env_, jobject _this, jlong instance, jstring jfilename, jboolean metadataOnly) {
    CRJNIEnv env(env_);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    auto filename = env.fromJavaString(jfilename);
    LVDocViewCallbackJNIBridge callback(env_, wrapper->getDocView(), _this);
    CRLog::info("Loading document %s", LCSTR(filename));
    auto res = doc->LoadDocument(filename.c_str(), metadataOnly != JNI_FALSE);
    if (res) {
        CRLog::info("Document %s is loaded successfully", LCSTR(filename));
#ifdef _DEBUG
        CRPropRef doc_props = doc->getDocProps();
        env.dumpPropsToDebugLog(doc_props);
#endif
        return JNI_TRUE;
    } else {
        CRLog::info("Document %s was not loaded due to an error", LCSTR(filename));
        if (nullptr == doc->getDocument()) {
            // doc->LoadDocument() can return false with doc->m_doc == NULL when:
            // 1. I/O error - failed to open file
            // 2. open archive without supported files
            CRLog::error("Document is NULL, inserting stub.");
            doc->createDefaultDocument(cs32("Error"),
                                       cs32("Error while opening file \"") + filename + "\"!");
        }
    }
    return JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    loadDocumentBuff_native
 * Signature: (J[BLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_loadDocumentBuff_1native
        (JNIEnv *env_, jobject _this, jlong instance, jbyteArray jbuff, jstring jStreamName,
         jboolean metadataOnly) {
    CRJNIEnv env(env_);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    LVStreamRef stream = env.jbyteArrayToStream(jbuff);
    lString32 streamName = env.fromJavaString(jStreamName);
    LVDocViewCallbackJNIBridge callback(env_, wrapper->getDocView(), _this);
    CRLog::info("Loading binary document data %s", LCSTR(streamName));
    auto res = doc->LoadDocument(stream, streamName.c_str(), metadataOnly);
    if (res) {
        CRLog::info("Document %s is loaded successfully", LCSTR(streamName));
#ifdef _DEBUG
        CRPropRef doc_props = doc->getDocProps();
        env.dumpPropsToDebugLog(doc_props);
#endif
        return JNI_TRUE;
    } else {
        CRLog::info("Document %s was not loaded due to an error", LCSTR(streamName));
        if (nullptr == doc->getDocument()) {
            // doc->LoadDocument() can return false with doc->m_doc == NULL when:
            // 1. I/O error - failed to open file
            // 2. open archive without supported files
            CRLog::error("Document is NULL, inserting stub.");
            doc->createDefaultDocument(cs32("Error"),
                                       cs32("Error while opening binary document data \"") +
                                       streamName + "\"!");
        }
    }
    return JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    renderPageBitmap_native
 * Signature: (JLandroid/graphics/Bitmap;B)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_renderPageBitmap_1native
        (JNIEnv *env, jobject _this, jlong instance, jobject bitmap, jbyte render_bpp) {
    //CRLog::trace("syncPageBitmap_native() entered : render_bpp=%d", render_bpp);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docview = wrapper->getDocView();
    LVDocViewCallbackJNIBridge callback(env, docview, _this);
    //CRLog::trace("syncPageBitmap_native() calling bitmap->lock");
    LVDrawBuf *drawbuf = BitmapAccessorInterface::getInstance()->lock(env, bitmap);
    if (nullptr != drawbuf) {
        if (render_bpp >= 16) {
            // native color depth
            docview->Draw(*drawbuf, false);
        } else {
            LVGrayDrawBuf grayBuf(drawbuf->GetWidth(), drawbuf->GetHeight(), render_bpp);
            docview->Draw(grayBuf, false);
            grayBuf.DrawTo(drawbuf, 0, 0, 0, nullptr);
        }
        //CRLog::trace("syncPageBitmap_native() calling bitmap->unlock");
        BitmapAccessorInterface::getInstance()->unlock(env, bitmap, drawbuf);
    } else {
        CRLog::error("bitmap accessor is invalid");
    }
    //CRLog::trace("syncPageBitmap_native() exiting");
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    resize_native
 * Signature: (II)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_resize_1native
        (JNIEnv *_env, jobject _this, jlong instance, jint width, jint height) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    LVDocViewCallbackJNIBridge callback(_env, doc, _this);
    doc->Resize(width, height);
    wrapper->updateBatteryIcons();
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    processCommand_native
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_processCommand_1native
        (JNIEnv *_env, jobject _this, jlong instance, jint command, jint param) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    LVDocViewCallbackJNIBridge callback(_env, wrapper->getDocView(), _this);
    //CRLog::trace("processCommand_native(%d, %d) -- passing to LVDocView", cmd, param);
    return wrapper->processCommand((LVDocCmd) command, param) != 0 ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getCurrentPageBookmark_native
 * Signature: (J)Lio/gitlab/coolreader_ng/project_s/Bookmark;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getCurrentPageBookmark_1native
        (JNIEnv *_env, jobject _this, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    if (!doc->isDocumentOpened())
        return nullptr;
    LVDocViewCallbackJNIBridge callback(_env, doc, _this);

    CRLog::trace("getCurrentPageBookmark_native: calling getBookmark()");
    ldomXPointer ptr = doc->getBookmark();
    if (ptr.isNull())
        return nullptr;
    CRBookmark bm(ptr);
    lString32 comment;
    lString32 titleText;
    lString32 posText;
    bm.setType(bmkt_pos);
    if (doc->getBookmarkPosText(ptr, titleText, posText)) {
        bm.setTitleText(titleText);
        bm.setPosText(posText);
    }
    bm.setStartPos(ptr.toString());
    int pos = ptr.toPoint().y;
    int fh = doc->getDocument()->getFullHeight();
    int percent = fh > 0 ? (int) (pos * (lInt64) 10000 / fh) : 0;
    if (percent < 0)
        percent = 0;
    if (percent > 10000)
        percent = 10000;
    bm.setPercent(percent);
    bm.setCommentText(comment);
    // TODO: add security checks: verify return values...
    jclass cls = _env->FindClass(PACKAGE_SIG "/Bookmark");
    jmethodID mid = _env->GetMethodID(cls, "<init>", "()V");
    jobject obj = _env->NewObject(cls, mid);
    CRObjectAccessor acc(_env, obj);
    CRStringField(acc, "startPos").set(bm.getStartPos());
    CRStringField(acc, "endPos").set(bm.getEndPos());
    CRStringField(acc, "titleText").set(bm.getTitleText());
    CRStringField(acc, "posText").set(bm.getPosText());
    CRStringField(acc, "commentText").set(bm.getCommentText());
    CRIntField(acc, "percent").set(bm.getPercent());
    CRIntField(acc, "type").set(bm.getType());
    CRLongField(acc, "timeStamp").set((lInt64) bm.getTimestamp() * 1000);
    return obj;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getPositionProps_native
 * Signature: (Ljava/lang/String;Z)Lio/gitlab/coolreader_ng/project_s/PositionProperties;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getPositionProps_1native
        (JNIEnv *_env, jobject _this, jlong instance, jstring _path, jboolean extInfo) {
    CRJNIEnv env(_env);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    if (!doc->isDocumentOpened())
        CRLog::warn("getPositionProps_native: document is not opened");

    jclass cls = _env->FindClass(PACKAGE_SIG "/PositionProperties");
    jmethodID mid = _env->GetMethodID(cls, "<init>", "()V");
    jobject obj = _env->NewObject(cls, mid);

    LVDocViewCallbackJNIBridge callback(_env, doc, _this);

    lString32 str = env.fromJavaString(_path);
    ldomXPointer bm;
    bool useCurPos; // use current Y position for scroll view mode
    doc->checkPos();
    if (!str.empty()) {
        bm = doc->getDocument()->createXPointer(str);
    } else {
        useCurPos = doc->getViewMode() == DVM_SCROLL;
        if (!useCurPos) {
            // For page turn command 'extInfo' must be 'false' for maximum speed.
            // For bookmarking 'extInfo' must be 'true' for more accurate result (y position).
            bm = doc->getBookmark(extInfo);
            if (bm.isNull()) {
                CRLog::error("getPositionPropsInternal: Cannot get current position bookmark");
            }
        }
    }
    CRObjectAccessor v(_env, obj);
    lvPoint pt = !bm.isNull() ? bm.toPoint() : lvPoint(0, doc->GetPos());
    CRIntField(v, "x").set(pt.x);
    CRIntField(v, "y").set(pt.y);
    CRIntField(v, "fullHeight").set(doc->GetFullHeight());
    CRIntField(v, "pageHeight").set(doc->GetHeight());
    CRIntField(v, "pageWidth").set(doc->GetWidth());
    CRIntField(v, "pageNo").set(doc->getCurPage());
    CRIntField(v, "pageCount").set(doc->getPageCount());
    CRIntField(v, "pageMode").set(doc->getViewMode() == DVM_PAGES ? doc->getVisiblePageCount() : 0);
    if (extInfo) {
        // In some cases, function LVDocView::getPageDocumentRange() is rather slow,
        // so if we don't need these fields 'charCount' and 'imageCount' in the returned result,
        // we just don't call this function.
        // In this case, 'extInfo' must be 'false'.
        doc->getMutex().lock();
        LVRef<ldomXRange> range = doc->getPageDocumentRange(-1);
        doc->getMutex().unlock();
        lString32 text;
        if (!range.isNull())
            text = range->getRangeText();
        int charCount = 0;
        for (int i = 0; i < text.length(); i++) {
            lChar32 ch = text[i];
            if (ch >= '0')
                charCount++;
        }
        CRIntField(v, "charCount").set(charCount);
        CRIntField(v, "imageCount").set(doc->getPageImageCount(range));
    } else {
        CRIntField(v, "charCount").set(0);
        CRIntField(v, "imageCount").set(0);
    }
    return obj;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getAvgTextLineHeight_native
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getAvgTextLineHeight_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    return doc->getAvgTextLineHeight();
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    checkLink_native
 * Signature: (JIII)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkLink_1native
        (JNIEnv *env_, jobject, jlong instance, jint x, jint y, jint delta) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    CRJNIEnv env(env_);
    lString32 link;
    for (int r = 0; r <= delta; r += 5) {
        link = wrapper->getLink(x, y, r);
        if (!link.empty())
            return env.toJavaString(link);
    }
    return nullptr;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    checkBookmark_native
 * Signature: (JIILio/gitlab/coolreader_ng/project_s/Bookmark;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_checkBookmark_1native
        (JNIEnv *env_, jobject, jlong instance, jint x, jint y, jobject bmk) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRObjectAccessor acc(env_, bmk);
    //CRLog::trace("checkBookmark_native(%d, %d)", x, y);
    CRBookmark *found = doc->findBookmarkByPoint(lvPoint(x, y));
    if (!found)
        return JNI_FALSE;
    //CRLog::trace("checkBookmark_native - found bookmark of type %d", found->getType());
    CRIntField(acc, "type").set(found->getType());
    CRIntField(acc, "percent").set(found->getPercent());
    CRStringField(acc, "startPos").set(found->getStartPos());
    CRStringField(acc, "endPos").set(found->getEndPos());
    CRStringField(acc, "titleText").set(found->getTitleText());
    CRStringField(acc, "posText").set(found->getPosText());
    CRStringField(acc, "commentText").set(found->getCommentText());
    CRLongField(acc, "timeStamp").set(found->getTimestamp() * 1000);
    return JNI_TRUE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getEmbeddedImage_native
 * Signature: (JIILio/gitlab/coolreader_ng/project_s/PageImageCache;B)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getEmbeddedImage_1native
        (JNIEnv *env_, jobject, jlong instance, jint x, jint y, jobject dstImage,
         jbyte render_bpp) {
    //CRLog::trace("getEmbeddedImage_native entered");
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    int imgWidth;
    int imgHeight;
    jboolean res = JNI_FALSE;
    CRObjectAccessor accImage(env_, dstImage);
    LVImageSourceRef image = doc->getImageByPoint(lvPoint(x, y));
    if (!image.isNull()) {
        imgWidth = image->GetWidth();
        imgHeight = image->GetHeight();
        if (imgWidth < 8 || imgHeight < 8)
            image.Clear();
    }
    if (!image.isNull()) {
        // create jbitmap
        jclass jc_Bitmap = env_->FindClass("android/graphics/Bitmap");
        jmethodID jm_createBitmap = env_->GetStaticMethodID(jc_Bitmap, "createBitmap",
                                                            "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
        // get enum Bitmap.Config
        jclass jc_BitmapConfig = env_->FindClass("android/graphics/Bitmap$Config");
        jfieldID jf_RGB_565 = env_->GetStaticFieldID(jc_BitmapConfig, "RGB_565",
                                                     "Landroid/graphics/Bitmap$Config;");
        jfieldID jf_ARGB_8888 = env_->GetStaticFieldID(jc_BitmapConfig, "ARGB_8888",
                                                       "Landroid/graphics/Bitmap$Config;");
        jobject config = env_->GetStaticObjectField(jc_BitmapConfig,
                                                    (render_bpp == 32) ? jf_ARGB_8888
                                                                       : jf_RGB_565);
        // TODO: if image is too big make bitmap with reduced size (pass max size as function argument)
        // TODO: use specific mutable bitmap factory if this function not work in all cases
        jobject jbitmap = env_->CallStaticObjectMethod(jc_Bitmap, jm_createBitmap, imgWidth,
                                                       imgHeight, config);
        if (env_->ExceptionCheck() != JNI_TRUE) {
            CRObjectField(accImage, "bitmap", "Landroid/graphics/Bitmap;").set(jbitmap);
            LVDrawBuf *buf = BitmapAccessorInterface::getInstance()->lock(env_, jbitmap);
            if (nullptr != buf) {
                buf->Draw(image, 0, 0, imgWidth, imgHeight, false);
                BitmapAccessorInterface::getInstance()->unlock(env_, jbitmap, buf);
                res = JNI_TRUE;
            }
        } else {
            CRLog::error("Failed to create mutable bitmap with size %dx%d",
                         imgWidth, imgHeight);
            env_->ExceptionClear();
        }
    }
    return res;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    goLink_native
 * Signature: (JLjava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goLink_1native
        (JNIEnv *env_, jobject, jlong instance, jstring link_) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    lString32 link = env.fromJavaString(link_);
    bool res = doc->goLink(link, true);
    return res ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    goToPosition_native
 * Signature: (JLjava/lang/String;Z)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_goToPosition_1native
        (JNIEnv *env_, jobject this_, jlong instance, jstring jxPath, jboolean saveToHistory) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    if (!doc->isDocumentOpened())
        return JNI_FALSE;
    LVDocViewCallbackJNIBridge callback(env_, doc, this_);
    lString32 xPath = env.fromJavaString(jxPath);
    ldomXPointer bm = doc->getDocument()->createXPointer(xPath);
    if (bm.isNull())
        return JNI_FALSE;
    if (saveToHistory)
        doc->savePosToNavigationHistory();
    doc->goToBookmark(bm);
    return JNI_TRUE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getSettings_native
 * Signature: (JLjava/util/Properties;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSettings_1native
        (JNIEnv *env_, jobject, jlong instance, jobject jprops) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    CRPropRef props = doc->propsGetCurrent();
    return env.toJavaProperties(jprops, props) ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    applySettings_native
 * Signature: (JLjava/util/Properties;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applySettings_1native
        (JNIEnv *env_, jobject this_, jlong instance, jobject jProps) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    LVDocViewCallbackJNIBridge callback(env_, doc, this_);
    CRPropRef props = env.fromJavaProperties(jProps);
    if (wrapper->isForcedScrollViewMode()) {
        int intValue;
        if (props->getInt(PROP_PAGE_VIEW_MODE, intValue)) {
            LVDocViewMode viewMode = (LVDocViewMode) intValue;
            wrapper->setPendingViewMode(viewMode);
        }
    }
    doc->propsUpdateDefaults(props);
    if (wrapper->isForcedScrollViewMode()) {
        // Remove PROP_PAGE_VIEW_MODE property
        CRPropRef tmpProps = LVCreatePropsContainer();
        for (int i = 0; i < props->getCount(); i++) {
            const char *name = props->getName(i);
            if (0 != lStr_cmp(name, PROP_PAGE_VIEW_MODE))
                tmpProps->setString(name, props->getValue(i));
        }
        props = tmpProps;
    }
    //CRPropRef oldProps = doc->propsGetCurrent();
    //CRPropRef diff = oldProps ^ props;
    //CRPropRef unknown = doc->propsApply(diff);
    CRPropRef unknown = doc->propsApply(props);
    wrapper->updateBatteryIcons();
    CRLog::trace("applySettings_native - done");
    return JNI_TRUE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setBatteryState_native
 * Signature: (JIII)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBatteryState_1native
        (JNIEnv *, jobject, jlong instance, jint batteryState, jint batteryCharger,
         jint chargeLevel) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    doc->setBatteryState(batteryState, batteryCharger, chargeLevel);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    isTimeChanged_native
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_isTimeChanged_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    return doc->isTimeChanged() ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    updateCache_native
 * Signature: (JJ)I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateCache_1native
        (JNIEnv *, jobject, jlong instance, jlong timeout) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    return wrapper->updateCache(timeout);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    cancelLongCachingOperation_1native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_cancelLongCachingOperation_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    return wrapper->cancelLongCachingOperation();
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    updateBookInfo_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/BookInfo;Z)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_updateBookInfo_1native
        (JNIEnv *env_, jobject this_, jlong instance, jobject jinfo, jboolean updatePath) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    if (!doc->isDocumentOpened()) {
        CRLog::warn("updateBookInfo_native(): document is not open!");
        return;
    }
    CRJNIEnv env(env_);
    LVDocViewCallbackJNIBridge callback(env_, doc, this_);
    env.updateBookInfo(jinfo, doc, updatePath);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    createDefaultDocument_native
 * Signature: (JLjava/lang/String;Ljava/lang/String;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_createDefaultDocument_1native
        (JNIEnv *env_, jobject, jlong instance, jstring title, jstring message) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    lString32 title_str = env.fromJavaString(title);
    lString32 message_str = env.fromJavaString(message);
    doc->createDefaultDocument(title_str, message_str);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setPageBackgroundTexture_1native
 * Signature: (J[BZ)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setPageBackgroundTexture_1native
        (JNIEnv *env_, jobject, jlong instance, jbyteArray jdata, jboolean tiled) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    LVImageSourceRef img;
    if (jdata != nullptr) {
        LVStreamRef stream = env.jbyteArrayToStream(jdata);
        if (!stream.isNull()) {
            img = LVCreateStreamImageSource(stream);
        }
    }
    doc->setBackgroundImage(img, tiled);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getTOC_native
 * Signature: (J)Lio/gitlab/coolreader_ng/project_s/DocumentTOCItem;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getTOC_1native
        (JNIEnv *env_, jobject _this, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    CRJNIEnv env(env_);
    if (!doc->isDocumentOpened())
        return nullptr;
    LVDocViewCallbackJNIBridge callback(env_, doc, _this);
    return env.toJavaTOCItem(doc->getToc());
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    canGoBack_native
 * Signature: (J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_canGoBack_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    return doc->canGoBack() ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getBookCoverData_native
 * Signature: (J)[B
 */
JNIEXPORT jbyteArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getBookCoverData_1native(
        JNIEnv *env_, jobject, jlong instance) {
    CRJNIEnv env(env_);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *doc = wrapper->getDocView();
    jbyteArray array = nullptr;
    if (doc && doc->isDocumentOpened()) {
        LVStreamRef imgStream = doc->getBookCoverImageStream();
        if (!imgStream.isNull())
            array = env.streamToJByteArray(imgStream);
    }
    return array;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    getSelection_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/Selection;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_getSelection_1native
        (JNIEnv *env_, jobject, jlong instance, jobject jSelection) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("getSelection_native: document is not opened");
        return JNI_FALSE;
    }
    ldomXRangeList &selections = docView->getDocument()->getSelections();
    if (selections.length() > 0) {
        ldomXRange currSel;
        currSel = *selections[0];
        if (!currSel.isNull()) {
            CRObjectAccessor selAcc(env_, jSelection);
            CRStringField sel_startPos(selAcc, "startXPtr");
            CRStringField sel_endPos(selAcc, "endXPtr");
            CRStringField sel_text(selAcc, "text");
            CRStringField sel_chapter(selAcc, "chapter");
            CRObjectField sel_startHandleWndPos(selAcc, "startHandleWndPos",
                                                "Landroid/graphics/Point;");
            CRObjectField sel_endHandleWndPos(selAcc, "endHandleWndPos",
                                              "Landroid/graphics/Point;");
            CRObjectField sel_startHandleDocPos(selAcc, "startHandleDocPos",
                                                "Landroid/graphics/Point;");
            CRObjectField sel_endHandleDocPos(selAcc, "endHandleDocPos",
                                              "Landroid/graphics/Point;");
            jobject jStartHandleWndPos = sel_startHandleWndPos.get();
            jobject jEndHandleWndPos = sel_endHandleWndPos.get();
            jobject jStartHandleDocPos = sel_startHandleDocPos.get();
            jobject jEndHandleDocPos = sel_endHandleDocPos.get();
            CRObjectAccessor startHandleWndPosAcc(env_, jStartHandleWndPos);
            CRObjectAccessor endHandleWndPosAcc(env_, jEndHandleWndPos);
            CRObjectAccessor startHandleDocPosAcc(env_, jStartHandleDocPos);
            CRObjectAccessor endHandleDocPosAcc(env_, jEndHandleDocPos);
            CRIntField startHandleWndPos_x(startHandleWndPosAcc, "x");
            CRIntField startHandleWndPos_y(startHandleWndPosAcc, "y");
            CRIntField endHandleWndPos_x(endHandleWndPosAcc, "x");
            CRIntField endHandleWndPos_y(endHandleWndPosAcc, "y");
            CRIntField startHandleDocPos_x(startHandleDocPosAcc, "x");
            CRIntField startHandleDocPos_y(startHandleDocPosAcc, "y");
            CRIntField endHandleDocPos_x(endHandleDocPosAcc, "x");
            CRIntField endHandleDocPos_y(endHandleDocPosAcc, "y");
            CRIntField sel_percent(selAcc, "percent");
            sel_startPos.set(currSel.getStart().toString());
            sel_endPos.set(currSel.getEnd().toString());
            lvRect startRect;
            lvRect endRect;
            currSel.getStart().getRect(startRect);
            currSel.getEnd().getRect(endRect);
            lvPoint startHandle(startRect.left, startRect.bottom);
            lvPoint endHandle(endRect.right, endRect.bottom);
            startHandleDocPos_x.set(startHandle.x);
            startHandleDocPos_y.set(startHandle.y);
            endHandleDocPos_x.set(endHandle.x);
            endHandleDocPos_y.set(endHandle.y);
            docView->docToWindowPoint(startHandle);
            docView->docToWindowPoint(endHandle);
            startHandleWndPos_x.set(startHandle.x);
            startHandleWndPos_y.set(startHandle.y);
            endHandleWndPos_x.set(endHandle.x);
            endHandleWndPos_y.set(endHandle.y);
            int page = docView->getBookmarkPage(currSel.getStart());
            int pages = docView->getPageCount();
            lString32 titleText;
            lString32 posText;
            docView->getBookmarkPosText(currSel.getStart(), titleText, posText);
            int percent = 0;
            if (pages > 1)
                percent = 10000 * page / (pages - 1);
            lString32 selText = currSel.getRangeText('\n', 8192);
            sel_percent.set(percent);
            sel_text.set(selText);
            sel_chapter.set(titleText);
            return JNI_TRUE;
        }
    }
    return JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    selectText_native
 * Signature: (JIIIIZ)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectText_1native
        (JNIEnv *, jobject, jlong instance, jint start_x, jint start_y, jint end_x, jint end_y,
         jboolean sticky_bounds) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("selectText_native: document is not opened");
        return JNI_FALSE;
    }
    lvPoint startpt(start_x, start_y);
    lvPoint endpt(end_x, end_y);
    ldomXPointer startxp = docView->getNodeByPoint(startpt);
    ldomXPointer endxp = docView->getNodeByPoint(endpt);
    if (!startxp.isNull() && !endxp.isNull()) {
        ldomXRange r(startxp, endxp);
        if (!r.getStart().isNull() && !r.getEnd().isNull()) {
            r.sort();
            if (sticky_bounds) {
                if (!r.getStart().isVisibleWordStart())
                    r.getStart().prevVisibleWordStart();
                if (!r.getEnd().isVisibleWordEnd())
                    r.getEnd().nextVisibleWordEnd();
            }
            if (!r.isNull()) {
                //lString32 end = r.getEnd().toString();
                //CRLog::debug("Range: %s - %s", UnicodeToUtf8(start).c_str(), UnicodeToUtf8(end).c_str());
                r.setFlags(1);
                docView->selectRange(r);
                return JNI_TRUE;
            }
        }
    }
    return JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    clearSelection_native
 * Signature: (J)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_clearSelection_1native
        (JNIEnv *, jobject, jlong instance) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("clearSelection_native: document is not opened");
        return;
    }
    docView->clearSelection();
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    docToWindowPoint_native
 * Signature: (JLandroid/graphics/Point;Landroid/graphics/Point;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_docToWindowPoint_1native
        (JNIEnv *env_, jobject, jlong instance, jobject jDst, jobject jSrc) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("docToWindowPoint_native: document is not opened");
        return;
    }
    CRObjectAccessor dstPointAcc(env_, jDst);
    CRIntField dstPoint_x(dstPointAcc, "x");
    CRIntField dstPoint_y(dstPointAcc, "y");
    CRObjectAccessor srcPointAcc(env_, jSrc);
    CRIntField srcPoint_x(srcPointAcc, "x");
    CRIntField srcPoint_y(srcPointAcc, "y");
    lvPoint point(srcPoint_x.get(), srcPoint_y.get());
    docView->docToWindowPoint(point);
    dstPoint_x.set(point.x);
    dstPoint_y.set(point.y);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setBookmarks_native
 * Signature: (J[Lio/gitlab/coolreader_ng/project_s/Bookmark;)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setBookmarks_1native
        (JNIEnv *env_, jobject, jlong instance, jobjectArray jbookmarks) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("setBookmarks_native: document is not opened");
        return;
    }
    LVPtrVector<CRBookmark> bookmarks;
    if (nullptr != jbookmarks) {
        jsize len = env_->GetArrayLength(jbookmarks);
        for (int i = 0; i < len; i++) {
            jobject obj = env_->GetObjectArrayElement(jbookmarks, i);
            CRObjectAccessor bmkAcc(env_, obj);
            CRIntField type(bmkAcc, "type");
            CRIntField percent(bmkAcc, "percent");
            CRStringField startPos(bmkAcc, "startPos");
            CRStringField endPos(bmkAcc, "endPos");
            CRStringField titleText(bmkAcc, "titleText");
            CRStringField posText(bmkAcc, "posText");
            CRStringField commentText(bmkAcc, "commentText");
            CRLongField timestamp(bmkAcc, "timeStamp");
            CRBookmark *bookmark = new CRBookmark(startPos.get(), endPos.get());
            bookmark->setType(type.get());
            bookmark->setPercent(percent.get());
            bookmark->setTitleText(titleText.get());
            bookmark->setPosText(posText.get());
            bookmark->setCommentText(commentText.get());
            bookmark->setTimestamp((time_t) (timestamp.get() / 1000));
            bookmarks.add(bookmark);
            env_->DeleteLocalRef(obj);
        }
    }
    docView->setBookmarkList(bookmarks);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setForceScrollViewMode_native
 * Signature: (JZ)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setForceScrollViewMode_1native
        (JNIEnv *, jobject, jlong instance, jboolean value) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::debug("setForceScrollViewMode_native: document is not opened");
        return JNI_FALSE;
    }
    return wrapper->setForceScrollViewMode(value) ? JNI_TRUE : JNI_FALSE;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    setDecimalPointChar_native
 * Signature: (JC)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_setDecimalPointChar_1native
        (JNIEnv *, jobject, jlong instance, jchar jChar) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    docView->setDecimalPointChar((lChar32) jChar);
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    applyPageInsets_native
 * Signature: (JIIIIZ)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_applyPageInsets_1native
        (JNIEnv *, jobject, jlong instance, jint left, jint top, jint right, jint bottom,
         jboolean allowPageHeaderOverlap) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    lvInsets insets = lvInsets(left, top, right, bottom);
    docView->setPageInsets(insets, allowPageHeaderOverlap);
}

static ldomXPointerEx &
moveXPtrLeftOrToSentenceStart(ldomXPointerEx &xptr, int max_hops, bool *at_start = nullptr) {
    if (nullptr != at_start)
        *at_start = false;
    if (xptr.isNull())
        return xptr;
    if (!xptr.isText() && !xptr.nextVisibleText() && !xptr.prevVisibleText())
        return xptr;
    for (int i = 0; i < max_hops; i++) {
        if (xptr.isSentenceStart()) {
            if (nullptr != at_start)
                *at_start = true;
            break;
        }
        if (!xptr.prevVisibleWordStartInSentence(true))
            return xptr;
    }
    return xptr;
}

static ldomXPointerEx &
moveXPtrRightOrToSentenceEnd(ldomXPointerEx &xptr, int max_hops, bool *at_end = nullptr) {
    if (nullptr != at_end)
        *at_end = false;
    if (xptr.isNull())
        return xptr;
    if (!xptr.isText() && !xptr.nextVisibleText() && !xptr.prevVisibleText())
        return xptr;
    for (int i = 0; i < max_hops; i++) {
        if (xptr.isSentenceEnd()) {
            if (nullptr != at_end)
                *at_end = true;
            break;
        }
        if (!xptr.nextVisibleWordEndInSentence(true))
            break;
    }
    return xptr;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    findTextAll_native
 * Signature: (JLjava/lang/String;ZI)[Lio/gitlab/coolreader_ng/project_s/SearchResultItem;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_findTextAll_1native
        (JNIEnv *env_, jobject, jlong instance, jstring text, jboolean caseSensitivity,
         jint maxCount) {
    CRJNIEnv env(env_);
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::error("findText_native: document is not opened!");
        return nullptr;
    }
    docView->checkRender();
    auto doc = docView->getDocument();
    auto pattern = env.fromJavaString(text);
    LVArray<ldomWord> words;
    bool res = doc->findText(pattern, !caseSensitivity, false, -1, -1, words, maxCount, 0);
    if (res) {
        jclass pjcSearchResultItem = env->FindClass(PACKAGE_SIG "/SearchResultItem");
        if (nullptr == pjcSearchResultItem)
            return nullptr;
        jmethodID pjmSearchResultItem_Ctor = env->GetMethodID(pjcSearchResultItem,
                                                              "<init>",
                                                              "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V");
        if (nullptr == pjmSearchResultItem_Ctor)
            return nullptr;
        int count = words.length();
        jobjectArray jarray = env->NewObjectArray(count, pjcSearchResultItem, nullptr);
        for (int i = 0; i < count; i++) {
            const ldomWord &word = words[i];
            bool at_sentence_start = false;
            bool at_sentence_end = false;
            lString32 titleText;
            lString32 posText;
            ldomXPointerEx xptr1 = word.getStartXPointer();
            ldomXPointerEx xptr2 = word.getEndXPointer();
            ldomXPointer xxptr1(xptr1.getNode(), xptr1.getOffset());
            if (!docView->getBookmarkPosText(xxptr1, titleText, posText)) {
                CRLog::debug("failed to get title for search fragment: xptr=%s",
                             LCSTR(xxptr1.toString()));
            }
            jstring jtitle = env.toJavaString(titleText);
            jstring jxptr1 = env.toJavaString(xptr1.toString());
            jstring jxptr2 = env.toJavaString(xptr2.toString());
            int ypos = xptr1.toPoint().y;
            // move xptr1 left to few words or to sentence begin
            moveXPtrLeftOrToSentenceStart(xptr1, 7, &at_sentence_start);
            // move xptr2 right to few words or to sentence end
            moveXPtrRightOrToSentenceEnd(xptr2, 7, &at_sentence_end);
            ldomXRange range(xptr1, xptr2);
            lString32 textFragment = range.getRangeText();
            if (!at_sentence_start)
                textFragment = cs32("...") + textFragment;
            if (!at_sentence_end)
                textFragment += cs32("...");
            jstring jtext = env.toJavaString(textFragment);
            jint offset;
            jint length;
            if (word.getNode() == xptr1.getNode()) {
                offset = word.getStart() - xptr1.getOffset();
                length = word.getEnd() - word.getStart();
            } else {
                offset = 0;
                length = -1;
            }
            int fh = doc->getFullHeight();
            jint percent = fh > 0 ? (int) (ypos * (lInt64) 10000 / fh) : 0;
            if (percent < 0)
                percent = 0;
            if (percent > 10000)
                percent = 10000;
            jobject jitem = env->NewObject(
                    pjcSearchResultItem,
                    pjmSearchResultItem_Ctor,
                    jtitle,
                    jtext,
                    jxptr1,
                    jxptr2,
                    offset,
                    length,
                    percent
            );
            if (nullptr != jitem) {
                env->SetObjectArrayElement(jarray, i, jitem);
                env->DeleteLocalRef(jitem);
            }
            env->DeleteLocalRef(jtitle);
            env->DeleteLocalRef(jtext);
            env->DeleteLocalRef(jxptr1);
            env->DeleteLocalRef(jxptr2);
        }
        return jarray;
    }
    return nullptr;
}

/*
 * Class:     io_gitlab_coolreader_ng_project_s_LVDocViewWrapper
 * Method:    selectSearchResult_native
 * Signature: (JLio/gitlab/coolreader_ng/project_s/SearchResultItem;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_LVDocViewWrapper_selectSearchResult_1native
        (JNIEnv *env_, jobject, jlong instance, jobject jitem) {
    auto *wrapper = (LVDocViewWrapperPrivate *) instance;
    auto *docView = wrapper->getDocView();
    if (!docView->isDocumentOpened()) {
        CRLog::error("selectSearchResultItem_native: document is not opened!");
        return JNI_FALSE;
    }
    if (nullptr == jitem) {
        CRLog::error("selectSearchResultItem_native: argument can't ne null!");
        return JNI_FALSE;
    }
    auto *doc = docView->getDocument();
    CRObjectAccessor itemAcc(env_, jitem);
    CRStringField startPtrField(itemAcc, "startPtr");
    CRStringField endPtrField(itemAcc, "endPtr");
    lString32 startPtr = startPtrField.get();
    lString32 endPtr = endPtrField.get();
    ldomXPointer xptr1 = doc->createXPointer(startPtr);
    ldomXPointer xptr2 = doc->createXPointer(endPtr);
    if (xptr1.isNull()) {
        CRLog::error("selectSearchResultItem_native: xptr1 is null!");
        return JNI_FALSE;
    }
    if (xptr2.isNull()) {
        CRLog::error("selectSearchResultItem_native: xptr2 is null!");
        return JNI_FALSE;
    }
    docView->clearSelection();
    docView->selectRange(ldomXRange(xptr1, xptr2, 1));
    ldomMarkedRangeList *ranges = docView->getMarkedRanges();
    if (ranges) {
        if (ranges->length() > 0) {
            int pos = ranges->get(0)->start.y;
            docView->SetPos(pos);
        }
    }
    return JNI_TRUE;
}

#ifdef __cplusplus
}
#endif
