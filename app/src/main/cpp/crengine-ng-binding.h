/***************************************************************************
 *   book reader based on crengine-ng                                      *
 *   Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>            *
 *                                                                         *
 *   This program is free software: you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.*
 ***************************************************************************/

/* Header for native methods of java class CREngineNGBinding */

#ifndef CRENGINE_NG_BINDING_H
#define CRENGINE_NG_BINDING_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    engineInit_native
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineInit_1native
        (JNIEnv *, jobject, jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    engineCleanup_native
 * Signature: ()V
 */
JNIEXPORT void JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_engineCleanup_1native
        (JNIEnv *, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    setCacheDir_native
 * Signature: (Ljava/lang/String;J)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setCacheDir_1native
        (JNIEnv *, jobject, jstring, jlong);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    setupHyphenations_native
 * Signature: ([Lio/gitlab/coolreader_ng/project_s/HyphenationDict;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_setupHyphenations_1native
        (JNIEnv *, jobject, jobjectArray);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    initFontsFromDir_native
 * Signature: (Ljava/lang/String;)Z
 */
JNIEXPORT jboolean JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_initFontsFromDir_1native(
        JNIEnv *, jobject, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getBookInfo_native
 * Signature: (Ljava/lang/String;)Lio/gitlab/coolreader_ng/project_s/BookInfo;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookInfo_1native(
        JNIEnv *, jobject, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getBookCoverData_native
 * Signature: (Ljava/lang/String;)[B
 */
JNIEXPORT jbyteArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getBookCoverData_1native(
        JNIEnv *, jobject, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    drawBookCover_native
 * Signature: ([BIIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/Bitmap;
 */
JNIEXPORT jobject JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_drawBookCover_1native(
        JNIEnv *,
        jobject,
        jbyteArray,
        jint,
        jint,
        jboolean,
        jstring,
        jstring,
        jstring,
        jstring,
        jint,
        jint);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getArchiveItems_native
 * Signature: (Ljava/lang/String;)[Lio/gitlab/coolreader_ng/project_s/Triplet;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getArchiveItems_1native(
        JNIEnv *, jclass, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getFontFaceList_native
 * Signature: ()[Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceList_1native(
        JNIEnv *, jobject);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getFontFaceListFiltered_native
 * Signature: (Lio/gitlab/coolreader_ng/project_s/CREngineNGBinding$FontFamily;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getFontFaceListFiltered_1native(
        JNIEnv *, jobject, jobject, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getDOMVersionCurrent_native
 * Signature: ()I
 */
JNIEXPORT jint JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getDOMVersionCurrent_1native(
        JNIEnv *, jclass);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    hyphenateWord_native
 * Signature: (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_hyphenateWord_1native(
        JNIEnv *, jclass, jstring, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getAvailableFontWeight_native
 * Signature: (Ljava/lang/String;)[I
 */
JNIEXPORT jintArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableFontWeight_1native(
        JNIEnv *, jclass, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getAvailableSynthFontWeight_native
 * Signature: ()[I
 */
JNIEXPORT jintArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getAvailableSynthFontWeight_1native(
        JNIEnv *, jclass);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    getHumanReadableLocaleName_native
 * Signature: (Ljava/lang/String;)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_getHumanReadableLocaleName_1native(
        JNIEnv *, jclass, jstring);

/*
 * Class:     io_gitlab_coolreader_ng_project_s_CREngineNGBinding
 * Method:    listFiles_native
 * Signature: (Ljava/io/File;)[Ljava/io/File;
 */
JNIEXPORT jobjectArray JNICALL
Java_io_gitlab_coolreader_1ng_project_1s_CREngineNGBinding_listFiles_1native(
        JNIEnv *, jobject, jobject);

#ifdef __cplusplus
}
#endif

#endif  // CRENGINE_NG_BINDING_H
