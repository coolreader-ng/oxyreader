# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Keep classes and members marked @UsedInJNI
-keep,allowobfuscation @interface io.gitlab.coolreader_ng.project_s.UsedInJNI

-keep @io.gitlab.coolreader_ng.project_s.UsedInJNI class * {*;}

-keepclasseswithmembers class * {
    @io.gitlab.coolreader_ng.project_s.UsedInJNI <methods>;
}

-keepclasseswithmembers class * {
    @io.gitlab.coolreader_ng.project_s.UsedInJNI <fields>;
}

-keepclasseswithmembers class * {
    @io.gitlab.coolreader_ng.project_s.UsedInJNI <init>(...);
}

# below are the classes and fields used on the JNI side.
-keep public class io.gitlab.coolreader_ng.project_s.CREngineNGBinding {
    native <methods>;
}

-keep public class io.gitlab.coolreader_ng.project_s.LVDocViewWrapper {
    native <methods>;
    private io.gitlab.coolreader_ng.project_s.DocViewCallback jni_docviewCallback;
}

-keepclassmembers class * implements io.gitlab.coolreader_ng.project_s.DocViewCallback {
    void onLoadFileStart(java.lang.String);
    java.lang.String onLoadFileFormatDetected(io.gitlab.coolreader_ng.project_s.DocumentFormat);
    void onLoadFileEnd();
    void onLoadFileFirstPagesReady();
    boolean onLoadFileProgress(int);
    void onFormatStart();
    void onFormatEnd();
    boolean onFormatProgress(int);
    boolean onExportProgress(int);
    void onLoadFileError(java.lang.String);
    void onExternalLink(java.lang.String, java.lang.String);
    void onImageCacheClear();
    boolean onRequestReload();
}

# For some reasons @UsedInJNI not work here...
-keepclassmembers public class io.gitlab.coolreader_ng.project_s.CREngineNGBinding$FontFamily {
    int code;
}
