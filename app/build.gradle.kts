plugins {
    id("com.android.application") version "8.8.1"
    id("org.jetbrains.kotlin.android") version "1.9.22"
}

android {
    namespace = "io.gitlab.coolreader_ng.project_s"
    compileSdk = 34

    // Use stable version of the cregine-ng library or not
    val crengine_ng_use_stable = true

    // Path to binary files of the stable crengine-ng (which are built using scripts in ${topsrc}/tools/)
    val crengine_ng_stable_bin_base_path = "${rootDir}/native-libs"
    // Path to binary files of the current/unstable crengine-ng (which are built externally)
    val crengine_ng_git_bin_base_path = "${rootDir}/../build-android"

    val crengine_ng_bin_base_path = if (crengine_ng_use_stable)
        crengine_ng_stable_bin_base_path
    else
        crengine_ng_git_bin_base_path

    defaultConfig {
        applicationId = "io.gitlab.coolreader_ng.lxreader"
        minSdk = 16
        targetSdk = 34
        // For minSDK less than 21
        multiDexEnabled = true
        // 1.2.3   -> 01020300
        // 1.2.3.0 -> 01020300
        // 1.2.3.4 -> 01020304
        versionCode = 80300
        versionName = "0.8.3"
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            externalNativeBuild {
                cmake {
                    val creCommonPath = file("${crengine_ng_bin_base_path}/image").canonicalPath
                    arguments += "-DCRENGINE_NG_COMMON_PREFIX_PATH=${creCommonPath}"
                    arguments += "-DCMAKE_BUILD_TYPE=Release"
                }
            }
        }
        debug {
            isMinifyEnabled = false
            isShrinkResources = false
            isJniDebuggable = true
            isDebuggable = true
            externalNativeBuild {
                cmake {
                    val creCommonPath =
                        file("${crengine_ng_bin_base_path}/image-debug").canonicalPath
                    arguments += "-DCRENGINE_NG_COMMON_PREFIX_PATH=${creCommonPath}"
                    arguments += "-DCMAKE_BUILD_TYPE=Debug"
                }
            }
            packaging {
                jniLibs.keepDebugSymbols.add("**/*.so")
            }
        }
        create("release_debinfo") {
            isMinifyEnabled = false
            isShrinkResources = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            externalNativeBuild {
                cmake {
                    val creCommonPath =
                        file("${crengine_ng_bin_base_path}/image-relwithdebinfo").canonicalPath
                    arguments += "-DCRENGINE_NG_COMMON_PREFIX_PATH=${creCommonPath}"
                    arguments += "-DCMAKE_BUILD_TYPE=RelWithDebInfo"
                }
            }
            packaging {
                jniLibs.keepDebugSymbols.add("**/*.so")
            }
        }
    }

    buildFeatures {
        buildConfig = true
    }

    flavorDimensions += "market"
    productFlavors {
        create("default") {
            dimension = "market"
            applicationIdSuffix = ".full"
            versionNameSuffix = ""
            buildConfigField("boolean", "ENABLE_INT_FM", "true")
        }
        create("gplay") {
            dimension = "market"
            applicationIdSuffix = ".gplay"
            versionNameSuffix = " (gplay)"
            buildConfigField("boolean", "ENABLE_INT_FM", "false")
        }
        create("fdroid") {
            dimension = "market"
            applicationIdSuffix = ".fdroid"
            versionNameSuffix = " (fdroid)"
            buildConfigField("boolean", "ENABLE_INT_FM", "true")
        }
    }

    externalNativeBuild {
        cmake {
            path("src/main/cpp/CMakeLists.txt")
            version = "3.22.1"
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    ndkVersion = "21.4.7075529"
    //ndkVersion = "25.1.8937393"
}

dependencies {
    implementation("androidx.core:core-ktx:1.12.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.activity:activity-ktx:1.8.2")
    implementation("androidx.fragment:fragment-ktx:1.6.2")

    // documentfile
    implementation("androidx.documentfile:documentfile:1.0.1")
    // RecyclerView
    implementation("androidx.recyclerview:recyclerview:1.3.2")
    // ViewPager2
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    // Material design
    implementation("com.google.android.material:material:1.11.0")
    // Android Preferences
    implementation("androidx.preference:preference-ktx:1.2.1")
    // Flexbox Layout
    implementation("com.google.android.flexbox:flexbox:3.0.0")
    // SwipeRefreshLayout
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")

    // For minSDK less than 21
    implementation("androidx.multidex:multidex:2.0.1")

    implementation(project(":genrescollection"))
    implementation(project(":mygesturedetector"))
    implementation(project(":TextValidators"))
    implementation(project(":MyColorSelectorView"))
    implementation(project(":ViewPager2NestedScroll"))
    implementation(project(":MyFlexRowLayout"))
}
