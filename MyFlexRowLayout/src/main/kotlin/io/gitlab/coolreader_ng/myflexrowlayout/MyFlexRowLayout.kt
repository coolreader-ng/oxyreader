/*
 * Multi-row layout with automatic splitting into multiple pages when space is limited
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.myflexrowlayout

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.Button


class MyFlexRowLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    ViewGroup(context, attrs, defStyleAttr) {

    private class ChildLayoutData(var view: View? = null) {
        var leftPos = 0
        var cellWidth = 0
        var cellHeight = 0
    }

    private class Row : ArrayList<ChildLayoutData>() {
        var topPos = 0
        var rowHeight = 0
        var rowWidth = 0
    }

    private class Page : ArrayList<Row>() {
        var pageWidth = 0
        var pageHeight = 0
    }

    private class BuildLayoutData(val widthMeasureSpec: Int, val heightMeasureSpec: Int) {
        val wLimit: Int         // maximum allowed width
        val hLimit: Int         // maximum allowed height
        var itemNo = 0          // current item # (continuous)
        var rowNo = 0           // current row # (continuous)
        var pageNo = 0          // current page #
        lateinit var row: Row   // current row
        lateinit var page: Page // current page

        init {
            val wMode = MeasureSpec.getMode(widthMeasureSpec)
            val wSize = MeasureSpec.getSize(widthMeasureSpec)
            val hMode = MeasureSpec.getMode(heightMeasureSpec)
            val hSize = MeasureSpec.getSize(heightMeasureSpec)
            wLimit = when (wMode) {
                MeasureSpec.AT_MOST -> wSize
                MeasureSpec.EXACTLY -> wSize
                else -> Int.MAX_VALUE
            }
            hLimit = when (hMode) {
                MeasureSpec.AT_MOST -> hSize
                MeasureSpec.EXACTLY -> hSize
                else -> Int.MAX_VALUE
            }
        }
    }

    var goNextButton: Button? = null
        set(value) {
            field = value
            field?.let { btn ->
                btn.setOnClickListener {
                    if (mCurrentPage < mPageCount) {
                        mCurrentPage++
                        requestLayout()
                        invalidate()
                    }
                }
            }
        }

    private var mCurrentPage = 0

    // all pages
    private val mPages = ArrayList<Page>()

    // total pages
    private var mPageCount = 0

    // all rows on all pages
    private val mRows = ArrayList<Row>()

    // total row count (continuous)
    private var mRowCount = 0

    // All items
    private val mItemsData = ArrayList<ChildLayoutData>()   // all items

    // items count
    private var mItemsCount = 0

    private val mTmpContainerRect = Rect()

    fun reset() {
        if (0 != mCurrentPage) {
            mCurrentPage = 0
            requestLayout()
            invalidate()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        buildChildLayout(widthMeasureSpec, heightMeasureSpec)
        // Hide all item on inactive pages
        for (i in 0 until mCurrentPage) {
            val page = mPages[i]
            for (row in page) {
                for (cld in row) {
                    cld.view?.visibility = View.GONE
                }
            }
        }
        for (i in mCurrentPage + 1 until mPageCount) {
            val page = mPages[i]
            for (row in page) {
                for (cld in row) {
                    cld.view?.visibility = View.GONE
                }
            }
        }
        val page = mPages[mCurrentPage]
        var width = page.pageWidth
        var height = page.pageHeight
        width = width.coerceAtLeast(suggestedMinimumWidth)
        height = height.coerceAtLeast(suggestedMinimumHeight)
        var childState = 0
        for (row in page) {
            for (item in row) {
                item.view?.let {
                    it.visibility = View.VISIBLE
                    childState = combineMeasuredStates(childState, it.measuredState)
                }
            }
        }
        setMeasuredDimension(
            resolveSizeAndState(width, widthMeasureSpec, childState),
            resolveSizeAndState(
                height,
                heightMeasureSpec,
                childState shl MEASURED_HEIGHT_STATE_SHIFT
            )
        )
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var i = 0
        if (mCurrentPage in 0..<mPageCount) {
            val page = mPages[mCurrentPage]
            for (row in page) {
                for (cld in row) {
                    cld.view?.let {
                        if (it.visibility != GONE) {
                            mTmpContainerRect.left = cld.leftPos
                            mTmpContainerRect.right = mTmpContainerRect.left + cld.cellWidth
                            mTmpContainerRect.top = row.topPos
                            mTmpContainerRect.bottom = mTmpContainerRect.top + row.rowHeight
                            val lp = it.layoutParams
                            if (lp is MarginLayoutParams) {
                                mTmpContainerRect.left += lp.leftMargin
                                mTmpContainerRect.right += lp.rightMargin
                                mTmpContainerRect.top += lp.topMargin
                                mTmpContainerRect.bottom += lp.bottomMargin
                            }
                            it.layout(
                                mTmpContainerRect.left, mTmpContainerRect.top,
                                mTmpContainerRect.right, mTmpContainerRect.bottom
                            )
                        }
                    }
                    i++
                }
            }
        }
    }

    private fun buildChildLayout(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val bld = BuildLayoutData(widthMeasureSpec, heightMeasureSpec)
        bld.apply {
            pageNo = 0
            rowNo = 0
            itemNo = 0
            page = mPages[pageNo].apply {
                clear()
                pageWidth = paddingLeft + paddingRight
                pageHeight = paddingTop + paddingBottom
            }
            row = mRows[rowNo].apply {
                clear()
                topPos = paddingTop
                rowHeight = 0
                rowWidth = 0
            }
        }
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            if (child != goNextButton) {
                doAddItem(child, bld)
            }
        }
        if (bld.row.isNotEmpty())
            doAddCurrentRowToCurrentPage(bld)
        mItemsCount = bld.itemNo
        mRowCount = if (bld.row.isEmpty())
            bld.rowNo
        else
            bld.rowNo + 1
        mPageCount = bld.pageNo + 1
        // Zerofill unused rows & pages
        for (i in mItemsCount until MAX_ITEMS) {
            mItemsData[i].also {
                it.view = null
                it.leftPos = 0
                it.cellWidth = 0
                it.cellHeight = 0
            }
        }
        for (i in mRowCount until MAX_ROWS) {
            mRows[i].also {
                it.clear()
                it.rowHeight = 0
                it.rowWidth = 0
            }
        }
        for (i in mPageCount until MAX_PAGES) {
            mPages[i].also {
                it.clear()
                it.pageWidth = 0
                it.pageHeight = 0
            }
        }
        doJustifyItems(bld)
    }

    private fun doAddItem(child: View, bld: BuildLayoutData) {
        bld.apply {
            var cld = mItemsData[itemNo]
            cld.view = child
            measureChildWithMargins(
                child,
                widthMeasureSpec,
                0,
                heightMeasureSpec,
                0
            )
            var childWidth = child.measuredWidth
            var childHeight = child.measuredHeight
            val lp = child.layoutParams
            if (lp is MarginLayoutParams) {
                childWidth += lp.leftMargin + lp.rightMargin
                childHeight += lp.topMargin + lp.bottomMargin
            }
            cld.cellWidth = childWidth
            cld.cellHeight = childHeight
            val testWidth = row.rowWidth + childWidth
            if (testWidth + paddingLeft + paddingRight <= wLimit) {
                // Add to current row
                cld.leftPos = paddingLeft + row.rowWidth
                row.add(cld)
                row.rowHeight = row.rowHeight.coerceAtLeast(childHeight)
                row.rowWidth = testWidth
            } else {
                val tmp = itemNo
                doAddCurrentRowToCurrentPage(bld)
                // After it the new row must be created
                // Also the current item may have been changed
                if (tmp != itemNo) {
                    cld = mItemsData[itemNo]
                    cld.view = child
                    cld.cellWidth = childWidth
                    cld.cellHeight = childHeight
                }
                cld.leftPos = paddingLeft + row.rowWidth
                row.add(cld)
                row.rowHeight = row.rowHeight.coerceAtLeast(childHeight)
                row.rowWidth += childWidth
            }
            itemNo++
        }
    }

    private fun doAddCurrentRowToCurrentPage(bld: BuildLayoutData) {
        // Add current row to current page
        // Prepare new row
        bld.apply {
            val testHeight = page.pageHeight + row.rowHeight
            if (testHeight < hLimit) {
                // add to current page
                val newRowPos = row.topPos + row.rowHeight
                page.add(row)
                page.pageWidth = page.pageWidth.coerceAtLeast(row.rowWidth)
                page.pageHeight = testHeight
                // prepare new row
                rowNo++
                row = mRows[rowNo]
                row.clear()
                row.topPos = newRowPos
                row.rowHeight = 0
                row.rowWidth = 0
            } else {
                // Current page overflow
                val btn = goNextButton
                if (null != btn) {
                    // Save all buttons below this page, as well as the last button on this page
                    // There itemNo it's number of the next free cell
                    val savedButtons = ArrayList<View>()
                    val startIdx = itemNo - row.size - 1
                    for (i in startIdx until itemNo)
                        mItemsData[i].view?.let { savedButtons.add(it) }
                    // Insert 'go next' button
                    val parent = (btn.parent as ViewGroup?)
                    if (parent !== this@MyFlexRowLayout) {
                        parent?.removeView(btn)
                        addView(
                            btn,
                            MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
                        )
                    }
                    itemNo = startIdx
                    val cld = mItemsData[itemNo]
                    cld.view = btn
                    // Measure new button
                    measureChildWithMargins(
                        btn,
                        widthMeasureSpec,
                        0,
                        heightMeasureSpec,
                        0
                    )
                    var childWidth = btn.measuredWidth
                    var childHeight = btn.measuredHeight
                    val lp = btn.layoutParams
                    if (lp is MarginLayoutParams) {
                        childWidth += lp.leftMargin + lp.rightMargin
                        childHeight += lp.topMargin + lp.bottomMargin
                    }
                    cld.cellWidth = childWidth
                    cld.cellHeight = childHeight
                    itemNo++
                    // Recalculate previous row height & page width & page height
                    val prevRow = mRows[rowNo - 1]
                    val oldPrevHeight = prevRow.rowHeight
                    prevRow.rowHeight = 0
                    prevRow.rowWidth = 0
                    for (item in prevRow) {
                        prevRow.rowHeight = prevRow.rowHeight.coerceAtLeast(item.cellHeight)
                        prevRow.rowWidth += item.cellWidth
                    }
                    page.pageHeight = page.pageHeight - oldPrevHeight + prevRow.rowHeight
                    page.pageWidth = paddingLeft + paddingRight
                    for (r in page)
                        page.pageWidth = page.pageWidth.coerceAtLeast(r.rowWidth)
                    // Create new page
                    pageNo++
                    page = mPages[pageNo]
                    page.clear()
                    page.pageWidth = paddingLeft + paddingRight
                    page.pageHeight = paddingTop + paddingBottom
                    // reset/prepare current row
                    row.clear()
                    row.topPos = paddingTop
                    row.rowHeight = 0
                    row.rowWidth = 0
                    for (b in savedButtons)
                        doAddItem(b, bld)
                } else {
                    pageNo++
                    page = mPages[pageNo]
                    page.clear()
                    page.pageWidth = paddingLeft + paddingRight
                    page.pageHeight = paddingTop + paddingBottom
                    row.topPos = paddingTop
                    page.add(row)
                    page.pageWidth = page.pageWidth.coerceAtLeast(row.rowWidth)
                    page.pageHeight += row.rowHeight
                    // Create new page
                    val newRowPos = paddingTop + row.rowHeight
                    // prepare new row
                    rowNo++
                    row = mRows[rowNo]
                    row.clear()
                    row.topPos = newRowPos
                    row.rowHeight = 0
                    row.rowWidth = 0
                }
            }
        }
    }

    private fun doJustifyItems(bld: BuildLayoutData) {
        bld.apply {
            for (page in mPages) {
                if (page.isEmpty())
                    break
                for (row in page) {
                    if (row.size < 2)
                        break
                    var occupied = 0
                    for (cld in row) {
                        occupied += cld.cellWidth
                    }
                    val freeSpace = wLimit - paddingLeft - paddingRight - occupied
                    val padding = freeSpace / (row.size - 1)
                    for (i in 1 until row.size) {
                        val cld = row[i]
                        cld.leftPos += i * padding
                    }
                }
            }
        }
    }

    init {
        // Preallocate page data
        for (i in 0 until MAX_PAGES) {
            val page = Page().apply { pageWidth = 0; pageHeight = 0 }
            mPages.add(page)
        }
        for (i in 0 until MAX_ROWS)
            mRows.add(Row().apply { rowHeight = 0; rowWidth = 0 })
        for (i in 0 until MAX_ITEMS)
            mItemsData.add(ChildLayoutData(null))
    }

    companion object {
        private const val MAX_PAGES = 10    // Maximum pages
        private const val MAX_ROWS = 300    // Total rows on all pages
        private const val MAX_ITEMS = 1000  // Total item on all pages
    }
}