#!/usr/bin/perl -w

$TARGET_DIR = "../../app/src/main/res/";

$INKSCAPE = "inkscape";
#$INKSCAPE = "/c/Progra~1/inkscape/inkscape.exe -e";

sub do_convert($$);

#                      dpi: 120       160       240        320         480            640
my %ic_actions_sizes  = (ldpi=>24, mdpi=>32, hdpi=>48, xhdpi=>64, xxhdpi=>96, xxxhdpi=>128);
my %ic_tabs_sizes     = (ldpi=>30, mdpi=>40, hdpi=>60, xhdpi=>80, xxhdpi=>120, xxxhdpi=>160);
my %ic_menu_sizes     = (ldpi=>36, mdpi=>48, hdpi=>72, xhdpi=>96, xxhdpi=>144, xxxhdpi=>192);
my %ic_launcher_sizes = (ldpi=>36, mdpi=>48, hdpi=>72, xhdpi=>96, xxhdpi=>144, xxxhdpi=>192);
my %ic_bigicons_sizes = (ldpi=>36, mdpi=>48, hdpi=>72, xhdpi=>96, xxhdpi=>144, xxxhdpi=>192);

my %ic_actions_list = (
    'cr3_browser_book-48x48-src.svg' => 'cr3_browser_book.png',
    'cr3_browser_book_chm-48x48-src.svg' => 'cr3_browser_book_chm.png',
    'cr3_browser_book_doc-48x48-src.svg' => 'cr3_browser_book_doc.png',
    'cr3_browser_book_epub-48x48-src.svg' => 'cr3_browser_book_epub.png',
    'cr3_browser_book_fb2-48x48-src.svg' => 'cr3_browser_book_fb2.png',
    'cr3_browser_book_fb3-48x48-src.svg' => 'cr3_browser_book_fb3.png',
    'cr3_browser_book_html-48x48-src.svg' => 'cr3_browser_book_html.png',
    'cr3_browser_book_md-48x48-src.svg' => 'cr3_browser_book_md.png',
    'cr3_browser_book_pdb-48x48-src.svg' => 'cr3_browser_book_pdb.png',
    'cr3_browser_book_rtf-48x48-src.svg' => 'cr3_browser_book_rtf.png',
    'cr3_browser_book_txt-48x48-src.svg' => 'cr3_browser_book_txt.png',
    'cr3_browser_book_odt-48x48-src.svg' => 'cr3_browser_book_odt.png',
    'mime-folder-48x48-src.svg' => 'mime_folder.png',
    'mime-text-x-generic-template-48x48-src.svg' => 'mime_text_x_generic_template.png'
);

my %ic_tabs_list=(
);

my %ic_menu_list=(
);

my %ic_bigicons_list=(
);

do_convert(\%ic_actions_list,  \%ic_actions_sizes);
do_convert(\%ic_tabs_list,     \%ic_tabs_sizes);
do_convert(\%ic_menu_list,     \%ic_menu_sizes);
do_convert(\%ic_bigicons_list, \%ic_bigicons_sizes);

1;

# functions

sub do_convert($$) {
    my ($src_listref, $src_sizesref) = @_;
    my %src_list = %$src_listref;
    my %src_sizes = %$src_sizesref;

    my ($srcfile, $dstfile);
    my ($dpi, $size);
    my $folder;
    my $resfile;
    my $cmd;
    my $ret;
    my ($srcmtime, $resmtime);

    while (($srcfile, $dstfile) = each(%src_list)) {
        if (-f $srcfile) {
            (undef,undef,undef,undef,undef,undef,undef,undef,undef,$srcmtime,undef,undef,undef) = stat($srcfile);
            while (($dpi, $size) = each(%src_sizes)) {
                $folder = "${TARGET_DIR}/mipmap-${dpi}/";
                if (-d $folder) {
                    $resfile = "${folder}/${dstfile}";
                    $resmtime = 0;
                    if (-f $resfile) {
                        (undef,undef,undef,undef,undef,undef,undef,undef,undef,$resmtime,undef,undef,undef) = stat($resfile);
                    }
                    if ($srcmtime > $resmtime) {
                        $cmd = "${INKSCAPE} --export-filename=${resfile} --export-overwrite --export-width=${size} --export-height=${size} ${srcfile}";
                        print "$cmd\n";
                        $ret = system($cmd);
                        print "Failed!\n" if $ret != 0;
                    } else {
                        print "File \"${srcfile}\" is not newer than \"${resfile}\", skiping.\n";
                    }
                }
            }
        }
    }
}
