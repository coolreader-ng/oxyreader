/*
 * Text Validators
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.textvalidators

import android.content.Context
import android.text.InputType
import java.security.InvalidParameterException

class IntValidator(context: Context, private val minimum: Int, private val maximum: Int) :
    AbstractValidator(context) {

    override fun validate(str: String): ValidationResult {
        val res = ValidationResult()
        val number: Int
        try {
            number = str.toInt()
        } catch (e: NumberFormatException) {
            errorString = mContext.getString(R.string.invalid_number_format)
            res.replacement = minimum.toString()
            return res
        }
        if (number < minimum) {
            errorString = mContext.getString(R.string.value_too_small)
            res.replacement = minimum.toString()
            return res
        } else if (number > maximum) {
            errorString = mContext.getString(R.string.value_too_big)
            res.replacement = maximum.toString()
            res.needUndo = true
            return res
        }
        res.res = true
        errorString = null
        return res
    }

    override val inputType: Int
        get() = if (minimum < 0) InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED else InputType.TYPE_CLASS_NUMBER

    init {
        if (minimum >= maximum)
            throw InvalidParameterException("argument 'minimum' always must be less than 'maximum'")
    }
}