/*
 * Text Validators
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package io.gitlab.coolreader_ng.textvalidators

import android.content.Context
import android.text.InputType
import java.security.InvalidParameterException

class DoubleValidator(
    context: Context,
    private val minimum: Double,
    private val maximum: Double,
    private val decimals: Int
) : AbstractValidator(context) {

    override fun validate(str: String): ValidationResult {
        val res = ValidationResult()

        // check number of digits after decimal point
        var dot_pos = str.indexOf('.')
        if (dot_pos == -1) dot_pos = str.indexOf(',')
        if (dot_pos >= 0) {
            val tdecimals = str.length - dot_pos - 1
            if (tdecimals > decimals) {
                errorString = mContext.getString(R.string.too_much_decimals)
                res.needUndo = true
                return res
            }
        }

        // check range
        val tval: Double
        try {
            tval = str.toDouble()
        } catch (e: NumberFormatException) {
            errorString = mContext.getString(R.string.invalid_number_format)
            return res
        }
        if (tval < minimum) {
            errorString = mContext.getString(R.string.value_too_small)
            return res
        }
        if (tval > maximum) {
            errorString = mContext.getString(R.string.value_too_big)
            res.needUndo = true
            return res
        }
        res.res = true
        errorString = null
        return res
    }

    override val inputType: Int
        get() {
            var type = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            if (minimum < 0)
                type = type or InputType.TYPE_NUMBER_FLAG_SIGNED
            return type
        }

    init {
        if (minimum >= maximum) throw InvalidParameterException("argument min always must be less than max")
    }
}