/*
 * Text Validators
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.textvalidators

import android.content.Context

/**
 * Abstract class of the validator.
 */
abstract class AbstractValidator internal constructor(internal var mContext: Context) {

    data class ValidationResult(
        var res: Boolean = false,
        var needUndo: Boolean = false,
        var replacement: String? = null
    )

    /**
     * Validation function.
     * A child class must override this function to implement the required validation procedure.
     *
     * @param str string for which this function must work.
     * @return validation result in the form of ValidationResult
     */
    abstract fun validate(str: String): ValidationResult

    /**
     * Input type.
     * Implement this property in a child class to specify a valid input type,
     * such as android.text.InputType.TYPE_CLASS_NUMBER | android.text.InputType.TYPE_NUMBER_FLAG_SIGNED.
     */
    abstract val inputType: Int

    /**
     * In this property, the child class should store a textual description of the validation error when it occurs.
     * Can be null, this will be interpreted as the absence of an error.
     */
    var errorString: String? = null
        internal set
}