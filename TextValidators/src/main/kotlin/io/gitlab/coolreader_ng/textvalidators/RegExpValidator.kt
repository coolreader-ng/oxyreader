/*
 * Text Validators
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.textvalidators

import android.content.Context
import android.text.InputType
import java.util.regex.Pattern

class RegExpValidator(context: Context, regexp: String) : AbstractValidator(context) {
    private val mPattern: Pattern

    override fun validate(str: String): ValidationResult {
        val res = ValidationResult()
        if (mPattern.matcher(str).matches()) {
            res.res = true
            errorString = null
        } else {
            if (str.isNotEmpty())
                res.needUndo = true
            errorString = mContext.getString(R.string.regexp_dont_matches)
        }
        return res
    }

    override val inputType: Int
        get() = InputType.TYPE_CLASS_TEXT

    init {
        mPattern = Pattern.compile(regexp)
    }
}