#!/bin/sh

###########################################################################
#   LxReader                                                              #
#   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               #
#                                                                         #
#   This program is free software: you can redistribute it and/or modify  #
#   it under the terms of the GNU General Public License as published by  #
#   the Free Software Foundation, either version 3 of the License, or     #
#   (at your option) any later version.                                   #
#                                                                         #
#   This program is distributed in the hope that it will be useful,       #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#   GNU General Public License for more details.                          #
#                                                                         #
#   You should have received a copy of the GNU General Public License     #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.#
###########################################################################

crengine_ng_dir="../../native-libs/image/arm64-v8a/"
top_srcdir="../.."

src_hyphdict_dir="${crengine_ng_dir}/share/crengine-ng/hyph"
raw_res_dir="${top_srcdir}/app/src/main/res/raw"

# Cleanup
old_hyphdicts=`ls ${raw_res_dir}/*.pattern 2>/dev/null`
for f in ${old_hyphdicts}
do
    rm -f "${f}"
done

# copy from crengine-ng
new_hyphdicts=`ls ${src_hyphdict_dir}/*.pattern 2>/dev/null`
for f in ${new_hyphdicts}
do
    #echo "${f}"
    hd_path=`dirname ${f}`
    hd_name=`basename ${f}`
    hd_name=`echo ${hd_name} | tr '[:upper:]' '[:lower:]'`
    hd_name=`echo ${hd_name} | sed -e 's/[-,]/_/g'`
    new_f="${raw_res_dir}/${hd_name}"
    #echo "${new_f}"
    cp -pv "${f}" "${new_f}"
done
