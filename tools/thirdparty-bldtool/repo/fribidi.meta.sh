
# Metadata & build instructions for deploy script

PN="fribidi"
PV="1.0.15"
# package revision: when patchset is changed (but not version), increase it
# when version changed, reset to "1".
REV="1"
SRCFILE="${PN}-${PV}.tar.xz"
SHA512="98295f1a7203f401d63cc1da7cce3be6975729055fea54640d25cf05a6e6bc27d2e1a3f8edd1ddf4c7fc5ff6f7f1e2daf2bf86683d4aed5988ade8bfa5da414f"

URL="https://github.com/fribidi/fribidi/releases/download/v${PV}/${SRCFILE}"

SOURCESDIR="${PN}-${PV}"

PATCHES="01-dont-check-native-c.patch"

src_configure() {
	cd "${BUILDDIR}" || die "chdir failed!"
	if [ -n "${CHOST}" ]
	then
		add_args="${add_args} --host=${CHOST} --target=${CHOST}"
	fi
	../${SOURCESDIR}/configure --prefix=${PREFIX} --enable-static --disable-shared \
		${add_args}
}

src_compile() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake V=1 || die "make failed!"
}

src_install() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake install || die "make install failed!"
}
