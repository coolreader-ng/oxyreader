
# Metadata & build instructions for deploy script

PN="utf8proc"
PV="2.9.0"
# package revision: when patchset is changed (but not version), increase it
# when version changed, reset to "1".
REV="2"
SRCFILE="${PN}-${PV}.tar.gz"
SHA512="fef52e9fabd77efdd42c31a96a80c792cb912ad3158354cec6b260c81062444e35dd0c9c8ff311fe6a540f0022dc2f26f007afd2578e86a31a0ac74ecfc3456f"

URL="https://github.com/JuliaStrings/utf8proc/releases/download/v${PV}/${SRCFILE}"

SOURCESDIR="${PN}-${PV}"

PATCHES="01-cmp0057.patch"

src_configure() {
	cd "${BUILDDIR}" || die "chdir failed!"
	cmake -G"${CMAKE_GENERATOR}" \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX="${PREFIX}" \
		-DBUILD_SHARED_LIBS=OFF \
		-DUTF8PROC_INSTALL=ON \
		-DUTF8PROC_ENABLE_TESTING=OFF \
		${CMAKE_ADD_ARGS} \
		-DCMAKE_C_FLAGS="${CFLAGS}" \
		-DCMAKE_CXX_FLAGS="${CXXFLAGS}" \
		../${SOURCESDIR}
}

src_compile() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake VERBOSE=1 || die "make failed!"
}

src_install() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake install || die "make install failed!"
}
