
# Metadata & build instructions for deploy script

PN="zstd"
PV="1.5.6"
# package revision: when patchset is changed (but not version), increase it
# when version changed, reset to "1".
REV="1"
SRCFILE="${PN}-${PV}.tar.gz"
SHA512="54a578f2484da0520a6e9a24f501b9540a3fe3806785d6bc9db79fc095b7c142a7c121387c7eecd460ca71446603584ef1ba4d29a33ca90873338c9ffbd04f14"

URL="https://github.com/facebook/zstd/releases/download/v${PV}/${SRCFILE}"
IGNORE_TAR_ERRORS=y

SOURCESDIR="${PN}-${PV}"

PATCHES=

src_configure() {
	cd "${BUILDDIR}" || die "chdir failed!"
	cmake -G"${CMAKE_GENERATOR}" \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX="${PREFIX}" \
		-DZSTD_BUILD_SHARED=OFF \
		-DZSTD_BUILD_STATIC=ON \
		-DZSTD_LEGACY_SUPPORT=OFF \
		-DZSTD_MULTITHREAD_SUPPORT=OFF \
		-DZSTD_BUILD_PROGRAMS=OFF \
		-DZSTD_BUILD_CONTRIB=OFF \
		${CMAKE_ADD_ARGS} \
		-DCMAKE_C_FLAGS="${CFLAGS}" \
		-DCMAKE_CXX_FLAGS="${CXXFLAGS}" \
		../${SOURCESDIR}/build/cmake
}

src_compile() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake VERBOSE=1 || die "make failed!"
}

src_install() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake install || die "make install failed!"
}
