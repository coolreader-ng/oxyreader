
# Metadata & build instructions for deploy script

PN="freetype"
PV="2.13.2"
# package revision: when patchset is changed (but not version), increase it
# when version changed, reset to "1".
REV="1"
SRCFILE="${PN}-${PV}.tar.xz"
SHA512="a5917edaa45cb9f75786f8a4f9d12fdf07529247e09dfdb6c0cf7feb08f7588bb24f7b5b11425fb47f8fd62fcb426e731c944658f6d5a59ce4458ad5b0a50194"
URL="https://download.savannah.gnu.org/releases/${PN}/${SRCFILE}"

SOURCESDIR="${PN}-${PV}"

PATCHES=

#src_configure() {
#	cd "${BUILDDIR}" || die "chdir failed!"
#	local add_args=
#	case "${CTARGET}" in
#	*-apple-macos*)
#		add_args="${add_args} --with-fsspec --with-fsref --with-quickdraw-toolbox --with-quickdraw-carbon --with-ats"
#		;;
#	esac
#	if [ -n "${CHOST}" ]
#	then
#		add_args="${add_args} --host=${CHOST} --target=${CHOST}"
#	fi
#	../${SOURCESDIR}/configure --prefix=${PREFIX} --enable-static --disable-shared \
#		--with-zlib=yes --with-bzip2=no --with-png=yes --with-harfbuzz=yes --with-brotli=no \
#		${add_args}
#}

src_configure() {
	cd "${BUILDDIR}" || die "chdir failed!"
	cmake -G"${CMAKE_GENERATOR}" \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX="${PREFIX}" \
		-DBUILD_SHARED_LIBS=OFF \
		-DFT_REQUIRE_ZLIB=ON \
		-DFT_DISABLE_BZIP2=ON \
		-DFT_REQUIRE_PNG=ON \
		-DFT_REQUIRE_HARFBUZZ=ON \
		-DFT_DISABLE_BROTLI=ON \
		-DPNG_PNG_INCLUDE_DIR="${PREFIX}/include/" \
		-DPNG_LIBRARY="${PREFIX}/lib/libpng16.a" \
		-DHarfBuzz_INCLUDE_DIR="${PREFIX}/include/harfbuzz" \
		-DHarfBuzz_LIBRARY="${PREFIX}/lib/libharfbuzz.a" \
		${CMAKE_ADD_ARGS} \
		-DCMAKE_C_FLAGS="${CFLAGS}" \
		-DCMAKE_CXX_FLAGS="${CXXFLAGS}" \
		../${SOURCESDIR}
}

src_compile() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake VERBOSE=1 || die "make failed!"
}

src_install() {
	cd "${BUILDDIR}" || die "chdir failed!"
	domake install || die "make install failed!"
}
