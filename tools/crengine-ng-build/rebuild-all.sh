#!/bin/bash

###########################################################################
#   LxReader, crengine-ng builder                                         #
#   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               #
#                                                                         #
#   This program is free software: you can redistribute it and/or modify  #
#   it under the terms of the GNU General Public License as published by  #
#   the Free Software Foundation, either version 3 of the License, or     #
#   (at your option) any later version.                                   #
#                                                                         #
#   This program is distributed in the hope that it will be useful,       #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#   GNU General Public License for more details.                          #
#                                                                         #
#   You should have received a copy of the GNU General Public License     #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.#
###########################################################################

die()
{
	echo $*
	exit 1
}

# lowercase $1
#buildtype=${1,,}
buildtype=`echo $1 | tr '[:upper:]' '[:lower:]'`
if [ "x${buildtype}" = "x" ]
then
	echo "You must specify build type, one of:"
	echo "  release"
	echo "  debug"
	echo "  relwithdebinfo"
	die "Terminated"
fi

./build-all.sh ${buildtype} clean
