#!/bin/bash

###########################################################################
#   LxReader, crengine-ng builder                                         #
#   Copyright (C) 2024 Aleksey Chernov <valexlin@gmail.com>               #
#                                                                         #
#   This program is free software: you can redistribute it and/or modify  #
#   it under the terms of the GNU General Public License as published by  #
#   the Free Software Foundation, either version 3 of the License, or     #
#   (at your option) any later version.                                   #
#                                                                         #
#   This program is distributed in the hope that it will be useful,       #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#   GNU General Public License for more details.                          #
#                                                                         #
#   You should have received a copy of the GNU General Public License     #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.#
###########################################################################

#  In make.conf the following variables must be defined:
# ANDROID_HOME
# ANDROID_NDK_ROOT
# ANDROID_NDK_HOST_TAG
# ANDROID_CMAKE
#  optional
# MAKE_OPTS

#set -x

VERSION="0.9.12"
SRCFILE="crengine-ng-${VERSION}.tar.bz2"
SRCDIR="crengine-ng-${VERSION}"
SRCURL="https://gitlab.com/coolreader-ng/crengine-ng/-/archive/${VERSION}/${SRCFILE}"
SHA512="bd400a8b3d6cc0938f82863a995885ce28a5ad02cfedd9429fd603698cd4f2bfb9fe8460cdb737c59c7008004e1c03fc9d1359f3c7223233d4e5380016cff4b0"


source ./make.conf

ANDROID_TOOLCHAIN_PATH_BIN="${ANDROID_NDK_ROOT}/toolchains/llvm/prebuilt/${ANDROID_NDK_HOST_TAG}/bin"
ANDROID_TOOLS_PATH_BIN="${ANDROID_NDK_ROOT}/prebuilt/${ANDROID_NDK_HOST_TAG}/bin:${ANDROID_CMAKE}/bin"
export PATH="${ANDROID_TOOLCHAIN_PATH_BIN}:${ANDROID_TOOLS_PATH_BIN}:${PATH}"

tmpd="./tmp"
libs_prefix_base=`realpath ../../native-libs/prefix`
target_dir_base=`realpath ../../native-libs`

die()
{
	echo $*
	exit 1
}

# lowercase $1
#buildtype=${1,,}
buildtype=`echo $1 | tr '[:upper:]' '[:lower:]'`
if [ "x${buildtype}" = "x" ]
then
	echo "You must specify build type, one of:"
	echo "  release"
	echo "  debug"
	echo "  relwithdebinfo"
	die "Terminated"
fi
clean_mode=no
mode=$2
if [ "x${mode}" = "xclean" ]
then
	clean_mode=yes
fi

cmake_build_type=
prefix_prefix=
builddir_prefix=
case "${buildtype}" in
	release)
		cmake_build_type="Release"
		buildfir_prefix="release"
		prefix_prefix="${target_dir_base}/image"
		;;
	debug)
		cmake_build_type="Debug"
		buildfir_prefix="debug"
		prefix_prefix="${target_dir_base}/image-debug"
		;;
	relwithdebinfo|releasewithdebinfo)
		cmake_build_type="RelWithDebInfo"
		buildfir_prefix="relwithdebinfo"
		prefix_prefix="${target_dir_base}/image-relwithdebinfo"
		;;
	*)
		die "Invalid build type!"
esac

arch_list="arm64-v8a armeabi-v7a x86_64 x86"

arm64_thirdparty="aarch64-linux-android"
armeabi_v7a_thirdparty="arm-linux-androideabi"
x86_64_thirdparty="x86_64-linux-android"
x86_thirdparty="i686-linux-android"


warn()
{
	echo $*
}

_var_value_is_true()
{
	local ret=1
	case "${1}" in
	"yes"|"YES"|"y"|"Y")
		ret=0
		;;
	"true"|"TRUE"|"t"|"T")
		ret=0
		;;
	"on"|"ON")
		ret=0
		;;
	esac
	return $ret
}

do_fetch()
{
	pushd "${tmpd}"
	local exist=
	local valid=
	test -f "${SRCFILE}" && exist=y
	if [ "x${exist}" != "xy" ]
	then
		curl -f -L -O ${SRCURL} || warn "Failed to fetch sources!"
	fi
	test -f "${SRCFILE}" && exist=y
	if [ "x${exist}" = "xy" ]
	then
		# check sha512 summ
		echo "${SHA512} *${SRCFILE}" > "${SRCFILE}.sha512"
		shasum -c "${SRCFILE}.sha512" && valid=y
		rm -f "${tmpd}/${SRCFILE}.sha512"
	fi
	if [ "x${valid}" != "xy" -a "x${exist}" = "xy" ]
	then
		rm -fv "${SRCFILE}"
	fi
	popd
	test "x$valid" = "xy" && return 0 || return 1
}

do_unpack()
{
	pushd "$tmpd"
	local ret=
	test -f "${SRCDIR}" && exist=y
	if [ ! -d "${SRCDIR}" ]
	then
		tar -xvjf ${SRCFILE}
		ret=$?
	else
		ret=0
	fi
	if [ $ret -ne 0 ]
	then
		rm -rfv ./${SRCDIR}
	fi
	popd
	return $ret
}

do_clean()
{
	rm -v *.log
	make clean
}

do_configure()
{
	cmake -G 'Unix Makefiles' \
		-DCMAKE_BUILD_TYPE=${cmake_build_type} \
		-DCMAKE_INSTALL_PREFIX="${prefix}" \
		${cmake_add_args} \
		-DPNG_PNG_INCLUDE_DIR="${libs_prefix}/include/" \
		-DPNG_LIBRARY="${libs_prefix}/lib/libpng16.a" \
		-DJPEG_INCLUDE_DIR="${libs_prefix}/include/" \
		-DJPEG_LIBRARY="${libs_prefix}/lib/libjpeg.a" \
		-DFREETYPE_INCLUDE_DIRS="${libs_prefix}/include/freetype2" \
		-DFREETYPE_LIBRARY="${libs_prefix}/lib/libfreetype.a" \
		-DHarfBuzz_INCLUDE_DIR="${libs_prefix}/include/harfbuzz" \
		-DHarfBuzz_LIBRARY="${libs_prefix}/lib/libharfbuzz.a" \
		-DFRIBIDI_INCLUDE_DIR="${libs_prefix}/include/fribidi" \
		-DFRIBIDI_LIBRARY="${libs_prefix}/lib/libfribidi.a" \
		-DLIBUNIBREAK_INCLUDE_DIR="${libs_prefix}/include/" \
		-DLIBUNIBREAK_LIBRARY="${libs_prefix}/lib/libunibreak.a" \
		-DZSTD_INCLUDE_DIR="${libs_prefix}/include/" \
		-DZSTD_LIBRARY="${libs_prefix}/lib/libzstd.a" \
		-DUTF8PROC_INCLUDE_DIR="${libs_prefix}/include/" \
		-DUTF8PROC_LIBRARY="${libs_prefix}/lib/libutf8proc.a" \
		-DCRE_BUILD_SHARED=OFF \
		-DCRE_BUILD_STATIC=ON \
		-DADD_DEBUG_EXTRA_OPTS=OFF \
		-DDOC_DATA_COMPRESSION_LEVEL=3 \
		-DDOC_BUFFER_SIZE=0x1000000 \
		-DENABLE_LARGEFILE_SUPPORT=ON \
		-DUSE_COLOR_BACKBUFFER=ON \
		-DUSE_LOCALE_DATA=ON \
		-DLDOM_USE_OWN_MEM_MAN=OFF \
		-DWITH_LIBPNG=ON \
		-DWITH_LIBJPEG=ON \
		-DWITH_FREETYPE=ON \
		-DWITH_HARFBUZZ=ON \
		-DWITH_LIBUNIBREAK=ON \
		-DWITH_FRIBIDI=ON \
		-DWITH_ZSTD=ON \
		-DWITH_UTF8PROC=ON \
		-DUSE_GIF=ON \
		-DUSE_NANOSVG=ON \
		-DUSE_CHM=ON \
		-DUSE_ANTIWORD=ON \
		-DUSE_FONTCONFIG=OFF \
		-DUSE_SHASUM=ON \
		-DUSE_CMARK_GFM=OFF \
		-DUSE_MD4C=ON \
		-DBUILD_TOOLS=OFF \
		-DENABLE_UNITTESTING=OFF \
		-DENABLE_LTO=OFF \
		"${tmpd}/${SRCDIR}" 2>&1 | tee conf.log
}

do_make()
{
	make ${MAKE_OPTS} VERBOSE=1 2>&1 | tee make.log
}

do_install()
{
	make install 2>&1 | tee install.log
}

do_strip()
{
	local target1="${prefix}/lib/libcrengine-ng.a"
	local target2="${prefix}/lib/libcrengine-ng.so"
	local strip_exe=
	local strip_args="--strip-debug -v"
	local have_strip=
	case "${arch}" in
	"arm64-v8a")
		strip_exe="${ANDROID_TOOLCHAIN_PATH_BIN}/aarch64-linux-android-strip"
		;;
	"armeabi-v7a")
		strip_exe="${ANDROID_TOOLCHAIN_PATH_BIN}/arm-linux-androideabi-strip"
		;;
	"x86_64")
		strip_exe="${ANDROID_TOOLCHAIN_PATH_BIN}/x86_64-linux-android-strip"
		;;
	"x86")
		strip_exe="${ANDROID_TOOLCHAIN_PATH_BIN}/i686-linux-android-strip"
		;;
	esac
	which ${strip_exe} > /dev/null 2>&1 && have_strip=yes
	if [ "x${have_strip}" != "xyes" ]
	then
		strip_exe="${ANDROID_TOOLCHAIN_PATH_BIN}/llvm-objcopy"
		strip_args="--strip-debug"
	fi
	which ${strip_exe} > /dev/null 2>&1 && have_strip=yes
	if [ "x${have_strip}" != "xyes" ]
	then
		echo "I'm sorry, 'strip' tool not found, skipping..."
		return 0
	fi
	if [ -f "${target1}" ]
	then
		${strip_exe} $strip_args "${target1}" 2>&1 | tee strip-debug.log
	fi
	if [ -f "${target2}" ]
	then
		${strip_exe} $strip_args "${target2}" 2>&1 | tee -a strip-debug.log
	fi
}

mkdir -p "${tmpd}" || die "mkdir failed!"
tmpd=`realpath ${tmpd}`
do_fetch || die "fetch failed!"
do_unpack || die "unpack failed"

for arch in $arch_list
do
	prefix="${prefix_prefix}/${arch}"
	build_dir="${buildfir_prefix}-${arch}"
	if [ ! -d "${build_dir}" ]
	then
		mkdir "${build_dir}" || die "mkdir failed!"
	fi
	pushd "${build_dir}"

	case "${arch}" in
	"arm64-v8a")
		libs_prefix="${libs_prefix_base}/${arm64_thirdparty}"
		;;
	"armeabi-v7a")
		libs_prefix="${libs_prefix_base}/${armeabi_v7a_thirdparty}"
		;;
	"x86_64")
		libs_prefix="${libs_prefix_base}/${x86_64_thirdparty}"
		;;
	"x86")
		libs_prefix="${libs_prefix_base}/${x86_thirdparty}"
		;;
	esac

	export PKG_CONFIG_PATH="${libs_prefix}/lib/pkgconfig"
	cmake_toolchain_file=${ANDROID_NDK_ROOT}/build/cmake/android.toolchain.cmake
	cmake_add_args="-DCMAKE_TOOLCHAIN_FILE=${cmake_toolchain_file} -DANDROID_ABI=${arch} -DANDROID_STL=c++_static"
	if _var_value_is_true "${ANDROID_PAGE_SIZE_16K}"
	then
		cmake_add_args="${cmake_add_args} -DANDROID_SUPPORT_FLEXIBLE_PAGE_SIZES=ON"
	fi
	export cmake_add_args
	export prefix
	export libs_prefix

	if [ "x${clean_mode}" = "xyes" ]
	then
		do_clean || warn "cleanup failed!"
	fi
	do_configure || die "configure failed!"
	do_make || die "make failed!"
	do_install || die "install failed!"
	if [ "x${buildtype}" = "xrelease" ]
	then
		do_strip || die "strip failed!"
	fi

	popd

	# Cleanup
	if [ "x${clean_mode}" = "xyes" ]
	then
		rm -rfv "./${build_dir}"
	fi
done
