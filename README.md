# LxReader
A program for reading e-books for the Android system.

## Description
Open source e-book reader for Android.

To work with e-book formats, the [crengine-ng](https://gitlab.com/coolreader-ng/crengine-ng) library is used.

Supported e-book formats through the use of the crengine-ng library: fb2, fb3 (incomplete), epub (non-DRM), doc, docx, odt, rtf, pdb, mobi (non-DRM), txt, html, Markdown, chm, tcr.

Main features:
* Most complete support for FB2 - styles, tables, footnotes at the bottom of the page
* Extensive font rendering capabilities: use of ligatures, kerning, hinting option selection, floating punctuation, simultaneous use of several fonts, including fallback fonts
* Word hyphenation using hyphenation dictionaries
* Ability to display 2 pages at the same time
* Displaying and Navigating Book Contents
* Ability to use bookmarks
* Reading books directly from a ZIP archive
* TXT auto reformat, automatic encoding recognition
* Flexible styling with CSS files
* Background pictures, textures, or solid background
* Animation of turning pages - like in a paper book or simple shift
* Customizable actions for touch screen zones
* View illustrations with scrolling and zooming - by long pressing on the illustration
* Select text and send it either to the clipboard or to another application, etc.
* Reading aloud with the possibility of using accent dictionary
* Built-in book library with search and/or filtering
* Simple built-in file manager for viewing the list of files on the device (not available in Google Play version)

## Installation
The program can be installed from various sources:
1. GitLab Releases: [link](https://gitlab.com/coolreader-ng/lxreader/-/releases)
2. Google Play: [link](https://play.google.com/store/apps/details?id=io.gitlab.coolreader_ng.lxreader.gplay)
3. F-Droid: [link](https://f-droid.org/packages/io.gitlab.coolreader_ng.lxreader.fdroid/)

All of these variants use different package names, so they can be installed simultaneously on the same device without interfering with each other.

## Authors and acknowledgment
A list of authors and contributors can be found in the AUTHORS file.

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
