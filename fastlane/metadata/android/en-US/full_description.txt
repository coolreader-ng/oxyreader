<p>Open source e-book reader for Android.</p>
<p>Supported e-book formats through the use of the crengine-ng library: fb2, fb3 (incomplete), epub (non-DRM), doc, docx, odt, rtf, pdb, mobi (non-DRM), txt, html, Markdown, chm, tcr.</p>
<p>Main features:</p>
<ul>
<li>Most complete support for FB2 - styles, tables, footnotes at the bottom of the page</li>
<li>Extensive font rendering capabilities: use of ligatures, kerning, hinting option selection, floating punctuation, simultaneous use of several fonts, including fallback fonts</li>
<li>Word hyphenation using hyphenation dictionaries</li>
<li>Ability to display 2 pages at the same time</li>
<li>Displaying and Navigating Book Contents</li>
<li>Ability to use bookmarks</li>
<li>Reading books directly from a ZIP archive</li>
<li>TXT auto reformat, automatic encoding recognition</li>
<li>Flexible styling with CSS files</li>
<li>Background pictures, textures, or solid background</li>
<li>Animation of turning pages - like in a paper book or simple shift</li>
<li>Customizable actions for touch screen zones</li>
<li>View illustrations with scrolling and zooming - by long pressing on the illustration</li>
<li>Select text and send it either to the clipboard or to another application, etc.</li>
<li>Reading aloud with the possibility of using accent dictionary</li>
<li>Built-in book library with search and/or filtering</li>
<li>Simple built-in file manager for viewing the list of files on the device</li>
</ul>
