pluginManagement {
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "lxreader"
include(":app")
include(":genrescollection")
include(":mygesturedetector")
include(":TextValidators")
include(":MyColorSelectorView")
include(":ViewPager2NestedScroll")
include(":MyFlexRowLayout")
