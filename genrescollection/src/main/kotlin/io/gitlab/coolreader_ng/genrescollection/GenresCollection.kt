/*
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.genrescollection

import android.content.Context
import android.content.res.Resources.NotFoundException
import android.util.Log
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.Stack

class GenresCollection private constructor() {

    // main container
    private var mCollection: MutableMap<String, GenreRecord> = LinkedHashMap()

    // key: genre code
    // value: genre record
    private val mAllGenres: MutableMap<String, GenreRecord> = HashMap()
    var version: Int = 0
        private set
    val collection: Map<String, GenreRecord>
        get() = mCollection
    val groups: List<String>
        get() {
            val list = ArrayList<String>()
            for ((key) in mCollection) {
                list.add(key)
            }
            return list
        }

    private fun addGenre(groupId: Int, groupCode: String, groupName: String, id: Int, code: String, name: String) {
        var group = mCollection[groupCode]
        if (null == group) {
            group = GenreRecord(groupId, groupCode, groupName, 0)
            mCollection[groupCode] = group
            mAllGenres[groupCode] = group
        }
        val groupChilds = group.childs
        // Check if already exist in this group
        var exist: GenreRecord? = null
        for (rec in groupChilds) {
            if (rec.code == code) {
                exist = rec
                break
            }
        }
        if (null == exist) {
            // add new child
            var record = mAllGenres[code]
            if (null != record) {
                if (id != record.id) {
                    Log.w(
                        TAG,
                        "genres: trying to add already existing genre '" + code + "' with different id: " + id + " != " + record.id + "!"
                    )
                    Log.w(TAG, "genres: new id will be ignored.")
                    //} else {
                    //	Log.d(TAG, "genres: for genre '" + code + "' duplicate found.");
                }
            } else {
                record = GenreRecord(id, code, name, 1)
                mAllGenres[code] = record
            }
            groupChilds.add(record)
        } else {
            Log.w(TAG, "genres: genre with code '$code' already exist in genre group '$groupCode', skipped.")
        }
    }

    private fun addAliases(genreCode: String, aliases: List<String>) {
        val genre = byCode(genreCode)
        if (null != genre) {
            for (alias in aliases) {
                if (genre.addAlias(alias))
                    mAllGenres[alias] = genre
            }
        } else {
            Log.w(TAG, "No such genre '$genreCode' to register aliases!")
        }
    }

    fun byCode(code: String): GenreRecord? {
        return mAllGenres[code]
    }

    fun byId(strId: String): GenreRecord? {
        var id = -1
        try {
            id = strId.toInt(10)
        } catch (ignored: Exception) {
        }
        return if (id > 0) byId(id) else null
    }

    fun byId(id: Int): GenreRecord? {
        var genre: GenreRecord? = null
        for ((_, value) in mAllGenres) {
            if (value.id == id) {
                genre = value
                break
            }
        }
        return genre
    }

    fun translate(code: String): String {
        val record = mAllGenres[code]
        // If not found, return as is.
        return record?.name ?: code
    }

    private fun loadGenresFromResource(context: Context): Boolean {
        var res = false
        try {
            val parser = context.resources.getXml(R.xml.union_genres)
            // parser data
            val tagStack = Stack<String>()
            var tag: String
            var text: String? = null
            var parentTag: String
            var groupId = -1
            var groupCode: String? = null
            var groupName: String? = null
            var str: String?
            var genreId = -1
            var genreCode: String? = null
            var genreName: String? = null
            var count = 0
            // genre aliases
            var aliasForCode: String? = null
            var aliasCode: String?
            // key: genre code
            // value: list of aliases
            val allAliases = HashMap<String, ArrayList<String>>()

            // start to parse
            parser.next()
            var eventType = parser.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                when (eventType) {
                    XmlPullParser.START_DOCUMENT -> {}
                    XmlPullParser.START_TAG -> {
                        tag = parser.name
                        //Log.d(TAG, "START_TAG: " + tag);
                        parentTag = if (!tagStack.empty()) tagStack.peek() else ""
                        tagStack.push(tag)
                        if ("genres" == tag) {
                            if (parentTag.isEmpty()) {
                                // root tag found, clearing genre's collection
                                mCollection = LinkedHashMap()
                                version = -1
                                str = parser.getAttributeValue(null, "version")
                                if (null != str) {
                                    try {
                                        version = str.toInt(10)
                                    } catch (e: Exception) {
                                        throw XmlPullParserException(e.toString())
                                    }
                                }
                                if (version < 1)
                                    throw XmlPullParserException("Invalid resource version: $str")
                            } else {
                                throw XmlPullParserException("the element 'genres' must be the root element!")
                            }
                        } else if ("group" == tag) {
                            if ("genres" == parentTag) {
                                groupCode = parser.getAttributeValue(null, "code")
                                groupName = parser.getAttributeValue(null, "name")
                                if (null != groupName) {
                                    groupName = resolveStringResource(context, groupName)
                                }
                                groupId = -1
                                str = parser.getAttributeValue(null, "id")
                                if (null != str) {
                                    groupId = try {
                                        str.toInt(10)
                                    } catch (e: Exception) {
                                        throw XmlPullParserException(e.toString())
                                    }
                                }
                                if (groupId < 0)
                                    throw XmlPullParserException("Invalid group id: $str")
                            } else {
                                throw XmlPullParserException("the 'group' element must only be inside the 'genres' element!")
                            }
                        } else if ("genre" == tag) {
                            if ("group" == parentTag) {
                                genreCode = parser.getAttributeValue(null, "code")
                                genreName = parser.getAttributeValue(null, "name")
                                if (null != genreName && genreName.isNotEmpty()) {
                                    genreName = resolveStringResource(context, genreName)
                                }
                                genreId = -1
                                str = parser.getAttributeValue(null, "id")
                                if (null != str) {
                                    genreId = try {
                                        str.toInt(10)
                                    } catch (e: Exception) {
                                        throw XmlPullParserException(e.toString())
                                    }
                                }
                                if (genreId < 0)
                                    throw XmlPullParserException("Invalid genre id: $str")
                            } else {
                                throw XmlPullParserException("the 'genre' element must only be inside the 'group' element!")
                            }
                        } else if ("aliases" == tag) {
                            aliasForCode = if ("genres" == parentTag) {
                                null
                            } else {
                                throw XmlPullParserException("the 'aliases' element must only be inside the 'genres' element!")
                            }
                        } else if ("alias" == tag) {
                            aliasForCode = if ("aliases" == parentTag) {
                                parser.getAttributeValue(null, "code")
                            } else {
                                throw XmlPullParserException("the 'alias' element must only be inside the 'aliases' element!")
                            }
                        }
                        text = ""
                    }
                    XmlPullParser.END_TAG -> {
                        //Log.d(TAG, "END_TAG: " + parser.getName());
                        tag = if (!tagStack.empty()) tagStack.pop() else ""
                        if (tag != parser.name) {
                            throw XmlPullParserException("end element '" + parser.name + "' not equal to start element '" + tag + "'")
                        }
                        when (tag) {
                            "genre" -> {
                                if (null != groupCode && groupCode.length > 0 && null != groupName && groupName.length > 0 && null != genreCode && genreCode.length > 0 && null != genreName && genreName.length > 0) {
                                    addGenre(groupId, groupCode, groupName, genreId, genreCode, genreName)
                                    count++
                                }
                                genreId = -1
                                genreCode = null
                                genreName = null
                            }
                            "group" -> {
                                groupId = -1
                                groupCode = null
                                groupName = null
                            }
                            "alias" -> {
                                // save alias to temporary container
                                aliasCode = text
                                if (null != aliasForCode && aliasForCode.isNotEmpty() && null != aliasCode && aliasCode.isNotEmpty()) {
                                    var aliases = allAliases[aliasForCode]
                                    if (null == aliases) {
                                        aliases = ArrayList()
                                        allAliases[aliasForCode] = aliases
                                    }
                                    // don't check already exist aliases here
                                    // checked in GenreRecord.addAlias()
                                    aliases.add(aliasCode)
                                }
                                aliasForCode = null
                            }
                        }
                    }
                    XmlPullParser.TEXT -> {
                        //Log.d(TAG, "TEXT: " + parser.getText());
                        text = parser.text
                    }
                }
                eventType = parser.next()
            }
            // Only after all genres are registered, add aliases
            for ((key, value) in allAliases) {
                addAliases(key, value)
            }
            res = count > 0
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        } catch (e: NotFoundException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        return res
    }

    companion object {
        private const val TAG = "genre"

        // instance
        private var mInstance: GenresCollection? = null

        fun getInstance(context: Context): GenresCollection {
            if (null == mInstance) {
                mInstance = GenresCollection()
                mInstance!!.loadGenresFromResource(context)
            }
            return mInstance!!
        }

        fun reloadGenresFromResource(context: Context): Boolean {
            if (null == mInstance) {
                mInstance = GenresCollection()
                return mInstance!!.loadGenresFromResource(context)
            }
            return mInstance!!.loadGenresFromResource(context)
        }

        private fun resolveStringResource(context: Context, resCode: String?): String? {
            if (null != resCode && resCode.startsWith("@")) {
                try {
                    val resId = resCode.substring(1).toInt(10)
                    if (resId != 0) {
                        val str = context.getString(resId)
                        if (str.isNotEmpty())
                            return str
                    }
                } catch (ignored: NumberFormatException) {
                    // ignore this exception, return original string
                }
            }
            return resCode
        }
    }
}