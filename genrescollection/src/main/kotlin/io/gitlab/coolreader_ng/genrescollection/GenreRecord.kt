/*
 * Copyright (C) 2024 by Aleksey Chernov <valexlin@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package io.gitlab.coolreader_ng.genrescollection

class GenreRecord internal constructor(val id: Int, val code: String, var name: String, val level: Int) {
    val childs: MutableList<GenreRecord> = ArrayList()
    private val mAliases: MutableList<String> = ArrayList()
    val aliases: List<String>
        get() = mAliases

    fun hasChilds(): Boolean {
        return childs.isNotEmpty()
    }

    fun contain(code: String): Boolean {
        if (this.code == code) return true
        for (record in childs) {
            if (record.code == code) return true
        }
        return false
    }

    fun hasAliases(): Boolean {
        return mAliases.isNotEmpty()
    }

    internal fun addAlias(alias: String): Boolean {
        var exist = false
        for (a in mAliases) {
            if (a == alias) {
                exist = true
                break
            }
        }
        if (!exist) {
            mAliases.add(alias)
        }
        return !exist
    }

    override fun toString(): String {
        if (hasChilds())
            return "GenreRecord[id:$id; code:$code; name:\"$name\"; level: $level; [childs: $childs]]"
        return "GenreRecord[id:$id; code:$code; name:\"$name\"; level: $level]"
    }
}